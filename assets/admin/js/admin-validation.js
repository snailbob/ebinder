
//
// setTimeout(function(){
//   if($('.modal.in').length == 0 && location.hash != '#quote'){
//     $('#myLoadingModal').modal('show');
//   }
// }, 0);


// JavaScript Document
var setComponentRestrictions = {};

$(document).ready(function() {
	//live validate on form fields
	// $(document).on('keyup blur', '.form-control',function(){
	//       $(this).valid();
	// });


	jQuery.extend(jQuery.validator.messages, {
		required: '<i class="fa fa-times text-danger"></i>',
		remote: '<i class="fa fa-times text-danger"></i>',
		email: '<i class="fa fa-times text-danger"></i>',
		url: '<i class="fa fa-times text-danger"></i>',
		date: '<i class="fa fa-times text-danger"></i>',
		dateISO: '<i class="fa fa-times text-danger"></i>',
		number: '<i class="fa fa-times text-danger"></i>',
		digits: '<i class="fa fa-times text-danger"></i>',
		creditcard: '<i class="fa fa-times text-danger"></i>',
		equalTo: '<i class="fa fa-times text-danger"></i>',
		accept: '<i class="fa fa-times text-danger"></i>',
		maxlength: '<i class="fa fa-times text-danger"></i>',
		minlength: '<i class="fa fa-times text-danger"></i>',
		rangelength: '<i class="fa fa-times text-danger"></i>',
		range: '<i class="fa fa-times text-danger"></i>',
		max: '<i class="fa fa-times text-danger"></i>',
		min: '<i class="fa fa-times text-danger"></i>',
		pattern: '<i class="fa fa-times text-danger"></i>',
	});

	$.validator.setDefaults({
		onkeyup: function(element, ev) {
			var TABKEY = 9;
			if(ev.keyCode != TABKEY) {
				$(element).valid();
			}
			console.log(ev);
		},
		// onfocusout: function(element) {
	  //   $(element).valid();
	  // },
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		success: function(label) {
			label.addClass("valid").html('<i class="fa fa-check text-success"></i>');
			console.log(label);
	  },

  });




	$( "#sortable" ).sortable();
	$( "#sortable" ).disableSelection();

	$('#faq_sortable').sortable({
	    axis: 'y',
			handle: '.fa-arrows-alt',
	    update: function (event, ui) {
	        var data = $(this).sortable('serialize');
					var table = $(this).data('table');

					var tb = {
						"table": table
					};
					data = data + "&" + $.param(tb);
					console.log(data);
	        // POST to server using $.post or $.ajax
	        $.ajax({
	            data: data,
	            type: 'POST',
	            url: base_url+'formsubmits/sort_faq',
							dataType: 'json',
							success: function(res){
								console.log(res);
							},
							error: function(err){
								console.log(err);

							}
	        });
	    }
	});


	//request for login, signup, add record
	var submitForm = function(passto, data, form){ //(submit link, data to pass, form selector)
		$(form).find('button[type="submit"]').button('loading');

		$.post(
			base_url+'formsubmits/'+passto,
			data,
			function(res){
				console.log(res);
				console.log(passto);

				if(res.result == 'ok'){
					if(passto == 'login'){
						$(form).find('.btn[type="submit"]').html('<i class="fa fa-check-circle"></i> Success!');
						setTimeout(function(){
							$(form).find('.btn[type="submit"]').html('<i class="fa fa-spinner fa-pulse"></i> Success!');


							//check if user from buy form
							if($(form).find('[name="refer_from"]').val() != ''){ // && res.userdata.stripe_id != ''
								$('.quote_continue_btn').data('id', res.userdata.id);

								var $single_form = $('#single_purchase_form');
								var udata = res.userdata;

								//generate only for regular customers
								if(udata.customer_type == 'Y'){
									$single_form.find('[name="first"]').val(udata.first_name);
									$single_form.find('[name="last"]').val(udata.last_name);
									$single_form.find('[name="bname"]').val(udata.business_name);
									$single_form.find('[name="address"]').val(udata.address);
									$single_form.find('[name="lat"]').val(udata.lat);
									$single_form.find('[name="lng"]').val(udata.lng);
									$single_form.find('[name="email"]').val(udata.email);
									$single_form.find('[name="country_code"]').val(udata.country_id);
									$single_form.find('.addon-shortcode').html('+'+udata.calling_code);
									$single_form.find('[name="mobile"]').val(udata.calling_digits);

									$single_form.find('[name="country_code"]').selectpicker('refresh');

								}



								$('.modal').modal('hide');

								var $mdl = $('#buyNowModal');
								var linksource = $(form).find('[name="from_buyform"]').val();
								if(linksource == 'req_buy_form'){

									//proceed to next tab
									var $tabpane = $('#tab-transit');
									var $progressbar = $('#progressbar');
									var premium = $('#buy_form').find('[name="premium"]').val();
									$mdl.find('.well.well-premium h1').html('$'+premium);
									$mdl.find('.show_form_btn').data('premium', premium);

									$tabpane.removeClass('active').next().addClass('active');
									$progressbar.find('.active').removeClass('active').addClass('done-muted done').next().addClass('active');

									$mdl.modal('show');
								}

								$mdl.modal('show');

								console.log('refer from buy form');
								if(res.userdata.stripe_id != ''){
									$('.payment_form_buy').removeClass('hidden');
									$('.payment_form_buy').siblings('form').addClass('hidden');
								}
								else{
									$('.payment_form_buy').addClass('hidden');
									$('.payment_form_buy').siblings('form').removeClass('hidden');
								}

							}
							else{
								if(res.userdata.super_admin == 'Y'){
									window.location.href = base_url+'webmanager';
								}
								else{
									window.location.href = base_url+'panel';
								}

	//							if(res.userdata.customer_type == 'Y'){
	//								window.location.href = base_url+'business';
	//							}
	//							else{
	//								window.location.href = 'http://'+res.userdata.domain+'.ebinder.com.au';//base_url+'dashboard';
	//							}
							}
						}, 700);

					}
					else if(passto == 'signup'){
						$('.row-signup').html(res.signup_success_view);
					}
					else if(passto == 'activate_domain'){
						window.location.href = base_url + 'login';
	//					if(res.type == 'customer'){
	//						window.location.href = base_url+'business';
	//					}
	//					else{
	//						window.location.href = 'http://'+res.domain+'.ebinder.com.au';//base_url+'dashboard';
	//					}
					}
					else{
						$(form).find('button[type="submit"]').html('<i class="fa fa-check-circle"></i> Success!');

						if(passto == 'update_agency' || passto == 'update_password' || passto == 'update_customer' || passto == 'update_student'){
							bootbox.alert('<h4>'+res.message+'</h4>');
							$(form).find('.btn').button('reset');
							console.log('sf');
						}
						else if(passto == 'customer_update_password' || passto == 'agency_update_password'){
							console.log('yea');
							bootbox.alert('<h4>'+res.message+'</h4>', function(){
								$('#loginModal').modal('show');
							});
						}
						else if(passto != 'signup' && passto != 'forgotpass'){
							window.location.reload(true);
						}
						else{
							$('.modal').modal('hide');
							bootbox.alert('<h4>'+res.message+'</h4>');
						}

					}
				}
				else{
					if(passto == 'login'){
	//					$(form).closest('.modal').find('.alert span.my-text').html(res.message);
	//					$(form).closest('.modal').find('.alert').removeClass('hidden alert-success alert-info').addClass('alert-danger');
						bootbox.alert('<h4>'+res.message+'</h4>');
						$(form).find('.btn').button('reset');
					}
					else{
						bootbox.alert('<h4>'+res.message+'</h4>');
						$(form).find('.btn').button('reset');
						console.log('ahh');
					}

				}

			},
			'json'
		).error(function(err){
			console.log(err);
		});


	};

	//confirm message to delete record
//	$(document).on('click', '.delete_btn', function(){
//		var $self = $(this);
//		var id = $self.data('id');
//		var table = $self.data('table');
//
//		bootbox.confirm('<h4>Confirm you wish to delete selected item.</h4>', function(e){
//			if(e){
//				//alert(id);
//				deleteRecord($self, id, table);
//			}
//		});
//		return false;
//	});

	if($(window).width() < 992){
		var $main_container = $('.the_main_container');
		$main_container.addClass('side-collapsed');
	}

	//auto toggle list-group-item
	$('#sideAccordion').find('a.active').closest('.group-items').children('a').find('i').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
	$('#sideAccordion').find('a.active').closest('.group-items').find('.list-group').first().addClass('in');


  $('.toggle_sidenav_expand').on('click', function(){
    var $self = $(this);
    var $sideNav = $('.sidebar_nav');
    var $main_container = $('.the_main_container');
    var $main_content = $('.main_content');
    $self.find('i').toggleClass('fa-caret-left fa-caret-right');

    var status = ($main_container.hasClass('side-collapsed')) ? 'expanded' : 'collapsed';

    if($main_container.hasClass('side-collapsed')){
      $main_container.removeClass('side-collapsed');
    }
    else{
      $main_container.addClass('side-collapsed');
    }

    //save to session
    $.post(
     base_url+'formsubmits/sidenav_settings',
     {status: status},
     function(res){
       console.log(res);
     },
     'json'
    ).error(function(err){
      console.log(err);
    });


    return false;
  });



	$('.show_history_btn').on('click', function(){
		var $self = $(this);
		var data = $self.data();
		var $mdl = $('#historyModal');
		$mdl.modal('show');
		$mdl.find('.history-content').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin"></i></p>');

		$.post(
			base_url+'formsubmits/get_agency_log',
			data,
			function(res){
				console.log(res);
				$mdl.find('.history-content').html(res.view);
			},
			'json'
		).error(function(err){
			console.log(err);
		});


		return false;
	});

	// Opens the sidebar menu
	$(".nav-toggle").click(function(e) {
		var $self = $(this);
		console.log('nav-toggle');
		e.preventDefault();
		$("#sidebar-wrapper").toggleClass("active");
		$self.toggleClass( "active" );

		$self.find('i').addClass('fa-spin');
		setTimeout(function(){
			$self.find('i').removeClass('fa-spin');
		}, 300);

//		var $tgl = $('#nav-toggle');
//
//		if($tgl.hasClass('active')){
//			$('.xmenu-background').css({background: 'rgba(255, 255, 255, .95)'});
//		}
//		else{
//			$('.xmenu-background').css({background: 'transparent'});
//		}


	});


	//resize cover
	function jqUpdateSize(){
		// Get the dimensions of the viewport
		var width = $(window).width();
		var height = $(window).height();
		var banner_offset = 96;
		var new_height = height - banner_offset;
		//$('#jqWidth').html(width);      // Display the width
		$('.banner-blue').css({height: new_height+'px'});    // Display the height
		var new_height2 = height - 91;
		$('#sidebar-wrapper').css({height: new_height2+'px'});
	};
	$(document).ready(jqUpdateSize);    // When the page first loads
	$(window).resize(jqUpdateSize);     // When the browser changes size

	$('.navbar-static-top').css({position: 'fixed', width: '100%', top: '0'});
	$('.main_container_admin').css({'padding-top': '120px'});


	$(document).on('click', '.update_valmess_btn', function(e){
		var id = $(this).data('id');
		var err = $(this).data('err');
		var conf = $(this).data('conf');
		var succ = $(this).data('succ');
		var desc = $(this).data('desc');
		var $self = $('#valmess_form');
		$self.find('input').val('');

		$self.find('[name="success"]').val(succ);
		$self.find('[name="confirm"]').val(conf);
		$self.find('[name="description"]').val(desc);
		$self.find('[name="id"]').val(id);
		$self.find('[name="error"]').val(err);


		$('#valMessModal').modal('show');

		return false;
	});


	$('#valmess_form').on('submit', function(){
		var $self = $(this);
		$self.find('.btn-primary').addClass('disabled').html('Saving changes..');

		console.log($self.serialize());
		$.post(
			base_url+'webmanager/popups/addupdate',
			$self.serialize(),
			function(res){
				console.log(res);
				window.location.reload(true);
			},
			'json'
		).error(function(err){
			console.log(err);
		});

		return false;
	});



	$(document).on('click', '.renewal-advice', function(){
		var id = $(this).data('id');
		console.log(id);

		bootbox.confirm('Confirm sending renewal advice email to broker.', function(e){
			if(e){
				$.post(
					base_url+'formsubmits/renewal_advice',
					{id: id},
					function(res){
						console.log(res);
						bootbox.alert('Renewal advice email successful sent.');
					},
					'json'
				).error(function(res){
					console.log(res);
				});


			}
		});
	});

	$(document).on('click', '.show-biz-response', function(){
		var id = $(this).data('id');
		console.log(id);
		$('#formModal').modal('show');
		$('.the_referral_response').html('<h2 class="text-center"><i class="fa fa-spinner fa-spin"></i></h2>');

		$.post(
			base_url+'webmanager/referral/get_biz_referral',
			{id: id},
			function(res){
				console.log(res);
				$('.the_referral_response').html(res.theview);
			},
			'json'
		).error(function(res){
			console.log(res);
		});

	});


	$('[name="product_id[]"], [name="access_rights[]"], [name="attributes[]"], [name="widget[]"]').on('change', function(){
		var $self = $(this);
		if($self.is(':checked')){
			$self.siblings('span').html('<i class="fa fa-trash"></i>');
		}
		else{
			$self.siblings('span').html('Add');
		}
		console.log($self.is(':checked'));
	});

	$('.reset-inline-checkbox').on('click', function(){
		var $self = $(this);
		$self.closest('div').find('[type="radio"]').prop('checked', false);
		return false;
	});


	//load #myActivityLogModal
	var $myActivityLogModal = $('#myActivityLogModal');
	var activityLogged = $myActivityLogModal.data('logged');
	if(activityLogged != 'Y'){
		$myActivityLogModal.modal('show');
	}

	$('.claim-chat-send').on('click', function(){
		var $self = $(this);
		var $chat = $('.chat-area .chat');
		var height = $chat[0].scrollHeight;
		var id = $self.data('id');
		var to = $self.data('to');
		var message_id = $self.data('message_id');
		var letter = $self.data('letter');
		var $textfield = $('.textfield');
		if($textfield.val() != ''){
			var html = '<div class="chat__item"><div class="chat__item-wrap"><div class="avatar"><div class="avatar__img">';
			html += '<img src="https://placeholdit.imgix.net/~text?txtsize=150&txt='+letter+'&w=200&h=200">';
			html += '</div></div><div class="chat__item-message-wrap"><div class="triangle"></div><div class="chat__item-message">';
			html += $textfield.val();
			html += '</div></div></div><div class="chat__date"><span>';

			$self.button('loading');

			$.post(
				base_url+'formsubmits/claim_submit_respond',
				{id: id, text: $textfield.val(), message_id: message_id, to: to},
				function(res){
					console.log(res);

					html += res.date;
					html += '</span><i class="fa fa-check-circle fa-fw text-success"></i></div></div>';
					$chat.append(html);
					$chat.scrollTop(height);
					$self.button('reset');
					$textfield.val('');
				},
				'json'
			).error(function(err){
				console.log(err);
			});

		}
	});

	$(document).on('click', '.assign_claim_btn', function(){
			var $self = $(this);
			var mdl = $self.data('modal');
			var id = $self.data('id');
			$(mdl).find('[name="id"]').val(id);
			$(mdl).modal('show');
	});


  $('#update_password_form').validate({
    ignore: '',
		rules: {
			pww: {
				minlength: 6
			},
			pww2: {
				required: true,
				equalTo: '#pww'
			},
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			var data = $(form).serialize();
			console.log(data);

      var verd = $('.password-verdict').text();

			if(verd == 'Normal' || verd == 'Weak' || verd == 'Very Weak'){
				bootbox.alert('Please assign atleast Medium strength password.');
				return false;
			}

      $(form).find('button').button('loading');

      $.post(
				base_url+'formsubmits/customer_update_password',
				$(form).serialize(),
				function(res){
					console.log(res);
          bootbox.alert(res.message);
          $(form).find('button').button('reset');
				},
				'json'
			).error(function(err){
				console.log(err);
			});


		}

	});



	$('#claim_assign_form').validate({
		ignore: [],
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');
			$.post(
				base_url+'formsubmits/claim_assign_user',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);

				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});

	$('.claims-action-btn a').on('click', function(){
		var $self = $(this);
		var stat = $self.data('status');
		var claim_id = $self.closest('.claims-action-btn').data('id');
		var text = $self.text();
		console.log(stat);
		bootbox.confirm('<h4>Confirm change status to '+text+'?</h4>', function(e){
			if(e){
				console.log('yea');

				$.post(
					base_url+'formsubmits/claim_change_status',
					{stat: stat, text: text, id: claim_id},
					function(res){
						console.log(res);
						window.location.reload(true);
						//bootbox.alert('Password didn\'t match. Please try again.');
					},
					'json'
				).error(function(err){
					console.log(err);
				});
			}
		});
	});

	$(document).on('click', '.referral_decision', function(){
		var $self = $(this);
		var decision = $self.data('decision');
		var id = $self.data('id');

		var message = (decision == 'A') ? 'Accept referral quote?' : 'Reject Referral Quote?';
		bootbox.confirm(message, function(e){
			if(e){

			}
		});
	});

  $(document).on('click', '.referral_note_btn', function(){
    var $self = $(this);
    var txt = $self.data('txt');
    var id = $self.data('id');
    var $mdl = $('#referralNoteModal');

    $mdl.find('[name="id"]').val(id);
    $mdl.find('[name="note"]').val(txt);
    $mdl.modal('show');

  });


	$('#note_referral_form').validate({
		ignore: [],
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			//  element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('[type="submit"]').button('loading');
			$.post(
				base_url+'formsubmits/note_referral_form',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);

				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});


	$('.producttab_btn').on('click', function(){
		var $self = $(this);
		var data = $self.data();
		var prod = data.id;
		var $menu = $('#menu-tab2');
		var $ul = $('.list-group-root');

		var prod_info = $ul.find('.producttab_btn[data-id="'+prod+'"]').data('info');
		$menu.removeClass('hidden');

		var $prod_btns = $('.div_product_actions').find('a');

		$prod_btns.data('id', prod);
		$prod_btns.data('info', prod_info);

		$ul.find('.list-group-item').removeClass('active');
		$ul.find('.producttab_btn[data-id="'+prod+'"]').addClass('active');
	});

	$('.producttab_btn_close').on('click', function(){
		var $menu = $('#menu-tab2');
		$menu.addClass('hidden');
	});



	$('#rating_product_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			 //element.closest('div').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button').button('loading');
			$.post(
				base_url+'formsubmits/rating_product_update',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);

				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});



	$('#password_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			 //element.closest('div').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button').button('loading');
			$.post(
				base_url+'formsubmits/activity_logged_pass',
				$(form).serialize(),
				function(res){
					console.log(res);
					if(res.result == 'match'){
						window.location.reload(true);
					}
					else{
						bootbox.alert('Password didn\'t match. Please try again.');
						$(form).find('button').button('reset');
					}
				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});

	$('#widget_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			var checked_count = $(form).find('[type="checkbox"]:checked').length;
			if(checked_count == 0){
				bootbox.alert('No widget selected.');
				return false;
			}


			$(form).find('button[type="submit"]').button('loading');

			$.post(
				base_url+'formsubmits/save_widget',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);
//					$(form).closest('.modal').modal('hide');
//
//					setTimeout(function(){
//						bootbox.alert('Successfully saved details.', function(){
//							window.location.href = base_url+ 'panel/quote'; //(true);
//						});
//						$(form).find('button[type="submit"]').html('Submitted!');
//					}, 600);
				},
				'json'
			).error(function(err){
				console.log(err);
				$(form).find('button[type="submit"]').button('reset');
			});

		}
   });


	$('.days_filter').on('change', function(){
		var $self = $(this);
		var val = $self.val();
		console.log(val);
		window.location.href = base_url + 'webmanager/dashboard/?day='+val;
	});

   $('#biz_referral_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');

			$.post(
				base_url+'formsubmits/referral_required',
				{data: $('#homebiz_form').serializeArray(), customer: $(form).serializeArray()},
				function(res){
					console.log(res);
						$(form).closest('.modal').modal('hide');

						setTimeout(function(){
							bootbox.alert('Successfully saved details.', function(){
								window.location.href = base_url+ 'panel/quote'; //(true);
							});
							$(form).find('button[type="submit"]').html('Submitted!');
						}, 600);
				},
				'json'
			).error(function(err){
				console.log(err);
				$(form).find('button[type="submit"]').button('reset');
			});

		}
   });



	//Form faq_form validate
	$('#faq_form').validate({
		rules: {
			question: {
				required: true
			},
			answer: {
				required: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type=submit]').button('loading');


			$.ajax({
				  type: "POST",

				  url: base_url+"webmanager/contents/add_faq",

				  data: $(form).serialize(),

				  success: function(data) {
						console.log(data);
						window.location.reload(true);
				  },
				  error: function(err){
					  console.log(err);
				  }
			});

		}
	});

	$('#the-referral-biz-form').validate({
		ignore: [],
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');
			$.post(
				base_url+'formsubmits/admin_referral',
				{data: $(form).serializeArray()},
				function(res){
					console.log(res);
					bootbox.alert('Successfully saved referral.');
					$(form).find('button[type="submit"]').button('reset');
				},
				'json'
			).error(function(err){
				console.log(err);
				$(form).find('button[type="submit"]').button('reset');
			});

		}
	});

	$(document).on('click', '.brokerage-whitelabel', function(){
		var $self = $(this);
		var id = $self.data('id');
		var white = $self.data('white');
		var stat = (white == 'Y') ? 'disable' : 'enable';
		bootbox.confirm('Confirm '+stat+' white labelling to brokerage ?', function(e){
			if(e){
				console.log(stat);
				$.post(
					base_url+'formsubmits/brokerage_whitelabel',
					{id: id, white: white},
					function(res){
						console.log(res);
						window.location.reload(true);
					},
					'json'
				).error(function(res){
					console.log(res);
				});
			}
		});
	});

	$(document).on('click', '.broker-white-link', function(){
		var $self = $(this);
		var id = $self.data('id');

		bootbox.confirm('Send broker unique white label link?', function(e){
			if(e){
				console.log('sdf');

				$.post(
					base_url+'formsubmits/send_broker_white_link',
					{id: id},
					function(res){
						console.log(res);
						bootbox.alert('Email sent successfully.');
					},
					'json'
				).error(function(res){
					console.log(res);
				});

			}
		});
	});


	$(document).on('click', '.view_details_btn', function(){
		var $mdl = $('#infoModal');
		var $self = $(this);
		var info = $self.data('info');
		var $container = $mdl.find('.the-info');
		console.log(info);

		var mess = 'Name: ' + info.company_name;
		mess += '<br>Address: ' + info.address;
		mess += '<br>Date added: ' + info.date_added;
		mess += '<br><br><strong>Primary contact</strong><br>First Name: ' + info.first;
		mess += '<br>Last: ' + info.last;
		mess += '<br>Email: ' + info.email;
		mess += '<br>Phone: ' + info.phone;
		mess += '<br>Unique Link: ' + base_url+ 'landing/brokerform/'+info.id;
		$container.html(mess);
//		for (var key in info) {
//		  if (info.hasOwnProperty(key)) {
//			$mdl.find('[name="'+key+'"]').val(info[key]);
//			console.log(key + " -> " + info[key]);
//		  }
//		}
		$mdl.modal('show');
	});

	$(document).on('click', '.update-product', function(){
		var $mdl = $('#newProductModal');
		var $self = $(this);
		var info = $self.data('info');
		console.log(info);
		for (var key in info) {
		  if (info.hasOwnProperty(key)) {
			$mdl.find('[name="'+key+'"]').val(info[key]);
			console.log(key + " -> " + info[key]);
		  }
		}
		$mdl.modal('show');
	});

	$(document).on('click', '.update-promocode', function(){
		var $mdl = $('#formModal');
		var $self = $(this);
		var info = $self.data('info');
		console.log(info);
		for (var key in info) {
		  if (info.hasOwnProperty(key)) {
			$mdl.find('[name="'+key+'"]').val(info[key]);
			console.log(key + " -> " + info[key]);
		  }
		}
		$mdl.modal('show');
	});

	$('.content_text').on('click',function(){
		$(this).hide();
		$('.edit_content').show();
		$('.edit_content').focus();
		$('.submit_content').show();
	});


	$('[name="ctype"]').bootstrapSwitch({
		onText: 'Customer',
		offText: 'Agency',
		onColor: 'info',
		offColor: 'info',
		labelWidth: 15,
	});

	$(document).keyup(function(e) {
	  if (e.keyCode == 27) {
		$('.edit_content').hide();
		$('.content_text').show();
		$('.submit_content').hide();
		   }   // esc
	});




	$(document).on('click', '.underwriter-first-user', function(){
		var $self = $(this);
		var id = $self.data('id');
		var info = $self.data('info');
		var to = $self.data('to');
		var agency_id = $self.data('agency_id');
		console.log(info);

		var txt = (to == 'Y') ? 'Confirm make user first underwriter?' : 'Confirm anassign first underwriter?';
		bootbox.confirm(txt, function(e){
			if(e){
				$.post(
					base_url+'formsubmits/make_first_underwriter',
					{id: id, agency_id: agency_id, to: to},
					function(res){
						console.log(res);
						bootbox.alert(res.message, function(){
							window.location.reload(true);
						});
					},
					'json'
				).error(function(res){
					console.log(res);
				});
			}
		});

	});

	  $('.list-group-item').on('click', function() {
		$('.glyphicon', this)
		.toggleClass('glyphicon-chevron-up')
	  .toggleClass('glyphicon-chevron-down');
	  });

	$(document).on('click', '.brokerage-update', function(){
		var $self = $(this);
		var id = $self.data('id');
		var arr = $self.data('info');
		var $form = $('#brokerage_form');
		$form.find('[name="id"]').val(id);
		$form.find('[name="company_name"]').val(arr.company_name);
		$form.find('[name="address"]').val(arr.address);
		$form.find('[name="lat"]').val(arr.lat);
		$form.find('[name="lng"]').val(arr.lng);
		$form.find('[name="first"]').val(arr.first);
		$form.find('[name="last"]').val(arr.last);
		$form.find('[name="email"]').val(arr.email);
		$form.find('[name="phone"]').val(arr.phone);
		//$form.find('.row-prime-contact').hide();

		$form.closest('.modal').modal('show');
		console.log(arr);
	});


	$('.show_mdl_form_btn').on('click', function(){
		var $self = $(this);
		var mdl = $self.data('controls-modal');
		$(mdl).modal('show');
		$(mdl).find('.edit_actions').hide();
		$(mdl).find('.form-control').val('');
		$(mdl).find('[name="id"]').val('');
		$(mdl).find('[name="broker_id"]').val('');
		$(mdl).find('[name="product_id[]"]').find('span').html('Add');
		$(mdl).find('form').removeClass('hidden').siblings('div').addClass('hidden');
		return false;
	});

	$('.user_edit_btn').on('click', function(){
		var $self = $(this);
		var mdl = $self.data('modal');
		var is_edit = $(mdl).find('form').hasClass('hidden');
		$(mdl).find('form').toggleClass('hidden').siblings('div').toggleClass('hidden');
		$(mdl).find('.edit_actions').show();
		if(is_edit){
			$self.html('Cancel');
		}
		else{
			$self.html('<i class="fa fa-edit"></i> Edit');
		}
	});

	//for brokers with brokerage details update
	$(document).on('click', '.customer-update2', function(){
		var $self = $(this);
		var id = $self.data('id');
		var arr = $self.data('arr');
		var $form = $('#brokerage_form');
		console.log(arr)
		$form.closest('.modal').find('h1').html('Edit Broker');
		$form.closest('.modal').find('.delete_btn_confirm').attr('data-id', arr.brokerage.id);
		$form.find('[name="id"]').val(arr.brokerage.id);
		$form.find('[name="broker_id"]').val(arr.id);
		$form.find('[name="first"]').val(arr.first_name);
		$form.find('[name="last"]').val(arr.last_name);
		$form.find('[name="brokerage_id"]').val(arr.brokerage_id);
		$form.find('[name="email"]').val(arr.email);
		$form.find('[name="brokerage_id"]').selectpicker('refresh');
		$form.find('[name="agency_id"]').val(arr.agency_id);
		$form.find('[name="agency_id"]').closest('.form-group').hide();

		$form.find('[name="mobile"]').val(arr.calling_digits);
		$form.find('[name="country_code"]').val(arr.country_id);

		var short = $('.select_country_tbl[data-id="'+arr.country_id+'"]').data('short');
		short = (short !== null) ? short : 'Select Country Code';
		console.log(short);
		$form.find('.country_btn').html(short);

		$form.find('[name="company_name"]').val(arr.brokerage.company_name);
		$form.find('[name="suite_number"]').val(arr.suite_number);
		$form.find('[name="address"]').val(arr.location);
		$form.find('[name="lat"]').val(arr.lat);
		$form.find('[name="lng"]').val(arr.lng);
		// $(form).find('[name="enable_whitelabel"]').val(arr.brokerage.enable_whitelabel);

		$form.find('[name="enable_whitelabel"]').bootstrapSwitch('state', false);
		var enable_whitelabel = 'No';
		if(arr.brokerage.enable_whitelabel == 'Y'){
			enable_whitelabel = 'Yes';
			$form.find('[name="enable_whitelabel"]').bootstrapSwitch('state', true);
		}




		$form.find('[name="product_id[]"]').prop('checked', false).closest('label').removeClass('active btn-primary').addClass('btn-default');
		$form.find('[name="product_id[]"]').closest('label').find('span').html('Add');

		if(arr.privileges.length > 0){
			$form.find('[name="access_rights[]"]').prop('checked', false).closest('label').addClass('btn-default').removeClass('active btn-primary');
			for(var i = 0; i < arr.privileges.length; i ++ ){
				$form.find('[value="'+arr.privileges[i]+'"]').prop('checked', true).closest('label').removeClass('btn-default').addClass('active btn-primary');
				$form.find('[value="'+arr.privileges[i]+'"]').closest('label').find('span').html('<i class="fa fa-trash-o"></i>');
			}
		}



		$form.closest('.modal').modal('show');
		$form.closest('.modal').find('.edit_actions').show();
		$('.user_edit_btn').html('<i class="fa fa-edit"></i> Edit');

		$form.addClass('hidden').siblings('div').removeClass('hidden');

		//show details
		var txt = '';
		txt += '<p><b>Broker Details</b>';
		txt += '<br>Brokerage Name: '+arr.brokerage.company_name;
		txt += '<br>Level/suite: '+arr.suite_number;
		txt += '<br>Street address: '+arr.location;
		txt += '</p><p><br><b>Broker Super Administrator</b>';
		txt += '<br>First Name: '+arr.first_name;
		txt += '<br>Last Name: '+arr.last_name;
		txt += '<br>Email: '+arr.email;
		txt += '<br>Contact Number: +'+arr.contact_no+'<p>';

		if(arr.unique_link == 'Y'){
			txt += '<p><br><b>Enable unique quick quote link?</b>';
			txt += '<br>'+enable_whitelabel+'<p>';
		}
		txt += '<p><br><b>Assigned Products</b>';
		txt += '<br>'+arr.privileges_text+'<p>';


		$form.siblings('div').html(txt);
	});

	$('.mobile_number').mask("9999 999 999");

	//for users/secondary underwriters
	$(document).on('click', '.customer-update', function(){
		var $self = $(this);
		var id = $self.data('id');
		var arr = $self.data('arr');
		var $form = $('#customer_form');

		$form.find('[name="id"]').val(id);
		$form.find('[name="first"]').val(arr.first_name);
		$form.find('[name="last"]').val(arr.last_name);
		$form.find('[name="position"]').val(arr.domain);
		$form.find('[name="brokerage_id"]').val(arr.brokerage_id);
		$form.find('[name="email"]').val(arr.email);
		$form.find('[name="brokerage_id"]').selectpicker('refresh');
		$form.find('[name="agency_id"]').val(arr.agency_id);
		$form.find('[name="agency_id"]').closest('.form-group').hide();
		$form.find('[name="mobile"]').val(arr.calling_digits);
		$form.find('[name="country_code"]').val(arr.country_id);

		var short = $('.select_country_tbl[data-id="'+arr.country_id+'"]').data('short');
		short = (short !== null) ? short : 'Select Country Code';
		console.log(short);
		$form.find('.country_btn').html(short);


		$form.find('[name="user_role"]').prop('checked', false).closest('label').addClass('btn-default').removeClass('active btn-primary');

		if(arr.first_underwriter == 'Y'){
			$form.find('[name="user_role"]').eq(0).prop('checked', true).closest('label').removeClass('btn-default').addClass('active btn-primary');
		}
		else{
			$form.find('[name="user_role"]').eq(1).prop('checked', true).closest('label').removeClass('btn-default').addClass('active btn-primary');
		}


		$form.closest('.modal').modal('show');
		console.log(arr);


	});

	$('.new_uw_user').on('click', function(){
		var $mdl = $('#addAgencyModal');
		$mdl.find('.form-control').val('');
		$mdl.find('[name="id"]').val('');
		$mdl.find('.access_rights').prop('checked', true).closest('label').removeClass('btn-default').addClass('active btn-primary');
		$mdl.find('[name="user_role"]').prop('checked', false).closest('label').addClass('btn-default').removeClass('active btn-primary');
		$mdl.find('.country_btn').html('Select Country Code..');
		$mdl.modal('show');
		return false;
	});


	$(document).on('click', '.customer2-update', function(){
		var $self = $(this);
		var id = $self.data('id');
		var arr = $self.data('arr');
		var $form = $('#customer2_form');

		$form.find('[name="id"]').val(id);
		$form.find('[name="first"]').val(arr.first_name);
		$form.find('[name="last"]').val(arr.last_name);
		$form.find('[name="email"]').val(arr.email);

		$form.find('[name="address"]').val(arr.location);
		$form.find('[name="lat"]').val(arr.lat);
		$form.find('[name="lng"]').val(arr.lng);


		$form.find('[name="company_name"]').val(arr.customer_info.company_name);
		$form.find('[name="annaul_revenue"]').val(arr.customer_info.annaul_revenue);
		$form.find('[name="employee_number"]').val(arr.customer_info.employee_number);

		$form.find('[name="country_short"]').val(arr.calling_code);
		$form.find('[name="mobile"]').val(arr.calling_digits);

		$form.find('[name="country_code"]').val(arr.country_id);
		$form.find('[name="country_code"]').selectpicker('refresh');

		$form.find('[name="brokerage_id"]').val(arr.brokerage_id);
		$form.find('[name="brokerage_id"]').selectpicker('refresh');
		$form.find('[name="agency_id"]').val(arr.agency_id);
		$form.find('[name="agency_id"]').closest('.form-group').hide();


	   $('[name="breakdown_by_state[]"]').each(function(index, element) {
		   $(this).val(arr.customer_info.breakdown_by_state[index]);
	   });

		if(arr.access_rights.length > 0){
			$form.find('[name="access_rights[]"]').prop('checked', false).closest('label').addClass('btn-default').removeClass('active btn-primary');
			for(var i = 0; i < arr.access_rights.length; i ++ ){
				$form.find('[value="'+arr.access_rights[i]+'"]').prop('checked', true).closest('label').removeClass('btn-default').addClass('active btn-primary');
			}
		}
		else{
			$form.find('[name="access_rights[]"]').prop('checked', true).closest('label').removeClass('btn-default').addClass('active btn-primary');
		}


		$form.closest('.modal').modal('show');
		console.log(arr);


	});

	$(document).on('click', '.agency-update', function(){
		var $self = $(this);
		var id = $self.data('id');
		var arr = $self.data('arr');
		var $form = $('#agencies_form');


		console.log(arr);


		$form.find('.first-under').addClass('hidden');
		$form.find('[name="id"]').val(id);
		$form.find('[name="name"]').val(arr.name);
		$form.find('[name="domain"]').val(arr.domain);
		$form.find('[name="background"]').val(arr.background); //floor/unit no
		$form.find('[name="address"]').val(arr.address);
		$form.find('[name="lat"]').val(arr.lat);
		$form.find('[name="lng"]').val(arr.lng);


		// $form.find('[name="unique_link"]').prop('checked', false);
		$form.find('[name="unique_link"]').bootstrapSwitch('state', false);

		if(typeof(arr.agency_details.unique_link) !== 'undefined'){
			if(arr.agency_details.unique_link == 'Y'){
				// $form.find('[name="unique_link"]').prop('checked', true);
				$form.find('[name="unique_link"]').bootstrapSwitch('state', true);

			}
		}


		$form.find('[name="product_id[]"]').prop('checked', false).closest('label').removeClass('active btn-primary').addClass('btn-default');
		$form.find('[name="product_id[]"]').closest('label').find('span').html('Add');

		if(typeof(arr.agency_details.product_id) !== 'undefined'){ // && arr.agency_details.product_id.length > 0
			$form.find('[name="product_id[]"]').prop('checked', false).closest('label').addClass('btn-default').removeClass('active btn-primary');
			for(var i = 0; i < arr.agency_details.product_id.length; i ++ ){
				$form.find('[value="'+arr.agency_details.product_id[i]+'"]').prop('checked', true).closest('label').removeClass('btn-default').addClass('active btn-primary');
				$form.find('[value="'+arr.agency_details.product_id[i]+'"]').closest('label').find('span').html('<i class="fa fa-trash-o"></i>');
			}
		}



		$form.closest('.modal').modal('show');


	});


	$(document).on('click', '.reject_btn', function(){
		var $self = $(this);
		var id = $self.closest('form').find('[name="id"]').val();
		$('.modal').modal('hide');
		bootbox.confirm('<h4 class="modal-title">Confirm you wish to reject selected request.</h4>', function(e){
			if(e){
				window.location.href = base_url+'webmanager/dashboard/reject_request/'+id;
			}
		});
	});

	$('.send_certificate_btn').on('click', function(e){
		var $self = $(this);
		var id = $self.data('id');
		$('.modal').hide();
		bootbox.confirm('<h4 class="modal-title">Confirm you wish to send certificate to selected customer.</h4>', function(e){
			if(e){
				$.post(
					base_url+'webmanager/dashboard/send_certificate',
					{id: id},
					function(res){
						console.log(res);
						bootbox.alert('<h4 class="modal-title"><i class="fa fa-check-circle text-success"></i> Insurance Certificate successfully sent.</h4>');
					},
					'json'
				).error(function(res){
					console.log(res);
				});
			}
		});

		e.preventDefault();
	});

	$('.generate_report_btn').on('click', function(e){
		var $self = $(this);
		var title = $self.data('title');
		var type = $self.data('type');
		var $mdl = $('#myModal');

		$mdl.find('.modal-title').html(title);
		$mdl.find('.report-type').val(type);
		$mdl.modal('show');
		e.preventDefault();
	});

	$('.generate_report_form').on('submit', function(){
		var $form = $(this);
		var form_data = $form.serialize()
		console.log(form_data); //return false;
		var date_from = $form.find('[name="date_from"]').val();
		var date_to = $form.find('[name="date_to"]').val();
		var type = 'quotes_report'; //$form.find('[name="type"]').val();
		console.log(base_url+'webmanager/reporting/'+type+'/'+date_from+'/'+date_to);
		window.location.href = base_url+'webmanager/reporting/'+type+'/'+date_from+'/'+date_to+'?'+form_data;
		$form.closest('.modal').modal('hide');
		return false;
	});


	$('[name="enable_whitelabel"]').bootstrapSwitch();
	$('.bs_switch').bootstrapSwitch();

	$('[name="new_template"]').bootstrapSwitch({
		onSwitchChange: function(event, state){
			console.log(event, state);

			var $row = $('.row-select-template');
			var $rowNew = $('.row-new-template');
			$('.report-attributes').slideDown();
			$('[name="attributes[]"]').prop('checked', false).closest('.btn').removeClass('btn-primary active').addClass('btn-default');
			$('.hidden[name="report_template"]').prop('checked', true);
			$('[name="template_name"]').val('');

			$('[name="attributes[]"]').siblings('span').html('Add');

			if(!state){
				var lenthh = $('.modal').find('[name="report_template"]').length;

				if(lenthh > 1){
					$('#selectTemplateModal').modal('show');
				}
				else{
					bootbox.confirm('There are no saved templates, would you like to create a new template instead?', function(e){
						if(e){
							$('[name="new_template"]').bootstrapSwitch('toggleState');
						}
					});
				}
				// $row.slideDown();
				$rowNew.slideUp();
			}
			else{
				// $row.slideUp();
				$rowNew.slideDown();

			}
		}
	});


	$('.edit_employee_btn').on('click', function(){
		var $self = $(this);
		var $mdl = $self.closest('.modal');
		if($self.text() == 'Cancel'){
			$mdl.find('.price_form .btn-default').click();
			$mdl.find('.submit_employee_form').addClass('hidden');
			$self.html('<i class="fa fa-edit"></i> Edit');
		}
		else{
			$mdl.find('.price-text').addClass('hidden');
			$mdl.find('.price-text').siblings().removeClass('hidden');

			$mdl.find('.submit_employee_form').removeClass('hidden');
			$self.html('Cancel');
		}
	});

	$('.submit_employee_btn').on('click', function(){
		var $self = $(this);
		var $mdl = $self.closest('.modal');
		$mdl.find('.price_form').submit();
		$self.button('loading');
		setTimeout(function(){
			$self.button('reset');
			$mdl.find('.submit_employee_form').addClass('hidden');
			$('.edit_employee_btn').html('<i class="fa fa-edit"></i> Edit');
			$mdl.find('.price_form .btn-default').click();

			var modal_id = $mdl.attr('id');
			var today = new Date();
			var dd = today.getDate();
			var mm = today.getMonth()+1; //January is 0!
			var yyyy = today.getFullYear();

			if(dd<10) {
			    dd='0'+dd
			}

			if(mm<10) {
			    mm='0'+mm
			}

			today = dd+'-'+mm+'-'+yyyy;


			$('a[href="#'+modal_id+'"]').find('small').html('Last Updated: '+today);
			$('a[href="#'+modal_id+'"]').find('.panel').addClass('flash_animation2');

			setTimeout(function(){
				$('a[href="#'+modal_id+'"]').find('.panel').removeClass('flash_animation2');
			}, 4500);

			$mdl.modal('hide');

		}, 1000);
	});

	$('[name="report_template"]').on('change', function(){
		var $self = $(this);
		var inputs = $self.data('inputs');
		var $attr = $('[name="attributes[]"]');
		$attr.prop('checked', false).closest('.btn').removeClass('btn-primary active').addClass('btn-default');
		$attr.siblings('span').html('Add');
		for (i = 0; i < inputs.length; i++) {
			console.log(inputs[i]);
			$('.report-attributes').find('[value="'+inputs[i]+'"]').siblings('span').html('<i class="fa fa-trash"></i>');
			$('.report-attributes').find('[value="'+inputs[i]+'"]').prop('checked', true).closest('.btn').addClass('btn-primary active');
		}

		console.log(name, inputs);
	});


	$('[data-toggle="buttons"]').on('click', function(){
		var $self = $(this);
		console.log($self.find('input').is(':checked'));
		setTimeout(function(){
      //reset if radio
      if($self.find('input[type="radio"]').length > 0){
        console.log('radio');
        $self.closest('.row').find('.btn').addClass('btn-default').removeClass('btn-primary active');
      }

      if($self.find('input').is(':checked')){
				$self.find('.btn').addClass('btn-primary act').removeClass('btn-default');
			}
			else{
				$self.find('.btn').addClass('btn-default').removeClass('btn-primary');
			}


		}, 24);
		console.log('haha');
	});



	$('.geolocation').geocomplete()
	.bind("geocode:result", function(event, result){

		var location_lat = result.geometry.location.lat();
		var location_long = result.geometry.location.lng();

		var $form = $(this).closest('form');

		$form.find('[name="lat"]').val(location_lat);
		$form.find('[name="lng"]').val(location_long);

		console.log(result);

	});



	$('.img-hover').on('click', function(){
		// window.location.href = base_url+'webmanager/settings/profile';
    $('#myProfileModal').modal('show');
		return false;
	});



	$('#quote_expiry_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
    submitHandler: function(form) {

			$(form).find('button[type="submit"]').button('loading');

			$.post(
				base_url+'formsubmits/update_quote_expiry',
				$(form).serialize(),
				function(res){
					console.log(res);
          $(form).find('button[type="submit"]').button('reset');
          $('.modal.in').modal('hide');

          setTimeout(function(){
            bootbox.alert('Expiry successfully updated.');

          },50);

				},
				'json'
			).error(function(err){
				console.log(err);

				$(form).find('button[type="submit"]').button('reset');
			});


		}
	});


	$('#assign_underwriter_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {

			$(form).find('button[type="submit"]').button('loading');

			$.post(
				base_url+'formsubmits/assign_underwriter_product',
				$(form).serialize(),
				function(res){
					console.log(res);
					bootbox.alert(action_messages.success.product_assigned_to_uw, function(){
						$(form).find('button[type="submit"]').button('reset');
						$('.modal.in').modal('hide');

						var modal_id = 'assignAgentModal';
						$('a[href="#'+modal_id+'"]').find('small').html('Last Updated: '+res.nowtime);
						$('a[href="#'+modal_id+'"]').find('.panel').addClass('flash_animation2');

						setTimeout(function(){
							$('a[href="#'+modal_id+'"]').find('.panel').removeClass('flash_animation2');
						}, 4500);


					});
				},
				'json'
			).error(function(err){
				console.log(err);

				$(form).find('button[type="submit"]').button('reset');
			});


		}
	});

	$('#agencies_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {

			$(form).find('button[type="submit"]').button('loading');
			var data = {
				id: $(form).find('[name="id"]').val(),
				utype: $(form).find('[name="utype"]').val(),
				name: $(form).find('[name="name"]').val(),
				domain: $(form).find('[name="domain"]').val(),
				product_id: $(form).find('[name="product_id[]"]').val(),
				agency_details: $(form).serializeArray(),
			};

			console.log(data);  //return false;
			$.post(
				base_url+'webmanager/customers/save_agency',
				data,
				function(res){
					console.log(res);
					if(res.result == 'ok'){
						if(res.action == 'add' && res.user_type == '0'){
							window.location.href = base_url + 'webmanager/customers/underwriters';
							return false;
						}

						window.location.reload(true);
					}
					else{
						bootbox.alert(res.message);
						$(form).find('button[type="submit"]').button('reset');
					}
//					$(form).find('button[type="submit"]').button('reset');
				},
				'json'
			).error(function(err){
				console.log(err);

				$(form).find('button[type="submit"]').button('reset');
			});


		}
	});

	$('#brokerage_form').validate({
		ignore: [],
		rules:{
			mobile: {
				required: true,
        pattern: /^[\d\s]+$/,
        minlength: 6,
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {

			$(form).find('button[type="submit"]').button('loading');

			$.post(
				base_url+'webmanager/customers/save_brokerage',
				{data: $(form).serializeArray() },
				function(res){
					console.log(res);
					window.location.reload(true);
					//$(form).find('button[type="submit"]').button('reset');
				},
				'json'
			).error(function(err){
				console.log(err);

				$(form).find('button[type="submit"]').button('reset');
			});


		}
	});

	$('#question_form').validate({
		ignore: [],
		rules: {
			question: {
				required: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');
			$.post(
				base_url+'webmanager/products/save_question',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);

				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});


	$('#occupation_form').validate({
		ignore: [],
		rules: {
			specific_name: {
				required: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');
			$.post(
				base_url+'webmanager/industries/save_question',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);

				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});



	$('#promocode_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {

			$(form).find('button[type="submit"]').button('loading');

			$.post(
				base_url+'formsubmits/save_promocode',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);
					//$(form).find('button[type="submit"]').button('reset');
				},
				'json'
			).error(function(err){
				console.log(err);

				$(form).find('button[type="submit"]').button('reset');
			});


		}
	});


	$('#prod_questionnaire_form').validate({
		ignore: [],
		rules: {
			product_name: {
				required: true
			},
			tab: {
				required: true
			},
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');
			$.post(
				base_url+'formsubmits/prod_questionnaire',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);

				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});


	$('#product_form').validate({
		ignore: [],
		rules: {
			product_name: {
				required: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');
			$.post(
				base_url+'webmanager/products/save_product',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);

				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});


	$('#industries_form').validate({
		ignore: [],
		rules: {
			category_name: {
				required: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');
			$.post(
				base_url+'webmanager/industries/save_product',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);

				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});




	$('#base_curr_form').validate({
		ignore: [],
		rules: {
			currency: {
				required: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');
			$.post(
				base_url+'webmanager/ratings/save_currency',
				$(form).serialize(),
				function(res){
					console.log(res);
					$('.base_currency').html($(form).find('[name="currency"]').val());
					$(form).closest('.modal').modal('hide');
					$(form).find('button[type="submit"]').button('reset');
				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});

	$('#add_port_form').validate({
		rules: {
			ccode: {
				required: true
			},
			portcode: {
				required: true,
				maxlength: 3
			},
			portname: {
				required: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');
			console.log($(form).serialize());
			submitForm('add_port_code', $(form).serialize(), form);
		}
	});

	$('#agency_form').validate({
		rules: {
			name: {
				required: true
			},
			email: {
				required: true,
				email: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');
			$.post(
				base_url+'webmanager/users/add',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);
					$(form).find('button[type="submit"]').button('reset');
				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});

	$('#baserate_form').validate({
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');

			$.post(
				base_url+'webmanager/settings/baserate',
				$(form).serialize(),
				function(res){
					console.log(res);
					bootbox.alert('Base rate updated successfully.');
					$(form).find('button[type="submit"]').button('reset');
				},
				'json'
			).error(function(err){
				console.log(err);
			});

		}
	});




	$('#customer_details_form').validate({
		ignore: [],
		rules:{
			mobile: {
				required: true,
        pattern: /^[\d\s]+$/,
        minlength: 6,
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {


			$(form).find('button[type="submit"]').button('loading');
			console.log($(form).serialize());
			$.post(
				base_url+'formsubmits/update_customer_admin',
				$(form).serialize(),
				function(res){
					console.log(res);

					$(form).find('button[type="submit"]').button('reset');
					bootbox.alert(res.message, function(){
						$('.modal.in').modal('hide');
					});

				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});



		$('.input-number').on('keyup', function(){
			var val = $(this).val();
			//var
			var n = parseInt($(this).val().replace(/\D/g,''),10);
			//n = (isNaN(n)) ? 0 : n;
			if(!isNaN(n)){
				$(this).val(n.toLocaleString());
			}
		});


   $('[name="breakdown_by_state[]"]').on('keyup', function(){
   		console.log('hey');
	   var total = 0;
	   $('[name="breakdown_by_state[]"]').each(function(index, element) {
		   var val = ($(this).val() != '') ? parseInt($(this).val()) : 0;
		   total += val;
	   });
	   $('.the-total').html(' ('+total+'%)');
	   console.log(total);
   });




	$('#customer2_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block ',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			//check if selected agency adding new underwriter
			var id = $(form).find('[name="id"]').val();
			var agency_id = $(form).find('[name="agency_id"]').val();
			if(id == '' && agency_id == ''){
				bootbox.alert('Please select agency.');
				return false;
			}

			$(form).find('button[type="submit"]').button('loading');
			console.log($(form).serialize());
			$.post(
				base_url+'webmanager/customers/add_customer2',
				{data: $(form).serializeArray() },
				function(res){
					console.log(res);
					if(res.result != 'ok'){
						$(form).find('button[type="submit"]').button('reset');
						bootbox.alert(res.message);
					}
					else{
						window.location.reload(true);
					}

				},
				'json'
			).error(function(err){
				console.log(err);
				$(form).find('button[type="submit"]').button('reset');
			});
		}
	});


	$('#customer_form').validate({
		ignore: [],
		rules: {
			first: {
				required: true
			},
			last: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			mobile: {
				required: true,
        pattern: /^[\d\s]+$/,
        minlength: 6,
			}

		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			//check if selected agency adding new underwriter
			var id = $(form).find('[name="id"]').val();
			var agency_id = $(form).find('[name="agency_id"]').val();
			if(id == '' && agency_id == ''){
				bootbox.alert('Please select agency.');
				return false;
			}

			$(form).find('button[type="submit"]').button('loading');
			console.log($(form).serialize());
			$.post(
				base_url+'webmanager/customers/add',
				$(form).serialize(),
				function(res){
					console.log(res);

					if(res.result != 'ok'){
						$(form).find('button[type="submit"]').button('reset');
						bootbox.alert(res.message);
					}
					else{
						window.location.reload(true);
					}

				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});


	$( ".ui-sortable-handle .panel-primary" )
  .mouseup(function() {
		$( this ).removeClass('panel-draggable');
  })
  .mousedown(function() {
		$( this ).addClass('panel-draggable');
  });

	$( ".ui-sortable-handle .panel-primary" ).on('blur', function(){
		$( this ).removeClass('panel-draggable');
	});


	$('.policy_filter_btn a').on('click', function(){
		var $self = $(this);
		var val = $self.data('value');
		var $search = $self.closest('.panel').find('.dataTables_filter input');
		$search.val( val ).keyup();
		$self.closest('li').addClass('active').siblings().removeClass('active');

	});


	$('#add_cargo_form').validate({
		rules: {
			code: {
				required: true
			},
			name: {
				required: true
			},
			price: {
				required: true,
				number: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button').button('loading');


			console.log($(form).serialize());
			$.post(
				base_url+'formsubmits/add_cargo_cat',
				$(form).serialize(),
				function(res){
					console.log(res);
					var $cargo_mdl = $('#cargoCategoryModal');
					var tr_ln = $cargo_mdl.find('tbody tr').last().attr('class');

					var tr = $cargo_mdl.find('tbody tr').first().clone().removeClass().addClass('tr-'+res.id).insertAfter('.'+tr_ln);

					//$cargo_mdl.find('.tr-'+res.id+' .price-text:eq(0)').html(res.hs_code);
					$cargo_mdl.find('.tr-'+res.id+' .price-text:eq(0)').html(res.specific_name);
					$cargo_mdl.find('.tr-'+res.id+' .price-text:eq(1)').html(res.price);
					var referr = false;
					if(res.referral == 'Yes'){
						referr = true;
					}
					$cargo_mdl.find('.tr-'+res.id+' [type="checkbox"]').prop('checked', referr);
					$cargo_mdl.find('.tr-'+res.id+' [type="checkbox"]').val(res.id);
					$cargo_mdl.find('.delete_btn').data('id', res.id);
					console.log(tr);
					$(form).find('input').val('');
					$(form).find('button').button('reset');
					$(form).closest('.modal').modal('hide');

					bootbox.alert(action_messages.success.new_occupation_added);
				},
				'json'
			).error(function(err){
				console.log(err);
			});

			//submitForm('add_cargo_cat', $(form).serialize(), form);

		}
	});

	$('#add_ded_form').validate({
		rules: {
			name: {
				required: true,
				number: true
			},
			price: {
				required: true,
				number: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {

			$(form).find('button').button('loading');
			console.log($(form).serialize());

			var txt1 = $(form).find('[name="name"]').val();
			var txt2 = $(form).find('[name="price"]').val();

			$.post(
				base_url+'formsubmits/add_deductible',
				$(form).serialize(),
				function(res){
					console.log(res);
					var modal_id = 'deductibleModal';
					$('a[href="#'+modal_id+'"]').find('small').html('Last Updated: '+res.nowtime);
					$('a[href="#'+modal_id+'"]').find('.panel').addClass('flash_animation2');

					var $cargo_mdl = $('#deductibleModal');
					var tr_ln = $cargo_mdl.find('tbody tr').last().attr('class');

					var tr = $cargo_mdl.find('tbody tr').first().clone().removeClass().addClass('tr-'+res.id).insertAfter('.'+tr_ln);

					//$cargo_mdl.find('.tr-'+res.id+' .price-text:eq(0)').html(res.hs_code);
					$cargo_mdl.find('.tr-'+res.id+' .price-text:eq(0)').html(txt1);
					$cargo_mdl.find('.tr-'+res.id+' .price-text:eq(1)').html(txt2);
					var referr = false;
					// if(res.referral == 'Yes'){
					// 	referr = true;
					// }
					$cargo_mdl.find('.tr-'+res.id+' [type="checkbox"]').prop('checked', referr);
					$cargo_mdl.find('.tr-'+res.id+' [type="checkbox"]').val(res.id);
					$cargo_mdl.find('.delete_btn').data('id', res.id);
					console.log(tr);


					$(form).find('input').val('');
					$(form).find('button').button('reset');
					$(form).closest('.modal').modal('hide');


					bootbox.alert(res.message);

					setTimeout(function(){
						$('a[href="#'+modal_id+'"]').find('.panel').removeClass('flash_animation2');
					}, 4500);

					$('.modal.in').modal('hide');
					$(form).find('button').button('reset');
					$(form).find('.form-control').val('');
				},
				'json'
			).error(function(err){
				console.log(err);
			});



			// submitForm('add_deductible', $(form).serialize(), form);

		}
	});


   setTimeout(function(){
    //  $('#myLoadingModal').modal('hide');
     $('.loading').hide();
		 $('.complete_loading_content').removeClass('hidden');
   }, 0);


});



//var deleteRecord = function(el, id, table){
//	var mydataTb = $('.mydataTb').DataTable();
//	$.post(
//		base_url+'formsubmits/delete_tb',
//		{id: id, table: table},
//		function(res){
//			console.log(res);
//			el.closest('tr').addClass('bg-danger').fadeOut('fast', function(){
//
//				mydataTb.destroy();
//				$.when($(this).remove()).then( function(){
//
//					mydataTb = $('.mydataTb').DataTable({
//						"bPaginate": true,
//						"bLengthChange": false,
//						"bFilter": false,
//						"bInfo": true
//					});
//				});
//
//			});
//		}
//
//	);
//};


function submitSubject(id){
	$('.form_subj').find('button.btn-primary').html('Loading..').addClass('disabled');
	var subb = $('#subject_name').val();
	if (subb == ''){
		bootbox.alert('<p class="lead">Please add subject</p>');
	}else{

		$.ajax({
			  type: "POST",

			  url: base_url+'webmanager/emails/update_subject',

			  data: {
				  subject: subb,
				  id: id

			  },

			  success: function(data) {
					console.log(data);
					$('#subjj').html(subb);
					$('.form_subj').find('button.btn-primary').html('Saved').removeClass('disabled');
					setTimeout(function(){
						$('.form_subj').hide();
						$('#subjj').show();
						$('.form_subj').find('button.btn-primary').html('Submit');
					}, 1000);
			  }

		});


	}
}


function userActivate(inc,user_id){
	var actt = '';
	if(inc == 'Y'){
		actt = '<p class="">Please confirm you wish to activate agency?</p>';
	}else{
		actt = '<p class="">Please confirm you wish to deactivate agency?</p>';
	}

	bootbox.confirm(actt, function(e){

		if(e){
			window.location.assign(base_url+"webmanager/users/activate/"+inc+"/"+user_id);

		}
	});

}//userActivate

function defaultAgency(agency_id){
	var actt = '<p class="">Please confirm you wish to make cargo category of this agency default?</p>';
	bootbox.confirm(actt, function(e){
		if(e){
			window.location.assign(base_url+"webmanager/ratings/makedefault/"+agency_id);

		}
	});
};

function customerActivate(inc,user_id){
	var actt = '';
	if(inc == 'Y'){
		actt = '<p class="">Please confirm you wish to activate user?</p>';
	}else{
		actt = '<p class="">Please confirm you wish to deactivate user?</p>';
	}

	bootbox.confirm(actt, function(e){

		if(e){
			window.location.assign(base_url+"webmanager/customers/activate/"+inc+"/"+user_id);

		}
	});

}//userActivate


function userDelete(user_id){
	var del_mess = '<p class="">Please confirm you wish to delete agency?</p>';


	bootbox.confirm(del_mess, function(e){

		if(e){
			window.location.assign(base_url+"webmanager/users/delete/"+user_id);

		}
	});

}//userDelete

function deleteAlternate(id){
	var del_mess = '<p class="">Please confirm you wish to delete content?</p>';


	bootbox.confirm(del_mess, function(e){

		if(e){
			window.location.assign(base_url+"webmanager/contents/delete_alt/"+id);

		}
	});

};

function customerDelete(user_id){
	var del_mess = '<p class="">Please confirm you wish to delete user?</p>';


	bootbox.confirm(del_mess, function(e){

		if(e){
			window.location.assign(base_url+"webmanager/customers/delete/"+user_id);

		}
	});

}//userDelete



function deleteFaq(id){
	var del_mess = '<p class="">Please confirm you wish to delete selected FAQ?</p>';


	bootbox.confirm(del_mess, function(e){

		if(e){
			window.location.assign(base_url+"webmanager/faq/delete/"+id);

		}
	});

}//deleteFaq



function getFaq(id){

	var data = {
		"id": id
	};
	data = $(this).serialize() + "&" + $.param(data);

	$.ajax({
		  type: "POST",

		  url: base_url+"webmanager/faq/get_info",

		  dataType: 'json',

		  data: data,

		  success: function(data) {
				console.log(data);
				$('#question').val(data.question);
				$('#answer').val(data.answer);
				$('#faq_id').val(id);
				$('#faqModal').modal('show');

		  }
	});

}


function userDetails(id){

	var $mdl = $('#infoModal');
	$mdl.find('.agency_info').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');

	$mdl.find('.agency_info').load( base_url+'landing/agency_info/'+id, function() {
	});

	$mdl.modal('show');
}


function customerDetails(id){

	var $mdl = $('#infoModal');
	$mdl.find('.customer_info').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');

	$mdl.find('.customer_info').load( base_url+'landing/customer_info/'+id, function() {
	});

	$mdl.modal('show');
}


//additional validations
$.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^\w+$/i.test(value);
}, "Letters, numbers, and underscores only please");


$(document).ready(function(e) {


	$('.price-text').on('click', function(){
		var $self = $(this);
		if(!$self.hasClass('not-editable')){
			$self.addClass('hidden');
			$self.siblings().removeClass('hidden');
			$self.siblings().find('[name="cargo-price"]').focus();
		}

	});

	$('.price_form').find('a').on('click', function(){
		$(this).closest('form').addClass('hidden').siblings().removeClass('hidden');
		return false;
	});


	$(document).on('click', '.dismiss-widget', function(){
		var $self = $(this);
		var id = $self.data('id');
		var target = $self.data('target');
		console.log(id, target);

		$('#'+target).remove();

		$.post(
			base_url+'formsubmits/delete_tb',
			{id: id, table: 'q_widget'},
			function(res){
				console.log(res);
			},
			'json'
		).error(function(err){
			console.log(err);
		});

	});



	$('.text_form [type="submit"]').on('click', function(){

		var $btn = $(this);
		var $self = $btn.closest('.text_form');
		console.log($self.serialize());

		$btn.button('loading');
		var newval = $self.find('input[type="text"]').val();
		$.post(
			base_url+'formsubmits/inline_text_update',
			$self.find('input').serialize(),
			function(res){
				console.log(res);
				$self.addClass('hidden').siblings('.price-text').removeClass('hidden').html(newval);
				$btn.button('reset');

				if(typeof(res.modal_id) !== 'undefined'){
					//update date and add flash effect
					var modal_id = res.modal_id;
					$('a[href="#'+modal_id+'"]').find('small').html('Last Updated: '+res.nowtime);
					$('a[href="#'+modal_id+'"]').find('.panel').addClass('flash_animation2');

					setTimeout(function(){
						$('a[href="#'+modal_id+'"]').find('.panel').removeClass('flash_animation2');
					}, 4500);
				}


			},
			'json'
		).error(function(err){
			console.log(err);
		});
		return false;
	});

	$('.price_form').each(function(index, element) {
		$(this).validate({
			rules: {
				'cargo-price': {
					required: true,
					number: true
				}
			},
			highlight: function(element) {
				$(element).closest('.form-group').addClass('has-warning');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-warning');
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				// element.closest('.form-group').append(error);
			},
			submitHandler: function(form) {

				$(form).find('button').addClass('disabled');
				var newval = $(form).find('[name="cargo-price"]').val();
				console.log($(form).serialize());
				$.post(
					base_url+'formsubmits/cargo_price_agency',
					$(form).serialize(),
					function(res){
						console.log(res);
						$(form).addClass('hidden').siblings().removeClass('hidden').html(newval);
						$(form).find('button').removeClass('disabled');

						if(typeof(res.modal_id) !== 'undefined'){
							//update date and add flash effect
							var modal_id = res.modal_id;
							$('a[href="#'+modal_id+'"]').find('small').html('Last Updated: '+res.nowtime);
							$('a[href="#'+modal_id+'"]').find('.panel').addClass('flash_animation2');

							setTimeout(function(){
								$('a[href="#'+modal_id+'"]').find('.panel').removeClass('flash_animation2');
							}, 4500);
						}



					},
					'json'
				).error(function(err){
					console.log(err);
				});


			}
		});


	});

	$('#zone_multi_form').validate({
		rules: {
			'zone[]': {
				required: true,
				number: true
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			 //element.closest('div').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button').button('loading');
			$.post(
				base_url+'webmanager/ratings/save_zone',
				$(form).serialize(),
				function(res){
					console.log(res);
					$(form).find('button').button('reset');
					$(form).closest('.modal').modal('hide');
				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});

	$('[name="ded_default[]"]').on('change', function(){
		var $self = $(this);
		var agency_id = $self.data('agency');
		var id = $self.val();
		if($self.is(':checked')){
			$self.closest('tr').siblings().find('input[type="checkbox"]').prop('checked', false);
			$.post(
				base_url+'webmanager/ratings/deductible_default',
				{agency_id: agency_id, id: id},
				function(res){
					console.log(res);
				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});
	var tableData = $('.table-datatable').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
		"initComplete": function(settings, json) {
			$('.loading-table').hide();
			$(this).show();
	  },
		"bInfo": true,
	    "language": {
	        search: "_INPUT_",
	        searchPlaceholder: "search for..."
	    }

	});

	$('.table-datatable-modal').dataTable({
	 	"bPaginate": false,
	 	"bLengthChange": false,
		"bInfo": false,
	 	"bFilter": true,
		"paging": false
 });

	$('.input_table_search').on('keyup', function(){
		var $self = $(this);
		var $search = $self.closest('.panel').find('.dataTables_filter input');
		$search.val( this.value ).keyup();

	});

	$('.input_table_search_modal').on('keyup', function(){
		var $search = $('.modal .dataTables_filter input');
		$search.val( this.value ).keyup();

	});


	$('.activitylog-datatable').dataTable({
		"bPaginate": true,
		"bLengthChange": true,
		"pageLength": 50,
		"bFilter": false,
		"bInfo": true
	});

	$('.select_country_tbl').on('click', function(){
		var $self = $(this);
		var short = $self.data('short');
		var calling = $self.data('calling');
		var id = $self.data('id');
		$('[name="country_code"]').val(id);
		$('[name="country_short"]').val(calling);
		$('.country_btn').addClass('flash_animation').html(short);

		setTimeout(function(){
			$('.country_btn').removeClass('flash_animation');
		}, 2100);

		$self.closest('.modal').modal('hide');
		console.log(id, calling, short);
	});
	// setTimeout(function(){
	// 	$(document).find('.dataTables_filter input').prop('placeholder', 'search for..');
	// }, 2500);
	// $(document).on('')


	$('.open_modal_form_btn').on('click', function(){
		var $self = $(this);
		var mdl = $self.data('controls-modal');
		var $form = $(mdl).find('form');
		$(mdl).modal('show');
		$(mdl).find('.form-control').val('');
		$(mdl).find('[name="id"]').val('');
		$(mdl).find('.policydoc').html('');
		$(mdl).find('.show_addcontent').removeClass('hidden');

		$form.find('.btn-group [type="checkbox"]').prop('checked', false).closest('label').removeClass('active btn-primary').addClass('btn-default');
		$form.find('.btn-group [type="checkbox"]').closest('label').find('span').html('Add');

		return false;
	});

	$('.review_policy_btn').on('click', function(e){
		var $self = $(this);
		var id = $self.data('id');
		var premium = $self.data('premium');
		var comment = $self.data('comment');
		var $mdl = $('#reviewModal');
		$mdl.find('[name="id"]').val(id);
		$mdl.find('[name="premium"]').val(premium);
		$mdl.find('[name="comment"]').html(comment);
		$mdl.modal('show');
		e.preventDefault();
	});

	//Form comment_form validate
//	$(document).on('click','.admin_review_submit_btn', function(){
//		var $self = $(this).closest('form');
//		if($self.find('[name="premium"]').val() == ''){
//			$self.submit();
//		}
//	});
//
//	$(document).on('submit', '.admin_review_form', function(){
//
//
//		$(this).validate({
//			rules: {
//				premium: {
//					required: true,
//					number: true
//				}
//			},
//			highlight: function(element) {
//				$(element).closest('.form-group').addClass('has-warning');
//			},
//			unhighlight: function(element) {
//				$(element).closest('.form-group').removeClass('has-warning');
//			},
//			errorElement: 'span',
//			errorClass: 'help-block',
//
//			errorPlacement: function(error, element) {
//				 element.closest('.form-group').append(error);
//			},
//			submitHandler: function(form) {
//				$(form).find('button[type="submit"]').button('loading');
//				console.log($(form).serialize());
//
//				$.post(
//					base_url+'webmanager/dashboard/accept_request',
//					$(form).serialize(),
//					function(res){
//						console.log(res);
//						window.location.reload(true);
//					},
//					'json'
//				).error(function(err){
//					console.log(err);
//				});
//
//			}
//		});
//
//		return false;
//
//	});


	$('#about_form').on('submit', function(){
		var about_content = $("#summernote").code();
		$self = $(this);
		$self.find('button.btn-sky').addClass('disabled').html('Saving changes..');

		$.post(
			base_url+'webmanager/pages/save_about',
			{about_content: about_content},
			function(res){
				console.log(res);
				$self.find('button.btn-sky').html('Saved!!');

				setTimeout(function(){
					$self.find('button.btn-sky').removeClass('disabled').html('Submit');
				}, 2000);

			}
		);

		return false;
	});



	$("#myCsvForm").ajaxForm({
		beforeSend: function()
		{
			$(".progress-csv").show();
			//clear everything
			$(".bar-csv").width('0%');
			$(".percent-csv").html("0%");
		},
		uploadProgress: function(event, position, total, percentComplete)
		{
			$(".bar-csv").width(percentComplete+'%');
			$(".percent-csv").html(percentComplete+'%');
			$(".upload_file_btn").addClass('disabled');

		},
		success: function()
		{
			$(".bar-csv").width('100%');
			$(".percent-csv").html('100%');
			$(".progress-csv").hide();
		},
		complete: function(response)
		{
			console.log(response.responseText);
			$(".upload_file_btn").removeClass('disabled');
			if(isNaN(response.responseText)){
				if(response.responseText == 'not_valid_file'){
					$(".progress-csv").hide();
					$('.bulk_error_mess').removeClass('hidden').html('<p class="">The file selected is invalid.</p>');
				}
				else if(response.responseText == 'not_readable'){
					$(".progress-csv").hide();
					$('.bulk_error_mess').removeClass('hidden').html('<p class="">File is empty or not readable.</p>');
				}
				else{
					$('.bulk_error_mess').addClass('hidden');

					//$("#file_up_text").show();
					//$("#message").after(response.responseText);
					$('.bulk_error_mess').removeClass('hidden').html('<p class="">Please update your file.<br>'+response.responseText+'</p>');

					//$('.bulk_error_mess').html('<p class="">Succcess! You can now send a request email to your friends.</p>');
				}


			}
			else{

				window.location.href = base_url+'webmanager/ratings/manage';
			}


		},
		error: function(response)
		{
			$('.bulk_error_mess').removeClass('hidden').html('<p class="lead"> Unable to upload file. Please refresh and try again.</p>');
			console.log(response.responseText);
		}

	});
	//end ajax file upload



  var $dragandrophandler = $("#dragandrophandler");
  $dragandrophandler.on('dragenter', function (e)
  {
      e.stopPropagation();
      e.preventDefault();
      $(this).css('border', '2px dashed #ff7b10');
  });
  $dragandrophandler.on('dragover', function (e)
  {
       e.stopPropagation();
       e.preventDefault();
  });
  $dragandrophandler.on('drop', function (e)
  {
			var $self = $(this);
       $self.css('border', '1px dashed #ff7b10').html('<i class="fa fa-paperclip fa-3x"></i><br>Choose a File or Drag it here');
       e.preventDefault();
       var files = e.originalEvent.dataTransfer.files;

       //We need to send dropped files to Server
      //  handleFileUpload(files,obj);
       console.log('files',files);
       console.log('dragandrophandler', $dragandrophandler);
       var form = $self.data('form');
       $(form).find('input[type="file"]').prop('files', files);
      //  $form.submit();

  });

  //If the files are dropped outside the div, file is opened in the browser window. To avoid that we can prevent ‘drop’ event on document.
  $(document).on('dragenter', function (e)
  {
      e.stopPropagation();
      e.preventDefault();
  });
  $(document).on('dragover', function (e)
  {
    e.stopPropagation();
    e.preventDefault();
    $dragandrophandler.css('border', '2px dashed #ff7b10');
  });
  $(document).on('drop', function (e)
  {
      e.stopPropagation();
      e.preventDefault();
  });




	//uploadPolicyForm
	$("#uploadPolicyForm").ajaxForm({
		beforeSend: function() {
			$(".progress-profile").show();
			//clear everything
			$(".bar-profile").width('0%');
			$(".percent-profile").html("0%");
		},
		uploadProgress: function(event, position, total, percentComplete){
			$(".bar-profile").width(percentComplete+'%');
			$(".percent-profile").html(percentComplete+'%');


		},
		success: function() {
			$(".bar-profile").width('100%');
			$(".percent-profile").html('100%');

		},
		complete: function(response) {
			if(response.responseText == 'not_img'){
				$(".progress-profile").hide();
				bootbox.alert('File selected is not a valid file.');

			}
			else{
				var rpt = response.responseText;
				var partsOfStr = rpt.split('__');
				var filename = partsOfStr[0].substring(13);
				var file_id = partsOfStr[1];

				$('#policy_doc_form').find('[name="id"]').val(file_id);

				$('.policydoc').html('<a href="'+base_url+'uploads/policyd/'+partsOfStr[0]+'" target="_blank" class=""><i class="fa fa-file-pdf-o"></i> '+filename+'</a> <small><i class="fa fa-times-circle text-muted delete_policy_btn"></i></small>');
			}
			console.log(response.responseText);

		},
		error: function(){
			bootbox.alert('<h4> Unable to upload file. Please refresh and try again.</h4>');
		},
		// dataType: 'json'

	});
	//end ajax file upload


	$(document).on('click', '.update_policydoc_btn', function(){
		var $self = $(this);
		var info = $self.data('info');
		var $mdl = (info.doc_type == '0') ? $('#policyModal') : $('#endorseModal');

		$mdl.find('[name="id"]').val(info.id);
		$mdl.find('[name="title"]').val(info.title);
		$mdl.find('[name="reference"]').val(info.reference);
		$mdl.find('[name="description"]').val(info.description);

		if(info.doc_type == '0'){
			$('[name="policydoc"]').val(info.id);
		}

		$mdl.find('.policydoc').html('<a href="'+base_url+'uploads/policyd/'+info.name+'" target="_blank" class=""><i class="fa fa-file-pdf-o"></i> '+info.file_name+'</a> <small><i class="fa fa-times-circle text-muted delete_policy_btn"></i></small>');


		$mdl.modal('show');
		return false;
	});


	$('#policy_doc_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			var file_id = $(form).find('[name="id"]').val();
			if(file_id == ''){
				bootbox.alert('Add policy wording doc (PDF).');
				return false;
			}

			$(form).find('[type="submit"]').button('loading');

			$.post(
				base_url+'formsubmits/policy_doc_form',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);
				},
				'json'

			).error(function(err){
				console.log(err);
			});
		}
	});


	$('#endorse_doc_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('[type="submit"]').button('loading');

			$.post(
				base_url+'formsubmits/endorse_doc_form',
				$(form).serialize(),
				function(res){
					console.log(res);
					window.location.reload(true);
				},
				'json'

			).error(function(err){
				console.log(err);
			});
		}
	});

	$(document).on('click', '.delete_btn_confirm', function(){
		var $self = $(this);
		var $mdl = $('#deleteModal');
		$mdl.find('[name="id"]').val($self.data('id'));
		$mdl.modal('show');
	});



	$('#delete_form').validate({
		ignore: [],
		rules: {
			'delete': {
				required: true,
				equalTo: '#delete2'
			}
		},

		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			 //element.closest('div').append(error);
		},
		submitHandler: function(form) {
			$(form).find('[type="submit"]').button('loading');
			$('.delete_form_btn2').button('loading');
			var id = $(form).find('[name="id"]').val();
			var type = $(form).find('[name="type"]').val();
			var $self = $('.delete_btn_confirm[data-id="'+id+'"]');
			var table = $self.data('table');
			console.log(id, $self);
			//deleteRecord($self, id, table);

			var mydataTb = $('.mydataTb').DataTable();
			$.post(
				base_url+'formsubmits/delete_tb',
				{id: id, table: table},
				function(res){
					console.log(res);


					if(type == 'table'){

						$self.closest('tr').addClass('bg-danger').fadeOut('fast', function(){
							$('.modal.in').modal('hide');
							mydataTb.destroy();
							$.when($(this).remove()).then( function(){

								mydataTb = $('.mydataTb').DataTable({
									"bPaginate": true,
									"bLengthChange": false,
									"bFilter": false,
									"bInfo": true
								});
							});

						});


					}
					else{
						window.location.href=base_url+'webmanager/customers/manage/brokers';
					}

				}

			).error(function(err){
				console.log(err);
			});

		}
	});



	$(document).on('click', '.delete_policy_btn', function(){
		var $self = $(this);
		var lnk = $self.closest('.policydoc').find('a').attr('href');
		bootbox.confirm('Confirm you wish to delete policy document.', function(e){
			if(e){
				$.post(
					base_url+'webmanager/policydoc/deletedoc',
					{lnk: lnk},
					function(res){
						console.log(res);
						$('.policydoc').html('');
					}
				);
			}

		});
		return false;
	});

	$(document).on('click', '.view-customer-btn', function(){
		var $self = $(this);
		var id = $self.data('id');
	});


    $('.summernote').summernote({
        height: 370,
		oninit: function() {
			//$("div.note-editor button[data-event='codeview']").click();
		}
    });



	$('.save_pages_contents_btn').on('click', function(){
		var $self = $(this);
		var data = $self.data();
		data.titlefield = $('[name="titlefield"]').val();
		data.contentfield = ($('.summernote').length > 0 ) ? $self.closest('div').find('.summernote').code() : '';

		console.log(data);

		$self.addClass('disabled').html('Loading..');

		$.post(
			base_url+'webmanager/contents/savetexts',
			data,
			function(res){
				console.log(res);

				if(data.id == ''){
					$('#pagetype').data('id', res.id);
					window.location.href = base_url+'webmanager/contents/alternate/'+res.id;
				}else{
					bootbox.alert('<h4 class="modal-title">Changes to '+data.title+' successully saved!</h4>');
					$self.removeClass('disabled').html('Save Changes');
				}

			},
			'json'
		).error(function(err){
			console.log(err);
		});


		return false;


	});

	$('.save_contents_btn').on('click', function(){
		var $self = $(this);
		var field = $self.data('field');
		var title = $self.data('title');
		var text = $self.closest('div').find('.summernote').code();
		$self.button('loading');
		console.log(text);
		$.post(
			base_url+'webmanager/insurance/savetexts',
			{text: text, field: field},
			function(res){
				console.log(res);
				bootbox.alert('<h4 class="modal-title">Changes to '+title+' successully saved!</h4>');
				$self.button('reset');
			},
			'json'
		).error(function(err){
			console.log(err);
		});

		return false;
	});


	$('.save_certificate_btn').on('click', function(){
		var $self = $(this);
		var text = $self.closest('div').find('.summernote').code();
		$self.button('loading');
		//console.log(cert);
		$.post(
			base_url+'webmanager/insurance/savetexts',
			{text: text, field: 'certificate_template'},
			function(res){
				console.log(res);
				bootbox.alert('Changes to certificate template successully saved!');
				$self.button('reset');
			},
			'json'
		).error(function(err){
			console.log(err);
		});
		return false;
	});


	$('.filter-portcode').find('a').on('click', function(e){
		var $self = $(this);
		var field = $self.data('field');
		$self.closest('form').find('[name="search_by"]').val(field);
		$self.closest('li').addClass('active').siblings().removeClass('active');
		e.preventDefault();
	});


	$('.checkbox-switch').bootstrapSwitch();

    var options = {
//        cellHeight: 80,
//        verticalMargin: 10
    };
    $('.grid-stack').gridstack(options);


    $('body').on('click', function(e){
    	//console.log(e.target);
    	if($(e.target).hasClass('sidebar-wrapper-admin')){
			console.log('ye');
    	}
    	else{
    		if($('.sidebar-wrapper-admin').hasClass('active') && !$(e.target).hasClass('nav-toggle') && !$(e.target).closest('.nav-toggle').length > 0){
	    		$('.nav-toggle').click();
	    		return false;
		    }
    	}
    });

});

$(function () {
	var options = {
		float: true
	};
	$('.grid-stack').gridstack(options);

	new function () {
		this.items = [
			{x: 0, y: 0, width: 2, height: 2},
			{x: 3, y: 1, width: 1, height: 2},
			{x: 4, y: 1, width: 1, height: 1},
			{x: 2, y: 3, width: 3, height: 1},
//                    {x: 1, y: 4, width: 1, height: 1},
//                    {x: 1, y: 3, width: 1, height: 1},
//                    {x: 2, y: 4, width: 1, height: 1},
			{x: 2, y: 5, width: 1, height: 1}
		];

		this.grid = $('.grid-stack').data('gridstack');

		this.addNewWidget = function () {
			var node = this.items.pop() || {
						x: 12 * Math.random(),
						y: 5 * Math.random(),
						width: 1 + 3 + 3 * Math.random(),
						height: 1 + 3 + 3 * Math.random()
					};
			this.grid.addWidget($('<div id="stack-item'+node.x+'"><div class="grid-stack-item-content panel"><div class="panel-body"> <button type="button" class="close" data-target="#stack-item'+node.x+'" data-dismiss="alert"> <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button> Basic panel example </div></div><div/>'),
				node.x, node.y, node.width, node.height);
			return false;
		}.bind(this);

		$('#add-new-widget').click(this.addNewWidget);
	};
});


/*jQuery material design*/

// Ripple-effect animation
(function($) {
    $(".ripplelink").click(function(e){
        var rippler = $(this);

        // create .ink element if it doesn't exist
        if(rippler.find(".ink").length == 0) {
            rippler.append("<span class='ink'></span>");
        }

        var ink = rippler.find(".ink");

        // prevent quick double clicks
        ink.removeClass("animate");

        // set .ink diametr
        if(!ink.height() && !ink.width())
        {
            var d = Math.max(rippler.outerWidth(), rippler.outerHeight());
            ink.css({height: d, width: d});
        }

        // get click coordinates
        var x = e.pageX - rippler.offset().left - ink.width()/2;
        var y = e.pageY - rippler.offset().top - ink.height()/2;

        // set .ink position and add class .animate
        ink.css({
          top: y+'px',
          left:x+'px'
        }).addClass("animate");
    })
})(jQuery);




jQuery(document).ready(function () {
	"use strict";
	var options = {};
	options.ui = {
		container: ".pwstrength_field",
		showVerdictsInsideProgressBar: true,
		viewports: {
			progress: ".pwstrength_viewport_progress"
		}
	};

	if($('#pww').length > 0){
		$('#pww').pwstrength(options);
	}

});
