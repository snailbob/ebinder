head.ready(function() {

    // Make sure the dom is ready
    jQuery(document).ready(function($) {
  	  $(".animated-top").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {

  	  });


        /*=============================================
        =            Epact Module                     =
        =============================================*/
        var epactCorporate = {

            // Define init method
            init: function() {

                  var $body = $(window.document.body);

                  if ($("body").hasClass("landing")) {
    				  var carouselCount = 0;
    				  // Changes the landing feature every 7 seconds
    				  setInterval(function(){
    					  carouselCount++;
    					  if (carouselCount == 1) {
    						  $("#feature-slide-1").hide();
    						  $("#feature-slide-3").fadeIn();
    						  //$("#feature-slide-2").fadeIn();
    					  } else if (carouselCount == 2) {
    						  $("#feature-slide-2").hide();
    						  $("#news-promo").addClass("dark");
    						  $("#feature-slide-3").fadeIn();
    					  }  else if (carouselCount == 3)  {
    						  $("#feature-slide-3").hide();
    						  $("#news-promo").removeClass("dark");
    						  $("#feature-slide-4").fadeIn();
    					  }	 else if (carouselCount == 4)  {
    						  $("#feature-slide-4").hide();
    						  $("#feature-slide-1").fadeIn();
    						  carouselCount = 0;
    					  }
    				  }, 7000);
                  }

                  $("label[for=inputEmail]").addClass("no-display");

                  // A clone of the login form at the top
				  var nonMobileLogin = $(".navbar-collapse #epact-login-form").clone();
				  var mobileLogin = $("#mini-login #epact-login-form").clone();

					function mobilizeMenu() {
						var viewportWidth = $(window).width();
						var viewportHeight = $(window).height();

						if (viewportWidth < 767) {

							// Removes the header login form
							if ($(".navbar-collapse #epact-login-form").length) {
								$(".navbar-collapse #epact-login-form").remove();
							}

							// Readds the mobile login form if it's not already there
							if (!($("#mini-login #epact-login-form").length)) {
								mobileLogin.appendTo("#mini-login");
							}

							$(".navbar").addClass("navbar-fixed-top mm-fixed-top");
							$("#main-container").addClass("mobilized");

							if ($("#cookies-detection").length) {
								$("#cookies-detection").css("top","120px");
								//$(".navbar").css("top","52px");
							}
						} else {

							// Readds the header login form if it's not already there
							if (!($(".navbar-collapse #epact-login-form").length)) {
								$("#mini-login #epact-login-form").remove();
								nonMobileLogin.prependTo(".navbar-collapse");
							}

							// Removes the mobile login form
							if ($("#mini-login #epact-login-form").length) {
								$("#mini-login #epact-login-form").remove();
							}

							$(".navbar").removeClass("navbar-fixed-top mm-fixed-top");
							$("#main-container").removeClass("mobilized");
						}
					}

					mobilizeMenu();

					$( window ).resize(function() {
						mobilizeMenu();
					});

					// Makes 3d transforms work with IE11
				 	Modernizr.addTest('preserve3d', function(){return Modernizr.testAllProps('transformStyle', 'preserve-3d');});

					 // For advanced example which allows you to customize how tweet time is
					 // formatted you simply define a function which takes a JavaScript date as a
					 // parameter and returns a string!
					 // See http://www.w3schools.com/jsref/jsref_obj_date.asp for properties
					 // of a Date object.
					 function dateFormatter(date) {
					   return date.toTimeString();
					 }


					 // ##### Advanced example 2 #####
					 // Similar as previous, except this time we pass a custom function to render the
					 // tweets ourself! Useful if you need to know exactly when data has returned or
					 // if you need full control over the output.
					 //twitterFetcher.fetch('445458471513636864', 'epact-tweets', 1, true, false, true, '', false, handleTweets, false);

					 function handleTweets(tweets){
					     var x = tweets.length;
					     var n = 0;
					     var element = document.getElementById('epact-tweets');
					     var html = '';
					     while(n < x) {
					       html += tweets[n];
					       n++;
					     }
					     html += '';
					     element.innerHTML = html;

			        	$('#epact-tweets a').attr('target', '_blank');
					 }

					// Displays the navbar dropdowns when hovering
				 	$('.navbar .dropdown').hover(function() {
					  $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
					}, function() {
					  $(this).find('.dropdown-menu').first().stop(true, true).slideUp(105)
					});

				 	$('#login-help-tooltip').tooltip();

				 	setTimeout(function() {

				 		var inputFocused = false;
				 		if ($("#inputEmail").is(":focus")) {
				 			inputFocused = true;
				 		}
					  	$("#my-menu").mmenu().on("opening.mm",function() {
							$(".navbar-brand").hide();
						}).on("closed.mm",function() {
							$(".navbar-brand").fadeIn();
							$(".mobile-spacer").css("margin-top","inherit");
						});

					  	if (inputFocused) {
					  		$("#inputEmail").focus();
					  	}

		                $("label[for=inputEmail]").remove();
		                $("label[for=inputEmail]").removeClass("no-display");

				 	}, 1000);
					// Initializes the slider menu on mobile


	            	// Prevents big animations from loading twice
	            	setTimeout(function() {
	                	$(".animated-top").removeClass("animated");
	            	}, 500);

	            	if ($("body").hasClass("faqs")) {
	            		$("#faq-section .collapse").collapse();
	            	}

			   		// Opens the slider menu
			   		$("#open-slider").click(function() {
						$(".navbar").removeClass("navbar-fixed-top");
			   			$("#my-menu").trigger("open");
						$(".mobile-spacer").css("margin-top","60px");
					});

					// Used for flipping information cards
					$('.hover').hover(function(){
						$(this).addClass('flip');
					},function(){
						$(this).removeClass('flip');
					});


					// Smooth scrolls to an element on the page
					$(".smoothScroll").click(function() {
						var scrollElement = $(this).data("scrollto");
						$('html, body').animate({
							scrollTop: $(scrollElement).offset().top - 110
						}, 1000);
					});

					// Smooth scrolls to an element on the page from the menu
					$(".menuScroll").click(function() {
						var parentElement= $(this).parent(".menuItem");
						var scrollElement = $(this).data("scrollto");
						$(".menuItem").removeClass("active");


						// Fixes switch between fixed and relative positioning when at the top of the page
						if (($("#home-nav").css("position") == "relative") && (scrollElement != "#home-what"))  {
							$('html, body').animate({
								scrollTop: $(scrollElement).offset().top - 228 //114 x 2 = scrolling menu pixel height
							}, 1000, function() {
								$(".menuItem").removeClass("active");
								$(parentElement).addClass("active");
							});
						} else {
							$('html, body').animate({
								scrollTop: $(scrollElement).offset().top - 114 //114 = scrolling menu pixel height
							}, 1000, function() {
								$(".menuItem").removeClass("active");
								$(parentElement).addClass("active");
							});
						}


					});
					// Smooth scrolls to an element on the page from the side indicator menu
					$(".side-item").click(function() {
						var scrollElement = $(this).data("anchor-target");
						$('html, body').animate({
							scrollTop: $(scrollElement).offset().top
						}, 1000);
					});

					// Adds a pulse effect to video links
					$(".video-link > img").hover(function() {
						$(this).addClass("animated pulse");
					}, function() {
						$(this).removeClass("animated pulse");
					});

					// Toggles the display of the mini mobile login popup
					$("#btn-mini-login").click(function() {
						$("#mini-login-fader").fadeToggle();
						$("#mini-login-popup").slideToggle();
					});

					// Toggles the display of the mini mobile login help popup
					$("#btn-mini-login-help").click(function() {
						$("#mini-login").hide();
						$("#mini-login-help").fadeIn();
					});

					// Closes the mini login when the user clicks on the x
					$(".close-mini-login").click(function() {
						$("#mini-login-fader").fadeOut();
						$("#mini-login-popup").slideUp();
						$("#mini-login-help").hide();
						$("#mini-login").show();
					});

					// Scrolls to the top of the page
					$("#top-button").click(function() {
						$('html, body').animate({
							scrollTop: 0
						}, 730);
					});
//
//					 Inits the carousels
					$('.carousel').carousel({
					  interval: 10000
					});


					if ($("body").hasClass("contactUs")) {
						function initialize() {
							  var epactLocation = new google.maps.LatLng(49.313946,-123.083546);
							  var mapOptions = {
								center: epactLocation,
								zoom: 14
							  };
						 	 var map = new google.maps.Map(document.getElementById('contact-map'), mapOptions);

							var marker = new google.maps.Marker({
								position: epactLocation,
								map: map,
								title:"ePACT Network Ltd."
							});

						}
						google.maps.event.addDomListener(window, 'load', initialize);
					}

					// Inits the collapsable items

					// Allows carousel items to be shifted by 1 col at a time
					$('#story-carousel .item').each(function(){
					  var next = $(this).next();
					  if (!next.length) {
						next = $(this).siblings(':first');
					  }
					  next.children(':first-child').clone().appendTo($(this));

					  if (next.next().length>0) {
						next.next().children(':first-child').clone().appendTo($(this));
					  }
					  else {
						$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
					  }
					});

					// Redirects user to case study when they click on the div
//					$(".who-item").click(function() {
//						if ($(this).data("url") != "") {
//							window.location.href = $(this).data("url");
//    					}
//					});

					// Toggles the title colours
//					$(".who-item").hover(function() {
//						$(this).find(".who-title").addClass("hovered");
//					}, function() {
//						$(this).find(".who-title").removeClass("hovered");
//					});



					// Swaps the team member page images when hovered
					var teamSwap = function () {
						var $this = $(this).find(".img-team");
						var newSource = $this.data('alt-src');
						$this.data('alt-src', $this.attr('src'));
						$this.attr('src', newSource);
					}
					$('.team-info').hover(teamSwap, teamSwap);

					// If the user clicks on register and they already have an account, shows login
					$("#register-showlogin").click(function() {
						$("#get-started-register").hide();
						$("#get-started-login").fadeIn();
					});

					$(".submit-corporate").click(function() {
						$(this).closest('form').submit();
						return false;
					});



					// Fades the organization tab
					$(".account-type").click(function() {

//						if ($("#get-started-login").length) {
//							$("#get-started-login").remove();
//						}
						$(".account-type").not(this).addClass("fadeIn");
						$(".account-type").not(this).removeClass("focused");
						$(this).addClass("focused");

						if ($(this).attr("id") == "family-account-type") {
							$("#get-started-login, #get-started-getintouch").hide();
							$("#get-started-register").fadeIn();
							$('html, body').animate({
								scrollTop: $("#get-started-register").offset().top - 100
							}, 750);
							} else {
							$("#get-started-login, #get-started-register").hide();
							$("#get-started-getintouch").fadeIn();
							$('html, body').animate({
								scrollTop: $("#get-started-getintouch").offset().top - 100
							}, 750);
						}
					});
//
					//initiating Skrollr
					var s = skrollr.init({
			            mobileCheck: function() {
			                //hack - forces mobile version to be off
			                return false;
			            }
					});


                  // Inits the scrolling bar on the homepage
                  jQuery(function($) {

	          		if ($("#home-nav").length) {

	          		  $('#home-nav').stickUp({
	          				parts: {
	          				  0:'home-what',
	          				  1:'our-solutions',
	          				  2:'home-why',
	          				  3:'more-reasons',
							  4:'pricing'
//							  ,
//	          				  3: 'epact-stories',
	          				},
	          				itemClass: 'menuItem',
	          				itemHover: 'active',
	          			});
	          		}

                  });
            }

        };

        // Trigger the init method
        epactCorporate.init();


    });

});
