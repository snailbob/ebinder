// JavaScript Document
var action_messages = '';

$(document).ready(function(e) {


  	var jqxhr = $.post(
  		base_url+'formsubmits/action_messages',
  		{},
  		function(res){
  			console.log(res);
  			action_messages = res;
  		},
  		'json'
  	).error(function(err){
  		console.log(err);
  	});

  	jqxhr.done(function(){
  		var banners = action_messages.banners;
  		var c = 2;
  		for(p in banners){
  			console.log(banners[p].image);
  			console.log($('.home-jumbo.slide'+c));

  			var img = base_url+'uploads/avatars/'+banners[p].image;
  			if(banners[p].image != ''){
  				$('.home-jumbo.slide'+c).css('background-image', 'url(' + img + ')');
  			}
  			c++;
  		}
  	});




  	jQuery.extend(jQuery.validator.messages, {
  		required: '<i class="fa fa-times text-danger"></i>',
  		remote: '<i class="fa fa-times text-danger"></i>',
  		email: '<i class="fa fa-times text-danger"></i>',
  		url: '<i class="fa fa-times text-danger"></i>',
  		date: '<i class="fa fa-times text-danger"></i>',
  		dateISO: '<i class="fa fa-times text-danger"></i>',
  		number: '<i class="fa fa-times text-danger"></i>',
  		digits: '<i class="fa fa-times text-danger"></i>',
  		creditcard: '<i class="fa fa-times text-danger"></i>',
  		equalTo: '<i class="fa fa-times text-danger"></i>',
  		accept: '<i class="fa fa-times text-danger"></i>',
  		maxlength: '<i class="fa fa-times text-danger"></i>',
  		minlength: '<i class="fa fa-times text-danger"></i>',
  		rangelength: '<i class="fa fa-times text-danger"></i>',
  		range: '<i class="fa fa-times text-danger"></i>',
  		max: '<i class="fa fa-times text-danger"></i>',
  		min: '<i class="fa fa-times text-danger"></i>',
      pattern: '<i class="fa fa-times text-danger"></i>',

  	});

    $.validator.setDefaults({
  		onkeyup: function(element, ev) {
        var TABKEY = 9;
        if(ev.keyCode != TABKEY) {
          $(element).valid();
        }
        console.log(ev);
  	  },
  		// onfocusout: function(element) {
      //   $(element).valid();
  	  // },
  		highlight: function(element) {
  			$(element).closest('.form-group').addClass('has-error');
  		},
  		unhighlight: function(element) {
  			$(element).closest('.form-group').removeClass('has-error');
  		},
  		success: function(label) {
  			label.addClass("valid").html('<i class="fa fa-check text-success"></i>');
  			console.log(label);
  	  },

    });


    $('[data-toggle="tooltip"]').tooltip();






 	//multiple modal
 	$(document).on('show.bs.modal', '.modal', function () {
 		var zIndex = 1040 + (10 * $('.modal:visible').length);
 		$(this).css('z-index', zIndex);
 		setTimeout(function() {
 			$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
 		}, 350);
 	});
 	//remove multiple scroll for modal
 	$(document).on('hidden.bs.modal', '.modal', function () {
 		if($('.modal:visible').length > 0){
 			setTimeout(function(){
 				$('body').addClass('modal-open');
 			},330);
 		}
 	});




	$('.img-hover').on('click', function(){
		// window.location.href = base_url+'panel/setup/profile';
    $('#myProfileModal').modal('show');
    return false;
	});



	//load #myActivityLogModal
	var $myActivityLogModal = $('#myActivityLogModal');
	var activityLogged = $myActivityLogModal.data('logged');
	if(activityLogged != 'Y'){
		$myActivityLogModal.modal('show');
	}




  $('.list-group-item').on('click', function() {
	$('.glyphicon', this)
    .toggleClass('glyphicon-chevron-up')
    .toggleClass('glyphicon-chevron-down');
  });


	$('.generate_report_form').on('submit', function(){
		var $form = $(this);
		var form_data = $form.serialize()
		console.log(form_data); //return false;
		var date_from = $form.find('[name="date_from"]').val();
		var date_to = $form.find('[name="date_to"]').val();
		var type = 'quotes_report'; //$form.find('[name="type"]').val();
		console.log(base_url+'webmanager/reporting/'+type+'/'+date_from+'/'+date_to);
		window.location.href = base_url+'webmanager/reporting/'+type+'/'+date_from+'/'+date_to+'?'+form_data;
		$form.closest('.modal').modal('hide');
		return false;
	});




	$('[name="attributes[]"]').on('change', function(){
    var $self = $(this);
		if($self.is(':checked')){
			$self.siblings('span').html('<i class="fa fa-trash"></i>');

		}
		else{
			$self.siblings('span').html('Add');

		}
		console.log($self.is(':checked'));
	});




	$('[name="report_template"]').on('change', function(){
		var $self = $(this);
		var inputs = $self.find('option:selected').data('inputs');
		var name = $self.find('option:selected').data('name');
    var $attr = $('[name="attributes[]"]');

		$('[name="template_name"]').val(name);

		$attr.prop('checked', false).closest('.btn').removeClass('btn-primary active').addClass('btn-default');
    $attr.siblings('span').html('Add');

    for (i = 0; i < inputs.length; i++) {
			console.log(inputs[i]);
      $('.report-attributes').find('[value="'+inputs[i]+'"]').siblings('span').html('<i class="fa fa-trash"></i>');
			$('.report-attributes').find('[value="'+inputs[i]+'"]').prop('checked', true).closest('.btn').removeClass('btn-default').addClass('btn-primary active');
		}

		console.log(name, inputs);
	});

	$('[data-toggle="buttons"]').on('click', function(){
		var $self = $(this);
		console.log($self.find('input').is(':checked'));
		setTimeout(function(){
      //reset if radio
      if($self.find('input[type="radio"]').length > 0){
        console.log('radio');
        $self.closest('.row').find('.btn').addClass('btn-default').removeClass('btn-primary active');
      }

      if($self.find('input').is(':checked')){
				$self.find('.btn').addClass('btn-primary act').removeClass('btn-default');
			}
			else{
				$self.find('.btn').addClass('btn-default').removeClass('btn-primary');
			}


		}, 24);
		console.log('haha');
	});


	//confirm message to delete record
	$(document).on('click', '.delete_btn', function(e){
		var $self = $(this);
		var id = $self.data('id');
		var table = $self.data('table');
    var alertt = $self.data('alert');


		if(alertt == 'no'){
			deleteRecord($self, id, table);
		}
		else{
			bootbox.confirm('<h4>'+action_messages.global.generic_delete_message+'</h4>', function(e){
				if(e){
					//alert(id);
					deleteRecord($self, id, table);
				}
			});

		}

		e.preventDefault();
	});

	$(document).on('click', '.customer-update', function(){
		var $self = $(this);
		var id = $self.data('id');
		var arr = $self.data('arr');
		var $form = $('#broker_form');

		$form.find('[name="id"]').val(id);
		$form.find('[name="first"]').val(arr.first_name);
		$form.find('[name="last"]').val(arr.last_name);
		$form.find('[name="first_broker"]').val(arr.first_broker);
		$form.find('[name="email"]').val(arr.email);
		//$form.find('[name="brokerage_id"]').selectpicker('refresh');


		$form.closest('.modal').modal('show');
		console.log(arr);


	});


	$('.selectpicker').selectpicker();


	$('#quote_form').find('[name="customer_id"]').on('change', function(){
	    var selected = $(this).find("option:selected").val();
	    console.log(selected);
	    if(selected != ''){
	    	window.location.href = base_url + 'panel/add_customer_quote/'+selected+'#quote';
	    }
	});



   $('[name="product[]"]').on('change', function(){
	   var $self = $(this);
	   $self.closest('label').removeClass('active');
	   $self.closest('label').find('.fa-stack-2x').toggleClass('fa-square-o fa-check-square-o');
	   $self.closest('label').find('.fa-stack-2x').toggleClass('text-muted text-warning');
	   if($self.is(':checked')){
		   $self.closest('label').addClass('active');
	   }

   });

	// Opens the sidebar menu
	$(".nav-toggle").click(function(e) {
		var $self = $(this);
		console.log('nav-toggle');
		e.preventDefault();
		$("#sidebar-wrapper").toggleClass("active");
		$self.toggleClass( "active" );

		$self.find('i').addClass('fa-spin');
		setTimeout(function(){
			$self.find('i').removeClass('fa-spin');
		}, 300);


	});

	var tableData = $('.table-datatable').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": true,
    "initComplete": function(settings, json) {
      $('.loading-table').hide();
			$(this).show();
	  },
		"bInfo": true,
	    "language": {
	        search: "_INPUT_",
	        searchPlaceholder: "search for..."
	    }

	});

	$('.input_table_search').on('keyup', function(){
		var $search = $('.dataTables_filter input');
		$search.val( this.value ).keyup();

	});

	//resize cover
	function jqUpdateSize(){
		// Get the dimensions of the viewport
		var width = $(window).width();
		var height = $(window).height();
		var banner_offset = 96;
		var new_height = height - banner_offset;
		//$('#jqWidth').html(width);      // Display the width
		$('.banner-blue').css({height: new_height+'px'});    // Display the height
		var new_height2 = height - 88;
		$('#sidebar-wrapper').css({height: new_height2+'px'});
	};
	$(document).ready(jqUpdateSize);    // When the page first loads
	$(window).resize(jqUpdateSize);     // When the browser changes size




//	$('.list-group-rabbit a').on('click', function(){
//		var $self = $(this);
//
//		$self.addClass('active').siblings().removeClass('active');
//
//	});

   $('.send-update-request-customer').on('click', function(){

	   var id = $(this).data('id');
	   bootbox.confirm(action_messages.global.send_updated_req_tocustomer, function(e){
		   if(e){
			   $.post(
					base_url+'formsubmits/send_updated_req_tocustomer',
					{id: id},
					function(res){
						console.log(res);
						bootbox.alert(action_messages.success.send_updated_req_tocustomer);
					},
					'json'

			   ).error(function(err){
				   console.log(err);
			   });

		   }
	   });

   });

   $('[name="1_in_respect_of_income_earned_for_the_current_financial_year_please_provide_a_breakdown_by_state[]"]').on('keyup', function(){
	   var total = 0;
	   $('[name="1_in_respect_of_income_earned_for_the_current_financial_year_please_provide_a_breakdown_by_state[]"]').each(function(index, element) {
		   var val = ($(this).val() != '') ? parseInt($(this).val()) : 0;
		   total += val;
	   });
	   $('.the-total').html(' ('+total+'%)');
	   console.log(total);
   });




   var the_item_count = 1;
   $('.add-items-btn').on('click', function(){
	   var $self = $(this);
	   var form = $self.data('form');
	   var container = $self.data('container');

	   var blnk = 0;

	   var theval = [];
	   var selected_text = $('.'+form).find('option:selected').text();
	   $('.'+form).find('input').each(function(index, element) {
		   theval.push($(this).val());
		   if($(this).val() == ''){
			   blnk += 1;
		   }

       });

	   blnk += ($('.'+form).find('select').val() == '') ? 1 : 0;
	   console.log(blnk);


	   $self.closest('div').find('.text-danger').addClass('hidden');
	   var count = the_item_count; //$('.'+container).find('.'+form+'-clone').length;
	   if(blnk == 0){
			var clone = $('.'+form).clone().removeClass(form).addClass(form+'-clone hidden '+form+'-clone-'+count);
			var sel_val = $('.'+form).find('select').val();
			clone.find('select').val(sel_val);


			//panel
			var panel = '<div class="panel"><div class="panel-body">';
			panel +='<a href="#" class="pull-right remove-item-btn" data-form="'+form+'" data-target="'+form+'-clone-'+count+'">Remove</a>';
			panel +='<strong>'+selected_text+'</strong>';
			panel +='<br/>'+theval[0];
			panel +='<br/>'+theval[1];
			panel +='<br/><strong>$'+theval[2]+'</strong>';
			panel +='</div></div>';

			$('.'+container).append(panel);
			$('.'+container).append(clone);
			console.log(clone);

			//reset
			$('.'+form).find('input').val('');
			$('.'+form).find('select').val('');

			//reset width of container
			$('.'+form).closest('div.the-form-col').removeClass('col-sm-12').addClass('col-sm-7');
			$('.'+container).removeClass('hidden');

			the_item_count++;
	   }
	   else{
		   $self.closest('div').find('.text-danger').removeClass('hidden');
	   }
	   return false;
   });


   var the_item_count3 = 1;
   $('.add-items-btn3').on('click', function(){
	   var $self = $(this);
	   var form = $self.data('form');
	   var container = $self.data('container');

	   var blnk = 0;

	   var theval = [];
	   var selected_text = $('.'+form).find('option:selected').text();
	   $('.'+form).find('input').each(function(index, element) {
		   theval.push($(this).val());
		   if($(this).val() == ''){
			   blnk += 1;
		   }

       });

	   blnk += ($('.'+form).find('select').val() == '') ? 1 : 0;
	   console.log(blnk);


	   $self.closest('div').find('.text-danger').addClass('hidden');
	   var count = the_item_count3; //$('.'+container).find('.'+form+'-clone').length;
	   if(blnk == 0){
			var clone = $('.'+form).clone().removeClass(form).addClass(form+'-clone hidden '+form+'-clone-'+count);
			var sel_val = $('.'+form).find('select').val();
			clone.find('select').val(sel_val);


			//panel
			var panel = '<div class="panel"><div class="panel-body">';
			panel +='<a href="#" class="pull-right remove-item-btn" data-form="'+form+'" data-target="'+form+'-clone-'+count+'">Remove</a>';
			panel +='Name of client: '+theval[0];
			panel +='<br/>Particulars: '+theval[1];
			panel +='<br/>Date of Claim: '+theval[2];
			panel +='<br/>Estimated Quantum: '+theval[3];
			panel +='</div></div>';

			$('.'+container).append(panel);
			$('.'+container).append(clone);
			console.log(clone);

			//reset
			$('.'+form).find('input').val('');

			//reset width of container
			$('.'+form).closest('div.the-form-col'); //.removeClass('col-sm-12').addClass('col-sm-7');
			$('.'+container).removeClass('hidden');

			the_item_count3++;
	   }
	   else{
		   $self.closest('div').find('.text-danger').removeClass('hidden');
	   }
	   return false;
   });


   var the_item_count_dyna = 1;
   $('.add-items-btn-dyna').on('click', function(){
	   var $self = $(this);
	   var form = $self.data('form');
	   var container = $self.data('container');

	   var blnk = 0;

	   var theval = [];
	   var thelabel = [];
	   var selected_text = $('.'+form).find('option:selected').text();
	   $('.'+form).find('input').each(function(index, element) {
		   theval.push($(this).val());
		   thelabel.push($(this).closest('div.row').find('label').text());
		   if($(this).val() == ''){
			   blnk += 1;
		   }

       });

	   blnk += ($('.'+form).find('select').val() == '') ? 1 : 0;
	   console.log(blnk);


	   $self.closest('div').find('.text-danger').addClass('hidden');
	   var count = the_item_count_dyna; //$('.'+container).find('.'+form+'-clone').length;
	   if(blnk == 0){
			var clone = $('.'+form).clone().removeClass(form).addClass(form+'-clone hidden '+form+'-clone-'+count);
			var sel_val = $('.'+form).find('select').val();
			//clone.find('select').val(sel_val);


			//panel
			var panel = '<div class="badge-like">';
			panel +='<a href="#" class="pull-right close remove-item-btn" data-form="'+form+'" data-target="'+form+'-clone-'+count+'"><i class="fa fa-trash-o"></i></a>';

			for(var ival = 0; ival < theval.length; ival++){

				panel += theval[ival]+'<br>';

			}
			panel +='</div>';

			$('.'+container+' .well').append(panel);
			$('.'+container+' .well').append(clone);
			console.log(clone);

			//reset
			$('.'+form).find('input').val('');

			//reset width of container
			$('.'+form).closest('div.the-form-col'); //.removeClass('col-sm-12').addClass('col-sm-7');
			$('.'+container).removeClass('hidden');

			the_item_count_dyna++;
	   }
	   else{
		   $self.closest('div').find('.text-danger').removeClass('hidden');
	   }
	   return false;
   });


   $(document).on('click', '.remove-item-btn', function(){
	   var $self = $(this);
	   var target = $self.data('target');
	   var form = $self.data('form');

	   //full length for form
	   var panel_length = $self.closest('div.badge-like').siblings('div.badge-like').length;
	   if(panel_length == 0){
		   $('.'+form).closest('div.the-form-col').removeClass('col-sm-7').addClass('col-sm-12');
		   $('.'+form).closest('div.the-form-col').siblings().addClass('hidden');
	   }

	   //remove the panel and form
	   $self.closest('.badge-like').remove();
	   $('.'+target).remove();

	   console.log(form, target, panel_length, $self.closest('div.panel'));
	   return false;
   });

   $('[name="business_category_id"]').on('change', function(){
	   var $self = $(this);
	   var val = $self.val();

	   if(val != ''){
		   $.post(
				base_url+'formsubmits/get_occupations',
				{catid: val},
				function(res){
					console.log(res);
					var html = '<option value="">Select Occupation</option>';
					for(var i = 0; i < res.length; i++){
						html += '<option value="'+res[i].id+'">'+res[i].specific_name+'</option>';
					}

					$('[name="occupation"]').html(html);
				},
				'json'

		   );

	   }

   });

   $('.radio-toggle-slide').find('[type="radio"]').on('change', function(){
	   var $self = $(this);
	   var $radtoggle = $self.closest('.radio-toggle-slide');
	   var totoggle = $radtoggle.data('totoggle');
	   var toopen = $radtoggle.data('toopen');
	   console.log(toopen, totoggle);
	   if((toopen == 'N' && $self.val() == 'N') || (toopen != 'N' && $self.val() == 'Y')) {
		   $('.'+totoggle).slideDown();
		   $('.'+totoggle).find('input, select').attr('required', true);

		   $('.'+totoggle).find('.form-group').removeClass('has-warning');
		   $('.'+totoggle).find('input[type="checkbox"], input[type="radio"]').attr('checked', false);
		   $('.'+totoggle).find('label.btn').removeClass('active');
		   $('.'+totoggle).find('input[type="text"], select').val('');

	   }
	   else{
		   $('.'+totoggle).slideUp();
		   $('.'+totoggle).find('input, select').attr('required', false);
	   }

   });


	//geolocation
    $('.geolocation').geocomplete()
	.bind("geocode:result", function(event, result){
		var location_lat = result.geometry.location.lat();
		var location_long = result.geometry.location.lng();
		var $self = $(this);

		$self.siblings('[name="lat"]').val(location_lat);
		$self.siblings('[name="lng"]').val(location_long);
	});

  $('.user_geolocation').on('keyup', function(){
    var $form = $(this).closest('form');
    if($(this).val() == ''){
      $form.find('[name="lat"]').val('');
      $form.find('[name="lat"]').valid('');
    }
    $form.find('[name="lat"]').valid();

  });

  setTimeout(function(){
    $('.user_geolocation').geocomplete({
        details: "ul.map-details",
        detailsAttribute: "data-geo"

    })
    .bind("geocode:result", function(event, result){

      var $form = $(this).closest('form'); //('#agency_profile_form');

      var comp_length = result.address_components.length - 1;
      var country_short = $('[data-geo="country_short"]').text(); //result.address_components[comp_length].short_name; //$form.find('[name="country_short"]').val(
      var location_lat = result.geometry.location.lat();
      var location_long = result.geometry.location.lng();

      $form.find('[name="lat"]').val(location_lat);
      $form.find('[name="lat"]').valid();
      $form.find('[name="long"]').val(location_long);
      $form.find('[name="lng"]').val(location_long);

      console.log(result, result.address_components, country_short);
      getCountryCalling(country_short, $form);

    });

  }, 5);


   $('[name="occupation_find"]').on('keyup', function(){
	   var $self = $(this);
	   var $id = $('[name="occupation_id"]');
	   var $suggest = $('.occupation-suggest');
	   $suggest.addClass('hidden');

	   $id.val('');

	   $.post(
	   		base_url+'formsubmits/search_occupation',
			{keyword: $self.val()},
			function(res){
				console.log(res);

			   if($self.val() != '' && res.length > 0){
				   $suggest.removeClass('hidden');
				   var html = '';
				   for(var i = 0; i < res.length; i++){
				   		html += '<a href="#" class="list-group-item" data-id="'+res[i].id+'">'+res[i].specific_name+'</a>';
				   }

				   $suggest.find('.list-group-suggest').html(html);
			   }
			   else{

			   }


			},
			'json'
	   );

   });


   $('.panel-group-products panel-heading').on('click', function(){

   });

	$(document).on('click', '.list-group-suggest a', function(){
		var $self = $(this);
		var id = $self.data('id');
		var $suggest = $('.occupation-suggest');
		$suggest.addClass('hidden');
		$('[name="occupation_find"]').val($self.text());
		$('[name="occupation_id"]').val(id);
		return false;
   });



	$('.price-text').on('click', function(){
		var $self = $(this);
		$self.addClass('hidden');
		$self.siblings().removeClass('hidden');
	});

	$('.price_form, .text_form').find('a').on('click', function(){
		$(this).closest('.text_form').addClass('hidden').siblings().removeClass('hidden');
		return false;
	});

	$('.text_form [type="button"]').on('click', function(){
		var $btn = $(this);
		var $self = $btn.closest('.text_form');

		$btn.button('loading');
		var newval = $self.find('input[type="text"]').val();
		console.log($self.serialize());
		$.post(
			base_url+'formsubmits/inline_text_update',
			$self.find('input').serialize(),
			function(res){
				console.log(res);
				$self.addClass('hidden').siblings('.price-text').removeClass('hidden').html(newval);
				$btn.button('reset');
			},
			'json'
		).error(function(err){
			console.log(err);
		});

	});


	$('.show_history_btn').on('click', function(){
		var $self = $(this);
		var data = $self.data();
		var $mdl = $('#historyModal');
		// $mdl.modal('show');
		// $mdl.find('.history-content').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin"></i></p>');

		$.post(
			base_url+'formsubmits/get_agency_log',
			data,
			function(res){
				console.log(res);
        bootbox.alert(res.view);
				// $mdl.find('.history-content').html(res.view);
			},
			'json'
		).error(function(err){
			console.log(err);
		});


		return false;
	});


	$('.claim-chat-send').on('click', function(){
		var $self = $(this);
		var $chat = $('.chat-area .chat');
		var height = $chat[0].scrollHeight;
		var id = $self.data('id');
		var to = $self.data('to');
		var message_id = $self.data('message_id');
		var letter = $self.data('letter');
		var $textfield = $('.textfield');
		if($textfield.val() != ''){
			var html = '<div class="chat__item"><div class="chat__item-wrap"><div class="avatar"><div class="avatar__img">';
			html += '<img src="https://placeholdit.imgix.net/~text?txtsize=150&txt='+letter+'&w=200&h=200">';
			html += '</div></div><div class="chat__item-message-wrap"><div class="triangle"></div><div class="chat__item-message">';
			html += $textfield.val();
			html += '</div></div></div><div class="chat__date"><span>';

			$self.button('loading');

			$.post(
				base_url+'formsubmits/claim_submit_respond',
				{id: id, text: $textfield.val(), message_id: message_id, to: to},
				function(res){
					console.log(res);

					html += res.date;
					html += '</span><i class="fa fa-check-circle fa-fw text-success"></i></div></div>';
					$chat.append(html);
					$chat.scrollTop(height);
					$self.button('reset');
					$textfield.val('');
				},
				'json'
			).error(function(err){
				console.log(err);
			});

		}
	});


	$('.abn_input_btn').on('click', function(){
		var $self = $(this);
		$self.button('loading');
		var txt = $('.abn_input').val();
		$.post(
			base_url+'formsubmits/abn_fetch',
			{txt: txt},
			function(res){
				console.log(res);
				bootbox.alert(res.message);
				$self.button('reset');
				//window.location.reload(true);
				//bootbox.alert('Password didn\'t match. Please try again.');
			},
			'json'
		).error(function(err){
			console.log(err);
		});

		console.log('abn');

	});


   $('[name="breakdown_by_state[]"]').on('keyup', function(){
   		console.log('hey');
	   var total = 0;
	   $('[name="breakdown_by_state[]"]').each(function(index, element) {
       var $self = $(this);
       var val = 0;

       if($self.length > 0 && $self.val() != ''){
         val = $self.val();
         val = val.replace(/\,/g,''); // 1125, but a string, so convert it to number
         val = parseInt(val,10);
      }

		   total += val;
	   });
	   $('.the-total').text('('+total+'%)');
	   console.log(total);
   });

	$(document).on('click', '.customer2-update', function(){
		var $self = $(this);
		var id = $self.data('id');
		var arr = $self.data('arr');
		var $form = $('#customer2_form');

		$form.find('[name="id"]').val(id);
		$form.find('[name="first"]').val(arr.first_name);
		$form.find('[name="last"]').val(arr.last_name);
		$form.find('[name="email"]').val(arr.email);

		$form.find('[name="address"]').val(arr.location);
		$form.find('[name="lat"]').val(arr.lat);
		$form.find('[name="lng"]').val(arr.lng);


		$form.find('[name="company_name"]').val(arr.customer_info.company_name);
		$form.find('[name="annaul_revenue"]').val(arr.customer_info.annaul_revenue);
		$form.find('[name="employee_number"]').val(arr.customer_info.employee_number);

		$form.find('[name="country_short"]').val(arr.calling_code);
		$form.find('[name="mobile"]').val(arr.calling_digits);

		$form.find('[name="country_code"]').val(arr.country_id);
		$form.find('[name="country_code"]').selectpicker('refresh');

		$form.find('[name="brokerage_id"]').val(arr.brokerage_id);
		$form.find('[name="brokerage_id"]').selectpicker('refresh');
		$form.find('[name="agency_id"]').val(arr.agency_id);
		$form.find('[name="agency_id"]').closest('.form-group').hide();


	   $('[name="breakdown_by_state[]"]').each(function(index, element) {
		   $(this).val(arr.customer_info.breakdown_by_state[index]);
	   });

		if(arr.access_rights.length > 0){
			$form.find('[name="access_rights[]"]').prop('checked', false).closest('label').addClass('btn-default').removeClass('active btn-primary');
			for(var i = 0; i < arr.access_rights.length; i ++ ){
				$form.find('[value="'+arr.access_rights[i]+'"]').prop('checked', true).closest('label').removeClass('btn-default').addClass('active btn-primary');
			}
		}
		else{
			$form.find('[name="access_rights[]"]').prop('checked', true).closest('label').removeClass('btn-default').addClass('active btn-primary');
		}


		// $form.closest('.modal').modal('show');
		console.log(arr);

    $.post(
        base_url + 'formsubmits/customer2_to_session',
        {data: arr},
        function(res){
          console.log(res);
          var txt = Math.random().toString(36).substr(2, 5);
          window.location.href = base_url+'panel/use_customer?tab=1&new='+txt+'#quote';
        },
        'json'
    );

	});


	$('.mobile_number').mask("9999 999 999");


  $('.new_quote_reset').on('click', function(){
    var prod_id = $(this).data('id');
      // onclick="$('#myModal').find('.form-control').val('')" data-toggle="modal" data-controls-modal="#myModal"
      $.post(
          base_url + 'formsubmits/new_quote_reset',
          {prod_id: prod_id},
          function(res){
            console.log(res);
            var txt = Math.random().toString(36).substr(2, 5);
            window.location.href = base_url+'panel/quote?tab=0&new='+txt+'#quote';
          },
          'json'
      );
  });


	$('[name="enable_whitelabel"]').bootstrapSwitch();
  $('.bs_switch').bootstrapSwitch().on('switchChange.bootstrapSwitch', function (event, state) {
    console.log(event, state);
    $('.the_new_cust').toggleClass('hidden');
    $('.the_existing').toggleClass('hidden');

    // if(!state){
    //   $('.the_new_cust').find('.form-control').attr('required', true);
    //   $('.the_existing').find('.form-control').attr('required', false);
    // }
    // else{
    //   $('.the_new_cust').find('.form-control').attr('required', false);
    //   $('.the_existing').find('.form-control').attr('required', true);
    // }
  });


  $('[name="new_template"]').bootstrapSwitch({
		onSwitchChange: function(event, state){
			console.log(event, state);

			var $row = $('.row-select-template');
			var $rowNew = $('.row-new-template');
			$('.report-attributes').slideDown();
			$('[name="attributes[]"]').prop('checked', false).closest('.btn').removeClass('btn-primary active').addClass('btn-default');
			$('.hidden[name="report_template"]').prop('checked', true);
			$('[name="template_name"]').val('');

			$('[name="attributes[]"]').siblings('span').html('Add');

			if(!state){
				var lenthh = $('.modal').find('[name="report_template"]').length;

				if(lenthh > 1){
					$('#selectTemplateModal').modal('show');
				}
				else{
					bootbox.confirm('There are no saved templates, would you like to create a new template instead?', function(e){
						if(e){
							$('[name="new_template"]').bootstrapSwitch('toggleState');
						}
					});
				}
				// $row.slideDown();
				$rowNew.slideUp();
			}
			else{
				// $row.slideUp();
				$rowNew.slideDown();

			}
		}
	});

  $('input[name="date_from"]').datetimepicker({
			format: 'DD-MM-YYYY'
	});

	$('input[name="date_to"]').datetimepicker({
			format: 'DD-MM-YYYY',
			minDate: moment().add(30, 'd')
	});



	//link both date pickers
	$('input[name="date_from"]').on("dp.change", function (e) {
		$('input[name="date_to"]').data("DateTimePicker").minDate(e.date);
	});
	$('input[name="date_to"]').on("dp.change", function (e) {
		$('input[name="date_from"]').data("DateTimePicker").maxDate(e.date);
	});




	$('#password_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-error');
		},
		errorElement: 'span',
		errorClass: 'help-block',
		errorPlacement: function(error, element) {
			 //element.closest('div').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button').button('loading');
			$.post(
				base_url+'formsubmits/activity_logged_pass',
				$(form).serialize(),
				function(res){
					console.log(res);
					if(res.result == 'match'){
						window.location.reload(true);
					}
					else{
						bootbox.alert(action_messages.error.user_confirm_auth);
						$(form).find('button').button('reset');
					}
				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});


	$('#customer_details_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {


			$(form).find('button[type="submit"]').button('loading');
			console.log($(form).serialize());
			$.post(
				base_url+'formsubmits/update_customer_admin',
				$(form).serialize(),
				function(res){
					console.log(res);

					$(form).find('button[type="submit"]').button('reset');
					bootbox.alert(action_messages.success.generic_update_message, function(){
            $('.modal.in').modal('hide');
          });

				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});



	$('#brokerage_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {

			$(form).find('button[type="submit"]').button('loading');

			$.post(
				base_url+'webmanager/customers/save_brokerage',
				{data: $(form).serializeArray() },
				function(res){
					console.log(res);
					window.location.reload(true);
					//$(form).find('button[type="submit"]').button('reset');
				},
				'json'
			).error(function(err){
				console.log(err);

				$(form).find('button[type="submit"]').button('reset');
			});


		}
	});



	$('#customer2_form').validate({
    ignore: [],
		rules:{
			mobile: {
				required: true,
        pattern: /^[\d\s]+$/,
        minlength: 6,
			}
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
      console.log(element);
			 element.closest('.form-group').append(error);
		},
    // messages:{
    //   'breakdown_by_state[]': {
    //     max: ''
    //   }
    // },
		submitHandler: function(form) {
      if($('.the-total').length > 0 && $('.the-total').text() != '(100%)'){
        console.log($('.the-total').html());
				bootbox.alert(action_messages.error.validate_total_revenue);
				return false;
			}






      //checkEmployees & Payroll section
      var $emp_no = $('[name="employee_number"]');
      if($emp_no.length > 0 && $emp_no.val() != ''){
        var emp_no = $emp_no.val();
        emp_no = emp_no.replace(/\,/g,''); // 1125, but a string, so convert it to number
        emp_no = parseInt(emp_no,10);

        var total = 0;
        $('[name="employee_by_location[]"]').each(function(index, element) {
          var the_val = $(this).val();
          if(the_val != ''){
            the_val = the_val.replace(/\,/g,''); // 1125, but a string, so convert it to number
            var val = parseInt(the_val,10);
            total += val;

          }


        });

        console.log(total, emp_no);

        if(total != emp_no){
          bootbox.alert(action_messages.error.validate_employee_by_location);
          return false;

        }

      }





			$(form).find('button[type="submit"]').button('loading');
			console.log($(form).serialize());
			$.post(
				base_url+'formsubmits/add_customer2',
        {data: $(form).serializeArray()},
				function(res){
          // $(form).find('button[type="submit"]').button('reset');

					 console.log(res);

           if(res.save_customer){
             window.location.href = base_url+'panel/use_customer';
           }
           else{
             window.location.href = base_url+'panel/use_customer?tab='+res.thenum_next+'#quote';

           }


					// if(res.result != 'ok'){
					// 	$(form).find('button[type="submit"]').button('reset');
					// 	bootbox.alert(res.message);
					// }
					// else{
					// 	window.location.reload(true);
					// }

				},
				'json'
			).error(function(err){
				console.log(err);
				$(form).find('button[type="submit"]').button('reset');
			});
		}
	});







   $('#claim_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');
			var uniquetime = $('#uniquetime').val();
			var data = $(form).serialize() + '&uniquetime=' + uniquetime;
			console.log(data);

			$.post(
				base_url+'formsubmits/save_claim',
				data,
				function(res){
					console.log(res);
          bootbox.alert(action_messages.success.generic_add_message, function(){
            window.location.reload(true);
          });
					// $(form).find('button[type="submit"]').button('reset');
				},
				'json'
			).error(function(err){
				console.log(err);
				$(form).find('button[type="submit"]').button('reset');
			});
		},
   });

   $('#broker_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');

			$.post(
				base_url+'formsubmits/submit_the_broker',
				$(form).serialize(),
				function(res){
					console.log(res);
					$(form).closest('.modal').modal('hide');
					bootbox.alert(res.message, function(){
						window.location.reload(true);
					});
				},
				'json'
			).error(function(err){
				console.log(err);
				$(form).find('button[type="submit"]').button('reset');
			});

		}
   });

   $('#biz_referral_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button[type="submit"]').button('loading');

			$.post(
				base_url+'formsubmits/referral_required',
				{data: $('#quote_form').serializeArray(), customer: $(form).serializeArray()},
				function(res){
					console.log(res);
						$(form).closest('.modal').modal('hide');

						setTimeout(function(){
							bootbox.alert(action_messages.success.generic_add_message, function(){
								window.location.href = base_url + 'panel/quote';
							});
							$(form).find('button[type="submit"]').html('Submitted!');
						}, 600);
				},
				'json'
			).error(function(err){
				console.log(err);
				$(form).find('button[type="submit"]').button('reset');
			});

		}
   });


	$('.renew_action_btn').on('click', function(){
		var $self = $(this);
		var id = $self.data('id');
		var type = $self.data('type');
		$self.closest('.modal').modal('hide');

		var mess = (type == 'accept') ? action_messages.global.confirm_renewal_quote : action_messages.global.request_customer_update;
    var action_mess = (type == 'accept') ? action_messages.success.confirm_renewal_quote : action_messages.success.request_customer_update;

  	bootbox.confirm(mess, function(e){
			if(e){

				$.post(
					base_url+'formsubmits/ml_broker_bind_renew',
					{id: id, type: type},
					function(res){
						console.log(res);
						bootbox.alert(action_mess, function(){
							window.location.href = base_url+'panel/quote?tab=6';
						});
					},
					'json'
				).error(function(err){
					console.log(err);
				});



			}
		});


	});


	$('.renew-policy2').on('click', function(){
		var $self = $(this);
		var id = $self.data('id');
		var $mdl = $('#renewModal');
		$mdl.find('.btn').attr('data-id', id);
		$mdl.modal('show');
	});
	$('.renew-policy').on('click', function(){
		var $self = $(this);
		var id = $self.data('id');
		var action = $self.data('action');
    var mess = 'Confirm endorsement.';

    if(action == 'renew') { mess = 'Accept renewal Quote'; }
    if(action == 'email') { mess = 'Email quote to customer'; }

		console.log(id, action);
		bootbox.confirm(mess, function(e){
			if(e){

				$.post(
					base_url+'formsubmits/ml_policy_renew',
					{id: id, action: action},
					function(res){
						console.log(res);
						window.location.href = base_url+'panel/quote?tab=1';
					},
					'json'
				).error(function(err){
					console.log(err);
				});



			}
		});
	});

	$('.biz-bind-now').on('click', function(){
		var $self = $(this);
    var id = $self.data('id');
    var type = $self.data('type');
    var message = (type == 'bind') ? 'Confirm to bind quote?' : action_messages.global.confirm_renewal_quote;
		console.log(id);
		bootbox.confirm(message, function(e){
			if(e){

				$.post(
					base_url+'formsubmits/ml_broker_bind',
					{id: id},
					function(res){
						console.log(res);
						bootbox.alert(action_messages.success.confirm_renewal_quote, function(){
							window.location.href = base_url+'panel/quote?tab=6';
						});
					},
					'json'
				).error(function(err){
					console.log(err);
				});



			}
		});
	});

   $('#quote_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {


      //if new_or_existing_customer
      if($(form).find('[name="new_or_existing_customer"]').length > 0){
          if($('[name="customer_id"]').val() == '' && $('[name="email"]').val() == ''){
            bootbox.alert('Please add/select customer.');
            return false;
          }
      }


    	$(form).find('button[type="submit"]').button('loading');

			$('#myModal').modal('show');
		//	return false;
			$.post(
				base_url+'formsubmits/homebiz1',
				{data: $(form).serializeArray()},
				function(res){
					console.log(res);
					//bootbox.alert('Successfully saved policies.');
					//$(form).find('button[type="submit"]').button('reset');
//					setTimeout(function(){
//						window.location.href = base_url+'panel/quote?tab='+res.thenum;
//					}, 1500);


					var title = (res.matching_referral.length == 0) ? 'Add Customer details to Seek instructions' : 'Referral Required';
					if((res.matching_referral.length > 0 || res.thenum == 6) && (res.biz_quote_id == '')){
						$('#myModal').modal('hide');
						setTimeout(function(){
							var $mdl = $('#myReferralModal');
							$mdl.find('.modal-title').html(title);
							$mdl.modal('show');
							$(form).find('button[type="submit"]').button('reset');
						}, 600);
					}
					else{
						setTimeout(function(){
							if(typeof(res.renewal) !== 'undefined'){
								bootbox.alert(action_messages.success.quote_form_completed, function(){
									window.location.href = base_url+'panel/quote/3';
								});
							}
							else{
								if(typeof(res.customer_quote) !== 'undefined'){
									if(res.customer_quote == '0'){
										window.location.href = base_url+'landing/brokerform/'+res.cb_id+'?tab='+res.thenum;
									}
									else{
										window.location.href = base_url+'landing/update_quote_info/'+res.customer_quote+'?tab='+res.thenum;
									}
								}
								else{
									window.location.href = base_url+'panel/quote?tab='+res.thenum+'#quote';
								}
							}
						}, 1500);
					}
				},
				'json'
			).error(function(err){
				console.log(err);
				$('#myModal').modal('hide');
				$(form).find('button[type="submit"]').button('reset');
			});


		}
   });



	//open login
	if((uri_2 == 'quote' || uri_2 == 'use_customer' || uri_2 == 'eproducts')  && location.hash == '#quote' || uri_2 == 'brokerform' || uri_2 == 'update_quote_info'){
    console.log('aw');
		$('#myModal').modal('show');
    setTimeout(function(){
      $('.quote_hidden').removeClass('hidden');
    }, 350)
	}
  else{
    $('.quote_hidden').removeClass('hidden');
  }

   $('#broker_policy_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		invalidHandler: function() {
			var $pgroup = $('.panel-group-products');
			$pgroup.find('.panel-collapse').addClass('in').attr('aria-expanded', 'true').css('height', 'auto');
			$pgroup.find('.panel-heading').removeClass('collapsed');

		},
		submitHandler: function(form) {
			$(form).find('button').button('loading');

			$.post(
				base_url+'formsubmits/save_broker_policy',
				{data: $(form).serializeArray()},
				function(res){
					console.log(res);

					bootbox.alert(action_messages.success.generic_add_message);
					$(form).find('button[type="submit"]').button('reset');
				},
				'json'
			).error(function(err){
				console.log(err);
				$(form).find('button[type="submit"]').button('reset');
			});

		}
   });

   $('#note_comment_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {
			$(form).find('button').button('loading');

$.post(
				base_url+'formsubmits/save_broker_note',
				$(form).serialize(),
				function(res){
					console.log(res);

					bootbox.alert(action_messages.success.generic_add_message);
					$(form).find('button[type="submit"]').button('reset');
				},
				'json'
			).error(function(err){
				console.log(err);
				$(form).find('button[type="submit"]').button('reset');
			});

		}
   });


   $('#customer_form').validate({
		ignore: [],
		rules: {
			first: {
				required: true
			},
			last: {
				required: true
			},
			bname: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
//			business_category_id: {
//				required: true
//			},
//			occupation: {
//				required: true
//			},
			'product[]': {
				required: true
			},
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {

			if($(form).find('[name="occupation_id"]').val() == '' && $(form).find('[name="occupation"]').val() == ''){
				bootbox.alert('Please select industry.');
				return false;
			}


			$(form).find('button[type="submit"]').button('loading');


			$.post(
				base_url+'formsubmits/save_customer',
				{input: $(form).serializeArray()},
				function(res){
					console.log(res);

					if(res.customer_type == 'N'){
						window.location.href = base_url2+'panel/customers/add?tab=2';
					}
					else{
						window.location.href = base_url+'business/customers?tab=2';
					}


//					$('.the-insurance-form').addClass('hidden');
//					$('.the-information-form').removeClass('hidden');
//
//					$('.level1').find('div').removeClass('text-primary');
//					$('.level2').find('div').addClass('text-primary');


				},
				'json'
			).error(function(err){
				console.log(err);
				$(form).find('button[type="submit"]').button('reset');
			});

		}
	});


   $('#addmember_form').validate({
		ignore: [],
		rules: {
			first: {
				required: true
			},
			last: {
				required: true
			},
			bname: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
		},
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		submitHandler: function(form) {

			$(form).find('button[type="submit"]').button('loading');

			$.post(
				base_url+'formsubmits/save_member',
				{input: $(form).serializeArray()},
				function(res){
					console.log(res);
					window.location.href = base_url2+'panel/';

				},
				'json'
			).error(function(err){
				console.log(err);
				$(form).find('button[type="submit"]').button('reset');
			});

		}
	});


	$('#tab2_form').validate({
		ignore: [],
		highlight: function(element) {
			$(element).closest('.form-group').addClass('has-warning');
		},
		unhighlight: function(element) {
			$(element).closest('.form-group').removeClass('has-warning');
		},
		errorElement: 'span',
		errorClass: 'help-block',

		errorPlacement: function(error, element) {
			 element.closest('.form-group').append(error);
		},
		invalidHandler: function() {
			var $pgroup = $('.panel-group-products');
			$pgroup.find('.panel-collapse').addClass('in').attr('aria-expanded', 'true').css('height', 'auto');
			$pgroup.find('.panel-heading').removeClass('collapsed');

		},
		submitHandler: function(form) {
			if($('.the-total').html() != '100'){
				bootbox.alert(action_messages.global.validate_total_revenue);
				return false;
			}
			console.log($(form).serialize());
			$(form).find('[type="submit"]').button('loading');
			$.post(
				base_url+'formsubmits/store_tab2',
				{data: $(form).serializeArray()},
				function(res){
					console.log(res);
					$(form).find('[type="submit"]').button('reset');
					bootbox.alert(res.message);
				},
				'json'

			).error(function(err){
				console.log(err);
				$(form).find('[type="submit"]').button('reset');
			});
		}
	});



	$('.policy_filter_btn a').on('click', function(){
		var $self = $(this);
		var val = $self.data('value');
		var $search = $self.closest('.panel').find('.dataTables_filter input');
		$search.val( val ).keyup();
		$self.closest('li').addClass('active').siblings().removeClass('active');

	});



	$('.input-number, [type="number"]').on('keyup', function(){
		var val = $(this).val();
		//var
		var n = parseInt($(this).val().replace(/\D/g,''),10);
		//n = (isNaN(n)) ? 0 : n;
		if(!isNaN(n)){
			$(this).val(n.toLocaleString());
		}
	});

	$('.input-number').on('keypress', function(evt){

		if (evt.which < 48 || evt.which > 57){
			evt.preventDefault();
		}
	});

    // Monkey patch jQuery form validator to handle arrays of input elements
    // http://blog.kyawzinwin.me/jquery-validation-for-array-of-input-elements/
    jQuery.validator.prototype.checkForm = function() {
        this.prepareForm();
        for ( var i = 0, elements = (this.currentElements = this.elements()); elements[i]; i++ ) {
            if (this.findByName( elements[i].name ).length != undefined && this.findByName( elements[i].name ).length > 1) {
                for (var cnt = 0; cnt < this.findByName( elements[i].name ).length; cnt++) {
                    this.check( this.findByName( elements[i].name )[cnt] );
                }
            } else {
                this.check( elements[i] );
            }
        }
        return this.valid();
    };

	$('.deactivate-member').on('click', function(){
		var $self = $(this);
		var id = $self.data('id');
		var enabled = $self.data('enabled');
		var txt = (enabled == 'N') ? 'activate' : 'deactivate';

		bootbox.confirm('Please confirm you wish to '+txt+' user?', function(e){
			if(e){
				console.log('w');
				$.post(
					base_url+'formsubmits/deactivate_member',
					{id: id, enabled: enabled},
					function(res){
						console.log(res);
						bootbox.alert(res.message, function(){
							window.location.reload(true);
						});
					},
					'json'

				).error(function(err){
					console.log(err);
					$(form).find('[type="submit"]').button('reset');
				});


			}
		});
	});

	$('.upload-policy-file-btn').on('click', function(){
		var $self = $(this);
		var id = $self.data('id');
		$('#img_type').val(id);
		console.log($('#img_type').val());
		$('.myfile-profile').click();
		return false;
	});

	//profile upload
	$("#myPolicyFiles").ajaxForm({
		beforeSend: function() {

			$(".progress-profile").show();
			//clear everything
			$(".bar-profile").width('0%');
			$(".percent-profile").html("0%");
		},
		uploadProgress: function(event, position, total, percentComplete){
			var prod_id = $('#img_type').val();
			var $btn = $('.upload-policy-file-btn[data-id="'+prod_id+'"]')
			$btn.button('loading');

			$(".bar-profile").width(percentComplete+'%');
			$(".percent-profile").html(percentComplete+'%');

		},
		success: function() {
			$(".bar-profile").width('100%');
			$(".percent-profile").html('100%');

		},
		complete: function(response) {
			if(response.responseText == 'not_img'){
				$(".progress-profile").hide();
				bootbox.alert('<p class="lead">File selected is not a valid product insurance file.</p>');

			}
			else{
				var prod_id = $('#img_type').val();
				var $btn = $('.upload-policy-file-btn[data-id="'+prod_id+'"]')
				$btn.button('reset');
				$btn.after('<a class="btn btn-danger" href="'+base_url+'uploads/policyd/'+response.responseJSON.name+'" target="_blank"><i class="fa fa-paper-clip"></i> '+response.responseJSON.file_name+'</a>')



//				$('#avatar_name').val(response.responseText);
//				$('.bootstrap-modal-cropper img').attr('src',base_url+''+response.responseText);
//				$("#cropImg").find('.modal-body').css('min-height', '560');
//				$("#cropImg").modal('show');
			}
			console.log(response);

		},
		error: function(){
			console.log('error');
			//bootbox.alert('<h4> Unable to upload file. Please refresh and try again.</h4>');
		},
		dataType: 'json'

	});
	//end ajax file upload



	var optionsClaims = {
		beforeSend: function()
		{
			$("#progress").show();
			//clear everything
			$("#bar").width('0%');
			$("#message").html("");
			$("#percent").html("0%");
		},
		uploadProgress: function(event, position, total, percentComplete)
		{
			$('#myModal').modal('show');
			$("#bar").width(percentComplete+'%');
			$("#percent").html(percentComplete+'%');


		},
		success: function()
		{
			$("#bar").width('100%');
			$("#percent").html('100%');

		},
		complete: function(response)
		{
			// $('#myModal').modal('hide');
			$("#file_up_text").show();
			$("#message").after(response.responseText);

			console.log(response.responseText);
		},
		error: function()
		{
			$("#file_up_text").show();
			$("#message").html("<font color='red'> ERROR: unable to upload files</font>");

		}

	};

	$("#myForm").ajaxForm(optionsClaims);




	//profile upload
	$("#myFormAvatar").ajaxForm({
		beforeSend: function() {
			$(".progress-profile").show();
			//clear everything
			$(".bar-profile").width('0%');
			$(".percent-profile").html("0%");
		},
		uploadProgress: function(event, position, total, percentComplete){
			$(".bar-profile").width(percentComplete+'%');
			$(".percent-profile").html(percentComplete+'%');


		},
		success: function() {
			$(".bar-profile").width('100%');
			$(".percent-profile").html('100%');

		},
		complete: function(response) {
			if(response.responseText == 'not_img'){
				$(".progress-profile").hide();
				bootbox.alert('<p class="lead">'+action_messages.error.upload_file_message+'</p>');

			}
			else{
				$('#avatar_name').val(response.responseText);
				$('.bootstrap-modal-cropper img').attr('src',base_url+''+response.responseText);
				$("#cropImg").find('.modal-body').css('min-height', '560');
				$("#cropImg").modal('show');
			}
			console.log(response.responseText);

		},
		error: function(){
			bootbox.alert('<h4> Unable to upload file. Please refresh and try again.</h4>');
		}

	});
	//end ajax file upload




	//crop on a modal
	var $modal = $("#cropImg"),
		$image = $modal.find(".bootstrap-modal-cropper > img"),
		originalData = {};


	var aspratio =  310/64;
	if($('#customer_img').length > 0 || $('#pagetype').val() == 'threeparts'){
		aspratio = 1 / 1;
	}
	if($('#pagetype').val() == 'firsthomesection'){
		aspratio = 740 / 318;
	}
	if($('#pagetype').val() == 'alternatingcontents'){
		aspratio = 527 / 259;
	}
	if($('#pagetype').val() == 'banner'){
		aspratio = 1800 / 647;
	}




	$modal.on("shown.bs.modal", function() {
		$image.cropper({
			aspectRatio: aspratio,
			multiple: true,
			data: originalData,

			strict: true,
			guides: false,
			highlight: false,
			dragCrop: false,
			cropBoxMovable: false,
			cropBoxResizable: false,

			done: function(data) {
				console.log(data);
			}
		});

	}).on("hidden.bs.modal", function() {
		originalData = $image.cropper("getData");
		$image.cropper("destroy");
	});


	//save crop image
	$('.sav_crop').click(function(e) {
    var $self = $(this);
		var aspect = { width: 310, height: 64};

		if($('#customer_img').length > 0){
			aspect = { width: 150, height: 150};
		}
		if($('#pagetype').val() == 'firsthomesection'){
			aspect = { width: 740, height: 318};
		}
		if($('#pagetype').val() == 'alternatingcontents'){
			aspect = { width: 527, height: 259};
		}
		if($('#pagetype').val() == 'threeparts'){
			aspect = { width: 152, height: 152};
		}
		if($('#pagetype').val() == 'banner'){
			aspect = { width: 1800, height: 647};
		}

		var hey = $image.cropper('getCroppedCanvas', aspect).toDataURL();
		//$image.cropper("getDataURL", "image/jpeg");
		console.log(hey);
		$(this).addClass('disabled');
		$(this).html('Loading...');

		var data = {
			"img_url": hey,
			"img_name": $('#avatar_name').val(),
			"page_type": ($('#pagetype').length > 0) ? $('#pagetype').val() : '',
			"page_id": ($('#pagetype').length > 0) ? $('#pagetype').data('id') : '',
			"img_type": $('#img_type').val()
		};

		data = $.param(data); // $(this).serialize() + "&" +

		$.ajax({
			  type: "POST",
			  url: base_url+"settings/save_profile_pic",
			  dataType: 'json',
			  data: data,
			  success: function(data) {
				  console.log(data);

				  if($('#pagetype').length > 0){
					  $('.img-page-preview').attr('src', hey).closest('p').removeClass('hidden');
					  $self.closest('.modal').modal('hide');

				  }
				  else{
					  window.location.reload();
				  }

			  },
			  error: function(err) {
				  console.log(err);
			  }
		});

	});


    $('.summernote').summernote({
        height: 370,
		oninit: function() {
			//$("div.note-editor button[data-event='codeview']").click();
		}
    });



	$('.save_certificate_btn').on('click', function(){
		var $self = $(this);
		var text = $self.closest('div').find('.summernote').code();
		$self.button('loading');
		//console.log(cert);
		$.post(
			base_url+'webmanager/insurance/savetexts',
			{text: text, field: 'certificate_template'},
			function(res){
				console.log(res);
				bootbox.alert(action_messages.success.generic_update_message);
				$self.button('reset');
			},
			'json'
		).error(function(err){
			console.log(err);
		});
		return false;
	});


   $('.keyboard .keyboard__number').on('click', function(){
	   var $self = $(this);
	   var num = $self.html();
	   var $input = $(document).find('.active-numpad');
	   var val = $input.val();
	   val = val + num;
	   $input.val(val);
	   $input.focus();
	   setTimeout(function(){
		   $input.keyup();
	   }, 0);

	   console.log(num, val);

   });

   $('.keyboard .keyboard__delete').on('click', function(){
	   var $input = $(document).find('.active-numpad');
	   var val = $input.val();
	   val = val.slice(0, -1);
	   $input.val(val);
	   $input.focus();
	   setTimeout(function(){
		   $input.keyup();
	   }, 0);

   });




 	//increment btn
 	$('.increment_input_btn button').on('click', function(){
 		var $self = $(this);
 		var $input = $self.closest('.form-group').find('input.input-number');
 		var value = $input.val();

 		if(value == '' || isNaN(value)){
 			value = 0;
 		}

 		if($self.find('i').hasClass('fa-plus-circle')){
 			value++;
 			$input.val(value);
 		}
 		else{
 			if(value != 0){
 				value--;
 			}
 			$input.val(value);
 		}
 		return false;
 	});





	$('.promo-code-btn').on('click', function(){
		var $self = $('.promo-code-input');
		var txt = ($self.val() != '') ? '<i class="fa fa-spinner fa-spin"></i> Validating...' : '';
		$self.siblings('.help-block').html(txt);
		$('[name="5_discount"]').val('0');

		if($self.val() != ''){

			$.post(
				base_url+'formsubmits/validate_promo',
				{promo: $self.val()},
				function(res){
					console.log(res);
					if(res.count > 0){
						var txt = '<span class="text-success">'+res.code[0].discount+'% discount</span>';
						$self.siblings('.help-block').html(txt);
						$('[name="5_discount"]').val(res.code[0].discount);
					}
					else{
						var txt = 'No matching promo found.';
						$self.siblings('.help-block').html(txt);
					}
				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}

		console.log($self.val());
    return false;
	});

	$('input.input-number-pad, input.input-number').on('focus', function(){
		console.log('yeah');
		var $self = $(this);
		$('input').removeClass('active-numpad');
		$self.addClass('active-numpad');

		$('.numpad').show('fast');
	});

	$('body').on('click', function(e){
			if($(e.target).hasClass('keyboard__number') || $(e.target).hasClass('keyboard__delete')  || $(e.target).is('input.input-number-pad') || $(e.target).is('input.input-number') ){
				console.log('yeah bdrop');
			}
			else{
				$('.numpad').fadeOut('fast');
			}

	});

	//search policies
    $('#system-search').keyup( function() {
       var that = this;
	   var table = $(that).data('table');
        // affect all table rows on in systems table
        var tableBody = $('.'+table+' tbody');
        var tableRowsClass = $('.'+table+' tbody tr');
        $('.search-sf').remove();
        tableRowsClass.each( function(i, val) {

            //Lower text for case insensitive
            var rowText = $(val).text().toLowerCase();
            var inputText = $(that).val().toLowerCase();
            if(inputText != '')
            {
                $('.search-query-sf').remove();
                tableBody.prepend('<tr class="search-query-sf"><td colspan="6"><strong>Searching for: "'
                    + $(that).val()
                    + '"</strong></td></tr>');
            }
            else
            {
                $('.search-query-sf').remove();
            }

            if( rowText.indexOf( inputText ) == -1 )
            {
                //hide rows
                tableRowsClass.eq(i).hide();

            }
            else
            {
                $('.search-sf').remove();
                tableRowsClass.eq(i).show();
            }
        });
        //all tr elements are hidden
        if(tableRowsClass.children(':visible').length == 0)
        {
            tableBody.append('<tr class="search-sf"><td class="text-muted" colspan="6">No entries found.</td></tr>');
        }
    });


    $('body').on('click', function(e){
    	console.log(e.target);
    	if($(e.target).hasClass('sidebar-wrapper-admin')){
			console.log('ye');
    	}
    	else{
    		if($('.sidebar-wrapper-admin').hasClass('active') && !$(e.target).hasClass('nav-toggle') && !$(e.target).closest('.nav-toggle').length > 0){
	    		$('.nav-toggle').click();
	    		return false;
		    }
    	}
    });

    $('.add_customer_btn').on('click', function(){
      var $mdl = $('#myModal');
      $mdl.find('.form-control').val('');
      // $mdl.modal('show');
      $.post(
    		base_url+'formsubmits/reset_customer2_form',
    		{},
    		function(res){
    			console.log(res);
          var txt = Math.random().toString(36).substr(2, 5);
          window.location.href = base_url+'panel/use_customer?tab=1&new='+txt+'#quote';
    		},
        'json'
    	);
      return false;


    });

   setTimeout(function(){
     //  $('#myLoadingModal').modal('hide');
      $('.loading').hide();
      $('.complete_loading_content').removeClass('hidden');

   }, 0);

});

// setTimeout(function(){
//   if($('.modal.in').length == 0 && location.hash != '#quote'){
//     $('#myLoadingModal').modal('show');
//   }
// }, 0);


function customerDetails(id){

	var $mdl = $('#infoModal');
	$mdl.find('.customer_info').html('<p class="text-center text-muted"><i class="fa fa-spinner fa-spin fa-2x"></i><br>Loading..</p>');

	$mdl.find('.customer_info').load( base_url+'landing/customer_info/'+id, function() {
	});

	$mdl.modal('show');
}

var deleteRecord = function(el, id, table){
	var mydataTb = $('.mydataTb').DataTable();
	$.post(
		base_url+'formsubmits/delete_tb',
		{id: id, table: table},
		function(res){
			console.log(res);
			el.closest('tr').addClass('bg-danger').fadeOut('fast', function(){

				mydataTb.destroy();
				$.when($(this).remove()).then( function(){

					mydataTb = $('.mydataTb').DataTable({
						"bPaginate": true,
						"bLengthChange": false,
						"bFilter": false,
						"bInfo": true
					});
				});

			});
		}

	);
};



var getCountryCalling = function(country_short, $form){
	$.ajax({
		  type: "POST",

		  url: base_url+"settings/show_country_code",

		  data: { country_short: country_short },

		  dataType: 'json',

		  success: function(data) {
			console.log(data);

			$form.find('.addon-shortcode').html('+'+data.calling_code);
			$form.find('[name="country_code"]').val(data.country_id);
			$form.find('[name="country_short"]').val(data.calling_code);
			$form.find('[name="country_code"]').selectpicker('refresh');

			//update button text to country
			var short = $('.select_country_tbl[data-id="'+data.country_id+'"]').data('short');
			short = (short !== null) ? short : 'Select Country Code';
			console.log(short);
			$form.find('.country_btn').html(short);



		  },
		  error: function(err){
			  console.log(err);
		  }
	});

};



//message file
function delete_the_file(file_id){
	thenum = file_id.replace( /^\D+/g, '');
	//alert(thenum);

	//delete the file in server and in table
	$.ajax({
	  type: "POST",

	  url: base_url+'formsubmits/delete_message_file',

	  data: { thenum: thenum},

	  success: function(data) {

			console.log(data);

	  }

	});

}//.message file



/*jQuery material design*/

// Ripple-effect animation
(function($) {
    $(".ripplelink").click(function(e){
        var rippler = $(this);

        // create .ink element if it doesn't exist
        if(rippler.find(".ink").length == 0) {
            rippler.append("<span class='ink'></span>");
        }

        var ink = rippler.find(".ink");

        // prevent quick double clicks
        ink.removeClass("animate");

        // set .ink diametr
        if(!ink.height() && !ink.width())
        {
            var d = Math.max(rippler.outerWidth(), rippler.outerHeight());
            ink.css({height: d, width: d});
        }

        // get click coordinates
        var x = e.pageX - rippler.offset().left - ink.width()/2;
        var y = e.pageY - rippler.offset().top - ink.height()/2;

        // set .ink position and add class .animate
        ink.css({
          top: y+'px',
          left:x+'px'
        }).addClass("animate");
    })
})(jQuery);
