'use strict';

(function() {

    var InboxApp = angular.module('InboxApp', AngularDefaultInjects.concat(['ui.router']));

    InboxApp.controller('InboxCtrl', ['$scope', 'FatModal', function($scope, FatModal) {

        var self = $scope.inboxctrl = this;

        self.openInbox = function(){
            FatModal({
                templateUrl: '/partial/inbox/partials/inbox.modal.tpl.html',
                controller: ['$scope', '$api', function($scope, $api) {

                    var self = $scope.inbox = this;

                    self.notifications = [];
                    self.unread = 0;
                    self.currentDate;

                    self.getInbox = function() {
                        $api.get('/api/v3/inbox/?page_size=200')
                            .then(function(res){
                                return res.data;
                            })
                            .then(function(res) {
                                if (!res || !res.data) {
                                    return;
                                }

                                self.unread = res.metadata.unread_count;

                                angular.forEach(res.data, function(notification) {
                                    var date = moment(notification.created_at);

                                    if(self.currentDate === undefined || date.isBefore(self.currentDate, 'day')){
                                        self.currentDate = date;

                                        self.notifications.push({
                                            divider: true,
                                            date: (self.currentDate.isSame(moment(), 'day')) ? 'Today' : self.currentDate.format('ll')
                                        });
                                    }

                                    if(date.isSame(moment(), 'day')){
                                        notification.created_at = date.fromNow();
                                    }else{
                                        notification.created_at = date.format('h:mm a');
                                    }

                                    self.notifications.push(notification);
                                });
                            });
                    };

                    self.getInbox();

                }]
            });
        };

    }]);

    var $inbox = $('<div id="inbox-app" ng-controller="InboxCtrl"></div>');

    $('body').append($inbox);

    angular.element('#inbox-app').ready(function () {
        angular.bootstrap('#inbox-app', ['InboxApp']);
    });

})();
