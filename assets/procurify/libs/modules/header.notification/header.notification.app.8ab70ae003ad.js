'use strict';

(function() {
	window.ProcurifyNotification = function(title, text) {
		Notification.requestPermission(function(permission) {
			if (permission !== 'granted') {
				return;
			}

			if (!title) {
				return;
			}

			var notification = new Notification(title, {
				icon: Global.config.STATIC_URL + 'public/images/favicon.ico',
				body: text
			});

			notification.onclick = function() {
				notification.close();
				window.focus();
			};
		});
	};
})();

(function() {

	var NotificationApp = angular.module('NotificationApp', AngularDefaultInjects.concat(['ui.router']));

	NotificationApp.controller('NotificationCtrl', ['$scope', 'FatModal', '$animate', function($scope, FatModal, $animate) {
	}]);

	NotificationApp.directive('notificationPopup', ['$templateCache', '$compile', 'FatModal', 'Pusher', 'ContentType', function($templateCache, $compile, FatModal, Pusher, ContentType) {
		return {
			restrict: 'A',
			link: function($scope, $element, $attrs) {

				var notificationTpl = $templateCache.get('/partial/core/partials/components/notifications-popup-tpl.html');

				var compiledTpl = $compile(notificationTpl)($scope);

				$element.popover({
					trigger: 'focus',
					container: $element,
					placement: 'bottom',
					title: 'Notifications',
					html: true,
					template: compiledTpl
				});
			},
			controller: ['$scope', '$rootScope', 'InboxAPI', '$filter', function($scope, $rootScope,InboxAPI, $filter) {

				var self = $scope.notification = this;

				$scope.ContentType = ContentType;

				// Controller
				self.notifications = [];
				self.isEmpty = true;
				self.unread_count = 0;

				// Fetech unread messages
				self.notificationsXHR = InboxAPI.getUnreadMessages()
					.then(function(res) {
						if (!res || !res.data.length) {
							self.checkEmpty();
							return;
						}

						self.unread_count = res.metadata.unread_count;

						angular.forEach(res.data, function(notification){
							ContentType.then(function(contentTypes){
								var model_label = $filter('ContentTypeLabel')(contentTypes[notification.message.content_type]);

								notification.context = '{1} #{2}'.assign(model_label, notification.message.object_pk);
							});
						});

						self.notifications = res.data;

						self.checkEmpty();
					});

				self.checkEmpty = function(){
					$rootScope.safeApply(function(){
						self.isEmpty = !self.notifications.length;
					});
				};

				// Pusher Connection

				Pusher.notification_channel.bind('inbox-message-received', function(notification) {
					$scope.$evalAsync(function() {
						self.unread_count += 1;


						ContentType.then(function(contentTypes){
							var model_label = $filter('ContentTypeLabel')(contentTypes[notification.message.content_type]);

							notification.context = '{1} #{2}'.assign(model_label, notification.message.object_pk);

							self.notifications.unshift(notification);

							self.checkEmpty();
						});

					});

					ProcurifyNotification('New Comment from {1} {2}'.assign(notification.message.userprofile.firstName, notification.message.userprofile.lastName), $('<p>' + notification.message.comment + '</p>').text());
				});

				self.openInbox = function() {
					FatModal({
						templateUrl: '/partial/inbox/partials/inbox.modal.tpl.html',
						controller: 'InboxCtrl',
						windowClass: 'InboxModal'
					});
				};

				self.markAsRead = function(notification, index){
					self.unread_count -= 1;

					self.notifications.splice(index, 1);

					InboxAPI.markAsRead(notification.id);
				};

				self.readComment = function(comment, index){
					InboxAPI.markAsRead(comment.id).then(function(res){
						self.notifications.splice(index, 1);

				    	window.location.replace(comment.message.user_url);
				    });
				};

				self.markAllAsRead = function(){
					InboxAPI.markAllAsRead().then(function(){
						self.unread_count = 0;
						self.notifications = [];
					});
				};
			}]
		};
	}]);

	NotificationApp.controller('InboxCtrl', ['$scope', '$api', '$filter', 'Pusher', 'ContentType', 'InboxAPI', function($scope, $api, $filter, Pusher, ContentType, InboxAPI) {

		var self = $scope.inbox = this;

		self.notifications = [];
		self.new_notifications = [];
		self.unread = 0;
		self.currentDate = null;

		self.page = 0;

		self.inboxXHR = null;
		self.isLoadingMore = null;
		self.haveMore = false;

		self.markAsRead = function(notification){
			notification.is_read = true;
			InboxAPI.markAsRead(notification.id)
				.catch(function(){
					notification.is_read = false;
				});
		};

		self.readComment = function(notification, index){
			notification.is_read = true;

			InboxAPI.markAsRead(notification.id).then(function(res){
		    	window.location.replace(notification.message.user_url);
		    });
		};

		self.markAllAsRead = function(){
			InboxAPI.markAllAsRead();

			self.unread = 0;

			angular.forEach(self.notifications, function(notification){
				notification.is_read = true;
			});

			angular.forEach(self.new_notifications, function(notification){
				notification.is_read = true;
			});
		};

		function formatNotification(notification) {
			var date = moment(notification.created_at);

			ContentType.then(function(contentTypes){
				var model_label = $filter('ContentTypeLabel')(contentTypes[notification.message.content_type]);

				notification.context = '{1} #{2}'.assign(model_label, notification.message.object_pk);
			});

			if (date.isSame(moment(), 'day')) {
				notification.created_at = date.fromNow();
			} else {
				notification.created_at = date.format('h:mm a');
			}
		}

		self.getInbox = function(page) {
			self.inboxXHR = $api.get('/api/v3/inbox/?page_size=20&page='+(page||1));

			if(page > 1){
				self.isLoadingMore = self.inboxXHR;
			}

			self.inboxXHR.then(function(res) {
					return res.data;
				})
				.then(function(res) {
					if (!res || !res.data) {
						return;
					}

					self.page = page || 1;

					self.unread = res.metadata.unread_count;

					// Have More?
					if(res.metadata.pagination.current_page < res.metadata.pagination.num_pages){
						self.haveMore = true;
					}else{
						self.haveMore = false;
					}

					angular.forEach(res.data, function(notification) {
						var date = moment(notification.created_at);

						if (!self.currentDate || date.isBefore(self.currentDate, 'day')) {
							self.currentDate = date;

							self.notifications.push({
								divider: true,
								date: (self.currentDate.isSame(moment(), 'day')) ? 'Today' : self.currentDate.format('ll')
							});
						}
						formatNotification(notification);

						self.notifications.push(notification);
					});
				});

		};

		self.loadMore = function(){
			if(self.inboxXHR.$$is_fetching || !self.haveMore){
				return;
			}

			self.getInbox(self.page + 1);
		};

		self.getInbox();

		/*------------------------------------*\
		    # PUSHER
		\*------------------------------------*/

		Pusher.notification_channel.bind('inbox-message-received', function(notification) {
			$scope.$evalAsync(function() {
				formatNotification(notification);
				self.new_notifications.unshift(notification);

				self.unread += 1;
			});
		});

	}]);

	angular.element('#header-notifification-app').ready(function() {
		angular.bootstrap('#header-notifification-app', ['NotificationApp']);
	});

})();
