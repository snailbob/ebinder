'use strict';

var Global = {};

jQuery(function($) {
    var Base = {
        init: function() {
            this.initCRSFHeader();
            this.responsiveBreakpoints();
            this.bindMobileMenu();
            this.bindNavigation();
        },

        initCRSFHeader: function() {
            $(document).ajaxSend(function(event, xhr, settings) {
                function getCookie(name) {
                    var cookieValue = null;
                    if (document.cookie && document.cookie !== '') {
                        var cookies = document.cookie.split(';');
                        for (var i = 0; i < cookies.length; i++) {
                            var cookie = jQuery.trim(cookies[i]);
                            // Does this cookie string begin with the name we want?
                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                break;
                            }
                        }
                    }
                    return cookieValue;
                }

                function sameOrigin(url) {
                    // url could be relative or scheme relative or absolute
                    var host = document.location.host; // host + port
                    var protocol = document.location.protocol;
                    var sr_origin = '//' + host;
                    var origin = protocol + sr_origin;
                    // Allow absolute or scheme relative URLs to same origin
                    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
                        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
                        // or any other URL that isn't scheme relative or absolute i.e relative.
                        !(/^(\/\/|http:|https:).*/.test(url));
                }

                function safeMethod(method) {
                    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
                }

                if (!safeMethod(settings.type) && sameOrigin(settings.url)) {
                    xhr.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
                }
            });
        },

        bindNavigation: function() {
            var sidebar = $('.l-nav-container');

            sidebar.find('.tab-locked').tooltip({
                title: '<i class="icon-lock"></i> This feature is limited',
                container: 'body',
                placement: 'right',
                html: true
            });

            sidebar.find('.nav-icon-tooltip[tooltip]').tooltip({
                container: 'body',
                placement: 'right',
                html: true
            });
        },

        bindMobileMenu: function() {
            $('.mobile-nav-btn').on('click', function() {
                $('body').toggleClass('mobile-nav-active');
            });
        },

        showSubMenu: function(e) {
            $(this).siblings('.nav-child').toggle();
        },

        responsiveBreakpoints: function() {

            var breakpoints = [{
                class: 'xlg-desktop',
                size: 1400
            }, {
                class: 'lg-desktop',
                size: 1200
            }, {
                class: 'desktop',
                size: 992
            }, {
                class: 'tablet',
                size: 640
            }, {
                class: 'mobile',
                size: 0
            }];

            var onResize = (function() {
                var activeClasses = [];
                var inactiveClasses = [];

                var onlyClass = null;

                $.each(breakpoints, function(index, breakpoint) {
                    if (breakpoint.size < window.innerWidth) {
                        if (!onlyClass) {
                            activeClasses.push(breakpoint.class + '-only');
                            onlyClass = breakpoint.class;
                        } else {
                            inactiveClasses.push(breakpoint.class + '-only');
                        }
                        activeClasses.push(breakpoint.class);
                    } else {
                        inactiveClasses.push(breakpoint.class);
                        inactiveClasses.push(breakpoint.class + '-only');
                    }
                });

                $('body').toggleClass(activeClasses.join(' '), true).toggleClass(inactiveClasses.join(' '), false);

            }).debounce(150);

            $(window).on('resize', onResize);

            onResize();
        }
    };

    Base.init();
});

$(function() {
    $('.help-btn-js').popover({
        trigger   : 'focus',
        placement : 'bottom',
        content   : '<div class="shortcut-popover__container">' +
            '<a id="procurify_tutorial" class="shortcut-popover__link" href="http://kb.procurify.com" target="_blank">Tutorial</a>' +
            '<br/>' +
            '<a class="shortcut-popover__link" href="http://kb.procurify.com/?article_category=set-up" target="_blank">How to get started?</a>' +
            '<hr/>' +
            '<a class="shortcut-popover__link" href="http://blog.procurify.com/" target="_blank">Procurify Blog</a>' +
            '<br/>' +
            '<a class="shortcut-popover__link refer-btn-js" href="javascript:;">Refer Procurify to a friend</a>' +
            '<br/>' +
            '<a class="shortcut-popover__link" href="http://procurify-features.uservoice.com/forums/328818-procurify-ideas" target="_blank">What should we add next?</a>' +
            '</div>',
        html: true,
        template: '<div class="popover shortcut-popover shortcut-popover--help" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    });

    $('.help-btn-js').on('shown.bs.popover', function() {
        $(this).addClass('active');
        $('#procurify_tutorial').attr('href', Global.knowledgebase_url);
    });

    $('.help-btn-js').on('hidden.bs.popover', function() {
        $(this).removeClass('active');
    });

    var $btnShortcut = $('.btn__shortcut-add');

    $btnShortcut.popover({
        trigger   : 'click',
        placement : 'bottom',
        title     : '<i class="icon-request"></i>Quick Request',
        content   : '<div class="shortcut-popover__container"><a class="shortcut-popover__link block" href="/request/">+ Order</a><a class="shortcut-popover__link block" href="/expense/">+ Expense</a><a class="shortcut-popover__link block" href="/travel/">+ Travel</a></div>',
        html      : true,
        template  : '<div class="popover shortcut-popover shortcut-popover--request" role="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    });

    var hidePopover = function() {
        $btnShortcut.popover('hide');

        if ($btnShortcut.data()['bs.popover']) {
            $btnShortcut.data()['bs.popover'].inState.click = false;
        }

        $(document).off('click', hideOnBodyClick);
    };

    var hideOnBodyClick = function(event) {
        $('[role="popover"]').each(function() {
            var $target = $(event.target);

            if (!$(this).is(event.target) &&
                $target.closest($(this)).length === 0 &&
                $target.closest($btnShortcut).length === 0) {
                hidePopover();
            }
        });
    };

    $btnShortcut.on('show.bs.popover', function() {
        $(document).off('click', hideOnBodyClick).on('click', hideOnBodyClick);
    });

});

(function() {
    $.ajax({
        method: 'GET',
        url: '/api/v2/user/me/notification/counts/',
        data: {
            pending_payment: 'True',
            pending_approval: 'True',
            procure: 'True',
            receive: 'True'
        }
    }).success(function(res) {

        if (res.data.pending_approval) {

            if (res.data.pending_approval.total) {
                $('.nav-badge-approval').text(res.data.pending_approval.total).show();
            }

            if (res.data.pending_approval.order) {
                $('.nav-badge-approval-order').text(res.data.pending_approval.order).show();
            }
            if (res.data.pending_approval.travel) {
                $('.nav-badge-approval-travel').text(res.data.pending_approval.travel).show();
            }
            if (res.data.pending_approval.expense) {
                $('.nav-badge-approval-expense').text(res.data.pending_approval.expense).show();
            }
            if (res.data.pending_approval.bill) {
                $('.nav-badge-approval-bill').text(res.data.pending_approval.bill).show();
            }
            if (res.data.pending_approval.payment) {
                $('.nav-badge-approval-payment').text(res.data.pending_approval.payment).show();
            }
        }

        if (res.data.pending_payment) {
            if (res.data.pending_payment.total) {
                $('.nav-badge-payment').text(res.data.pending_payment.total).show();
            }
            if (res.data.pending_payment.expense) {
                $('.nav-badge-payment-expense-records').text(res.data.pending_payment.expense).show();
            }
            if (res.data.pending_payment.po) {
                $('.nav-badge-payment-payment-records').text(res.data.pending_payment.po).show();
            }
        }

        if (res.data.pending_procure) {
            $('.nav-badge-procure').text(res.data.pending_procure).show();
        }
        if (res.data.pending_procure) {
            $('.nav-badge-procure-procurement').text(res.data.pending_procure).show();
        }
        if (res.data.pending_receive) {
            $('.nav-badge-receive').text(res.data.pending_receive).show();
        }
    });

})();



(function() {

    var breakpoints = [{
        class: 'lg-desktop',
        size: 1200
    }, {
        class: 'desktop',
        size: 992
    }, {
        class: 'tablet',
        size: 640
    }, {
        class: 'mobile',
        size: 0
    }];

    var activeClasses = [];
    var inactiveClasses = [];

    var onlyClass = null;

    $.each(breakpoints, function(index, breakpoint) {
        if (breakpoint.size < window.innerWidth) {
            if (!onlyClass) {
                activeClasses.push(breakpoint.class + '-only');
                onlyClass = breakpoint.class;
            } else {
                inactiveClasses.push(breakpoint.class + '-only');
            }
            activeClasses.push(breakpoint.class);
        } else {
            inactiveClasses.push(breakpoint.class);
            inactiveClasses.push(breakpoint.class + '-only');
        }
    });


    $('body').toggleClass(activeClasses.join(' '), true).toggleClass(inactiveClasses.join(' '), false);

})();

$(function(){
    var scrolling = false;
    var sidebar = $('.l-nav-container');
    var mainContent = $('.l-app-container');

    function checkScrollbarPosition() {

        var sidebarHeight = sidebar.outerHeight(),
            windowHeight = $(window).height(),
            mainContentHeight = mainContent.outerHeight(),
            scrollTop = $(window).scrollTop();

        ( ( scrollTop + windowHeight > sidebarHeight ) && ( mainContentHeight - sidebarHeight !== 0 ) ) ? sidebar.addClass('is-fixed').css('bottom', 0) : sidebar.removeClass('is-fixed').attr('style', '');

        scrolling = false;
    }

    $(window).on('scroll', function(){
        if( !scrolling ) {
            (!window.requestAnimationFrame) ? setTimeout(checkScrollbarPosition, 300) : window.requestAnimationFrame(checkScrollbarPosition);
            scrolling = true;
        }
    });

    checkScrollbarPosition();
});

/*------------------------------------*\
    # Set up bindings for emma switch
\*------------------------------------*/
(function() {
    $(document).ready(function() {
        $('.btn-switch-js').on('click', function(e) {
            $('#onboarding-version-switch').modal({backdrop: true});
        });

        $('.btn--request-switch').on('click', function(){
            var switchRequest = $.ajax({
                method: 'GET',
                url: '/api/v2/internal/new-version-request/'
            });
            switchRequest.then(function(res){
                $('#onboarding-version-switch').modal('hide');
                $('body').removeClass('has-overlay');
                $('#onboarding-version-switch-success').modal({backdrop: true});
                // $('.')
            });
        });
    });
})();

/*------------------------------------*\
    # Addition info in nav
\*------------------------------------*/

(function() {
    var $btnShortcut = $('.nav-more-js');

    $btnShortcut.popover({
        container: 'body',
        placement: 'right',
        content: '<div class="nav-more-popover"> ' +
        '<a href="http://goo.gl/Nx797s" target="_blank">Change log</a>' +
        '<a href="https://goo.gl/pwVECK" target="_blank">iPhone</a>' +
        '<a href="https://goo.gl/fOv0Ng" target="_blank">Android</a>' +
        '<br/>' +
        '<a href="javascript:;" class="refer-btn-js">Refer a Friend</a>' +
        '<br/>' +
        '<a href="http://procurify-features.uservoice.com/forums/328818-procurify-ideas" target="_blank">What should we add next?</a>' +
        '</div>',
        html: true,
        trigger: 'click',
        template: '<div class="popover popover-close-js" role="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    });

    var hidePopover = function() {
        $btnShortcut.popover('hide');

        if ($btnShortcut.data()['bs.popover']) {
            $btnShortcut.data()['bs.popover'].inState.click = false;
        }

        $(document).off('click', hideOnBodyClick);
    };

    var hideOnBodyClick = function(event) {
        $('[role="popover"]').each(function() {
            var $target = $(event.target);

            if (!$(this).is(event.target) &&
                $target.closest($(this)).length === 0 &&
                $target.closest($btnShortcut).length === 0) {
                hidePopover();
            }
        });
    };

    $btnShortcut.on('show.bs.popover', function() {
        $(document).off('click', hideOnBodyClick).on('click', hideOnBodyClick);
    });
})();

/*------------------------------------*\
    # DOMAIN SWITCHING
\*------------------------------------*/

$(function(){

    var subdomain = Global.domain.split('.procurify.')[0];

    var $domainswitch = $('<div class="header-section header-domain-switch">\
        <i class="header-section-icon fa fa-home fa-fw mr5"></i>\
        <select class="header-domain-switch-js header-domain-switch-select"></select>\
    </div>');

    $domainswitch.find('select').on('change', function(event){
        window.location.replace(location.origin.replace(subdomain, $(this).val()) );
    });

    $.get('/api/v3/admin/domain-switching/')
        .success(function(res){
            if(!res || !res.data || !res.data.active || !res.data.procurify_domains || !res.data.procurify_domains.length){
                return;
            }

            $domainswitch.insertAfter('.btn--help.btn--header').css('display', 'inline-block');

            var optionsHTML = '';

            $.each(res.data.procurify_domains, function(index, domain){
                optionsHTML += '<option value="{2}">{1}</option>'.assign(domain.name, domain.subdomain);
            });

            $('.header-domain-switch-js').html(optionsHTML).val(subdomain);
        });
});
