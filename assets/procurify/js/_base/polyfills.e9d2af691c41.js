//first, checks if it isn't implemented yet
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) {
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}

if (!Object.get){
    var Getter = function(object){
        var Get = function(key){
            var obj = object || {};
            return Getter(obj[key] || null)
        };

        Get.value = object;

        return Get;
    };

    Object.get = Getter;
}
