<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Formsubmits extends CI_Controller {

	public function __construct(){
		parent::__construct();
		header('Access-Control-Allow-Origin: *');
		//header('Content-Type: application/json');

	}

	public function index(){
		echo 'test';
	}
	/***************************
		login
	***************************/
	public function login(){

		//google_id=&facebook_id=&email=%40gmail.com&password=Darknite1
		$google_id = ''; //$_POST['google_id'];
		$facebook_id = ''; //$_POST['facebook_id'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$utype = $_POST['utype'];
		$domain_name = (isset($_POST['domain_name'])) ? $_POST['domain_name'] : '' ;


//		$usertype = 'customer';
//
//		if(isset($_POST['usertype'])){
//			$usertype = $_POST['usertype'];
//		}
		$matching_agency = $this->master->getRecords('agency', array('domain'=>$domain_name));
		$agency = $this->master->getRecords('agency', array('email'=>$email));

		//filter if subdomain
		if(count($matching_agency) > 0 && !empty($domain_name)){
			$customer = $this->master->getRecords('customers', array('email'=>$email, 'agency_id'=>$matching_agency[0]['id']));
		}
		else{
			$customer = $this->master->getRecords('customers', array('email'=>$email));
		}
		// $student = $this->master->getRecords('students', array('email'=>$email));

		$arr = array(
			'result'=>'not_exist',
			'google_id'=>$google_id,
			'fb_id'=>$facebook_id,
			'email'=>$email
		);


		//get validation message from admin
		$action_message = $this->common->get_message('user_confirm_auth');

		$arr['message'] = $action_message['error'];



		//default activity log
		$log_activity = array(
			'name'=>$email.' User not found',
			'type'=>'login',
			'details'=>serialize($arr)
		);




		$count = 1;
		if(count($customer) > 0){
			$arr['result'] = 'error';
			$arr['message'] = '<i class="fa fa-warning text-warning"></i> Wrong email or password.';


			foreach($customer as $r=>$value){
				$active_agency = $this->master->getRecords('agency', array('id'=>$value['agency_id'], 'enabled'=>'Y'));
				if(empty($active_agency)){
					$arr['result'] = 'error';
					$arr['message'] = '<i class="fa fa-warning text-warning"></i> Agency deactivated/not found.';
				}
				else if($value['password'] == md5($password)){
//					unset($value['password']);
//					$r_add = $_SERVER['REMOTE_ADDR'];
//					$value['ip'] = $r_add;
//
//					$value['name'] = $value['first_name'].' '.$value['last_name'];
//					$value['type'] = 'customer';
//					$value['address'] = $value['location'];
//					$value['info_format'] = (@unserialize($value['customer_info'])) ? unserialize($value['customer_info']) : array();
					$the_customer = $this->common->customer_format($value['id']);


					$count_domain_match = 0;
					$value['multiple_account'] = 'no';
					//include agency to switch

					$user_agencies = $the_customer['my_agencies'];
					if(count($user_agencies) > 1){
						$value['multiple_account'] = 'yes';

						if(!empty($domain_name)){
							foreach($user_agencies as $agg=>$aggv){
								if($aggv['domain'] == $domain_name){
									//detect user as single in a domain login
									$value['multiple_account'] = 'no';
									$whr_cc = array('agency_id'=>$aggv['id'], 'email'=>$email);
									$the_customer = $this->common->customer_format(NULL, $whr_cc);
									if(!empty($the_customer)){
										$count_domain_match++;
									}
								}
							}

						}
					}

					//set user to array
					$arr['userdata'] = $the_customer;

					//filter matching user logging in a domain
					if($count_domain_match == 0 && count($user_agencies) > 1 && !empty($domain_name)){
						$arr['result'] = 'error';
						$arr['message'] = '<i class="fa fa-warning text-warning"></i> Account not belong to this domain.';
					}

					else if($the_customer['domain'] != $domain_name && !empty($domain_name)){
						$arr['result'] = 'error';
						$arr['message'] = '<i class="fa fa-warning text-warning"></i> Account not belong to this domain.';
					}


					//user is active
					else if($the_customer['enabled'] != 'N'){
						$this->session->set_userdata($the_customer);


						$arr['result'] = 'ok';

						$arr['message'] = $action_message['success'];

						$user_log = array(
							'broker_id'=>$the_customer['id'],
							'first_broker'=>$the_customer['first_broker'],
							'super_admin'=>$the_customer['super_admin'],
							'brokerage_id'=>$the_customer['brokerage_id']
						);
						$this->master->insertRecord('biz_user_log', $user_log);


						$log_activity = array(
							'name'=>$the_customer['name'].' Login successful',
							'type'=>'login',
							'details'=>serialize($the_customer)
						);


					}
					else{
						$arr['result'] = 'error';
						$arr['message'] = '<i class="fa fa-warning text-warning"></i> Account not activated.';


						$log_activity = array(
							'name'=>$the_customer['name'].' login with account not activated',
							'type'=>'login',
							'details'=>serialize($the_customer)
						);


					}

				}
				else{
					if($count == 1){
						//send email to user if login failed
						$arr['send_login_error'] = $this->common->failed_login_noti($email);

						//check social if has
						$customer_li = $this->master->getRecordCount('customers', array('email'=>$email, 'fb_id !='=>''));
						$customer_gp = $this->master->getRecordCount('customers', array('email'=>$email, 'google_id !='=>''));
						$social_total = $customer_li + $customer_gp;
						$arr['social_total'] = $social_total;

						if($social_total > 0){
							$arr['result'] = 'error';
							$arr['disable_forgot_pass'] = 'yes';
							$arr['message'] = '<i class="fa fa-warning text-warning"></i> Oops, looks like you opted for single sign-on with either Google+ or LinkedIn.';
						}

						$log_activity = array(
							'name'=>$email.' Wrong password',
							'type'=>'login',
							'details'=>serialize($arr)
						);

					}
				}
				$count++;
			}
		}

		$this->common->insert_activity_log($log_activity);


		echo json_encode($arr);

	}

	/***************************
		signup
	***************************/
	public function signup(){

		//google_id=&facebook_id=&name=s&email=%40gmail.com&password=Darknite1
		$google_id = $_POST['google_id'];
		$facebook_id = $_POST['facebook_id'];
		$name = $_POST['name'];
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$company = $_POST['company'];
		$domain = (isset($_POST['domain'])) ? $_POST['domain'] : '';
		$email = $_POST['email'];
		$password = (isset($_POST['password'])) ? $_POST['password'] : '';
		$utype = $_POST['utype'];
		$calling_digits = $_POST['phone'];
		$social = $this->session->userdata('social');

		//check where_from
		$refer_from = isset($_POST['refer_from']) ? $_POST['refer_from'] : '';
		$append_getmethod = '';
		if($refer_from != ''){
			$append_getmethod = '?refer_from=buy_form';
		}

		$nowtime = date('Y-m-d H:i:s');
		$id = '';

		$google_img = $_POST['google_img'];
		$fb_img = $_POST['fb_img'];
		$social_type = $_POST['social_type'];
		$social_site = $_POST['social_site'];

		$gdata = '';
		$linkeddata = '';
		if(isset($social['network'])){
			if($social['network'] == 'google'){
				$google_id = (isset($social['id'])) ? $social['id'] : '';
				$google_img = (isset($social['avatar'])) ? $social['avatar'] : '';
				$gdata = serialize($social);
			}
			else{
				$facebook_id = (isset($social['id'])) ? $social['id'] : '';
				$fb_img = (isset($social['avatar'])) ? $social['avatar'] : '';
				$linkeddata = serialize($social);
			}
		}

		$result = 'error';
		$arr = array(
			'google_id'=>$google_id,
			'fb_id'=>$facebook_id,
			'google_img'=>$google_img,
			'google_data'=>$gdata,
			'fb_id'=>$facebook_id,
			'fb_img'=>$fb_img,
			'fb_data'=>$linkeddata,
			'password'=>md5($password),
			'email'=>$email,
			'status'=>'N',
			'enabled'=>'N',
			'calling_digits'=>$calling_digits,
			'date_added'=>$nowtime
		);



		$dbtable = 'customers';
		$emailer_file_name = 'signup-mail-to-broker';
		$email_name = $first_name.' '.$last_name;

		//store name for student or customer
		$arr['first_name'] = $first_name;
		$arr['last_name'] = $last_name;
		$arr['domain'] = $domain;
		$arr['business_name'] = $company;
		$arr['customer_type'] = 'N';

		$id = $this->master->insertRecord($dbtable, $arr, true);


		//logs
		$log_activity = array(
			'name'=>$first_name.' new user signup successful',
			'type'=>'signup',
			'details'=>serialize($arr)
		);
		$this->common->insert_activity_log($log_activity);


		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings
		$info_arr = array(
			'to'=>$email,
			'from'=>$adminemail,
			'subject'=>'Greetings from Ebinder',
			'view'=>$emailer_file_name,
		);


		$other_info = array(
			'password'=>'',
			'emailer_file_name'=>$emailer_file_name,
			'name'=>$email_name,
			'agency_name'=>$name,
			'user_name'=>$email,
			'user_email'=>$email,
			'link'=>'landing/verify_user/'.$id.'/'.md5($id).'/'.$dbtable.'/broker'.$append_getmethod
		);

		$this->emailer->sendmail($info_arr, $other_info);

		//get validation message from admin
		$action_message = $this->common->get_message('user_signup');

		$arr['other_info'] = $other_info;
		$arr['info_arr'] = $info_arr;
		$arr['message'] = $action_message['success'];
		$arr['signup_success_view'] = $this->load->view('front/signup_success_view', '', true);
		$result = 'ok';

		$arr['result'] = $result;
		echo json_encode($arr);
	}





	/***************************
		linkedin
	***************************/
	public function linkedin(){
		$id = $_POST['id'];
		$data = $_POST['data'];
		$uri_1 = $_POST['uri_1'];
		$domain_name = (isset($_POST['domain_name'])) ? $_POST['domain_name'] : '';

		// $user = $this->master->getRecords('customers', array('customer_type'=>'N', 'enabled'=>'Y', 'fb_id'=>$data['id']));

		$matching_agency = $this->master->getRecords('agency', array('domain'=>$domain_name));

		//filter if subdomain
		if(count($matching_agency) > 0 && !empty($domain_name)){
			$user = $this->master->getRecords('customers', array('customer_type'=>'N', 'enabled'=>'Y', 'fb_id'=>$data['id'], 'agency_id'=>$matching_agency[0]['id']));
		}
		else{
			$user = $this->master->getRecords('customers', array('customer_type'=>'N', 'enabled'=>'Y', 'fb_id'=>$data['id']));
		}

		$data['type'] = 'signup';
		$data['network'] = 'linkedin';

		if(!empty($id)){
			$facebook_id = (isset($data['id'])) ? $data['id'] : '';
			$fb_img = (isset($data['avatar'])) ? $data['avatar'] : '';
			$linkeddata = serialize($data);

			$arr = array(
				'fb_id'=>$facebook_id,
				'fb_img'=>$fb_img,
				'fb_data'=>$linkeddata,
				'status'=>'Y',
				'enabled'=>'Y',
			);

			$this->master->updateRecord('customers', $arr, array('id'=>$id));
			$the_customer = $this->common->customer_format($id);
			$data['userdata'] = $the_customer;
			$data['type'] = ($uri_1 == 'settings') ? 'connect' : 'complete_signup';
			$this->session->set_userdata($the_customer);
		}

		else if(count($user) > 0){

			$the_customer = $this->common->customer_format($user[0]['id']);

			if(count($the_customer['my_agencies']) > 0 || $the_customer['super_admin'] == 'Y'){
				$data['userdata'] = $the_customer;
				$data['type'] = 'login';
				$this->session->set_userdata($the_customer);
			}

		}

		else{
			$data['type'] = 'signup';
			$data['network'] = 'linkedin';
			// $this->session->set_userdata('social', $data);
		}

		echo json_encode($data);
	}

	/***************************
		googleplus
	***************************/
	public function googleplus(){
		$id = $_POST['id'];
		$data = $_POST['data'];
		$uri_1 = $_POST['uri_1'];
		$domain_name = (isset($_POST['domain_name'])) ? $_POST['domain_name'] : '';

		// $user = $this->master->getRecords('customers', array('customer_type'=>'N', 'enabled'=>'Y', 'google_id'=>$data['id']));

		$matching_agency = $this->master->getRecords('agency', array('domain'=>$domain_name));

		//filter if subdomain
		if(count($matching_agency) > 0 && !empty($domain_name)){
			$user = $this->master->getRecords('customers', array('customer_type'=>'N', 'enabled'=>'Y', 'google_id'=>$data['id'], 'agency_id'=>$matching_agency[0]['id']));
		}
		else{
			$user = $this->master->getRecords('customers', array('customer_type'=>'N', 'enabled'=>'Y', 'google_id'=>$data['id']));
		}


		$data['type'] = 'signup';
		$data['network'] = 'google';


		if(!empty($id)){
			$google_id = (isset($data['id'])) ? $data['id'] : '';
			$google_img = (isset($data['avatar'])) ? $data['avatar'] : '';
			$gdata = serialize($data);

			$arr = array(
				'google_id'=>$google_id,
				'google_img'=>$google_img,
				'google_data'=>$gdata,
				'status'=>'Y',
				'enabled'=>'Y',
			);

			$this->master->updateRecord('customers', $arr, array('id'=>$id));
			$the_customer = $this->common->customer_format($id);
			$data['userdata'] = $the_customer;
			$data['type'] = ($uri_1 == 'settings') ? 'connect' : 'complete_signup';
			// $this->session->set_userdata($the_customer);
		}
		else if(count($user) > 0){
			$the_customer = $this->common->customer_format($user[0]['id']);

			if(count($the_customer['my_agencies']) > 0 || $the_customer['super_admin'] == 'Y'){
				$data['userdata'] = $the_customer;
				$data['type'] = 'login';
				$this->session->set_userdata($the_customer);
			}

		}
		else{
			$data['type'] = 'signup';
			$data['network'] = 'google';

			// $this->session->set_userdata('social', $data);
		}
		echo json_encode($data);
	}

	/***************************
		validate_sms_code
	***************************/
	public function validate_sms_code(){
		$id = (isset($_POST['id'])) ? $_POST['id'] : '';
		$code = $_POST['code'];

		$arr['result'] = 'unmatch';
		$arr['code'] = $code;
		$arr['id'] = $id;

		$thecode = '';
		foreach($code as $r=>$value){
			$thecode .= $value;
		}
		$arr['thecode'] = $thecode;


		if(!empty($id)){


			$dbtable = 'customers';
			$the_user = $this->master->getRecords($dbtable, array('id'=>$id));
			$user_code  = $the_user[0]['sms_code'];


			if($user_code == $thecode){
				$this->master->updateRecord($dbtable, array('sms_verified'=>'Y'), array('id'=>$id));
				$arr['result'] = 'match';
			}

		}
		else{
			$user_code = $this->session->userdata('customer_sms_code');
			if($user_code == $thecode){
				$arr['result'] = 'match';
				$this->session->set_userdata('customer_mobile_ok', 'ok');
			}

		}

		echo json_encode($arr);
	}


	/***************************
		activate_domain
	***************************/
	public function activate_domain(){
		$id = $_POST['id'];
		$agency_id = $_POST['agency_id'];
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$sms_code = (isset($_POST['sms_code'])) ? $_POST['sms_code'] : '';
		$company = (isset($_POST['company'])) ? $_POST['company'] : '';
		$domain = (isset($_POST['domain'])) ? $_POST['domain'] : '';
		$password = (isset($_POST['password'])) ? $_POST['password'] : '';
		$calling_digits = (isset($_POST['phone'])) ? $_POST['phone'] : '';
		$type = (isset($_POST['type'])) ? $_POST['type'] : 'broker';

		$arr = array(
			'password'=>md5($password),
		//	'calling_digits'=>$calling_digits,
		);



		$dbtable = 'customers';

		//store name for student or customer
		$arr['first_name'] = $first_name;
		$arr['last_name'] = $last_name;
		$arr['domain'] = $domain;

		if(!empty($company)){
			$arr['business_name'] = $company;

		}



		$arr['agency_id'] = $agency_id;

		$the_user = $this->master->getRecords($dbtable, array('id'=>$id));
		$user_code  = $the_user[0]['sms_code'];


		if($user_code == $sms_code){

			if($agency_id == '0'){
				$a_data = array(
					'name'=>$company,
					'domain'=>$domain,
					'contact_no'=>$calling_digits
				);
				$agency_id = $this->common->clone_agency($a_data);
				$arr['super_admin'] = 'Y';
			}
			$arr['sms_verified'] = 'Y';
			$arr['enabled'] = 'Y';

			$this->master->updateRecord($dbtable, $arr, array('id'=>$id));

			//get validation message from admin
			$action_message = $this->common->get_message('user_signup');

			$the_customer = $this->common->customer_format($id);
			$this->session->set_userdata($the_customer);


			$arr['the_customer'] = $the_customer;
			$arr['message'] = $action_message['success'];
			$result = 'ok';
		}

		else{
			$arr['message'] = 'SMS code didn\'t match. Please try again.';
			$result = 'error';
		}

		$arr['type'] = $type;
		$arr['result'] = $result;
		echo json_encode($arr);
	}


	/***************************
		update_student
	***************************/
	public function update_student(){
		$inst_first_name = $_POST['inst_first_name'];
		$inst_last_name = $_POST['inst_last_name'];
		$inst_email = $_POST['inst_email'];
		$inst_id = $_POST['inst_id'];


		$arr = array(
			'first_name'=>$inst_first_name,
			'last_name'=>$inst_last_name,
			'email'=>$inst_email,
		);
		$this->master->updateRecord('students', $arr, array('id'=>$inst_id));

		$this->session->set_userdata($arr);

		//get validation message from admin
		$action_message = $this->common->get_message('generic_update_message');

		$arr['message'] = $action_message['success'];
		$result = 'ok';

		$arr['result'] = $result;
		echo json_encode($arr);
	}


	/***************************
		update_customer admin
	***************************/
	public function update_customer_admin(){
		$id = $_POST['id'];
		$email = $_POST['email'];
		$first = $_POST['first'];
		$last = $_POST['last'];
		$domain = $_POST['domain'];


		$country_id = $_POST['country_code'];
		$country_short = $_POST['country_short'];

		$mobile = $_POST['mobile'];
		$phone_trim = ltrim($mobile, '0');
		$contact_no = $country_short.$phone_trim;


		$data = array(
			'first_name'=>$first,
			'last_name'=>$last,
			'email'=>$email,
			'country_id'=>$country_id,
			'calling_code'=>$country_short,
			'calling_digits'=>$mobile,
			'contact_no'=>$contact_no,
			'domain'=>$domain,
		);

		$this->master->updateRecord('customers', $data, array('id'=>$id));


		$user = $this->common->customer_format($id);


		$this->session->set_userdata($user);

		$data['message'] = ' Details successfully updated.';

		$result = 'ok';

		$data['result'] = $result;
		echo json_encode($data);

	}

	/***************************
		update_customer
	***************************/
	public function update_customer(){

	//inst_first_name=sd&inst_last_name=ss&inst_email=inst%40inst.com&location=Davao+City%2C+Davao+Region%2C+Philippines&lat=7.190708&long=125.45534099999998&inst_id=5&country_code=174&mobile=09108096447&country_short=PH&about=adfadf&calling_code=63

		$inst_first_name = $_POST['inst_first_name'];
		$inst_last_name = $_POST['inst_last_name'];
		$inst_email = $_POST['inst_email'];
		$location = $_POST['address'];
		$business_name = $_POST['business_name'];
		$lat = $_POST['lat'];
		$long = $_POST['long'];
		$inst_id = $_POST['inst_id'];
		$country_id = $_POST['country_code'];
		$country_short = $_POST['country_short'];
		$about = $_POST['about'];
		$country_short = $_POST['country_short'];
		$calling_code = $_POST['calling_code'];

		$mobile = $_POST['mobile'];
		$phone_trim = ltrim($mobile, '0');
		$contact_no = $calling_code.$phone_trim;


		$arr = array(
			'first_name'=>$inst_first_name,
			'last_name'=>$inst_last_name,
			'email'=>$inst_email,
			'location'=>$location,
			'business_name'=>$business_name,
			'lat'=>$lat,
			'lng'=>$long,
			'country_id'=>$country_id,
			'calling_code'=>$calling_code,
			'calling_digits'=>$mobile,
			'contact_no'=>$contact_no,
			'about'=>$about
		);


		$this->master->updateRecord('customers', $arr, array('id'=>$inst_id));


		$arr['address'] = $location;
		$this->session->set_userdata($arr);

		//get validation message from admin
		$action_message = $this->common->get_message('generic_update_message');

		$arr['message'] = $action_message['success'];
		$result = 'ok';

		$arr['result'] = $result;
		echo json_encode($arr);

	}



	/***************************
		customer_genre
	***************************/
	public function customer_genre(){
		$id = $_POST['id'];
		$genres = array();
		if(isset($_POST['genres'])){
			$genres = $_POST['genres'];
		}

		$arr = array(
			'dance_genres'=>serialize($genres)
		);

		$this->master->updateRecord('customers', $arr, array('id'=>$id));

		//get validation message from admin
		$action_message = $this->common->get_message('generic_update_message');

		$arr['message'] = $action_message['success'];
		$result = 'ok';

		$this->session->set_flashdata($result, $arr['message']);
		$arr['result'] = $result;
		echo json_encode($arr);

	}

	/***************************
		update_agency
	***************************/
	public function update_agency(){
		//agency_name=w&agency_email=%40gmail.com&address=Davao+City%2C+Davao+Region%2C+Philippines&lat=7.190708&long=125.45534099999998&agency_id=2&country_code=174&mobile=3245325&country_short=PH&background=345435&facilities=2332%0D%0A

		$agency_name = $_POST['agency_name'];
		$agency_email = $_POST['agency_email'];
		$address = $_POST['address'];
		$lat = $_POST['lat'];
		$long = $_POST['long'];
		$agency_id = $_POST['agency_id'];
		$country_id = $_POST['country_code'];
		$country_short = $_POST['country_short'];
		$website = $_POST['website'];
		$minimum_premium = $_POST['minimum_premium'];
		$background = $_POST['background'];
		$facilities = $_POST['facilities'];
		$calling_code = $_POST['calling_code'];

		$mobile = $_POST['mobile'];
		$phone_trim = ltrim($mobile, '0');
		$contact_no = $calling_code.$phone_trim;


		$result = 'error';

		$arr = array(
			'name'=>$agency_name,
			'email'=>$agency_email,
			'address'=>$address,
			'lat'=>$lat,
			'lng'=>$long,
			'country_id'=>$country_id,
			'calling_code'=>$calling_code,
			'calling_digits'=>$mobile,
			'contact_no'=>$contact_no,
			'background'=>$background,
			'facilities'=>$facilities,
			'minimum_premium'=>$minimum_premium,
			'website'=>$website
		);


		$this->master->updateRecord('agency', $arr, array('id'=>$agency_id));

		$this->session->set_userdata($arr);

		//get validation message from admin
		$action_message = $this->common->get_message('generic_update_message');

		$arr['message'] = $action_message['success'];
		$result = 'ok';

		$arr['result'] = $result;
		echo json_encode($arr);
	}

	/***************************
		agency_update_password
	***************************/
	public function agency_update_password(){
		$id = $_POST['id'];
		$pw = $_POST['pw'];

		$arr = array(
			'password'=>md5($pw),
			'status'=>'Y'
		);

		$this->master->updateRecord('agency', $arr, array('id'=>$id));

		//get validation message from admin
		$action_message = $this->common->get_message('save_password');

		$arr['message'] = $action_message['success'];
		$result = 'ok';
		$arr['result'] = $result;
		echo json_encode($arr);
	}


	/***************************
		customer_update_password
	***************************/
	public function customer_update_password(){
		$id = $_POST['id'];
		$pw = $_POST['pww'];

		$arr = array(
			'password'=>md5($pw),
			'status'=>'Y'
		);

		$this->master->updateRecord('customers', $arr, array('id'=>$id));

		//get validation message from admin
		$action_message = $this->common->get_message('save_password');

		$arr['message'] = $action_message['success'];
		$result = 'ok';
		$arr['result'] = $result;
		echo json_encode($arr);
	}

	/***************************
		update_password
	***************************/
	public function update_password(){
		$id = $_POST['id'];
		$type = 'agency';
		$pw = $_POST['pw'];

		$arr = array(
			'password'=>md5($pw)
		);

		if(isset($_POST['type'])){
			$type = $_POST['type'];
			$this->master->updateRecord('customers', $arr, array('id'=>$id));
		}
		else{
			$this->master->updateRecord('agency', $arr, array('id'=>$id));
		}



		//get validation message from admin
		$action_message = $this->common->get_message('save_password');

		$arr['message'] = $action_message['success'];
		$result = 'ok';
		$arr['result'] = $result;

		echo json_encode($arr);
	}




	/***************************
		customers
	***************************/
	public function resend_customer(){
		$id = $_POST['id'];
		$customer = $this->master->getRecords('customers', array('id'=>$id));
		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings
		$info_arr=array(
			'to'=>$customer[0]['email'],
			'from'=>$adminemail,
			'subject'=>'Greetings from Ebinder',
			'view'=>'registration-mail-to-customer',
			'emailer_file_name'=>'registration-mail-to-customer',
		);


		$other_info = array(
			'password'=>'',
			'view'=>'registration-mail-to-customer',
			'emailer_file_name'=>'registration-mail-to-customer',
			'name'=>$this->common->customer_name($customer[0]['id']),
			'user_name'=>$customer[0]['email'],
			'user_email'=>$customer[0]['email'],
			'link'=>'settings/verify/customers/'.$id.'/'.md5($id)
		);

		$this->emailer->sendmail($info_arr,$other_info);


		//get validation message from admin
		$action_message = $this->common->get_message('resend_confirmation');

		$arr['other_info'] = $other_info;
		$arr['info_arr'] = $info_arr;
		$arr['message'] = $action_message['success'];
		$result = 'ok';

		echo json_encode($arr);
	}

	/***************************
		customers
	***************************/
	public function customers(){
		//first=sdf&id=&last=sdf&id=&email=ddsfsdfsdsdf%40ads.com&id=
		$user_id = $this->session->userdata('id');
		$first = $_POST['first'];
		$last = $_POST['last'];
		$email = $_POST['email'];
		$id = $_POST['id'];
		$nowtime = date('Y-m-d H:i:s');

		$result = 'error';

		$arr = array(
			'first_name'=>$first,
			'last_name'=>$last,
			'email'=>$email
		);

		//save update
		if($id != ''){
			$this->master->updateRecord('customers', $arr, array('id'=>$id));

			//get validation message from admin
			$action_message = $this->common->get_message('generic_update_message');

			$arr['message'] = $action_message['success'];
			$result = 'ok';

		}
		else{
			$arr['date_added'] = $nowtime;
			$arr['status'] = 'N';
			$arr['agency_id'] = $user_id;
			$id = $this->master->insertRecord('customers', $arr, true);

			//admin emailer info
			$adminemail = $this->common->admin_email();

			//email settings
			$info_arr=array(
				'to'=>$email,
				'from'=>$adminemail,
				'subject'=>'Greetings from Ebinder',
				'view'=>'registration-mail-to-customer',
				'emailer_file_name'=>'registration-mail-to-customer',
			);


			$other_info = array(
				'password'=>'',
				'view'=>'registration-mail-to-customer',
				'emailer_file_name'=>'registration-mail-to-customer',
				'name'=>$first.' '.$last,
				'user_name'=>$email,
				'user_email'=>$email,
				'link'=>'settings/verify/customers/'.$id.'/'.md5($id)
			);

			$this->emailer->sendmail($info_arr,$other_info);


			//get validation message from admin
			$action_message = $this->common->get_message('generic_add_message');

			$arr['other_info'] = $other_info;
			$arr['info_arr'] = $info_arr;
			$arr['message'] = $action_message['success'];
			$result = 'ok';

		}

		$this->session->set_flashdata($result, $arr['message']);

		$arr['result'] = $result;
		echo json_encode($arr);


	}


	/***************************
		forgotpass
	***************************/
 	public function forgotpass(){
		$email = $_POST['email'];

		$user = $this->master->getRecords('agency',array('email'=>$email));
		$type = 'agency';
		if(count($user) == 0){
			$user = $this->master->getRecords('customers',array('email'=>$email));
			$type = 'customers';
		}

		//get validation message from admin
		$action_message = $this->common->get_message('reset_password');

		$arr['message'] = $action_message['error'];
		$result = 'error';

		if(count($user)>0){

			//generate random password

			$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
			$pass = array(); //remember to declare $pass as an array
			$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache

			for ($i = 0; $i < 8; $i++) {

				$n = rand(0, $alphaLength);
				$pass[] = $alphabet[$n];

			}

			$random_pass = implode($pass);


			$this->master->updateRecord($type,array('password'=>md5($random_pass)), array('id'=>$user[0]['id']));



			//admin emailer info
			$adminemail = $this->common->admin_email();

			//email settings
			$info_arr = array(
				'to'=>$email,
				'from'=>$adminemail,
				'subject'=>'Greetings from Ebinder',
				'view'=>'forget-password-mail-to-user',
				'emailer_file_name'=>'forget-password-mail-to-user',
			);


			$other_info = array(
				'password'=>$random_pass,
				'view'=>'forget-password-mail-to-user',
				'emailer_file_name'=>'forget-password-mail-to-user',
				'name'=>'',//$first.' '.$last,
				'agency_name'=>'',
				'user_name'=>$email,
				'user_email'=>$email,
				'link'=>'#forgot'
			);

			$this->emailer->sendmail($info_arr, $other_info);
			$arr['other_info'] = $other_info;
			$arr['info_arr'] = $info_arr;

			$arr['message'] = $action_message['success'];
			$result = 'ok';



		}

		$arr['result'] = $result;
		echo json_encode($arr);

	}

	/***************************
		classes
	***************************/
	public function classes(){

		//google_id=&facebook_id=&name=s&email=%40gmail.com&password=Darknite1
		$user_id = $this->session->userdata('id');
		$name = $_POST['name'];
		$class_id = $_POST['class_id'];
		$genres = array();
		$dayweek = array(); //$_POST['dayweek']; //array
		$sched_start = array(); // $_POST['start'];
		$sched_end = array(); // $_POST['end'];
		$location = $_POST['location'];
		$lat = $_POST['lat'];
		$long = $_POST['long'];
		$cost = $_POST['cost'];
		$max_students = $_POST['max_students'];
		$customers = array();
		$nowtime = date('Y-m-d H:i:s');
		$level = 'Beginners';
		if(isset($_POST['level'])){
			$level = $_POST['level'];
		}

		if(isset($_POST['genres'])){
			$genres = $_POST['genres'];
		}

		if(isset($_POST['customers'])){
			$customers = $_POST['customers'];
		}

		$result = 'error';

		$arr = array(
			'name'=>$name,
			'genres'=>serialize($genres),
			'location'=>$location,
			'lat'=>$lat,
			'lng'=>$long,
			'customers'=>serialize($customers),
			'cost'=>$cost,
			'schedule'=>serialize($dayweek),
			'sched_start'=>serialize($sched_start),
			'sched_end'=>serialize($sched_end),
			'max_students'=>$max_students,
			'level'=>$level
		);


		//save update
		if($class_id != ''){
			$this->master->updateRecord('classes', $arr, array('id'=>$class_id));

			//get validation message from admin
			$action_message = $this->common->get_message('generic_update_message');

			$arr['message'] = $action_message['success'];
			$result = 'ok';

		}
		else{
			$arr['date_added'] = $nowtime;
			$arr['user_id'] = $user_id;
			$this->master->insertRecord('classes', $arr);

			//get validation message from admin
			$action_message = $this->common->get_message('generic_add_message');

			$arr['message'] = $action_message['success'];
			$result = 'ok';

		}

		if(!isset($arr['message'])){
			$arr['message'] = 'Something went wrong.';
		}

		$this->session->set_flashdata($result, $arr['message']);

		$arr['result'] = $result;
		echo json_encode($arr);

	}


	/***************************
		delete_tb
	***************************/
	public function delete_tb(){
		$id = $_POST['id'];
		$tb = $_POST['table'];

		//delete associated data
		if($tb == 'agency'){
			$users = $this->master->getRecords('customers', array('agency_id'=>$id));
			if(count($users) > 0){
				foreach($users as $r=>$value){

					$policies = $this->master->getRecords('biz_referral', array('broker_id'=>$value['id']));
					if(count($policies) > 0){
						foreach($policies as $rr=>$valuee){
							$this->master->deleteRecord('biz_referral', 'id', $valuee['id']);
						}
					}

					$policies2 = $this->master->getRecords('biz_renewal_history', array('broker_id'=>$value['id']));
					if(count($policies2) > 0){
						foreach($policies2 as $rr2=>$valuee2){
							$this->master->deleteRecord('biz_referral', 'id', $valuee2['id']);
						}
					}


					$this->master->deleteRecord('customers', 'id', $value['id']);
				}
			}
			$brokerage = $this->master->getRecords('brokerage', array('agency_id'=>$id));
			if(count($brokerage) > 0){
				foreach($brokerage as $r=>$value){
					$this->master->deleteRecord('brokerage', 'id', $value['id']);
				}
			}
		}
		if($tb == 'brokerage'){
			$users = $this->master->getRecords('customers', array('brokerage_id'=>$id));
			if(count($users) > 0){
				foreach($users as $r=>$value){

					$policies = $this->master->getRecords('biz_referral', array('broker_id'=>$value['id']));
					if(count($policies) > 0){
						foreach($policies as $rr=>$valuee){
							$this->master->deleteRecord('biz_referral', 'id', $valuee['id']);
						}
					}

					$policies2 = $this->master->getRecords('biz_renewal_history', array('broker_id'=>$value['id']));
					if(count($policies2) > 0){
						foreach($policies2 as $rr2=>$valuee2){
							$this->master->deleteRecord('biz_referral', 'id', $valuee2['id']);
						}
					}

					$this->master->deleteRecord('customers', 'id', $value['id']);

				}
			}
		}

		$this->master->deleteRecord($tb, 'id', $id);
		echo $id;
	}

	public function delete_agency(){

	}

	/***************************
		action_messages
	***************************/
	public function action_messages(){
		$action_messages = $this->common->get_message();
		$banners = $this->master->getRecords('admin_contents', array('type'=>'banner'));
		$action_messages['banners'] = $banners;
		echo json_encode($action_messages);
	}



	/***************************
		contactus
	***************************/
	public function contactus(){

		$inputName = $this->input->post('inputname');
		$inputCompany = $this->input->post('inputwebsite');
		$inputEmail = $this->input->post('inputemail');
		$inputsubject = $this->input->post('inputsubject');
		$inputmessage = $this->input->post('inputmessage');
		$inputCountry = $this->input->post('inputCountry');

		$this->load->library('email');

		$this->email->set_mailtype("html");
		$this->email->from($inputEmail, $inputName);
		$this->email->to('nga.personal@gmail.com');

		$this->email->subject('A E-Binder Visitor contacted you.');



		$msg = '

			<html>
				<body>
					<p>Name: '.$inputName .'</p>
					<p>Email Address: '.$inputEmail .'</p>
					<p>Company: '.$inputCompany .'</p>
					<p>Country: '.$inputCountry .'</p>
					<p>Subject: '.$inputsubject .'</p>
					<p>Message: '.$inputmessage .'</p>
				</body>
			</html>

		';
		$this->email->message($msg);

		//$this->email->send();

		echo json_encode(array('msg' => $this->email->print_debugger()));
	}

	/***************************
		social_login
	***************************/
	public function social_login(){

	//&id=1141912292492016&u_email=snailbob%40live.com&fb_profile=https%3A%2F%2Fgraph.facebook.com%2F1141912292492016%2Fpicture%3Ftype%3Dlarge&fb_data=%7B%22id%22%3A%221141912292492016%22%2C%22email%22%3A%22snailbob%40live.com%22%2C%22first_name%22%3A%22Bobby%22%2C%22gender%22%3A%22male%22%2C%22last_name%22%3A%22Gemong%22%2C%22link%22%3A%22https%3A%2F%2Fwww.facebook.com%2Fapp_scoped_user_id%2F1141912292492016%2F%22%2C%22locale%…_time%22%3A%222015-06-26T10%3A49%3A59%2B0000%22%2C%22verified%22%3Atrue%7D

		$type = $_POST['type'];
		$id = $_POST['id'];
		$email = $_POST['email'];
		$img = $_POST['img'];
		$action_type = $_POST['action_type'];
		$name = $_POST['name'];

		$nowtime = date('Y-m-d H:i:s');

		$whr_user = array(
			'email'=>$email,
			$type.'_id'=>$id //social type
		);

		$whr_m_user = array(
			'email'=>$email
		);

		$agency = $this->master->getRecords('agency', $whr_user);
		$customer = $this->master->getRecords('customers', $whr_user);
		$student = $this->master->getRecords('students', $whr_user);

		$agency2 = $this->master->getRecords('agency', $whr_m_user);
		$customer2 = $this->master->getRecords('customers', $whr_m_user);

		//get validation message from admin
		$action_message = $this->common->get_message('user_login');
		$message = $action_message['error'];
		$result = 'error';

		//detect actions
		if($action_type == 'signup'){

			//get validation message from admin
			$action_message = $this->common->get_message('agency_signup');

			$message = $action_message['error'];
			$result = 'error';
			if(count($agency) == 0){

				$arr['status'] = 'N';
				$arr['enabled'] = 'N';
				$arr['email'] = $email;
				$arr['name'] = $name;
				$arr[$type.'_id'] = $id;
				$arr[$type.'_img'] = $img;
				$arr['date_added'] = $nowtime;

				$id = $this->master->insertRecord('agency', $arr, true);

				//admin emailer info
				$adminemail = $this->common->admin_email();

				//email settings
				$info_arr = array(
					'to'=>$email,
					'from'=>$adminemail,
					'subject'=>'Greetings from Ebinder',
					'view'=>'registration-mail-to-agency',
					'emailer_file_name'=>'registration-mail-to-agency',
				);


				$other_info = array(
					'password'=>'',
					'view'=>'registration-mail-to-agency',
					'emailer_file_name'=>'registration-mail-to-agency',
					'name'=>'',//$first.' '.$last,
					'agency_name'=>'',//$name,
					'user_name'=>$email,
					'user_email'=>$email,
					'link'=>'landing/verify_agency/'.$id.'/'.md5($id)
				);

				$this->emailer->sendmail($info_arr, $other_info);

				$message = $action_message['success'];
				$result = 'ok';

			}//not exist
		}

		//if login
		else {

			if(count($agency) > 0){
				$arr['result'] = 'error';
				$arr['message'] = '<i class="fa fa-warning text-warning"></i> Connect incorrect.';
				foreach($agency as $r=>$value){
					unset($value['password']);
					$value['type'] = 'agency';
					$value['location'] = $value['address'];
					$arr['userdata'] = $value;

					$value['multiple_account'] = 'no';
					//include customer to switch
					if(count($customer2) > 0){
						$value['multiple_account'] = 'yes';
					}


					if($value['enabled'] != 'N'){
						$this->session->set_userdata($value);
						$result= 'ok';
						$message = $action_message['success'];
					}
					else{
						$arr['result'] = 'error';
						$arr['message'] = '<i class="fa fa-warning text-warning"></i> Account not activated.';
					}


				}
			}
			else if(count($customer) > 0){
				$arr['result'] = 'error';
				$arr['message'] = '<i class="fa fa-warning text-warning"></i> Connect incorrect.';
				foreach($customer as $r=>$value){

					unset($value['password']);
					$value['name'] = $value['first_name'].' '.$value['last_name'];
					$value['type'] = 'customer';
					$value['address'] = $value['location'];
					$arr['userdata'] = $value;

					$value['multiple_account'] = 'no';
					//include agency to switch
					if(count($agency2) > 0){
						$value['multiple_account'] = 'yes';
					}

					if($value['enabled'] != 'N'){
						$this->session->set_userdata($value);
						$result= 'ok';
						$message = $action_message['success'];
					}
					else{
						$arr['result'] = 'error';
						$arr['message'] = '<i class="fa fa-warning text-warning"></i> Account not activated.';
					}



				}
			}
			else if(count($student) > 0){
				$arr['result'] = 'error';
				$arr['message'] = '<i class="fa fa-warning text-warning"></i> Connect incorrect.';
				foreach($student as $r=>$value){

					unset($value['password']);
					$value['name'] = $value['first_name'].' '.$value['last_name'];
					$value['type'] = 'student';
					//$value['address'] = $value['location'];
					$arr['userdata'] = $value;

					$value['multiple_account'] = 'no';
					//include agency to switch
//						if(count($agency2) > 0){
//							$value['multiple_account'] = 'yes';
//						}


					if($value['enabled'] != 'N'){
						$this->session->set_userdata($value);
						$result = 'ok';
						$message = $action_message['success'];
					}
					else{
						$arr['result'] = 'error';
						$arr['message'] = '<i class="fa fa-warning text-warning"></i> Account not activated.';
					}

				}
			}
		}

		$arr['result'] = $result;
		$arr['action'] = $action_type;
		$arr['message'] = $message;
		echo json_encode($arr);

	}


	/***************************
		social_login
	***************************/
	public function submit_schedule(){
		//class=18&max_student=33&start_day=Tue+7%2F7%2F2015&end_day=Tue+7%2F7%2F2015&start_time=6%3A44+PM&end_time=10%3A44+PM&duration=240&recur=&weekly_recure=3&days_in_week%5B%5D=1
		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');

		$class = $_POST['class'];
		$max_student = $_POST['max_student'];
		$start_day = $_POST['start_day'];
		$end_day = $start_day; //$_POST['end_day'];
		$start_time = $_POST['start_time'];
		$end_time = $_POST['end_time'];
		$duration = $_POST['duration'];
		$recur = '';
		if(isset($_POST['recur'])){
			$recur = 'yeah';
		}

		$weekly_recure = $_POST['weekly_recure'];
		$days_in_week = array();
		if(isset($_POST['days_in_week'])){
			$days_in_week = $_POST['days_in_week'];
		}

		$schedule_date = array($start_day, $end_day);
		$schedule_date = serialize($schedule_date);
		$schedule_time = array($start_time, $end_time);
		$schedule_time = serialize($schedule_time);

		$start_time_format = $_POST['start_time_format'];
		$end_time_format = $_POST['end_time_format'];

		$start_date_format = $_POST['start_date_format'];
		$end_date_format = $start_date_format;

		$the_end_date_format = $_POST['end_date_format'];


		$nowtime = date('Y-m-d H:i:s');


		//get validation message from admin
		$action_message = $this->common->get_message('generic_add_message');


		$concat_startdate_time = $start_date_format.' 00:00';
		$concat_enddate_time = $end_date_format.' 00:00';

		//format schedule date
		if($start_time_format != ''){
			$concat_startdate_time = $start_date_format.' '.$start_time_format;
			$start_date_format = $start_date_format.' '.$start_time_format.':00'; //T
		}

		if($end_time_format != ''){
			$concat_enddate_time = $end_date_format.' '.$end_time_format;
			$end_date_format = $end_date_format.' '.$end_time_format.':00'; //T
		}



		//run this if recur is not set set
		if($recur == ''){
			$arr = array(
				'start'=>$start_date_format,
				'end'=>$end_date_format,
				'agency_id'=>$user_id,
				'class_id'=>$class,
				'max_student'=>$max_student,
				'schedule_date'=>$schedule_date,
				'schedule_time'=>$schedule_time,
				'duration'=>$duration,
				'day_in_week'=>serialize($days_in_week),
				'status'=>'Y',
				'date_added'=>$nowtime
			);

			$this->master->insertRecord('schedules', $arr);

			$test['the_schedules'] = $arr;
			$test['message'] = $action_message['success'];
			$test['result'] = 'ok';
			$this->session->set_flashdata($test['result'], $action_message['success']);
			echo json_encode($test);
			return false;

		}



		//get remaining days from the start day
		$current_year = date('Y');
		$endyr_date = new DateTime($current_year.'-12-31 23:59');
		$strt_date = new DateTime($start_date_format);
		$eend_date = new DateTime($the_end_date_format);
		$diff = $strt_date->diff($endyr_date);
		$diff2 = $strt_date->diff($eend_date);

		$remaining_days = $diff->y * 365 + $diff->m * 30 + $diff->d + $diff->h/24; // + $diff->i / 60;
		$remaining_endby_days = $diff2->y * 365 + $diff2->m * 30 + $diff2->d + $diff2->h/24; // + ($diff2->i / 60)/24;


		$the_schedules = array();


		//$weekly_recure = $weekly_recure * 7;
		$until = ($remaining_days/$weekly_recure);
		$test['weekly_recure'] = $weekly_recure;
		$test['until'] = $until;
		$test['days_in_week'] = $days_in_week; //weekday
		$test['recurrence_type'] = 'if weekly';

		$test['message'] = 'if weekly';
		$test['status'] = 'if weekly';


		//End after x occurence
		//$weekly_looper = $remaining_endby_days;
		$weekly_looper = $weekly_recure;

		//apply recurrence
		$day_after_day = 1;
		$current_week_number = date('W', strtotime($concat_startdate_time));
		$curr_week_number = 0;
		$test['week_number_count'] = array();



		$total_stored_sched = 0;
		for($x = 0; $x < $remaining_days; $x++){ //loop until remaining days of the year
			$weekday_id = date('w', strtotime($concat_startdate_time));
			$new_week_number = date('W', strtotime($concat_startdate_time));

			$test['week_number_count'][] = $new_week_number;


			if(($curr_week_number % $weekly_recure) == 0){ //modulus for weekly recur
				if(in_array($weekday_id, $days_in_week)){

					$arr = array(
						'start'=>$start_date_format,
						'end'=>$end_date_format,
						'agency_id'=>$user_id,
						'class_id'=>$class,
						'max_student'=>$max_student,
						'schedule_date'=>$schedule_date,
						'schedule_time'=>$schedule_time,
						'duration'=>$duration,
						'day_in_week'=>serialize($days_in_week),
						'status'=>'Y',
						'date_added'=>$nowtime
					);
					$the_schedules[] = $arr;
					$this->master->insertRecord('schedules', $arr);
				}
			}


			//update number of week
			if($current_week_number != $new_week_number){
				$current_week_number = $new_week_number;
				$curr_week_number++;
			}


			//change date variables
			$start_added_date_time = strtotime($concat_startdate_time.' + '.$day_after_day.' DAYS');
			$end_added_date_time = strtotime($concat_enddate_time.' + '.$day_after_day.' DAYS');

			$concat_startdate_time = date("Y-m-d H:i", $start_added_date_time);
			$concat_enddate_time = date("Y-m-d H:i", $end_added_date_time);

			$start_time_format = substr($concat_startdate_time, -5);
			$end_time_format = substr($concat_enddate_time, -5);
			$start_date_format = substr($concat_startdate_time, 0, 10);
			$end_date_format = substr($concat_enddate_time, 0, 10);

			$start_date_format = $start_date_format.' '.$start_time_format.':00';  //T
			$end_date_format = $end_date_format.' '.$end_time_format.':00';  //T


			//End by date
			if($total_stored_sched == $weekly_looper){
				$test['message'] = $action_message['success'];
				$test['the_schedules'] = $the_schedules;
				$test['result'] = 'ok';
				$this->session->set_flashdata($test['result'], $action_message['success']);
				echo json_encode($test);
				return false;
			}
			//End by date


			//increment stored scheds
			if(($curr_week_number % $weekly_recure) == 0){ //modulus for weekly recur
				if(in_array($weekday_id, $days_in_week)){
					$total_stored_sched++;
				}
			}



		}//end for loop


		//success if reach this section
		$test['message'] = $action_message['success'];
		$test['result'] = 'success';
		$this->session->set_flashdata($test['status'], $action_message['success']);

		//if no schedule added
		if(count($the_schedules) == 0){
			$test['message'] = '<i class="fa fa-info text-warning"></i> No schedule was added. Please double check your recurring pattern.';
			$test['result'] = 'ok';
			$this->session->set_flashdata($test['result'], $test['message']);


		}

		$test['the_schedules'] = $the_schedules;
		echo json_encode($test);

	}

	public function remove_card(){
		$id = $_POST['id'];
		$stripe = $_POST['stripe'];

		Stripe::setApiKey("sk_test_21OnMrInOQSqxd6aSeAO3Nu0"); //sk_test_UTHtN8EB5Y5xwTB1OkeS6lJ8"); //sk_test_21OnMrInOQSqxd6aSeAO3Nu0");

		$cu = Stripe_Customer::retrieve($stripe);
		$cu->delete();

		$arr = array(
			'stripe_id'=>''
		);

		$this->master->updateRecord('customers',$arr,array('id'=>$id));

		//get validation message from admin
		$action_message = $this->common->get_message('customer_remove_card');
		$message = $action_message['success'];
		$result = 'ok';
		$this->session->set_flashdata($result, $message);

		$arr['result'] = $result;
		$arr['message'] = $message;
		echo json_encode($arr);
	}

	public function cargo_price(){
		//cargo-price=0.02&cargo-price-id=1
		$price = $_POST['cargo-price'];
		$id = $_POST['cargo-price-id'];
		$this->master->updateRecord('cargo_category', array('price'=>$price), array('id'=>$id));
		echo json_encode(array('id'=>$id));
	}


	public function inline_text_update(){
		$user_id = $this->session->userdata('id');
		$text_input = $_POST['text_input'];
		$id = $_POST['id'];
		$field_name = $_POST['field_name'];
		$db_name = $_POST['db_name'];


		$this->master->updateRecord($db_name, array($field_name=>$text_input), array('id'=>$id));



		//store edit log
		if($db_name == 'q_business_specific'){ //occupations multiplier
			$typee = 'occupation';
			$arr['modal_id'] = 'cargoCategoryModal';
			$save_agency_ratings_log = $this->common->save_agency_ratings_log($typee);
		}
		if($db_name == 'biz_prod_questionnaire'){ //preview_form multiplier
			$typee = 'preview_form';
			$save_agency_ratings_log = $this->common->save_agency_ratings_log($typee);
		}

		$nowtime = date('d-m-Y');
		$arr['id'] = $id;
		$arr['nowtime'] = $nowtime;
		echo json_encode($arr);

	}

	public function cargo_price_agency(){
		//cargo-price=0.02&cargo-price-id=1
		$user_id = $this->session->userdata('id');

		if($user_id == '' || $this->session->userdata('logged_admin') != ''){
			$user_id = $this->common->default_cargo();
		}
		$type = $this->session->userdata('type');

		$agency_id = $_POST['cargo-agency-id'];
		$price = $_POST['cargo-price'];
		$id = $_POST['cargo-price-id'];
		$pricetype = $_POST['agency-price-type'];

		$db_name = 'agency';


		$theprice = ($price) ? $price : 0;
		if(isset($_POST['db-name'])){
			$db_name = $_POST['db-name'];

			//format float for deductible
			if($db_name == 'deductibles'){
				$theprice = floatval($theprice);
			}
		}
		else{
			$agency_prices = $this->common->agency_prices($user_id, $pricetype);
			//set value to the index
			$agency_prices[$id] = $price;
			$theprice = serialize($agency_prices);
		}


		if($agency_id != ''){
			$this->master->updateRecord($db_name, array($pricetype=>$theprice), array('id'=>$agency_id));
		}
		else{
			$this->master->updateRecord('cargo_category', array('price'=>$price), array('id'=>$id));
		}


		//store edit log
		if($pricetype == 'cargo_prices'){ //occupations multiplier
			$typee = 'occupation';
			$arr['modal_id'] = 'cargoCategoryModal';
			$save_agency_ratings_log = $this->common->save_agency_ratings_log($typee);
		}
		else if($pricetype == 'transportation_prices'){ //employees
			$typee = 'employees';
			$arr['modal_id'] = 'transportationModal';

			$save_agency_ratings_log = $this->common->save_agency_ratings_log($typee);
		}
		else if($pricetype == 'deductible' || $pricetype == 'rate'){ //deductible multiplier
			$typee = 'deductible';
			$arr['modal_id'] = 'deductibleModal';

			$save_agency_ratings_log = $this->common->save_agency_ratings_log($typee);
		}

		$nowtime = date('d-m-Y');
		$arr['id'] = $id;
		$arr['nowtime'] = $nowtime;
		echo json_encode($arr);

	}

	public function buyform_to_session(){
		$buy_inputs = $_POST['buy_inputs'];
		$cargo_inputs = $_POST['cargo_inputs'];
		$premium = $_POST['premium'];
		$cargo_price = $_POST['cargo_price'];
		$currency = $_POST['currency'];
		$selected_deductible = $_POST['selected_deductible'];
		$referral_id = $this->session->userdata('referral_id');
		$max_insured = $_POST['max_insured'];

		//format buy input array
		$buy_form_inputs = array();
		if(is_array($buy_inputs)){
			foreach($buy_inputs as $r=>$value){
				$buy_form_inputs[$value['name']] = $value['value'];
			}
		}

		//check currency value to max insured
		$default_cargo = $this->common->db_field_id('admin', 'default_cargo', '2');
		$base_currency = $this->common->base_currency($default_cargo);
		$insured_val = $this->common->numeric_currency($buy_form_inputs['invoice']);
		$insured_converted_val = ($base_currency != $currency) ? $this->common->converted_value($currency, $base_currency, $insured_val) : $insured_val;

		$refer = false;

		if($insured_converted_val > $max_insured){
			$refer = true;
		}

		//add converted val to session
		$buy_inputs[] = array(
			'name'=>'converted_invoice',
			'value'=>$insured_converted_val
		);
		//add default currency
		$buy_inputs[] = array(
			'name'=>'base_currency',
			'value'=>$base_currency
		);


		//add cargo inputs to buy inputs
		foreach($cargo_inputs as $cargoi){
			$buy_inputs[] = $cargoi;
		}

		$arr = array(
			'buy_inputs'=>$buy_inputs,
			'premium'=>round($premium, 2),
			'premium_format'=>number_format($premium, 2, '.', ','),
			'selected_deductible'=>$selected_deductible,
			'cargo_price'=>$cargo_price,
			'currency'=>$currency,
			'refer'=>$refer
			//'bindoption'=>$bindoption
		);
		if($referral_id == ''){
			$this->session->set_userdata($arr);
		}
		//$arr['premium'] = $this->session->userdata('premium');
		echo json_encode($arr);

	}

	public function admin_refer_country(){



	}

	public function select_consignor(){
		$mycustomers = $_POST['mycustomers'];
		$customer = $this->master->getRecords('agent_customers', array('id'=>$mycustomers));

		if(count($customer) == 0){
			$details = array(
				'first_name'=>'',
				'last_name'=>'',
				'business_name'=>'',
				'location'=>'',
				'lat'=>'',
				'lng'=>'',
				'country_id'=>'',
				'contact_no'=>'',
				'calling_code'=>'',
				'calling_digits'=>'',
				'email'=>''
			);
		}
		else{
			$details = unserialize($customer[0]['details']);
		}

		$details['id'] = $mycustomers;
		$arr['single'] = $details; //consignee

		$this->session->set_userdata($arr);
		echo json_encode($arr);
	}

	public function single_purch_form(){
		$user_id = $this->session->userdata('id');
		$single = $this->session->userdata('single');
		$single_email = $single['email'];

		$inputs = $_POST['inputs'];
		$first = $_POST['first'];
		$last = $_POST['last'];
		$bname = $_POST['bname'];
		$address = ''; //$_POST['address'];
		$lat = ''; //$_POST['lat'];
		$lng = ''; //$_POST['lng'];
		$email = $_POST['email'];
		$country_code = $_POST['country_code'];
		$ccode = $_POST['ccode'];
		$mobile = $_POST['mobile'];

		$info = $_POST['info'];
		$crefer = $_POST['crefer'];
		$buy_inputs = $this->session->userdata('buy_inputs');
		$premium = $this->session->userdata('premium');
		$premium = round($premium, 2);
		$nowtime = date('Y-m-d H:i:s');

		$arr = array(
			'first_name'=>$first,
			'last_name'=>$last,
			'business_name'=>$bname,
			'location'=>$address,
			'lat'=>$lat,
			'lng'=>$lng,
			'country_id'=>$country_code,
			'contact_no'=>$ccode.ltrim($mobile,'0'),
			'calling_code'=>$ccode,
			'calling_digits'=>ltrim($mobile,'0'),
			'email'=>$email
		);

		if($user_id != ''){
			//if a consignor
			if($info == 'consignor'){
				$this->master->updateRecord('customers', $arr, array('id'=>$user_id));
			}

			//if customer adding agent
			else if($info == 'agent_customer'){


				$cust_info = array(
					'agent_id'=>$user_id,
					'name'=>$first.' '.$last,
					'email'=>$email,
					'details'=>serialize($arr),
					'date_added'=>$nowtime,
				);
				$id = '';
				if(isset($_POST['id'])){
					$id = $_POST['id'];
				}


				if($id == ''){
					$this->session->set_flashdata('ok', 'Customer successfully added.');
					$id = $this->master->insertRecord('agent_customers', $cust_info, true);



					$dbtable = 'agent_customers';
					$emailer_file_name = 'signup-mail-to-broker';
					$email_name = $first.' '.$last;


					//admin emailer info
					$adminemail = $this->common->admin_email();

					//email settings
					$info_arr = array(
						'to'=>$email,
						'from'=>$adminemail,
						'subject'=>'Greetings from Ebinder',
						'view'=>$emailer_file_name,
					);


					$other_info = array(
						'password'=>'',
						'emailer_file_name'=>$emailer_file_name,
						'name'=>$email_name,
						'agency_name'=>'', //$name,
						'user_name'=>$email,
						'user_email'=>$email,
						'link'=>'landing/verify_user/'.$id.'/'.md5($id).'/'.$dbtable
					);

					$this->emailer->sendmail($info_arr, $other_info);

				}
				else{
					$this->session->set_flashdata('ok', 'Customer successfully updated.');
					$this->master->updateRecord('agent_customers', $cust_info, array('id'=>$id));
				}
			}

		}
		else{
			$user_id = 0;
		}

		//check if consignee
		if($info == 'consignee'){
			$consignee = $arr;
			$arr = array();
			$arr['consignee'] = $consignee;
		}
		else{
			$arr['single'] = $arr;
			$arr['address'] = $address;
		}

		//store to session if not new customer
		if($info != 'agent_customer'){
			$this->session->set_userdata($arr);
		}


		//check if country needs referral
		if($crefer != ''){
			$details = array(
				'buy_inputs'=>$buy_inputs,
				'single_input'=>$arr['single'],
				'premium'=>$premium
			);


			$data = array(
				'customer_id'=>$user_id,
				'details'=>serialize($details),
				'status'=>'S',
				'email'=>$single_email,
				'date_added'=>$nowtime
			);

			$booking_id = $this->master->insertRecord('bought_insurance', $data, true);

			//admin emailer info
			$adminemail = $this->common->admin_email();

			//email settings for agency
			$info_arr = array(
				'to'=>$this->common->recovery_admin(), //$adminemail,
				'from'=>$adminemail,
				'subject'=>'Greetings from Ebinder',
				'view'=>'referral-notification-to-admin',
				'emailer_file_name'=>'referral-notification-to-admin',
			);


			$other_info = array(
				'password'=>'',
				'view'=>'referral-notification-to-admin',
				'emailer_file_name'=>'referral-notification-to-admin',
				'name'=>'',//$first.' '.$last,
				'agency_name'=>'Admin',
				'user_name'=>$adminemail,
				'user_email'=>$adminemail,
				'link'=>'webmanager/insurance'
			);
			$data['info_arr1'] = $info_arr;
			$data['other_info1'] = $other_info;
			$this->emailer->sendmail($info_arr,$other_info);

			//email settings for student
			$info_arr = array(
				'to'=>$single_email,
				'from'=>$adminemail,
				'subject'=>'Greetings from Ebinder',
				'view'=>'referral-notification-to-customer',
				'emailer_file_name'=>'referral-notification-to-customer',
			);

			$other_info = array(
				'password'=>'',
				'view'=>'referral-notification-to-customer',
				'emailer_file_name'=>'referral-notification-to-customer',
				'name'=>$single['first_name'].' '.$single['last_name'],
				'agency_name'=>'', //$agency_info[0]['name'],
				'user_name'=>$single_email,
				'user_email'=>$single_email,
				'link'=>'dashboard' //landing/download/'.$user_id.'/'.$booking_id.'/'.md5($user_id)
			);
			if($user_id == '0'){
				//$other_info['link'] = 'landing/reqdetails/'.$booking_id.'/'.md5($email);
			}

			$data['info_arr2'] = $info_arr;
			$data['other_info2'] = $other_info;
			$this->emailer->sendmail($info_arr,$other_info);

			$arr['data'] = $data;
		}

		//get validation message from admin
		$action_message = $this->common->get_message('referral_request');
		$message = $action_message['success'];
		$result = 'ok';

		$arr['result'] = $result;
		$arr['message'] = $message;

		echo json_encode($arr);

	}

	public function rate_submit(){
		// $user_id = $this->session->userdata('id');
		$user_id = (!empty($user_id)) ? $user_id : $this->common->default_cargo();

		$logged_id = $this->session->userdata('id');;

		//$type = $this->session->userdata('type');

		$nowtime = date('Y-m-d H:i:s');
		$type = $_POST['type'];
		$rate = $_POST['rate'];
		//$rate = round($rate, 2);
		//$rate = floatval($rate);
		$old_val = $this->common->db_field_id('agency', $type, $user_id);
		$this->master->updateRecord('agency', array($type=>$rate), array('id'=>$user_id));

		$arr = array(
			'type'=>$type,
			'agency_id'=>$user_id,
			'new_val'=>$rate,
			'old_val'=>$old_val,
			'user_id'=>$logged_id,
			'date_added'=>$nowtime
		);
		$this->master->insertRecord('agency_log', $arr);

		echo json_encode(array('rate'=>$rate));
	}

	public function get_agency_log(){
		$type = $_POST['type'];
		$agency_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];

		$data['agency_log'] = $this->master->getRecords('agency_log', array('type'=>$type,'agency_id'=>$agency_id));
		$data['type'] = $type;

		$data['view'] = $this->load->view('domains/rating_history_details', $data, true);
		echo json_encode($data);
	}

	public function add_deductible(){

		$user_id = $this->session->userdata('id');
		if($user_id == '' || $this->session->userdata('logged_admin') != ''){
			$user_id = (!empty($user_id)) ? $user_id : $this->common->default_cargo();
		}

		$logged_id = $user_id;



		$name = $_POST['name'];
		$price = $_POST['price'];

		$user_id = $this->common->default_cargo();

		$arr = array(
			'agency_id'=>$user_id,
			'deductible'=>$name,
			//'the_default', 'N',
			'rate'=>$price
		);
		$id = $this->master->insertRecord('deductibles', $arr, true);


		//logs
		// $log_activity = array(
		// 	'name'=>'Added new deductible.',
		// 	'type'=>'deductibles',
		// 	'details'=>serialize($arr)
		// );
		// $this->common->insert_activity_log($log_activity);


		//$type = $this->session->userdata('type');

		$nowtime = date('Y-m-d H:i:s');


		$arr = array(
			'type'=>'deductible',
			'agency_id'=>$user_id,
			'new_val'=>$price,
			'old_val'=>'',
			'user_id'=>$logged_id,
			'date_added'=>$nowtime
		);
		$this->master->insertRecord('agency_log', $arr);



		$nowtime = date('d-m-Y');

		// $arr['log_activity'] = $log_activity;
		$arr['id'] = $id;
		$arr['nowtime'] = $nowtime;
		$arr['result'] = 'ok';
		$arr['message'] = 'Deductible succesfully added.';
		echo json_encode($arr);
	}

	public function add_port_code(){
		$ccode = $_POST['ccode'];
		$portcode = $_POST['portcode'];
		$portname = $_POST['portname'];

		$arr = array(
			'country_code'=>$ccode,
			'port_code'=>$portcode,
			'port_name'=>$portname,
		);

		$this->master->insertRecord('port_codes', $arr);

		$arr['result'] = 'ok';
		$arr['message'] = 'Port code succesfully added.';
		$this->session->set_flashdata($arr['result'], $arr['message']);
		echo json_encode($arr);

	}

	public function add_cargo_cat(){
		$code = ''; //$_POST['code'];
		$name = $_POST['name'];
		$price = $_POST['price'];
		$referral = '';
		if(isset($_POST['referral'])){
			$referral = 'Yes';
		}

		$user_id = $this->common->default_cargo();
		$pricetype = 'cargo_prices';
		$agency_prices = $this->common->agency_prices($user_id, $pricetype);
		$cargo_max = $this->common->agency_prices($user_id,'cargo_max');

		$arr = array(
		//	'hs_code'=>$code,
			'specific_name'=>$name,
			'referral'=>$referral
		);

		$id = $this->master->insertRecord('q_business_specific', $arr, true);

		//set value to the index
		$agency_prices[$id] = $price;
		//$cargo_max[$id] = ($maxin) ? $maxin : 0;
		$this->master->updateRecord('agency', array($pricetype=>serialize($agency_prices)), array('id'=>$user_id));
		//$this->master->updateRecord('agency', array('cargo_max'=>serialize($cargo_max)), array('id'=>$user_id));


		//logs
		$log_activity = array(
			'name'=>'Added new occupation.',
			'type'=>'occupation',
			'details'=>serialize($arr)
		);
		$this->common->insert_activity_log($log_activity);




		//$arr['cargo_max'] = $cargo_max[$id];
		$arr['price'] = $agency_prices[$id];
		$arr['id'] = $id;
		$arr['result'] = 'ok';
		$arr['message'] = 'Occupation succesfully added.';
		echo json_encode($arr);
	}

	public function currencies(){
		$this->load->view('front/currencies-stripe');
	}

	public function currs(){
		$arr = $_POST['arr'];
		$curr[] = array();
		foreach($arr as $r){
			$r2 = substr($r, 0, 3);
			$arrs = array(
				'currency'=>$r2,
				'name'=>$r
			);
			$this->master->insertRecord('au_stripe_currencies', $arrs);
			$curr[] = $r2;
		}

		echo json_encode($curr);
	}

	public function reset_buyform(){
		$page = $this->uri->segment(3);
		$arr = array(
			'buy_inputs',
			'premium',
			'premium_format',
			'selected_deductible',
			'cargo_price',
			'currency',
			'single',
			'consignee'
		);

		foreach ($arr as $key) {
			$this->session->unset_userdata($key);
		}

		redirect(base_url().$page);
	}

	public function validate_domain(){
		$domain = $_POST['domain'];
		$arr = array(
			'domain'=>$domain
		);

		$brokers = $this->master->getRecords('agency', $arr);
		$arr['count'] = count($brokers);
		$arr['agency'] = $brokers;

		echo json_encode($arr);
	}







	/***************************
		get_occupations
	***************************/
	public function get_occupations(){
		$catid = $_POST['catid'];

		$jobs = $this->master->getRecords('q_business_specific', array('category_id'=>$catid));

		echo json_encode($jobs);
	}


	/***************************
		search_occupation
	***************************/
	public function search_occupation(){
		$keyword = $_POST['keyword'];

		$this->db->like('specific_name', $keyword);

		$jobs = $this->master->getRecords('q_business_specific');

		echo json_encode($jobs);
	}



	/***************************
		save_member
	***************************/
	public function save_member(){

		$user_session = $this->session->all_userdata();

		$input = $_POST['input'];
		$data = array();
		foreach($input as $r=>$value){
			if($value['name'] != 'product[]'){
				$data[$value['name']] = $value['value'];
			}
			else{
				$data['product'][] = $value['value'];
			}
		}

		$email = $data['email'];
		$dbtable = 'customers';
		$emailer_file_name = 'signup-mail-to-broker';

		$nowtime = date('Y-m-d H:i:s');
		$arr = array(
			'email'=>$email,
			'status'=>'N',
			'enabled'=>'N',
			'date_added'=>$nowtime
		);

		$c_type = 'N';
		//store name for student or customer
		$arr['first_name'] = $data['first'];
		$arr['last_name'] = $data['last'];
		$arr['domain'] = '';
		$arr['business_name'] = $data['bname'];
		$arr['customer_type'] = $c_type;
		$arr['customer_info'] = serialize($data);
		$arr['privileges'] = $data['privileges'];
		$arr['super_admin'] = $data['super_admin'];

		$email_name = $data['first'].' '.$data['last'];


		$customer_type = (isset($data['customer_type'])) ? $data['customer_type'] : 'N';

		$arr['studio_id'] = (isset($user_session['id'])) ? $user_session['id'] : $this->session->userdata('id');
		$id = $this->master->insertRecord($dbtable, $arr, true);
		$arr['id'] = $id;
		$data['customer_id'] = $id;
		//$data['customer_type'] = $c_type;


		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings
		$info_arr = array(
			'to'=>$email,
			'from'=>$adminemail,
			'subject'=>'Greetings from Ebinder',
			'view'=>$emailer_file_name,
		);

		$append_getmethod = '';
		$other_info = array(
			'password'=>'',
			'emailer_file_name'=>$emailer_file_name,
			'name'=>$email_name,
			'agency_name'=>'',
			'user_name'=>$email,
			'user_email'=>$email,
			'link'=>'landing/verify_user/'.$id.'/'.md5($id).'/'.$dbtable.'/customer'.$append_getmethod
		);
		$arr['info_arr'] = $info_arr;
		$arr['other_info'] = $other_info;
		$arr['user_session'] = $user_session;

		$action_message = $this->common->get_message('customer_added');

		$this->session->set_flashdata('info', 'Organization member successfully added. Verification email sent to user.');// $action_message['success']);

		$this->session->set_userdata('info_format', $data);

		echo json_encode($arr);
	}


	/***************************
		save_customer
	***************************/
	public function save_customer(){
		$user_session = $this->session->all_userdata();

		$input = $_POST['input'];
		$data = array();
		foreach($input as $r=>$value){
			if($value['name'] != 'product[]'){
				$data[$value['name']] = $value['value'];
			}
			else{
				$data['product'][] = $value['value'];
			}
		}


		$email = $data['email'];
		$dbtable = 'customers';
		$emailer_file_name = 'signup-mail-to-broker';

		$nowtime = date('Y-m-d H:i:s');
		$arr = array(
			'email'=>$email,
			'status'=>'N',
			'enabled'=>'N',
			'date_added'=>$nowtime
		);

		$c_type = 'Y';
		//store name for student or customer
		$arr['first_name'] = $data['first'];
		$arr['last_name'] = $data['last'];
		$arr['domain'] = '';
		$arr['business_name'] = $data['bname'];
		$arr['customer_type'] = $c_type;
		$arr['customer_info'] = serialize($data);

		$email_name = $data['first'].' '.$data['last'];


		$customer_type = (isset($data['customer_type'])) ? $data['customer_type'] : 'N';
		if($customer_type == 'N' && $user_session['id'] == $data['user_id']){

			$arr['studio_id'] = $user_session['id'];
			$id = $this->master->insertRecord($dbtable, $arr, true);
			$arr['id'] = $id;
			$data['customer_id'] = $id;
			//$data['customer_type'] = $c_type;


			//admin emailer info
			$adminemail = $this->common->admin_email();

			//email settings
			$info_arr = array(
				'to'=>$email,
				'from'=>$adminemail,
				'subject'=>'Greetings from Ebinder',
				'view'=>$emailer_file_name,
			);

			$append_getmethod = '';
			$other_info = array(
				'password'=>'',
				'emailer_file_name'=>$emailer_file_name,
				'name'=>$email_name,
				'agency_name'=>'',
				'user_name'=>$email,
				'user_email'=>$email,
				'link'=>'landing/verify_user/'.$id.'/'.md5($id).'/'.$dbtable.'/customer'.$append_getmethod
			);

			$arr['info_arr'] = $info_arr;
			$arr['other_info'] = $other_info;

			$action_message = $this->common->get_message('customer_added');

			$this->session->set_flashdata('info', $action_message['success']);

			$this->session->set_userdata('info_format', $data);

		}
		else{
			$arr = array();
			$arr['first_name'] = $data['first'];
			$arr['last_name'] = $data['last'];
			$arr['business_name'] = $data['bname'];
			$arr['customer_info'] = serialize($data);

			$this->master->updateRecord($dbtable, $arr, array('id'=>$user_session['id']));
			$arr['id'] = $user_session['id'];

			$this->session->set_userdata('info_format', $data);


		}

		$arr['customer_type'] = $customer_type;

		echo json_encode($arr);
	}



	public function store_tab2(){
		$data = $_POST['data'];
		$nowtime = date('Y-m-d H:i:s');

		$arr = array();
		foreach($data as $r=>$value){
			if (strpos($value['name'], '[]') !== false) {
				$key = trim($value['name'], "[]");
				$arr[$key][] = $value['value'];
			}
			else{
				$arr[$value['name']] = $value['value'];
			}
		}

		$info_format = $this->session->userdata('info_format');
		$id = $this->session->userdata('id');
		$id = ('N' == $info_format['customer_type']) ? $info_format['user_id'] : $id;
		$broker_id = ('N' == $info_format['customer_type']) ? $this->session->userdata('studio_id') : $info_format['user_id'];

		$ins = array(
			'user_info'=>serialize($info_format),
			'answer'=>serialize($arr),
			'answer_arr'=>serialize($data),
			'customer_id'=>$id,
			'broker_id'=>$broker_id,
			'answer'=>serialize($arr),
			'date_added'=>$nowtime
		);

		$this->master->insertRecord('q_answers', $ins);

		//get validation message from admin
		$action_message = $this->common->get_message('answered_questions');
		$arr['message'] = $action_message['success'];


		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings
		$broker_email = $this->common->db_field_id('customers', 'email', $broker_id);
		$emailer_file_name = 'questionnaire-completed-to-broker';
		$info_arr = array(
			'to'=>$broker_email,
			'from'=>$adminemail,
			'subject'=>'Greetings from Ebinder',
			'view'=>$emailer_file_name,
		);


		$other_info = array(
			'password'=>'',
			'emailer_file_name'=>$emailer_file_name,
			'name'=>'', //$email_name,
			'agency_name'=>'', //$name,
			'user_name'=>$broker_email,
			'user_email'=>$broker_email,
			'link'=>'login'
		);

		$this->emailer->sendmail($info_arr, $other_info);
		$arr['info_arr'] = $info_arr;
		$arr['other_info'] = $other_info;

		echo json_encode($arr);
	}


	public function save_broker_note(){
		$id = $_POST['id'];
		$notes = $_POST['notes'];

		$arr = array(
			'comment'=>$notes
		);

		$this->master->updateRecord('q_answers', $arr, array('id'=>$id));

		echo json_encode($arr);
	}


	public function save_paymentmethod(){
		$nonceFromTheClient = $_POST['nonce'];

		$arr = array('nonce'=>$nonceFromTheClient);

		//for add payment only
		$id = $this->session->userdata('id');

		$result = Braintree_Customer::create(array(
			'firstName' => $this->session->userdata('first_name'),
			'lastName' => $this->session->userdata('last_name'),
			'company' => $this->session->userdata('business_name'),
			'email' => $this->session->userdata('email'),
			'paymentMethodNonce' => $nonceFromTheClient
		));

		if ($result->success) {
			$bcid = $result->customer->id; //echo($result->customer->paymentMethods[0]->token);

			$this->master->updateRecord('customers', array('stripe_id'=>$bcid), array('id'=>$id));

			//get validation message from admin
			$action_message = $this->common->get_message('customer_add_card');
			$message = $action_message['success'];
			$arr['message'] = $message;
			//$this->session->set_flashdata('success', $message);

		} else {
			foreach($result->errors->deepAll() AS $error) {
				$err = $error->code . ": " . $error->message;

				//$this->session->set_flashdata('error', $err);

				$arr['message'] = $err;
			}
		}

		echo json_encode($arr);

	}

	public function send_updated_req_tocustomer(){
		$id = $_POST['id'];
		$customer_info = $this->common->customer_format($id);
		//admin emailer info
		$adminemail = $this->common->admin_email();
		$email = $customer_info['email'];
		$email_name = $customer_info['first_name'];
		$user_name = $this->session->userdata('first_name').' '.$this->session->userdata('last_name');
		$user_email = $this->session->userdata('email');
		$emailer_file_name = 'send-update-request-to-customer';
		//email settings
		$info_arr = array(
			'to'=>$email,
			'from'=>$adminemail,
			'subject'=>'Greetings from Ebinder',
			'view'=>$emailer_file_name,
		);

		$other_info = array(
			'password'=>'',
			'emailer_file_name'=>$emailer_file_name,
			'name'=>$email_name,
			'agency_name'=>'',
			'user_name'=>$user_name,
			'user_email'=>$user_email,
			'link'=>'login'
		);

		$this->emailer->sendmail($info_arr, $other_info);

		//get validation message from admin
		$action_message = $this->common->get_message('broker_request_response_update');

		$arr['other_info'] = $other_info;
		$arr['info_arr'] = $info_arr;
		$arr['message'] = $action_message['success'];

		echo json_encode($arr);
	}

	public function charge_customer(){
		$sale_result = Braintree_Transaction::sale(
		  array(
			'customerId' => '55484848',
			'amount' => '5.00',
			//'billingAddressId' => 'AA',
			//'shippingAddressId' => 'AB'
		  )
		);

		//$result = Braintree_Test_Transaction::settle($sale_result->transaction->id);
		//$succ = $result->success;
		# true

		$status = $sale_result->transaction->status;
		$id = $sale_result->transaction->id;
		$type = $sale_result->transaction->type;
		$currencyIsoCode = $sale_result->transaction->currencyIsoCode;
		$amount = $sale_result->transaction->amount;
//		$date = $sale_result->transaction->date;
//		$timezone_type = $sale_result->transaction->timezone_type;
//		$timezone = $sale_result->transaction->timezone;
//		$firstName = $sale_result->transaction->firstName;
//		$lastName = $sale_result->transaction->lastName;
		//$paymentId = $sale_result->paymentId; //$result->transaction->status;

		$arr = array(
			'status'=>$status,
			'transaction_id'=>$id,
			'type'=>$type,
			'currency'=>$currencyIsoCode,
			'amount'=>$amount,
//			'date'=>$date,
//			'timezone_type'=>$timezone_type,
//			'timezone'=>$timezone,
//			'firstName'=>$firstName,
//			'lastName'=>$lastName
		);
//		var_dump($sale_result).'<br><br><br>';
//		var_dump($status).'<br><br><br>';
//		var_dump($id).'<br>';
		$nowtime = date('Y-m-d H:i:s');
		$data = array(
			'broker_id'=>'1',
			'transaction_id'=>$id,
			'arr'=>serialize($arr),
			'date_added'=>$nowtime
		);

		$this->master->insertRecord('q_transactions', $data);

		echo json_encode($arr);
		//var_dump($result).'<br>';
	}

	public function save_broker_policy(){
		$id = $this->session->userdata('id');
		$data = $_POST['data'];


		$vdata = array();
		foreach($data as $r=>$value){
			$name = str_replace('[]', '', $value['name']);
			$vdata[$name][] = $value['value'];
		}


		$arr = array(
			'broker_policies'=>serialize($vdata)
		);

		$this->master->updateRecord('customers', $arr, array('id'=>$id));

		echo json_encode($vdata);
	}



	/***************************
		policy_files
	***************************/
	public function policy_files() {
		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');
		$output_dir = "uploads/policyd/";
		if(isset($_FILES["myfile"])) {
			//Filter the file types , if you want.
			if ($_FILES["myfile"]["error"] > 0) {
			  echo 'error';
			}
			else {
				$ff = $_FILES["myfile"]["name"];
				$file_name = $_FILES["myfile"]["name"];
				$file_name = substr($file_name, -3);

				if($file_name == 'exe'){
					echo 'not_img';
					return false;
				}

				$thefilename = uniqid().str_replace(' ', '_', $_FILES["myfile"]["name"]);

				//move the uploaded file to uploads folder;
				move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$thefilename); //$_FILES["myfile"]["name"]);

				$img_type = $_POST['img_type'];
				$arr = array (
					'name'=>$thefilename,
					'product_id'=>$img_type,
					'broker_id'=>$user_id
				);

				$file_id = $this->master->insertRecord('policy_docs',$arr, true);




				//logs
				$log_activity = array(
					'name'=>'Policy document updated.',
					'type'=>'policydoc',
					'details'=>serialize($arr)
				);
				$this->common->insert_activity_log($log_activity);



				$arr['id'] = $file_id;
				$arr['file_name'] = $ff;
				echo json_encode($arr);

			}
		}

	}

	public function deactivate_member(){
		$id = $_POST['id'];
		$enabled = ($_POST['enabled'] == 'Y') ? 'N' : 'Y';

		$data = array('enabled'=>$enabled);
		$this->master->updateRecord('customers', $data, array('id'=>$id));

		$data['message'] = 'User activate status updated.';
		echo json_encode($data);
	}

	public function googleplus_customer(){
		$data = $_POST['data'];
		$user_id = $this->session->userdata('id');

		$arr = array(
			'google_id'=>$data['id'],
			'google_data'=>serialize($data),
			'google_img'=>$data['avatar']
		);
		$this->master->updateRecord('customers', $arr, array('id'=>$user_id));

		echo json_encode($data);
	}

	public function linkedin_customer(){
		$data = $_POST['data'];
		$user_id = $this->session->userdata('id');

		$arr = array(
			'fb_id'=>$data['id'],
			'fb_data'=>serialize($data),
			'fb_img'=>$data['avatar']
		);
		$this->master->updateRecord('customers', $arr, array('id'=>$user_id));

		echo json_encode($data);
	}



	public function homebiz1(){
		$admin = $this->master->getRecords('admin');
		$biz_referral = (@unserialize($admin[0]['biz_referral'])) ? unserialize($admin[0]['biz_referral']) : array();
		$data = $_POST['data'];
		$uri2 = $_POST['uri2'];
		$input = $data;

		$biz_quote_id = $this->session->userdata('biz_quote_id');
		$biz_quote_id = (!empty($biz_quote_id)) ? $biz_quote_id : '';

		$matching_referral = array();

		$thenum = '1';
		$data = array();
		foreach($input as $r=>$value){
			$thename = explode('_', $value['name']);
			$thenum = $thename[0];

			$bizname = substr($value['name'], 2);

			if(isset($biz_referral[$bizname])){
				if($biz_referral[$bizname] == $value['value']){
					$matching_referral[] = $bizname;
				}
			}

			$thelast_char = substr($value['name'], -2);
			$stripped_name = substr($value['name'], 0, -2);
			if($thelast_char != '[]'){
				$data[$value['name']] = $value['value'];
			}
			else{
				$data[$stripped_name][] = $value['value'];
			}
		}
		$biz1 = $this->session->userdata('homebiz0');
		$biz1 = (!isset($biz1['user_id'])) ? $this->session->userdata('homebiz1') : '';
		$logged_admin = $this->session->userdata('logged_admin_email');


		if(($thenum < 7 && !isset($biz1['user_id'])) || $thenum == 1 || $thenum == 0){
			$this->session->set_userdata('homebiz'.$thenum, $data);
			$data['thenum'] = $thenum + 1;

			if($thenum == 1){
				$data['save_customer_from_quote'] = $this->common->save_customer_from_quote();
			}
			else if($thenum == 6){
				$data['completed_details'] = $this->common->completed_details();

				//logs
				$log_activity = array(
					'name'=>'Quote form completed.',
					'type'=>'quote complete',
					'details'=>serialize($data)
				);
				$this->common->insert_activity_log($log_activity);

			}

		}
		else if($thenum > 6){
			if(!empty($biz_quote_id)){
				$data['renewal'] = $this->common->renew_policy($biz_quote_id);

			}
			else{
				// $data['completed_details'] = $this->common->completed_details();
				//
				// //logs
				// $log_activity = array(
				// 	'name'=>'Quote form completed.',
				// 	'type'=>'quote complete',
				// 	'details'=>serialize($data)
				// );
				// $this->common->insert_activity_log($log_activity);

			}


			$data['thenum'] = 1;
			$this->session->unset_userdata('current_premium');
			$this->session->unset_userdata('biz_quote_id');
			$this->session->unset_userdata('for_renewal');

			$this->common->new_quote_reset();
			// $this->session->unset_userdata('homebiz1');
			// $this->session->unset_userdata('homebiz2');
			// $this->session->unset_userdata('homebiz3');
			// $this->session->unset_userdata('homebiz4');
			// $this->session->unset_userdata('homebiz5');
			// $this->session->unset_userdata('homebiz6');
			// $this->session->unset_userdata('homebiz7');
		}
		else{
			$data['thenum'] = 1;
		}

		//if admin, empty referral match
		$data['matching_referral'] = (!empty($logged_admin)) ? array() : $matching_referral;
		$data['biz_quote_id'] = $biz_quote_id;

		if($this->session->userdata('customer_quote') != '' && $uri2 != 'quote'){
			$data['customer_quote'] = $this->session->userdata('customer_quote');
			$data['cb_id'] = $this->session->userdata('customer_broker_id');
		}

		$data['biz1'] = $biz1;
		echo json_encode($data);
	}


	public function email_save_quote(){
		$action = $_POST['action'];
		$data = $this->common->completed_details($action);

		//logs
		$log_activity = array(
			'name'=>'Quote form '.$action,
			'type'=>'quote '.$action,
			'details'=>serialize($data)
		);
		$this->common->insert_activity_log($log_activity);
		echo json_encode($data);

	}

	public function update_quote_expiry(){
		$base_currency = $_POST['base_currency']; //expiry quote
		$agency_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];

		$arr = array('base_currency'=>$base_currency);
		$this->master->updateRecord('agency', $arr, array('id'=>$agency_id));


		$arr['agency_id'] = $agency_id;

		echo json_encode($arr);

	}

	public function admin_referral(){
		$data = $_POST['data'];
		$input = $data;

		$thenum = '1';
		$data = array();
		foreach($input as $r=>$value){
			$thename = explode('_', $value['name']);
			$thenum = $thename[0];
			if($value['name'] != 'product[]'){
				$data[$value['name']] = $value['value'];
			}
			else{
				$data['product'][] = $value['value'];
			}
		}

		$tosave = array(
			'biz_referral'=>serialize($data)
		);
		$this->master->updateRecord('admin', $tosave, array('id'=>'2'));


		$typee = 'referral_point';
		$save_agency_ratings_log = $this->common->save_agency_ratings_log($typee);




		echo json_encode($data);
	}


	public function referral_required(){
		$user_session = $this->session->all_userdata();

		$customer = $_POST['customer'];
		$data = $_POST['data'];
		$nowtime = date('Y-m-d H:i:s');

		$homebiz_response = $this->common->homebiz_response();
		$needs_referral = (!empty($homebiz_response['homebiz6'])) ? 'N' : 'Y';


		$arr = array(
			'date_added'=>$nowtime,
			'needs_referral'=>$needs_referral,
			'broker_id'=>$user_session['id'],
			'brokerage_id'=>$user_session['brokerage_id'],
			'agency_id'=>$user_session['agency_id'],
			'customer'=>serialize($customer),
			'content'=>serialize($homebiz_response)
		);

		$quote_id = $this->master->insertRecord('biz_referral', $arr, true);

		$sel_customer_id = $homebiz_response['homebiz1']['1_customer_id'];

		//seek to customer
		if($needs_referral == 'Y'){
			//send customer email notification
			$customer_info = $this->common->customer_format($sel_customer_id);

			$email = (isset($customer_info['email'])) ? $customer_info['email'] : $homebiz_response['homebiz0']['email'];
			$name = (isset($customer_info['name'])) ? $customer_info['name'] : $homebiz_response['homebiz0']['company_name'];

			$emailer_file_name = 'seek-instruction-to-customer';
			//admin emailer info
			$adminemail = $this->common->admin_email();

			//email settings
			$info_arr = array(
				'to'=>$email,
				'from'=>$adminemail,
				'subject'=>'Greetings from Ebinder',
				'view'=>$emailer_file_name,
			);


			$other_info = array(
				'password'=>'',
				'emailer_file_name'=>$emailer_file_name,
				'name'=>$name,
				'agency_name'=>$name,
				'user_name'=>$email,
				'user_email'=>$email,
				'link'=>'landing/renew_approval?id='.$quote_id.'&email='.$email.'&cid='.md5($quote_id).'&name='.md5($name)
			);

			$this->emailer->sendmail($info_arr, $other_info);
			$data['other_info'] = $other_info;
			$data['info_arr'] = $info_arr;
		}



		//reset session
		$user_data = $this->session->all_userdata();

		foreach ($user_data as $key => $value) {
			//do not remove admin session
			$this->common->new_quote_reset();

		}

		echo json_encode($data);
	}

	public function email_biz_customer(){
		$user_session = $this->session->all_userdata();

		$customer = array();
		$data = array();
		$nowtime = date('Y-m-d H:i:s');
		//reset session
		$user_data = $this->session->all_userdata();

		$homebiz_response = $this->common->homebiz_response();
		$customer = $homebiz_response['homebiz5'];
		$is_admin = (!empty($user_data['logged_admin_email'])) ? 'Y' : 'N';

		$arr = array(
			'date_added'=>$nowtime,
			'broker_id'=>$user_session['broker_id'],
			'brokerage_id'=>$user_session['brokerage_id'],
			'agency_id'=>$user_session['agency_id'],
			'customer'=>serialize($customer),
			'content'=>serialize($homebiz_response),
			'is_admin'=>$is_admin,
			'needs_referral'=>'N'
		);

		$theid = $this->master->insertRecord('biz_referral', $arr, true);



		if($is_admin == 'Y'){
			$email = $customer['5_email_address'];
			$name = $customer['5_contact_name'];
			$emailer_file_name = 'biz_insurance_to_user';
			//admin emailer info
			$adminemail = $this->common->admin_email();

			//email settings
			$info_arr = array(
				'to'=>$email,
				'from'=>$adminemail,
				'subject'=>'Greetings from Ebinder',
				'view'=>$emailer_file_name,
			);


			$other_info = array(
				'password'=>'',
				'emailer_file_name'=>$emailer_file_name,
				'name'=>$name,
				'agency_name'=>$name,
				'user_name'=>$email,
				'user_email'=>$email,
				'link'=>'panel/quote?id='.$theid
			);

			$this->emailer->sendmail($info_arr, $other_info);
			$data['other_info'] = $other_info;
			$data['info_arr'] = $info_arr;
		}


		foreach ($user_data as $key => $value) {
			//do not remove admin session
			if($key != 'logged_admin' && $key != 'logged_admin_email'){
				$this->session->unset_userdata($key);
			}
		}

		$data['arr'] = $arr;
		echo json_encode($data);

	}

	public function ml_customer_confirm_quote(){
		$id = $_POST['id'];


		$whr = array(
			'id'=>$quote_id
		);
		$quote = $this->common->format_referral($whr);


		$data['renew_approved'] = 'Y';
		$this->master->updateRecord('biz_referral', $data, array('id'=>$id));



		//admin emailer info
		$adminemail = $this->common->admin_email();
		$emailer_file_name = 'policy-renewal-approved-to-broker';

		$email = $this->common->db_field_id('customers', 'email', $quote['broker_id']);
		$name = $this->common->db_field_id('customers', 'name', $quote['broker_id']);

		//email settings
		$info_arr = array(
			'to'=>'nga@spacelli.com',//$arr['email'],
			'from'=>$adminemail,
			'subject'=>'Greetings from Ebinder',
			'view'=>$emailer_file_name,
		);


		$other_info = array(
			'password'=>'',
			'emailer_file_name'=>$emailer_file_name,
			'name'=>'Admin',// $arr['name'],
			'agency_name'=>$name,
			'user_name'=>$email,
			'user_email'=>$email,
			//'details'=>$message,
			'link'=>'webmanager'
		);

		$this->emailer->sendmail($info_arr, $other_info);

		$arr['other_info'] = $other_info;
		$arr['info_arr'] = $info_arr;

		echo json_encode($data);

	}



	public function ml_broker_bind_renew(){
		$user_session = $this->session->all_userdata();


		$id = $_POST['id'];
		$quote_id = $_POST['id'];
		$type = $_POST['type'];
		$nowtime = date('Y-m-d H:i:s');
		$arr['id'] = $id;

		$quote = $this->common->format_referral($arr);

		$homebiz_response = $this->common->homebiz_response();


		// echo json_encode($quote); return false;



		$days = 365;
		$date = $quote['date_bound'];
		$date = strtotime("+".$days." days", strtotime($date));
		$date_bound = date("Y-m-d H:i:s", $date);


		$old_val = $quote['original_value'];
		$old_val['referral_id'] = $old_val['id'];
		unset($old_val['id']);



		$arr['renew_requested'] = 'Y';
		// if(!empty($is_customer)){
		// 	$arr['renew_approved'] = 'Y';
		// }

		if($type == 'accept'){
			$arr['date_renewed'] = $nowtime;
			$arr['date_bound'] = $date_bound;
			$arr['renew_requested'] = 'N';
			$arr['renew_approved'] = 'N';
		}

		$history_id = $this->master->insertRecord('biz_renewal_history',$old_val, true);

		$this->master->updateRecord('biz_referral', $arr, array('id'=>$quote_id));



		//send customer email notification
		$sel_customer_id = $homebiz_response['homebiz1']['1_customer_id'];
		$customer_info = $this->common->customer_format($sel_customer_id);

		$email = (isset($customer_info['email'])) ? $customer_info['email'] : $homebiz_response['homebiz0']['email'];
		$name = (isset($customer_info['name'])) ? $customer_info['name'] : $homebiz_response['homebiz0']['company_name'];

		$emailer_file_name = 'policy-renewal-to-customer';
		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings
		$info_arr = array(
			'to'=>$email,
			'from'=>$adminemail,
			'subject'=>'Greetings from Ebinder',
			'view'=>$emailer_file_name,
		);


		$other_info = array(
			'password'=>'',
			'emailer_file_name'=>$emailer_file_name,
			'name'=>$name,
			'agency_name'=>$name,
			'user_name'=>$email,
			'user_email'=>$email,
			'link'=>'landing/renew_approval?id='.$quote_id.'&email='.$email.'&cid='.md5($quote_id).'&name='.md5($name)
		);

		//send only if request
		if($type != 'accept' && $quote['renew_approved'] == 'N' && empty($is_customer)){
			$this->emailer->sendmail($info_arr, $other_info);
			$arr['other_info'] = $other_info;
			$arr['info_arr'] = $info_arr;
		}


		echo json_encode($arr);


	}

	public function ml_broker_bind(){
		$id = $_POST['id'];
		$nowtime = date('Y-m-d H:i:s');
		$arr['id'] = $id;

		$biz_referral = $this->master->getRecords('biz_referral', $arr);

		$content = (isset($biz_referral[0]['content'])) ? unserialize($biz_referral[0]['content']) : array();

		$sess = array(
			'for_renewal'=>'no',
			'biz_quote_id'=>$id
		);
		$this->session->set_userdata($sess);

		$count = 1;
		foreach($content['themenu'] as $r=>$value){
			if(!empty($value['data'])){
				$this->session->set_userdata('homebiz'.$count, $value['data']);
			}
			$count++;
		}

		$data['bound'] = 'Y';
		$data['date_bound'] = $nowtime;
		$this->master->updateRecord('biz_referral', $data, array('id'=>$id));


		//logs
		$log_activity = array(
			'name'=>'Quote bound.',
			'type'=>'quote bound',
			'details'=>serialize($data)
		);
		$this->common->insert_activity_log($log_activity);




		$data['content'] = $content;
		$data['biz_referral'] = $biz_referral;
		echo json_encode($data);

	}


	public function ml_policy_renew(){
		$id = $_POST['id'];
		$action = $_POST['action'];
		$nowtime = date('Y-m-d H:i:s');
		$arr['id'] = $id;

		$biz_referral = $this->master->getRecords('biz_referral', $arr);

		$content = (isset($biz_referral[0]['content'])) ? unserialize($biz_referral[0]['content']) : array();

		$sess = array(
			'biz_quote_id'=>$id,
			'for_renewal'=>($action == 'renew') ? 'yes' : 'endorse',
		);
		$this->session->set_userdata($sess);

		$count = 1;
		foreach($content['themenu'] as $r=>$value){
			if(!empty($value['data'])){
				$this->session->set_userdata('homebiz'.$count, $value['data']);

				if($count == 5){
					$this->session->set_userdata('current_premium', $value['data']);
				}
			}
			$count++;
		}

//		$data['bound'] = 'Y';
//		$data['date_bound'] = $nowtime;
//		$this->master->updateRecord('biz_referral', $data, array('id'=>$id));


		$data['content'] = $content;
		$data['biz_referral'] = $biz_referral;
		echo json_encode($data);

	}
	public function submit_demo(){
		$data = (isset($_POST['data'])) ? $_POST['data'] : array();

		$arr = array();
		$message = '<p><strong>Request Details</strong></p>';
		foreach($data as $r=>$value){
			$message .= ucfirst($value['name']).': '.$value['value'].' <br/ >';

			$arr[$value['name']] = $value['value'];
		}


		$mess_type = ($arr['type'] == 'Request access') ? 'request_access_message' : 'request_demo_message';

		//get validation message from admin
		$action_message = $this->common->get_message($mess_type);
		$arr['message'] = $action_message['success'];



		//admin emailer info
		$adminemail = $this->common->admin_email();
		$emailer_file_name = 'demo_request_to_admin';

		$to_email = 'nga@ebinder.com.au';

		//check if from subdomain/agency
		$domain = $this->common->the_domain();
		if($domain != ''){
			$agency = $this->master->getRecords('agency', array('domain'=>$domain));

			if(count($agency) > 0){
				$underwriter = $this->master->getRecords('customers', array('agency_id'=>$agency[0]['id'], 'first_underwriter'=>'y'));
				$to_email = (count($underwriter) > 0) ? $underwriter[0]['email'] : $to_email;
			}
			else{
				$message .= 'Domain: '.$domain;
			}
		}


		//email settings
		$info_arr = array(
			'to'=>$to_email,//$arr['email'],
			'from'=>$adminemail,
			'subject'=>'Greetings from Ebinder',
			'view'=>$emailer_file_name,
		);


		$other_info = array(
			'password'=>'',
			'emailer_file_name'=>$emailer_file_name,
			'name'=>'Admin',// $arr['name'],
			'agency_name'=>$arr['name'],
			'user_name'=>$arr['email'],
			'user_email'=>$arr['email'],
			'details'=>$message,
			'link'=>'webmanager'
		);

		if(!empty($data)){
			$this->emailer->sendmail($info_arr, $other_info);
		}

		$arr['other_info'] = $other_info;
		$arr['info_arr'] = $info_arr;
		echo json_encode($arr);

	}

	public function send_sms_code(){
		$id = $_POST['id'];
		$country_id = $_POST['country_id'];
		$calling_code = $this->common->db_field_id('country_t', 'calling_code', $country_id, 'country_id');
		$sms_code = mt_rand(0, 9999);
		$sms_code = sprintf("%04d", $sms_code);

		$phone = $_POST['phone'];
		$phone_trim = ltrim($phone, '0');
		$contact_no = $calling_code.$phone_trim;



		$arr = array(
			'country_id'=>$country_id,
			'calling_code'=>$calling_code,
			'calling_digits'=>$phone,
			'contact_no'=>$contact_no,
			'sms_code'=>$sms_code,
		);


		$this->load->library('nexmo');
		// set response format: xml or json, default json
		$this->nexmo->set_format('json');

		$newphrase = 'test';


		$from = 'Ebinder'; //(isset($sms_data['nodata'])) ? 'Spacelli' : '61477765075';
		$message = array(
			'text' => 'Your Ebinder 4 digit code is: '.$sms_code
		);

		$to = $contact_no;
		$response = $this->nexmo->send_message($from, $to, $message);

		$errtxt = '';
		if($response->messages){
			foreach ($response->messages as $messageinfo) {
				$recipient = $messageinfo->{'to'};
				$status = $messageinfo->{'status'};

				if($status !='0'){
					$errtxt = $messageinfo->{'error-text'};
				}
			}


			if($status == '0'){
				$this->master->updateRecord('customers', $arr, array('id'=>$id));
			}

			$arr['sms_status'] = $status;
			$arr['sms_errtxt'] = $errtxt;
			$arr['message'] = $message;


		}


		echo json_encode($arr);

	}


	public function submit_the_broker(){
		$id = $_POST['id'];
		$email = $_POST['email'];
		$first = $_POST['first'];
		$last = $_POST['last'];
		$first_broker = $_POST['first_broker'];
		$position = (isset($_POST['position'])) ? $_POST['position'] : '';
		$user_session = $this->session->all_userdata();
		$nowtime = date('Y-m-d H:i:s');

		$country_id = $_POST['country_code'];
		$country_short = $_POST['country_short'];

		$mobile = $_POST['mobile'];
		$phone_trim = ltrim($mobile, '0');
		$contact_no = $country_short.$phone_trim;



		if(empty($id)){

			$data = array(
				'first_name'=>$first,
				'last_name'=>$last,
				'email'=>$email,
				'domain'=>$position,
				'first_broker'=>$first_broker,
				'country_id'=>$country_id,
				'calling_code'=>$country_short,
				'calling_digits'=>$mobile,
				'contact_no'=>$contact_no,
				'customer_type'=>'N',
				'brokerage_id'=>$user_session['brokerage_id'],
				'agency_id'=>$user_session['agency_id'],
				'studio_id'=>$user_session['id'],
				'enabled'=>'N',
				'status'=>'N',
				'date_added'=>$nowtime,
			);

			$broker_id = $this->master->insertRecord('customers', $data, true);


			//admin emailer info
			$adminemail = $this->common->admin_email();
			$emailer_file_name = 'signup-mail-to-broker';
			$dbtable = 'customers';

			//email settings
			$info_arr = array(
				'to'=>$email,
				'from'=>$adminemail,
				'subject'=>'Welcome to Transit Insurance',
				'view'=>$emailer_file_name,
			);


			$other_info = array(
				'password'=>'',
				'emailer_file_name'=>$emailer_file_name,
				'name'=>$first. ' '.$last,
				'agency_name'=>$id,
				'user_name'=>$email,
				'user_email'=>$email,
				'link'=>'landing/verify_user/'.$broker_id.'/'.md5($broker_id).'/'.$dbtable.'/broker'
			);


			$this->emailer->sendmail($info_arr, $other_info);

			$data['other_info'] = $other_info;
			$data['info_arr'] = $info_arr;

			//get validation message from admin
			$action_message = $this->common->get_message('generic_add_message');
			$data['message'] = $action_message['success'];



		}
		else{
			$data = array(
				'first_name'=>$first,
				'last_name'=>$last,
				'email'=>$email,
				'domain'=>$position,
				'first_broker'=>$first_broker,
				'country_id'=>$country_id,
				'calling_code'=>$country_short,
				'calling_digits'=>$mobile,
				'contact_no'=>$contact_no,
			);
			$this->master->updateRecord('customers', $data, array('id'=>$id));
			//get validation message from admin
			$action_message = $this->common->get_message('generic_update_message');
			$data['message'] = $action_message['success'];

		}

		echo json_encode($data);

	}

	public function make_first_underwriter(){
		$id = $_POST['id'];
		$to = $_POST['to'];
		$agency_id = $_POST['agency_id'];
		$brokers = $this->master->getRecords('customers', array('first_underwriter'=>'Y', 'agency_id'=>$agency_id));

		if(!empty($brokers)){
			$this->master->updateRecord('customers', array('first_underwriter'=>'N'), array('id'=>$brokers[0]['id']));
		}

		$data = array(
			'first_underwriter'=>$to,

		);
		$this->master->updateRecord('customers', $data, array('id'=>$id));
		$data['message'] = 'First underwriter successfully updated.';

		echo json_encode($data);

	}

	public function save_promocode() {
		$product_name = $_POST['promo_code'];
		$helper = $_POST['discount'];
		$id = $_POST['id'];

		$user_session = $this->session->all_userdata();
		$nowtime = date('Y-m-d H:i:s');


		$msg = 'error';

		$arr = array(
			'promo_code'=>$product_name,
			'discount'=>$helper,
			'broker_id'=>$user_session['id'],
			'agency_id'=>$user_session['agency_id'],
		);

		if($id != ''){
			if($this->master->updateRecord('biz_promocode',$arr, array('id'=>$id))){


				$this->session->set_flashdata('success',' Promo code updated successfully');
				$msg = 'success';
			}
		}
		else{
			$arr['date_added'] = $nowtime;
			$this->master->insertRecord('biz_promocode',$arr);

			$this->session->set_flashdata('success',' Promo code added successfully');
			$msg = 'success';
		}


		$typee = 'promo_code';
		$save_agency_ratings_log = $this->common->save_agency_ratings_log($typee);


		echo json_encode($arr);

	}


	public function renewal_advice(){
		$id = $_POST['id'];

		$arr['id'] = $id;

		$arr['renewal_advice'] = $this->common->renewal_advice($id);

		echo json_encode($arr);

	}

	public function validate_promo(){
		$promo = $_POST['promo'];
		$user_session = $this->session->all_userdata();

		$biz_promocode = $this->master->getRecords('biz_promocode', array('agency_id'=>$user_session['agency_id'], 'promo_code'=>$promo));
		$arr['promo'] = $promo;
		$arr['count'] = count($biz_promocode);
		$arr['code'] = $biz_promocode;
		echo json_encode($arr);
	}

	public function save_widget(){
		$user_session = $this->session->all_userdata();
		$widget = $_POST['widget'];
		$nowtime = date('Y-m-d H:i:s');

		$data = array();


		if(count($widget)){
			foreach($widget as $r=>$value){

				$arr = array(
					// 'referral_id'=>$premium,
					'agency_id'=>(!empty($user_session['agency_id'])) ? $user_session['agency_id'] : 6,
					'broker_id'=>(!empty($user_session['id'])) ? $user_session['id'] : '0',
					'date_added'=>$nowtime
				);

				$arr['type'] = $value;

				if($value == 'premium'){
					$arr['title'] = 'Total bound premium';

				}
				else if($value == 'topbrokers'){
					$arr['title'] = 'Top 5 brokers';
				}
				else if($value == 'topcustomers'){
					$arr['title'] = 'Top 5 customers';
				}

				$data[] = $arr;
				$this->master->insertRecord('q_widget', $arr);

			}
		}


		echo json_encode($data);
	}

	public function remove_widget(){

	}


	public function activity_logged_pass(){
		$user_id = $this->session->userdata('id');
		$password = $_POST['password'];

		$upass = '';
		if(empty($user_id)){
			$brokers = $this->master->getRecords('admin');
			$upass = $brokers[0]['password'];
		}
		else{
			$brokers = $this->master->getRecords('customers', array('id'=>$user_id));
			$upass = $brokers[0]['password'];
		}


		$arr['result'] = 'notmatch';
		if(md5($password) == $upass){
			$arr['result'] = 'match';
			$this->session->set_userdata('activity_logged', 'Y');
		}

		echo json_encode($arr);
	}

	public function upload_file(){



		$output_dir = "uploads/message-uploads/";


		if(isset($_FILES["myfile"]))
		{
			//Filter the file types , if you want.
			if ($_FILES["myfile"]["error"] > 0)
			{
			  echo '<a href="javascript: ;" onclick="$(this).hide();" class="text-danger" id="error_up"><i class="fa fa-warning"></i> Error Uploading File</a>';//$_FILES["file"]["error"] . "<br>";
			}
			else
			{
				$thefilename = uniqid().str_replace(' ', '_', $_FILES["myfile"]["name"]);

				//move the uploaded file to uploads folder;
				move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$thefilename); //$_FILES["myfile"]["name"]);


					$insrt_arr = array (
						'file_upload_name'=>$thefilename, //$_FILES["myfile"]["name"],
						'seeker_id'=>$this->session->userdata('id'),
						'temp_id'=>$_POST['uniquetime'],
						'message_id'=>'0' //$cc_case_id
					);
					if ($file_id = $this->master->insertRecord('claims_message_file',$insrt_arr,true)){

						echo ' <a href="'.base_url().'uploads/message-uploads/'.$thefilename.'" target="_blank" id = "file_del'.$file_id.'" class="file_attch_btn"><i class="fa fa-paperclip"></i>'.$_FILES["myfile"]["name"].'</a> <span onclick="$(\'#file_del'.$file_id.'\').hide();$(this).hide();delete_the_file(event.target.id);" style="cursor: default" id="delete_file'.$file_id.'">&times;</span>';

					}
					else{
					  echo '<a href="javascript: ;" onclick="$(this).hide();" class="text-danger" id="error_up"><i class="fa fa-warning"></i> Error Uploading File</a>';//$_FILES["file"]["error"] . "<br>";
					}
			}

		}


	}


	public function delete_message_file(){

		$thenum = $_POST['thenum'];

	    $file = $this->master->getRecords('claims_message_file',array('file_upload_id'=>$thenum));

		$file_name = $file[0]['file_upload_name'];

		if($this->master->deleteRecord('claims_message_file','file_upload_id',$thenum)) {

			@unlink('uploads/message-uploads/'.$file_name);
			//$this->session->set_flashdata('success',' Document successfully deleted.');
			echo 'success';

		}


	}

	public function save_claim(){
		$user_session = $this->session->all_userdata();

		//claim=asdf&customer_id=4&policy_id=36&details=dsf&uniquetime=1476958615721348103

		$claim = $_POST['claim'];
		$customer_id = $_POST['customer_id'];
		$policy_id = $_POST['policy_id'];
		$details = $_POST['details'];
		$broker_id = $user_session['id'];
		$agency_id = $user_session['agency_id'];
		$uniquetime = $_POST['uniquetime'];
		$nowtime = date('Y-m-d H:i:s');

		$arr = array(
			'date_added'=>$nowtime,
			'claim'=>$claim,
			'customer_id'=>$customer_id,
			'policy_id'=>$policy_id,
			'details'=>$details,
			'broker_id'=>$broker_id,
			'agency_id'=>$agency_id
		);

		$id = $this->master->insertRecord('claims', $arr, true);

		//update attached file id
		$this->master->updateRecord('claims_message_file',array('message_id'=>$id), array('temp_id'=>$uniquetime));
		$whr_first = array(
			'customer_type'=>'N',
			'first_underwriter'=>'Y',
			'agency_id'=>$agency_id,
		);
		$first_under = $this->master->getRecords('customers', $whr_first, '*', array('first_underwriter'=>'ASC'));
		$arr['first_under'] = $first_under;

		if(!empty($first_under)){
			foreach($first_under as $r=>$value){

				//admin emailer info
				$adminemail = $this->common->admin_email();
				$emailer_file_name = 'new-claim-to-underwriter';
				$email_name = $value['first_name']; //$first_name.' '.$last_name;
				$email = $value['email'];
				//email settings
				$info_arr = array(
					'to'=>$email,
					'from'=>$adminemail,
					'subject'=>'Greetings from Ebinder',
					'view'=>$emailer_file_name,
				);


				$other_info = array(
					'password'=>'',
					'emailer_file_name'=>$emailer_file_name,
					'name'=>$email_name,
					'agency_name'=>'',
					'user_name'=>$email,
					'user_email'=>$email,
					'link'=>'#login'
				);

				$this->emailer->sendmail($info_arr, $other_info);

				$arr['other_info'][] = $other_info;
				$arr['info_arr'][] = $info_arr;
			}

		}

		$arr['id'] = $id;


		$log_activity = array(
			'name'=>'New claim was submitted.',
			'type'=>'claim_submitted',
			'details'=>serialize($arr)
		);
		$this->common->insert_activity_log($log_activity);


		echo json_encode($arr);
	}


	public function claim_submit_respond(){
		$message_id = $_POST['message_id'];
		$id = $_POST['id'];
		$to = $_POST['to'];
		$text = $_POST['text'];
		$nowtime = date('Y-m-d H:i:s');

		$to_user = $this->common->customer_format($to);

		$arr = array (
			'message'=>$text, //$_FILES["myfile"]["name"],
			'seeker_id'=>$id,
			'message_id'=>$message_id //$cc_case_id
		);


		$this->master->insertRecord('claims_message_file', $arr);
		$arr['date'] = date_format(date_create($nowtime), 'd M Y, h:i:s A');


		//admin emailer info
		$adminemail = $this->common->admin_email();
		$emailer_file_name = 'claim-new-message';
		$email_name = $to_user['name']; //$first_name.' '.$last_name;
		$email = $to_user['email'];
		//email settings
		$info_arr = array(
			'to'=>$email,
			'from'=>$adminemail,
			'subject'=>'Greetings from Ebinder',
			'view'=>$emailer_file_name,
		);

		$other_info = array(
			'password'=>'',
			'emailer_file_name'=>$emailer_file_name,
			'name'=>$email_name,
			'agency_name'=>'',
			'user_name'=>$email,
			'user_email'=>$email,
			'link'=>'#login'
		);

		$this->emailer->sendmail($info_arr, $other_info);


		$arr['other_info'] = $other_info;
		$arr['info_arr'] = $info_arr;


		$log_activity = array(
			'name'=>'Claim respond added',
			'type'=>'claim_respond',
			'details'=>serialize($other_info)
		);
		$this->common->insert_activity_log($log_activity);


		echo json_encode($arr);
	}

	public function claim_change_status(){
		$id = $_POST['id'];
		$text = $_POST['text'];
		$stat = $_POST['stat'];

		$arr = array(
			'status'=>$stat,
		);

		$this->session->set_flashdata('success', 'Claim status successfully updated to '.$text);

		$this->master->updateRecord('claims', $arr, array('id'=>$id));


		$claim = $this->master->getRecords('claims', array('id'=>$id));
		$to_user = $this->common->customer_format($claim[0]['broker_id']);

		//admin emailer info
		$adminemail = $this->common->admin_email();
		$emailer_file_name = 'claim-new-message';
		$email_name = $to_user['name']; //$first_name.' '.$last_name;
		$email = $to_user['email'];
		//email settings
		$info_arr = array(
			'to'=>$email,
			'from'=>$adminemail,
			'subject'=>'Greetings from Ebinder',
			'view'=>$emailer_file_name,
		);

		$other_info = array(
			'password'=>'',
			'emailer_file_name'=>$emailer_file_name,
			'name'=>$email_name,
			'agency_name'=>'',
			'user_name'=>$email,
			'user_email'=>$email,
			'link'=>'#login'
		);

		$this->emailer->sendmail($info_arr, $other_info);


		$arr['other_info'] = $other_info;
		$arr['info_arr'] = $info_arr;

		$arr['id'] = $id;

		$log_activity = array(
			'name'=>'Claim status updated to '.$text,
			'type'=>'claim_status',
			'details'=>serialize($arr)
		);
		$this->common->insert_activity_log($log_activity);

		echo json_encode($arr);

	}

	public function select_agency(){

		$user_session = $this->session->all_userdata();
		$agency = $_POST['agency'];

		$matching_user = $this->master->getRecords('customers', array('agency_id'=>$agency, 'email'=>$user_session['email']));

		$the_customer = $this->common->customer_format($matching_user[0]['id']);

		$arr['userdata'] = $the_customer;

		$this->session->set_userdata($the_customer);

		echo json_encode($arr);

	}

	public function prod_questionnaire(){
		$product_name = $_POST['product_name'];
		$helper = $_POST['helper'];
		$tab = $_POST['tab'];
		$id = $_POST['id'];
		$broker_id = $_POST['broker_id'];


		$nowtime = date('Y-m-d H:i:s');
		$msg = 'error';

		$arr = array(
			'question'=>$product_name,
			'tooltip'=>$helper,
			'referral_custom'=>'Y',
			'tab'=>$tab,
			'broker_id'=>$broker_id,
		);

		if($id != ''){
			if($this->master->updateRecord('biz_prod_questionnaire',$arr, array('id'=>$id))){
				$this->session->set_flashdata('success',' Referral Point updated successfully');
				$msg = 'success';
			}
		}
		else{
			$arr['date_added'] = $nowtime;
			$this->master->insertRecord('biz_prod_questionnaire',$arr);

			$this->session->set_flashdata('success',' Referral Point added successfully');
			$msg = 'success';
		}

		echo json_encode($arr);

	}

	public function brokerage_whitelabel(){
		$id = $_POST['id'];
		$white = $_POST['white'];
		$to = ($white == 'Y') ? 'N' : 'Y';

		$arr = array(
			'enable_whitelabel'=>$to
		);

		$this->master->updateRecord('brokerage',$arr, array('id'=>$id));
		$this->session->set_flashdata('success',' Updated white labelling updated successfully');
		echo json_encode($arr);

	}

	public function send_broker_white_link(){
		$id = $_POST['id'];
		$arr = array(
			'id'=>$id
		);


		$the_customer = $this->common->customer_format($id);
		$emailer_file_name = 'white-label-link-to-broker';
		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings
		$info_arr=array(
			'to'=>$the_customer['email'],
			'from'=>$adminemail,
			'subject'=>'Greetings from Ebinder',
			'view'=>$emailer_file_name,
			'emailer_file_name'=>$emailer_file_name,
		);


		$other_info = array(
			'password'=>'',
			'view'=>$emailer_file_name,
			'emailer_file_name'=>$emailer_file_name,
			'name'=>$this->common->customer_name($the_customer['id']),
			'user_name'=>$the_customer['email'],
			'user_email'=>$the_customer['email'],
			'details'=>base_url().'landing/brokerform/'.$id,
			'link'=>'landing/brokerform/'.$id
		);

		$this->emailer->sendmail($info_arr,$other_info);

		$arr['other_info'] = $other_info;
		$arr['info_arr'] = $info_arr;

		echo json_encode($arr);

	}

	public function assign_underwriter_product(){
		$underwriter = $_POST['underwriter'];
		$agency_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];

		$arr = array(
			'assigned_underwriter'=>$underwriter
		);
		$this->master->updateRecord('agency', $arr, array('id'=>$agency_id));

		$typee = 'assign_underwriter';
		$save_agency_ratings_log = $this->common->save_agency_ratings_log($typee);

		$nowtime = date('d-m-Y');
		$arr['nowtime'] = $nowtime;

		echo json_encode($arr);
	}


	public function rating_product_update(){
		$product_id = $_POST['product_id'];
		$agency_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];
		$agency_info = $this->master->getRecords('agency', array('id'=>$agency_id));
		$agency_details = (@unserialize($agency_info[0]['agency_details'])) ? unserialize($agency_info[0]['agency_details']) : array();

		$agency_details['product_id'] = $product_id;

		$arr = array(
			'agency_details'=>serialize($agency_details)
		);

		$this->master->updateRecord('agency', $arr, array('id'=>$agency_id));
		$this->session->set_flashdata('success', 'Products successfully updated.');
		echo json_encode($arr);
	}


	public function abn_fetch(){

		$txt = $_POST['txt'];
		$url = 'http://abr.business.gov.au/json/AbnDetails.aspx?abn='.$txt;
		$url .= '&guid=c0f669dd-dced-416d-9859-5f565c8d62bc';

		$json = file_get_contents($url);

		$objct = str_replace('callback(', '', $json);
		$objct = str_replace(')', '', $objct);

		$obj = json_decode($objct);


		$mess = 'Unable to connect to ABN lookup API service.';
		if(is_object($obj)){
			if($obj->Message){
				$mess = $obj->Message;
			}
			else{
				$mess = 'Entity Type Name: '.$obj->EntityTypeName;
				$mess .= '<br>Entity Name: '.$obj->EntityName;

			}
		}

		$arr = array(
			'message'=>$mess
		);

		echo json_encode($arr);
	}



	public function reset_customer2_form(){
		$this->session->unset_userdata('customer_details');

		$arr = array(
			'message'=>'reset'
		);
		echo json_encode($arr);

	}


	public function add_customer2(){


			$admin = $this->master->getRecords('admin');
			$biz_referral = (@unserialize($admin[0]['biz_referral'])) ? unserialize($admin[0]['biz_referral']) : array();
			$data = $_POST['data'];
			$input = $data;

			$biz_quote_id = $this->session->userdata('biz_quote_id');

			$logged_admin = $this->session->userdata('logged_admin_email');

			$biz1 = $this->session->userdata('customer_details');

			$data = (!empty($biz1)) ? $biz1 : array() ;



			//reset array form fields
			foreach($input as $r=>$value){
				$thelast_char = substr($value['name'], -2);
				$stripped_name = substr($value['name'], 0, -2);
				if($thelast_char == '[]'){
					$data[$stripped_name] = array();
				}
			}




			foreach($input as $r=>$value){

				$thelast_char = substr($value['name'], -2);
				$stripped_name = substr($value['name'], 0, -2);
				if($thelast_char != '[]'){
					$data[$value['name']] = $value['value'];
				}
				else{
					$data[$stripped_name][] = $value['value'];
				}
			}

			$thenum = (isset($data['thenum'])) ? $data['thenum'] : '1';



			if(($thenum < 5 && !empty($biz1)) || $thenum == 1){
				$this->session->set_userdata('customer_details', $data);
				$data['thenum_next'] = $thenum + 1;


				if($thenum == 4){
					$data['completed_details'] = array(); //$this->common->completed_details();

				}

			}
			else if($thenum > 4){
				$this->session->set_userdata('customer_details', $data);
				$data['save_customer'] = $this->common->save_customer();
				$data['thenum_next'] = 1;
				$this->session->unset_userdata('customer_details');
			}
			else{
				$thenum = 1;
			}


			echo json_encode($data);

	}

	public function customer2_to_session(){
		$data = $_POST['data'];
		$customer_info = $data['customer_info'];
		$customer_info['id'] = $data['id'];


		$this->session->set_userdata('customer_details', $customer_info);
		echo json_encode($data);

	}


	public function add_customer2x(){

		$user_session = $this->session->all_userdata();
		$user_id = (empty($user_session['id'])) ? '0' : $user_session['id'];

		$customer_details = $_POST['data'];
		$data = array();
		foreach($customer_details as $r=>$value){

			$thelast_char = substr($value['name'], -2);
			$stripped_name = substr($value['name'], 0, -2);
			if($thelast_char != '[]'){
				$data[$value['name']] = $value['value'];
			}
			else{
				$data[$stripped_name][] = $value['value'];
			}
		}


		$id = $data['id'];
		$studio_id = $data['broker_id']; //broker id for customers
		$agency_id = (isset($data['agency_id'])) ? $data['agency_id'] : $this->session->userdata('logged_admin_agency');
		$agency_id = (!empty($agency_id)) ? $agency_id : $this->session->userdata('logged_admin_agency');
		$is_agency = (isset($data['is_agency'])) ? 'yes' : '';
		$access_rights = (isset($data['access_rights'])) ? $data['access_rights'] : '';




		$first = $data['first'];
		$last = $data['last'];
		$email = $data['email'];


		$country_id = $data['country_code'];
		$country_short = $data['country_short'];

		$mobile = $_POST['mobile'];
		$phone_trim = ltrim($mobile, '0');
		$contact_no = $country_short.$phone_trim;


		$nowtime = date('Y-m-d H:i:s');


		if($id == ''){


			$broker = array(
				'first_name'=>$data['first'],
				'last_name'=>$data['last'],
				'email'=>$data['email'],
				'customer_type'=>$data['ctype'],
				'country_id'=>$country_id,
				'calling_code'=>$country_short,
				'calling_digits'=>$mobile,
				'contact_no'=>$contact_no,
				'location'=>$data['address'],
				'lat'=>$data['lat'],
				'lng'=>$data['lng'],
				'studio_id'=>$studio_id,
				'brokerage_id'=>$data['brokerage_id'],
				'customer_info'=>serialize($data),
				'agency_id'=>$this->common->default_cargo(),
				'status'=>'N'
			);



			$customer_id = $this->master->insertRecord('customers', $broker, true);


			//admin emailer info
			$adminemail = $this->common->admin_email();
			$emailer_file_name = 'signup-mail-to-broker';
			$dbtable = 'customers';

			//email settings
			$info_arr = array(
				'to'=>$email,
				'from'=>$adminemail,
				'subject'=>'Welcome to Transit Insurance',
				'view'=>$emailer_file_name,
			);


			$other_info = array(
				'password'=>'',
				'emailer_file_name'=>$emailer_file_name,
				'name'=>$first. ' '.$last,
				'agency_name'=>$id,
				'user_name'=>$email,
				'user_email'=>$email,
				'link'=>'landing/verify_user/'.$customer_id.'/'.md5($customer_id).'/'.$dbtable.'/broker'
			);

			//$this->emailer->sendmail($info_arr, $other_info);

			$data['other_info'] = $other_info;
			$data['info_arr'] = $info_arr;


			$success_mess = 'Customer details successfully added.';
			$this->session->set_flashdata('success',$success_mess);
			$data['result'] = 'ok';
			$data['action'] = 'add';


		}

		else{

			$broker = array(
				'first_name'=>$data['first'],
				'last_name'=>$data['last'],
				'country_id'=>$country_id,
				'calling_code'=>$country_short,
				'calling_digits'=>$mobile,
				'contact_no'=>$contact_no,
				'location'=>$data['address'],
				'customer_info'=>serialize($data),
				'lat'=>$data['lat'],
				'lng'=>$data['lng']
			);

			$this->master->updateRecord('customers', $broker, array('id'=>$data['id']));

			$success_mess = 'Customer details successfully updated.';
			$this->session->set_flashdata('success',$success_mess);
			$data['result'] = 'ok';
			$data['action'] = 'update';
		}


		echo json_encode($data);


	}


	public function customer_send_access_code(){
		$to = $_POST['number'];
		$sms_code = mt_rand(0, 9999);
		$sms_code = sprintf("%04d", $sms_code);

		$this->session->set_userdata('customer_sms_code', $sms_code);


		$this->load->library('nexmo');
		// set response format: xml or json, default json
		$this->nexmo->set_format('json');

		$newphrase = 'test';


		$from = 'Ebinder'; //(isset($sms_data['nodata'])) ? 'Spacelli' : '61477765075';
		$message = array(
			'text' => 'Your Ebinder 4 digit code is: '.$sms_code
		);

		$response = $this->nexmo->send_message($from, $to, $message);

		$errtxt = '';
		if($response->messages){
			foreach ($response->messages as $messageinfo) {
				$recipient = $messageinfo->{'to'};
				$status = $messageinfo->{'status'};

				if($status !='0'){
					$errtxt = $messageinfo->{'error-text'};
				}
			}


			$arr['sms_status'] = $status;
			$arr['sms_errtxt'] = $errtxt;
			$arr['message'] = $message;


		}


		echo json_encode($arr);
	}


	public function customer_mobile_ok(){
		$phonecode = $_POST['phonecode'];

		$thecode = '';
		foreach($phonecode as $r=>$value){
			$thecode .= $value;
		}

		$arr = array(
			'code'=>$thecode
		);

		echo json_encode($arr);
	}

	public function new_quote_reset(){
		$prod_id = $_POST['prod_id'];

		$this->session->set_userdata('quote_product_id', $prod_id);
		$this->common->new_quote_reset();

		echo json_encode(array('result'=>'ok'));

	}
	public function claim_assign_user(){
		$customer_id = $_POST['customer_id'];
		$id = $_POST['id'];

		$arr = array(
			'assigned_user'=>$customer_id
		);

		$this->master->updateRecord('claims', $arr, array('id'=>$id));


		//get validation message from admin
		$action_message = $this->common->get_message('assign_user_form_claim');

		$arr['message'] = $action_message['success'];
		$this->session->set_flashdata('success', $arr['message']);

		echo json_encode($arr);

	}



	public function note_referral_form(){
		$note = $_POST['note'];
		$id = $_POST['id'];

		$arr = array(
			'referral_notes'=>$note
		);

		$this->master->updateRecord('biz_referral', $arr, array('id'=>$id));


		//get validation message from admin
		$action_message = $this->common->get_message('generic_add_message');

		$arr['message'] = $action_message['success'];
		$this->session->set_flashdata('success', $arr['message']);

		echo json_encode($arr);

	}


	public function sidenav_settings(){
		$status = $_POST['status'];

		$arr = array(
			'status'=>$status
		);

		$this->session->set_userdata('sidenav_settings', $status);

		echo json_encode($arr);

	}

	public function endorse_doc_form(){
		$id = $_POST['id'];
		$product_id = $_POST['product_id'];
		$title = $_POST['title'];
		$description = $_POST['description'];
		$reference = $_POST['reference'];

		$user_session = $this->session->all_userdata();
		$nowtime = date('Y-m-d H:i:s');


		$arr = array(
			'product_id'=>$product_id,
			'title'=>$title,
			'doc_type'=>'1',
			'product_id'=>$product_id,
			'reference'=>$reference,
			'broker_id'=>$user_session['id'],
			'agency_id'=>$user_session['logged_admin_agency'],
			'description'=>$description,
		);

		if(empty($id)){
			$arr['date_added'] = $nowtime;

			$this->master->insertRecord('policy_docs', $arr);
			$action_message = $this->common->get_message('generic_add_message');

		}
		else{
			$this->master->updateRecord('policy_docs', $arr, array('id'=>$id));
			$action_message = $this->common->get_message('generic_update_message');

		}

		$arr['message'] = $action_message['success'];
		$this->session->set_flashdata('success', $arr['message']);

		echo json_encode($arr);

	}

	public function policy_doc_form(){
		$id = $_POST['id'];
		$product_id = $_POST['product_id'];
		$reference = $_POST['reference'];

		$user_session = $this->session->all_userdata();
		$nowtime = date('Y-m-d H:i:s');

		$arr = array(
			'product_id'=>$product_id,
			'reference'=>$reference,
			'doc_type'=>'0',
			'product_id'=>$product_id,
			'broker_id'=>$user_session['id'],
			'agency_id'=>$user_session['logged_admin_agency'],
		);
		$arr['date_added'] = $nowtime;

		$this->master->updateRecord('policy_docs', $arr, array('id'=>$id));

		$action_message = $this->common->get_message('generic_add_messagex');
		$arr['message'] = $action_message['success'];
		$this->session->set_flashdata('success', $arr['message']);

		echo json_encode($arr);

	}

	public function check_session(){
		$user_data = $this->session->all_userdata();
		echo json_encode($user_data);

	}

	public function sort_faq(){
		$items = $_POST['item'];
		$table = $_POST['table'];

		$i = 1;

		foreach ($items as $value) {
		    $this->master->updateRecord($table, array('sort'=>$i), array('id'=>$value));
		    $i++;
		}
		echo json_encode($items);

	}

}
