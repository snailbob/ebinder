<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$user_session = $this->session->all_userdata();
		
		if(empty($user_session['id'])){
			redirect(base_url().'login');
		}
		else if($user_session['customer_type'] != 'Y'){
			redirect(base_url().'panel');
		}
	
	}	
	
	
	/***************************
		index
	***************************/
	public function index(){
		$user_session = $this->session->all_userdata();
	
		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');
	
		$id = $user_session['id'];
		$broker = $this->common->brokers($id);
		$base_link = $this->common->base_url();
		$data = array(
			'view'=>'dashboard',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'title'=>'Customer Panel',
			'singular'=>'Customer Panel',
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'base_link'=>$base_link
		);
		
		
//		echo json_encode($data);
//		return false;
		$this->load->view('domains/view', $data);
	}
	

	/***************************
		customers
	***************************/
	public function customers(){
		$user_session = $this->session->all_userdata();
	
		$type = $this->uri->segment(3);
	
		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);
		$customers = $this->common->customers($id);
		$q_products = $this->master->getRecords('q_products');

		$base_link = $this->common->base_url();
		$data = array(
			'view'=> 'customers_profile',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'customers'=>$customers,
			'q_products'=>$q_products,
			'title'=> 'My Information',
			'singular'=>'My Information',
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'base_link'=>$base_link
		);
		
		$thepage = (isset($_GET['tab'])) ? $_GET['tab'] : '1';
		
		
		//for tab2, check if have added info
		if($thepage == '2'){
			$data['view'] = 'customer_tab2';
		}
		
		
		$data['bizcat'] = $this->master->getRecords('q_business_categories');
		$data['countries'] = $this->common->country_format();
		
//		echo json_encode($data);
//		return false;
		$this->load->view('domains/view', $data);
	}
	

	

	/***************************
		settings
	***************************/
	public function settings(){

		$user_session = $this->session->all_userdata();
	
		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');
	
		$id = $user_session['id'];
		$broker = $this->common->brokers($id);
		$base_link = $this->common->base_url();
		$data = array(
			'view'=>'customer_settings',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'title'=>'Customer Settings',
			'singular'=>'Customer Settings',
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'base_link'=>$base_link
		);
		
		
//		echo json_encode($data);
//		return false;
		$this->load->view('domains/view', $data);
		
	}
	
	
	/***************************
		logout
	***************************/
	public function logout(){
		$user_data = $this->session->all_userdata();
	
		foreach ($user_data as $key => $value) {
			//do not remove admin session
			if($key != 'logged_admin' && $key != 'logged_admin_email'){
				$this->session->unset_userdata($key);
			}
		}		
		
		redirect(base_url().'login');

	}
	

}
