
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use Abraham\TwitterOAuth\TwitterOAuth;

class Twitter extends CI_Controller{


	public function __construct(){

		parent::__construct();

	}

  public function index(){
		$user_id = (isset($_GET['id'])) ? $_GET['id'] : '';
		$access_keys = $this->common->twitter_access();


		// create TwitterOAuth object
		$twitteroauth = new TwitterOAuth($access_keys['consumer_key'], $access_keys['consumer_secret']);

		// request token of application
		$request_token = $twitteroauth->oauth(
		    'oauth/request_token', [
		        'oauth_callback' => $access_keys['url_callback']
		    ]
		);

		// throw exception if something gone wrong
		if($twitteroauth->getLastHttpCode() != 200) {
		    throw new \Exception('There was a problem performing this request');
		}

		// save token of application to session
		$to_session['twitter_user_id'] = $user_id;
		$to_session['oauth_token'] = $request_token['oauth_token'];
		$to_session['oauth_token_secret'] = $request_token['oauth_token_secret'];

		$this->session->set_userdata($to_session);
		// generate the URL to make request to authorize our application
		$url = $twitteroauth->url(
		    'oauth/authorize', [
		        'oauth_token' => $request_token['oauth_token']
		    ]
		);

		// and redirect
		redirect($url);
  }

	public function login(){

	}

	public function callback(){

		$oauth_verifier = filter_input(INPUT_GET, 'oauth_verifier');


		$access_keys = $this->common->twitter_access();
		$oauth_token = $this->session->userdata('oauth_token');
		$oauth_token_secret = $this->session->userdata('oauth_token_secret');

		if (empty($oauth_verifier) || empty($oauth_token) || empty($oauth_token_secret)) {
		    // something's missing, go and login again
		    $this->session->set_flashdata('info', 'User not found.');
		    redirect($access_keys['url_login']);
		}
		else{


			// connect with application token
			$connection = new TwitterOAuth(
			    $access_keys['consumer_key'],
			    $access_keys['consumer_secret'],
			    $oauth_token,
			    $oauth_token_secret
			);

			// request user token
			$token = $connection->oauth(
			    'oauth/access_token', [
			        'oauth_verifier' => $oauth_verifier
			    ]
			);

			$twitter = new TwitterOAuth(
			    $access_keys['consumer_key'],
			    $access_keys['consumer_secret'],
			    $token['oauth_token'],
			    $token['oauth_token_secret']
			);

			// Let's get the user's info
			$user_info = $twitter->get('account/verify_credentials');

			// get user's id
			$twitter_id = $user_info->id;
			$logged_id = $this->session->userdata('id');
			$twitter_user_id = $this->session->userdata('twitter_user_id');


			if(empty($logged_id)){
				//login if match
				$user = $this->master->getRecords('customers', array('customer_type'=>'N', 'enabled'=>'Y', 'twitter_id'=>$twitter_id));
				$user_signup = $this->master->getRecords('customers', array('id'=>$twitter_user_id));

				//signup
				if(count($user_signup) > 0){
					$twitter_data = array(
						'twitter_id'=>$twitter_id,
						'twitter_data'=>serialize($token)
					);

					$user = $this->master->updateRecord('customers', $twitter_data, array('id'=>$twitter_user_id));

					//get validation message from admin
					$action_message = $this->common->get_message('complete_profile_via_social');
					$message = $action_message['success'];
					$this->session->set_flashdata('success', $message);


					redirect('signup?id='.$twitter_user_id.'&activation=yes&type=broker&cid='.md5($twitter_user_id));

				}
				//login
				else if(count($user) > 0){
					$the_customer = $this->common->customer_format($user[0]['id']);

					//check if underwriter
					if($the_customer['super_admin'] == 'Y'){
						$this->session->set_userdata($the_customer);
						redirect('webmanager/dashboard');
					}
					//check if broker have active agency
					else if(count($the_customer['my_agencies']) > 0){
						$this->session->set_userdata($the_customer);
						redirect('panel');
					}

					//if no matching user
					else{
						$this->session->set_flashdata('info', 'User not found.');
				    redirect($access_keys['url_login']);
					}
				}
				else{
					$this->session->set_flashdata('info', 'User not found.');
			    redirect($access_keys['url_login']);
				}
			}
			else{
				$twitter_data = array(
					'twitter_id'=>$twitter_id,
					'twitter_data'=>serialize($token)
				);
				$user = $this->master->updateRecord('customers', $twitter_data, array('id'=>$logged_id));
				redirect('settings/social_login');
			}

			// $this->session->set_userdata('twitter_token',$token);
			// echo '<br><br>'; print_r($token);
		}

	}
}
