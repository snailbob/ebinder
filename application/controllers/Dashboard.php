<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
	}	
	
	/***************************
		index
	***************************/
	public function index(){
//		if($this->session->userdata('id') == ''){
//			redirect(base_url());
//		}
		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');
		$address = $this->session->userdata('address');
		$stripe_id = $this->session->userdata('stripe_id');
		
		$policy_docs = $this->master->getRecords('policy_docs','','*',array('id'=>'DESC'));
		$insurances = array();
		
		if($type == 'customer'){
			$insurances = $this->master->getRecords('bought_insurance', array('customer_id'=>$user_id));
		}
		
		

		$data = array(
			'view'=>'dashboard_view',
			'title'=>'Dashboard',
			'insurances'=>$insurances,
			'address'=>$address,
			'policy_docs'=>$policy_docs,
			'stripe_id'=>$stripe_id,
		);
		
		$this->load->view('includes/user_view', $data);
		
	}
	

	/*-------------------------------------
		referrals
	-------------------------------------*/
	public function referrals(){
		if($this->session->userdata('id') == ''){
			redirect(base_url());
		}

		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');

		//seekers
		$insurances = $this->master->getRecords('bought_insurance', array('customer_id'=>$user_id, 'status !='=>'P'), '', array('status'=>'DESC'));

		$data = array(
			'title'=>'Policy Referrals',
			'view'=>'manage-referrals',
			'insurances'=>$insurances

		);
		
		$this->load->view('includes/user_view', $data);

	}

	/*-------------------------------------
		transactions
	-------------------------------------*/
	public function transactions(){
		if($this->session->userdata('id') == ''){
			redirect(base_url());
		}

		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');

		//seekers
		$insurances = $this->master->getRecords('bought_insurance', array('customer_id'=>$user_id, 'status'=>'P'), '', array('status'=>'DESC'));

		$data = array(
			'title'=>'Transactions',
			'view'=>'manage-transactions',
			'insurances'=>$insurances

		);
		
		$this->load->view('includes/user_view', $data);

	}

	/***************************
		buy
	***************************/
	public function buy(){
		if($this->session->userdata('id') == ''){
			redirect(base_url());
		}

		$cargos = $this->master->getRecords('cargo_category');
		$transportations = $this->common->transportations();
		$countries = $this->master->getRecords('country_t');
		$currencies = $this->master->getRecords('api_currencies', array('in_stripe'=>'Y'));

		$admin_info = $this->master->getRecords('admin');
		$the_agency = $this->master->getRecords('agency', array('id'=>$admin_info[0]['default_cargo']));
		$deductibles = $this->master->getRecords('deductibles', array('agency_id'=>$admin_info[0]['default_cargo']), '', array('not_default'=>'DESC'));
		$default_ded = array(
			'rate'=>'1',
			'deductible'=>'1'
		);
		if(count($deductibles) > 0){
			$default_ded['rate'] = $deductibles[0]['rate'];
			//$deductibles[0]['deductible'] = str_replace(',','', $deductibles[0]['deductible']);
		}
	
		$minimum_premium = $this->common->agency_minimum_premium($admin_info[0]['default_cargo']);
		$max_insured = $this->common->agency_max_insured($admin_info[0]['default_cargo']);
		$agency_id = '0';
		if(count($the_agency) > 0){
			$agency_id = $the_agency[0]['id'];
		}
		
		$policy_docs = $this->master->getRecords('policy_docs','','*',array('id'=>'DESC'));
		
		$cargo_prices = $this->common->agency_prices($agency_id,'cargo_prices');
		$cargo_max = $this->common->agency_prices($agency_id,'cargo_max');
		$transportation_prices = $this->common->agency_prices($agency_id,'transportation_prices');
		$country_prices = $this->common->agency_prices($agency_id,'country_prices');
		$country_referral = $this->common->agency_prices($agency_id,'country_referral');
		$zone_multiplier = $this->common->agency_prices($agency_id,'zone_multiplier');

		$data = array(
			'cargos'=>$cargos,
			'transportations'=>$transportations,
			'countries'=>$countries,
			'currencies'=>$currencies,
			'cargo_prices'=>$cargo_prices,
			'cargo_max'=>$cargo_max,
			'transportation_prices'=>$transportation_prices,
			'country_prices'=>$country_prices,
			'country_referral'=>$country_referral,
			'zone_multiplier'=>$zone_multiplier,
			'the_agency'=>$the_agency,
			'deductibles'=>$deductibles,
			'default_ded'=>$default_ded,
			'minimum_premium'=>$minimum_premium,
			'max_insured'=>$max_insured,
			'title'=>($this->session->userdata('customer_type') == 'N') ? 'Get Quote' : 'Buy Insurance',
			'view'=>'buy_view',
			'policy_docs'=>$policy_docs
		);

		//transit form vars
		//get session values
		$buy_inputs = $this->session->userdata('buy_inputs');
		$premium = $this->session->userdata('premium');
		$cargo_price = $this->session->userdata('cargo_price');
		$bindoption = $this->session->userdata('bindoption');
		$useremail = $this->session->userdata('email');
		$email = '';
		$trans_icon = $this->common->transportations_icon();

		
		$user_id = $this->session->userdata('id');
		$customer_type = $this->session->userdata('customer_type');
		$mycustomers = array();
		
		if($customer_type == 'N'){
			$mycustomers = $this->master->getRecords('agent_customers', array('agent_id'=>$user_id));
		}
		
		
		$shipment_date = '';
		$vessel_name = '';
		$transitfrom = '';
		$portloading = '';
		$transitto = '';
		$portdischarge = '';
		$cargocat = '';
		$goods_desc = '';
		$goods_count = 100;
		$transmethod = '';
		$currency = '';
		$invoice = '';
		$cargo_max_val = '';
		$default_deductible = '';

		if($buy_inputs != ''){ //count($buy_inputs) > 0
			foreach($buy_inputs as $r=>$value){
				${$value['name']} = $value['value'];
			}
			$goods_count = 100 - strlen($goods_desc);
	
		}
		if($useremail != ''){
			$email = $useremail;
		}
		
		//update value of default ded when not found
		$existing_ded = $this->master->getRecordCount('deductibles', array('rate'=>$default_deductible));
		$selected_deductible = ($default_deductible) ? $default_deductible : $default_ded['rate'];
		
		if($existing_ded == 0){
			$selected_deductible = $default_ded['rate'];
			$default_deductible = $selected_deductible;
		}

		$data['premium'] = $premium;
		$data['cargo_price'] = $cargo_price;
		$data['cargo_max_val'] = $cargo_max_val;
		$data['bindoption'] = $bindoption;
		$data['useremail'] = $useremail;
		$data['email'] = $email;
		$data['buy_inputs'] = $buy_inputs;
		$data['trans_icon'] = $trans_icon;
		$data['selected_deductible'] = $selected_deductible;
		
		$data['shipment_date'] = $shipment_date;
		$data['vessel_name'] = $vessel_name;
		$data['transitfrom'] = $transitfrom;
		$data['portloading'] = $portloading;
		$data['transitto'] = $transitto;
		$data['portdischarge'] = $portdischarge;
		$data['cargocat'] = $cargocat;
		$data['goods_desc'] = $goods_desc;
		$data['goods_count'] = $goods_count;
		$data['transmethod'] = $transmethod;
		$data['currency'] = $currency;
		$data['invoice'] = $invoice;
		$data['mycustomers'] = $mycustomers;
		$data['view'] = 'buy_insurance';
		$data['title'] = ($this->session->userdata('customer_type') == 'N') ? 'Get Quote' : 'Buy Insurance';


		$this->load->view('includes/user_view', $data);
	}
	
	
	
	/***************************
		quotes
	***************************/
	public function quotes(){
		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');
		$insurances = $this->master->getRecords('bought_quote', array('customer_id'=>$user_id));
		
		$data = array(
			'view'=>'manage-quotes',
			'title'=>'Quotes',
			'insurances'=>$insurances
		);
		
		$this->load->view('includes/user_view', $data);
	
	}
	
	
	/***************************
		rating
	***************************/
	public function rating(){
		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');
		
		$agency_info = $this->master->getRecords('agency', array('id'=>$user_id));
		$cargo = $this->master->getRecords('cargo_category');
		$transportations = $this->common->transportations();
		$countries = $this->master->getRecords('country_t');

		$cargo_prices = $this->common->agency_prices($user_id,'cargo_prices');
		$transportation_prices = $this->common->agency_prices($user_id,'transportation_prices');
		$country_prices = $this->common->agency_prices($user_id,'country_prices');
		
		
		$data = array(
			'view'=>'rating_view',
			'title'=>'Rating',
			'cargo'=>$cargo,
			'transportations'=>$transportations,
			'countries'=>$countries,
			'cargo_prices'=>$cargo_prices,
			'transportation_prices'=>$transportation_prices,
			'country_prices'=>$country_prices,
			'agency_info'=>$agency_info,
		);
		
		$this->load->view('includes/user_view', $data);

	}
	
	public function get_scheds(){
		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');
		$sorted_schedule = $this->common->classes_schedules($user_id, NULL);//studio_id, class_id, type
		
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($sorted_schedule, JSON_PRETTY_PRINT);
		return false;
	}


	/***************************
		mycustomers for agents
	***************************/
	public function mycustomers(){
		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');

		$countries = $this->master->getRecords('country_t');

		
		$arr = array('user_id'=>$user_id);

		$customers = $this->master->getRecords('agent_customers', array('agent_id'=>$user_id));


		$data = array(
			'view'=>'manage_customers_view',
			'title'=>'Manage Customers',
			'customers'=>$customers,
			'countries'=>$countries
		);
		
		$this->load->view('includes/user_view', $data);

	}
	


	/***************************
		classes
	***************************/
	public function settings(){

		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');

		$studio = $this->master->getRecords('agency', array('id'=>$user_id));

		$data = array(
			'view'=>'settings_view',
			'title'=>'Settings',
			'studio'=>$studio
		);
		
		$this->load->view('includes/user_view', $data);

		
	}
	
	
	/***************************
		logout
	***************************/
	public function logout(){
		$user_data = $this->session->all_userdata();
	
		foreach ($user_data as $key => $value) {
			//do not remove admin session
			if($key != 'logged_admin' && $key != 'logged_admin_email'){
				$this->session->unset_userdata($key);
			}
		}		
		
		redirect(base_url());

	}
	
	/***************************
		reset_inputs
	***************************/
	public function reset_inputs(){
		
		$details = array(
			'buy_inputs'=>'',
			'single_input'=>'',
			'consignee'=>'',
			'premium'=>'',
			'premium_format'=>'',
			'selected_deductible'=> '',
			'cargo_price'=>'',
			'currency'=>''
		);
		
		//unset from session
		foreach ($details as $key => $value) {
			$this->session->unset_userdata($key);
		}		
	
	}
	

}
