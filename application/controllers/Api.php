<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Api extends CI_Controller{


	public function __construct(){

		parent::__construct();

	}

	public function login(){
		$username = (isset($_POST['username'])) ? $_POST['username'] : 'snailbob@live.com';
		$password = (isset($_POST['password'])) ? $_POST['password'] : 'Darknite1';
	
		$agency = $this->master->getRecords('agency', array('email'=>$username));
		$users = $this->master->getRecords('customers', array('email'=>$username));

		$count = 0;
	
		$arr = array(
			'result'=>'success',
			'count'=>$count
		);
	
		//get validation message from admin
		$action_message = $this->common->get_message('user_login');
		
		$arr['message'] = $action_message['error'];
	
		
		if(count($users) > 0){
			foreach($users as $r=>$value){
				if($value['password'] == md5($password)){
					unset($value['password']);
					$value['name'] = $value['first_name'].' '.$value['last_name'];
					$value['type'] = 'customer';
					$value['address'] = $value['location'];
					$arr['userdata'] = $value;
					
					
				
					$value['multiple_account'] = 'no';
					//include agency to switch
					if(count($agency) > 0){
						$value['multiple_account'] = 'yes';
					}
					if($value['enabled'] != 'N'){
						//$this->session->set_userdata($value);
						$count++;
						$arr['count'] = $count;
					
						$user_id = '2';
						$user_type = 'N';
						$buy_mob = $this->common->buy_mob($value['id'], $value['customer_type']);
						
						$arr['form'] = $buy_mob;
						$arr['result'] = 'ok';
						$arr['message'] = $action_message['success'];
					}
					else{
						$arr['result'] = 'error';
						$arr['message'] = '<i class="fa fa-warning text-warning"></i> Account not activated.';
					}
					
				}
			}
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		echo json_encode($arr, JSON_PRETTY_PRINT);
	}
	
	public function referrals(){
		$user_id = (isset($_POST['user_id'])) ? $_POST['user_id'] : '2';

		$insurances = $this->master->getRecords('bought_insurance', array('customer_id'=>$user_id, 'status !='=>'P'), '', array('status'=>'DESC'));
		
		$data = array();
		if(count($insurances) > 0){
			foreach($insurances as $r=>$value){
				$value['details'] = unserialize($value['details']);
				$value['premium'] = $value['details']['premium'];
				
				$value['stat'] = $this->common->policy_request_status($value['status']);
				$value['customer_name'] = $this->common->customer_name($value['customer_id']);
				
				$value['date_purch'] = ($value['date_purchased'] == '0000-00-00 00:00:00') ? 'NA' : date_format(date_create($value['date_purchased']), 'F d, Y - l');
				
				$buy_inputs = $value['details']['buy_inputs'];
			
				$insurance_details = array();
				foreach($buy_inputs as $bi=>$bival){
					$insurance_details[$bival['name']] = $bival['value'];
				}
				
				$value['currency'] = (isset($insurance_details['currency'])) ? $insurance_details['currency'] : 'AUD';
				$value['buy_inputs'] = $insurance_details;
			
				$single_input = (isset($value['details']['single_input'])) ? $value['details']['single_input'] : array();
			
				$value['cert'] = $this->common->the_cert_data($value['id']);

				$value['single_input'] = $single_input;
				unset($value['details']);
				$data[] = $value;
			}
		}
		
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		echo json_encode($data, JSON_PRETTY_PRINT);
		
	}
	
	public function transactions(){
		$user_id = (isset($_POST['user_id'])) ? $_POST['user_id'] : '2';
	
		$insurances = $this->master->getRecords('bought_insurance', array('customer_id'=>$user_id, 'status'=>'P'), '', array('status'=>'DESC'));

		$data = array();
		if(count($insurances) > 0){
			foreach($insurances as $r=>$value){
				$value['details'] = unserialize($value['details']);
				$value['premium'] = $value['details']['premium'];
				
				$value['stat'] = $this->common->policy_request_status($value['status']);
				$value['customer_name'] = $this->common->customer_name($value['customer_id']);
				
				$value['date_purch'] = ($value['date_purchased'] == '0000-00-00 00:00:00') ? 'NA' : date_format(date_create($value['date_purchased']), 'F d, Y - l');
				
				$buy_inputs = $value['details']['buy_inputs'];
			
				$insurance_details = array();
				foreach($buy_inputs as $bi=>$bival){
					$insurance_details[$bival['name']] = $bival['value'];
				}
				
				$value['currency'] = (isset($insurance_details['currency'])) ? $insurance_details['currency'] : 'AUD';
				$value['buy_inputs'] = $insurance_details;
			
				$single_input = (isset($value['details']['single_input'])) ? $value['details']['single_input'] : array();
				
				$value['cert'] = $this->common->the_cert_data($value['id']);
				
				$value['single_input'] = $single_input;
				$value['consignee'] = (isset($value['details']['consignee'])) ? $value['details']['consignee'] : array();
				unset($value['details']);
				$data[] = $value;
			}
		}
		

		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		echo json_encode($data, JSON_PRETTY_PRINT);
		
	}	
	
	
	public function quotes(){
		$user_id = (isset($_POST['user_id'])) ? $_POST['user_id'] : '2';
	
		$insurances = $this->master->getRecords('bought_quote', array('customer_id'=>$user_id), '*', array('id'=>'DESC'));

		$data = array();
		if(count($insurances) > 0){
			foreach($insurances as $r=>$value){
				$value['details'] = unserialize($value['details']);
				$value['premium'] = $value['details']['premium'];
				
				$value['stat'] = $this->common->quote_status($value['status']);
				$value['customer_name'] = $this->common->customer_name($value['customer_id']);
				
				$value['date_purch'] = ($value['date_purchased'] == '0000-00-00 00:00:00') ? 'NA' : date_format(date_create($value['date_purchased']), 'F d, Y - l');
				
				$buy_inputs = $value['details']['buy_inputs'];
			
				$insurance_details = array();
				foreach($buy_inputs as $bi=>$bival){
					$insurance_details[$bival['name']] = $bival['value'];
				}
				
				$value['currency'] = (isset($insurance_details['currency'])) ? $insurance_details['currency'] : 'AUD';
				$value['buy_inputs'] = $insurance_details;
			
				$single_input = (isset($value['details']['single_input'])) ? $value['details']['single_input'] : array();
				
				$value['single_input'] = $single_input;
				$consignee = (isset($value['details']['consignee'])) ? $value['details']['consignee'] : array();
				
				$value['consignee'] = $consignee;
				//unset($value['details']);
				$data[] = $value;
			}
		}
		

		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		echo json_encode($data, JSON_PRETTY_PRINT);
		
	}



	public function customers(){
		$user_id = (isset($_POST['user_id'])) ? $_POST['user_id'] : '2';
		
		$customers = $this->master->getRecords('agent_customers', array('agent_id'=>$user_id));

		$data = array();
		if(count($customers) > 0){
			foreach($customers as $r=>$value){
				$value['details'] = unserialize($value['details']);
				$data[] = $value;
			}
		}
		

		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		echo json_encode($data, JSON_PRETTY_PRINT);

	}


	public function buy(){
		$user_id = '2';
		$user_type = 'N';
		$buy_mob = $this->common->buy_mob($user_id, $user_type);
	
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		echo json_encode($buy_mob, JSON_PRETTY_PRINT);
	}
	
	public function buyform(){
		$buy_inputs = $_POST['buy_inputs'];
		$user_id = $_POST['user_id'];
		$premium = $_POST['premium'];
		$cargo_price = $_POST['cargo_price'];
		$currency = $_POST['currency'];
		$selected_deductible = $_POST['selected_deductible'];

		$max_insured = $_POST['max_insured'];
	
		//format buy_inputs data
		$arr = array();
		while ($val = current($buy_inputs)) {
			$b = array(
				'name'=>key($buy_inputs),
				'value'=>$val
			);
			if(key($buy_inputs) == 'sdate_format'){
				$b = array(
					'name'=>'shipment_date',
					'value'=>$val
				);
			}
			$arr[] = $b;
			next($buy_inputs);
		}	
		
		
		//format buy input array
		$buy_form_inputs = array();
		if(is_array($arr)){
			foreach($arr as $r=>$value){
				$buy_form_inputs[$value['name']] = $value['value'];
			}
		}
		
		//check currency value to max insured
		$default_cargo = $this->common->db_field_id('admin', 'default_cargo', '2');
		$base_currency = $this->common->base_currency($default_cargo);
		$insured_val = $this->common->numeric_currency($buy_form_inputs['invoice']);
		$insured_converted_val = ($base_currency != $currency) ? $this->common->converted_value($currency, $base_currency, $insured_val) : $insured_val;
		
		$refer = false;
		
		if($insured_converted_val > $max_insured){
			$refer = true;
		}
		
		//add converted val to session
		$arr[] = array(
			'name'=>'converted_invoice',
			'value'=>$insured_converted_val
		);
		//add default currency
		$arr[] = array(
			'name'=>'base_currency',
			'value'=>$base_currency
		);
		
		
		$buy_inputs_format = array();
		if(is_array($arr)){
			foreach($arr as $r=>$value){
				$buy_inputs_format[$value['name']] = $value['value'];
			}
		}
		
		$data = array(
			'buy_inputs'=>$arr,
			'buy_inputs_format'=>$buy_inputs_format,
			'premium'=>round($premium, 2),
			'premium_format'=>number_format($premium, 2, '.', ','),
			'selected_deductible'=>$selected_deductible,
			'cargo_price'=>$cargo_price,
			'currency'=>$currency,
			'refer'=>$refer
			//'bindoption'=>$bindoption
		);
		
			
	
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		echo json_encode($data, JSON_PRETTY_PRINT);
	}
		
	public function ports(){
		$country_id = (isset($_POST['country_id'])) ? $_POST['country_id'] : '1';
		$countries = $this->master->getRecords('country_t', array('country_id'=>$country_id));
		$country_code = (count($countries) > 0) ? $countries[0]['iso2'] : 'PH';
	    $port_codes = $this->master->getRecords('port_codes',array('country_code'=>$country_code));

		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		echo json_encode($port_codes, JSON_PRETTY_PRINT);
	}
	
	public function single_purchase(){
		$user_id = $_POST['user_id'];
		$single = $_POST['single'];
		$single_email = $_POST['user_email'];
		$info = $_POST['info'];
		$crefer = $_POST['crefer'];
		$consignor_info = $_POST['consignor_info'];

		$country_id = $consignor_info['country_id'];
		$calling_code = $this->common->db_field_id('country_t', 'calling_code', $country_id, $idfield = 'country_id');
		$calling_digits = ltrim($consignor_info['calling_digits'],'0');
		
		$first = $consignor_info['first_name'];
		$last = $consignor_info['last_name'];
		$email = $consignor_info['email'];
		$address = $consignor_info['address'];
		
		$arr = array(
			'first_name'=>$first,
			'last_name'=>$last,
			'business_name'=>$consignor_info['business_name'],
			'location'=>$address,
			'lat'=>$consignor_info['lat'],
			'lng'=>$consignor_info['lng'],
			'country_id'=>$country_id,
			'contact_no'=>$calling_code.$calling_digits,
			'calling_code'=>$calling_code,
			'calling_digits'=>$calling_digits,
			'email'=>$email
		);
	
		$buy_inputs = $_POST['buy_inputs']; //$this->session->userdata('buy_inputs');
		$premium = $_POST['premium']; //$this->session->userdata('premium');
		//$premium = round($premium, 2);
		$nowtime = date('Y-m-d H:i:s');
		
		
		if($user_id != ''){
			//if a consignor
			if($info == 'consignor'){
				$this->master->updateRecord('customers', $arr, array('id'=>$user_id));
			}
			
			//if customer adding agent 
			else if($info == 'agent_customer'){
				$cust_info = array(
					'agent_id'=>$user_id,
					'name'=>$first.' '.$last,
					'email'=>$email,
					'details'=>serialize($arr),
					'date_added'=>$nowtime,
				);
				$id = '';
				if(isset($_POST['id'])){
					$id = $_POST['id'];
				}
				if($id == ''){
					$this->session->set_flashdata('ok', 'Customer successfully added.');
					$this->master->insertRecord('agent_customers', $cust_info);
				}
				else{
					$this->session->set_flashdata('ok', 'Customer successfully updated.');
					$this->master->updateRecord('agent_customers', $cust_info, array('id'=>$id));
				}
			}

		}
		else{
			$user_id = 0;
		}
		
		//check if consignee
		if($info == 'consignee'){
			$consignee = $arr;
			$arr = array();
			$arr['consignee'] = $consignee;
		}
		else{
			$arr['single'] = $arr;
			$arr['address'] = $address;
		}
		
		//store to session if not new customer
		if($info != 'agent_customer'){
			$this->session->set_userdata($arr); 
		}
		
		
		//check if country needs referral
		if($crefer != ''){
			$details = array(
				'buy_inputs'=>$buy_inputs,
				'single_input'=>$arr['single'],
				'premium'=>$premium
			);
					
			
			$data = array(
				'customer_id'=>$user_id,
				'details'=>serialize($details),
				'status'=>'S',
				'email'=>$single_email,
				'date_added'=>$nowtime
			);
			
			$booking_id = $this->master->insertRecord('bought_insurance', $data, true);
			
			//admin emailer info
			$adminemail = $this->common->admin_email();
	
			//email settings for agency
			$info_arr = array(
				'to'=>$this->common->recovery_admin(), //$adminemail,
				'from'=>$adminemail,
				'subject'=>'Welcome to Transit Insurance',
				'view'=>'referral-notification-to-admin',
				'emailer_file_name'=>'referral-notification-to-admin',
			);
			
	
			$other_info = array(
				'password'=>'',
				'view'=>'referral-notification-to-admin',
				'emailer_file_name'=>'referral-notification-to-admin',
				'name'=>'',//$first.' '.$last,
				'agency_name'=>'Admin',
				'user_name'=>$adminemail,
				'user_email'=>$adminemail,
				'link'=>'webmanager/insurance'
			);
			$data['info_arr1'] = $info_arr;
			$data['other_info1'] = $other_info;
			$this->emailer->sendmail($info_arr,$other_info);
						
			//email settings for student
			$info_arr = array(
				'to'=>$single_email,
				'from'=>$adminemail,
				'subject'=>'Welcome to Transit Insurance',
				'view'=>'referral-notification-to-customer',
				'emailer_file_name'=>'referral-notification-to-customer',
			);
	
			$other_info = array(
				'password'=>'',
				'view'=>'referral-notification-to-customer',
				'emailer_file_name'=>'referral-notification-to-customer',
				'name'=>$single['first_name'].' '.$single['last_name'],
				'agency_name'=>'', //$agency_info[0]['name'],
				'user_name'=>$single_email,
				'user_email'=>$single_email,
				'link'=>'dashboard' //landing/download/'.$user_id.'/'.$booking_id.'/'.md5($user_id)
			);
			if($user_id == '0'){
				//$other_info['link'] = 'landing/reqdetails/'.$booking_id.'/'.md5($email);
			}
			
			$data['info_arr2'] = $info_arr;
			$data['other_info2'] = $other_info;
			$this->emailer->sendmail($info_arr,$other_info);
				
			$arr['data'] = $data;				
		}
	
		//get validation message from admin
		$action_message = $this->common->get_message('referral_request');
		$message = $action_message['success'];
		$result = 'ok';
		
		$arr['result'] = $result;
		$arr['message'] = $message;
				
		header('Access-Control-Allow-Origin: *');
		header('Content-Type: application/json');
		echo json_encode($arr, JSON_PRETTY_PRINT);

	}
	
	
	public function save_quote(){
		$user_id = $_POST['user_id'];
		$nowtime = date('Y-m-d H:i:s');
	
		$consignor_info = $_POST['single_input'];
		$consignee_info = $_POST['consignee'];
		$premium = $_POST['premium'];
		$buy_inputs = $_POST['buy_inputs'];
		//single data
		$country_id = $consignor_info['country_id'];
		$calling_code = $this->common->db_field_id('country_t', 'calling_code', $country_id, $idfield = 'country_id');
		$calling_digits = ltrim($consignor_info['calling_digits'],'0');
		
		$first = $consignor_info['first_name'];
		$last = $consignor_info['last_name'];
		$email = $consignor_info['email'];
		$address = $consignor_info['address'];
		
		$single = array(
			'first_name'=>$first,
			'last_name'=>$last,
			'business_name'=>$consignor_info['business_name'],
			'location'=>$address,
			'lat'=>$consignor_info['lat'],
			'lng'=>$consignor_info['lng'],
			'country_id'=>$country_id,
			'contact_no'=>$calling_code.$calling_digits,
			'calling_code'=>$calling_code,
			'calling_digits'=>$calling_digits,
			'email'=>$email
		);

	
	
	
		//consignee data
		$country_id = $consignee_info['country_id'];
		$calling_code = $this->common->db_field_id('country_t', 'calling_code', $country_id, $idfield = 'country_id');
		$calling_digits = ltrim($consignee_info['calling_digits'],'0');
		
		$first = $consignee_info['first_name'];
		$last = $consignee_info['last_name'];
		$email = $consignee_info['email'];
		$address = $consignee_info['address'];
		
		$consignee = array(
			'first_name'=>$first,
			'last_name'=>$last,
			'business_name'=>$consignee_info['business_name'],
			'location'=>$address,
			'lat'=>$consignee_info['lat'],
			'lng'=>$consignee_info['lng'],
			'country_id'=>$country_id,
			'contact_no'=>$calling_code.$calling_digits,
			'calling_code'=>$calling_code,
			'calling_digits'=>$calling_digits,
			'email'=>$email
		);
	
	
	
	
	
		//get session data
		$details = array(
			'buy_inputs'=>$buy_inputs,
			'single_input'=>$single,
			'consignee'=>$consignee,
			'premium'=>$premium
		);
				
		$data = array(
			'customer_id'=>$user_id,
			'details'=>serialize($details),
			'status'=>'S',
			'email'=>$single['email']
		);
		
		
		$data['date_added'] = $nowtime;
		$booking_id = $this->master->insertRecord('bought_quote', $data, true);
	
	
		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings for customer
		$info_arr = array(
			'to'=>$single['email'],
			'from'=>$adminemail,
			'subject'=>'Welcome to Transit Insurance',
			'view'=>'quote-send-to-consignor',
			'emailer_file_name'=>'quote-send-to-consignor',
		);

		$other_info = array(
			'password'=>'',
			'view'=>'quote-send-to-consignor',
			'emailer_file_name'=>'quote-send-to-consignor',
			'name'=>$single['first_name'].' '.$single['last_name'],
			'agency_name'=>'', //$agency_info[0]['name'],
			'user_name'=>$single['email'],
			'user_email'=>$single['email'],
			'link'=>'landing/confirm_quote/'.$user_id.'/'.$booking_id.'/'.md5($user_id)
		);

		
		$data['info_arr2'] = $info_arr;
		$data['other_info2'] = $other_info;
		$this->emailer->sendmail($info_arr,$other_info);
		$data['booking_id'] = $booking_id;	

		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');

		echo json_encode($data, JSON_PRETTY_PRINT); // $single['email'];
	}


	public function now_send_quote(){
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$email = $_POST['email'];
		$email_to = $_POST['emailto'];
		$buy_inputs = $_POST['buy_inputs'];
		$b = $this->common->buy_inputs($buy_inputs);
		
		//admin emailer info
		$adminemail = $this->common->admin_email();
		$emailer_file_name = 'quote-sending';
		//email settings for customer
		$info_arr = array(
			'to'=>$email_to,
			'from'=>$adminemail,
			'subject'=>'Welcome to Transit Insurance',
			'view'=>$emailer_file_name,
			'emailer_file_name'=>$emailer_file_name,
		);

		$other_info = array(
			'password'=>'',
			'view'=>$emailer_file_name,
			'emailer_file_name'=>$emailer_file_name,
			'name'=>'',
			'details'=>$b,
			'agency_name'=>'',
			'user_name'=>$first_name.' '.$first_name,
			'user_email'=>$email,
			'link'=>'',
		);

		
		$data['info_arr'] = $info_arr;
		$data['other_info'] = $other_info;
		$data['result'] = 'OK';
		$this->emailer->sendmail($info_arr,$other_info);

		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');

		echo json_encode($data);
	}
	
	public function now_save_quote(){
		$user_id = $_POST['user_id'];
		$booking_id = '';
		$single = array();
		$email = $_POST['email'];
		$nowtime = date('Y-m-d H:i:s');

		$buy_inputs = $_POST['buy_inputs'];
		$consignee = array();
		$premium = $_POST['premium'];
		//get session data
		$details = array(
			'buy_inputs'=>$buy_inputs,
			'single_input'=>(is_array($single)) ? $single : array(),
			'consignee'=>(is_array($consignee)) ? $consignee : array(),
			'premium'=>$premium
		);
				
		$data = array(
			'customer_id'=>$user_id,
			'details'=>serialize($details),
			'status'=>'D',
			'email'=>(isset($single['email'])) ? $single['email'] : $email
		);
		
		$data['date_added'] = $nowtime;
		
		if($booking_id != ''){
			$this->master->updateRecord('bought_quote', $data, array('id'=>$booking_id));
		}
		else{
			$booking_id = $this->master->insertRecord('bought_quote', $data, true);
		}
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
		echo json_encode($details);
	}

	
}


?>