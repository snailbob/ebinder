<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Home extends CI_Controller{


	public function __construct(){

		parent::__construct();

		$user_session = $this->session->all_userdata();

		if(!empty($user_session['id']) && !empty($user_session['logged_admin_email'])){
			redirect($this->common->base_url().'webmanager');
		}
		else if(!empty($user_session['id']) && empty($user_session['logged_admin_email'])){
			redirect($this->common->base_url().'panel');
		}

	}

	public function index(){

//		$url = $_SERVER["SERVER_NAME"];
//		$account = str_replace(".ebinder.com.au","",$url);
//
//		if($account != 'ml'){
//			echo '';
//			return false;
//		}


		//check for subdomain
/*		$url = $_SERVER["SERVER_NAME"];
		$account = str_replace(".ebinder.com.au","",$url);

		//if($account == 'localhost'){
		if($account != '' && $account != 'ebinder.com.au'){
			$match = $this->master->getRecords('customers', array('domain'=>$account));

			if(count($match) > 0){
				//echo json_encode($match[0]);

				$curr_sess = $this->session->all_userdata();

	//				if(empty($curr_sess['id'])){
	//					$sess = $this->common->find_active_session($account);
	//
	//					if(count($sess) > 0){
	//						$this->session->set_userdata($sess[0]);
	//						$curr_sess = $this->session->all_userdata();
	//					}
	//				}

				$broker = $this->common->brokers($match[0]['id']);
				$data = array(
					'user'=>$broker,
					'account'=>$account,
					'user_sess'=>$curr_sess
				);

				if(empty($curr_sess['id'])){
					redirect(base_url().'login');
					//$this->load->view('domains/login', $data);
				}
				else{
					redirect($this->common->base_url().'panel');
					//$this->load->view('domains/dash', $data);
				}

				//echo json_encode($data);


				return false;

			}

			else{
				redirect('404/'.$account);
			}

			return false;
		}
//		$curr_sess = $this->session->all_userdata();
//		echo json_encode($curr_sess);
//		return false;
	*/



		//home page script
		$mobile_code = $this->master->getRecords('country_t');
		$firsthomesection = $this->master->getRecords('admin_contents', array('type'=>'firsthomesection'));

		$mainhometitle = $this->master->getRecords('admin_contents', array('type'=>'mainhometitle'));



		$alternatingcontenthead = $this->master->getRecords('admin_contents', array('type'=>'alternatingcontenthead'));

		$alternatingcontents = $this->master->getRecords('admin_contents', array('type'=>'alternatingcontents'), '*', array('sort'=>'ASC'));

		$threepartshead = $this->master->getRecords('admin_contents', array('type'=>'threepartshead'));

		$threepartshead2 = $this->master->getRecords('admin_contents', array('type'=>'threepartshead2'));

		$aboutus = $this->master->getRecords('admin_contents', array('type'=>'aboutus'));

		$threeparts = $this->master->getRecords('admin_contents', array('type'=>'threeparts'));
		$threeparts2 = $this->master->getRecords('admin_contents', array('type'=>'threeparts2'));
		$banners = $this->master->getRecords('admin_contents', array('type'=>'banner'));
		$faq = $this->master->getRecords('faq');
		$social = $this->session->userdata('social');

		$data = array(
			'mobile_code'=>$mobile_code,
			'mainhometitle'=>$mainhometitle,
			'firsthomesection'=>$firsthomesection,
			'alternatingcontenthead'=>$alternatingcontenthead,
			'alternatingcontents'=>$alternatingcontents,
			'threepartshead'=>$threepartshead,
			'threeparts'=>$threeparts,
			'threepartshead2'=>$threepartshead2,
			'threeparts2'=>$threeparts2,
			'aboutus'=>$aboutus,
			'faq'=>$faq,
			'social'=>$social,
			'banners'=>$banners
		);


		$the_domain = $this->common->the_domain();
		if(($the_domain != '') && $this->session->userdata('id') == ''){
			header('X-Frame-Options: GOFORIT');

			$whr_domain = array(
				'domain'=>$the_domain,
				'enabled'=>'Y'
			);
			$data['active_domain'] = $this->master->getRecords('agency', $whr_domain);
			$this->load->view('frontpage/header_view',$data);
			$this->load->view('frontpage/login_only', $data);
			$this->load->view('frontpage/footer_view');
			// $this->load->view('frontpage/subhome_iframe', $data);
			// echo $this->common->the_domain();
			return false;


		}




		$this->load->view('frontpage/header_view',$data);
		$this->load->view('frontpage/anewskin_view',$data);
		$this->load->view('frontpage/footer_view');

	}

	public function subhome(){
		$the_domain = $this->common->the_domain();

		$whr_domain = array(
			'domain'=>$the_domain,
			'enabled'=>'Y'
		);
		$data['active_domain'] = $this->master->getRecords('agency', $whr_domain);
		// $this->load->view('frontpage/header_view',$data);
		// $this->load->view('frontpage/login_only', $data);
		// $this->load->view('frontpage/footer_view');
		// echo $this->common->the_domain();
		$this->load->view('frontpage/subhome_iframe', $data);
	}

	public function business_capture(){
		$nowtime = date('Y-m-d H:i:s');
		if(isset($_POST['arr'])){
				$arr = $_POST['arr'];

			$insrt = array(
				'arr'=>serialize($arr),
				'name'=>'6_select',
				'date_added'=>$nowtime
			);
			$this->master->insertRecord('q_select', $insrt);

//			foreach($arr as $r=>$value){
//			}
			echo json_encode($arr);
			return false;
		}

		$this->load->view('domains/_business_capture');
	}

	public function login(){

		$data['page_title'] = "Cloud-based Crisis Management System";

		$this->load->view('frontpage/header_view',$data);
		$this->load->view('frontpage/login_view',$data);
		$this->load->view('frontpage/footer_view');

	}

	public function forohfor(){

		$data['page_title'] = "Cloud-based Crisis Management System";

		$this->load->view('frontpage/header_view',$data);
		$this->load->view('frontpage/forohfor',$data);
		$this->load->view('frontpage/footer_view');

	}


	public function show_country_code() {
		$counrty_code = $_POST['country_short'];

		$mobile_code = $this->master->getRecords('country_t',array('iso2'=>$counrty_code));

		$data['calling_code'] = '';
		$data['country_id'] = '';
		$data['result'] = 'empty';

		if(count($mobile_code) > 0){
			$data['calling_code'] = $mobile_code[0]['calling_code'];
			$data['country_id'] = $mobile_code[0]['country_id'];
			$data['result'] = 'success';
		}

		echo json_encode($data);

	}


	public function currency_con(){
		$this->load->view('user/test-curr-conversion');
	}

	public function savecurr(){
		$arr = $_POST['arr'];

		$a = array();
		foreach($arr as $r){
			//$this->master->insertRecord('api_currencies', $r);
			$a[] = $r;
		}

		echo json_encode($a);
	}

	public function paymentcron(){

		//cron job only/ and day first of month
		$theday = date('d');
		$the_param = (isset($_GET['pay'])) ? $_GET['pay'] : '';
		$the_rserver = $_SERVER['REMOTE_ADDR'];
		$nowtime = date('Y-m-d H:i:s');
		$access_log = array(
			'authorized'=>'yes',
			'date'=>$nowtime,
			'remote_server'=>$the_rserver
		);
		if($theday != '01'){ // || $the_param != '24a6ce0a5a6cc4dadea4345a33187f6e'
			echo 'WARNING!! Remote server: '.$the_rserver.' - day is :'.$theday;
			$access_log['authorized'] = 'no';
			$this->master->insertRecord('sms', array('status'=>serialize($access_log)));
			return false;
		}

		$this->master->insertRecord('sms', array('status'=>serialize($access_log)));

		$brokers = $this->master->getRecords('customers', array('customer_type'=>'N', 'status'=>'Y'));

		$tots = array();
		if(count($brokers) > 0){
			foreach($brokers as $r=>$value){
				$total = $this->common->total_billing($value['id'], $nowtime);
				$tots[] = $total;

				if($value['stripe_id'] != ''){
					$sale_result = Braintree_Transaction::sale(
					  array(
						'customerId' => $value['stripe_id'],
						'amount' => $total,
						//'billingAddressId' => 'AA',
						//'shippingAddressId' => 'AB'
					  )
					);

					$status = $sale_result->transaction->status;
					$id = $sale_result->transaction->id;
					$type = $sale_result->transaction->type;
					$currencyIsoCode = $sale_result->transaction->currencyIsoCode;
					$amount = $sale_result->transaction->amount;

					$arr = array(
						'status'=>$status,
						'transaction_id'=>$id,
						'type'=>$type,
						'currency'=>$currencyIsoCode,
						'amount'=>$amount
					);

					$data = array(
						'broker_id'=>$value['id'],
						'transaction_id'=>$id,
						'arr'=>serialize($arr),
						'date_added'=>$nowtime
					);

					$this->master->insertRecord('q_transactions', $data);

				}

			}
		}


		echo json_encode($tots);

	}
}


?>
