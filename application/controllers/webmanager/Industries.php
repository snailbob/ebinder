<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Industries extends CI_Controller 

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	/*------------------------------------
	index
	-------------------------------------*/
	public function index() {

		redirect(base_url().'webmanager/products/manage');

	}


	/*------------------------------------
	manage
	-------------------------------------*/
	public function manage() {
		$q_products = $this->master->getRecords('q_business_categories');

		$data = array(
			'middle_content'=>'manage-industries',
			'title'=>'Industries',
			'singular_title'=>'Industry',
			'q_products'=>$q_products
		);	
		$this->load->view('admin/admin-view',$data);

	}
	
	
	
	/*------------------------------------
	occupations
	-------------------------------------*/
	public function occupations(){
		$id = $this->uri->segment(4);
		//$product = $this->master->getRecords('q_business_categories', array('id'=>$id));
		$questionnaires = $this->master->getRecords('q_business_specific'); //, array('category_id'=>$id)
		
//		if(count($product) == 0){
//			$this->session->set_flashdata('error','No occupation found.');
//			redirect('webmanager/products');
//			return false;
//		}
	
		$data = array(
			'middle_content'=>'manage-occupations',
			'title'=>'Occupations',
			'singular_title'=>'Occupation',
			//'products'=>$product,
			'product_id'=>$id,
			'questionnaires'=>$questionnaires
		);	
		$this->load->view('admin/admin-view',$data);
	}
	
	/*------------------------------------
	save_product
	-------------------------------------*/
	public function save_product() {
		$product_name = $_POST['category_name'];
		$helper = ''; //$_POST['helper'];
		$id = $_POST['id'];
		$broker_id = $_POST['broker_id'];
		
		
		$nowtime = date('Y-m-d H:i:s');
		$msg = 'error';
		
		$arr = array(
			'category_name'=>$product_name,
			//'helper'=>$helper,
			'broker_id'=>$broker_id,
		);
		
		if($id != ''){
			if($this->master->updateRecord('q_business_categories',$arr, array('id'=>$id))){
				$this->session->set_flashdata('success',' Industry updated successfully');
				$msg = 'success';
			}
		}
		else{
			$arr['date_added'] = $nowtime;	
			$this->master->insertRecord('q_business_categories',$arr);
		
			$this->session->set_flashdata('success',' Industry added successfully');
			$msg = 'success';
		}
		
		echo json_encode($arr);
		
	}
	
	
	/*------------------------------------
	save_question
	-------------------------------------*/
	public function save_question() {
		$question = $_POST['specific_name'];
		$id = $_POST['id'];
		$broker_id = $_POST['broker_id'];
		$product_id = $_POST['product_id'];
		
		
		$nowtime = date('Y-m-d H:i:s');
		$msg = 'error';
		
		$arr = array(
			'specific_name'=>$question,
			'broker_id'=>$broker_id,
		);
		
		if($id != ''){
			if($this->master->updateRecord('q_business_specific',$arr, array('id'=>$id))){
				$this->session->set_flashdata('success',' Occupation updated successfully');
				$msg = 'success';
			}
		}
		else{
			$arr['category_id'] = 0;	
			$arr['date_added'] = $nowtime;	
			$this->master->insertRecord('q_business_specific',$arr);
		
			$this->session->set_flashdata('success',' Occupation added successfully');
			$msg = 'success';
		}
		
		echo json_encode($arr);
		
	}
	

}