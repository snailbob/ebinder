<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Insurance extends CI_Controller 

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/jobs/manage');

	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function manage() {
		$insurances = $this->master->getRecords('bought_insurance');

		$data = array(
			'middle_content'=>'manage-insurance',
			'title'=>'Bought Insurances',
			'insurances'=>$insurances
		);	
		
		$this->load->view('admin/admin-view',$data);


	}

	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function portcodes() {
		$search_key = '';
		$search_by = 'port_name';
		if(isset($_GET['search'])){
			$search_key = $_GET['search'];
		}
		if(isset($_GET['search_by'])){
			$search_by = $_GET['search_by'];
		}
		if($search_key != '' ){
			$this->db->like($search_by, $search_key); 
		}
		
		$port_codes_count = $this->master->getRecordCount('port_codes');
		$countries = $this->master->getRecords('country_t');
		$config['base_url'] = base_url().'webmanager/insurance/portcodes';
		$config['total_rows'] = $port_codes_count;
		$config['per_page'] = 200; 
		$config['uri_segment'] = 4;
		
		
		$config['full_tag_open'] = ' <div class="btn-group btn-group-sm">';
		$config['full_tag_close'] = '</div>';
		
		$config['display_pages'] = TRUE;
		$config['first_link'] = FALSE;
		$config['last_link'] = FALSE;

		$config['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
		$config['next_tag_open'] = '<div class="btn btn-default text-muted">';
		$config['next_tag_close'] = '</div>';
		
		$config['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
		$config['prev_tag_open'] = '<div class="btn btn-default text-muted">';
		$config['prev_tag_close'] = '</div>';
		
		$config['num_tag_open'] = '<div class="btn btn-default text-muted">';
		$config['num_tag_close'] = '</div>';
		
		$config['cur_tag_open'] = '<div class="btn btn-default active"><b>';
		$config['cur_tag_close'] = '</b></div>';

		// Add your query string
		if($search_key != ''){
			$config['suffix'] = '?search='.$search_key.'&search_by='.$search_by;
		}
		
		$this->pagination->initialize($config); 

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;


		$whr_arr = '';
		$select = '*';
		$order = array(
			'id'=>'ASC'
		);
		
		if($search_key != ''){
			$this->db->like($search_by, $search_key); 
		}
	    $port_codes = $this->master->getRecords('port_codes',$whr_arr,$select,$order,$page,$config['per_page']);
	
		$page_from = $page;
		$page_to = $page + count($port_codes);
		$page_from = 1 + $page_from;
	
		$data = array(
			'middle_content'=>'manage-portcodes',
			'title'=>'Port Codes',
			'port_codes'=>$port_codes,
			'page'=>$page,
			'page_from'=>$page_from,
			'port_codes_count'=>$port_codes_count,
			'page_to'=>$page_to,
			'countries'=>$countries,
			'search_key'=>$search_key,
			'search_by'=>$search_by	
		);	
		
		$this->load->view('admin/admin-view',$data);


	}
	
	public function store_cargo_prices(){
		$prices = $this->master->getRecords('cargo_category');
		
		$admin_info = $this->master->getRecords('admin');
		$the_agency = $this->master->getRecords('agency', array('id'=>$admin_info[0]['default_cargo']));

		$cargop = array();
		foreach($prices as $r=>$value){
			$cargop[$value['id']] = $value['multiplier'];
		}
		//$this->master->updateRecord('agency', array('cargo_prices'=>serialize($cargop)),array('id'=>$admin_info[0]['default_cargo']));
	}
	
	public function upload_csv(){
		$ports = $this->session->userdata('hscodes');
		
		$names = array();
		$count = 1;
		for($count; $count < count($ports);$count++){
			if($count != 0){
				$names[] = $ports[$count];
				//$this->master->insertRecord('cargo_category', $ports[$count]);
			}
		}
		
//		$ports = $this->csv('hs_codes.csv');
		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');
//		$this->session->set_userdata('hscodes',$ports);
		echo json_encode($names, JSON_PRETTY_PRINT);

	}
	
	public function csv($csv_file){

		$f = fopen(base_url()."uploads/portcds/".$csv_file, "r");

		$ports = array();
		while (($data = fgetcsv($f, 1000, ",")) !== FALSE) {
			
			if(array_key_exists('5', $data)){
				$ports[] = array(
					'hs_code'=>$data[0],
					'description'=>$data[1],
					'multiplier'=>$data[2],
					'uninsurable'=>$data[3],
					'master_line'=>$data[4],
					'referral'=>$data[5]
				);
			}
		}		
		
		return $ports;
	}
	
	
	public function certificate(){
		$insurances = $this->master->getRecords('bought_insurance');
		$admin_info = $this->master->getRecords('admin');
	
//		$data = $this->common->the_cert_editor(); //the_cert_data('0');
//		$data['middle_content'] = 'manage-certificate';
//		$data['title'] = 'Manage Certificate';
//		$data['insurances'] = $insurances;
//		$data['admin_info'] = $admin_info;


		$data = array(
			'middle_content'=>'manage-certificate',
			'title'=>'Manage Certificate',
			'admin_info'=>$admin_info,
			'insurances'=>$insurances
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	public function savetexts(){
		$text = $_POST['text'];
		$field = $_POST['field'];
		$this->master->updateRecord('admin', array($field=>$text), array('id'=>'2'));
		echo json_encode(array($field=>$text));
	}
	
	public function savecert(){
		$cert = $_POST['cert'];
		$this->master->updateRecord('admin', array('certificate_template'=>$cert), array('id'=>'2'));
		echo json_encode(array('certificate_template'=>$cert));
	}

//	public function addcity(){
//		$name = $_POST['name'];
//		$uniquetime = $_POST['uniquetime'];
//		$city_id = $_POST['city_id'];
//		$now_date = date('Y-m-d H:i:s');
//		
//		$arr = array(
//			'name'=>$name
//		);
//		
//		
//		if($city_id != ''){
//			$this->master->updateRecord('admin_cities', $arr, array('id'=>$city_id));
//			$this->session->set_flashdata('success','City updated successfully.');
//		}
//		else{
//			$arr['date_added'] = $now_date;
//			$city_id = $this->master->insertRecord('admin_cities', $arr, true);
//			$this->session->set_flashdata('success','City added successfully.');
//		}
//		
//		$this->master->updateRecord('admin_cities_file', array('city_id'=>$city_id), array('temp_id'=>$uniquetime));
//		
//		$arr['city_id'] = $city_id;
//		echo json_encode($arr);
//	
//	}
//
//	public function upload(){
//
//		$output_dir = "uploads/cities/";
//		
//		
//		if(isset($_FILES["myfile"]))
//		{
//			//Filter the file types , if you want.
//			if ($_FILES["myfile"]["error"] > 0)
//			{
//			  echo '<a href="javascript: ;" onclick="$(this).hide();" class="text-danger" id="error_up"><i class="fa fa-warning"></i> Error Uploading File</a>';//$_FILES["file"]["error"] . "<br>";
//			}
//			else
//			{
//				$thefilename = uniqid().str_replace(' ', '_', $_FILES["myfile"]["name"]);
//				
//				//move the uploaded file to uploads folder;
//				move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$thefilename); //$_FILES["myfile"]["name"]);
//								
//				
//					$insrt_arr = array (
//						'file_upload_name'=>$thefilename, //$_FILES["myfile"]["name"],
//						'temp_id'=>$_POST['uniquetime'],
//						'city_id'=>$_POST['city_id'] //$cc_case_id
//					);					
//					if ($file_id = $this->master->insertRecord('admin_cities_file',$insrt_arr,true)){
//						
//						echo ' <a href="'.base_url().$output_dir.$thefilename.'" target="_blank" id = "file_del'.$file_id.'" class="file_attch_btn"><i class="fa fa-paperclip"></i>'.$_FILES["myfile"]["name"].'</a> <span onclick="$(\'#file_del'.$file_id.'\').hide();$(this).hide();delete_the_city_file(event.target.id);" style="cursor: default" id="delete_file'.$file_id.'">&times;</span>';
//					
//					}
//					else{
//					  echo '<a href="javascript: ;" onclick="$(this).hide();" class="text-danger" id="error_up"><i class="fa fa-warning"></i> Error Uploading File</a>';//$_FILES["file"]["error"] . "<br>";
//					}
//			}
//		
//		}		
//
//	}	
//	
//	
//	/*-------------------------------------
//			delete view loading
//	-------------------------------------*/
//	public function delete() {
//		$id = $_POST['id'];
//		$files = $this->master->getRecords('admin_cities_file',array('city_id'=>$id));
//		
//		if($this->master->deleteRecord('admin_cities','id',$id)) {
//			
//			if(count($files) > 0){
//				foreach($files as $r=>$file){
//					@unlink('uploads/cities/'.$file['file_upload_name']);
//					$this->master->deleteRecord('admin_cities_file','id',$file['id']);
//				}
//			}
//				
//			
//			echo 'ok';
//		}
//	}
//	
//
//	public function delete_file(){
//		
//		$thenum = $_POST['thenum'];
//	    $file = $this->master->getRecords('admin_cities_file',array('id'=>$thenum));
//		
//		$file_name = $file[0]['file_upload_name'];
//		
//		if($this->master->deleteRecord('admin_cities_file','id',$thenum)){
//
//			@unlink('uploads/cities/'.$file_name);
//			echo 'success';
//		}
//	}	
//		

}