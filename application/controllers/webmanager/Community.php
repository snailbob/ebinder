<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Community extends CI_Controller 

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/users/manage');

	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function manage() {
		$communities = $this->master->getRecords('community');

		$data = array(
			'middle_content'=>'manage-community',
			'page_title'=>'Change Password',
			'communities'=>$communities
		);	
		$this->load->view('admin/admin-view',$data);


	}
	#--------------------------------------------->>manage contents<<-------------------------------------
	public function contents() {
		$type = $this->uri->segment(4);
		$community = $this->master->getRecords('community',array('permalink'=>$type));


		if(count($community) > 0){
			
			$community_blog = $this->master->getRecords('community_blog',array('community_permalink'=>$community[0]['permalink']), '*', array('id'=>'DESC'));
			
			$data = array(
				'middle_content'=>'manage-community-contents',
				'page_title'=>'Change Password',
				'community'=>$community,
				'community_blog'=>$community_blog
			);	
			$this->load->view('admin/admin-view',$data);
		
		}
		else{
			$this->session->set_flashdata('error','Community you selected doesn\'t exist.');
			redirect('webmanager/community/manage');
		
		}
	
	
	}
	
	
	#--------------------------------------------->>add_update_comm view loading<<-------------------------------------
	public function blogpost() {

		$type = $this->uri->segment(4);
		$post_id = $this->uri->segment(5);
		$community = $this->master->getRecords('community',array('permalink'=>$type));
		$blog_post = $this->master->getRecords('community_blog',array('id'=>$post_id));

		if(count($blog_post)> 0){

			$post_info = array(
				'blog_title' => $blog_post[0]['title'],
				'blog_id' => $blog_post[0]['id'],
				'blog_content' => $blog_post[0]['content'],
				'blog_type' => $blog_post[0]['type']
			);
		}else{
			$post_info = array(
				'blog_title' => '',
				'blog_id' => '',
				'blog_content' => '',
				'blog_type' => ''
			);
		}
		
		$data = array(
			'middle_content'=>'manage-community-blog-post',
			'page_title'=>'Change Password',
			'community'=>$community,
			'post_info'=>$post_info
		);	
		$this->load->view('admin/admin-view',$data);
	
	}
	
	#--------------------------------------------->>add_update_comm view loading<<-------------------------------------
	public function add_update_comm_post() {
		
		if(isset($_POST['blog_id'])){ //check if form submit
			$blog_id =  $_POST['blog_id'];
			$blog_comm_perm =  $_POST['blog_comm_perm'];
			$blog_title =  $_POST['blog_title'];
			$blog_content =  $_POST['blog_content'];
			$blog_type =  $_POST['blog_type'];
			$nowtime = date('Y-m-d H:i:s');
			
	

			if($blog_id != ''){
				$arr = array(
					'title'=>$blog_title,
					'content'=>$blog_content,
					'community_permalink'=>$blog_comm_perm,
					'type'=>$blog_type
				);
				
				
				if($this->master->updateRecord('community_blog',$arr,array('id'=>$blog_id))){
					$this->session->set_flashdata('success',' Post updated successfully. <a href="'.base_url().'community/post/'.$blog_comm_perm.'/'.$blog_id.'" target="_blank">View post</a>');
					echo $blog_id; //'success';
				}else{
					$this->session->set_flashdata('success',' Something went wrong. Please try again.');
					echo 'error';
				}
	
			}
			else{
				$arr = array(
					'title'=>$blog_title,
					'content'=>$blog_content,
					'community_permalink'=>$blog_comm_perm,
					'type'=>$blog_type,
					'date_added'=>$nowtime
				);
					
				
				if($npost_id = $this->master->insertRecord('community_blog',$arr, true)){
					$this->session->set_flashdata('success',' Post added successfully. <a href="'.base_url().'community/post/'.$blog_comm_perm.'/'.$npost_id.'" target="_blank">View post</a>');
					echo $npost_id; //'success';
				}else{
					$this->session->set_flashdata('success',' Something went wrong. Please try again.');
					echo 'error';
				}
	
			
			}
		}else{
			redirect('webmanager/community');
		}
	
	}
	#--------------------------------------------->>add_update_comm view loading<<-------------------------------------
	public function add_update_comm() {
		$name = $_POST['name'];
		$perm = $_POST['perm'];
		$comm_desc = $_POST['comm_desc'];
		$comm_wiki = $_POST['comm_wiki'];
		$id = $_POST['id'];

		$arr = array(
			'name'=>$name,
			'description'=>$comm_desc,
			'wiki_link'=>$comm_wiki,
			'permalink'=>$perm
		);
	
		if($id != ''){
			if($this->master->updateRecord('community',$arr,array('id'=>$id))){
				$this->session->set_flashdata('success',' Community updated successfully');
				echo 'success';
			}else{
				$this->session->set_flashdata('success',' Something went wrong. Please try again.');
				echo 'error';
			}
	
		}else{
			
			
			$communities = $this->master->getRecords('community', array('permalink'=>$perm));
			if(count($communities) > 0){
				$this->session->set_flashdata('error',' Community permalink must be unique.');
				echo 'success';
				return false;
			}//check if already exist
			
			
			
			if($this->master->insertRecord('community',$arr)){
				$this->session->set_flashdata('success',' Community added successfully');
				echo 'success';
			}else{
				echo 'error';
			}
	
		
		}
	}
	
	#--------------------------------------------->>delete view loading<<-------------------------------------
	public function delete() {
		$id = $this->uri->segment(4);

		if($this->master->deleteRecord('community','id',$id)) {	
		
			$success_mess = 'Community successfully deleted';
			$this->session->set_flashdata('success',$success_mess);
			redirect('webmanager/community/manage');
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
			redirect('webmanager/community/manage');
		}
	}
	#--------------------------------------------->>deleteblog view loading<<-------------------------------------
	public function deleteblog() {
		$id = $this->uri->segment(4);
		$perm = $this->uri->segment(5);

		if($this->master->deleteRecord('community_blog','id',$id)) {	
		
			$success_mess = 'Post successfully deleted';
			$this->session->set_flashdata('success',$success_mess);
			redirect('webmanager/community/contents/'.$perm);
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
			redirect('webmanager/community/contents/'.$perm);
		}
	}

}