<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productowners extends CI_Controller

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}





	#--------------------------------------------->>manage view agencies<<-------------------------------------
	public function index() {
		$agency_id = $this->uri->segment(4);

		$whr = array(
			'domain !='=>'',
			'user_type'=>'1'
		);
		$users = $this->master->getRecords('agency', $whr, '*', array('id'=>'DESC'));
		$q_products = $this->master->getRecords('q_products');


		$single = 'Product Owner';
		$title = 'Product Owners';



		$data = array(
			'middle_content'=>'manage-productowner',
			'title'=>$title,
			'singular_title'=>$single,
			'q_products'=>$q_products,
			'users'=>$users
		);
		$this->load->view('admin/admin-view',$data);


	}




}
