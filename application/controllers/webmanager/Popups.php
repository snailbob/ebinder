<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Popups extends CI_Controller

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}

	public function manage(){

		$messages = $this->master->getRecords('validation_messages');

		$data = array(
			'page_title'=>'Manage CC',
			'middle_content'=>'manage-popups',
			'messages'=>$messages
		);

		$this->load->view('admin/admin-view',$data);

	}
	
	public function addupdate(){
		$id = $_POST['id'];
		$desc = $_POST['description'];
		$succ = $_POST['success'];
		$err = $_POST['error'];
		$conf = $_POST['confirm'];
		
		
		$arr = array(
			'confirm_message'=>$conf,
			'error_message'=>$err,
			'description'=>$desc,
			'success_message'=>$succ
		);
		
		$this->master->updateRecord('validation_messages', $arr, array('id'=>$id));
		$this->session->set_flashdata('success',' Message updated successfully.');
		echo json_encode($arr);
		
	}
	
}

?>