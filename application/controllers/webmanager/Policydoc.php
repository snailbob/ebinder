<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Policydoc extends CI_Controller

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/policydoc/manage');

	}


	/*--------------------------------
		manage view loading
	--------------------------------*/
	public function manage() {
		$user_session = $this->session->all_userdata();
		$whr = array();

		if(isset($user_session['logged_admin_agency'])){
			$whr = array(
				'agency_id'=>$user_session['logged_admin_agency']
			);
		}

		$policy_docs = $this->master->getRecords('policy_docs', $whr,'*',array('date_updated'=>'DESC'));

		$data = array(
			'middle_content'=>'manage-policydocs',
			'title'=>'Policy Documents',
			'singular_title'=>'Policy',
			'policy_docs'=>$policy_docs
		);
		$this->load->view('admin/admin-view',$data);


	}

	public function deletedoc(){
		$lnk = $_POST['lnk'];
		$len = explode('/', $lnk);
		$file_len = count($len) - 1;
		$thefilename = $len[$file_len];

		$policy_docs = $this->master->getRecords('policy_docs', array('name'=>$thefilename));
		$filename = array();
		$id = array();
		if(count($policy_docs) > 0){
			foreach($policy_docs as $r=>$value){
				$filename[] = $value['name'];
				$id[] = $value['id'];
			}
		}
		$count = 0;
		if(count($filename) > 0){
			foreach($filename as $r){
				@unlink('uploads/policyd/'.$r);
				$this->master->updateRecord('policy_docs',array('name'=>''), array('id'=>$id[$count]));
				// $this->master->deleteRecord('policy_docs','id',$id[$count]);
				$count++;
			}
		}
		echo 'ok';

	}

	#--------------------------------------------->>delete view loading<<-------------------------------------
	public function delete() {
		$id = $this->uri->segment(4);

		if($this->master->deleteRecord('faq','id',$id)) {

			$success_mess = 'FAQ successfully deleted';
			$this->session->set_flashdata('success',$success_mess);
			redirect('webmanager/faq/manage');
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
			redirect('webmanager/faq/manage');
		}
	}



}
