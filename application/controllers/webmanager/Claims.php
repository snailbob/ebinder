<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Claims extends CI_Controller

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;

	}


	public function index() {

		$admin_info = $this->master->getRecords('admin');

		$agency_id = $this->session->userdata('logged_admin_agency');



		$user_session = $this->session->all_userdata();

		$type = $this->uri->segment(3);

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);

		$base_link = $this->common->base_url();

		$broker_id = $user_session['id'];

		$claims = $this->master->getRecords('claims', array('agency_id'=>$agency_id));

		$uniquetime= time().mt_rand();


		$whr_first = array(
			'super_admin'=>'Y',
			'agency_id'=>$agency_id,
		);
		$first_under = $this->common->customer_format(NULL, $whr_first);



		$data = array(
			'middle_content'=>'manage-claims',
			'title'=>'Manage Claims',
			'admin_info'=>$admin_info,
			'first_under'=>$first_under,
			'claims'=>$claims
		);

		$this->load->view('admin/admin-view',$data);

	}


	public function eachclaim() {

		$admin_info = $this->master->getRecords('admin');

		$agency_id = $this->session->userdata('logged_admin_agency');



		$user_session = $this->session->all_userdata();

		$type = $this->uri->segment(4);

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);

		$base_link = $this->common->base_url();

		$broker_id = $user_session['id'];

		$claims = $this->master->getRecords('claims', array('id'=>$type));
		$brokerr = $this->common->customer_format($claims[0]['broker_id']);


		$whr_first = array(
			'customer_type'=>'N',
			'agency_id'=>$claims[0]['agency_id'],
		);
		$first_under = $this->master->getRecords('customers', $whr_first, '*', array('first_underwriter'=>'ASC'));
		$underwriter = $this->common->customer_format($first_under[0]['id']);

		$messages = $this->master->getRecords('claims_message_file', array('message_id'=>$type));

		$to = $claims[0]['broker_id'];


		$uniquetime= time().mt_rand();


		$data = array(
			'middle_content'=>'manage-claims-each',
			'title'=>'Manage Claims',
			'admin_info'=>$admin_info,
			'user'=>$broker,
			'message_id'=>$type,
			'to'=>$to,
			'messages'=>$messages,
			'underwriter'=>$underwriter,
			'broker'=>$brokerr,
			'claims'=>$claims
		);

		$this->load->view('admin/admin-view',$data);

	}



}
