<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller 

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	/*************************
	about
	*************************/
	
	public function aboutus(){


		$about_us = $this->master->getRecords('pages_content',array('page_type'=> 'aboutus', 'name'=>''));
		$nguyen = $this->master->getRecords('pages_content',array('name'=> 'nguyen'));
		$stooke = $this->master->getRecords('pages_content',array('name'=> 'stooke'));
		$advisors = $this->master->getRecords('pages_content',array('html_tag'=> 'h4', 'page_type'=> 'aboutus'));

		$data = array(
			'middle_content'=>'manage-aboutus',
			'page_title'=>'Change Password',
			'about_us'=>$about_us,
			'nguyen'=>$nguyen,
			'stooke'=>$stooke,
			'advisors'=>$advisors
		);
		
		$this->load->view('admin/admin-view',$data);

	}
	
	
	public function save_about(){
		$id = $_POST['pk'];
		$new_content = $_POST['value'];
		
		$this->master->updateRecord('pages_content', array('content'=>$new_content), array('id'=> $id));
		$data['id'] = $id;
		$data['value'] = $new_content;
		echo json_encode($data);
		
	}
	
	public function save_advisor(){
		$id = $_POST['pk'];
		$tag = $_POST['tag'];
		$class = $_POST['class'];
		
		$the_class = '';
		foreach($class as $value){
			if(substr($value, 0, 8) != 'editable'){
				$the_class .= $value.' ';
			}
		}
		
		$name = $_POST['name'];
		$new_content = $_POST['value'];
		
		$data['classes'] = $the_class;
		$data['html_tag'] = $tag;
		$data['name'] = $name;
		$data['content'] = $new_content;
		$data['page_type'] = 'aboutus';
		
		$the_advi = $this->master->getRecords('pages_content', array('name'=>$name, 'html_tag'=>$tag, 'classes'=>$the_class));
		
		if(count($the_advi) > 0){
			$this->master->updateRecord('pages_content', $data, array('id'=>$the_advi[0]['id']));
		}
		else{
			$this->master->insertRecord('pages_content', $data);
		}
		echo json_encode($data);
	}

}