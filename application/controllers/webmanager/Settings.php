<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Settings extends CI_Controller {

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}

	public function commss(){
		$wow = array();

		for($i = 1; $i < 11; $i++){
			$wow[] = $i;
		}

		$this->master->updateRecord('admin_login', array('commission_rate'=>serialize($wow),'commission_rate_manual'=>serialize($wow)), array('id'=>'1'));
	}



	/*---------------------------------------------
				resetdb
	----------------------------------------------*/
	public function resetdb() {

		echo 'uri-4 must be "delete"';
		if($this->uri->segment(4) == 'delete'){ //isset($_POST['action'])){
			/*do not include
			admin_login,
			countries,
			countries_currency,
			country_t,
			email_id_master,
			forum_category_master
			outgoing_emails
			pages_content


			*/

			//custom delete organization_master
			$dbname = 'community';
			$this->master->deleteRecord($dbname,'id >','7');
			$reset_query = 'ALTER TABLE '.$dbname.' AUTO_INCREMENT = 8';
			$this->db->query($reset_query);
			echo $dbname.' - reset/empty success<br>';


			$dbs = array(
				//'community_blog',
				'faq',
				'forum_post_master',
				'goods_list',
				'notifications',
				'private_messages',
				'private_message_file',
				//'references',
				'reviews',
				'service_list',
				'sms',
				'social_verification',
				'space_list',
				'space_list_file',
				'space_seekers',
				'stripe_webhook',
				'task_job',
				'task_job_file',
				'task_offers'
			);


			foreach($dbs as $dbname){

				$query = 'DELETE FROM '.$dbname;
				$reset_query = 'ALTER TABLE `'.$dbname.'` AUTO_INCREMENT = 1';
				$this->db->query($query);
				$this->db->query($reset_query);
				echo $dbname.' -  reset/empty success<br>';
			}

			//$this->session->set_flashdata('success','Database reset successful.');

			return false;

		}//reset end


//		$admin_details = $this->master->getRecords('admin_login');
//		$data = array(
//			'middle_content'=>'reset-db-view',
//			'page_title'=>'Reset Database',
//		);
//
//		$this->load->view('admin/admin-view',$data);


	}



	public function changePassword(){



		if(isset($_POST['change_admin_pw'])){


			$pass = $this->input->post('password');
			$pass2 = $this->input->post('password2');
			$admin_email = $this->input->post('email');
			$recovery_email = $this->input->post('recovery_email');


			if($pass != $pass2){
				$this->session->set_flashdata('error','Password didn\'t match.');
				redirect(base_url().'webmanager/settings/changePassword');
				return false;
			}

			$data_array = array(
				'password'=>md5($pass),
				'recovery_email'=>$recovery_email,
				'email'=>$admin_email
			);


			if($this->master->updateRecord('admin',$data_array,array('id'=>'2'))){

				$this->session->set_flashdata('success','Infomation updated successfully.');
				redirect(base_url().'webmanager/settings/changePassword');

			}

			else{

				$this->session->set_flashdata('error','Error while updating information');

			}


		}


		$admin_details = $this->master->getRecords('admin');



		$data = array(
			'middle_content'=>'change-password',
			'page_title'=>'Change Password',
			'admin_details'=>$admin_details
		);

		$this->load->view('admin/admin-view',$data);


	}











	/*--------------------------------------
		Function for updating emails
	-----------------------------------*/

	public function manageEmail(){

		if(isset($_POST['admin_update_email'])){

			$this->form_validation->set_rules('contact_email','Contact Email','required|xss_clean|valid_email');
			$this->form_validation->set_rules('info_email','info email','required|xss_clean|valid_email');
			$this->form_validation->set_rules('support_email','Support Email','required|xss_clean|valid_email');



			if($this->form_validation->run()){

				$contact_email=$this->input->post('contact_email','',true);
				$info_email=$this->input->post('info_email','',true);
				$support_email=$this->input->post('support_email','',true);

				$data_array = array(
					'contact_email '=>$contact_email,
					'info_email'=>$info_email,
					'support_email'=>$support_email
				);



				if($this->master->updateRecord('email_id_master',$data_array,array('id'=>'1'))){

					$this->session->set_flashdata('success','Admin Email updated successfully.');
					redirect(base_url().'webmanager/settings/manageEmail/');

				}

				else{

					$this->session->set_flashdata('error','Error while updating Admin Email');

				}

			}

		}

		$admin_email = $this->master->getRecords('email_id_master');

		$data = array(
			'middle_content'=>'update-admin-email',
			'page_title'=>'Update Admin Email',
			'admin_email'=>$admin_email
		);

		$this->load->view('admin/admin-view',$data);

	}



	public function logo(){
		$agency_id = $this->uri->segment(4);
		$agency = $this->master->getRecords('agency', array('id'=>$agency_id));
		$agencyname = $this->common->db_field_id('agency', 'name', $agency_id);

		$data = array(
			'middle_content'=>'logo-settings-view',
			'title'=>'Upload '.$agencyname.' Logo',
			'agency'=>$agency,
			'agency_id'=>$agency_id,
		);

		$this->load->view('admin/admin-view',$data);
	}

	public function baserate(){
		$admin_details = $this->master->getRecords('admin');

		if(isset($_POST['base_rate'])){
			$this->master->updateRecord('admin', array('commission_rate'=>$_POST['base_rate']), array('id'=>'2'));

			echo json_encode(array('id'=>'2'));
			return false;
		}

		$data = array(
			'middle_content'=>'settings-baserate-view',
			'admin'=>$admin_details,
			'title'=>'Customer Base Rate'
		);

		$this->load->view('admin/admin-view',$data);
	}


	public function users() {
		$type = 'brokers'; //$this->uri->segment(4);
		$uid = $this->uri->segment(5);
		$agency_id = $this->common->sess_agency_id();
		$utype = ($type == 'brokers') ? 'N' : 'Y';
		$logged_id = $this->session->userdata('id');

		$countries = $this->common->country_format();

		$name = ($uid != '') ? $this->common->db_field_id('customers', 'first_name', $uid).' '.$this->common->db_field_id('customers', 'last_name', $uid) : '';
		$ss = ($type == 'brokers') ? 'User' : 'Customer';
		$single = $name.' '.$ss;
		$title = $single.'s';

		$access_rights = $this->common->access_rights();

		$whr = array(
			'customer_type'=>$utype,
			// 'id !='=>$logged_id,
			'super_admin'=>'Y',
		);


		if($agency_id != '6'){
			$whr['agency_id']=$agency_id;
		}

		if($uid != ''){
			$whr['studio_id'] = $uid;
			$users = $this->master->getRecords('customers', $whr, '*', array('id'=>'DESC'));
		}
		else{

			$users = $this->master->getRecords('customers', $whr, '*', array('id'=>'DESC'));
		}

		$brokerage = $this->master->getRecords('brokerage');
		$data = array(
			'middle_content'=>'manage-underwriters',
			'title'=>$title,
			'brokerage'=>$brokerage,
			'singular_title'=>$single,
			'access_rights'=>$access_rights,
			'countries'=>$countries,
			'users'=>$users
		);
		$this->load->view('admin/admin-view',$data);

	}



	public function activitylog() {

		$admin_info = $this->master->getRecords('admin');

		$activity_logged = $this->session->userdata('activity_logged');
		$agency_id = $this->session->userdata('logged_admin_agency');

		$logs = (empty($agency_id)) ? $this->master->getRecords('activity_log', '', '*', array('id'=>'DESC')) : $this->master->getRecords('activity_log', array('agency_id'=>$agency_id), '*', array('id'=>'DESC'));

		$activity_log = ($activity_logged == 'Y') ? $logs : array();
		$data = array(
			'middle_content'=>'manage-activitylog',
			'title'=>'Acitivity Log',
			'admin_info'=>$admin_info,
			'activity_log'=>$activity_log
		);

		$this->load->view('admin/admin-view',$data);

	}

	public function agencyprofile() {

		$admin_info = $this->master->getRecords('admin');
		$agency_id = $this->session->userdata('agency_id');
		$agency = $this->master->getRecords('agency', array('id'=>$agency_id));

		$data = array(
			'middle_content'=>'settings-agencyprofile',
			'title'=>'Agency Profile',
			'admin_info'=>$admin_info,
			'agency'=>$agency,
		);

		$this->load->view('admin/admin-view',$data);

	}



	public function profile(){


		$id = $this->session->userdata('id');

		$user = $this->common->customer_format($id);

		$brokerage = $this->master->getRecords('brokerage', array('id'=>$user['brokerage_id']));

		$countries = $this->common->country_format();
		$single = 'My Profile';
		$title = $single;

		$data = array(
			'middle_content'=>'settings-my-profile',
			'title'=>$title,
			'singular_title'=>$single,
			'countries'=>$countries,
			'user'=>$user,
			'brokerage'=>$brokerage
		);


		$this->load->view('admin/admin-view',$data);
	}



		public function password(){
			$user_session = $this->session->all_userdata();

			$success = $this->session->flashdata('success');
			$danger = $this->session->flashdata('danger');
			$info = $this->session->flashdata('info');

			$id = $user_session['id'];

			$base_link = $this->common->base_url();
			$data = array(
				'middle_content'=>'social_email_password',
				'id'=>$id,
				'user_sess'=>$user_session,
				'title'=>'Change Password',
				'singular'=>'Change Password',
				'success'=>$success,
				'danger'=>$danger,
				'info'=>$info,
				'base_link'=>$base_link
			);


	//		echo json_encode($mymembers);
	//		return false;
			$this->load->view('admin/admin-view',$data);



		}

}
