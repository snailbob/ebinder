<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Referral extends CI_Controller

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	/*------------------------------------
	index
	-------------------------------------*/
	public function index() {

		redirect(base_url().'webmanager/products/manage');

	}


	/*------------------------------------
	manage
	-------------------------------------*/
	public function manage() {
		$admin = $this->master->getRecords('admin');

		$homebiz1 = (@unserialize($admin[0]['biz_referral'])) ? unserialize($admin[0]['biz_referral']) : array();
		$data = array(
			'middle_content'=>'manage-referral-biz',
			'title'=>'Manage referral points',
			'singular_title'=>'Referral Manager',
			'homebiz1'=>$homebiz1,
			'admin'=>$admin
		);
		$this->load->view('admin/admin-view',$data);

	}



	/*------------------------------------
	submitted
	-------------------------------------*/
	public function submitted() {
		$user_session = $this->session->all_userdata();

		$whr = array(
			'agency_id'=>$user_session['agency_id'],
			'needs_referral'=>'Y',
			'bound'=>'N'
		);
		$format_referral = $this->common->format_referral($whr);

		$data = array(
			'middle_content'=>'manage-referral-submitted-biz',
			'title'=>'Referrals',
			'singular_title'=>'Referral',
			'format_referral'=>$format_referral
		);

		$this->load->view('admin/admin-view',$data);

	}


	/*------------------------------------
	quote
	-------------------------------------*/
	public function quote() {
		$biz_referral = $this->master->getRecords('biz_referral', array('needs_referral'=>'N'));

		$data = array(
			'middle_content'=>'manage-referral-quote-biz',
			'title'=>'Quotes',
			'singular_title'=>'Quote',
			'biz_referral'=>$biz_referral
		);

		$this->load->view('admin/admin-view',$data);

	}





	/*------------------------------------
	questionnaires
	-------------------------------------*/
	public function questionnaires(){
		$id = $this->uri->segment(4);
		$product = $this->master->getRecords('q_products', array('id'=>$id));
		$questionnaires = $this->master->getRecords('questionnaires', array('product_id'=>$id));

		if(count($product) == 0){
			$this->session->set_flashdata('error','No product found.');
			redirect('webmanager/products');
			return false;
		}

		$data = array(
			'middle_content'=>'manage-questionnaires',
			'title'=>$product[0]['product_name'].' Questionnaire',
			'singular_title'=>'Question',
			'products'=>$product,
			'product_id'=>$id,
			'questionnaires'=>$questionnaires
		);
		$this->load->view('admin/admin-view',$data);
	}

	/*------------------------------------
	save_product
	-------------------------------------*/
	public function save_product() {
		$product_name = $_POST['product_name'];
		$helper = $_POST['helper'];
		$id = $_POST['id'];
		$broker_id = $_POST['broker_id'];


		$nowtime = date('Y-m-d H:i:s');
		$msg = 'error';

		$arr = array(
			'product_name'=>$product_name,
			'helper'=>$helper,
			'broker_id'=>$broker_id,
		);

		if($id != ''){
			if($this->master->updateRecord('q_products',$arr, array('id'=>$id))){
				$this->session->set_flashdata('success',' Product updated successfully');
				$msg = 'success';
			}
		}
		else{
			$arr['date_added'] = $nowtime;
			$this->master->insertRecord('q_products',$arr);

			$this->session->set_flashdata('success',' Product added successfully');
			$msg = 'success';
		}

		echo json_encode($arr);

	}


	/*------------------------------------
	save_question
	-------------------------------------*/
	public function save_question() {
		$question = $_POST['question'];
		$id = $_POST['id'];
		$broker_id = $_POST['broker_id'];
		$product_id = $_POST['product_id'];


		$nowtime = date('Y-m-d H:i:s');
		$msg = 'error';

		$arr = array(
			'question'=>$question,
			'broker_id'=>$broker_id,
			'product_id'=>$product_id,
		);

		if($id != ''){
			if($this->master->updateRecord('questionnaires',$arr, array('id'=>$id))){
				$this->session->set_flashdata('success',' Question updated successfully');
				$msg = 'success';
			}
		}
		else{
			$arr['date_added'] = $nowtime;
			$this->master->insertRecord('questionnaires',$arr);

			$this->session->set_flashdata('success',' Question added successfully');
			$msg = 'success';
		}

		echo json_encode($arr);

	}


	public function get_biz_referral(){
		$id = $_POST['id'];
		$homebizdata = $this->common->homebiz_data();

		$arr['id'] = $id;

		$biz_referral = $this->master->getRecords('biz_referral', $arr);

		$arr['content'] = (isset($biz_referral[0]['content'])) ? unserialize($biz_referral[0]['content']) : array();
		$arr['customer'] = (isset($biz_referral[0]['customer'])) ? unserialize($biz_referral[0]['customer']) : array();
		$arr = array_merge($arr, $homebizdata);

		$arr['theview'] = $this->load->view('admin/referral-each-view', $arr, true);
		echo json_encode($arr);

	}


	/*------------------------------------
	policies
	-------------------------------------*/
	public function policies() {
		$biz_referral = $this->master->getRecords('biz_referral', array('needs_referral'=>'Y'));
		$user_session = $this->session->all_userdata();

		$whr = array(
			'agency_id'=>$user_session['logged_admin_agency'],
			'needs_referral'=>'N',
			// 'bound'=>'Y'
		);
		$format_referral = $this->common->format_referral($whr);

		//echo json_encode($format_referral); return false;

		$data = array(
			'middle_content'=>'manage-submitted-policies',
			'title'=>'Policies',
			'singular_title'=>'Policy',
			'biz_referral'=>$biz_referral,
			'format_referral'=>$format_referral
		);

		$this->load->view('admin/admin-view',$data);

	}


	/*------------------------------------
	transactions
	-------------------------------------*/
	public function transactions() {
		$biz_referral = $this->master->getRecords('biz_referral', array('needs_referral'=>'Y'));
		$user_session = $this->session->all_userdata();

		$whr = array(
			'agency_id'=>$user_session['logged_admin_agency'],
			'endorse_details !='=>'',
			'db_table'=>'biz_renewal_history'
		);
		$format_referral = $this->common->format_referral($whr);

		//echo json_encode($format_referral); return false;

		$data = array(
			'middle_content'=>'manage-submitted-transaction',
			'title'=>'Transactions',
			'singular_title'=>'Transaction',
			'biz_referral'=>$biz_referral,
			'format_referral'=>$format_referral
		);

		$this->load->view('admin/admin-view',$data);

	}


}
