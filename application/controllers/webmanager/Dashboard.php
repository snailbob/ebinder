<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Dashboard extends CI_Controller {


	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	/*-------------------------------------
		index
	-------------------------------------*/
	public function index(){
		//redirect('admin/dashboard/users');

		//seekers

		$user_session = $this->session->all_userdata();

		$agency_id = (isset($user_session['logged_admin_agency'])) ? $user_session['logged_admin_agency'] : '0';

		$whr = array(
			'agency_id'=>$agency_id,
			'needs_referral'=>'N',
			'bound'=>'Y'
		);
		$user_id = (isset($user_session['id'])) ? $user_session['id'] : '0';
		$format_referral = $this->common->format_referral($whr);
		$q_widget = $this->master->getRecords('q_widget', array('broker_id'=>$user_id));


		$total_premium_all = $this->common->total_premium_all($format_referral);
		$widget_topbrokers = $this->common->widget_topbrokers();
		// echo json_encode($widget_topbrokers);return false;

		$data = array(
			'title'=>'E-binder',
			'middle_content'=>'dashboard',
			'format_referral'=>$format_referral,
			'total_premium_all'=>$total_premium_all,
			'widget_topbrokers'=>$widget_topbrokers,
			'q_widget'=>$q_widget
		);

		$this->load->view('admin/admin-view',$data);

	}

	/*-------------------------------------
		referrals
	-------------------------------------*/
	public function referrals(){
		//redirect('admin/dashboard/users');

		//seekers
		$insurances = $this->master->getRecords('bought_insurance', array('status !='=>'P'), '', array('status'=>'DESC'));

		$data = array(
			'title'=>'Policy Referrals',
			'middle_content'=>'manage-referrals',
			'insurances'=>$insurances

		);

		$this->load->view('admin/admin-view',$data);

	}

	/*-------------------------------------
		transactions
	-------------------------------------*/
	public function transactions(){
		//redirect('admin/dashboard/users');

		//seekers
		$insurances = $this->master->getRecords('bought_insurance', array('status'=>'P'), '', array('status'=>'DESC'));

		$data = array(
			'title'=>'Transactions',
			'middle_content'=>'manage-transactions',
			'insurances'=>$insurances

		);

		$this->load->view('admin/admin-view',$data);

	}

	public function accept_request(){
		//premium=1.57&id=21&comment=sdff
		$premium = $_POST['premium'];
		$id = $_POST['id'];
		$comment = $_POST['comment'];

		$insurances = $this->master->getRecords('bought_insurance', array('id'=>$id));

		$details = unserialize($insurances[0]['details']);
		$details['premium'] = round($premium, 2);
		$arr = array(
			'comment'=>$comment,
			'details'=>serialize($details),
			'status'=>'A'
		);

		$this->master->updateRecord('bought_insurance', $arr, array('id'=>$id));

		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings for agency
		$info_arr = array(
			'to'=>$insurances[0]['email'], //$adminemail,
			'from'=>$adminemail,
			'subject'=>'Welcome to Transit Insurance',
			'view'=>'referral-acceptance-to-user',
			'emailer_file_name'=>'referral-acceptance-to-user',
		);


		$other_info = array(
			'password'=>'',
			'view'=>'referral-acceptance-to-user',
			'emailer_file_name'=>'referral-acceptance-to-user',
			'name'=>'',//$first.' '.$last,
			'agency_name'=>'Admin',
			'user_name'=>$adminemail,
			'user_email'=>$adminemail,
			'link'=>'landing/purchasepolicy/'.$id.'/'.md5($insurances[0]['email'])
		);

		$data['info_arr1'] = $info_arr;
		$data['other_info1'] = $other_info;
		$this->emailer->sendmail($info_arr,$other_info);
		$arr['data'] = $data;

		$this->session->set_flashdata('ok', ' Referral request successfully accepted.');
		redirect(base_url().'webmanager/dashboard/referrals');
		//echo json_encode($arr);
	}

	public function send_certificate(){
		$id = $_POST['id'];
		$insurances = $this->master->getRecords('bought_insurance', array('id'=>$id));

		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings for agency
		$info_arr = array(
			'to'=>$insurances[0]['email'], //$adminemail,
			'from'=>$adminemail,
			'subject'=>'Welcome to Transit Insurance',
			'view'=>'send-certifiicate-to-user',
			'emailer_file_name'=>'send-certifiicate-to-user',
		);

		$other_info = array(
			'password'=>'',
			'view'=>'send-certifiicate-to-user',
			'emailer_file_name'=>'send-certifiicate-to-user',
			'name'=>'',//$first.' '.$last,
			'agency_name'=>'Admin',
			'user_name'=>$adminemail,
			'user_email'=>$adminemail,
			'link'=>'landing/download/'.$id, //landing/purchasepolicy/'.$id.'/'.md5($insurances[0]['email'])
		);

		$data['info_arr1'] = $info_arr;
		$data['other_info1'] = $other_info;
		$this->emailer->sendmail($info_arr,$other_info);

		echo json_encode($other_info);
	}

	public function reject_request(){
		$id = $this->uri->segment('4');
		$insurances = $this->master->getRecords('bought_insurance', array('id'=>$id));

		$arr = array(
			'status'=>'R'
		);
		$this->master->updateRecord('bought_insurance', $arr, array('id'=>$id));

		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings for agency
		$info_arr = array(
			'to'=>$insurances[0]['email'], //$adminemail,
			'from'=>$adminemail,
			'subject'=>'Welcome to Transit Insurance',
			'view'=>'referral-rejected-to-user',
			'emailer_file_name'=>'referral-rejected-to-user',
		);


		$other_info = array(
			'password'=>'',
			'view'=>'referral-rejected-to-user',
			'emailer_file_name'=>'referral-rejected-to-user',
			'name'=>'',//$first.' '.$last,
			'agency_name'=>'Admin',
			'user_name'=>$adminemail,
			'user_email'=>$adminemail,
			'link'=>'dashboard', //landing/purchasepolicy/'.$id.'/'.md5($insurances[0]['email'])
		);

		$data['info_arr1'] = $info_arr;
		$data['other_info1'] = $other_info;
		$this->emailer->sendmail($info_arr,$other_info);

		$this->session->set_flashdata('ok', ' Request successfully rejected.');
		redirect(base_url().'webmanager/dashboard');
	}




	/*-----------------------------------------
		Admin login page
	-----------------------------------------*/
	public function login(){

		$error="";

		//if(isset($_SESSION['logged_admin']))
		if($this->session->userdata('logged_admin') != ''){

			redirect(base_url().'webmanager/dashboard');
		}

		else{

			if(isset($_POST['btn_admin_login'])){

				$email = $this->input->post('user_name',true);
				$pw = $this->input->post('pass_word',true);
				$chk_arr = array(
					'name'=>$email,
					'password'=>md5($pw)
				);
				$row = $this->master->getRecords('admin',$chk_arr);
				if(count($row) > 0){

					//$_SESSION['logged_admin'] = $row[0]['email'];

					$this->session->set_userdata('logged_admin', $row[0]['name']);
					$this->session->set_userdata('logged_admin_email', $row[0]['email']);

					redirect(base_url().'webmanager/dashboard');

				}

				else{
					$error = "Invalid Credentials";
				}


			}


			$data = array(
				'page_title'=>'Admin Login',
				'error'=>$error
			);

			$this->load->view('admin/index',$data);


		}

	}




	/*-----------------------------------------
		back_to_admin
	-----------------------------------------*/
	public function back_to_admin(){

		$row = $this->master->getRecords('admin');
		if(count($row) > 0 && $this->session->userdata('is_admin')){


			$user_data = $this->session->all_userdata();
			foreach ($user_data as $key => $value) {
				$this->session->unset_userdata($key);
			}

			$this->session->set_userdata('logged_admin', $row[0]['name']);
			$this->session->set_userdata('logged_admin_email', $row[0]['email']);

		}

		redirect(base_url().'webmanager/dashboard');


	}






	/*----------------------------------------------
				Admin Forgot password
	-----------------------------------------------*/

	public function forgotpassword(){

		$whr = array('id'=>'2');

		$email_id = $this->master->getRecords('admin',$whr,'*');

		$data = array(
			'page_title'=>"Forget Password",
			'middle_content'=>'forget-password',
			'success'=>'',
			'error'=>''
		);


		if(isset($_POST['btn_forget'])){


			$email = $this->input->post('user_name',true);

			if($email == $email_id[0]['email']){

				$info_arr = array(
					'from'=>$email_id[0]['email'],
					'to'=>$email_id[0]['email'],
					'subject'=>'Password Recovery',
					'view'=>'forget-password-mail-to-admin'
				);

				$other_info = array(
					'password'=>'Darknite1',//$email_id[0]['pass_word'],
					'email'=>$email
				);

				$this->email_sending->sendmail($info_arr,$other_info);
				$data['success']='Password mail send successfully.';
			}

			else{
				$data['error'] = 'Email Address is invalid.';
			}

		}

		$this->load->view('admin/forget-password',$data);

	}


	/*-----------------------------
			Admin logout
	-----------------------------*/

	public function logout(){

		//unset($_SESSION['logged_admin']);
//		$theuser = $this->session->userdata('logged_admin_brokerage');
//
//		$this->session->unset_userdata('logged_admin');
//		$this->session->unset_userdata('logged_admin_email');
//		$this->session->unset_userdata('logged_admin_brokerage');
//		$this->session->unset_userdata('logged_admin_id');
//		$this->session->unset_userdata('logged_admin_agency');

		$user_data = $this->session->all_userdata();


		//logs
		$log_activity = array(
			'name'=>$user_data['logged_admin'].' logout from ebinder.',
			'type'=>'logout',
			'details'=>serialize($user_data)
		);
		$this->common->insert_activity_log($log_activity);


		foreach ($user_data as $key => $value) {
			$this->session->unset_userdata($key);
		}

		if($this->common->the_domain() == '' || $this->common->the_domain() == 'localhost'){
			redirect('webmanager');
		}
		else{
			redirect(base_url());
		}


	}



}
