<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contents extends CI_Controller

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	/*--------------------------------
		manage view faq
	--------------------------------*/
	public function faq() {
		$faqs = $this->master->getRecords('faq','','*',array('sort'=>'ASC'));
		$faqs_seekers = $this->master->getRecords('faq',array('for_seeker'=>'Y'),'*',array('sort'=>'ASC'));
		$faqs_traders = $this->master->getRecords('faq',array('for_seeker'=>'N'),'*',array('sort'=>'ASC'));

		$data = array(
			'middle_content'=>'manage-faqs',
			'page_title'=>'FAQs',
			'faqs'=>$faqs,
			'faqs_seekers'=>$faqs_seekers,
			'faqs_traders'=>$faqs_traders
		);
		$this->load->view('admin/admin-view',$data);


	}


	public function add_faq() {
		$q = $_POST['question'];
		$a = $_POST['answer'];
		$faq_id = $_POST['faq_id'];

		if($faq_id !=''){
			if($this->master->updateRecord('faq',array('question'=>$q,'answer'=>$a),array('id'=>$faq_id))) {
				$success_mess = 'FAQ successfully updated';
				$this->session->set_flashdata('success',$success_mess);
				echo 'success';
			}
			else{
				$this->session->set_flashdata('error','Something went wrong. Please try again.');
				echo 'error';
			}
		}
		else{
			if($id = $this->master->insertRecord('faq',array('question'=>$q,'answer'=>$a),true)) {
				$success_mess = 'FAQ successfully added';
				$this->session->set_flashdata('success',$success_mess);
				echo 'success';
			}
			else{
				$this->session->set_flashdata('error','Something went wrong. Please try again.');
				echo 'error';
			}

		}

	}


	public function legal(){
		$admin_info = $this->master->getRecords('admin');

		$data = array(
			'middle_content'=>'manage-legals',
			'title'=>'Legal',
			'admin_info'=>$admin_info,
		);

		$this->load->view('admin/admin-view',$data);
	}


	public function legal2(){
		$admin_info = $this->master->getRecords('admin');

		$data = array(
			'middle_content'=>'manage-legals2',
			'title'=>'Common Terms &amp; Conditions',
			'admin_info'=>$admin_info,
		);

		$this->load->view('admin/admin-view',$data);
	}

	public function bannertext(){

		$thefield = 'banner_text';
		$admin_info = $this->master->getRecords('admin','', $thefield);

		$data = array(
			'middle_content'=>'manage-contenttext',
			'title'=>'Banner Text',
			'admin_info'=>$admin_info,
			'thefield'=>$thefield
		);

		$this->load->view('admin/admin-view',$data);

	}

	public function pagetext(){

		$thefield = 'light_section';
		$admin_info = $this->master->getRecords('admin','', $thefield);

		$data = array(
			'middle_content'=>'manage-contenttext',
			'title'=>'Page Text',
			'admin_info'=>$admin_info,
			'thefield'=>$thefield
		);

		$this->load->view('admin/admin-view',$data);

	}

	public function savetexts(){
		$contentfield = $_POST['contentfield'];
		$title = $_POST['title'];
		$titlefield = $_POST['titlefield'];
		$type = $_POST['type'];
		$id = $_POST['id'];

		$arr = array(
			'title'=>$titlefield,
			'content'=>$contentfield
		);

		if($id == ''){
			$arr['type'] = $type;
			$id = $this->master->insertRecord('admin_contents', $arr, true);
			$arr['id'] = $id;
		}
		else{
			$this->master->updateRecord('admin_contents', $arr, array('id'=>$id));
		}


		echo json_encode($arr);

	}

	public function aboutus(){

		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'aboutus';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));

		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'About Us Footer',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);

		$this->load->view('admin/admin-view',$data);

	}
	public function aboutuspage(){

		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'aboutuspage';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));

		$data = array(
			'middle_content'=>'manage-aboutuspage',
			'title'=>'About Us Page',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);

		$this->load->view('admin/admin-view',$data);

	}

	public function firstsection(){

		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'firsthomesection';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));

		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'Home Main Message',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);

		$this->load->view('admin/admin-view',$data);

	}


	public function topheadtitle(){

		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'topheadtitle';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));

		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'Top Head Title',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);

		$this->load->view('admin/admin-view',$data);

	}
	public function mainhometitle(){

		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'mainhometitle';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));

		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'Main Home Title',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);

		$this->load->view('admin/admin-view',$data);

	}




	public function alternatehead(){

		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'alternatingcontenthead';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));

		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'Alternating Content Head',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);

		$this->load->view('admin/admin-view',$data);

	}


	public function manage_alternates(){
		$pagetype = 'alternatingcontents';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype), '*', array('sort'=>'ASC'));

		$data = array(
			'middle_content'=>'manage-alternates',
			'title'=>'Alternates',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
		);

		$this->load->view('admin/admin-view',$data);
	}


	public function alternate(){
		$id = $this->uri->segment(4);

		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'alternatingcontents';
		$admin_info = $this->master->getRecords('admin_contents', array('id'=>$id));

		if(count($admin_info) == 0){
			$admin_info = array(array(
				'title'=>'',
				'id'=>'',
				'content'=>'',
				'image'=>''
			));
		}

		$data = array(
			'middle_content'=>'add-alternate',
			'title'=>'Alternate',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);

		$this->load->view('admin/admin-view',$data);
	}

	public function delete_alt(){
		$user_id = $this->uri->segment(4);

		if($this->master->deleteRecord('admin_contents','id',$user_id)) {
			$success_mess = 'Content successfully deleted';
			$this->session->set_flashdata('success',$success_mess);
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
		}

		redirect('webmanager/contents/manage_alternates');
	}


	public function threeparts(){
		$type = (isset($_GET['type'])) ? $_GET['type'] : 'threepartshead';
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = $type;
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));

		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>(isset($_GET['type'])) ? 'Three Columns Head Pt 2' : 'Three Columns Head',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);

		$this->load->view('admin/admin-view',$data);

	}


	public function manage_threeparts(){
		$type = (isset($_GET['type'])) ? $_GET['type'] : 'threeparts';
		$params = ($type == 'threeparts2') ? '?type=threeparts2' : '';
		$pagetype = $type;
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));

		$data = array(
			'middle_content'=>'manage-threeparts',
			'title'=>'Three Column Contents',
			'admin_info'=>$admin_info,
			'params'=>$params,
			'pagetype'=>$pagetype,
		);

		$this->load->view('admin/admin-view',$data);
	}


	public function threepart(){
		$id = $this->uri->segment(4);

		$type = (isset($_GET['type'])) ? $_GET['type'] : 'threeparts';
		$params = ($type == 'threeparts2') ? '?type=threeparts2' : '';

		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'threeparts';
		$admin_info = $this->master->getRecords('admin_contents', array('id'=>$id));

		if(count($admin_info) == 0){
			$admin_info = array(array(
				'title'=>'',
				'id'=>'',
				'content'=>'',
				'image'=>''
			));
		}

		$data = array(
			'middle_content'=>'add-alternate',
			'title'=>'Three Column Content',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'params'=>$params,
			'contentfield'=>$contentfield
		);

		$this->load->view('admin/admin-view',$data);
	}

	public function banners(){
		$pagetype = 'banner';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));

		$data = array(
			'middle_content'=>'manage-banners',
			'title'=>'Homepage Typo Content',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
		);

		$this->load->view('admin/admin-view',$data);
	}


	public function banner(){
		$id = $this->uri->segment(4);

		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'banner';
		$admin_info = $this->master->getRecords('admin_contents', array('id'=>$id));

		if(count($admin_info) == 0){
			$admin_info = array(array(
				'title'=>'',
				'id'=>'',
				'content'=>'',
				'image'=>''
			));
		}

		$data = array(
			'middle_content'=>'add-alternate',
			'title'=>'Homepage Typo Effect',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);

		$this->load->view('admin/admin-view',$data);
	}

}
