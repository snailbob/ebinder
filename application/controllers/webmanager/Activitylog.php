<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Activitylog extends CI_Controller 

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	public function index() {

		$admin_info = $this->master->getRecords('admin');
		
		$activity_logged = $this->session->userdata('activity_logged');
		$agency_id = $this->session->userdata('logged_admin_agency');

		$logs = (empty($agency_id)) ? $this->master->getRecords('activity_log', '', '*', array('id'=>'DESC')) : $this->master->getRecords('activity_log', array('agency_id'=>$agency_id), '*', array('id'=>'DESC'));
		
		$activity_log = ($activity_logged == 'Y') ? $logs : array();
		$data = array(
			'middle_content'=>'manage-activitylog',
			'title'=>'Acitivity Log',
			'admin_info'=>$admin_info,
			'activity_log'=>$activity_log
		);	
		
		$this->load->view('admin/admin-view',$data);	

	}

	

}