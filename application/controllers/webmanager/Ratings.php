<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ratings extends CI_Controller

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/jobs/manage');

	}


	#--------------------------------------------->>products view loading<<-------------------------------------
	public function products() {
		$admin_info = $this->master->getRecords('admin');
		$user_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];
		$agency_info = $this->master->getRecords('agency', array('id'=>$user_id));

		$agency_info_format = array();
		if(!empty($agency_info)){
			$ggg = array();
			foreach($agency_info as $r=>$value){
				$value['agency_details'] = (@unserialize($value['agency_details'])) ? unserialize($value['agency_details']) : array();
				$ggg = $value;
			}
			$agency_info_format = $ggg;
		}


		$prod_id = (isset($agency_info_format['agency_details']['product_id'])) ? $agency_info_format['agency_details']['product_id'] : array();


		$q_products = $this->master->getRecords('q_products');


		$data = array(
			'middle_content'=>'manage-ratings-products',
			'title'=>'Product',
			'singular_title'=>'Product',
			'agency_info'=>$agency_info_format,
			'q_products'=>$q_products,
			'prod_id'=>$prod_id,
		);

		//echo json_encode($data); return false;

		$this->load->view('admin/admin-view',$data);
	}

	#--------------------------------------------->>product view loading<<-------------------------------------
	public function product() {
		$prod_id = $this->uri->segment(4);
		$admin_info = $this->master->getRecords('admin');
		$user_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];
		$agency_info = $this->master->getRecords('agency', array('id'=>$user_id));

		$whr = array(
			'customer_type'=>'N',
			'super_admin'=>'Y',
		);
		$whr['agency_id'] = $user_id;
		$underwriters = $this->master->getRecords('customers', $whr);

		$deductibles = $this->master->getRecords('deductibles', array('agency_id'=>$user_id));
		$cargo = $this->master->getRecords('q_business_specific'); //this->master->getRecords('cargo_category');
		$transportations = $this->common->transportations();
		$countries = $this->master->getRecords('country_t');
		$currencies = $this->master->getRecords('api_currencies', array('in_stripe'=>'Y'));


		$cargo_prices = $this->common->agency_prices($user_id,'cargo_prices');
		$cargo_max = $this->common->agency_prices($user_id,'cargo_max');
		$transportation_prices = $this->common->agency_prices($user_id,'transportation_prices');
		$country_prices = $this->common->agency_prices($user_id,'country_prices');
		$country_referral = $this->common->agency_prices($user_id,'country_referral');
		$zone_multiplier = $this->common->agency_prices($user_id,'zone_multiplier');
		$base_currency = ($agency_info[0]['base_currency']) ? $agency_info[0]['base_currency'] : 'AUD';

		$data = array(
			'middle_content'=>'manage-ratings-product',
			'title'=>'Customize Product Rating',
			'cargo'=>$cargo,
			'deductibles'=>$deductibles,
			'transportations'=>$transportations,
			'countries'=>$countries,
			'cargo_prices'=>$cargo_prices,
			'cargo_max'=>$cargo_max,
			'transportation_prices'=>$transportation_prices,
			'country_prices'=>$country_prices,
			'agency_info'=>$agency_info,
			'country_referral'=>$country_referral,
			'zone_multiplier'=>$zone_multiplier,
			'user_id'=>$user_id,
			'agency_id'=>$user_id,
			'currencies'=>$currencies,
			'base_currency'=>$base_currency,
			'underwriters'=>$underwriters
		);
//		echo json_encode($data);
//		return false;


		$this->load->view('admin/admin-view',$data);

	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function manage() {
		$admin_info = $this->master->getRecords('admin');
		$user_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];
		$agency_info = $this->master->getRecords('agency', array('id'=>$user_id));

		$whr = array(
			'customer_type'=>'Y',
			'super_admin'=>'Y',
		);
		$whr['agency_id'] = $user_id;
		$underwriters = $this->master->getRecords('customers', $whr);


		//check if an underwriter
//		$logged_admin_brokerage = $this->session->userdata('logged_admin_brokerage');
//		if(!empty($logged_admin_brokerage)){
//			$the_agency = $this->session->userdata('brokerage');
//			$the_agency_id = (isset($the_agency['agency_id'])) ? $the_agency['agency_id'] : '';
//
//			if(!empty($the_agency_id)){
//				$agency_info2 = $this->master->getRecords('agency', array('id'=>$the_agency_id));
//				if(!empty($agency_info2)){
//					$agency_info = $agency_info2;
//					$user_id = $agency_info[0]['id'];
//				}
//			}
//
//		}



		$deductibles = $this->master->getRecords('deductibles', array('agency_id'=>$user_id));
		$cargo = $this->master->getRecords('q_business_specific'); //this->master->getRecords('cargo_category');
		$transportations = $this->common->transportations();
		$countries = $this->master->getRecords('country_t');
		$currencies = $this->master->getRecords('api_currencies', array('in_stripe'=>'Y'));


		$cargo_prices = $this->common->agency_prices($user_id,'cargo_prices');
		$cargo_max = $this->common->agency_prices($user_id,'cargo_max');
		$transportation_prices = $this->common->agency_prices($user_id,'transportation_prices');
		$country_prices = $this->common->agency_prices($user_id,'country_prices');
		$country_referral = $this->common->agency_prices($user_id,'country_referral');
		$zone_multiplier = $this->common->agency_prices($user_id,'zone_multiplier');
		$base_currency = ($agency_info[0]['base_currency']) ? $agency_info[0]['base_currency'] : 'AUD';

		$data = array(
			'middle_content'=>'manage-cargo',
			'title'=>'Rating',
			'cargo'=>$cargo,
			'deductibles'=>$deductibles,
			'transportations'=>$transportations,
			'countries'=>$countries,
			'cargo_prices'=>$cargo_prices,
			'cargo_max'=>$cargo_max,
			'transportation_prices'=>$transportation_prices,
			'country_prices'=>$country_prices,
			'agency_info'=>$agency_info,
			'country_referral'=>$country_referral,
			'zone_multiplier'=>$zone_multiplier,
			'user_id'=>$user_id,
			'agency_id'=>$user_id,
			'currencies'=>$currencies,
			'base_currency'=>$base_currency,
			'underwriters'=>$underwriters
		);
//		echo json_encode($data);
//		return false;


		$this->load->view('admin/admin-view',$data);

	}

	public function save_currency(){
		$id = $_POST['id'];
		$currency = $_POST['currency'];
		$this->master->updateRecord('agency', array('base_currency'=>$currency), array('id'=>$id));
		echo json_encode(array('base_currency'=>$currency));
	}


	public function save_zone(){
		$zone = $_POST['zone'];
		$id = $_POST['id'];
		$this->master->updateRecord('agency', array('zone_multiplier'=>serialize($zone)), array('id'=>$id));
		echo json_encode(array('id'=>$id));
	}

	public function deductible_default(){
		$agency_id = $_POST['agency_id'];
		$id = $_POST['id'];

		$this->master->updateRecord('deductibles', array('not_default'=>'N'), array('id'=>$id));

		$not_def = $this->master->getRecords('deductibles', array('agency_id'=>$agency_id, 'id !='=>$id));
		if(count($not_def) > 0){
			foreach($not_def as $r=>$value){
				$this->master->updateRecord('deductibles', array('not_default'=>'Y'), array('id'=>$value['id']));
			}
		}
		echo json_encode(array('id'=>$id));
	}

	public function country_referral(){
		$admin_info = $this->master->getRecords('admin');
		$user_id = $admin_info[0]['default_cargo'];

		$type = $_POST['type'];
		$country_id = $_POST['country_id'];
		$country_referral = $this->common->agency_prices($user_id,'country_referral');

		if($type == 'set'){
			$country_referral[$country_id] = $country_id;
		}
		else{
			unset($country_referral[$country_id]);
		}

		$data = array(
			'country_referral'=>serialize($country_referral)
		);
		$this->master->updateRecord('agency', $data , array('id'=>$user_id));

		echo json_encode($data);
	}

	public function cargo_referral(){
		$admin_info = $this->master->getRecords('admin');
		$user_id = $admin_info[0]['default_cargo'];

		$type = $_POST['type'];
		$id = $_POST['id'];

		$data = array(
			'referral'=>$type
		);
		$this->master->updateRecord('q_business_specific', $data , array('id'=>$id));

		echo json_encode($data);
	}

	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function view() {
		$agency_id = $this->uri->segment(4);
		$cargo = $this->master->getRecords('cargo_category');
		$agencies = $this->master->getRecords('agency');
		$the_agency = $this->master->getRecords('agency', array('id'=>$agency_id));
		$admin_info = $this->master->getRecords('admin');

		$agency_prices = $this->common->agency_prices($agency_id, 'cargo_prices');

		$data = array(
			'middle_content'=>'view-cargo',
			'cargo'=>$cargo,
			'agencies'=>$agencies,
			'the_agency'=>$the_agency,
			'admin_info'=>$admin_info,
			'agency_prices'=>$agency_prices
		);
		$this->load->view('admin/admin-view',$data);


	}


	public function makedefault(){
		$id = $this->uri->segment(4);
		$this->master->updateRecord('admin', array('default_cargo'=>$id), array('id'=>'2'));
		$this->session->set_flashdata('success', 'Default cargo category successfully updated.');
		redirect('webmanager/cargo/manage');
	}


	public function promo() {
		$type = 'brokers'; //$this->uri->segment(4);
		$utype = ($type == 'brokers') ? 'N' : 'Y';

		$user_session = $this->session->all_userdata();
		$whr = array();

		if(isset($user_session['agency_id'])){
			$whr = array(
				'agency_id'=>$user_session['agency_id']
			);
		}
		$promocodes = $this->master->getRecords('biz_promocode', $whr, '*', array('id'=>'DESC'));

		$single = 'Promo Code';
		$title = $single;

		$data = array(
			'middle_content'=>'manage-promocode',
			'title'=>$title,
			'promocodes'=>$promocodes,
			'singular_title'=>$single,
		);

		$this->load->view('admin/admin-view',$data);

	}



	public function upload(){

		$output_dir = "uploads/users-bulk/";

		$bulk_id = $_POST['bulk_id'];


		//get validation message from admin
		$action_message_id = '70';
		$action_message = array(); //$this->common_model->get_message($action_message_id);

		if(isset($_FILES["mycsv_input"])) {

			if(substr($_FILES["mycsv_input"]["name"], -3) == 'csv'){

				//Filter the file types , if you want.
				if ($_FILES["mycsv_input"]["error"] > 0) {

				  echo 'error_upload';//$_FILES["file"]["error"] . "<br>";

				}
				else
				{
					$thefilename = uniqid().str_replace(' ', '_', $_FILES["mycsv_input"]["name"]);

					//move the uploaded file to uploads folder;
					move_uploaded_file($_FILES["mycsv_input"]["tmp_name"],$output_dir.$thefilename); //$_FILES["myfile"]["name"]);

					$the_emails = $this->csv($thefilename, $bulk_id);

					if($the_emails != ''){
						$this->session->set_flashdata('success', 'Bulk occupation successfully added.');// $action_message['success']);

						echo $the_emails;
					}
					else{
						echo 'not_readable';
					}


				}

			}//end of check file ext
			else{
				echo 'not_valid_file';
			}

		}

	}


	public function csv($csv_file, $bulk_id){

		$cc_id = $this->session->userdata('logged_cc_login_id');
		$org_id = $this->session->userdata('cc_selected_orgnaization');


//		$bulk_noti = $this->master_model->getRecords('bulk_notification', array('cc_id'=>$cc_id,'org_id'=>$org_id), '*', array('file_count'=>'DESC'));
//
//		if(count($bulk_noti) > 0){
//			$bulk_count = $bulk_noti[0]['file_count'] + 1;
//		}
//		else{
//			$bulk_count = 1;
//		}
//
//		//store uploaded file to delete all
//		$the_bulk = $this->master_model->getRecords('bulk_notification', array('id'=>$bulk_id));
//
//
//		if($the_bulk[0]['file_name'] !=''){
//
//			$files = unserialize($the_bulk[0]['file_name']);
//			$files[] = $csv_file;
//
//			$arr = array(
//				'file_name'=>serialize($files),
//			);
//
//			$this->master_model->updateRecord('bulk_notification',$arr, array('id'=>$bulk_id));
//
//		}else{
//			$files = array();
//			$files[] = $csv_file;
//
//			$arr = array(
//				'file_name'=>serialize($files),
//			);
//
//			$this->master_model->updateRecord('bulk_notification',$arr, array('id'=>$bulk_id));
//
//		}




		$f = fopen(base_url()."uploads/users-bulk/".$csv_file, "r");

		$name = array();
		$emails = array();
		$countrycode = array();
		$mobile = array();
		while (($data = fgetcsv($f, 1000, ",")) !== FALSE) {

			if(array_key_exists('0', $data)){
				$name[] = $data[0];
			}
			if(array_key_exists('1', $data)){
				$emails[] = $data[1]; //aka multiplier
			}
			if(array_key_exists('2', $data)){
				$countrycode[] = $data[2]; //aka referral
			}
		}


		$output = array();
		$count_inv_mobile = 0;
		$count_inv_email = 0;
		$email_rows_error = array();
		$sms_rows_error = array();
		for($i = 1; $i < count($name); $i++){

			if($emails[$i] !=''){
				if(!is_numeric($emails[$i])){
					$count_inv_mobile += 1;
					$sms_rows_error[] = $i;
				}
			}

		}

		$user_session = $this->session->all_userdata();
		$nowtime = date('Y-m-d H:i:s');

		$result = ''; //$bulk_id;

		if($count_inv_email == 0 && $count_inv_mobile == 0 ){

			for($i = 1; $i < count($name); $i++){

				$output['specific_name'] = $name[$i];
				$output['multiplier'] = $emails[$i];
				$output['referral'] = ($countrycode[$i] == 'Yes') ? $countrycode[$i] : '';
				$output['agency_id'] = $user_session['agency_id'];
				$output['broker_id'] = $user_session['id'];
				$output['date_added'] = $nowtime;

				$occu_id = $this->master->insertRecord('q_business_specific', $output, true);

				$agency = $this->master->getRecords('agency', array('id'=>$user_session['agency_id']));
				$cargo_val = unserialize($agency[0]['cargo_prices']);
				$cargo_val[$occu_id] = $emails[$i];

				$this->master->updateRecord('agency', array('cargo_prices'=>serialize($cargo_val)), array('id'=>$user_session['agency_id']));
			}

			$result = $user_session['id'];


		}//end of no invalid

		else{
			$result = $count_inv_mobile .' invalid multiplier found.';



			if(count($sms_rows_error) > 0){

				$result .= '<table class="table table-striped table-condensed table-hover text-left text-muted">';


					$result .=
						'<thead><tr>
						  <th>Invalid Multiplier</th>
						</tr></thead>';

					 $result .=  '<tbody>';


				$i = 1;
				foreach($sms_rows_error as $err_sms){

					$result .=
						'<tr>
						  <td><i class="fa fa-times-circle text-danger"></i> <i>'.$emails[$err_sms].'</i> on line '.$err_sms.'</td>
						</tr>';

					$i++;
				}

				$result .= '
					  </tbody>
					</table>';

			}//end display invalid mobile



			$result .= '<table class="table table-striped table-condensed table-hover text-left text-muted">';


			$result .=
				'<thead><tr>
				  <th colspan="4">Valid Data</th>
				</tr></thead>';

			 $result .=  '<tbody>';


			$count_valid = 0;
			for($i = 1; $i < count($name); $i++){

				if(!in_array($i, $sms_rows_error) && !in_array($i, $email_rows_error)){



					$result .=
						'<tr>
						  <td><i class="fa fa-check-circle text-success"></i> '.$name[$i].'</td>
						  <td>'.$emails[$i].'</td>
						  <td>'.$countrycode[$i].'</td>
						</tr>';

					$count_valid++;

				}

			}

			if($count_valid == 0){

				$result .=
					'<tr>
					  <td colspan="4">No valid row found.</td>
					</tr>';

			}

			$result .= '
				  </tbody>
				</table>';


		}

		return $result;
	}




}
