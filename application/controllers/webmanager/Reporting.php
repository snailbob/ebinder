<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reporting extends CI_Controller 

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/reporting/form');

	}


	/*--------------------------------
		manage view loading
	--------------------------------*/
	public function manage() {
		
		$insurances = $this->master->getRecords('bought_quote', array('status'=>'P'));

		$data = array(
			'middle_content'=>'manage-reporting',
			'title'=>'Reporting',
			'insurances'=>$insurances
		);	
		
		$this->load->view('admin/admin-view',$data);

	}
	
	
	/*--------------------------------
		form
	--------------------------------*/
	public function form() {
		$user_session = $this->session->all_userdata();
		$id = (empty($user_session['id'])) ? 0 : $user_session['id'];
		$agency_id = (isset($user_session['agency_id'])) ? $user_session['agency_id'] : '0';
		
		$whr = array('customer_approved'=>'Y', 'bound'=>'Y');
		
		if(!empty($agency_id)){
			$whr['agency_id'] = $agency_id; 
		}
		$biz_referral = $this->common->format_referral($whr);
		
		//echo json_encode($biz_referral); return false;

		$report_header = $this->common->report_header();
		$report_icons = $this->common->report_icons();
		$report_templates = $this->master->getRecords('report_template', array('user_id'=>$id));

		$data = array(
			'middle_content'=>'manage-reporting-form',
			'title'=>'Reporting',
			'report_header'=>$report_header,
			'report_icons'=>$report_icons,
			'report_templates'=>$report_templates,
		);	
		
		$this->load->view('admin/admin-view',$data);

	}
	
	
	public function quotes_report(){
		$user_session = $this->session->all_userdata();
		$id = $user_session['id'];

		$date_from = $this->uri->segment('4');
		$date_to = $this->uri->segment('5');
		
		// http://localhost/nguyen/ebinder.com.au/webmanager/reporting/quotes_report/14-02-2019/16-03-2019?date_from=14-02-2019&type=&date_to=16-03-2019&new_template=on&template_name=ddd&attributes%5B%5D=1&attributes%5B%5D=2
		//date_from=28-09-2016&type=bordereaux&date_to=28-10-2016&report_template=&template_name=&report_header
		$report_template = (isset($_GET['report_template'])) ? $_GET['report_template'] : '';
		$template_name = $_GET['template_name'];
		$attributes = (isset($_GET['attributes'])) ? $_GET['attributes'] : array();
		
		$nowtime = date('Y-m-d H:i:s');

		$store = array(
			'name'=>$template_name,
			'details'=>serialize($attributes)
		);
		
		if(!empty($template_name)){
			if($report_template == ''){
				$store['date_added'] = $nowtime;
				$store['user_id'] = $id;
				$this->master->insertRecord('report_template', $store);
			}
			else{
				$this->master->updateRecord('report_template', $store, array('id'=>$report_template));
			}
		}
		
		$date = date('d-m-Y');
		
		//echo json_encode($store);	

		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=ebinder_report'.$date.'.csv');
		
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		
		
		//fetch header title and, data name
		$report_header_title = $this->common->report_header();
		
		$header = array();
		if(count($attributes) > 0){
			foreach($attributes as $r=>$value){
				$header[] = $report_header_title[$value];
			}
		}
		
		// output the column headings
		fputcsv($output, $header);
		
		$rows = $this->common->ebinder_report($date_from, $date_to, $header);
		
		// loop over the rows, outputting them
		foreach($rows as $r=>$val){
			fputcsv($output, $val);
		}
	}

	public function billings(){
		$date_from = $this->uri->segment('4');
		$date_to = $this->uri->segment('5');
		$date = date('d-m-Y');
			
		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=billing_report'.$date.'.csv');
		
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		
		// output the column headings
		fputcsv($output, array('Date of purchase', 'Agent', 'Consignee details', 'Quote Number', 'Premium'));
		
		$rows = $this->common->billing_report($date_from, $date_to);
		
		// loop over the rows, outputting them
		foreach($rows as $r=>$val){
			fputcsv($output, $val);
		}

	}

	public function bordereaux(){
//		$r = $this->common->bordereaux_report();
//		header('Content-Type: application/json');
//		header('Access-Control-Allow-Origin: *');
//		echo json_encode($r, JSON_PRETTY_PRINT);
//		return false;

		$date_from = $this->uri->segment('4');
		$date_to = $this->uri->segment('5');
		$date = date('d-m-Y');
			
		// output headers so that the file is downloaded rather than displayed
		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename=bordereaux_report'.$date.'.csv');
		
		// create a file pointer connected to the output stream
		$output = fopen('php://output', 'w');
		
		$column_header = array(
			'Consignor Details',
			'Consignee Details',
			'Shipment Date',
			'Insured Value',
			'Transit From',
			'Cargo Category',
			'Port of Loading',
			'Port of Discharge',
			'Transit To',
			'Description',
			'Deductible',
			'Currency',
			'Premium',
			'Quote Number/ID',
			'Base Currency',
			'Converted Insured Value',
			'Agent',
			'Purchase Date',
		);
		// output the column headings
		fputcsv($output, $column_header);
		
		$rows = $this->common->bordereaux_report($date_from, $date_to);
		
		// loop over the rows, outputting them
		foreach($rows as $r=>$val){
			fputcsv($output, $val);
		}

	}

	

}