<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customers extends CI_Controller

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/customers/manage');

	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function broker() {
		$id = $this->uri->segment(4);
		$user_id = $id;

		$user = $this->common->customer_format($id);


		$brokerage = $this->master->getRecords('brokerage', array('id'=>$user['brokerage_id']));

		$countries = $this->common->country_format();
		$single = 'Broker';
		$title = $single;

		if(!isset($user['id'])){
			redirect('webmanager/customers/manage/brokers');
		}


		//agency info and product details
		$agency_info = $this->master->getRecords('agency', array('id'=>$user_id));

		$agency_info_format = array();
		if(!empty($agency_info)){
			$ggg = array();
			foreach($agency_info as $r=>$value){
				$value['agency_details'] = (@unserialize($value['agency_details'])) ? unserialize($value['agency_details']) : array();
				$ggg = $value;
			}
			$agency_info_format = $ggg;
		}


		$prod_id = (@unserialize($user['privileges'])) ? unserialize($user['privileges']) : array();


		$q_products = $this->master->getRecords('q_products');






		$data = array(
			'middle_content'=>'manage-broker-update',
			'title'=>$title,
			'singular_title'=>$single,
			'countries'=>$countries,
			'user'=>$user,
			'agency_info'=>$agency_info_format,
			'q_products'=>$q_products,
			'prod_id'=>$prod_id,
			'brokerage'=>$brokerage
		);

		//echo json_encode($data); return false;
		$this->load->view('admin/admin-view',$data);


	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function brokerage() {
		$agency_id = $this->common->default_cargo();

		if($agency_id == '6'){
			$users = $this->master->getRecords('brokerage');
		}
		else{
			$users = $this->master->getRecords('brokerage', array('agency_id'=>$agency_id));
		}

		$the_brokers = $this->master->getRecords('customers', array('customer_type'=>'N', 'super_admin'=>'N', 'agency_id'=>$agency_id));

		$single = 'Brokerage';
		$title = $single;

		$data = array(
			'middle_content'=>'manage-brokers',
			'title'=>$title,
			'singular_title'=>$single,
			'the_brokers'=>$the_brokers,
			'users'=>$users
		);
		$this->load->view('admin/admin-view',$data);


	}

	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function manage() {
		$type = $this->uri->segment(4);
		$uid = $this->uri->segment(5);
		$pagetype = $this->uri->segment(6);
		$utype = ($type == 'brokers') ? 'N' : 'Y';
		$agency_id = $this->common->default_cargo();

		$user_session = $this->session->all_userdata();
		$type = $this->uri->segment(4);
		$id = (isset($user_session['id'])) ? $user_session['id'] : '';

		$logged_user = $this->common->customer_format($id);
		// echo json_encode($logged_user); return false;

		$name = ($uid != '') ? $this->common->db_field_id('customers', 'first_name', $uid).' '.$this->common->db_field_id('customers', 'last_name', $uid) : '';
		$ss = ($type == 'brokers') ? 'Broker' : 'Customer';
		$single = $name.' '.$ss;
		$title = $single.'s';

		$whr = array(
			'customer_type'=>$utype,
			'super_admin'=>'N',
		);

		$countries = $this->common->country_format();


		if($agency_id != '6'){
			$whr['agency_id']=$agency_id;
		}

		if($uid != ''){
			$whr['studio_id'] = $uid;
			$users = $this->master->getRecords('customers', $whr, '*', array('id'=>'DESC'));
		}
		else{

			$users = $this->master->getRecords('customers', $whr, '*', array('id'=>'DESC'));
		}

		$brokerage = $this->master->getRecords('brokerage', array('agency_id'=>$agency_id));


		$q_products = $this->master->getRecords('q_products');



		$data = array(
			'middle_content'=>($type == 'brokers') ? 'manage-customers' : 'manage-customers2',
			'title'=>$title,
			'brokerage'=>$brokerage,
			'singular_title'=>$single,
			'q_products'=>$q_products,
			'users'=>$users,
			'logged_user'=>$logged_user,
			'countries'=>$countries
		);




		//check if broker and for message
		if($type == 'brokers' && (!empty($uid)) && $pagetype == 'message'){

			$admin_info = $this->master->getRecords('admin');

			$agency_id = $this->session->userdata('logged_admin_agency');



			$user_session = $this->session->all_userdata();

			$type = $this->uri->segment(4);

			$id = $user_session['id'];
			$broker = $this->common->brokers($id);

			$logged_user = $this->common->customer_format($id);


			$base_link = $this->common->base_url();

			$broker_id = $user_session['id'];

			$claims = array(); //$this->master->getRecords('claims', array('id'=>$type));
			$brokerr = $this->common->customer_format($uid);


			$underwriter = $this->common->customer_format($id);


			$message_idd = $uid.$id;

			$messages = $this->master->getRecords('claims_message_file', array('message_id'=>$message_idd));

			$to = $uid;


			$uniquetime= time().mt_rand();


			$data = array(
				'middle_content'=>'manage-claims-message',
				'title'=>'Messages',
				'admin_info'=>$admin_info,
				'user'=>$broker,
				'logged_user'=>$logged_user,
				'message_id'=>$type,
				'to'=>$to,
				'messages'=>$messages,
				'underwriter'=>$underwriter,
				'broker'=>$brokerr,
				'claims'=>$claims
			);

		}






		$this->load->view('admin/admin-view',$data);

	}


	#--------------------------------------------->>manage underwriters<<-------------------------------------
	public function underwriters() {
		$type = $this->uri->segment(4);
		$uid = $this->uri->segment(5);
		$utype = 'N';
		$agency_id = $this->common->default_cargo();
		$agency_nofirst = $this->common->agency_nofirst();


		$name = ($uid != '') ? $this->common->db_field_id('customers', 'first_name', $uid).' '.$this->common->db_field_id('customers', 'last_name', $uid) : '';
		$ss = 'Underwriter';
		$single = $name.' '.$ss;
		$title = $single.'s';

		$whr = array(
			'customer_type'=>$utype,
			'super_admin'=>'Y',
			'first_underwriter'=>'Y',
		);


		if($agency_id != '6'){
			$whr['agency_id']=$agency_id;
		}

		if($uid != ''){
			$whr['studio_id'] = $uid;
			$users = $this->master->getRecords('customers', $whr, '*', array('id'=>'DESC'));
		}
		else{

			$users = $this->master->getRecords('customers', $whr, '*', array('id'=>'DESC'));
		}
		$countries = $this->common->country_format();



		$brokerage = $this->master->getRecords('brokerage', array('agency_id'=>$agency_id));
		$data = array(
			'middle_content'=>'manage-underwriters2',
			'title'=>$title,
			'brokerage'=>$brokerage,
			'singular_title'=>$single,
			'countries'=>$countries,
			'agency_nofirst'=>$agency_nofirst,
			'users'=>$users
		);

//		echo json_encode($data['agency_nofirst']); return false;
		$this->load->view('admin/admin-view',$data);

	}





	#--------------------------------------------->>manage view agencies<<-------------------------------------
	public function agencies() {
		$agency_id = $this->uri->segment(4);

		$whr = array(
			'domain !='=>'',
			'user_type'=>'0'
		);
		$users = $this->master->getRecords('agency', $whr, '*', array('id'=>'DESC'));
		$q_products = $this->master->getRecords('q_products');


		$single = 'Agency';
		$title = 'Agencies';

		if(!empty($agency_id)){
			$whr = array(
				'customer_type'=>'N',
				'super_admin'=>'Y',
				'agency_id'=>$agency_id
			);
			$agency = $this->master->getRecords('agency', array('id'=>$agency_id));

			$users = $this->master->getRecords('customers', $whr, '*', array('id'=>'DESC'));

			$single = 'Underwriter';
			$title = (empty($agency)) ? 'Underwriters' : $agency[0]['name'].' Underwriters';

		}


		$data = array(
			'middle_content'=>(empty($agency_id)) ? 'manage-agencies' : 'manage-underwriters2',
			'title'=>$title,
			'singular_title'=>$single,
			'q_products'=>$q_products,
			'users'=>$users
		);
		$this->load->view('admin/admin-view',$data);


	}



	public function add_customer2(){

		$user_session = $this->session->all_userdata();
		$user_id = (empty($user_session['id'])) ? '0' : $user_session['id'];

		$customer_details = $_POST['data'];
		$data = array();
		foreach($customer_details as $r=>$value){

			$thelast_char = substr($value['name'], -2);
			$stripped_name = substr($value['name'], 0, -2);
			if($thelast_char != '[]'){
				$data[$value['name']] = $value['value'];
			}
			else{
				$data[$stripped_name][] = $value['value'];
			}
		}


		$id = $data['id'];
		$studio_id = $data['broker_id']; //broker id for customers
		$agency_id = (isset($data['agency_id'])) ? $data['agency_id'] : $this->session->userdata('logged_admin_agency');
		$agency_id = (!empty($agency_id)) ? $agency_id : $this->session->userdata('logged_admin_agency');
		$is_agency = (isset($data['is_agency'])) ? 'yes' : '';
		$access_rights = (isset($data['access_rights'])) ? $data['access_rights'] : '';




		$first = $data['first'];
		$last = $data['last'];
		$email = $data['email'];


		$country_id = $data['country_code'];
		$country_short = $data['country_short'];

		$mobile = $data['mobile'];
		$phone_trim = ltrim($mobile, '0');
		$contact_no = $country_short.$phone_trim;


		$nowtime = date('Y-m-d H:i:s');


		if($id == ''){


			$broker = array(
				'first_name'=>$data['first'],
				'last_name'=>$data['last'],
				'email'=>$data['email'],
				'customer_type'=>$data['ctype'],
				'country_id'=>$country_id,
				'calling_code'=>$country_short,
				'calling_digits'=>$mobile,
				'contact_no'=>$contact_no,
				'location'=>$data['address'],
				'lat'=>$data['lat'],
				'lng'=>$data['lng'],
				'studio_id'=>$studio_id,
				'brokerage_id'=>$data['brokerage_id'],
				'customer_info'=>serialize($data),
				'agency_id'=>$this->common->default_cargo(),
				'status'=>'N'
			);



			$customer_id = $this->master->insertRecord('customers', $broker, true);


			//admin emailer info
			$adminemail = $this->common->admin_email();
			$emailer_file_name = 'signup-mail-to-broker';
			$dbtable = 'customers';

			//email settings
			$info_arr = array(
				'to'=>$email,
				'from'=>$adminemail,
				'subject'=>'Welcome to Transit Insurance',
				'view'=>$emailer_file_name,
			);


			$other_info = array(
				'password'=>'',
				'emailer_file_name'=>$emailer_file_name,
				'name'=>$first. ' '.$last,
				'agency_name'=>$id,
				'user_name'=>$email,
				'user_email'=>$email,
				'link'=>'landing/verify_user/'.$customer_id.'/'.md5($customer_id).'/'.$dbtable.'/broker'
			);

			$this->emailer->sendmail($info_arr, $other_info);

			$data['other_info'] = $other_info;
			$data['info_arr'] = $info_arr;


			$success_mess = 'Customer details successfully added.';
			$this->session->set_flashdata('success',$success_mess);
			$data['result'] = 'ok';
			$data['action'] = 'add';


		}

		else{

			$broker = array(
				'first_name'=>$data['first'],
				'last_name'=>$data['last'],
				'country_id'=>$country_id,
				'calling_code'=>$country_short,
				'calling_digits'=>$mobile,
				'contact_no'=>$contact_no,
				'location'=>$data['address'],
				'customer_info'=>serialize($data),
				'lat'=>$data['lat'],
				'lng'=>$data['lng']
			);

			$this->master->updateRecord('customers', $broker, array('id'=>$data['id']));

			$success_mess = 'Customer details successfully updated.';
			$this->session->set_flashdata('success',$success_mess);
			$data['result'] = 'ok';
			$data['action'] = 'update';
		}


		echo json_encode($data);


	}

	public function add(){

		$user_session = $this->session->all_userdata();
		$user_id = (empty($user_session['id'])) ? '0' : $user_session['id'];

		$id = $_POST['id'];
		$studio_id = $_POST['studio_id'];
		$agency_id = (isset($_POST['agency_id'])) ? $_POST['agency_id'] : $this->session->userdata('logged_admin_agency');
		$agency_id = (!empty($agency_id)) ? $agency_id : $this->session->userdata('logged_admin_agency');
		$is_agency = (isset($_POST['is_agency'])) ? 'yes' : '';
		$access_rights = (isset($_POST['access_rights'])) ? $_POST['access_rights'] : '';
		$user_role = (isset($_POST['user_role'])) ? $_POST['user_role'] : '';
		$position = (isset($_POST['position'])) ? $_POST['position'] : '';

		$email = $_POST['email'];
		$first = $_POST['first'];
		$last = $_POST['last'];

		$country_id = (isset($_POST['country_code'])) ? $_POST['country_code'] : '';
		$country_short = (isset($_POST['country_short'])) ? $_POST['country_short'] : '';

		$mobile = (isset($_POST['mobile'])) ? $_POST['mobile'] : '';
		$phone_trim = ltrim($mobile, '0');
		$contact_no = $country_short.$phone_trim;



		$brokerage_id = $_POST['brokerage_id'];
		$ctype = 'N';
		$mess = ' User successfully added.';
		if(isset($_POST['utype'])){
			//$ctype = $_POST['utype']; //'N'; //Y';
		//	$mess = ' User successfully added.';
		}
		$nowtime = date('Y-m-d H:i:s');

		$data = array(
			'first_name'=>$first,
			'last_name'=>$last,
			'brokerage_id'=>$brokerage_id,
			'customer_type'=>$ctype,
			'email'=>$email,
			'country_id'=>$country_id,
			'calling_code'=>$country_short,
			'calling_digits'=>$mobile,
			'contact_no'=>$contact_no,
		);




//		echo json_encode($data);
//		return false;

		if($id == ''){



			//check duplicate underwriter
			if(!empty($is_agency)){
				$existAgent = $this->master->getRecordCount('customers', array('email'=>$email));

				if($existAgent > 0){
					$data['result'] = 'duplicate';
					$data['message'] = 'User already exist.';

					echo json_encode($data);
					return false;
				}
			}
			else{
				//check if exist as underwriter
				$existAgent = $this->master->getRecordCount('customers', array('email'=>$email,'super_admin'=>'Y'));
				if($existAgent > 0){
					$data['result'] = 'duplicate';
					$data['message'] = 'Broker already exist as underwriter.';

					echo json_encode($data);
					return false;
				}
				//check if already exist broker
				if(!empty($agency_id)){
					$existBroker = $this->master->getRecordCount('customers', array('email'=>$email,'agency_id'=>$agency_id));
					if($existBroker > 0){
						$data['result'] = 'duplicate';
						$data['message'] = 'Broker already exist in this agency.';

						echo json_encode($data);
						return false;
					}

				}


			}



			if($access_rights != ''){
				$data['access_rights'] = serialize($access_rights);
			}

			$data['first_underwriter'] = (empty($user_id)) ? 'Y' : 'N';
			$data['super_admin'] = (empty($is_agency)) ? 'N' : 'Y';
			$data['agency_id'] = (empty($agency_id)) ? '0' : $agency_id;

			$data['customer_type'] = $ctype;
			$data['studio_id'] = (empty($studio_id)) ? 0 : $studio_id;

			$data['status'] = 'N';
			$data['enabled'] = 'N';
			$data['date_added']= $nowtime;


			if(!empty($user_role)){
				$data['customer_type'] = 'N';

				if($user_role == 'Super Administrator'){
					$data['first_underwriter'] = 'Y';
					$data['underwriter_user'] = 'Y';
					$data['access_rights'] = serialize($this->common->access_rights_all());
				}

				if($user_role == 'Broker'){
					$data['first_broker'] = 'Y';
				}

			}

			$data['domain'] = $position;


			$id = $this->master->insertRecord('customers', $data, TRUE);


			//logs
			$log_activity = array(
				'name'=>$data['first_name'].' added user.',
				'type'=>'added',
				'details'=>serialize($data)
			);
			$this->common->insert_activity_log($log_activity);



			//admin emailer info
			$adminemail = $this->common->admin_email();
			$emailer_file_name = (empty($is_agency)) ? 'signup-mail-to-broker' : 'signup-mail-to-underwriter';
			$dbtable = 'customers';

			//email settings
			$info_arr = array(
				'to'=>$email,
				'from'=>$adminemail,
				'subject'=>'Welcome to Transit Insurance',
				'view'=>$emailer_file_name,
			);


			$other_info = array(
				'password'=>'',
				'emailer_file_name'=>$emailer_file_name,
				'name'=>$first. ' '.$last,
				'agency_name'=>$brokerage_id,
				'user_name'=>$email,
				'user_email'=>$email,
				'link'=>'landing/verify_user/'.$id.'/'.md5($id).'/'.$dbtable.'/broker'
			);


			$this->emailer->sendmail($info_arr, $other_info);
			$data['info_arr'] = $info_arr;
			$data['other_info'] = $other_info;


		}
		else{

			$brokerage = $this->master->getRecords('brokerage', array('id'=>$brokerage_id));

			if(count($brokerage) > 0){
				if($brokerage[0]['broker_id'] == 0){

					$agency_id = '0'; //$this->common->clone_agency_deducts($brokerage_id);

					//update brokerage
					$this->master->updateRecord('brokerage', array('broker_id'=>$id), array('id'=>$brokerage_id));
					$data['first_broker'] = 'Y';



					//logs
					$log_activity = array(
						'name'=>$data['first_name'].' assigned as first broker.',
						'type'=>'update first broker',
						'details'=>serialize($data)
					);
					$this->common->insert_activity_log($log_activity);


					//admin emailer info
					$adminemail = $this->common->admin_email();
					$emailer_file_name = 'brokerage-assign-to-broker';
					$dbtable = 'customers';

					//email settings
					$info_arr = array(
						'to'=>$email,
						'from'=>$adminemail,
						'subject'=>'Welcome to Transit Insurance',
						'view'=>$emailer_file_name,
					);

					$other_info = array(
						'password'=>'',
						'emailer_file_name'=>$emailer_file_name,
						'name'=>$first. ' '.$last,
						'agency_name'=>$id,
						'user_name'=>$email,
						'user_email'=>$email,
						'link'=>''
					);

					$this->emailer->sendmail($info_arr, $other_info);

				}
			}

			$mess = ' User successfully updated.';




			if($access_rights != ''){
				$data['access_rights'] = serialize($access_rights);

				//underwriter users
				if(!empty($user_role)){
					$data['first_broker'] = 'N';
					$data['first_underwriter'] = 'N';

					if($user_role == 'Super Administrator'){
						$data['first_underwriter'] = 'Y';

						$access_rights = $this->common->access_rights_all();
						$data['access_rights'] = serialize($access_rights);
					}

					if($user_role == 'Broker'){
						$data['first_broker'] = 'Y';
					}



				}

				if($user_id == $id){
					$this->session->set_userdata('access_rights', $access_rights);
				}

			}

			$data['domain'] = $position;

			$this->master->updateRecord('customers', $data, array('id'=>$id));


			//logs
			$log_activity = array(
				'name'=>$data['first_name'].' updated user info.',
				'type'=>'update user',
				'details'=>serialize($data)
			);
			$this->common->insert_activity_log($log_activity);


			$data['other_info'] = (isset($other_info)) ? $other_info : array();
			$data['info_arr'] = (isset($info_arr)) ? $info_arr : array();

		}




		$data['result'] = 'ok';
		$this->session->set_flashdata('success',$mess);
		echo json_encode($data);

	}


	#--------------------------------------------->>activate view loading<<-------------------------------------
	public function activate() {
		$inc = $this->uri->segment(4);
		$user_id = $this->uri->segment(5);

		if($this->master->updateRecord('customers',array('enabled'=>$inc),array('id'=>$user_id))){
			if($inc == 'Y'){
				$success_mess = 'User successfully activated';

				//logs
				$log_activity = array(
					'name'=>$data['first_name'].' user activated .',
					'type'=>'activated',
					'details'=>serialize($data)
				);
				$this->common->insert_activity_log($log_activity);


			}
			else{
				$success_mess = 'User successfully deactivated';

				//logs
				$log_activity = array(
					'name'=>$data['first_name'].' user deactivated .',
					'type'=>'deactivated',
					'details'=>serialize($data)
				);
				$this->common->insert_activity_log($log_activity);


			}

			$this->session->set_flashdata('success',$success_mess);

			redirect('webmanager/customers/manage/brokers');
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
			redirect('webmanager/customers/manage/brokers');
		}
	}


	#--------------------------------------------->>get_info view loading<<-------------------------------------
	public function get_info() {
		$id = $_POST['id'];

		$user = $this->master->getRecords('customers',array('id'=>$id));


		$info = array(
			'email'=>$user[0]['email'],
			'first_name'=>$user[0]['first_name'],
			'last_name'=>$user[0]['last_name'],
			'address'=>$user[0]['address'],
			'require_space'=>$user[0]['require_space'],
			'trade_space'=>$user[0]['trade_space'],
		);

		echo json_encode($info);

	}
	#--------------------------------------------->>get_info view loading<<-------------------------------------
	public function update_info() {
		$first = $_POST['firstname'];
		$last = $_POST['lastname'];
		$address = $_POST['address'];
		$trade_space = $_POST['trade_space'];
		$require_space = $_POST['require_space'];
		$id = $_POST['id'];


		$arr = array(
			'first_name'=>$first,
			'last_name'=>$last,
			'address'=>$address,
			'require_space'=>$require_space,
			'trade_space'=>$trade_space
		);

		if($this->master->updateRecord('customers',$arr,array('id'=>$id))){
			$this->session->set_flashdata('success',' Customer info updated successfully');
			echo 'success';
		}else{
			echo 'error';
		}
	}


	#--------------------------------------------->>delete view loading<<-------------------------------------
	public function delete() {
		$user_id = $this->uri->segment(4);

		if($this->master->deleteRecord('customers','id',$user_id)) {

			$success_mess = 'Customer successfully deleted';
			$this->session->set_flashdata('success',$success_mess);





			redirect('webmanager/customers/manage');
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
			redirect('webmanager/customers/manage');
		}
	}



	public function save_agency(){
		$id = $_POST['id'];
		$email = ''; //$_POST['email'];
		$first = ''; //$_POST['first'];
		$last =''; // $_POST['last'];
		$phone = ''; //$_POST['phone'];
		$address = ''; //$_POST['address'];
		$lat = ''; //$_POST['lat'];
		$lng = ''; //$_POST['lng'];


		$name = $_POST['name'];
		$domain = (isset($_POST['domain'])) ? $_POST['domain'] : '';
		$product_id = (isset($_POST['product_id'])) ? $_POST['product_id'] : '';

		$agency_details = (isset($_POST['agency_details'])) ? $_POST['agency_details'] : '';




		$agencies = $this->master->getRecordCount('agency', array('domain'=>''));
		$nowtime = date('Y-m-d H:i:s');

		$adetails_arr = array();
		foreach($agency_details as $r=>$value){

			$thelast_char = substr($value['name'], -2);
			$stripped_name = substr($value['name'], 0, -2);
			if($thelast_char != '[]'){
				$adetails_arr[$value['name']] = $value['value'];
			}
			else{
				$adetails_arr[$stripped_name][] = $value['value'];
			}
		}


/*		$address1 = $adetails_arr['address1'];
		$address2 = $adetails_arr['address2'];
		$city = $adetails_arr['city'];
		$state = $adetails_arr['state'];


		$address = array(
			$address1,
			$address2,
			$city,
			$state
		);
*/
		// 0 = regular agency, 1 = product owner
		$user_type = (isset($adetails_arr['user_type'])) ? $adetails_arr['user_type'] : '0';

		$data = array(
			'name'=>$name,
			'domain'=>$domain,
			'contact_no'=>$phone,
			'address'=>$adetails_arr['address'],
			'lat'=>$adetails_arr['lat'],
			'lng'=>$adetails_arr['lng'],
			'background'=>$adetails_arr['background'],
			'email'=>$adetails_arr['email'],
			'user_type'=>$user_type,
			'product_id'=>$product_id,
			'agency_details'=>serialize($adetails_arr),
		);

		//echo json_encode($data); return false;
		if($id == ''){


			//check duplicate agency
			$exist_agency = $this->master->getRecordCount('agency',array('domain'=>$domain));

			if($exist_agency > 0){
				$data['message'] = 'Agency domain already exist.';
				$data['result'] = 'duplicate';
				echo json_encode($data);
				return false;

			}





			$success_mess = ($user_type == '0') ? 'Agency successfully added. Add agency\'s first underwriter.' : 'Product Owner successfully added.';
			$this->session->set_flashdata('success',$success_mess);
			$data['date_added'] = $nowtime;
			$id = $this->common->clone_agency($data);

			//logs
			$log_activity = array(
				'name'=>$name.' agency was added.',
				'type'=>'agency',
				'details'=>serialize($data)
			);
			$this->common->insert_activity_log($log_activity);




			$data['action'] = 'add';
			//if(!empty($email)){
//
//
//				$broker = array(
//					'first_name'=>$first,
//					'last_name'=>$last,
//					'email'=>$email,
//					'location'=>$address,
//					'lat'=>$lat,
//					'lng'=>$lng,
//					'customer_type'=>'N',
//					'agency_id'=>$id,
//					'status'=>'N',
//					'first_underwriter'=>'Y',
//					'super_admin'=>'Y'
//				);
//
//				$broker_id = $this->master->insertRecord('customers', $broker, true);
//
//				$agency_id = '0'; //$this->common->clone_agency_deducts($id);
//
//
//				$this->master->updateRecord('brokerage', array('broker_id'=>$broker_id, 'agency_id'=>$agency_id), array('id'=>$id));
//
//				//admin emailer info
//				$adminemail = $this->common->admin_email();
//				$emailer_file_name = 'signup-mail-to-underwriter';
//				$dbtable = 'customers';
//
//				//email settings
//				$info_arr = array(
//					'to'=>$email,
//					'from'=>$adminemail,
//					'subject'=>'Welcome to Transit Insurance',
//					'view'=>$emailer_file_name,
//				);
//
//
//				$other_info = array(
//					'password'=>'',
//					'emailer_file_name'=>$emailer_file_name,
//					'name'=>$first. ' '.$last,
//					'agency_name'=>$id,
//					'user_name'=>$email,
//					'user_email'=>$email,
//					'link'=>'landing/verify_user/'.$broker_id.'/'.md5($broker_id).'/'.$dbtable.'/broker'
//				);
//
//
//				$this->emailer->sendmail($info_arr, $other_info);
//
//				$data['other_info'] = $other_info;
//				$data['info_arr'] = $info_arr;
//
//			}
		}
		else{




			if(empty($domain)){
				$data = array(
					'name'=>$name,
					'address'=>$adetails_arr['address'],
					'lat'=>$adetails_arr['lat'],
					'lng'=>$adetails_arr['lng'],
					'background'=>$adetails_arr['background']
				);
			}
			else{


				//check duplicate agency
				$exist_agency = $this->master->getRecordCount('agency',array('domain'=>$domain, 'id !='=>$id));

				if($exist_agency > 0){
					$data['message'] = 'Agency domain already exist.';
					$data['result'] = 'duplicate';
					echo json_encode($data);
					return false;

				}

				unset($data['email']);
				unset($data['contact_no']);
			}

			$this->master->updateRecord('agency', $data, array('id'=>$id));
			$data['action'] = 'update';

			//logs
			$log_activity = array(
				'name'=>$name.' agency was updated.',
				'type'=>'agency',
				'details'=>serialize($data)
			);
			$this->common->insert_activity_log($log_activity);

			$success_mess = 'Agency successfully updated.';
			$this->session->set_flashdata('success',$success_mess);
		}

		$data['result'] = 'ok';
		$data['id'] = $id;
		echo json_encode($data);



	}

	public function save_brokerage(){
		$agency_id = $this->common->default_cargo();
		$data = $_POST['data'];
		$id = '';
		$input = $data;

		$nowtime = date('Y-m-d H:i:s');

		$data = array();
		foreach($input as $r=>$value){

			if($value['name'] != 'id'){


				$thelast_char = substr($value['name'], -2);
				$stripped_name = substr($value['name'], 0, -2);
				if($thelast_char != '[]'){
					$data[$value['name']] = $value['value'];
				}
				else{
					$data[$stripped_name][] = $value['value'];
				}


				//$data[$value['name']] = $value['value'];
			}
			else{
				$id = $value['value'];
			}
		}

		$data['enable_whitelabel'] = (isset($data['enable_whitelabel'])) ? 'Y' : 'N';




		$my_products = (isset($data['product_id'])) ? $data['product_id'] : array();
		$suite_number = $data['suite_number'];
		$country_id = $data['country_code'];
		$country_short = $data['country_short'];


		$mobile = (isset($data['mobile'])) ? $data['mobile'] : '';
		$phone_trim = ltrim($mobile, '0');
		$contact_no = $country_short.$phone_trim;

		$data['phone'] = $contact_no;

		unset($data['product_id']);
		unset($data['suite_number']);
		unset($data['country_code']);
		unset($data['mobile']);
		unset($data['country_short']);


		if($id == ''){
			$data['agency_id'] = $this->common->default_cargo();
			$data['date_added'] = $nowtime;


			//check duplicate broker in agency
			$existAgent = $this->master->getRecordCount('customers', array('email'=>$data['email'], 'agency_id'=>$agency_id));
			if($existAgent > 0){
				$success_mess = 'Underwriter already exist.';
				$this->session->set_flashdata('error',$success_mess);
				echo json_encode($data);
				return false;
			}


			$id = $this->master->insertRecord('brokerage', $data, true);

			//logs
			$log_activity = array(
				'name'=>'Brokerage was added.',
				'type'=>'brokerage',
				'details'=>serialize($data)
			);
			$this->common->insert_activity_log($log_activity);



			$email = $data['email'];
			$first = $data['first'];
			$last = $data['last'];

			if(!empty($email)){



				$broker = array(
					'first_name'=>$data['first'],
					'last_name'=>$data['last'],
					'email'=>$data['email'],
					'customer_type'=>'N',
					'country_id'=>$country_id,
					'calling_code'=>$country_short,
					'calling_digits'=>$mobile,
					'contact_no'=>$contact_no,
					'suite_number'=>$suite_number,
					'location'=>$data['address'],
					'lat'=>$data['lat'],
					'lng'=>$data['lng'],
					'brokerage_id'=>$id,
					'privileges'=>serialize($my_products),
					'agency_id'=>$this->common->default_cargo(),
					'status'=>'N',
					'enabled'=>'N',
					'first_broker'=>'Y',
					'date_added'=>$nowtime
				);

				$broker_id = $this->master->insertRecord('customers', $broker, true);

				$agency_id = '0';//$this->common->clone_agency_deducts($id);


				$this->master->updateRecord('brokerage', array('broker_id'=>$broker_id), array('id'=>$id));

				//admin emailer info
				$adminemail = $this->common->admin_email();
				$emailer_file_name = 'signup-mail-to-broker';
				$dbtable = 'customers';

				//email settings
				$info_arr = array(
					'to'=>$email,
					'from'=>$adminemail,
					'subject'=>'Welcome to Transit Insurance',
					'view'=>$emailer_file_name,
				);


				$other_info = array(
					'password'=>'',
					'emailer_file_name'=>$emailer_file_name,
					'name'=>$first. ' '.$last,
					'agency_name'=>$id,
					'user_name'=>$email,
					'user_email'=>$email,
					'link'=>'landing/verify_user/'.$broker_id.'/'.md5($broker_id).'/'.$dbtable.'/broker'
				);

				$this->emailer->sendmail($info_arr, $other_info);

				$data['other_info'] = $other_info;
				$data['info_arr'] = $info_arr;


				$data['broker'] = $broker;
				$success_mess = 'Broker details successfully added.';
				$this->session->set_flashdata('success',$success_mess);
			}
		}
		else{
			$success_mess = 'Broker details successfully updated. <span class="hidden">broker_'.$id.'</span>';
			$this->session->set_flashdata('success',$success_mess);

//			unset($data['email']);
//			unset($data['first']);
//			unset($data['last']);
//			unset($data['phone']);
			$this->master->updateRecord('brokerage', $data, array('id'=>$id));





			$broker = array(
				'first_name'=>$data['first'],
				'last_name'=>$data['last'],
				'country_id'=>$country_id,
				'calling_code'=>$country_short,
				'calling_digits'=>$mobile,
				'contact_no'=>$contact_no,
				'suite_number'=>$suite_number,
				'location'=>$data['address'],
				'lat'=>$data['lat'],
				'lng'=>$data['lng']
			);

			if(!empty($my_products)){
				$broker['privileges'] = serialize($my_products);
			}

			$this->master->updateRecord('customers', $broker, array('id'=>$data['broker_id']));


			//logs
			$log_activity = array(
				'name'=>'Brokerage was updated.',
				'type'=>'brokerage',
				'details'=>serialize($data)
			);
			$this->common->insert_activity_log($log_activity);

			$data['broker'] = $broker;

		}

		echo json_encode($data);

	}

	public function accessportal(){
		$id = $this->uri->segment(4);
		$the_customer = $this->common->customer_format($id);
		$agency = $this->master->getRecords('agency', array('email'=>$the_customer['email']));

		$arr['userdata'] = $the_customer;


		$the_customer['multiple_account'] = 'no';
		//include agency to switch
		if(count($agency) > 0){
			$the_customer['multiple_account'] = 'yes';
		}
		if($this->session->userdata('logged_admin') == 'admin'){ //the_customer['enabled'] != 'N'){
			if($the_customer['super_admin'] == 'Y'){
				$the_customer['is_admin'] = 'yes';
				$this->session->set_userdata($the_customer);

				redirect('webmanager');

			}
			else{
				$this->session->set_userdata($the_customer);


				redirect('panel');

			}
		}
		else{
			echo 'invalid link';
		}

		//echo json_encode($the_customer);
	}


	public function accesslog() {
		$type = 'brokers'; //$this->uri->segment(4);
		$utype = ($type == 'brokers') ? 'N' : 'Y';

		$user_session = $this->session->all_userdata();

		$logs = $this->master->getRecords('biz_user_log', array('brokerage_id'=>$user_session['brokerage_id'], 'super_admin'=>'Y'), '*', array('id'=>'DESC'));

		$single = 'Access Log';
		$title = $single;

		$data = array(
			'middle_content'=>'manage-accesslog',
			'title'=>$title,
			'logs'=>$logs,
			'singular_title'=>$single,
		);

		$this->load->view('admin/admin-view',$data);

	}



}
