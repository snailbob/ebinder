<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jobs extends CI_Controller 

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/jobs/manage');

	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function manage() {
		$tasks = $this->master->getRecords('task_job','','*',array('id'=>'DESC'));

		$data = array(
			'middle_content'=>'manage-tasks',
			'page_title'=>'Change Password',
			'tasks'=>$tasks
		);	
		$this->load->view('admin/admin-view',$data);


	}

	
	#--------------------------------------------->>delete view loading<<-------------------------------------
	public function delete() {
		$id = $this->uri->segment(4);

		if($this->master->deleteRecord('task_job','id',$id)) {	
		
			$success_mess = 'Job successfully deleted';
			$this->session->set_flashdata('success',$success_mess);
			redirect('webmanager/jobs/manage');
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
			redirect('webmanager/jobs/manage');
		}
	}

	

}