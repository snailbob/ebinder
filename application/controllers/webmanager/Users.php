<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/users/manage');

	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function manage() {
		$users = $this->master->getRecords('agency');

		$data = array(
			'middle_content'=>'manage-users',
			'page_title'=>'Change Password',
			'users'=>$users
		);
		$this->load->view('admin/admin-view',$data);


	}

	public function add(){
		$email = $_POST['email'];
		$name = $_POST['name'];
		$nowtime = date('Y-m-d H:i:s');
		$data = array(
			'name'=>$name,
			'email'=>$email,
			'status'=>'N',
			'date_added'=>$nowtime
		);
		$id = $this->master->insertRecord('agency', $data, TRUE);

		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings for studio
		$info_arr = array(
			'to'=>$email,
			'from'=>$adminemail,
			'subject'=>'Welcome to Transit Insurance',
			'view'=>'new-agency-confirm-email',
		);

		$other_info = array(
			'password'=>'',
			'emailer_file_name'=>'new-agency-confirm-email',
			'name'=>$name,
			'studio_name'=>$name,
			'user_name'=>$email,
			'user_email'=>$email,
			'link'=>'settings/confirm_agency/'.$id.'/'.md5($email)
		);

		$data['info_arr'] = $info_arr;
		$data['other_info'] = $other_info;
		$this->emailer->sendmail($info_arr,$other_info);
		$this->session->set_flashdata('success',' Agency successfully added.');
		echo json_encode($data);

	}

	#--------------------------------------------->>activate view loading<<-------------------------------------
	public function activate() {
		$inc = $this->uri->segment(4);
		$user_id = $this->uri->segment(5);

		if($this->master->updateRecord('agency',array('enabled'=>$inc),array('id'=>$user_id))){
			if($inc == 'Y'){
				$success_mess = 'Agency successfully activated';
			}
			else{
				$success_mess = 'Agency successfully deactivated';
			}

			$this->session->set_flashdata('success',$success_mess);

		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
		}

		redirect('webmanager/customers/agencies');

	}


	#--------------------------------------------->>get_info view loading<<-------------------------------------
	public function get_info() {
		$id = $_POST['id'];

		$user = $this->master->getRecords('agency',array('id'=>$id));


		$info = array(
			'email'=>$user[0]['email'],
			'first_name'=>$user[0]['first_name'],
			'last_name'=>$user[0]['last_name'],
			'address'=>$user[0]['address'],
			'require_space'=>$user[0]['require_space'],
			'trade_space'=>$user[0]['trade_space'],
		);

		echo json_encode($info);

	}
	#--------------------------------------------->>get_info view loading<<-------------------------------------
	public function update_info() {
		$first = $_POST['firstname'];
		$last = $_POST['lastname'];
		$address = $_POST['address'];
		$trade_space = $_POST['trade_space'];
		$require_space = $_POST['require_space'];
		$id = $_POST['id'];


		$arr = array(
			'first_name'=>$first,
			'last_name'=>$last,
			'address'=>$address,
			'require_space'=>$require_space,
			'trade_space'=>$trade_space
		);

		if($this->master->updateRecord('agency',$arr,array('id'=>$id))){
			$this->session->set_flashdata('success',' Studio info updated successfully');
			echo 'success';
		}else{
			echo 'error';
		}
	}


	#--------------------------------------------->>delete view loading<<-------------------------------------
	public function delete() {
		$user_id = $this->uri->segment(4);

		if($this->master->deleteRecord('agency','id',$user_id)) {

			//delete offers and tasks
			$this->master->deleteRecord('customers','studio_id',$user_id);
			$this->master->deleteRecord('classes','user_id',$user_id);

			$success_mess = 'Studio successfully deleted';
			$this->session->set_flashdata('success',$success_mess);
			redirect('webmanager/users/manage');
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
			redirect('webmanager/users/manage');
		}
	}


}
