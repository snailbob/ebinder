<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contents extends CI_Controller 

{

	public function __construct(){

		parent::__construct();}


		
	
	public function legal(){
		$admin_info = $this->master->getRecords('admin');
	
		$data = array(
			'middle_content'=>'manage-legals',
			'title'=>'Legal',
			'admin_info'=>$admin_info,
		);	
		
		$this->load->view('admin/admin-view',$data);
	}
	
	public function bannertext(){
		
		$thefield = 'banner_text';
		$admin_info = $this->master->getRecords('admin','', $thefield);
	
		$data = array(
			'middle_content'=>'manage-contenttext',
			'title'=>'Banner Text',
			'admin_info'=>$admin_info,
			'thefield'=>$thefield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	public function pagetext(){
		
		$thefield = 'light_section';
		$admin_info = $this->master->getRecords('admin','', $thefield);
	
		$data = array(
			'middle_content'=>'manage-contenttext',
			'title'=>'Page Text',
			'admin_info'=>$admin_info,
			'thefield'=>$thefield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	public function savetexts(){
		$contentfield = $_POST['contentfield'];
		$title = $_POST['title'];
		$titlefield = $_POST['titlefield'];
		$type = $_POST['type'];
		$id = $_POST['id'];
		
		$arr = array(
			'title'=>$titlefield,
			'content'=>$contentfield
		);
		
		if($id == ''){
			$arr['type'] = $type; 
			$id = $this->master->insertRecord('admin_contents', $arr, true);
			$arr['id'] = $id;
		}
		else{
			$this->master->updateRecord('admin_contents', $arr, array('id'=>$id));
		}
		
		
		echo json_encode($arr);

	}
	
	public function aboutus(){
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'aboutus';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'About Us Page',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	public function firstsection(){
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'firsthomesection';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'First Home Section',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	

	public function alternatehead(){
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'alternatingcontenthead';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'Alternating Content Head',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	
	public function manage_alternates(){
		$pagetype = 'alternatingcontents';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-alternates',
			'title'=>'Alternates',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
		);	
		
		$this->load->view('admin/admin-view',$data);
	}

	
	public function alternate(){
		$id = $this->uri->segment(4);
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'alternatingcontents';
		$admin_info = $this->master->getRecords('admin_contents', array('id'=>$id));
		
		if(count($admin_info) == 0){
			$admin_info = array(array(
				'title'=>'',
				'id'=>'',
				'content'=>'',
				'image'=>''
			));
		}
	
		$data = array(
			'middle_content'=>'add-alternate',
			'title'=>'Alternate',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
	}
	
	public function delete_alt(){
		$user_id = $this->uri->segment(4);

		if($this->master->deleteRecord('admin_contents','id',$user_id)) {	
			$success_mess = 'Content successfully deleted';
			$this->session->set_flashdata('success',$success_mess);
		}else{
			$this->session->set_flashdata('error','Something went wrong. Please try again.');
		}
		
		redirect('webmanager/contents/manage_alternates');
	}
	

	public function threeparts(){
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'threepartshead';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-aboutus',
			'title'=>'Three Columns Head',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
		
	}
	
	
	public function manage_threeparts(){
		$pagetype = 'threeparts';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-threeparts',
			'title'=>'Three Column Contents',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
		);	
		
		$this->load->view('admin/admin-view',$data);
	}

	
	public function threepart(){
		$id = $this->uri->segment(4);
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'threeparts';
		$admin_info = $this->master->getRecords('admin_contents', array('id'=>$id));
		
		if(count($admin_info) == 0){
			$admin_info = array(array(
				'title'=>'',
				'id'=>'',
				'content'=>'',
				'image'=>''
			));
		}
	
		$data = array(
			'middle_content'=>'add-alternate',
			'title'=>'Three Column Content',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
	}	

	public function banners(){
		$pagetype = 'banner';
		$admin_info = $this->master->getRecords('admin_contents', array('type'=>$pagetype));
	
		$data = array(
			'middle_content'=>'manage-banners',
			'title'=>'Homepage Banners',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
		);	
		
		$this->load->view('admin/admin-view',$data);
	}

	
	public function banner(){
		$id = $this->uri->segment(4);
		
		$titlefield = 'title';
		$contentfield = 'content';
		$pagetype = 'banner';
		$admin_info = $this->master->getRecords('admin_contents', array('id'=>$id));
		
		if(count($admin_info) == 0){
			$admin_info = array(array(
				'title'=>'',
				'id'=>'',
				'content'=>'',
				'image'=>''
			));
		}
	
		$data = array(
			'middle_content'=>'add-alternate',
			'title'=>'Homepage Banner',
			'admin_info'=>$admin_info,
			'pagetype'=>$pagetype,
			'titlefield'=>$titlefield,
			'contentfield'=>$contentfield
		);	
		
		$this->load->view('admin/admin-view',$data);
	}	

}