<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller 

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	/*------------------------------------
	index
	-------------------------------------*/
	public function index() {

		redirect(base_url().'webmanager/products/manage');

	}


	/*------------------------------------
	manage
	-------------------------------------*/
	public function manage() {
		$q_products = $this->master->getRecords('q_products');

		$data = array(
			'middle_content'=>'manage-products',
			'title'=>'Insurance Products',
			'singular_title'=>'Insurance Product',
			'q_products'=>$q_products
		);	
		$this->load->view('admin/admin-view',$data);

	}
	
	
	
	/*------------------------------------
	questionnaires
	-------------------------------------*/
	public function questionnaires(){
		$id = $this->uri->segment(4);
		$product = $this->master->getRecords('q_products', array('id'=>$id));
		$questionnaires = $this->master->getRecords('questionnaires', array('product_id'=>$id));
		
		if(count($product) == 0){
			$this->session->set_flashdata('error','No product found.');
			redirect('webmanager/products');
			return false;
		}
	
		$data = array(
			'middle_content'=>'manage-questionnaires',
			'title'=>$product[0]['product_name'].' Questionnaire',
			'singular_title'=>'Question',
			'products'=>$product,
			'product_id'=>$id,
			'questionnaires'=>$questionnaires
		);	
		$this->load->view('admin/admin-view',$data);
	}
	
	/*------------------------------------
	save_product
	-------------------------------------*/
	public function save_product() {
		$product_name = $_POST['product_name'];
		$helper = $_POST['helper'];
		$id = $_POST['id'];
		$broker_id = $_POST['broker_id'];
		
		
		$nowtime = date('Y-m-d H:i:s');
		$msg = 'error';
		
		$arr = array(
			'product_name'=>$product_name,
			'helper'=>$helper,
			'broker_id'=>$broker_id,
		);
		
		if($id != ''){
			if($this->master->updateRecord('q_products',$arr, array('id'=>$id))){
				$this->session->set_flashdata('success',' Product updated successfully');
				$msg = 'success';
			}
		}
		else{
			$arr['date_added'] = $nowtime;	
			$this->master->insertRecord('q_products',$arr);
		
			$this->session->set_flashdata('success',' Product added successfully');
			$msg = 'success';
		}
		
		echo json_encode($arr);
		
	}
	
	
	/*------------------------------------
	save_question
	-------------------------------------*/
	public function save_question() {
		$question = $_POST['question'];
		$id = $_POST['id'];
		$broker_id = $_POST['broker_id'];
		$product_id = $_POST['product_id'];
		
		
		$nowtime = date('Y-m-d H:i:s');
		$msg = 'error';
		
		$arr = array(
			'question'=>$question,
			'broker_id'=>$broker_id,
			'product_id'=>$product_id,
		);
		
		if($id != ''){
			if($this->master->updateRecord('questionnaires',$arr, array('id'=>$id))){
				$this->session->set_flashdata('success',' Question updated successfully');
				$msg = 'success';
			}
		}
		else{
			$arr['date_added'] = $nowtime;	
			$this->master->insertRecord('questionnaires',$arr);
		
			$this->session->set_flashdata('success',' Question added successfully');
			$msg = 'success';
		}
		
		echo json_encode($arr);
		
	}
	

}