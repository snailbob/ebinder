<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reviews extends CI_Controller 

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/users/manage');

	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function manage() {
		$reviews = $this->master->getRecords('reviews',array('average_star <'=>2.5));

		$data = array(
			'middle_content'=>'manage-reviews',
			'page_title'=>'Change Password',
			'reviews'=>$reviews
		);	
		$this->load->view('admin/admin-view',$data);


	}
	
	
	#--------------------------------------------->>toggle_show view loading<<-------------------------------------
	public function toggle_show() {
		$rev_id = $this->uri->segment(4);
		$i = $this->uri->segment(5);
		
		if($this->master->updateRecord('reviews',array('allowed_to_display'=>$i),array('id'=>$rev_id))){

			$this->session->set_flashdata('success','Review successfully updated.');
			redirect('webmanager/reviews/manage');

		}else{

			$this->session->set_flashdata('success','Something went wrong. Please try again');
			redirect('webmanager/reviews/manage');
		}
	}
	
	
	

}