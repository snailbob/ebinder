<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Emails extends CI_Controller 

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/emails/manage');

	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function manage() {
		$emails = $this->master->getRecords('outgoing_emails');

		$data = array(
			'middle_content'=>'manage-emails',
			'page_title'=>'Change Password',
			'emails'=>$emails
		);	
		$this->load->view('admin/admin-view',$data);


	}


	#--------------------------------------------->>update view loading<<-------------------------------------
	public function update()

	{

		$mess_id = $this->uri->segment(4);

		if(isset($_POST['add_reseller'])) {

			//$this->form_validation->set_rules('e_name','Name','required|xss_clean');


			$e_name=$this->input->post('e_name',true);

			$content=$this->input->post('content',true);


			$mess_arr=array(
			//	'name'=>$e_name,
				'content'=>$content
				
			);

					

			if($this->master->updateRecord('outgoing_emails',$mess_arr,array('id'=>$mess_id)))

			{

				$this->session->set_flashdata('success',' Email updated successfully');

				redirect(base_url().'webmanager/emails/update/'.$mess_id);

			}

			else

			{

				$this->session->set_flashdata('error','Error while updating reseller');

				redirect(base_url().'webmanager/emailer/update/'.$mess_id);

			}


		}

		

		//$this->db->join('cf_login_master as login','login.login_id=cc.login_id');

		$mess_info=$this->master->getRecords('outgoing_emails',array('id'=>$mess_id));


		$data=array('page_title'=>'Update CC','middle_content'=>'update-emailer','mess_info'=>$mess_info);

		$this->load->view('admin/admin-view',$data);

	}

	
	
	public function update_subject(){
		$id = $_POST['id'];
		$subject = $_POST['subject'];

		if($this->master->updateRecord('outgoing_emails',array('subject'=>$subject),array('id'=>$id))){
			echo 'success';
		}
		
		
	}
	
}