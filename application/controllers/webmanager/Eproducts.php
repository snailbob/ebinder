<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Eproducts extends CI_Controller

{

	public function __construct(){

		parent::__construct();
		$this->config->set_item('base_url',$this->common->base_url()) ;
	}


	#--------------------------------------------->>index view loading<<-------------------------------------
	public function index() {

		redirect(base_url().'webmanager/users/manage');

	}


	#--------------------------------------------->>manage view loading<<-------------------------------------
	public function manage() {
		$q_products = $this->master->getRecords('q_products', '', '*', array('id'=>'DESC'));

		$data = array(
			'middle_content'=>'manage-eproducts',
			'title'=>'Products',
			'singular_title'=>'Product',
			'q_products'=>$q_products
		);
		$this->load->view('admin/admin-view',$data);


	}


	/*------------------------------------
	questionnaires
	-------------------------------------*/
	public function questionnaires(){
		$id = $this->uri->segment(4);
		$product = $this->master->getRecords('q_products', array('id'=>$id));
		$questionnaires = $this->load->view('domains/quote/quote_form2','', true);
		$claims = $this->load->view('domains/quote/quote_form3', '', true);
		$declarations = $this->load->view('domains/quote/quote_form4', '',  true);

		if(count($product) == 0){
			$this->session->set_flashdata('error','No product found.');
			redirect('webmanager/products');
			return false;
		}

		$data = array(
			'middle_content'=>'manage-equestionnaires',
			'title'=>$product[0]['product_name'].' Questionnaire',
			'singular_title'=>'Question',
			'products'=>$product,
			'product_id'=>$id,
			'claims'=>$claims,
			'declarations'=>$declarations,
			'questionnaires'=>$questionnaires
		);
		$this->load->view('admin/admin-view',$data);
	}



	/*------------------------------------
	ratings
	-------------------------------------*/
	public function ratings() {
		$admin_info = $this->master->getRecords('admin');
		$user_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];
		$agency_info = $this->master->getRecords('agency', array('id'=>$user_id));


		$deductibles = $this->master->getRecords('deductibles', array('agency_id'=>$user_id));
		$cargo = $this->master->getRecords('q_business_specific'); //this->master->getRecords('cargo_category');
		$transportations = $this->common->transportations();
		$countries = $this->master->getRecords('country_t');
		$currencies = $this->master->getRecords('api_currencies', array('in_stripe'=>'Y'));


		$cargo_prices = $this->common->agency_prices($user_id,'cargo_prices');
		$cargo_max = $this->common->agency_prices($user_id,'cargo_max');
		$transportation_prices = $this->common->agency_prices($user_id,'transportation_prices');
		$country_prices = $this->common->agency_prices($user_id,'country_prices');
		$country_referral = $this->common->agency_prices($user_id,'country_referral');
		$zone_multiplier = $this->common->agency_prices($user_id,'zone_multiplier');
		$base_currency = ($agency_info[0]['base_currency']) ? $agency_info[0]['base_currency'] : 'AUD';

		$data = array(
			'middle_content'=>'manage-eratings',
			'title'=>'Rating',
			'cargo'=>$cargo,
			'deductibles'=>$deductibles,
			'transportations'=>$transportations,
			'countries'=>$countries,
			'cargo_prices'=>$cargo_prices,
			'cargo_max'=>$cargo_max,
			'transportation_prices'=>$transportation_prices,
			'country_prices'=>$country_prices,
			'agency_info'=>$agency_info,
			'country_referral'=>$country_referral,
			'zone_multiplier'=>$zone_multiplier,
			'user_id'=>$user_id,
			'agency_id'=>$user_id,
			'currencies'=>$currencies,
			'base_currency'=>$base_currency
		);
//		echo json_encode($data);
//		return false;


		$this->load->view('admin/admin-view',$data);

	}




	/***************************
		preview
	***************************/
	public function preview(){
		$first_customer_placeholder = $this->master->getRecords('customers');
		$user_session = $this->common->customer_format($first_customer_placeholder[0]['id']);

		$type = $this->uri->segment(3);
		$modde = $this->uri->segment(4);
		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$mode = ($modde == 'edit') ? 'quote_editable' : 'quote';

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);

		$base_link = $this->common->base_url();

		$questionnaire = $this->master->getRecords('biz_prod_questionnaire');

		//get current active product id
		$prod_type = (isset($_GET['product_id'])) ? $_GET['product_id']: $this->session->userdata('preview_product_id');
		if(!empty($prod_type)) {
			$this->session->set_userdata('preview_product_id', $prod_type);
		}
		$prod_type = (empty($prod_type)) ? '3' : $prod_type;
		$product_name = $this->common->db_field_id('q_products', 'product_name', $prod_type);


		$data = array(

			'view'=>'ml_customer_quote',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'title'=>$product_name,
			'singular_title'=>$product_name,
			'biz_referral'=>array(),
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'base_link'=>$base_link
		);

		$thepage = (isset($_GET['tab']) && !empty($_GET['tab'])) ? $_GET['tab'] : '1';
		$data['thepage'] = $thepage;
		$data['theindex'] = $thepage - 1;
		$data['quote_view'] = 'domains/'.$mode.'/quote_form'.$thepage;
		$data['bizcat'] = $this->master->getRecords('q_business_categories');
		$data['countries'] = $this->common->country_format();
		$data['questionnaire'] = $questionnaire;


		$homebizdata = $this->common->homebiz_data();

		$data = array_merge($data, $homebizdata);

		$data['the_quote'] = array();
		if($thepage == '5'){
			$data['the_quote'] = $this->common->thequote($data);
		}


		// echo json_encode($data);
		// return false;

		$this->load->view('domains/view', $data);

	}



}
