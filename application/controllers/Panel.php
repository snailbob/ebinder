<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$user_session = $this->session->all_userdata();
		if(empty($user_session['id'])){
			redirect();
		}
		else{
			if($this->common->base_url() == 'http://localhost/nguyen/ebinder.com.au/'){ //http://localhost/nguyen/ml.ebinder.com.au/
				if(($this->common->the_domain() != $user_session['domain']) && ($this->common->the_domain() == '')){
					// redirect($this->common->base_url().'panel');

				}
			}
			else{
				if(($this->common->the_domain() != $user_session['domain'])){
					$url = 'http://'.$user_session['domain'].'.ebinder.com.au/panel';
					redirect($url);
				}
			}
		}


		$this->config->set_item('base_url',$this->common->base_url()) ;

	}


	/***************************
		index
	***************************/
	public function index(){

		// echo $this->common->the_domain(); //$this->common->base_url();
		// return false;
		$user_session = $this->session->all_userdata();

		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$user_id = (isset($user_session['id'])) ? $user_session['id'] : '0';
		$id = $user_id;



		$agency_id = (isset($user_session['logged_admin_agency'])) ? $user_session['logged_admin_agency'] : '0';

		$whr = array(
			'agency_id'=>$agency_id,
			'needs_referral'=>'N',
			'bound'=>'Y'
		);
		$format_referral = $this->common->format_referral($whr);
		$q_widget = $this->master->getRecords('q_widget', array('broker_id'=>$user_id));


		$total_premium_all = $this->common->total_premium_all($format_referral);
		$widget_topcustomers = $this->common->widget_topcustomers($id);




		$mymembers = array();
		if($user_session['customer_type'] == 'N' && $user_session['studio_id'] == '0'){
			$mymembers = $this->common->customers_list($id, 'N');
		}


		$broker = $this->common->brokers($id);
		$base_link = $this->common->base_url();
		$data = array(
			'view'=>'dashboard',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'title'=>'Panel',
			'singular'=>'Organization Member',
			'customers'=>$mymembers,
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'format_referral'=>$format_referral,
			'total_premium_all'=>$total_premium_all,
			'widget_topcustomers'=>$widget_topcustomers,
			'q_widget'=>$q_widget,
			'base_link'=>$base_link
		);


		// echo json_encode($format_referral);
		// return false;
		 $this->load->view('domains/view', $data);

		//$this->load->view('user_panel/admin-view',$data);


	}


	/***************************
		addmember
	***************************/
	public function addmember(){
		$user_session = $this->session->all_userdata();

		$type = $this->uri->segment(3);
		$answer_id = $this->uri->segment(4);

		$id = $user_session['id'];

		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$broker = $this->common->brokers($id);
		$base_link = $this->common->base_url();


		$data = array(

			'view'=>'add_org_member_view',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'title'=> 'Add Organization Member',
			'singular'=>'Add Organization Member',
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
		);
		$data['countries'] = $this->common->country_format();

//		echo json_encode($customers);
//		return false;
		$this->load->view('domains/view', $data);

	}

	/***************************
		login
	***************************/
	public function login(){
		$this->load->view('domains/login');
	}




	/***************************
		managepolicies
	***************************/
	public function managepolicies(){
		$user_session = $this->session->all_userdata();

		$type = $this->uri->segment(3);
		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);

		$base_link = $this->common->base_url();

		$broker_id = $user_session['id'];

		if($user_session['first_broker'] == 'Y'){

			$whr = array('brokerage_id'=>$user_session['brokerage_id'],'bound'=>'Y');
			$biz_referral = $this->common->format_referral($whr);
		}
		else{
			$whr = array('broker_id'=>$broker_id, 'bound'=>'Y');
			$biz_referral = $this->common->format_referral($whr);
		}




		$data = array(
			'view'=>($type == '') ? 'ml-manage-policies' : 'ml-each-quote',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'title'=>'Policies',
			'biz_referral'=>$biz_referral,
			'singular'=>'Policies',
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'base_link'=>$base_link
		);



		if($type != ''){
			$arr['id'] = $type;

			$homebizdata = $this->common->homebiz_data();
			$biz_referral = $this->master->getRecords('biz_referral', $arr);

			$data['biz_referral'] = $biz_referral;
			$data['content'] = (isset($biz_referral[0]['content'])) ? unserialize($biz_referral[0]['content']) : array();
			$data['customer'] = (isset($biz_referral[0]['customer'])) ? unserialize($biz_referral[0]['customer']) : array();

			$data = array_merge($data, $homebizdata);

		}


				// echo json_encode($biz_referral);
				// return false;
		$this->load->view('domains/view', $data);


	}



	/***************************
		managequote
	***************************/
	public function managequote(){
		$user_session = $this->session->all_userdata();

		$type = $this->uri->segment(3);
		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);

		$base_link = $this->common->base_url();

		$broker_id = $user_session['id'];

		if($user_session['first_broker'] == 'Y'){
			$biz_referral = $this->master->getRecords('biz_referral', array('brokerage_id'=>$user_session['brokerage_id'], 'customer_approved'=>'Y', 'bound'=>'N'));
		}
		else{
			$biz_referral = $this->master->getRecords('biz_referral', array('broker_id'=>$broker_id, 'customer_approved'=>'Y', 'bound'=>'N'));
		}




		$data = array(
			'view'=>($type == '') ? 'ml-manage-quotes' : 'ml-each-quote',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'title'=>'Quotes',
			'biz_referral'=>$biz_referral,
			'singular_title'=>'Quote',
			'singular'=>'Quote',
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'base_link'=>$base_link
		);



		if($type != ''){
			$arr['id'] = $type;

			$homebizdata = $this->common->homebiz_data();
			$biz_referral = $this->master->getRecords('biz_referral', $arr);

			$data['biz_referral'] = $biz_referral;
			$data['content'] = (isset($biz_referral[0]['content'])) ? unserialize($biz_referral[0]['content']) : array();
			$data['customer'] = (isset($biz_referral[0]['customer'])) ? unserialize($biz_referral[0]['customer']) : array();

			// echo json_encode($data['content']); return false;
			$data = array_merge($data, $homebizdata);

		}


		$this->load->view('domains/view', $data);


	}

	/***************************
		claims
	***************************/
	public function claims(){
		$user_session = $this->session->all_userdata();



		$type = $this->uri->segment(3);
		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);

		$base_link = $this->common->base_url();

		$broker_id = $user_session['id'];

		$claims = $this->master->getRecords('claims', array('broker_id'=>$id));
		$customers = $this->common->customers_list($id, 'Y');


		if($user_session['first_broker'] == 'Y'){

			$whr = array('brokerage_id'=>$user_session['brokerage_id'], 'bound'=>'Y'); //, 'customer_approved'=>'Y'
			$biz_referral = $this->common->format_referral($whr);
		}
		else{
			$whr = array('broker_id'=>$broker_id, 'bound'=>'Y'); //'customer_approved'=>'Y',
			$biz_referral = $this->common->format_referral($whr);
		}

		// echo json_encode($customers); return false;
		$uniquetime= time().mt_rand();

		$data = array(
			'view'=>($type == '') ? 'ml-claims' : 'ml-claims-add',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'title'=>($type == '') ? 'Claims' : 'Claim',
			'claims'=>$claims,
			'customers'=>$customers,
			'biz_referral'=>$biz_referral,
			'singular'=>'Claim',
			'singular_title'=>'Claim',
			'uniquetime'=>$uniquetime,
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'base_link'=>$base_link
		);


//		echo json_encode($data);
//		return false;
		$this->load->view('domains/view', $data);


	}

	/***************************
		claim
	***************************/
	public function claim(){
		$user_session = $this->session->all_userdata();


		$type = $this->uri->segment(3);
		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);

		$base_link = $this->common->base_url();

		$broker_id = $user_session['id'];

		$claims = $this->master->getRecords('claims', array('id'=>$type));
		$brokerr = $this->common->customer_format($claims[0]['broker_id']);


		$whr_first = array(
			'customer_type'=>'N',
			'agency_id'=>$claims[0]['agency_id'],
		);
		$first_under = $this->master->getRecords('customers', $whr_first, '*', array('first_underwriter'=>'ASC'));
		$underwriter = $this->common->customer_format($first_under[0]['id']);

		$messages = $this->master->getRecords('claims_message_file', array('message_id'=>$type));

		$to = $first_under[0]['id'];

		$uniquetime= time().mt_rand();

		$data = array(
			'view'=>'ml-claims-each',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'title'=>'Claim Details',
			'to'=>$to,
			'message_id'=>$type,
			'claims'=>$claims,
			'broker'=>$brokerr,
			'underwriter'=>$underwriter,
			'singular'=>'Claim',
			'uniquetime'=>$uniquetime,
			'messages'=>$messages,
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'base_link'=>$base_link
		);


//		echo json_encode($data);
//		return false;
		$this->load->view('domains/view', $data);


	}



	/***************************
		quote
	***************************/
	public function quote(){

		$user_session = $this->session->all_userdata();

		$type = $this->uri->segment(3);
		$type = (empty($type)) ? '3' : $type;
		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);
		$customers = $this->common->customers_list($id);
		$q_products = $this->master->getRecords('q_products');
		$quote_product_id = $this->session->userdata('quote_product_id');
		$product_name = $this->common->db_field_id('q_products', 'product_name', $quote_product_id);


		$broker_customers = $this->common->customer_format(NULL, array('studio_id'=>$id)); //brokerage_brokers($brokerage_id);
		$broker_customers = (isset($customers['id'])) ? array($customers) : $customers;

		// echo json_encode($broker_customers); return false;
		$broker_id = $user_session['id'];


		if($user_session['first_broker'] == 'Y'){
			$whr = array(
				'brokerage_id'=>$user_session['brokerage_id'],
				// 'needs_referral'=>'N',
				'bound'=>'N'
			);
			$biz_referral = $this->common->format_referral($whr);

		}
		else{
			$whr = array(
				'broker_id'=>$broker_id,
				// 'needs_referral'=>'N',
				'bound'=>'N'
			);
			$biz_referral = $this->common->format_referral($whr);

		}

		// $biz_referral2 = array();
		// if(count($biz_referral) > 0) {
		// 	foreach($biz_referral as $u=>$value){
		// 		if(!empty($value['exp_timeto_display'])){
		// 			$biz_referral2[] = $value;
		// 		}
		// 	}
		// }
		// $biz_referral = $biz_referral2;

		//  echo json_encode($biz_referral); return false;


		$base_link = $this->common->base_url();

		$data = array(

			'view'=>'ml_customer_quote',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'customers'=>$customers,
			'broker_customers'=>$broker_customers,
			'q_products'=>$q_products,
			'title'=>'Quotes', //$product_name.' Quotes',
			'singular'=>$product_name.' Quote',
			'singular_title'=>$product_name.' Quote',
			'biz_referral'=>$biz_referral,
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'base_link'=>$base_link
		);

		$thepage = (isset($_GET['tab']) && !empty($_GET['tab'])) ? $_GET['tab'] : '0';

		if($thepage > 7){
				$thepage = '0';
		}


		$data['thepage'] = $thepage;
		$data['theindex'] = ($thepage != 0) ? $thepage - 1 : 0;
		$data['quote_view'] = 'domains/quote/quote_form'.$thepage;
		$data['bizcat'] = $this->master->getRecords('q_business_categories');
		$data['countries'] = $this->common->country_format();





		$homebizdata = $this->common->homebiz_data('quote');

		$data = array_merge($data, $homebizdata);

		$data['the_quote'] = array();
		if($thepage == '5'){
			$data['the_quote'] = $this->common->thequote($data);
		}


		// echo json_encode($biz_referral);
		// return false;

		$this->load->view('domains/view', $data);

	}

	/***************************
		customers
	***************************/
	public function customers(){
		$user_session = $this->session->all_userdata();

		$type = $this->uri->segment(3);
		$answer_id = $this->uri->segment(4);

		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$brokerage_id = $user_session['brokerage_id'];
		$broker = $this->common->brokers($id);
		$customers = $this->common->brokerage_brokers($brokerage_id);
		$customers = (isset($customers[0])) ? $customers : array($customers);
		//$customers = $this->common->responses_format($answer_id, array('broker_id'=>$id));



		$q_products = $this->master->getRecords('q_products');

		$customer_empty = (isset($user_session['info_format']['product'])) ? 'customers_profile' : 'customers_add';
		$base_link = $this->common->base_url();

		$data = array(

			'view'=>($type == 'add') ? $customer_empty : 'customers_all',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'customers'=>$customers,
			'q_products'=>$q_products,
			'title'=>($type == 'add') ? 'Add Quote' : 'Users',
			'singular'=>($type == 'add') ? 'Quote' : 'User',
			'singular_title'=>($type == 'add') ? 'Quote' : 'User',
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'base_link'=>$base_link
		);

		$data['countries'] = $this->common->country_format();

		$thepage = (isset($_GET['tab'])) ? $_GET['tab'] : '1';


		//for tab2, check if have added info
		if($thepage == '2' && $customer_empty == 'customers_profile'){
			$data['view'] = 'customer_tab2';
		}


		if($type == 'add'){
			$data['bizcat'] = $this->master->getRecords('q_business_categories');
			$data['countries'] = $this->common->country_format();
		}

		if($type == 'policy'){
			$data['title'] = 'Policy Management';
			$data['view'] = 'broker_policy_view';
		}


		if(!empty($answer_id)){
			$data['title'] = 'Customer Responses';
			$data['responses'] = $this->common->responses_format($answer_id, array('broker_id'=>$id));
			$data['view'] = 'response_each_view';
		}


/*		echo json_encode($customers);
		return false;
*/

		$this->load->view('domains/view', $data);
	}



	/***************************
		use_customer
	***************************/
	public function use_customer(){
		$user_session = $this->session->all_userdata();

		$type = $this->uri->segment(3);
		$answer_id = $this->uri->segment(4);

		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$agency_id = $this->common->default_cargo();

		$brokerage_id = $user_session['brokerage_id'];
		$broker = $this->common->brokers($id);
		$customers = $this->common->customer_format(NULL, array('studio_id'=>$id, 'customer_type'=>'Y')); //brokerage_brokers($brokerage_id);
		$customers = (isset($customers['id'])) ? array($customers) : $customers;
		//$customers = $this->common->responses_format($answer_id, array('broker_id'=>$id));

		$occupations = $this->master->getRecords('q_business_specific');
		$q_products = $this->master->getRecords('q_products');

		$customer_empty = (isset($user_session['info_format']['product'])) ? 'customers_profile' : 'customers_add';
		$base_link = $this->common->base_url();

		$single = ($type == 'add') ? 'Quote' : 'Customer';

		$countries = $this->common->country_format();

		$brokerage = $this->master->getRecords('brokerage', array('agency_id'=>$agency_id));

		$customer_details = $this->session->userdata('customer_details');
		$is_edit = (isset($customer_details) && !empty($customer_details['id'])) ? 'Edit' : 'Add';
		$data = array(

			'view'=>($type == 'add') ? $customer_empty : 'customers_use_all',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'customers'=>$customers,
			'q_products'=>$q_products,
			'title'=>($type == 'add') ? 'Add Quote' : 'Customers',
			'singular'=>$single,
			'singular_title'=>$single,
			'countries'=>$countries,
			'customer_details'=>$customer_details,
			'is_edit'=>$is_edit,
			'brokerage'=>$brokerage,
			'occupations'=>$occupations,
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'base_link'=>$base_link
		);


		$thepage = (isset($_GET['tab']) && !empty($_GET['tab'])) ? $_GET['tab'] : '1';

		if($thepage > 7){
				$thepage = '1';
		}

		$data['thepage'] = $thepage;
		$data['theindex'] = $thepage - 1;

		//skip claims and quote & policies if add
		$cd_id = '';
		if(isset($customer_details['id'])){
			if($customer_details['id'] != ''){
				$cd_id = $customer_details['id'];
			}
		}
		$current_page = $thepage;
		if($thepage == '5' || $thepage == '6'){
			if(empty($cd_id)){
				$current_page = '7';
			}
		}


		$data['themenu'] = $this->common->customers_tab_menu($customer_details);

		$data['quote_view'] = 'domains/quote/customer_form'.$current_page;


/*		echo json_encode($customers);
		return false;
*/

		$this->load->view('domains/view', $data);
	}


	public function add_customer_quote(){
		$id = $this->uri->segment(3);
		$customer = $this->common->customer_format($id); //brokerage_brokers($brokerage_id);
		$customer_info = (@unserialize($customer['customer_info'])) ? unserialize($customer['customer_info']) : array();
		$brokerage = (isset($customer_info['company_name'])) ? $customer_info['company_name'] : '';

		$occupation_id = (isset($customer_info['occupation_id'])) ? $customer_info['occupation_id'] : '';
		$occupation_find = (!empty($occupation_id)) ? $this->common->db_field_id('q_business_specific', 'specific_name', $occupation_id) : '';
		$homebiz1 = array(
			'user_id'=>$customer['studio_id'],
			'1_customer_id'=>$id,
			'1_customer_name'=>$brokerage, //$customer['first_name'].' '.$customer['last_name'],
			'1_total_annual_revenue'=>(isset($customer_info['annaul_revenue'])) ? $customer_info['annaul_revenue'] : '',
			'occupation_id'=>$occupation_id,
			'occupation_find'=>$occupation_find,
			'1_please_state_the_total_number_of_employees_including_self'=>(isset($customer_info['employee_number'])) ? $customer_info['employee_number'] : '',
			'1_in_respect_of_income_earned_for_the_current_financial_year_please_provide_a_breakdown_by_state'=>(isset($customer_info['breakdown_by_state'])) ? $customer_info['breakdown_by_state'] : array(),

		);

		$homebiz0 = array(
			'user_id'=>$customer['studio_id'],
			'customer_id'=>$id,
			'new_or_existing_customer'=>'Existing'
		);

		$this->session->set_userdata('homebiz0', $homebiz0);
		$this->session->set_userdata('homebiz1', $homebiz1);
		// echo json_encode($homebiz1); return false;
		redirect('panel/quote?tab=1#quote');

	}


	/***************************
		productstest
	***************************/
	public function productstest(){
		$data['responses'] = $this->common->responses_format(5);

		echo json_encode($data['responses']);
		return false;
	}


	/***************************
		userlog
	***************************/
	public function userlog(){
		$user_session = $this->session->all_userdata();

		$type = $this->uri->segment(3);
		$answer_id = $this->uri->segment(4);

		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);

		$activity_logged = $this->session->userdata('activity_logged');
		$q_products = $this->master->getRecords('biz_user_log', array('brokerage_id'=>$user_session['brokerage_id'], 'first_broker'=>'Y'), '*', array('id'=>'DESC'));

		$q_products = ($activity_logged == 'Y') ? $q_products : array();



		$single = 'User Log';
		$data = array(

			'view'=>'ml_userlog_view',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'q_products'=>$q_products,
			'title'=>$single,
			'singular'=>$single,
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info
		);
		$this->load->view('domains/view', $data);

	}


	/***************************
		products
	***************************/
	public function products(){
		$user_session = $this->session->all_userdata();

		$type = $this->uri->segment(3);
		$answer_id = $this->uri->segment(4);

		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);
		$q_products = $this->master->getRecords('q_products');

		$single = ($type == 'responses') ? 'Customer Response' : 'Product Insurance';
		$data = array(

			'view'=>($type == 'responses') ? 'responses_view' : 'products_view',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'q_products'=>$q_products,
			'title'=>$single.'s',
			'singular'=>$single,
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info
		);

		if($type == 'responses') {
			$data['responses'] = $this->common->responses_format($answer_id, array('broker_id'=>$id));

			if(!empty($answer_id)){
				$data['view'] = 'response_each_view';
			}
		}

		$data['countries'] = $this->common->country_format();

//		echo json_encode($data['responses']);
//		return false;
		$this->load->view('domains/view', $data);
	}


	/***************************
		billing
	***************************/
	public function billing(){
		$user_session = $this->session->all_userdata();
		$nowtime = NULL; //date('Y-m-d H:i:s');
		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);
		$customers = $this->common->customers($id, $nowtime);


		$single = 'Billing';
		$data = array(
			'view'=> 'billing_view',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'customers'=>$customers,
			'title'=>$single.'s',
			'singular'=>$single,
			'success'=>$success,
			'danger'=>$danger,
			'nowtime'=>$nowtime,
			'info'=>$info
		);

//		echo json_encode($customers);
//		return false;
		$this->load->view('domains/view', $data);
	}

	/***************************
		transactions
	***************************/
	public function transactions(){
		$user_session = $this->session->all_userdata();

		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);
		$transactions = $this->common->transactions($id);


		$single = 'Transaction History';
		$data = array(
			'view'=> 'transactions_view',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'transactions'=>$transactions,
			'title'=>$single,
			'singular'=>$single,
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info
		);

//		echo json_encode($customers);
//		return false;
		$this->load->view('domains/view', $data);
	}


	/***************************
		reporting
	***************************/
	public function reporting(){
		$user_session = $this->session->all_userdata();

		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);
		$transactions = $this->common->transactions($id);

		$report_header = $this->common->report_header();
		$report_icons = $this->common->report_icons();
		$report_templates = $this->master->getRecords('report_template', array('user_id'=>$id));


		$single = 'Reporting';
		$data = array(
			'view'=> 'reporting_view',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'transactions'=>$transactions,
			'title'=>$single,
			'singular'=>$single,
			'report_header'=>$report_header,
			'report_icons'=>$report_icons,
			'report_templates'=>$report_templates,
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info
		);

//		echo json_encode($customers);
//		return false;
		$this->load->view('domains/view', $data);
	}



	/***************************
		share
	***************************/
	public function share(){
		$user_session = $this->session->all_userdata();

		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);

		$prod_q_products = $this->master->getRecords('q_products');
		$prod_prod_id = (@unserialize($broker['privileges'])) ? unserialize($broker['privileges']) : array('3');


		$single = 'Quote Link';
		$data = array(
			'view'=> 'manage-share-link',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'prod_q_products'=>$prod_q_products,
			'prod_prod_id'=>$prod_prod_id,
			'title'=>$single,
			'singular'=>$single,
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info
		);

//		echo json_encode($customers);
//		return false;
		$this->load->view('domains/view', $data);
	}



	/***************************
		setup
	***************************/
	public function setup(){
		$user_session = $this->session->all_userdata();

		$type = $this->uri->segment(3);

		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);
		$user = $this->common->customer_format($id);

		$single = (!empty($type)) ? ucfirst(str_replace('_', ' ', $type)) : 'Profile';
		$view = (!empty($type)) ? $type.'_view' : 'profile_view';

		$data = array(

			'view'=>$view,
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'title'=>$single,
			'singular'=>$single,
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info
		);
		$data['countries'] = $this->common->country_format();

		if($type == 'quote_slip'){
			$insurances = $this->master->getRecords('bought_insurance');
			$admin_info = $this->master->getRecords('admin');

			$data['insurances'] = $insurances;
			$data['admin_info'] = $admin_info;


		}
		else if($type == 'payment_method'){
			$data['client_token'] = $this->common->braintree_clienttoken();
		}
		else if($type != 'logo' && $type != 'profile'){
			$base_url = $this->common->base_url();
			redirect($base_url.'panel/setup/profile');
		}

//		echo json_encode($user);
//		return false;
		$this->load->view('domains/view', $data);
	}


	/***************************
		message
	***************************/
	public function message(){
		$user_session = $this->session->all_userdata();

		//type == underwriter id
		$type = $this->uri->segment(3);
		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];
		$broker = $this->common->brokers($id);

		$base_link = $this->common->base_url();

		$broker_id = $user_session['id'];

		$claims = array(); //$this->master->getRecords('claims', array('id'=>$type));
		$brokerr = $this->common->customer_format($type);


//		$whr_first = array(
//			'customer_type'=>'N',
//			'agency_id'=>$claims[0]['agency_id'],
//		);
//		$first_under = $this->master->getRecords('customers', $whr_first, '*', array('first_underwriter'=>'ASC'));


		$underwriter = $this->common->customer_format($type);

		$message_idd = $id.$type;
		$messages = $this->master->getRecords('claims_message_file', array('message_id'=>$message_idd));

		$to = $type;

		$uniquetime= time().mt_rand();

		$data = array(
			'view'=>'ml-claims-message',
			'id'=>$id,
			'user'=>$broker,
			'user_sess'=>$user_session,
			'title'=>'Message',
			'to'=>$to,
			'message_id'=>$message_idd,
			'claims'=>$claims,
			'broker'=>$brokerr,
			'underwriter'=>$underwriter,
			'singular'=>'Claim',
			'uniquetime'=>$uniquetime,
			'messages'=>$messages,
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'base_link'=>$base_link
		);


//		echo json_encode($data);
//		return false;
		$this->load->view('domains/view', $data);


	}



	/***************************
		brokerage
	***************************/
	public function brokerage() {
		$id = $this->session->userdata('id');

		$user = $this->common->customer_format($id);


		$brokerage = $this->master->getRecords('brokerage', array('id'=>$user['brokerage_id']));

		$countries = $this->common->country_format();
		$single = 'Brokerage';
		$title = $single;


		$data = array(
			'view'=>'manage-broker-update',
			'title'=>$title,
			'singular_title'=>$single,
			'countries'=>$countries,
			'user'=>$user,
			'brokerage'=>$brokerage
		);

		//echo json_encode($data); return false;
		$this->load->view('domains/view', $data);


	}




	/***************************
		logout
	***************************/
	public function logout(){
		$user_data = $this->session->all_userdata();

		//logs
		$log_activity = array(
			'name'=>$user_data['name'].' logout from ebinder.',
			'type'=>'logout',
			'details'=>serialize($user_data)
		);
		$this->common->insert_activity_log($log_activity);

		//log user session time
		$arr = $this->common->user_session_time($user_data);

		//echo json_encode($arr); return false;

		foreach ($user_data as $key => $value) {
			//do not remove admin session
			if($key != 'logged_admin' && $key != 'logged_admin_email' && $key != 'logged_admin_id' && $key != 'logged_admin_brokerage'){
				$this->session->unset_userdata($key);
			}
		}
		redirect();
		// if($this->common->the_domain() == '' || $this->common->the_domain() == 'localhost'){
		// 	redirect('#login');
		// }
		// else{
		// 	redirect('http://ebinder.com.au');
		// }

	}


	public function testbrain(){

		$nonce = $this->common->braintree_clienttoken();
		echo $nonce;
	}


	public function password(){
		$user_session = $this->session->all_userdata();

		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$id = $user_session['id'];

		$base_link = $this->common->base_url();
		$data = array(
			'view'=>'social_email_password',
			'id'=>$id,
			'user_sess'=>$user_session,
			'title'=>'Change Password',
			'singular'=>'Change Password',
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info,
			'base_link'=>$base_link
		);


//		echo json_encode($mymembers);
//		return false;
		 $this->load->view('domains/view', $data);



	}


}
