<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {

	public function __construct(){
		parent::__construct();
	}

	/***************************
		profile
	***************************/
	public function profile(){
		if($this->session->userdata('id') == ''){
			redirect(base_url());
		}
		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');

		$agency = $this->master->getRecords('agency', array('id'=>$user_id));
		$customer = $this->master->getRecords('customers', array('id'=>$user_id));
		$student = $this->master->getRecords('students', array('id'=>$user_id));

		$countries = $this->master->getRecords('country_t');


		$data = array(
			'view'=>'settings_view',
			'title'=>'Settings',
			'countries'=>$countries
		);

		if($type == 'agency'){
			//get fields
			$the_fields = array();
			if(count($agency) > 0){
				foreach($agency[0] as $r=>$value){
					if($r == 'name' || $r == 'email'){
						$the_fields[] = $r;
					}
				}
			}

			$data['agency'] = $agency;
			$data['the_fields'] = $the_fields;
			$data['agency'] = $agency;



		}
		else if($type == 'customer'){

			//get fields
			$the_fields = array();
			if(count($customer) > 0){
				foreach($customer[0] as $r=>$value){
					if($r == 'first_name' || $r == 'last_name' || $r == 'email'){
						$the_fields[] = $r;
					}
				}
			}

			$data['customer'] = $customer;
			$data['the_fields'] = $the_fields;
			$data['view'] = 'settings_customer_view';
		}
		else if($type == 'student'){

			//get fields
			$the_fields = array();
			if(count($student) > 0){
				foreach($student[0] as $r=>$value){
					if($r == 'first_name' || $r == 'last_name' || $r == 'email'){
						$the_fields[] = $r;
					}
				}
			}

			$data['customer'] = $student;
			$data['the_fields'] = $the_fields;
			$data['view'] = 'settings_student_view';
		}

		$this->load->view('includes/user_view', $data);

	}

	/***************************
		logo
	***************************/
	public function logo(){
		if($this->session->userdata('id') == ''){
			redirect(base_url());
		}

		$data['view'] = 'settings_logo_view';
		$data['title'] = 'Update Logo';

		$this->load->view('includes/user_view', $data);
	}

	/***************************
		verify
	***************************/
	public function confirm_agency(){
		$id = $this->uri->segment(3);
		$md5email = $this->uri->segment(4);
		$agency = $this->master->getRecords('agency', array('id'=>$id));
		$countries = $this->master->getRecords('country_t');
		//get fields
		$the_fields = array();
		if(count($agency) > 0){
			foreach($agency[0] as $r=>$value){
				if($r == 'name' || $r == 'email'){
					$the_fields[] = $r;
				}
			}
		}


		$data = array(
			'view'=>'agency_verify_view',
			'title'=>'Verify',
			'agency'=>$agency,
			'the_fields'=>$the_fields,
			'countries'=>$countries
		);
		$this->load->view('includes/view', $data);


	}


	/***************************
		verify
	***************************/
	public function verify(){
		$user = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$cryptt = $this->uri->segment(5);

		$customer = $this->master->getRecords('customers', array('id'=>$id));
		$countries = $this->master->getRecords('country_t');

		//get fields
		$the_fields = array();
		if(count($customer) > 0){
			foreach($customer[0] as $r=>$value){
				if($r == 'first_name' || $r == 'last_name' || $r == 'email'){
					$the_fields[] = $r;
				}
			}
		}


		$data = array(
			'view'=>'customer_verify_view',
			'title'=>'Verify',
			'customer'=>$customer,
			'the_fields'=>$the_fields,
			'countries'=>$countries
		);
		$this->load->view('includes/view', $data);

	}

	/***************************
		show_country_code
	***************************/
	public function show_country_code() {
		$counrty_code = $_POST['country_short'];

		$mobile_code = $this->master->getRecords('country_t',array('iso2'=>$counrty_code));

		$data['calling_code'] = '';
		$data['country_id'] = '';
		$data['result'] = 'empty';

		if(count($mobile_code) > 0){
			$data['calling_code'] = $mobile_code[0]['calling_code'];
			$data['country_id'] = $mobile_code[0]['country_id'];
			$data['result'] = 'success';
		}

		echo json_encode($data);

	}


	/***************************
		policydoc
	***************************/
	public function policydoc() {
		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');
		$output_dir = "uploads/policyd/";

		$policydoc = (isset($_POST['policydoc'])) ? $_POST['policydoc'] : '';

		if(isset($_FILES["myfile"])) {
			//Filter the file types , if you want.
			if ($_FILES["myfile"]["error"] > 0) {
			  echo 'error';
			}
			else {
				$file_name = $_FILES["myfile"]["name"];
				$file_name = substr($file_name, -3);

				if($file_name != 'pdf'){
					echo 'not_img';
					return false;
				}

				$thefilename = uniqid().str_replace(' ', '_', $_FILES["myfile"]["name"]);

				//move the uploaded file to uploads folder;
				move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$thefilename); //$_FILES["myfile"]["name"]);

				$arr = array (
					'name'=>$thefilename,
				);

				if(empty($policydoc)){
					$doc_id = $this->master->insertRecord('policy_docs',$arr, true);
				}
				else{
					$doc_id = $policydoc;
					$this->master->updateRecord('policy_docs',$arr, array('id'=>$doc_id));
				}


				$typee = 'policy_doc';
				$save_agency_ratings_log = $this->common->save_agency_ratings_log($typee);


				echo $thefilename.'__'.$doc_id;


			}
		}
	}

	/***************************
		policy_files
	***************************/
	public function policy_files() {
		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');
		$output_dir = "uploads/policyd/";
		if(isset($_FILES["myfile"])) {
			//Filter the file types , if you want.
			if ($_FILES["myfile"]["error"] > 0) {
			  echo 'error';
			}
			else {
				$ff = $_FILES["myfile"]["name"];
				$file_name = $_FILES["myfile"]["name"];
				$file_name = substr($file_name, -3);

				if($file_name == 'exe'){
					echo 'not_img';
					return false;
				}

				$thefilename = uniqid().str_replace(' ', '_', $_FILES["myfile"]["name"]);

				//move the uploaded file to uploads folder;
				move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$thefilename); //$_FILES["myfile"]["name"]);

				$img_type = $_POST['img_type'];
				$arr = array (
					'name'=>$thefilename,
					'product_id'=>$img_type,
					'broker_id'=>$user_id
				);

				$file_id = $this->master->insertRecord('policy_docs',$arr, true);
				$arr['id'] = $file_id;
				$arr['file_name'] = $ff;
				echo json_encode($arr);

			}
		}

	}
	/***************************
		update_avatar
	***************************/
	public function update_avatar() {
		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');
		$output_dir = "uploads/avatars/";

		$agency_id = (isset($_POST['agency_id'])) ? $_POST['agency_id'] : '';
		$pageid = (isset($_POST['pageid'])) ? $_POST['pageid'] : '';
		$img_type = (isset($_POST['img_type'])) ? $_POST['img_type'] : '';

		if(isset($_FILES["myfile"])) {
			//Filter the file types , if you want.
			if ($_FILES["myfile"]["error"] > 0) {
			  echo 'error';
			}
			else {
				$file_name = $_FILES["myfile"]["name"];
				$file_name2 = substr($file_name, -4);
				$file_name = substr($file_name, -3);

				if($file_name != 'jpg' && $file_name != 'gif' && $file_name != 'png' && $file_name != 'JPG' && $file_name != 'GIF' && $file_name != 'PNG' && $file_name2 != 'jpeg' && $file_name2 != 'JPEG'){
					echo 'not_img';
					return false;
				}

				$thefilename = uniqid().str_replace(' ', '_', $_FILES["myfile"]["name"]);

				//move the uploaded file to uploads folder;
				move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$thefilename);


				if($img_type != ''){

					if($pageid != ''){
						$arr = array (
							'image'=>$thefilename
						);
						$this->master->updateRecord('admin_contents',$arr, array('id'=>$pageid));
					}

				}

				echo $output_dir.$thefilename;


			}

		}

	}


	/***************************
		save_profile_pic
	***************************/
	public function save_profile_pic() {
		$img_url = $_POST['img_url'];
		$img_name = $_POST['img_name'];
		$img_type = $_POST['img_type'];
		$page_type = $_POST['page_type'];
		$page_id = $_POST['page_id'];
		$agency_id = $_POST['agency_id'];

		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');

		list($type, $img_url) = explode(';', $img_url);
		list(, $img_url)      = explode(',', $img_url);
		$data = base64_decode($img_url);


		if($page_type == 'avatar'){
			$img_dr = $img_name;
			$img = explode('/', $img_name);
			$this->master->updateRecord('customers', array('avatar'=>$img[2]), array('id'=>$user_id));


			//$this->master->updateRecord('admin_contents', array('image'=>$img_name), array('id'=>$page_id));

		}

		else {
			$img_dr = $img_name;
			$img = explode('/', $img_name);

			if(!empty($agency_id)){
				$this->master->updateRecord('agency', array('avatar'=>$img[2]), array('id'=>$agency_id));
			}

			// $img_dr = 'uploads/avatars/customer'.$user_id.'.png';
			// $this->master->updateRecord('customers', array('logo_added'=>'Y'), array('id'=>$user_id));
		}

		file_put_contents($img_dr, $data);

		$arr = array(
			//'a'=>$img_url,
			'image'=>$img_name //$this->common->avatar($user_id, $type)
		);
		echo json_encode($arr);

	}

	/***************************
		add_stripe_customer
	***************************/
	public function add_stripe_customer(){

		$user_id = $this->session->userdata('id');
		$stripe_id = $this->session->userdata('stripe_id');
		$email = $this->session->userdata('email');
		$token = $_POST['token'];
		$from_buy = $_POST['from_buy'];

		$buy_inputs = $this->session->userdata('buy_inputs');
		$single_input = $this->session->userdata('single'); //array();
		$consignee = $this->session->userdata('consignee'); //array();

		$currency = $this->session->userdata('currency');
		$premium = $this->session->userdata('premium'); //round($_POST['premium'], 2);
		$premium = round($premium, 2);
		$single_email = $_POST['email'];

		$card = $_POST['card'];
		$nowtime = date('Y-m-d H:i:s');

		$referral_id = $this->session->userdata('referral_id');


		//get validation message from admin
		$action_message = $this->common->get_message('customer_add_card');
		$message = $action_message['error'];
		$result = 'error';

		Stripe::setApiKey("sk_test_21OnMrInOQSqxd6aSeAO3Nu0"); //sk_test_UTHtN8EB5Y5xwTB1OkeS6lJ8"); //sk_test_21OnMrInOQSqxd6aSeAO3Nu0");

		try {

			if($from_buy == 'no'){

				// Create a Customer
				$customer = Stripe_Customer::create(array(
				  "description" => "transit_insurance_customer",
				  "email" => $this->session->userdata('email'),
				  "card" => $token // obtained with Stripe.js
				));

				$arr = array(
					'stripe_id'=>$customer->id
				);

				$this->master->updateRecord('customers', $arr, array('id'=>$user_id));
				$this->session->set_userdata($arr);
				$message = $action_message['success'];
				$result = 'ok';

			}//not from buy form
			else{
				//stripe charge data
				$charge_data = array(
					"amount" => $premium*100, // amount in cents, again
					"currency" => strtolower($currency), // "usd",
					//"customer" => $customer_info[0]['stripe_id'],
					"description" => 'bought Single Marine Transit Policy'
				);

				//check if logged in
				if($user_id != '' && $stripe_id != ''){
					$customer_info = $this->master->getRecords('customers', array('id'=>$user_id));
					$single_email = $email;
					$charge_data['customer'] = $customer_info[0]['stripe_id'];
				}
				else{
					$charge_data['card'] = $token;
					if($user_id == ''){
						$user_id = 0;
					}
				}

				//check email if empty
				if($single_email == ''){
					$single_email = $email;
				}

				//charge card directly
				$charge = Stripe_Charge::create($charge_data);

				$details = array(
					'buy_inputs'=>$buy_inputs,
					'single_input'=>$single_input,
					'consignee'=>$consignee,
					'premium'=>$premium
				);

				$data = array(
					'customer_id'=>$user_id,
					'details'=>serialize($details),
					'status'=>'P',
					'email'=>$single_email
				);


				$sess_remove = array(
					'buy_inputs'=>'',
					'single_input'=>'',
					'consignee'=>'',
					'premium'=>'',
					'premium_format'=>'',
					'selected_deductible'=> '',
					'cargo_price'=>'',
					'currency'=>''
				);

				//unset from session
				foreach ($sess_remove as $key => $value) {
					$this->session->unset_userdata($key);
				}

				//check charge status
				if($charge->status == 'succeeded'){
					$data['date_purchased'] = $nowtime;
					$data['stripe_charge_id'] = $charge->id;
				}

				//check if referred
				if($referral_id == ''){
					$data['date_added'] = $nowtime;
					$booking_id = $this->master->insertRecord('bought_insurance', $data, true);
				}
				else{
					$booking_id = $referral_id;
					$this->master->updateRecord('bought_insurance', $data, array('id'=>$booking_id));

					$remove_sess = array(
						'referral_id'=>'',
						'referral_comment'=>''
					);
					$this->session->set_userdata($remove_sess);
				}
				//admin emailer info
				$adminemail = $this->common->admin_email();

				//email settings for agency
				$info_arr = array(
					'to'=>$this->common->recovery_admin(),//$adminemail,
					'from'=>$adminemail,
					'subject'=>'Welcome to Transit Insurance',
					'view'=>'payment-notification-to-admin',
					'emailer_file_name'=>'payment-notification-to-admin',
				);


				$other_info = array(
					'password'=>'',
					'view'=>'payment-notification-to-admin',
					'emailer_file_name'=>'payment-notification-to-admin',
					'name'=>'',//$first.' '.$last,
					'agency_name'=>'Admin',
					'user_name'=>$adminemail,
					'user_email'=>$adminemail,
					'link'=>'webmanager/insurance'
				);
				$data['info_arr1'] = $info_arr;
				$data['other_info1'] = $other_info;
				$this->emailer->sendmail($info_arr,$other_info);

				//email settings for customer
				$info_arr = array(
					'to'=>$single_email,
					'from'=>$adminemail,
					'subject'=>'Welcome to Transit Insurance',
					'view'=>'payment-notification-to-customer',
					'emailer_file_name'=>'payment-notification-to-customer',
				);

				$other_info = array(
					'password'=>'',
					'view'=>'payment-notification-to-customer',
					'emailer_file_name'=>'payment-notification-to-customer',
					'name'=>'',//$first.' '.$last,
					'agency_name'=>'', //$agency_info[0]['name'],
					'user_name'=>$single_email,
					'user_email'=>$single_email,
					'link'=>'dashboard' //landing/download/'.$user_id.'/'.$booking_id.'/'.md5($user_id)
				);

				if($user_id == '0'){
					//$other_info['link'] = 'landing/reqdetails/'.$booking_id.'/'.md5($email);
				}


				$data['info_arr2'] = $info_arr;
				$data['other_info2'] = $other_info;
				$this->emailer->sendmail($info_arr,$other_info);

				$arr['data'] = $data;

				//get validation message from admin
				$action_message = $this->common->get_message('buy_insurance');
				$message = $action_message['success'];
				$result = 'ok';
			}
		}
		catch(Stripe_CardError $e){

			$body = $e->getJsonBody();
			$err  = $body['error'];

			$message = $err['message'];

		}
		catch (Stripe_InvalidRequestError $e) {
		  // Invalid parameters were supplied to Stripe's API

			$body = $e->getJsonBody();
			$err  = $body['error'];

			$message = $err['message'];
		}
		catch (Stripe_AuthenticationError $e) {
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)

			$body = $e->getJsonBody();
			$err  = $body['error'];

			$message = $err['message'];
		}
		catch (Stripe_ApiConnectionError $e) {
		  // Network communication with Stripe failed

			$body = $e->getJsonBody();
			$err  = $body['error'];

			$message = $err['message'];
		}
		catch (Stripe_Error $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email

			$body = $e->getJsonBody();
			$err  = $body['error'];

			$message = $err['message'];
		}
		catch (Exception $e) {
		  // Something else happened, completely unrelated to Stripe

			$body = $e->getJsonBody();
			$err  = $body['error'];

			$message = $err['message'];
		}

		$arr['result'] = $result;
		$arr['message'] = $message;
		echo json_encode($arr);
	}


	public function save_quote(){
		$user_id = $this->session->userdata('id');
		$single = $this->session->userdata('single');
		$nowtime = date('Y-m-d H:i:s');

		//get session data
		$details = array(
			'buy_inputs'=>$this->session->userdata('buy_inputs'),
			'single_input'=>$single,
			'consignee'=>$this->session->userdata('consignee'),
			'premium'=>$this->session->userdata('premium')
		);

		$data = array(
			'customer_id'=>$user_id,
			'details'=>serialize($details),
			'status'=>'S',
			'email'=>$single['email']
		);

		$data['date_added'] = $nowtime;
		$booking_id = $this->master->insertRecord('bought_quote', $data, true);


		//admin emailer info
		$adminemail = $this->common->admin_email();

		//email settings for customer
		$info_arr = array(
			'to'=>$single['email'],
			'from'=>$adminemail,
			'subject'=>'Welcome to Transit Insurance',
			'view'=>'quote-send-to-consignor',
			'emailer_file_name'=>'quote-send-to-consignor',
		);

		$other_info = array(
			'password'=>'',
			'view'=>'quote-send-to-consignor',
			'emailer_file_name'=>'quote-send-to-consignor',
			'name'=>$single['first_name'].' '.$single['last_name'],
			'agency_name'=>'', //$agency_info[0]['name'],
			'user_name'=>$single['email'],
			'user_email'=>$single['email'],
			'link'=>'landing/confirm_quote/'.$user_id.'/'.$booking_id.'/'.md5($user_id)
		);


		$data['info_arr2'] = $info_arr;
		$data['other_info2'] = $other_info;
		$this->emailer->sendmail($info_arr,$other_info);


		$arr = array(
			'buy_inputs',
			'premium',
			'premium_format',
			'selected_deductible',
			'cargo_price',
			'currency',
			'single',
			'consignee'
		);

		foreach ($arr as $key) {
			$this->session->unset_userdata($key);
		}


		header('Content-Type: application/json');
		header('Access-Control-Allow-Origin: *');

		echo json_encode($data, JSON_PRETTY_PRINT); // $single['email'];
	}


	public function check_student_card(){
		$user_id = $this->session->userdata('id');
		$type = $this->session->userdata('type');

		$user_info = $this->master->getRecords('students', array('id'=>$user_id));
		$status = 'error';
		if(count($user_info) > 0){
			if($user_info[0]['stripe_id'] != ''){
				$status = 'ok';
			}
		}
		echo $status;

	}


	public function social_login(){
		// $account = $this->session->userdata('domain');
		// $base_link = ($account != 'ebinder.com.au' && $account != 'localhost') ? 'https://'.$account.'.ebinder.com.au/' : base_url();
		// $this->config->set_item('base_url',$base_link) ;

		$user_session = $this->session->all_userdata();
		$id = $user_session['id'];
		$user = $this->common->customer_format($id);

		if(empty($id)){
			redirect();
		}

		$success = $this->session->flashdata('success');
		$danger = $this->session->flashdata('danger');
		$info = $this->session->flashdata('info');

		$data = array(
			'view'=>'social_login',
			'id'=>$id,
			'user_sess'=>$user,
			'title'=>'Social Login',
			'singular'=>'Social Login',
			'success'=>$success,
			'danger'=>$danger,
			'info'=>$info
		);


//		echo json_encode($mymembers);
//		return false;

		if(isset($user_session['logged_admin_id'])){

			$data['middle_content'] = $data['view'];
			$this->load->view('admin/admin-view',$data);
		}
		else{
			$this->load->view('domains/view', $data);

		}



		// $this->load->view('frontpage/header_view',$data);
		// $this->load->view('frontpage/social', $data);
		// $this->load->view('frontpage/footer_view');
		// echo $this->common->the_domain();



	}




}
