<?php

class Common_model extends CI_Model
{

	public function __construct() {

		parent::__construct();

	}


	public function sess_agency_id(){
		return $this->session->userdata('logged_admin_agency');
	}

	public function days_of_week(){
		$arr = array(
			'Sunday',
			'Monday',
			'Tuesday',
			'Wednesday',
			'Thursday',
			'Friday',
			'Saturday'
		);

		return $arr;

	}

public function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
		// Calculate the distance in degrees
		$point1_lat = floatval($point1_lat);
		$point1_long = floatval($point1_long);
		$point2_lat = floatval($point2_lat);
		$point2_long = floatval($point2_long);

		$degrees = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));

		// Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
		switch($unit) {
			case 'km':
				$distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
				break;
			case 'mi':
				$distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
				break;
			case 'nmi':
				$distance =  $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
		}
		return round($distance, $decimals);
	}


	public function get_message($name = NULL){
		$message['id'] = '';
		$message['name'] = $name;
		$message['success'] = '<i class="fa fa-check-circle text-success"></i> Action successful.';
		$message['error'] = '<i class="fa fa-warning text-info"></i> Action failed to execute.';
		$message['info'] = '<i class="fa fa-info-circle"></i> No issue found.';
		$message['confirm'] = '<i class="fa fa-question-circle text-info"></i> Confirm action.';

		if($name != NULL){
			$mess = $this->master->getRecords('validation_messages', array('name'=>$name));

			if(count($mess) > 0){
				$message['id'] = $mess[0]['id'];
				$message['success'] = '<i class="fa fa-check-circle text-success"></i> '.$mess[0]['success_message'];

				if($mess[0]['error_message'] != ''){
					$message['error'] = '<i class="fa fa-warning  text-warning"></i> '.$mess[0]['error_message'];
				}

				if($mess[0]['info_message'] != ''){
					$message['info'] = '<i class="fa fa-info-circle  text-info"></i> '.$mess[0]['info_message'];
				}

				if($mess[0]['confirm_message'] != ''){
					$message['confirm'] = '<i class="fa fa-question-circle text-info"></i> '.$mess[0]['confirm_message'];
				}
			}

			return $message;

		}
		else{
			$messages = array();
			$mess = $this->master->getRecords('validation_messages');
			$error = array();
			$success = array();
			$gloval = array();
			foreach($mess as $r=>$value){
				$confirm_fa = '<i class="fa fa-question-circle text-info"></i> ';
				$success_fa = '<i class="fa fa-check-circle text-success"></i> ';
				$err_fa = '<i class="fa fa-info-circle text-info"></i> ';
				$gloval[$value['name']] = $confirm_fa.$value['confirm_message'];
				$error[$value['name']] = $err_fa.$value['error_message'];
				$success[$value['name']] = $success_fa.$value['success_message'];
			}
			$messages['success'] = $success;
			$messages['error'] = $error;
			$messages['global'] = $gloval;
			return $messages;

		}
	}


	public function colors(){
		$arr = array(
			'bg-choco',
			'bg-lavander',
			'bg-maroon',
			'bg-deep',
			'bg-grass',
			'bg-blood'
		);

		return $arr;

	}

	public function agency_logo($id){
		$img = base_url().'assets/procurify/images/logo/ebinder-logo.png'; //logo-horizontal-64.7a505e99594f.png';

		$agency = $this->master->getRecords('agency', array('id'=>$id));

		if(count($agency) > 0){
			if($agency[0]['avatar'] != '55a87f06c64d8Screenshot_2015-06-04_23.20.47.png'){
				$img = base_url().'uploads/avatars/'.$agency[0]['avatar'];
			}
		}
		return $img;

	}

	public function logo($id){
		$img = base_url().'assets/procurify/images/logo/ebinder-logo.png'; //logo-horizontal-64.7a505e99594f.png';

		$customer = $this->master->getRecords('customers', array('id'=>$id));



		if(count($customer) > 0){
			//logo is by agency
			$agency = $this->master->getRecords('agency', array('id'=>$customer[0]['agency_id']));

			if(count($agency) > 0){
				if($agency[0]['avatar'] != '55a87f06c64d8Screenshot_2015-06-04_23.20.47.png'){
					$img = base_url().'uploads/avatars/'.$agency[0]['avatar'];
				}
			}

				// if($customer[0]['customer_type'] == 'N'){
				// 	if($customer[0]['logo_added'] != 'N'){
				// 		$img = base_url().'uploads/avatars/customer'.$id.'.png';
				// 	}
				// }
				// else{
				// 	$type = $this->db_field_id('customers', 'logo_added', $customer[0]['studio_id']);
				// 	if($type == 'Y'){
				// 		$img = base_url().'uploads/avatars/customer'.$customer[0]['studio_id'].'.png';
				// 	}
				// }
		}


		return $img;
	}

	public function brokerage($brokerage_id = ''){
		$brokerage = $this->master->getRecords('brokerage', array('id'=>$brokerage_id));

		$name = 'Unassigned';
		if(count($brokerage) > 0){
			$name = $brokerage[0]['company_name'];
		}

		return $name;
	}

	public function avatar($id, $type = 'agency'){

		if($type == 'agency'){
			$info = $this->master->getRecords('agency',array('id'=>$id));
			$default = base_url().'uploads/avatars/agency.jpg';
		}

		else if($type == 'customer'){
			$info = $this->master->getRecords('customers',array('id'=>$id));
			$default = base_url().'uploads/avatars/download.jpg';
		}
		else{
			$info = $this->master->getRecords('students',array('id'=>$id));
			$default = base_url().'uploads/avatars/download.jpg';
		}

		if(count($info) > 0){

			if($info[0]['avatar'] != ''){ //check if not default

				$default = base_url().'uploads/avatars/'.$info[0]['avatar'];

			}

		}// not existing user

		return $default;

	}

	public function class_name($id){
		$class = $this->master->getRecords('classes', array('id'=>$id));
		$name = 'Not specified';
		if(count($class) > 0){
			$name = $class[0]['name'];
		}
		return $name;
	}

	public function genre_name($id){
		$genre = $this->master->getRecords('genres', array('id'=>$id));
		$name = 'Not specified';
		if(count($genre) > 0){
			$name = $genre[0]['name'];
		}
		return $name;
	}

	public function agency_name($id){
		$agency = $this->master->getRecords('agency', array('id'=>$id));
		$name = 'Not specified';
		if(count($agency) > 0){
			$name = $agency[0]['name'];
		}
		return $name;
	}

	public function customer_name($id){
		$customer = $this->master->getRecords('customers', array('id'=>$id));
		$name = 'Not exist';
		if(count($customer) > 0){
			$name = $customer[0]['first_name']. ' '. $customer[0]['last_name'];
		}
		return $name;
	}

	public function responses_format($id = NULL, $filter = NULL){

		if(!empty($id)){
			$data = $this->master->getRecords('q_answers', array('id'=>$id));
		}
		else{
			if(!empty($filter)){
				$data = $this->master->getRecords('q_answers', $filter);
			}
			else{
				$data = $this->master->getRecords('q_answers');
			}
		}

		$arr = array();

		if(count($data) > 0){
			foreach($data as $r=>$value){
				$value['answer'] = unserialize($value['answer']);
				$user_info = unserialize($value['user_info']);
				$value['answer_arr'] = (@unserialize($value['answer_arr'])) ? unserialize($value['answer_arr']) : array();

				$prod_text = '';
				$prod_arr = array();
				$pcount = 1;
				foreach($user_info['product'] as $r=>$pval) {
				    $cat_answer = array();
				    $pval_text = '';

					if($pval != '5'){
						if(!is_numeric($pval)) {
							 if($pval != 'cyber_iability'){
								  $prod_text .= ucwords(str_replace('_', ' ', $pval));
							  } else{
								  $prod_text .= 'Cyber Liability';
							  }


							  //categorize the answers
							  $pval_title = ($pval != 'cyber_iability') ? ucwords(str_replace('_', ' ', $pval)) : 'Cyber Liability';
							  $pval_text = explode('_', $pval);




						}
						else{
							if($pval == '3'){
								$prod_text .= 'Contents and Building';
							}
							else{
								$prod_text .= $this->db_field_id('q_products', 'product_name', $pval);
							}

							  //categorize the answers
							  $pval_title = ($pval == '3') ? 'Contents and Building' : $this->db_field_id('q_products', 'product_name', $pval);

							  $pval_text = array($pval.'_');

						}



						  //filter answers
						  if(count($value['answer_arr']) > 0){

							  foreach($value['answer_arr'] as $aa=>$aav){
								  if(strpos($aav['name'], $pval_text[0]) === 0){ //0 === strpos($aav['name'], $pval_text)){
									  $cat_answer[] = $aav;

								  }

							  }
						  }


						//categorize answers
						$prod_arr[] = array(
							'title'=>$pval_title,
							'pval_text'=>$pval_text,
							'answers'=>$cat_answer
						);

					}


					if($pcount < count($user_info['product'])){
						$prod_text .= ', ';
					}
					$pcount++;


				}



				$user_info['prod_text'] = $prod_text;
				$value['user_info'] = $user_info;
				$value['prod_arr'] = $prod_arr;


				$value['status_text'] = ($value['sent'] == 'Y') ? 'Sent to Insurers/Underwriters' : 'Pending';

				if(count($data) == 1){
					$arr = $value;
				}
				else{
					$arr[] = $value;
				}

			}
		}

		return $arr;
	}

	public function clone_agency($data){
		$nowtime = date('Y-m-d H:i:s');
		//create agency table entry for ratings
		$admin_info = $this->master->getRecords('admin');
		$agency_id = $admin_info[0]['default_cargo'];
		$agency_info = $this->master->getRecords('agency', array('id'=>$agency_id));

		$toclone = $agency_info[0];
		unset($toclone['id']);



		//$toclone['brokerage_id'] = $brokerage_id;
		$toclone['name'] = $data['name'];
		$toclone['background'] = (isset($data['background'])) ? $data['background'] : '';
		$toclone['agency_details'] = (isset($data['agency_details'])) ? $data['agency_details'] : serialize(array());
		$toclone['domain'] = $data['domain'];
		$toclone['contact_no'] = $data['contact_no'];
		$toclone['address'] = (isset($data['address'])) ? $data['address'] : '';
		$toclone['lat'] = (isset($data['lat'])) ? $data['lat'] : '';
		$toclone['lng'] = (isset($data['lng'])) ? $data['lng'] : '';
		$toclone['email'] = (isset($data['email'])) ? $data['email'] : '';
		$toclone['product_id'] = (isset($data['product_id'])) ? $data['product_id'] : '0';
		$toclone['user_type'] = (isset($data['user_type'])) ? $data['user_type'] : '0';
		$toclone['password'] = '';
		$toclone['date_added'] = $nowtime;
		$agency_id2 = $this->master->insertRecord('agency', $toclone, true);



//		$deductibles = $this->master->getRecords('deductibles', array('agency_id'=>$agency_id));
//		$toclone2 = $deductibles[0];
//		unset($toclone2['id']);
//
//		$toclone2['agency_id'] = $agency_id2;
//		$toclone2['date_added'] = $nowtime;
//		$deductibles = $this->master->insertRecord('deductibles', $toclone2, true);


		return $agency_id2;
	}

	public function broker_agencies($email){
		$brokers = $this->master->getRecords('customers', array('email'=>$email, 'customer_type'=>'N', 'super_admin'=> 'N', 'agency_id !='=>'0'));
		$agencies = array();
		if(count($brokers) > 0){
			foreach($brokers as $r=>$value){
				//filter active agency
				$active_agency = $this->master->getRecords('agency', array('id'=>$value['agency_id'], 'enabled'=>'Y'));
				if(!empty($active_agency)){
					$agency = array(
						'id'=>$value['agency_id'],
						'name'=>$this->db_field_id('agency', 'name', $value['agency_id']),
						'domain'=>$this->db_field_id('agency', 'domain', $value['agency_id']),
					);
					$agencies[] = $agency;
				}
			}
		}

		return $agencies;
	}

	public function customer_format($id = NULL, $filter = NULL){
		if(!empty($id)){
			$customer = $this->master->getRecords('customers', array('id'=>$id), '*', array('id'=>'DESC'));
		}
		else{
			if(!empty($filter)){
				$customer = $this->master->getRecords('customers', $filter, '*', array('id'=>'DESC'));
			}
			else{
				$customer = $this->master->getRecords('customers','', '*', array('id'=>'DESC'));
			}
		}

		$arr = array();

		if(count($customer) > 0){
			foreach($customer as $r=>$value){
				unset($value['password']);
				$r_add = $_SERVER['REMOTE_ADDR'];
				$value['ip'] = $r_add;

				$value['name'] = $value['first_name'].' '.$value['last_name'];
				$value['type'] = 'customer';
				$value['img'] = $this->common->avatar($value['id'], 'customer');
				$value['status_text'] = ($value['status'] == 'Y') ? 'Verified' : 'Not Verified';
				$value['enabled_text'] = ($value['enabled'] == 'Y') ? 'Yes' : 'No';
				$value['super_admin_text'] = ($value['super_admin'] == 'Y') ? 'Super Admin' : 'Regular User';
				$value['privileges_text'] = ($value['enabled'] == 'Y') ? 'Yes' : 'No';
				$value['access_rights'] = (@unserialize($value['access_rights'])) ? unserialize($value['access_rights']) : $this->access_rights();
				$value['customer_info_format'] = (@unserialize($value['customer_info'])) ? unserialize($value['customer_info']) : array();


				$brokerage = $this->master->getRecords('brokerage', array('id'=>$value['brokerage_id']));
				$value['brokerage'] = (empty($brokerage)) ? array() : $brokerage[0];

				$agency = $this->format_agency(array('id'=>$value['agency_id']));
				$value['agency'] = (empty($agency)) ? array() : $agency[0];
				$value['position'] = $value['domain'];
				$value['domain'] = (empty($agency)) ? 'demo' : $agency[0]['domain'];
				$value['my_agencies'] = $this->broker_agencies($value['email']);

				//set the company
				//$value['domain'] = ($value['studio_id'] == '0') ? $value['domain'] : $this->db_field_id('customers', 'domain', $value['studio_id']);

				//get logo
				$usertype = ($value['customer_type'] != 'N') ? $value['id'] : $value['studio_id'];
				$value['logo'] = $this->common->logo($usertype);
				$value['address'] = $value['location'];
				//$value['info_format'] = (@unserialize($value['customer_info'])) ? unserialize($value['customer_info']) : array();
				$resp_filter = array(
					'customer_id'=>$value['id'],
					'broker_id'=>$value['studio_id']
				);
				//$the_response = $this->common->responses_format(NULL, $resp_filter);
				//$value['response'] = $the_response;

				$value['logged_admin_agency'] = $value['agency_id'];
				//check if underwriter
				if($value['super_admin'] == 'Y'){
					//$value = array();
					$value['logged_admin'] = $value['name'];
					$value['logged_admin_email'] = $value['email'];
					$value['logged_admin_brokerage'] = $value['brokerage_id'];
					$value['logged_admin_id'] = $value['id'];
				}


				if(count($customer) == 1){
					$arr = $value;
				}
				else{
					$arr[] = $value;
				}
			}
		}

		return $arr;
	}

	public function customers_tab_menu($customer_details){
		$cd_id = '';
		if(isset($customer_details['id'])){
			if($customer_details['id'] != ''){
				$cd_id = $customer_details['id'];
			}
		}

		//blank all data for each item
		if(!empty($cd_id)){
			$customer_details = array();
		}



    $themenu = array(
      array(
        'name'=>'Organisation',
        'data'=>(isset($customer_details['company_name'])) ? $customer_details['company_name'] : '',
      ),
      array(
        'name'=>'Business Activities',
        'data'=>(isset($customer_details['occupation_id'])) ? $customer_details['occupation_id'] : '',
      ),
      array(
        'name'=>'Financial Details',
        'data'=>(isset($customer_details['annaul_revenue'])) ? $customer_details['annaul_revenue'] : '',
      ),
      array(
        'name'=>'Employees &amp; Payroll',
        'data'=>(isset($customer_details['employee_number'])) ? $customer_details['employee_number'] : '',
      ),
      array(
        'name'=>'Notes',
        'data'=>(isset($customer_details['notes'])) ? $customer_details['notes'] : '',
      )
    );

    $themenu2 = array(
      array(
        'name'=>'Organisation',
        'data'=>(isset($customer_details['company_name'])) ? $customer_details['company_name'] : '',
      ),
      array(
        'name'=>'Business Activities',
        'data'=>(isset($customer_details['occupation_id'])) ? $customer_details['occupation_id'] : '',
      ),
      array(
        'name'=>'Financial Details',
        'data'=>(isset($customer_details['annaul_revenue'])) ? $customer_details['annaul_revenue'] : '',
      ),
      array(
        'name'=>'Employees &amp; Payroll',
        'data'=>(isset($customer_details['employee_number'])) ? $customer_details['employee_number'] : '',
      ),
      array(
        'name'=>'Claims',
        'data'=>(isset($customer_details['notes'])) ? $customer_details['notes'] : '',
      ),
      array(
        'name'=>'Quotes &amp; Policies',
        'data'=>(isset($customer_details['notes'])) ? $customer_details['notes'] : '',
      ),
      array(
        'name'=>'Notes',
        'data'=>(isset($customer_details['notes'])) ? $customer_details['notes'] : '',
      )
    );

		$arr = $themenu;
		if(!empty($cd_id)){
			$arr = $themenu2;
		}

		return $arr;
	}


	public function save_customer_from_quote(){
		//biz_quote_id
		$brokerage_id = $this->session->userdata('brokerage_id');
		$biz_quote_id = $this->session->userdata('biz_quote_id');
		$homebiz_response = $this->common->homebiz_response();
		$needs_referral = (!empty($homebiz_response['homebiz6'])) ? 'N' : 'Y';


		$arr = array(
			'needs_referral'=>$needs_referral,
			'content'=>serialize($homebiz_response)
		);

		//save customer changes details
		$the_customer = (isset($homebiz_response['homebiz1']['1_customer_id'])) ? $homebiz_response['homebiz1']['1_customer_id'] : '';
		if(!empty($the_customer)){
			$customer = $this->common->customer_format($the_customer); //brokerage_brokers($brokerage_id);
			$customer_info = (@unserialize($customer['customer_info'])) ? unserialize($customer['customer_info']) : array();
			$customer_info['annaul_revenue'] = (isset($homebiz_response['homebiz1']['1_total_annual_revenue'])) ? $homebiz_response['homebiz1']['1_total_annual_revenue'] : '';
			$customer_info['occupation_id'] = (isset($homebiz_response['homebiz1']['occupation_id'])) ? $homebiz_response['homebiz1']['occupation_id'] : '';
			$customer_info['employee_number'] = (isset($homebiz_response['homebiz1']['1_please_state_the_total_number_of_employees_including_self'])) ? $homebiz_response['homebiz1']['1_please_state_the_total_number_of_employees_including_self'] : '';
			$customer_info['breakdown_by_state'] = (isset($homebiz_response['homebiz1']['1_in_respect_of_income_earned_for_the_current_financial_year_please_provide_a_breakdown_by_state'])) ? $homebiz_response['homebiz1']['1_in_respect_of_income_earned_for_the_current_financial_year_please_provide_a_breakdown_by_state'] : array();

			$upd_customer = array(
				'customer_info'=>serialize($customer_info)
			);
			$this->master->updateRecord('customers', $upd_customer, array('id'=>$the_customer));
		}

		return true;
	}

	public function completed_details($quote_options = 'Bind'){
		//biz_quote_id
		$brokerage_id = $this->session->userdata('brokerage_id');
		$biz_quote_id = $this->session->userdata('biz_quote_id');
		$homebiz_response = $this->common->homebiz_response();
		$needs_referral = (!empty($homebiz_response['homebiz6'])) ? 'N' : 'Y';


		$arr = array(
			'needs_referral'=>$needs_referral,
			'content'=>serialize($homebiz_response)
		);

		//save customer changes details
		$the_customer = (isset($homebiz_response['homebiz1']['1_customer_id'])) ? $homebiz_response['homebiz1']['1_customer_id'] : '';
		if(!empty($the_customer)){
			$customer = $this->common->customer_format($the_customer); //brokerage_brokers($brokerage_id);
			$customer_info = (@unserialize($customer['customer_info'])) ? unserialize($customer['customer_info']) : array();
			$customer_info['annaul_revenue'] = (isset($homebiz_response['homebiz1']['1_total_annual_revenue'])) ? $homebiz_response['homebiz1']['1_total_annual_revenue'] : '';
			$customer_info['occupation_id'] = (isset($homebiz_response['homebiz1']['occupation_id'])) ? $homebiz_response['homebiz1']['occupation_id'] : '';
			$customer_info['employee_number'] = (isset($homebiz_response['homebiz1']['1_please_state_the_total_number_of_employees_including_self'])) ? $homebiz_response['homebiz1']['1_please_state_the_total_number_of_employees_including_self'] : '';
			$customer_info['breakdown_by_state'] = (isset($homebiz_response['homebiz1']['1_in_respect_of_income_earned_for_the_current_financial_year_please_provide_a_breakdown_by_state'])) ? $homebiz_response['homebiz1']['1_in_respect_of_income_earned_for_the_current_financial_year_please_provide_a_breakdown_by_state'] : array();

			$upd_customer = array(
				'customer_info'=>serialize($customer_info)
			);
			$this->master->updateRecord('customers', $upd_customer, array('id'=>$the_customer));
		}


		if(!empty($biz_quote_id)){


			$this->master->updateRecord('biz_referral', $arr, array('id'=>$biz_quote_id));

			$email = $this->common->db_field_id('brokerage', 'email', $brokerage_id);
			$name = $this->common->db_field_id('brokerage', 'company_name', $brokerage_id);
			$emailer_file_name = 'customer-insured-to-underwriter';
			//admin emailer info
			$adminemail = $this->common->admin_email();

			//email settings
			$info_arr = array(
				'to'=>$email,
				'from'=>$adminemail,
				'subject'=>'Greetings from Ebinder',
				'view'=>$emailer_file_name,
			);

			$other_info = array(
				'password'=>'',
				'emailer_file_name'=>$emailer_file_name,
				'name'=>$name,
				'agency_name'=>$name,
				'user_name'=>$email,
				'user_email'=>$email,
				'link'=>'login',//landing/ml_quote?id='.$biz_quote_id
			);

			$this->emailer->sendmail($info_arr, $other_info);
			$data['other_info'] = $other_info;
			$data['info_arr'] = $info_arr;


		}
		else{

			$user_session = $this->session->all_userdata();

			$customer = array(); //$_POST['customer'];
			$data = array(); //$_POST['data'];
			$nowtime = date('Y-m-d H:i:s');

			$homebiz_response = $this->common->homebiz_response();
			$needs_referral = (!empty($homebiz_response['homebiz6'])) ? 'N' : 'Y';


			$arr = array(
				'date_added'=>$nowtime,
				'needs_referral'=>$needs_referral,
				'broker_id'=>(empty($user_session['broker_id'])) ? $user_session['id'] : $user_session['broker_id'],
				'brokerage_id'=>$user_session['brokerage_id'],
				'agency_id'=>$user_session['agency_id'],
				'customer'=>serialize($customer),
				'content'=>serialize($homebiz_response)
			);

			//check users option selected
			// $quote_options = $homebiz_response['homebiz5']['5_quote_options'];
			$homebiz_response['homebiz5']['5_quote_options'] = $quote_options;

			if($quote_options == 'Bind'){
				$arr['bound'] = 'Y';
				$arr['needs_referral'] = 'N';
				$arr['customer_approved'] = 'Y';
				$arr['date_bound'] = $nowtime;
			}
			else if($quote_options == 'Email'){
				// $arr['needs_referral'] = 'Y';
				$quote_id = $this->master->insertRecord('biz_referral', $arr, true);
				$data['quote_id'] = $quote_id;

				$sel_customer_id = $homebiz_response['homebiz1']['1_customer_id'];
				if(!empty($sel_customer_id)){
					$customer_info = $this->customer_format($sel_customer_id);

					$email = (isset($customer_info['email'])) ? $customer_info['email'] : $homebiz_response['homebiz0']['email'];
					$name = (isset($customer_info['name'])) ? $customer_info['name'] : $homebiz_response['homebiz0']['company_name'];
					$emailer_file_name = 'quote-send-copy-to-customer';
					//admin emailer info
					$adminemail = $this->common->admin_email();

					//email settings
					$info_arr = array(
						'to'=>$email,
						'from'=>$adminemail,
						'subject'=>'Greetings from Ebinder',
						'view'=>$emailer_file_name,
					);

					$other_info = array(
						'password'=>'',
						'emailer_file_name'=>$emailer_file_name,
						'name'=>$name,
						'agency_name'=>$name,
						'user_name'=>$email,
						'user_email'=>$email,
						'link'=>'landing/quote_email_view/'.$quote_id
					);

					$this->emailer->sendmail($info_arr, $other_info);
					$data['other_info'] = $other_info;
					$data['info_arr'] = $info_arr;

				}

				return $data;

			}

			$quote_id = $this->master->insertRecord('biz_referral', $arr, true);
			// $data['quote_id'] = $quote_id;

			$sel_customer_id = $homebiz_response['homebiz1']['1_customer_id'];

			//seek to customer
			if($needs_referral == 'Y'){
				//send customer email notification
				$customer_info = $this->customer_format($sel_customer_id);

				$email = (isset($customer_info['email'])) ? $customer_info['email'] : $homebiz_response['homebiz0']['email'];
				$name = (isset($customer_info['name'])) ? $customer_info['name'] : $homebiz_response['homebiz0']['company_name'];
				$emailer_file_name = 'seek-instruction-to-customer';
				//admin emailer info
				$adminemail = $this->common->admin_email();

				//email settings
				$info_arr = array(
					'to'=>$email,
					'from'=>$adminemail,
					'subject'=>'Greetings from Ebinder',
					'view'=>$emailer_file_name,
				);


				$other_info = array(
					'password'=>'',
					'emailer_file_name'=>$emailer_file_name,
					'name'=>$name,
					'agency_name'=>$name,
					'user_name'=>$email,
					'user_email'=>$email,
					'link'=>'landing/renew_approval?id='.$quote_id.'&email='.$email.'&cid='.md5($quote_id).'&name='.md5($name)
				);

				$this->emailer->sendmail($info_arr, $other_info);
				$data['other_info'] = $other_info;
				$data['info_arr'] = $info_arr;
			}


		}
		return $data;

	}

	public function customer_status($status){
		$stat = '<i class="fa fa-circle text-red"></i> Invitation not yet accepted';
		if($status == 'Y'){
			$stat = '<i class="fa fa-circle text-success"></i> Invitation accepted';
		}
		return $stat;
	}

	public function quote_id($id){
		$year = date("Y");
		$format = sprintf("%'.04d", $id);

		return 'Q'.$format.$year;
	}

	public function db_field_id($db, $field, $id, $idfield = 'id'){
		$class = $this->master->getRecords($db, array($idfield=>$id));
		$name = 'Not specified';
		if(count($class) > 0){
			if(isset($class[0][$field])){
				$name = $class[0][$field];
			}
		}
		return $name;
	}

	public function genre_array($arr){

		$new_arr = array();
		if($arr != ''){
			$arr = unserialize($arr);
			if(count($arr) > 0){
				foreach($arr as $val){
					$new_arr[] = $this->genre_name($val);
				}
			}
		}
		return $new_arr;
	}

	public function customer_array($arr){

		$new_arr = array();
		if($arr != ''){
			$arr = unserialize($arr);
			if(count($arr) > 0){

				foreach($arr as $val){
					$new_arr[] = $this->customer_name($val);
				}
			}
		}
		return $new_arr;
	}

	public function display_array($arr){
		$data = 'No records found';
		if($arr != ''){
			if(!is_array($arr)){
				$arr = unserialize($arr);
			}
			if(count($arr) > 0){
				$data = '';
				$i = 1;
				foreach($arr as $val){
					$data .= $val;

					if($i < count($arr)){
						$data .=', ';
					}

					$i++;
				}
			}

		}

		return $data;
	}


	public function active_referral_count($user_id = NULL){
		$whr = array(
			'status !='=>'P'
		);

		if($user_id != NULL){
			$whr['customer_id'] = $user_id;
		}
		$insurances = $this->master->getRecordCount('bought_insurance', $whr, '', array('status'=>'DESC'));
		return $insurances;
	}

	public function completed_transaction_count($user_id = NULL){
		$whr = array(
			'status'=>'P'
		);

		if($user_id != NULL){
			$whr['customer_id'] = $user_id;
		}
		$insurances = $this->master->getRecordCount('bought_insurance', $whr, '', array('status'=>'DESC'));
		return $insurances;
	}

	public function the_cert_editor(){
		//set empty default
		$so = '[[';
		$sog = '[[';
		$sc = ']]';
		$longline = 'INSURANCE DETAILS HERE';

		$arr = array(
			'consignor'=>$so.'consignor'.$sc,
			'consignee'=>$so.'consignee'.$sc,
			'shipmentdate'=>$so.'shipmentdate'.$sc,
			'insurance'=>$so.'insurance'.$sc,
			'transitfrom'=>$so.'transitfrom'.$sc,
			'cargocat'=>$so.'cargocat'.$sc,
			'portloading'=>$so.'portloading'.$sc,
			'portdischarge'=>$so.'portdischarge'.$sc,
			'transitto'=>$so.'transitto'.$sc,
			'description'=>$so.'description'.$sc,
			'longline'=>$so.'longline'.$sc,
			'shortline'=>$so.'shortline'.$sc,
			'shortlineg'=>$sog.'shortlineg'.$sc,
			'medline'=>$sog.'medline'.$sc,
			'deductible'=>$so.'deductible'.$sc,
			'currency'=>'AUD',
			'premium'=>$so.'premium'.$sc,
			'quoteid'=>$so.'quoteid'.$sc,
			'logolink'=>$so.'logolink'.$sc,
			'signlink'=>$so.'signlink'.$sc,
			'base_currency'=>$so.'base_currency'.$sc,
			'conv_insurance'=>$so.'converterted_insurance'.$sc,
		);

		return $arr;
	}

	public function pdf_data_placeholder(){

		//set empty default
		$logolink = base_url().'assets/frontpage/corporate/images/crisisflo-logo-medium.png';
		$signlink = base_url().'assets/img/cert/cert-signiture.jpg';
		$so = '<span style="background-color: #ffff00">';
		$sog = '<span style="background-color: #c0c0c0">';
		$sc = '</span>';
		$longline = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

		$medline = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

		$shortline = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
		$consigns = 'Name, Business Name and Address&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

		$arr = array(
			'consignor'=>$so.$consigns.$sc,
			'consignee'=>$so.$consigns.$sc,
			'shipmentdate'=>$so.$shortline.$sc,
			'insurance'=>$so.$shortline.$sc,
			'transitfrom'=>$so.$shortline.$sc,
			'cargocat'=>$so.$shortline.$sc,
			'portloading'=>$so.$shortline.$sc,
			'portdischarge'=>$so.$shortline.$sc,
			'transitto'=>$so.$shortline.$sc,
			'description'=>$so.$shortline.$sc,
			'longline'=>$so.$longline.$sc,
			'shortline'=>$so.$shortline.$sc,
			'shortlineg'=>$sog.$shortline.$sc,
			'medline'=>$sog.$medline.$sc,
			'deductible'=>$so.$shortline.$sc,
			'currency'=>$so.$shortline.$sc,
			'premium'=>$so.$shortline.$sc,
			'quoteid'=>$so.$shortline.$sc,
			'logolink'=>$logolink,
			'signlink'=>$signlink,
			'base_currency'=>'',// $so.$shortline.$sc,
			'conv_insurance'=>'', //$so.$shortline.$sc,
		);

		return $arr;
	}

	public function the_cert_data($req_id){
		$bought = $this->master->getRecords('bought_insurance', array('id'=>$req_id));

		$arr = $this->pdf_data_placeholder();

		$so = '<span style="background-color: #ffff00">';
		$sog = '<span style="background-color: #c0c0c0">';
		$sc = '</span>';

		//check if exist
		if(count($bought) > 0){
			foreach($bought as $r=>$value){
				$details = unserialize($value['details']);
				$buy_inputs = $details['buy_inputs'];
				$insurance_details = array();
				foreach($buy_inputs as $r=>$value){
					$insurance_details[$value['name']] = $value['value'];
				}

				$single_input = (isset($details['single_input'])) ? $details['single_input'] : array();
				$single_consignee = (isset($details['consignee'])) ? $details['consignee'] : array();

				if(isset($details['single_input'])){
					$consignor = 'Name: '.$single_input['first_name'].' '.$single_input['first_name'].'<br>';
					$consignor .= 'Business Name: '.$single_input['business_name'].'<br>';
					$consignor .= 'Address: '.$single_input['location'];
					$arr['consignor'] = $so.$consignor.$sc;
				}

				if(isset($details['consignee'])){
					$consignee = 'Name: '.$single_consignee['first_name'].' '.$single_consignee['first_name'].'<br>';
					$consignee .= 'Business Name: '.$single_consignee['business_name'].'<br>';
					$consignee .= 'Address: '.$single_consignee['location'];
					$arr['consignee'] = $so.$consignee.$sc;
				}

				if(isset($insurance_details['shipment_date'])){
					 $arr['shipmentdate'] = $so.$insurance_details['shipment_date'].$sc;
				}

				$insurance_curr = (isset($insurance_details['currency'])) ? $insurance_details['currency'] : 'AUD';

				$arr['premium'] = $so.$insurance_curr.number_format($details['premium'], 2, '.', ',').$sc;
				$arr['currency'] = $insurance_curr;
				$arr['insurance'] = $so.$insurance_curr.$insurance_details['invoice'].$sc;

				if(isset($insurance_details['base_currency'])){
					if($insurance_curr != $insurance_details['base_currency']){
						$arr['base_currency'] = $so.$insurance_details['base_currency'].$sc;
						$conv_val = number_format($insurance_details['converted_invoice'], 2, '.', ',');
						$arr['conv_insurance'] = $so.$arr['base_currency'].$conv_val.$sc;
					}
				}



				$transfrom = $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitfrom'], 'country_id');
				$arr['transitfrom'] = $so.$transfrom.$sc;

				$vessel_name = (isset($insurance_details['vessel_name'])) ? $insurance_details['vessel_name'] : '';
				$arr['cargocat'] = $so.$insurance_details['transmethod'].' - Name of Vessel\Aircraft: '.$vessel_name.$sc;

				if(isset($insurance_details['portloading'])){
					$portloading = (is_numeric($insurance_details['portloading'])) ? $this->common->selectedport($insurance_details['portloading']) : $insurance_details['portloading'];
					$arr['portloading'] = $so.$portloading.$sc;
				}

				if(isset($insurance_details['portdischarge'])){
					$portdischarge = (is_numeric($insurance_details['portdischarge'])) ? $this->common->selectedport($insurance_details['portdischarge']) : $insurance_details['portdischarge'];
					$arr['portdischarge'] = $so.$portdischarge.$sc;
				}


				$transsitto = $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitto'], 'country_id');
				$arr['transitto'] = $so.$transsitto.$sc;
				$arr['description'] = $so.$insurance_details['goods_desc'].$sc;

				$default_deductible = (isset($insurance_details['default_deductible'])) ? $insurance_details['default_deductible'] : 0;
				$dedd = $this->common->db_field_id('deductibles', 'deductible', $default_deductible, 'rate');
				$arr['deductible'] = $so.$dedd.$sc;
			}
		}

		return $arr;

	}

	public function the_quote_data($req_id , $no_highlight = ''){
		$bought = $this->master->getRecords('bought_quote', array('id'=>$req_id));

		$arr = $this->pdf_data_placeholder();

		$so = '<span style="background-color: #ffff00">';
		$sog = '<span style="background-color: #c0c0c0">';
		$sc = '</span>';
		$separator = '<br>';

		if($no_highlight != ''){
			$so = '';
			$sog = '';
			$sc = '';
			$separator = ', ';
		}


		//get session data
		$details = array(
			'buy_inputs'=>$this->session->userdata('buy_inputs'),
			'single_input'=>$this->session->userdata('single'),
			'consignee'=>$this->session->userdata('consignee'),
			'premium'=>$this->session->userdata('premium')
		);

		$buy_inputs = $details['buy_inputs'];
		$insurance_details = array();
		if(is_array($buy_inputs)){
			foreach($buy_inputs as $r=>$value){
				$insurance_details[$value['name']] = $value['value'];
			}
		}

		$single_input = (isset($details['single_input'])) ? $details['single_input'] : array();
		$single_consignee = (isset($details['consignee'])) ? $details['consignee'] : array();

		//check if exist
		if(count($bought) > 0){
			foreach($bought as $r=>$value){
				$details = unserialize($value['details']);
				$buy_inputs = $details['buy_inputs'];
				//$insurance_details = array();
				foreach($buy_inputs as $r=>$value){
					$insurance_details[$value['name']] = $value['value'];
				}

				$single_input = (isset($details['single_input'])) ? $details['single_input'] : array();
				$single_consignee = (isset($details['consignee'])) ? $details['consignee'] : array();

			}
		}

		if(count($single_consignee) > 0){
			if(count($bought) > 0){
				$arr['quoteid'] = $this->quote_id($bought[0]['id']);
			}

			if(isset($details['single_input'])){
				$consignor = 'Name: '.$single_input['first_name'].' '.$single_input['first_name'].$separator;
				$consignor .= 'Business Name: '.$single_input['business_name'].$separator;
				$consignor .= 'Address: '.$single_input['location'];
				$arr['consignor'] = $so.$consignor.$sc;
			}

			if(isset($details['consignee'])){
				$consignee = 'Name: '.$single_consignee['first_name'].' '.$single_consignee['first_name'].$separator;
				$consignee .= 'Business Name: '.$single_consignee['business_name'].$separator;
				$consignee .= 'Address: '.$single_consignee['location'];
				$arr['consignee'] = $so.$consignee.$sc;
			}

			if(isset($insurance_details['shipment_date'])){
				 $arr['shipmentdate'] = $so.$insurance_details['shipment_date'].$sc;
			}

			$insurance_curr = (isset($insurance_details['currency'])) ? $insurance_details['currency'] : 'AUD';

			$arr['premium'] = $so.$insurance_curr.number_format($details['premium'], 2, '.', ',').$sc;
			$arr['currency'] = $insurance_curr;
			$arr['insurance'] = $so.$insurance_curr.$insurance_details['invoice'].$sc;

			if(isset($insurance_details['base_currency'])){
				$base_curr = $insurance_details['base_currency'];
				if($insurance_curr != $base_curr){
					$arr['base_currency'] = $so.$base_curr.$sc;
					$conv_val = number_format($insurance_details['converted_invoice'], 2, '.', ',');
					$arr['conv_insurance'] = $so.$base_curr.$conv_val.$sc;
				}
			}



			$transfrom = $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitfrom'], 'country_id');
			$arr['transitfrom'] = $so.$transfrom.$sc;

			$vessel_name = (isset($insurance_details['vessel_name'])) ? $insurance_details['vessel_name'] : '';
			$arr['cargocat'] = $so.$insurance_details['transmethod'].' - Name of Vessel\Aircraft: '.$vessel_name.$sc;

			if(isset($insurance_details['portloading'])){
				$portloading = (is_numeric($insurance_details['portloading'])) ? $this->common->selectedport($insurance_details['portloading']) : $insurance_details['portloading'];
				$arr['portloading'] = $so.$portloading.$sc;
			}

			if(isset($insurance_details['portdischarge'])){
				$portdischarge = (is_numeric($insurance_details['portdischarge'])) ? $this->common->selectedport($insurance_details['portdischarge']) : $insurance_details['portdischarge'];
				$arr['portdischarge'] = $so.$portdischarge.$sc;
			}


			$transsitto = $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitto'], 'country_id');
			$arr['transitto'] = $so.$transsitto.$sc;
			$arr['description'] = $so.$insurance_details['goods_desc'].$sc;

			$default_deductible = (isset($insurance_details['default_deductible'])) ? $insurance_details['default_deductible'] : 0;
			$dedd = $this->common->db_field_id('deductibles', 'deductible', $default_deductible, 'rate');
			$arr['deductible'] = $so.$dedd.$sc;
		}

		return $arr;

	}

	public function billing_report($date_from, $date_to){
		$insurances = $this->master->getRecords('bought_quote', array('status'=>'P'));

		$billings = array();
		if(count($insurances) > 0){
			foreach($insurances as $r=>$value){
				$insurance = $this->common->the_quote_data($value['id'], 'no_highlight');

				$details = unserialize($value['details']);
				$premium = (isset($details['premium'])) ? $details['premium'] : 0;
				$premium = number_format($premium, 2, '.', ',');
				$buy_inputs = (isset($details['buy_inputs'])) ? $details['buy_inputs'] : array();
				$insurance_details = array();
				foreach($buy_inputs as $bi=>$bival){
					$insurance_details[$bival['name']] = $bival['value'];
				}

				$single_input = (isset($details['single_input'])) ? $details['single_input'] : array();
				$cust_name = 'Unknown';
				if(isset($single_input['first_name']) && isset($single_input['last_name'])){
					$cust_name = $single_input['first_name'].' '.$single_input['last_name'];
				}
				$transitto = (isset($insurance_details['transitto'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitto'], 'country_id') : 'not specified';
				$transitfrom = (isset($insurance_details['transitfrom'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitfrom'], 'country_id') : 'not specified';


				$date_purch = explode(' - ', $insurance_details['shipment_date']);

				$agent = $this->common->customer_name($value['customer_id']);

				$billings[] = array(
					'date'=>date_format(date_create($date_purch[0]),'d-m-Y'),
					'purchase_date'=>$date_purch[0],
					'agent'=>$agent,
					'consignee'=>$insurance['consignee'],
					'qoute'=>$this->quote_id($value['id']),
					'premium'=>'$'.$premium
				);
			}
		}


		//filter by date range
		$filtered_billings = array();
		if(count($billings) > 0){
			foreach($billings as $r=>$value){
				if(strtotime($value['date']) >= strtotime($date_from) &&
					strtotime($value['date']) <= strtotime($date_to)){

						unset($value['date']);
						$filtered_billings[] = $value;
				}
			}
		}

		//sort if not empty
		if(count($filtered_billings) > 0){
			uasort($filtered_billings,array($this,'date_compare'));
		}

		return $filtered_billings;
	}

	public function product_enabled_text($arr = NULL){
		$txt = '';
		if(!empty($arr)){
			$count = 1;
			foreach($arr as $r=>$value){
				$txt .= $this->product_name($value);

				if($count != count($arr)){
						$txt .= ', ';
				}
				$count++;
			}
		}
		else{
			$txt = 'Nothing selected';
		}
		return $txt;
	}

	public function access_rights(){
		$arr = array(
			'Dashboard',
			//'Manage Brokerage',
			'Manage Brokers',
			'Manage Claims',
			//'Manage Ratings',
			'Manage Policies',
			//'Manage Transactions',
			//'Access Log',
			// 'Policy Document',
			// 'Reporting',
			//'Activity Log',
			// 'Settings',
		);

			return $arr;
	}
	public function access_rights_all(){
		$arr = array(
				'Dashboard',
				'Manage Brokerage',
				'Manage Brokers',
				'Manage Claims',
				'Manage Ratings',
				'Manage Policies',
				'Manage Transactions',
				'Manage Referrals',
				//'Access Log',
				'Policy Document',
				'Reporting',
				//'Activity Log',
				'Settings',
			);

			return $arr;
	}

	public function report_icons(){
		$column_header = array(
			'fa-quote-left',
			'fa-key',
			'fa-user',
			'fa-usd',
			'fa-users',
			'fa-building-o',
			'fa-plus-circle',
			'fa-child',
			'fa-picture-o',
			'fa-check-circle',
			'fa-calendar',
			'fa-times-circle-o',
		);

		return $column_header;
	}

	public function report_header_name(){
		$column_header = array(
			'consignor',
			'consignee',
			'shipmentdate',
			'insurance',
			'transitfrom',
			'cargocat',
			'portloading',
			'portdischarge',
			'transitto',
			'description',
			'deductible',
			'currency',
			'premium',
			'quoteid',
			'base_currency',
			'conv_insurance',
			'agent',
			'purchase_date',
		);

		return $column_header;
	}

	public function report_header(){
		$column_header = array(
			'Quote ID',
			'Policy ID',
			'Customer Details',
			'Total Annaul Revenue',
			'Total Employees',
			'Occupation',
			'Total Premium',
			'Insured Names',
			'Interested Parties',
			'Date Bound',
			'Remaining Days',
			'Expiration Date',
		);

		return $column_header;
	}

	public function ebinder_report($date_from, $date_to, $header_name = array()){
		$user_session = $this->session->all_userdata();
		$id = $user_session['id'];
		$agency_id = $user_session['agency_id'];
		$brokerage_id = $user_session['brokerage_id'];
		$first_broker = $user_session['first_broker'];
		$logged_admin_id = (isset($user_session['logged_admin_id'])) ? $user_session['logged_admin_id'] : '';
		$logged_admin = (isset($user_session['logged_admin'])) ? $user_session['logged_admin'] : '';

		$whr = array('customer_approved'=>'Y', 'bound'=>'Y');
		if(!empty($logged_admin_id)){
			$whr['agency_id'] = $agency_id;
		}
		else{
			if($first_broker == 'Y'){
				$whr['brokerage_id'] = $brokerage_id;
			}
			else{
				$whr['broker_id'] = $id;
			}
		}
		$biz_referral = $this->common->format_referral($whr);


		//check date range
		$arr = array();
		if(!empty($biz_referral)){
			foreach($biz_referral as $r=>$value){
				if(strtotime($value['date_bound']) >= strtotime($date_from) &&
					strtotime($value['date_bound']) <= strtotime($date_to)){

						$arr[] = $value;
				}

			}
		}

		//filter data
		$arr2 = array();
		if(!empty($arr)){
			foreach($arr as $r=>$value){
				$contents = $value['content'];
				$customer = 'Name: '.$value['customer']['name'].', '.'Mobile: '.$value['customer']['phone'].', '.'Email: '.$value['customer']['email'];
				$total_annaul_revenue = (isset($contents['homebiz1']['1_total_annual_revenue'])) ? '$'.$contents['homebiz1']['1_total_annual_revenue'] : '';
				$total_employees = (isset($contents['homebiz1']['1_please_state_the_total_number_of_employees_including_self'])) ? $contents['homebiz1']['1_please_state_the_total_number_of_employees_including_self'] : '';
				$occupation = (isset($contents['homebiz1']['occupation_find'])) ? $contents['homebiz1']['occupation_find'] : '';
				$total_premium = (isset($contents['homebiz5']['5_total_premium_payable_including_gst'])) ? '$'.$contents['homebiz5']['5_total_premium_payable_including_gst'] : '';

				$insured_names = '';
				if(isset($contents['homebiz6']['6_insured_name'])){
					$insured = $contents['homebiz6']['6_insured_name'];
					if(!empty($insured)){
						foreach($insured as $rins=>$rvalue){
							if(!empty($rvalue)){
								$insured_names .= $rvalue.', ';
							}
						}
					}
				}

				$interested_parties = '';
				if(isset($contents['homebiz6']['6_interested_parties'])){
					$insured = $contents['homebiz6']['6_interested_parties'];
					if(!empty($insured)){
						foreach($insured as $rins=>$rvalue){
							if(!empty($rvalue)){
								$interested_parties .= $rvalue.', ';
							}
						}
					}
				}


				$newval = array(
					'quote_id'=>$value['quote_id'],
					'policy_id'=>$value['policy_id'],
					'customer_details'=>$customer,
					'total_annaul_revenue'=>$total_annaul_revenue,
					'total_employees'=>$total_employees,
					'occupation'=>$occupation,
					'total_premium'=>$total_premium,
					'insured_names'=>$insured_names,
					'interested_parties'=>$interested_parties,
					'date_bound'=>$value['date_bound'],
					'remaining_days'=>$value['days_remaining'],
					'expiration_date'=>$value['date_expires'],

				);


				$arr2[] = $newval;

			}
		}

		//filter data
		$arr3 = array();
		if(!empty($arr2)){
			foreach($arr2 as $r=>$value){
				$newval = array();
				if(!empty($header_name)){
					foreach($header_name as $hr=>$hvalue){
						$hvalue = url_title($hvalue, 'underscore', TRUE);
						$newval[$hvalue] = $value[$hvalue];
					}
				}
				$arr3[] = $newval;
			}
		}

		return $arr3;

	}



	public function ebinder_quote_report($quote_id, $header_name = array()){
		$user_session = $this->session->all_userdata();
		$id = $user_session['id'];
		$agency_id = $user_session['agency_id'];
		$brokerage_id = $user_session['brokerage_id'];
		$first_broker = $user_session['first_broker'];
		$logged_admin_id = $user_session['logged_admin_id'];
		$logged_admin = $user_session['logged_admin'];



		$date_from = '0000-00-00 00:00:00';
		$date_to = date('Y-m-d H:i:s');



		$whr['id'] = $id;
		$biz_referral = $this->common->format_referral($whr);


		//check date range
		$arr = array();
		if(!empty($biz_referral)){
			foreach($biz_referral as $r=>$value){
				if(strtotime($value['date_bound']) >= strtotime($date_from) &&
					strtotime($value['date_bound']) <= strtotime($date_to)){

						$arr[] = $value;
				}

			}
		}

		//filter data
		$arr2 = array();
		if(!empty($arr)){
			foreach($arr as $r=>$value){
				$contents = $value['content'];
				$customer = 'Name: '.$value['customer']['name'].', '.'Mobile: '.$value['customer']['phone'].', '.'Email: '.$value['customer']['email'];
				$total_annaul_revenue = (isset($contents['homebiz1']['1_total_annual_revenue'])) ? '$'.$contents['homebiz1']['1_total_annual_revenue'] : '';
				$total_employees = (isset($contents['homebiz1']['1_please_state_the_total_number_of_employees_including_self'])) ? $contents['homebiz1']['1_please_state_the_total_number_of_employees_including_self'] : '';
				$occupation = (isset($contents['homebiz1']['occupation_find'])) ? $contents['homebiz1']['occupation_find'] : '';
				$total_premium = (isset($contents['homebiz5']['5_total_premium_payable_including_gst'])) ? '$'.$contents['homebiz5']['5_total_premium_payable_including_gst'] : '';

				$insured_names = '';
				if(isset($contents['homebiz6']['6_insured_name'])){
					$insured = $contents['homebiz6']['6_insured_name'];
					if(!empty($insured)){
						foreach($insured as $rins=>$rvalue){
							if(!empty($rvalue)){
								$insured_names .= $rvalue.', ';
							}
						}
					}
				}

				$interested_parties = '';
				if(isset($contents['homebiz6']['6_interested_parties'])){
					$insured = $contents['homebiz6']['6_interested_parties'];
					if(!empty($insured)){
						foreach($insured as $rins=>$rvalue){
							if(!empty($rvalue)){
								$interested_parties .= $rvalue.', ';
							}
						}
					}
				}


				$newval = array(
					'quote_id'=>$value['quote_id'],
					'policy_id'=>$value['policy_id'],
					'customer_details'=>$customer,
					'total_annaul_revenue'=>$total_annaul_revenue,
					'total_employees'=>$total_employees,
					'occupation'=>$occupation,
					'total_premium'=>$total_premium,
					'insured_names'=>$insured_names,
					'interested_parties'=>$interested_parties,
					'date_bound'=>$value['date_bound'],
					'remaining_days'=>$value['days_remaining'],
					'expiration_date'=>$value['date_expires'],

				);


				$arr2[] = $newval;

			}
		}

		//filter data
		$arr3 = array();
		if(!empty($arr2)){
			foreach($arr2 as $r=>$value){
				$newval = array();
				if(!empty($header_name)){
					foreach($header_name as $hr=>$hvalue){
						$hvalue = url_title($hvalue, 'underscore', TRUE);
						$newval[$hvalue] = $value[$hvalue];
					}
				}
				$arr3[] = $newval;
			}
		}

		return $biz_referral;

	}


	public function bordereaux_report($date_from, $date_to){
		$insurances = $this->master->getRecords('bought_quote', array('status'=>'P'));

		$billings = array();
		if(count($insurances) > 0){
			foreach($insurances as $r=>$value){
				$insurance = $this->common->the_quote_data($value['id'], 'no_highlight');

				$details = unserialize($value['details']);
				$premium = (isset($details['premium'])) ? $details['premium'] : 0;
				$premium = number_format($premium, 2, '.', ',');
				$buy_inputs = (isset($details['buy_inputs'])) ? $details['buy_inputs'] : array();
				$insurance_details = array();
				foreach($buy_inputs as $bi=>$bival){
					$insurance_details[$bival['name']] = $bival['value'];
				}

				$single_input = (isset($details['single_input'])) ? $details['single_input'] : array();
				$cust_name = 'Unknown';
				if(isset($single_input['first_name']) && isset($single_input['last_name'])){
					$cust_name = $single_input['first_name'].' '.$single_input['last_name'];
				}
				$transitto = (isset($insurance_details['transitto'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitto'], 'country_id') : 'not specified';
				$transitfrom = (isset($insurance_details['transitfrom'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitfrom'], 'country_id') : 'not specified';


				$date_purch = explode(' - ', $insurance_details['shipment_date']);

				$agent = $this->common->customer_name($value['customer_id']);

				unset($insurance['longline']);
				unset($insurance['shortlineg']);
				unset($insurance['shortline']);
				unset($insurance['medline']);
				unset($insurance['logolink']);
				unset($insurance['signlink']);
//				unset($insurance['conv_insurance']);
//				unset($insurance['base_currency']);

				$insurance['agent'] = $agent;
				$insurance['purchase_date'] = $date_purch[0]; //date_format(date_create($date_purch[0]), 'Y-m-d');
				$insurance['date'] = date_format(date_create($date_purch[0]), 'd-m-Y');
				$billings[] = $insurance;

			}
		}


		//filter by date range
		$filtered_billings = array();
		if(count($billings) > 0){
			foreach($billings as $r=>$value){
				if(strtotime($value['date']) >= strtotime($date_from) &&
					strtotime($value['date']) <= strtotime($date_to)){

						unset($value['date']);
						$filtered_billings[] = $value;
				}
			}
		}

		//sort if not empty
		if(count($filtered_billings) > 0){
			uasort($filtered_billings,array($this,'date_compare'));
		}

		return $filtered_billings;
	}

	public function date_compare($a, $b){
		$t1 = strtotime($a['purchase_date']);
		$t2 = strtotime($b['purchase_date']);
		return $t1 - $t2;
	}

	public function selectedport($id){
		$txt = '';

		if(is_numeric($id)){
			$port_codes = $this->master->getRecords('port_codes',array('id'=>$id));

			if(count($port_codes) > 0){
				$txt = $port_codes[0]['country_code'].' - '.$port_codes[0]['port_name'].'('.$port_codes[0]['port_code'].')';
			}
		}
		else{
			$txt = $id;
		}
		return $txt;
	}

	public function policy_request_status($status){
		$stat = '<i class="fa fa-circle text-muted"></i> Pending';
		if($status == 'P'){
			$stat = '<i class="fa fa-circle text-success"></i>  Paid';
		}

		return $stat;
	}

	public function quote_status($status){
		$stat = '<i class="fa fa-circle text-muted"></i> Pending';
		if($status == 'P'){
			$stat = '<i class="fa fa-circle text-success"></i> Bound';
		}
		else if($status == 'A'){
			$stat = '<i class="fa fa-circle text-info"></i> Confirmed';
		}
		else if($status == 'R'){
			$stat = '<i class="fa fa-circle text-danger"></i> Rejected';
		}
		return $stat;
	}


	public function agency_nofirst(){
		$agency = $this->master->getRecords('agency', array('id !='=>'6', 'domain !='=>''));

		$arr = array();

		if(!empty($agency)){
			foreach($agency as $r=>$value){
				$agency_id = $value['id'];
				$first = $this->master->getRecordCount('customers', array('agency_id'=>$agency_id, 'first_underwriter'=>'Y'));

				if($first == 0){
					$arr[] = $value;
				}
			}
		}

		return $arr;
	}

	public function agency_customers($agency_id){
		$customer = $this->master->getRecords('customers', array('agency_id'=>$agency_id));
		$customers = array();


		if(count($customer) > 0){
			foreach($customer as $r=>$value){
				$arr = array(
					'id'=>$value['id'],
					'name'=>$value['first_name'].' '.$value['last_name'],
					'avatar'=>$this->common->avatar($value['id'], 'customer')
				);

				$customers[] = $arr;
			}
		}

		return $customers;
	}

	public function prorate($days){
		$permonth = $this->db_field_id('admin', 'commission_rate', '2');
		$perday = $permonth/30;

		$prorate = (!empty($days)) ? $days : 0;
		$prorate = $prorate * $perday;

		return round($prorate, 2);

	}

	public function brokers($id, $paymentdate = NULL){
		$customer = $this->master->getRecords('customers', array('id'=>$id));
		$customers = array();


		if(count($customer) > 0){
			$count = count($customer);
			foreach($customer as $r=>$value){

				$value['full_name'] = $value['first_name'].' '.$value['last_name'];
				$value['img'] = $this->avatar($value['id'], 'customer');
				$value['status_text'] = ($value['status'] == 'Y') ? 'Verified' : 'Not Verified';
				$value['format_date_reg'] = date_format(date_create($value['date_added']), 'd M Y');
				$value['base_link'] = $this->common->base_url();


				$date_stored = ($value['last_payment_date'] != '0000-00-00 00:00:00') ? $value['last_payment_date'] : $value['date_added'];
				$date_assigned = new DateTime($date_stored);

				$nowtime = (!empty($paymentdate)) ? $paymentdate : date('Y-m-1 H:i:s');
				$effectiveDate = (!empty($paymentdate)) ? strtotime("+0 months", strtotime($nowtime)) : strtotime("+1 months", strtotime($nowtime)); // returns timestamp
				$effectiveDate = date('Y-m-d',$effectiveDate); // formatted version

				$payout_date = new DateTime($effectiveDate);

				$diff = $date_assigned->diff($payout_date);

				$storage_days = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $diff->h/24;
				$value['days_to_pay'] = $storage_days;
				$value['payment_date'] = date_format(date_create($effectiveDate), 'd M Y');
				$value['prorate'] = $this->prorate($storage_days);
				$value['policies'] = (@unserialize($value['broker_policies'])) ? unserialize($value['broker_policies']) : array();

				$customers[] = $value;

				if($count == 1){
					$customers = $value;
				}
			}
		}

		return $customers;

	}

	public function total_billing($id, $paymentdate = NULL){
		$customers = $this->customers($id, $paymentdate);
		$total = 0;
		$count = 1;
		if(count($customers) > 0){
			foreach($customers as $r=>$value){
				if($count > 5){
					$total += $value['prorate'];
				}
				$count++;
			}
		}

		return $total;
	}


	public function transactions($id){
		$customers = $this->brokers($id);
		$trans = $this->master->getRecords('q_transactions', array('broker_id'=>$id));
		$arr = array();

		if(count($trans) > 0){
			foreach($trans as $r=>$value){
				$value['arr'] = unserialize($value['arr']);
				$value['date_added_format'] = date_format(date_create($value['date_added']), 'd M Y');
				$arr[] = $value;
			}
		}

		return $arr;
	}

	public function customers_list($id, $type = 'Y'){
		$arr = array();

		$whr = array('studio_id'=>$id);
		if($type == 'Y'){
			$whr['customer_type'] = 'Y';
		}
		else if($type == 'N'){
			$whr['customer_type'] = 'N';
		}

		$customer = $this->master->getRecords('customers', $whr);
		if(count($customer) > 0){
			$count = count($customer);
			foreach($customer as $r=>$value){

				$arr[] = $this->customer_format($value['id']);
			}
		}
		return $arr;

	}

	public function brokerage_brokers($brokerage_id){
		$arr = array();

		$whr = array('brokerage_id'=>$brokerage_id);
		$whr['customer_type'] = 'N';

		$customer = $this->master->getRecords('customers', $whr, '*', array('id'=>'DESC'));
		if(count($customer) > 0){
			$count = count($customer);
			foreach($customer as $r=>$value){

				$arr[] = $this->customer_format($value['id']);
			}
		}
		return $arr;

	}

	public function customers($id, $paymentdate = NULL){
		$arr = array();
		$customer = $this->master->getRecords('customers', array('studio_id'=>$id));
		if(count($customer) > 0){
			$count = count($customer);
			foreach($customer as $r=>$value){

				$arr[] = $this->brokers($value['id'], $paymentdate);
			}
		}
		return $arr;

	}

	public function broker_info($id, $field = 'full_name'){
		$brokers = $this->brokers($id);

		$info = (isset($brokers[$field])) ? $brokers[$field] : 'Unknown';
	}

	public function agency_prices($agency_id, $type){
		$agency = $this->master->getRecords('agency', array('id'=>$agency_id));
		$mini = array();

		if(count($agency) > 0){
			$min = $agency[0][$type];
			if($min != ''){
				$mini = unserialize($min);
			}
		}
		return $mini;
	}


	public function agency_minimum_premium($agency_id){
		$agency = $this->master->getRecords('agency', array('id'=>$agency_id));
		$mini = 0;

		if(count($agency) > 0){
			$mini = $agency[0]['minimum_premium'];

		}
		return $mini;
	}

	public function agency_max_insured($agency_id){
		$agency = $this->master->getRecords('agency', array('id'=>$agency_id));
		$mini = 0;

		if(count($agency) > 0){
			$mini = str_replace(',','', $agency[0]['max_insurance']);

		}
		return $mini;
	}

	public function base_currency($agency_id){
		$agency = $this->master->getRecords('agency', array('id'=>$agency_id));
		$mini = 0;

		if(count($agency) > 0){
			$mini = ($agency[0]['base_currency'] != '') ? $agency[0]['base_currency'] : 'AUD';
		}
		return $mini;
	}

	public function converted_value($from,$to, $amount){
		$url = 'http://currency-api.appspot.com/api/'.$from.'/'.$to.'.json?amount='.$amount;

		$result = file_get_contents($url);
		$result = json_decode($result);

		$value = $amount;
		if ($result->success) {
			$value = $result->amount;
		}

		return $value;
	}

	public function numeric_currency($amount){

		$b = str_replace( ',', '', $amount );

		if( is_numeric( $b ) ) {
			$amount = $b;
		}

		return $amount;
	}

	public function cargo_name($cargo_id){
		$cargo_category = $this->master->getRecords('cargo_category', array('id'=>$cargo_id));

		$name = 'Not specified';
		if(count($cargo_category) > 0){
			$name = $cargo_category[0]['hs_code'].' &middot; '.$cargo_category[0]['description'];
		}
		return $name;
	}

	public function employee_multiplier($employee_count){
		$row = 0;
		if($employee_count >= 10 && $employee_count <= 20){
			$row = 1;
		}
		else if($employee_count >= 20 && $employee_count <= 50){
			$row = 2;
		}
		else if($employee_count >= 51 && $employee_count <= 100){
			$row = 3;
		}
		else if($employee_count >= 101){
			$row = 4;
		}
		return $row;
	}

	public function transportations(){
		$arr = array(
			'1 to 10 employees',
			'11 to 20 employees',
			'21 to 50 employees',
			'51 to 100 employees',
			'>100 employees'
		);
		return $arr;
	}

	public function transportations_icon(){
		$arr = array(
			'fa fa-ship',
			'fa fa-plane',
			'fa fa-truck',
			'fa fa-globe',
			'fa fa-globe'
		);
		return $arr;
	}

	public function admin_email(){
		$admin = $this->master->getRecords('admin');
		return $admin[0]['email'];
	}

	public function recovery_admin(){
		$admin = $this->master->getRecords('admin');
		return $admin[0]['recovery_email'];
	}

	public function default_cargo(){
		$the_agency = $this->session->userdata('logged_admin_agency');
		$the_agency_id = $the_agency; //(isset($the_agency['agency_id'])) ? $the_agency['agency_id'] : '';

		$user_id = (!empty($the_agency_id)) ? $the_agency_id : '';

		if($user_id == ''){// || $this->session->userdata('logged_admin') != ''){
			$admin = $this->master->getRecords('admin');
			$user_id =  $admin[0]['default_cargo'];
		}

		return $user_id;

	}

	public function encrypt( $q ) {
		$cryptKey = 'TMhh4G1UrC4j9QtxVSk06qORx2aD93Ye';
		$qEncoded = base64_encode( mcrypt_encrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), $q, MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ) );
		return( $qEncoded );
	}

	public function decrypt( $q ) {
		$cryptKey = 'TMhh4G1UrC4j9QtxVSk06qORx2aD93Ye';
		$qDecoded = rtrim( mcrypt_decrypt( MCRYPT_RIJNDAEL_256, md5( $cryptKey ), base64_decode( $q ), MCRYPT_MODE_CBC, md5( md5( $cryptKey ) ) ), "\0");
		return( $qDecoded );
	}


	public function find_active_session($domain){
		$srvr = $_SERVER['REMOTE_ADDR'];

		$this->db->like('data', $domain);
		$all_sess = $this->master->getRecords('ci_sessions');

		$nowtime = date('Y-m-d H:i:s');

		$arr = array();

		if(count($all_sess) > 0){
			foreach($all_sess as $r=>$value){
				$restored = $this->decode_session($value['data']);

				$date = date('Y-m-d H:i:s', $restored['__ci_last_regenerate']);

				//get date difference from now vs session
				$seconds = strtotime($nowtime) - strtotime($date);
				$days    = floor($seconds / 86400);
				$hours   = floor(($seconds - ($days * 86400)) / 3600);
				$minutes = floor(($seconds - ($days * 86400) - ($hours * 3600))/60);

				$restored['active_session'] = ($minutes < 120) ? true : false;
				$restored['minute'] = $minutes;

				if($restored['active_session']){
					if($restored['domain'] == $domain && $srvr == $value['ip_address']){
						$arr[] = $restored;
					}
				}

			}
		}

		return $arr;
	}

	public function decode_session($session_string){
		$current_session = session_encode();
		foreach ($_SESSION as $key => $value){
			unset($_SESSION[$key]);
		}
		session_decode($session_string);
		$restored_session = $_SESSION;
		foreach ($_SESSION as $key => $value){
			unset($_SESSION[$key]);
		}
		session_decode($current_session);
		return $restored_session;
	}


	public function base_url(){
		$url = $_SERVER["SERVER_NAME"];
		$account = str_replace(".ebinder.com.au","",$url);

		$base_link = ($account != 'ebinder.com.au' && $account != 'localhost') ? 'https://'.$url.'/' : base_url();
		return $base_link;
	}

	public function bbase_url(){
		$url = $_SERVER["SERVER_NAME"];
		$account = str_replace(".ebinder.com.au","",$url);

		$account = ($account != 'localhost' && $this->session->userdata('domain') != '') ? $this->session->userdata('domain') : 'localhost';
		$base_link = ($account != 'ebinder.com.au' && $account != 'localhost') ? 'https://'.$account.'.ebinder.com.au/' : base_url();
		return $base_link;
	}

	public function the_domain(){
		$url = $_SERVER["SERVER_NAME"];
		$account = str_replace(".ebinder.com.au","",$url);

		return ($account != '' && $account != 'ebinder.com.au' && $account != 'localhost') ? $account : '';
	}


	public function q_select_format($name){
		$data = $this->master->getRecords('q_select', array('name'=>$name));
		$dd = array();
		foreach($data as $r=>$value){
			$dd = unserialize($value['arr']);
		}

		return $dd;
	}

	public function country_format(){
		$countries = $this->master->getRecords('country_t', array(), '*', array('country_id'=>'ASC'));
		$arr = array();
		foreach($countries as $r=>$value){
			$value['short_name_iso2'] = $value['short_name'].' (+'.$value['calling_code'].')';
			$arr[] = $value;
		}
		return $arr;
	}

	public function print_var_name($var) {
		foreach($GLOBALS as $var_name => $value) {
			if ($value === $var) {
				return $var_name;
			}
		}

		return false;
	}


	public function braintree_clienttoken(){
//		Braintree_Configuration::environment('sandbox');
//		Braintree_Configuration::merchantId('ryjn4z3r67v79dbk');
//		Braintree_Configuration::publicKey('npkyhcw7jrv4cwjq');
//		Braintree_Configuration::privateKey('66112dd4f9bd6db4557a725b9481b0b9');

		$clientToken = Braintree_ClientToken::generate();
		return $clientToken;

	}


	public function thequote($arr){
		$homebiz1 = $arr['homebiz1'];
		$current_premium = $this->session->userdata('current_premium');
		$user_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];
		$agency_info = $this->master->getRecords('agency', array('id'=>$user_id));

		$deductibles = $this->master->getRecords('deductibles', array('agency_id'=>$user_id));
		$cargo = $this->master->getRecords('q_business_specific'); //this->master->getRecords('cargo_category');
		$transportations = $this->common->transportations();
		$countries = $this->master->getRecords('country_t');
		$currencies = $this->master->getRecords('api_currencies', array('in_stripe'=>'Y'));


		$cargo_prices = $this->common->agency_prices($user_id,'cargo_prices');
		$cargo_max = $this->common->agency_prices($user_id,'cargo_max');
		$transportation_prices = $this->common->agency_prices($user_id,'transportation_prices');
		$country_prices = $this->common->agency_prices($user_id,'country_prices');
		$country_referral = $this->common->agency_prices($user_id,'country_referral');
		$zone_multiplier = $this->common->agency_prices($user_id,'zone_multiplier');
		$base_currency = ($agency_info[0]['base_currency']) ? $agency_info[0]['base_currency'] : 'AUD';


		//Base Rate x Annual Revenue x Employee Multiplier (if applicable) x Occupation Multiplier
		$base_rate = ($agency_info[0]['base_rate']) ? $agency_info[0]['base_rate'] : 0;
		$policy_fee = ($agency_info[0]['policy_fee']) ? $agency_info[0]['policy_fee'] : 0;
		$annaul_revenue = $homebiz1['1_total_annual_revenue'];
		$annaul_revenue =  preg_replace("/[^0-9.]/", "", $annaul_revenue);
		$occupation_id = $homebiz1['occupation_id'];
		$occupation_multiplier = (isset($cargo_prices[$occupation_id])) ? $cargo_prices[$occupation_id] : 0;
		$employees = $homebiz1['1_please_state_the_total_number_of_employees_including_self'];
		$employees = preg_replace("/[^0-9,.]/", "", $employees);
		$employee_row = $this->employee_multiplier($employees);
		$employee_multiplier = (isset($transportation_prices[$employee_row])) ? $transportation_prices[$employee_row] : '';

		//calculations
		$thequote = $base_rate * $annaul_revenue * $employee_multiplier * $occupation_multiplier;
		$thequote = $thequote + $policy_fee;

		$data = array(
			'policy_fee'=>$policy_fee,
			'total'=>$thequote,
			'total_format'=>number_format($thequote, 2, '.', ','),
			'base_rate'=>$base_rate,
			'annaul_revenue'=>$annaul_revenue,
			'occupation_multiplier'=>$occupation_multiplier,
			'employee_multiplier'=>$employee_multiplier,
			'employee_row'=>$employee_row,
			'employees'=>$employees
		);

		//if endorse
		if(isset($current_premium['5_base_rate'])){
			$whr = array(
				'id'=>$this->session->userdata('biz_quote_id')
			);
			$biz_referral = $this->format_referral($whr);

			$days = 365;
			$date = $biz_referral['date_bound'];
			$date = strtotime("+".$days." days", strtotime($date));
			$date_expires = date("Y-m-d H:i:s", $date);

			//calculate expiration of 60days
			$date_bound = new DateTime($date_expires);
			$date_today = (isset($homebiz1['1_date_of_endorsement'])) ? $homebiz1['1_date_of_endorsement'] : date('Y-m-d H:i:s');
			$date_today = new DateTime($date_today); //'2016-03-01 00:00:00');//

			$diff = $date_today->diff($date_bound);

			$total_days = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $diff->h/24; // + $diff->i / 60;
			$remaining = 365 - $total_days;
			$days_prorate = ($remaining / 365);


			$premium = $current_premium['5_total_premium_payable_including_gst'];
			$premium = preg_replace("/[^0-9.]/", "", $premium);
			$premium2 = $thequote;
			$premium_difference = $premium - $premium2;
			$prorate = $days_prorate * $premium_difference;
			$prorate = number_format($prorate, 2, '.', ',');
			$endorse_details = array(
				'date_expires'=>$date_expires,
				'date_endorsement'=>isset($homebiz1['1_date_of_endorsement']) ? $homebiz1['1_date_of_endorsement'] : '',
				'current_premium'=>number_format($premium, 2, '.', ','),
				'endorsed_premium'=>number_format($thequote, 2, '.', ','),
				'days_difference'=>$remaining,
				'days_prorate'=>round($days_prorate, 2),
				'premium_difference'=>$premium_difference,
				'prorate'=>$prorate
			);
			$data['endorse'] = $endorse_details;
		}


		return $data;
	}


	public function homebiz_data($page = 'preview'){
		$data['homebiz0'] = $this->session->userdata('homebiz0');
		$data['homebiz1'] = $this->session->userdata('homebiz1');
		$data['homebiz2'] = $this->session->userdata('homebiz2');
		$data['homebiz3'] = $this->session->userdata('homebiz3');
		$data['homebiz4'] = $this->session->userdata('homebiz4');
		$data['homebiz5'] = $this->session->userdata('homebiz5');
		$data['homebiz6'] = $this->session->userdata('homebiz6');
		$data['homebiz7'] = $this->session->userdata('homebiz7');



		$themenu = array(
			array(
				'name'=>'Your Business',
				'data'=>$data['homebiz1'],
			),
			array(
				'name'=>'Questionnaire',
				'data'=>$data['homebiz2'],
			),
			array(
				'name'=>'Claims',
				'data'=>$data['homebiz2'],
			),
			array(
				'name'=>'Declarations',
				'data'=>$data['homebiz3'],
			),
			array(
				'name'=>'Quote',
				'data'=>$data['homebiz5'],
			),
			array(
				'name'=>'Insured Details',
				'data'=>$data['homebiz6'],
			),
			array(
				'name'=>'Done',
				'data'=>$data['homebiz7'],
			),
		);


		if($page == 'quote'){
			$customertab = array(
				'name'=>'Customer',
				'data'=>$data['homebiz0'],
			);
			array_unshift($themenu , $customertab);
		}



		$data['themenu'] = $themenu;


		$tab2_select = array(
			'Brick/Stone/Concrete',
			'Sandwich Panel - EPS',
			'Iron on Steel',
			'Iron on Wood',
			'Mixed > 75% Brick/Concrete/Iron on Steel',
			'Mixed < 75% Brick/Concrete/Iron on Steel',
			'Fibro',
			'Timber',
			'Other',
		);
		$data['tab2_select'] = $tab2_select;


		$the_states = array(
			'NSW',
			'ACT',
			'QLD',
			'VIC',
			'TAS',
			'SA',
			'WA',
			'NT',
			'Overseas'
		);
		$data['the_states'] = $the_states;

		$tab2_select2 = array(
			'Asbestos',
			'Colorbond Steel',
			'Concrete',
			'Fibro',
			'Iron',
			'Metal',
			'Tiles',
			'Other',
		);
		$data['tab2_select2'] = $tab2_select2;

		$tab2_select3 = array(
			'$500',
			'$750',
			'$1,000',

		);
		$data['tab2_select3'] = $tab2_select3;

		return $data;

	}


	public function homebiz_response($page = 'preview'){
		$data['homebiz0'] = $this->session->userdata('homebiz0');
		$data['homebiz1'] = $this->session->userdata('homebiz1');
		$data['homebiz2'] = $this->session->userdata('homebiz2');
		$data['homebiz3'] = $this->session->userdata('homebiz3');
		$data['homebiz4'] = $this->session->userdata('homebiz4');
		$data['homebiz5'] = $this->session->userdata('homebiz5');
		$data['homebiz6'] = $this->session->userdata('homebiz6');
		$data['homebiz7'] = $this->session->userdata('homebiz7');

		$themenu = array(
			array(
				'name'=>'Your Business',
				'data'=>$data['homebiz1'],
			),
			array(
				'name'=>'Questionnaire',
				'data'=>$data['homebiz2'],
			),
			array(
				'name'=>'Claims',
				'data'=>$data['homebiz3'],
			),
			array(
				'name'=>'Declarations',
				'data'=>$data['homebiz4'],
			),
			array(
				'name'=>'Quote',
				'data'=>$data['homebiz5'],
			),
			array(
				'name'=>'Insured Details',
				'data'=>$data['homebiz6'],
			),
			array(
				'name'=>'Done',
				'data'=>$data['homebiz7'],
			),
		);


		if($page == 'quote'){
			$customertab = array(
				'name'=>'Customer',
				'data'=>$data['homebiz0'],
			);
			array_unshift($themenu , $customertab);
		}



		$data['themenu'] = $themenu;

		return $data;

	}


	public function quote_id_format($id){
		$theid = $this->master->getRecords('biz_referral', array('id'=>$id));
		$value = (empty($theid)) ? array() : $theid[0];
		$id = (empty($id)) ? 0 : $value['id'];
		$yr = substr($value['date_added'], 2, 2);
		$mo = substr($value['date_added'], 5, 2);

		$sprint = sprintf("%04d", $id);

		return 'Q'.$yr.$mo.$sprint;
	}

	public function policy_id_format($id){
		$theid = $this->master->getRecords('biz_referral', array('id'=>$id));
		$value = (empty($theid)) ? array() : $theid[0];
		$id = (empty($id)) ? 0 : $value['id'];
		$yr = substr($value['date_added'], 2, 2);
		$mo = substr($value['date_added'], 5, 2);

		$sprint = sprintf("%04d", $id);

		return 'P'.$yr.$mo.$sprint;
	}

	public function failed_login_noti($email){
		$adminemail = $this->common->admin_email();
		$emailer_file_name = 'failed-login-noti';

		//email settings
		$info_arr = array(
			'to'=>$email,
			'from'=>$adminemail,
			'subject'=>'Greetings from Ebinder',
			'view'=>$emailer_file_name,
		);


		$other_info = array(
			'password'=>'',
			'emailer_file_name'=>$emailer_file_name,
			'name'=>'',
			'agency_name'=>'',
			'user_name'=>'',
			'user_email'=>$email,
			'link'=>'#login'
		);

		$this->emailer->sendmail($info_arr, $other_info);


		$arr['other_info'] = $other_info;
		$arr['info_arr'] = $info_arr;
		return $arr;
	}

	public function format_agency($data = ''){
		$whr = $data;
		unset($whr['db_table']);

		$agency = $this->master->getRecords('agency', $data, '*', array('id'=>'DESC'));

		$arr = array();
		if(!empty($agency)){
			foreach($agency as $r=>$value){
				$value['agency_details'] = (isset($value['agency_details'])) ? unserialize($value['agency_details']) : array();
				$arr[] = $value;
			}
		}

		return $arr;

	}

	public function format_referral($data = ''){
		if(isset($data['db_table'])){
			$whr = $data;
			unset($whr['db_table']);
			$quotes = $this->master->getRecords($data['db_table'], $whr, '*', array('id'=>'DESC'));
		}
		else{
			$quotes = $this->master->getRecords('biz_referral', $data, '*', array('id'=>'DESC'));
		}
		$arr = array();
		$nowtime = date('Y-m-d H:i:s');

		if(!empty($quotes)){
			foreach($quotes as $r=>$value){
				$value['original_value'] = $value;
				$value['content'] = unserialize($value['content']);
				$customer = unserialize($value['customer']);
				$value['customer'] = array(
					'name'=>$value['content']['homebiz1']['1_customer_name'], //(isset($customer[0]['value'])) ? $customer[0]['value'] : $value['content']['homebiz1']['1_customer_name'] ,
					'phone'=>(isset($customer[1]['value'])) ? $customer[1]['value'] : '',
					'email'=>(isset($customer[2]['value'])) ? $customer[2]['value'] : ''
				);

				if(isset($data['db_table'])){
					$value['quote_id'] = $this->quote_id_format($value['referral_id']);
					$value['policy_id'] = $this->policy_id_format($value['referral_id']);
				}
				else{
					$value['quote_id'] = $this->quote_id_format($value['id']);
					$value['policy_id'] = $this->policy_id_format($value['id']);
				}

				$value['endorse_details'] = (isset($value['endorse_details'])) ? unserialize($value['endorse_details']) : array();
				$value['product_name'] = (isset($value['content']['homebiz1']['1_product_id'])) ? $this->product_name($value['content']['homebiz1']['1_product_id']) : $this->product_name();


				//quote Expiry
				$agency_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];
				$quote_expiry = $this->common->db_field_id('agency', 'base_currency', $agency_id); //quote expiry
				$quote_expiry = (is_numeric($quote_expiry)) ? $quote_expiry : '0';
				$date_addedd = $value['date_added']; //date($value['date_added'], strtotime("+".$quote_expiry." days"));

				$date_addedd = new DateTime($value['date_added']);
				$date_addedd->add(new DateInterval('P'.$quote_expiry.'D'));
				$date_addedd2 = $date_addedd->format('Y-m-d H:i:s') . "\n";

				$biz5 = (isset($value['content']['homebiz5'])) ? $value['content']['homebiz5'] : array();
				$value['total_premium_format'] = (isset($biz5['5_total_premium_payable_including_gst'])) ? $biz5['5_total_premium_payable_including_gst'] : '0';
				$value['total_premium'] = preg_replace("/[^0-9.]/", "", $value['total_premium_format']);


				$value['date_addedd'] = $date_addedd2;
				$value['quote_expiry'] = $quote_expiry;
				$value['agency_idd'] = $agency_id;



				$date_a = new DateTime($date_addedd2);
				$date_b = new DateTime($nowtime);

				$interval = $date_b->diff($date_a); //date_diff($date_a, $date_b);

				$value['expiry_timexx'] = $interval; //->format('%d %h:%i');
				$value['expiry_time'] = ($interval->invert == 0) ? $interval->d : ($interval->d * -1); //check if inverted
				$value['expiry_time2'] = $interval->format('%h hr %i min');
				$timeto_display = 0;

				if($value['expiry_time'] == 0){
					$timeto_display =  $value['expiry_time2'];
				}
				else if($value['expiry_time'] > 0){
					$timeto_display = $value['expiry_time'] .' day/s';
				}

				$value['exp_timeto_display'] = $timeto_display;



				//referral and quote status
				$value['unbind_quote_status'] = 'Current';
				if($value['exp_timeto_display'] == 0){
					$value['unbind_quote_status'] = 'Expired';
				}

				$value['referral_status_text'] = 'No Decision';
				if($value['needs_referral'] == 'Y'){
					$value['unbind_quote_status'] = 'Referred';
				}
				if($value['referral_status'] == 'R'){
					$value['referral_status_text'] = 'Rejected';
					$value['unbind_quote_status'] = 'Rejected';
				}
				if($value['referral_status'] == 'A'){
					$value['referral_status_text'] = 'Accepted';

				}





				//calculate expiration of 60days
				$date_bound = new DateTime($value['date_bound']);
				$date_today = new DateTime($nowtime); //'2016-03-01 00:00:00');//

				$diff = $date_bound->diff($date_today);

				$total_days = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $diff->h/24; // + $diff->i / 60;
				$remaining = 365 - $total_days;

				$value['days_remaining'] = round($remaining, 0);
				$value['days_bound'] = round($total_days, 0);


				$days = 365;
				$date = $value['date_bound'];
				$date = strtotime("+".$days." days", strtotime($date));
				$value['date_expires'] = date("Y-m-d H:i:s", $date);

				if($value['bound'] == 'Y'){
					$value['status'] = ($remaining <= 60) ? 'Pending Renewal' : 'Bound';
					if($remaining <= 0){
						$value['status'] = 'Expired';
					}

					if($value['renew_requested'] == 'Y'){
						$value['status'] = 'Pending Customer Review';
					}
					if($value['renew_approved'] == 'Y'){
						$value['status'] = 'Renewal Info Updated';
					}
				}
				else{
					$value['status'] = 'Pending Bind';
				}

				if(isset($data['id'])){
					$arr = $value;
				}
				else{
					$arr[] = $value;
				}
			}
		}

		if(!isset($arr['id'])){
			$arr = $this->common->array_orderby($arr, 'days_remaining', SORT_ASC);
		}

		return $arr;
	}


	public function array_orderby(){
		$args = func_get_args();
		$data = array_shift($args);
		foreach ($args as $n => $field) {
			if (is_string($field)) {
				$tmp = array();
				foreach ($data as $key => $row)
					$tmp[$key] = $row[$field];
				$args[$n] = $tmp;
				}
		}
		$args[] = &$data;
		call_user_func_array('array_multisort', $args);
		return array_pop($args);
	}

	public function renewal_advice($id){
		$whr = array('id'=>$id);
		$format_referral = $this->format_referral($whr);

		$emailer_file_name = 'renewal-advice-to-broker';
		//admin emailer info
		$adminemail = $this->common->admin_email();

		$details = 'Policy Number: '.$format_referral['policy_id'];
		$details .= '<br>Product: ML';
		$details .= '<br>Customer Name: '.$format_referral['customer']['name'];
		$details .= '<br>Expiry Date: '.date_format(date_create($format_referral['date_expires']), 'd M Y').'('.$format_referral['days_remaining'].'d left)';

		$email = $this->db_field_id('customers', 'email', $format_referral['broker_id']);
		$name = $this->db_field_id('customers', 'first_name', $format_referral['broker_id']);
		//email settings
		$info_arr = array(
			'to'=>$email,
			'from'=>$adminemail,
			'subject'=>'Greetings from Ebinder',
			'view'=>$emailer_file_name,
		);

		$other_info = array(
			'password'=>'',
			'emailer_file_name'=>$emailer_file_name,
			'name'=>$name,
			'agency_name'=>$name,
			'user_name'=>$email,
			'user_email'=>$email,
			'details'=>$details,
			'link'=>'login'
		);

		$this->emailer->sendmail($info_arr, $other_info);
		$data['other_info'] = $other_info;
		$data['info_arr'] = $info_arr;

		return $data;

	}

	public function renew_policy($quote_id){
		$user_session = $this->session->all_userdata();
		$nowtime = date('Y-m-d H:i:s');

		$is_customer = $this->session->userdata('customer_quote');

		$whr = array(
			'id'=>$quote_id
		);
		$quote = $this->common->format_referral($whr);
		$homebiz_response = $this->common->homebiz_response();


		$arr = array(
			//'customer'=>serialize($quote['customer']),
			'content'=>serialize($homebiz_response)
		);


		$user_session['for_renewal'] = (isset($user_session['for_renewal'])) ? $user_session['for_renewal'] : 'no';
		if($user_session['for_renewal'] == 'no'){
			$this->master->updateRecord('biz_referral', $arr, array('id'=>$quote_id));
		}
		else{
			$days = 365;
			$date = $quote['date_bound'];
			$date = strtotime("+".$days." days", strtotime($date));
			$date_bound = date("Y-m-d H:i:s", $date);


			$old_val = $quote['original_value'];
			$old_val['referral_id'] = $old_val['id'];
			unset($old_val['id']);

			//endorse
			if($user_session['for_renewal'] == 'endorse'){
				$old_val['endorse_details'] = serialize($homebiz_response['homebiz5']);
			}
			else{
				$arr['renew_requested'] = 'Y';
				if($quote['renew_approved'] == 'Y'){
					$arr['date_renewed'] = $nowtime;
					$arr['date_bound'] = $date_bound;
				}

				if(!empty($is_customer)){
					$arr['renew_approved'] = 'Y';
				}

				if($quote['renew_approved'] == 'Y'){
					$arr['renew_requested'] = 'N';
					$arr['renew_approved'] = 'N';
				}
			}

			$history_id = $this->master->insertRecord('biz_renewal_history',$old_val, true);

			$this->master->updateRecord('biz_referral', $arr, array('id'=>$quote_id));



			//send customer email notification
			$email = $quote['customer']['email'];
			$name = $quote['customer']['name'];
			$emailer_file_name = 'policy-renewal-to-customer';
			//admin emailer info
			$adminemail = $this->common->admin_email();

			//email settings
			$info_arr = array(
				'to'=>$email,
				'from'=>$adminemail,
				'subject'=>'Greetings from Ebinder',
				'view'=>$emailer_file_name,
			);


			$other_info = array(
				'password'=>'',
				'emailer_file_name'=>$emailer_file_name,
				'name'=>$name,
				'agency_name'=>$name,
				'user_name'=>$email,
				'user_email'=>$email,
				'link'=>'landing/renew_approval?id='.$quote_id.'&email='.$email.'&cid='.md5($quote_id).'&name='.md5($name)
			);

			//send only if request
			if($user_session['for_renewal'] == 'yes' && $quote['renew_approved'] == 'N' && empty($is_customer)){
				$this->emailer->sendmail($info_arr, $other_info);
				$arr['other_info'] = $other_info;
				$arr['info_arr'] = $info_arr;
			}

		}

		return $arr;
	}


	public function total_premium_all($list){
		$grand_total = 0;
		if(!empty($list)){
			foreach($list as $r=>$value){
				$biz5 = (isset($value['content']['homebiz5'])) ? $value['content']['homebiz5'] : array();
				$total = (isset($biz5['5_total_premium_payable_including_gst'])) ? $biz5['5_total_premium_payable_including_gst'] : 0;
				$premium = preg_replace("/[^0-9.]/", "", $total);

				$grand_total += $premium;
			}
		}

		return number_format($grand_total, 2, '.', ',');

	}

	public function widget_topbrokers($agency_id = NULL){
		$agency_id = (!empty($agency_id)) ? $agency_id : $this->common->default_cargo();
		$whr = array(
			'agency_id'=>$agency_id,
			'needs_referral'=>'N',
			// 'bound'=>'Y'
		);
		$format_referral = $this->common->format_referral($whr);

		$arr = array();
		$brokers = array();

		if(count($format_referral) > 0){
			$arr = $this->common->array_orderby($format_referral, 'total_premium', SORT_DESC);
			foreach($arr as $r=>$value){
				if(!in_array($value['broker_id'], $brokers) && count($brokers) < 5){
					$brokers[] = $value['broker_id'];
				}
			}
		}

		//fetch top brokers from top policies



		return $brokers;
	}

	public function widget_topcustomers($id = NULL){
		$whr = array(
			'broker_id'=>$id,
			'bound'=>'Y'
		);
		$format_referral = $this->common->format_referral($whr);

		$arr = array();
		$brokers = array();

		if(count($format_referral) > 0){
			$arr = $this->common->array_orderby($format_referral, 'total_premium', SORT_DESC);
			foreach($arr as $r=>$value){
				$customer = $value['customer'];
				$name = (isset($customer['name'])) ? $customer['name'] : 'Unknown';


				if(!in_array($name, $brokers) && count($brokers) < 5){
					$brokers[] = $name;
				}
			}
		}


		return $brokers;
	}

	public function insert_activity_log($log_activity){
		$agency_id = $this->session->userdata('logged_admin_agency');
		$agency_id = (empty($agency_id)) ? 0 : $agency_id;
		$nowtime = date('Y-m-d H:i:s');

		$log_activity['agency_id'] = $agency_id;
		$log_activity['date_added'] = $nowtime;

		$id = $this->master->insertRecord('activity_log', $log_activity, true);
		return $id;

	}

	public function user_session_time($user_data = array()){

		$whr = array('type'=>'login', 'date_added !='=>'0000-00-00 00:00:00');
		$login = $this->master->getRecords('activity_log', $whr, '*', array('id'=>'DESC') , 0, 1);
		$data = 0;



		if(!empty($login) && isset($user_data['id'])){
			$date_stored = $login[0]['date_added'];
			$date_assigned = new DateTime($date_stored);

			$nowtime = date('Y-m-d H:i:s');
			$payout_date = new DateTime($nowtime);

			$diff = $date_assigned->diff($payout_date);

			$interval = date_diff($payout_date,$date_assigned);


			$sess_days = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $diff->h/24;
			$hr_diff = $interval->format('%H:%I:%S'); //$diff->format("%H:%I:%S");

			//logs
			$log_activity = array(
				'name'=>$user_data['name'].' has been logged in for '.$hr_diff, //.' '.$nowtime.' '.$date_stored,
				'type'=>'session_time',
				'details'=>serialize($user_data)
			);
			$data = $this->insert_activity_log($log_activity);
			// if($sess_days < 1){
			//
			// }

		}

		return $data; //$nowtime.' '.$date_stored;
	}

	public function claim_status($status = 'S'){
		$text = 'Submitted';
		if($status == 'P'){
			$text = 'Pending Acceptance';
		}
		if($status == 'U'){
			$text = 'Under Review';
		}
		if($status == 'A'){
			$text = 'approved';
		}
		if($status == 'D'){
			$text = 'Declined';
		}
		if($status == 'R'){
			$text = 'Re-opened';
		}
		return $text;

	}

	public function save_customer(){

		$user_session = $this->session->all_userdata();
		$user_id = (empty($user_session['id'])) ? '0' : $user_session['id'];

		$data = $this->session->userdata('customer_details');



		$id = $data['id'];
		$studio_id = $data['broker_id']; //broker id for customers
		$agency_id = (isset($data['agency_id'])) ? $data['agency_id'] : $this->session->userdata('logged_admin_agency');
		$agency_id = (!empty($agency_id)) ? $agency_id : $this->session->userdata('logged_admin_agency');
		$is_agency = (isset($data['is_agency'])) ? 'yes' : '';
		$access_rights = (isset($data['access_rights'])) ? $data['access_rights'] : '';




		$first = $data['first'];
		$last = $data['last'];
		$email = $data['email'];


		$country_id = $data['country_code'];
		$country_short = $data['country_short'];


		$mobile = $data['mobile'];
		$phone_trim = ltrim($mobile, '0');
		$contact_no = $country_short.$phone_trim;


		$nowtime = date('Y-m-d H:i:s');


		if($id == ''){


			$broker = array(
				'first_name'=>$data['first'],
				'last_name'=>$data['last'],
				'email'=>$data['email'],
				'customer_type'=>$data['ctype'],
				'country_id'=>$country_id,
				'calling_code'=>$country_short,
				'calling_digits'=>$mobile,
				'contact_no'=>$contact_no,
				'location'=>$data['address'],
				'lat'=>$data['lat'],
				'lng'=>$data['lng'],
				'studio_id'=>$studio_id,
				'brokerage_id'=>$data['brokerage_id'],
				'customer_info'=>serialize($data),
				'agency_id'=>$this->common->default_cargo(),
				'status'=>'N',
				'date_added'=>$nowtime
			);



			$customer_id = $this->master->insertRecord('customers', $broker, true);


			// //admin emailer info
			// $adminemail = $this->common->admin_email();
			// $emailer_file_name = 'signup-mail-to-broker';
			// $dbtable = 'customers';
			//
			// //email settings
			// $info_arr = array(
			// 	'to'=>$email,
			// 	'from'=>$adminemail,
			// 	'subject'=>'Welcome to Transit Insurance',
			// 	'view'=>$emailer_file_name,
			// );
			//
			//
			// $other_info = array(
			// 	'password'=>'',
			// 	'emailer_file_name'=>$emailer_file_name,
			// 	'name'=>$first. ' '.$last,
			// 	'agency_name'=>$id,
			// 	'user_name'=>$email,
			// 	'user_email'=>$email,
			// 	'link'=>'landing/verify_user/'.$customer_id.'/'.md5($customer_id).'/'.$dbtable.'/broker'
			// );
			//
			// $this->emailer->sendmail($info_arr, $other_info);

			// $data['other_info'] = $other_info;
			// $data['info_arr'] = $info_arr;


			$success_mess = 'Customer details successfully added.';
			$this->session->set_flashdata('success',$success_mess);
			$data['result'] = 'ok';
			$data['action'] = 'add';


		}

		else{

			$broker = array(
				'first_name'=>$data['first'],
				'last_name'=>$data['last'],
				'country_id'=>$country_id,
				'calling_code'=>$country_short,
				'calling_digits'=>$mobile,
				'contact_no'=>$contact_no,
				'location'=>$data['address'],
				'customer_info'=>serialize($data),
				'lat'=>$data['lat'],
				'lng'=>$data['lng']
			);

			$this->master->updateRecord('customers', $broker, array('id'=>$data['id']));

			$success_mess = 'Customer details successfully updated.';
			$this->session->set_flashdata('success',$success_mess);
			$data['result'] = 'ok';
			$data['action'] = 'update';
		}

		$this->session->unset_userdata('customer_details');



		return $data;



	}


	public function brokercode($id){
		$agentcode = 'B';
		$format_id = sprintf("%03d", $id);
		$agentcode = $agentcode.$format_id;
		return $agentcode;
	}

	public function format_id($id = 0, $prefix = 'T'){
		$agentcode = $prefix;
		$format_id = 1000 + $id;
		$agentcode = $agentcode.$format_id;
		return $agentcode;
	}


	public function customer_format_id($id = 0, $prefix = 'CID'){
		$agentcode = $prefix;
		$format_id = 10000 + $id;
		$agentcode = $agentcode.$format_id;
		return $agentcode;
	}

	public function save_agency_ratings_log($type = 'occupation'){
		$user_id = $this->common->default_cargo();
		$logged_id = $this->session->userdata('id');

		$nowtime = date('Y-m-d H:i:s');

		$arr = array(
			'type'=>$type,
			'agency_id'=>$user_id,
			'user_id'=>$logged_id,
			'date_added'=>$nowtime
		);
		$this->master->insertRecord('agency_log', $arr);
		return $arr;

	}

	public function new_quote_reset(){
		$data[] = 'homebiz0';
		$data[] = 'homebiz1';
		$data[] = 'homebiz2';
		$data[] = 'homebiz3';
		$data[] = 'homebiz4';
		$data[] = 'homebiz5';
		$data[] = 'homebiz6';
		$data[] = 'homebiz7';
		$data[] = 'for_renewal';
		$data[] = 'biz_quote_id';

		foreach ($data as $key => $value) {
			$this->session->unset_userdata($value);
		}

		return true;

	}


	public function product_name($quote_product_id = '3'){
		$quote_product_id = (empty($quote_product_id)) ? '3' : $quote_product_id;
		$product_name = $this->common->db_field_id('q_products', 'product_name', $quote_product_id);
		return $product_name;
	}

	public function twitter_access(){
		$access_keys = array(
		    //
		    'consumer_key'      => 'WqstQuVAdxpolkZjz7n78k1Pv',
		    'consumer_secret'   => 'tfyyR84tIS0h7GkbK6WSlWTsPDpFsdAKJOUwIJRyCfNA90rWNC',

		    //
				'url_callback'      => base_url().'twitter/callback',
		    'url_login'         => base_url().'#login',
		);
		return $access_keys;
	}
}

?>
