    <section class="section section-simple">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 class="section-heading text-center">Quotation For Transits</h1>
    
                </div>

            	<div class="col-lg-10 col-lg-offset-1">
                	<?php
						//get session values
						$buy_inputs = $this->session->userdata('buy_inputs');
						$premium = $this->session->userdata('premium');
						$useremail = $this->session->userdata('email');
						
						$name = '';
						$tele = '';
						$email = '';
						$transitfrom = '';
						$transitto = '';
						$cargocat = '';
						$goods_desc = '';
						$transmethod = '';
						$invoice = '';
						$freight = '';
						$insurance = '';
						if(count($buy_inputs) > 0){
							$name = $buy_inputs[0]['value'];
							$tele = $buy_inputs[1]['value'];
							$email = $buy_inputs[2]['value'];
							$transitfrom = $buy_inputs[3]['value'];
							$transitto = $buy_inputs[4]['value'];
							$cargocat = $buy_inputs[5]['value'];
							$goods_desc = $buy_inputs[6]['value'];
							$transmethod = $buy_inputs[7]['value'];
							$invoice = $buy_inputs[8]['value'];
							$freight = $buy_inputs[9]['value'];
							$insurance = $buy_inputs[10]['value'];
						}
						if($useremail != ''){
							$email = $useremail;
						}
					?>
                	<div class="row">
                        <form id="buy_form" class="form-horizontal form-input-lg">
                            <div class="col-sm-6">
                                  <div class="form-group">
                                    <label for="input1" class="col-sm-6 control-label">Name *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" name="name" id="input1" placeholder="Name" value="<?php echo $name; ?>">
                                    </div>
                                  </div>                
                                  <div class="form-group">
                                    <label for="input2" class="col-sm-6 control-label">Telephone *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" name="tele" id="input2" placeholder="Telephone" value="<?php echo $tele; ?>">
                                    </div>
                                  </div>                
                                  <div class="form-group">
                                    <label for="input3" class="col-sm-6 control-label">Email  *</label>
                                    <div class="col-sm-6">
                                      <input type="email" class="form-control" name="email" id="input3" placeholder="Email" value="<?php echo $email; ?>">
                                    </div>
                                  </div>                
                                  <div class="form-group">
                                    <label for="transitfrom" class="col-sm-6 control-label">Transit From *</label>
                                    <div class="col-sm-6">
                                        <select class="form-control" name="transitfrom" id="transitfrom">
                                          <option value="">Select State</option>
                                          <?
										  foreach($countries as $r=>$value){
											  $price = 0;
											  if(isset($country_prices[$value['country_id']])){
												  $price = $country_prices[$value['country_id']];
											  }
											  
											  echo '<option value="'.$value['country_id'].'" data-price="'.$price.'"';
											  if($transitfrom == $value['country_id']){
												  echo ' selected="selected"';											  	
											  }
											  echo '>'.$value['short_name'].'</option>';
										  			
										  }?>
										  
                                          
                                        </select>

                                    </div>
                                  </div>                
                                  <div class="form-group">
                                    <label for="transitto" class="col-sm-6 control-label">Transit To *</label>
                                    <div class="col-sm-6">

                                        <select class="form-control" name="transitto" id="transitto">
                                          <option value="">Select State</option>
                                          <?
										  foreach($countries as $r=>$value){
											  $price = 0;
											  if(isset($country_prices[$value['country_id']])){
												  $price = $country_prices[$value['country_id']];
											  }
											  
											  echo '<option value="'.$value['country_id'].'" data-price="'.$price.'"';
											  if($transitto == $value['country_id']){
												  echo ' selected="selected"';											  	
											  }
											  echo '>'.$value['short_name'].'</option>';
										  			
										  }?>
                                          
                                        </select>
                                    </div>
                                  </div>                
                                  <div class="form-group">
                                    <label for="cargocat" class="col-sm-6 control-label">Cargo Category *</label>
                                    <div class="col-sm-6">
                                        
                                        <select class="form-control" name="cargocat" id="cargocat">
                                          <option value="" data-price="0" data-bindoption="Y">Select Category</option>
                                          <?php
										  	foreach($cargos as $r=>$value){
												$acargo_price = $value['price'];
												if(count($the_agency) > 0) {
													$acargo_price = 0;
													if(isset($cargo_prices[$value['id']])) {
														$acargo_price = $cargo_prices[$value['id']];
													}
												}
												
												echo '<option value="'.$value['id'].'" data-price="'.$acargo_price.'" data-bindoption="'.$value['bindoption'].'" ';
												if($cargocat == $value['id']){
													echo 'selected="selected"'; 
												}
												
												echo '>'.$value['name'].'</option>';
											}
										  ?>
                                                    
                                        </select>

                                    </div>
                                  </div>                
                                  <div class="form-group">
                                    <label for="goods_desc" class="col-sm-6 control-label">Brief Description of Goods *</label>
                                    <div class="col-sm-6">
                                      <textarea rows="6" class="form-control" name="goods_desc" id="goods_desc"><?php echo nl2br($goods_desc); ?></textarea>
                                    </div>
                                  </div>                
                            </div><!--col-sm-6-->
                        
                            <div class="col-sm-6">
                                  <div class="form-group">
                                    <label for="transmethod" class="col-sm-6 control-label">Transport Method *</label>
                                    <div class="col-sm-6">

                                        <select class="form-control" name="transmethod" id="transmethod">
                                          <option value="">Select Method</option>
                                          
                                          <?php
										  $count = 0;
                                          foreach($transportations as $r){
											  $price = 0;
											  if(isset($transportation_prices[$count])){
											  	$price = $transportation_prices[$count];
											  }
											  echo '<option value="'.$r.'"';
											  if($transmethod == $r) { echo ' selected="selected"'; }
											  
											  echo ' data-price="'.$price.'">'.$r.'</option>';
											  
										  	$count++;
										  } ?>
                                          
                                        </select>
                                        
                                    </div>
                                  </div>                
                                  <div class="form-group">
                                    <label for="invoice" class="col-sm-6 control-label">Invoice Value *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" name="invoice" id="invoice" placeholder="Invoice Value" value="<?php echo $invoice; ?>">
                                    </div>
                                  </div>                
                                  <div class="form-group">
                                    <label for="freight" class="col-sm-6 control-label">Freight, Duty & Sundry Costs</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" name="freight" id="freight" placeholder="Freight, Duty & Sundry Costs" value="<?php echo $freight; ?>">
                                    </div>
                                  </div>                
                                  <div class="form-group">
                                    <label for="insurance" class="col-sm-6 control-label">Value for Insurance *</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" name="insurance" id="insurance" placeholder="Value for Insurance" value="<?php echo $insurance; ?>">
                                    </div>
                                  </div>                
                                  <?php /*?><div class="form-group text-right input-pad-sm">

                                    <div class="col-xs-12 form-inline">
                                      <input type="text" class="form-control" style="width: 80px;" name="add1" id="add1" placeholder="">
                                      <label>+</label>
                                      <input type="text" class="form-control" style="width: 80px;" name="add2" id="add2" placeholder="">
                                      <label>=</label>
                                      <input type="text" class="form-control" style="width: 80px;" name="add2" id="add2" placeholder="">
                                    </div>
                                  	<div class="col-sm-12"><h4 class="text-muted text-right">Kindly Answer for Security Reason</h4></div>
                                  </div>  <?php */?>              
                                  <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-4">
                                      <input type="hidden" name="base_rate" value="<?php echo $the_agency[0]['base_rate']; ?>">
                                      <input type="hidden" name="minimum_premium" value="<?php echo $the_agency[0]['minimum_premium']; ?>">
                                      <button class="btn btn-danger btn-xl btn-block">Continue</button>
                                    </div>
                                  </div>                
                                  <div class="form-group">
                                  	<div class="col-sm-10 col-sm-offset-2">
                                    	<div class="well well-sm">An email will be sent to your nominated address.</div>
                                    </div>
                                  </div>                
                            </div><!--col-sm-6-->
                        </form>                    
                    </div>                
                </div>
            </div>
        </div>

        
    </section>
    
    

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="buyNowModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">
                        
                            <h2>Insurance Premium Cost </h2>
                            <hr class="star-primary">
                            
                            <div class="well text-center">
                            	<h1 style="font-size:75px !important">23</h1>
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary btn-xl btn-block show_form_btn">Buy Now</button>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->    
