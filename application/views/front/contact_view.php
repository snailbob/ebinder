<style>
.form-group {
display: inline-block;
margin-bottom: 20px;
vertical-align: middle;
text-align: left;
}
</style>


<div id="home-what" class="section" >
    <div class="container">
        <h1>Contact Us</h1>
           
           
            <div class="col-md-8 column col-md-offset-2"  id="contact" style=" background: rgba(255,255,255,.5); padding:20px; padding-top: 40px; margin-top: 20px; border-radius: 5px;">
            

                <form id="contact_form">
                  <div class="col-lg-6 form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-user"></i></span>
                      <input type="text" class="form-control" required id="inputname" name="inputname" minlength="3" placeholder="Your Name*">
                    
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-6 -->
                  
                  <div class="col-lg-6 form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-globe"></i></span>

                            <select class="form-control" name="inputCountry" id="inputCountry" style="width: 100%;" required>
                            <option value="">Country*</option>

							<?php
								
								foreach($countries as $c=>$cou){
									echo '<option value="'.$cou['short_name'].'">'.$cou['short_name'].'</option>';
								}
							?>
                            </select>                    

                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-6 -->
                  <div class="clearfix"></div>
                  
                  <div class="col-lg-6 form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-building"></i></span>
                    <input type="text" class="form-control" id="inputwebsite" name="inputwebsite" placeholder="Company*" required>
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-6 -->

                  
                  <div class="col-lg-6 form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                    <input type="email" class="form-control" id="inputemail" name="inputemail" placeholder="Email Address*" required >
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-6 -->


                  <div class="col-lg-12 form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-link"></i></span>
                    <input type="text" class="form-control" id="inputsubject" name="inputsubject" placeholder="Subject">
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-6 -->
                  <div class="col-lg-12 form-group">
                    <div class="input-group">
                      <span class="input-group-addon "><i class="fa fa-envelope"></i></span>
                    <textarea class="form-control" rows="4" id="inputmessage" name="inputmessage" placeholder="Your Message*" required minlength="3"></textarea>
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-6 --><br>
    
                  <div class="col-lg-12 form-group">
                    <div class="input-group">
                             <button type="submit"  id="form_submit" class="btn btn-primary">Send Message</button> * Required Fields
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-6 --><br>
                </form>

                
            </div>
            
            <div class="col-md-8 column col-md-offset-2"  id="contact" style=" background: rgba(255,255,255,.5); padding:20px; margin-top: 20px; border-radius: 5px;">
    			<div style=" text-align:left; margin-top: 20px;">
                	<div class="col-md-6 col-sm-12">
                    <p class="lead">Suite 3, Level 8, 179 Queen St<br />
                    Melbourne, Victoria<br />
                    Australia 3000</p>
                    </div>
                	<div class="col-md-6 col-sm-12 hidden-sm hidden-xs">
                    <img class="pull-right" src="<?php echo base_url()?>assets/frontpage/images/staticmap.png" width="70%" height="70%" alt="Map"/>
                	</div>
                	<div class="col-md-6 col-sm-12 visible-sm visible-xs">
                    <img src="<?php echo base_url()?>assets/frontpage/images/staticmap.png" width="70%" height="70%" alt="Map"/>
                	</div>
                </div>
                <div style="clear: both; height: 30px;"></div>
            </div>

    </div> <!-- /container -->
</div> <!-- /homepage-what -->
