<div class="col-md-6 col-md-offset-3 text-center" style="padding-top: 150px;">
    	<img src="<?php echo base_url().'assets/frontpage/corporate/images/newimg/heart-thanks.png' ?>" />
        
        <h1>Thanks for signing up!</h1>
        <p class="lead">
        	We've sent you an email with a link to verify your account.<br />
Don't see an email? Check your junk/spam folder.
        
        </p>
    
</div>
