    <section class="bg-primary no-padding-bottom">
        <div class="container">
            <div class="col-xs-12">
                <h2 class="section-heading text-center">Forgot Password</h2>
                <hr class="light" />
                <p class="text-center">Enter your email below and we will send you instructions on how to reset your password</p>
            </div>
        </div>
    </section>
    <section class="login_section">

        <div class="container">
            
        
            <div class="omb_login">

                <div class="row omb_row-sm-offset-3">
                    <div class="col-xs-12 col-sm-6">	
                        <form class="omb_loginForm" id="forgot_form">
                        	<div class="form-group">
                                <div class="input-group input-group-lg">
                                    <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                    <input type="email" class="form-control" name="email" placeholder="Email Address">
                                </div>
                            </div>

                        	<div class="form-group">
                                <button class="btn btn-lg btn-primary btn-block" type="submit">Send</button>
                            </div>
        
                        </form>
                    </div>
                </div>
            </div>
        
        
        
</div>

        
    </section>
