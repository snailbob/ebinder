<style>
.form-group {
display: inline-block;
margin-bottom: 20px;
vertical-align: middle;
text-align: left;
}
</style>


<div id="faq-what" class="section" >
    <div class="container">
        <h1>FAQs</h1>


            <div class="col-md-8 column col-md-offset-2" >


<div class="panel-group text-left" id="accordion" role="tablist" aria-multiselectable="true">

  <?php
    $faqcount = 1;
    foreach($faq as $r=>$value){ ?>



  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="heading<?php echo $faqcount?>">
      <h3 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $faqcount?>" aria-expanded="<?php echo 'false'; //($faqcount == 1) ? 'true' : 'false' ?>" aria-controls="collapse<?php echo $faqcount?>">
          <?php echo $value['question'] ?>
          <i class="fa fa-caret <?php echo 'fa-caret-right'; //($faqcount == 1) ? 'fa-caret-down' : 'fa-caret-right' ?> text-info pull-right"></i>
        </a>
      </h3>
    </div>
    <div id="collapse<?php echo $faqcount?>" class="panel-collapse collapse <?php //echo ($faqcount == 1) ? 'in' : '' ?>" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
        <p><?php echo nl2br($value['answer']) ?></p>

      </div>
    </div>
  </div>
    <?php $faqcount++; } ?>

</div>




            </div>

    </div> <!-- /container -->
</div> <!-- /homepage-what -->
