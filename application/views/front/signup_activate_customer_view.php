<div id="home-whatx" class="section login-section">
    <div class="container">


            <div class="row row-signup">
                <div class="col-sm-6 col-sm-offset-3">
                    <h1>Account Activation</h1>
                    <hr class="star-primary">
                    <p class="lead text-center">Yay! Almost there!</p>
                    <div class="alert alert-success" style="display: none;">
                        <button type="button" class="close" onclick="$(this).closest('.alert').hide();" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="my-text"></span>
                    </div>



                </div>

               	<div class="col-sm-12">
                    <div class="row text-left">
                        <div class="col-xs-12 col-md-8 col-md-offset-2">

                            <form class="omb_loginForm activation_form" id="activation2_form">
                                <input type="hidden" class="form-control" name="id" value="<?php echo $user['id'] ?>">
                                <input type="hidden" class="form-control" name="type" value="<?php echo (isset($_GET['type'])) ? $_GET['type'] : 'broker'; ?>">
                                <input type="hidden" class="form-control" name="google_id">
                                <input type="hidden" class="form-control" name="facebook_id">
                                <input type="hidden" class="form-control refer_from" name="refer_from" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; } else { echo $this->session->userdata('id'); }?>">

                                <input type="hidden" class="form-control" name="google_img">
                                <input type="hidden" class="form-control" name="fb_img">
                                <input type="hidden" class="form-control" id="social_type" name="social_type">
                                <input type="hidden" class="form-control" id="social_site" name="social_site">


                                <div class="form-group hidden">
                                    <div class="dropdown dropdown-form dropdown-signup">
                                      <button class="btn btn-primary btn-block btn-outline"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-chevron-down fa-fw pull-right"></i>
                                        Sign up as <span class="u_type"> - Agency</span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                        <li><a href="#" data-type="Customer">Customer</a></li>
                                      </ul>
                                    </div>

                                    <input type="hidden" name="utype" value="Customer" />
                                </div>

                                <div class="form-group studio_only hidden">


                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" class="form-control" name="name" placeholder="Agency Name">
                                    </div>
                                </div>


                                <div class="row notfor_studio">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                <input type="text" class="form-control" name="first_name" placeholder="First Name" value="<?php echo $user['first_name']?>">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="<?php echo $user['last_name']?>">
                                            </div>
                                        </div>

                                    </div>

                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-building-o fa-fw"></i></span>
                                                <input type="text" class="form-control the_company_field" name="company" placeholder="Company Name" value="<?php echo $user['business_name']?>">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                                <input  type="password" class="form-control" name="password" id="pw" placeholder="Password">
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="pwstrength_viewport_progress"></div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="well well-sm">
                                                        Use a mix of letters, numbers, and symbols in your password
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    </div>

                                </div>


                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="form-group">
                                          <div class="checkbox">
                                            <label style="color: #666">
                                              <input type="checkbox" name="terms" required="required"> I agree with the <a href="#" class="terms_btn" data-field="terms2">common terms and conditions</a>.
                                            </label>
                                          </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="form-group">
                                            <button class="btn btn-warning btn-block" type="submit">Create My Portal</button>
                                        </div>
                                    </div>

                                </div>


                                <?php /*?><div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-globe
        fa-fw"></i></span>
                                        <input type="text" class="form-control the_domain_field" name="domain" placeholder="Company Domain">
                                    </div>
                                    <p class="text-muted the_domain_text hidden"></p>
                                </div>


                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                        <input  type="password" class="form-control" name="password" placeholder="Password">
                                    </div>
                                </div><?php */?>


                            </form>
                        </div>
                    </div>


                </div>

            </div>



    </div> <!-- /container -->
</div> <!-- /homepage-what -->
