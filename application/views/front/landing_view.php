
    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1 class="text-pink">MEMORIES CREATED…ONE STEP AT A TIME</h1>
                <h3 class="header-subtext text-primary">Booking a dance class just got easier.</h3>                
                                
                                
            </div>
        </div>
    </header>
    
    <section class="bg-black-trans search-section">
    	<div class="container">

            <div class="row ">
                <div class="col-md-8 col-md-offset-2 front-form">
                    <form class="" id="search_class_form">
                        <?php /*?><div class="input-group input-group-lg">
                          <input type="text" class="form-control geocomplete" placeholder="Where do you want to dance?" name="location">
                          <a href="#" class="input-group-addon bg-red" id="basic-addon2">Get Started</a>
                        </div>
                        
                        <div class="row sm-gutter">
                        	<div class="col-lg-6 col-xs-12">
                            	<div class="form-group">
                                  <input type="text" class="form-control input-lg geocomplete" placeholder="Where do you want to dance?" name="location">
                                </div>
                            </div>
                        	<div class="col-lg-3 col-xs-6">
                            	<div class="form-group">
                                  <a href="#" class="btn btn-red btn-lg btn-block">Find Classes</a>
                                </div>
                            
                            </div>
                        	<div class="col-lg-3 col-xs-6">
                            	<div class="form-group">
                                  <button class="btn btn-red btn-lg btn-block">Browse Studios</button>
                                </div>
                            </div>
                        
                        </div>  <?php */?>                                  
                        <div class="form-group">
                          <a href="<?php echo base_url().'buy'?>" class="btn btn-red btn-lg btn-block">Buy Transit Insurance</a>
                          <?php /*?><button class="btn btn-red btn-lg btn-block">Buy Transit Insurance</button><?php */?>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="cover-section" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading  no-padding-top wow fadeIn">WELCOME TO TRANSIT INSURANCE</h2>
                    <h4 class="wow fadeIn">Finding and booking dance classes is now as easy as 5, 6, 7, 8.</h4>
                    <a href="#" class="hidden btn btn-default btn-xl wow tada">Get Started!</a>
                </div>
            </div>
        </div>
    </section>

    <section class="cover-section" id="services">
        <div class="container">
            <div class="row no-gutter">
                <div class="col-md-6 col-lg-8 text-center"></div>
                <div class="col-md-6 col-lg-4 text-center">
                    <h2 class="section-heading wow fadeIn">ALL EYES ARE ON YOU…MAKE AN IMPRESSION</h2>
                    <h4 class="wow fadeIn">Find dance classes for special occasions.</h4>
                </div>
            </div>
        </div>
    </section>

    <section class="cover-section" id="inspire">
        <div class="container">
            <div class="row no-gutter">
                <div class="col-lg-6 col-lg-offset-3 text-center">
                    <h2 class="section-heading wow fadeIn">BE INSPIRED</h2>
                    <h4 class="wow fadeIn">Dance offers a creative outlet to express your emotions when words don't do them justice - book a dance class today.</h4>
                </div>
            </div>
        </div>
    </section>
