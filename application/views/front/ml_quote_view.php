

    <section class="bg-black-trans search-section">
    	<div class="container">

			<?php
				if($this->session->userdata('customer_mobile_ok') == '' && $this->session->userdata('logged_admin') == ''){
					?>

					<form id="customer_mob_form">
	                    <div class="well" style="margin-top: 50px;">
							<h3>
								Customer: <?php echo $customer[0]['value'] ?>
							</h3>

							<p class="lead">
								To ensure security, we need to send SMS to your number for verification.
							</p>

							<?php
								$customer_id = (isset($content['homebiz1']['1_customer_id'])) ? $content['homebiz1']['1_customer_id'] : '';
								$user = (!empty($customer_id)) ? $this->common->customer_format($customer_id) : array(); //brokerage_brokers($brokerage_id);

								$mobile = (isset($user['contact_no'])) ? $user['contact_no'] : '639369830471';
								$mob_format = '*******'.substr($mobile, -3);


							?>
							<p>Send verification code to your mobile number: <?php echo $mob_format; ?></p>

                        	<button class="btn btn-primary send_code_tocustomer" data-number="<?php echo $mobile; ?>" type="button">Send Code Now</button>



	                        <div class="row row-phonecode hidden">
	                        	<br><br>
	                            <div class="col-md-3">
	                                <div class="form-group">
	                                	<label>
	                                    	4 Digit Code
	                                        <i class="fa fa-times-circle-o text-danger hidden" data-toggle="tooltip" data-title="Code not match"></i>
	                                        <i class="fa fa-check-circle-o text-success hidden" data-toggle="tooltip" data-title="SMS Verified"></i>
	                                    </label>
	                                    <input type="text" class="form-control input-number" name="phonecode[]" value="" required="required">
	                                </div>
	                            </div>

	                            <div class="col-md-3">
	                                <div class="form-group">
	                                	<label>&nbsp;</label>
	                                    <input type="text" class="form-control input-number" name="phonecode[]" value="" required="required">
	                                </div>
	                            </div>

	                            <div class="col-md-3">
	                                <div class="form-group">
	                                	<label>&nbsp;</label>
	                                    <input type="text" class="form-control input-number" name="phonecode[]" value="" required="required">
	                                </div>
	                            </div>

	                            <div class="col-md-3">
	                                <div class="form-group">
	                                	<label>&nbsp;</label>
	                                    <input type="text" class="form-control input-number" name="phonecode[]" value="" required="required">
	                                </div>
	                            </div>


	                            <!-- <div class="col-md-12">
	                            			                        <div class="form-group">
	                            			                        	<button class="btn btn-primary" type="submit">Submit</button>
	                            			                        </div>
	                            </div>
	                             -->


	                        </div>



	                    </div>

					</form>
					<?php
				}
				else{
			?>

            <div class="row ">
                <div class="col-md-8 col-md-offset-2 front-form">

                    <h2>
                    	<a href="#" class="btn btn-info customer_approves pull-right" data-id="<?php echo $id ?>">Approve</a>
                    	<a href="<?php echo base_url().'landing/update_quote_info/'.$_GET['id'] ?>" class="btn btn-warning pull-right" data-id="<?php echo $id ?>" style="margin-right: 5px">Update</a>
                        Customer: <?php echo $customer[0]['value'] ?>

                    </h2>



<?php
	foreach($content['themenu'] as $r=>$value){
		if(!empty($value['data']) && ($value['name'] != 'Done' && $value['name'] != 'Quote')){ //
			echo '<hr>';

			echo '<h3>'.$value['name'].'</h3>';
			foreach($value['data'] as $vr=>$vdata){
				$strip_first = explode('_', $vr);
				$the_question = '';

				$i = 1;
				if($vr == 'occupation_find' || $vr == 'occupation_id'){
					$i = 0;
				}
				for($i; $i < count($strip_first); $i++ ){
					$the_question .= $strip_first[$i].' ';
				}
				$the_question = str_replace ('[]', '', $the_question);
				$the_question = ucfirst($the_question);

				if($vr != 'user_id' && $vr != 'customer_type'){

					if(is_array($vdata)){
						echo '<p>'.$the_question.' :<br>';
						foreach($vdata  as $vdr=>$vdv){
							echo $the_states[$vdr].': '.$vdv.'%<br>';
						}
						echo '</p>';
					}
					else{
						echo '<p>'.$the_question.' : <strong>'.$vdata.'</strong></p>';
					}

				}

			}
		}
	}
?>



                </div>
            </div>



			<?php } ?>



        </div>
    </section>
