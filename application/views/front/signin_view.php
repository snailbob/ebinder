<div id="home-whatx" class="section login-section">
    <div class="container">

        <div class="row">
			<div class="col-xs-12 col-sm-6 col-sm-offset-3 col-lg-4 col-lg-offset-4">
                <h1>Login to your account</h1>
                <hr class="star-primary">
                <p class="lead text-center">Doesn't have an account? <a href="<?php echo base_url().'signup'?>" class="login_btnx">Sign Up</a><br /><br /></p>
                
    
                    <div class="row">
                        <div class="col-xs-12">	
                            <div class="row social-btns">
                                <div class="col-sm-6">
                                    <div class="form-groupx">
                                        <button type="button" class="btn btn-default btn-block" id="customBtn"><i class="fa fa-google-plus text-danger"></i> Google+</button>
                                    </div>
                                </div>
                                
                                <div class="col-sm-6">
                                    <div class="form-groupx">
                                        <button type="button" class="btn btn-primary btn-block" id="MyLinkedInButton"><i class="fa fa-linkedin"></i> LinkedIn</button>
                                    </div>
                                </div> 
                            </div>
                        
                            <hr class="star-primary">

                            <form class="omb_loginForm" id="login_form">
                                <input type="hidden" class="form-control" name="google_id">
                                <input type="hidden" class="form-control" name="facebook_id">
                                <input type="hidden" class="form-control refer_from" name="refer_from" value="">
                                <input type="hidden" class="form-control" name="from_buyform" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; }?>">

                                <div class="form-group hidden">
                                    <div class="dropdown dropdown-form dropdown-login">
                                      <button class="btn btn-primary btn-block btn-outline"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-chevron-down fa-fw pull-right" style="margin-right: 10px;"></i>
                                        Log In as <span class="u_type"> - Agency</span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                        <li><a href="#" data-type="Customer">Customer</a></li>
                                      </ul>
                                    </div>
                                    
                                    <input type="hidden" name="utype" value="Customer" />
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                        <input type="email" class="form-control" name="email" placeholder="Email Address">
                                    </div>
                                </div>
              
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                        <input  type="password" class="form-control" name="password" placeholder="Password">
                                    </div>
                                </div>
              
                                <div class="form-group hidden">
                                    <label>Log In as</label>
                                    <input type="checkbox" name="usertype" checked>
                                </div>
              
                                <div class="form-group">
                                    <button class="btn btn-primary btn-block" type="submit">Log In</button>
                                </div>
    
            
                            </form>

                            <p class="omb_forgotPwd">
                                <a href="#" class="forgot_btn">Forgot password?</a>
                            </p>
                        </div>
                    </div>	    	
                </div>
                            
                

				<?php /*?><form class="form text-left" id="validate_domain_form">
					<div class="signup-form" style="margin-top: 50px">
						<div class="row">
							<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                            	<div class="form-group">
                                    <label>Company Domain</label>
                                    <input class="form-control input-lg" type="text" name="domain" placeholder="company domain">
                                
                                </div>
							</div>
							<div class="col-sm-4 col-lg-4">
                            	<div class="form-group">
                                    <label>&nbsp;</label><br />

                                    <p class="form-control-static" style="margin-top: 10px; text-transform: uppercase">.ebinder.com.au</p>
                                </div>
        
					
                    		</div>
						</div>
						<div class="row mt20">
							<div class="col-lg-12 text-center-xs">
                            	<div class="form-group">
                                    <button type="submit" class="btn btn-lg btn-warning btn-block">Log In</button>
                                </div>
        
        
							</div>	
						</div>
					</div>
				</form><?php */?>
			</div>
		</div>

    </div> <!-- /container -->
</div> <!-- /homepage-what -->


