<div id="home-whatx" class="section login-section">
    <div class="container">


            <div class="row row-signup">
                <div class="col-sm-6 col-sm-offset-3">
                    <h1>Sign up </h1>
                    <hr class="star-primary">
                    <p class="lead text-center">Already have an account? <a href="<?php echo base_url().'login'?>" class="login_btnx">Log In</a><br /><br /></p>
                    <div class="alert alert-success" style="display: none;">
                        <button type="button" class="close" onclick="$(this).closest('.alert').hide();" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="my-text"></span>
                    </div>
                
                    <div class="row social-btns">
                        <div class="col-sm-6">
                            <div class="form-groupx">
                                <button id="customBtn" type="button" class="btn btn-default btn-block"><i class="fa fa-google-plus text-danger"></i> Google+</button>
                            </div>
                        </div>
                        
                        <div class="col-sm-6">
                            <div class="form-groupx">
                                <button type="button" class="btn btn-primary btn-block" id="MyLinkedInButton"><i class="fa fa-linkedin"></i> LinkedIn</button>
                            </div>
                        </div> 
                    </div>
                    <hr class="star-primary">
                
                
                </div>
                
               	<div class="col-sm-12">
                    <div class="row text-left">
                        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">	
         
                            <form class="omb_loginForm" id="signup_form">
                                <input type="hidden" class="form-control" name="google_id">
                                <input type="hidden" class="form-control" name="facebook_id">
                                <input type="hidden" class="form-control refer_from" name="refer_from" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; } else { echo $this->session->userdata('id'); }?>">
                                
                                <input type="hidden" class="form-control" name="google_img">
                                <input type="hidden" class="form-control" name="fb_img">
                                <input type="hidden" class="form-control" id="social_type" name="social_type">
                                <input type="hidden" class="form-control" id="social_site" name="social_site">
                                
        
                                <div class="form-group hidden">
                                    <div class="dropdown dropdown-form dropdown-signup">
                                      <button class="btn btn-primary btn-block btn-outline"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-chevron-down fa-fw pull-right"></i>
                                        Sign up as <span class="u_type"> - Agency</span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li class="active"><a href="#" data-type="Agency">Agency</a></li>
        
                                        <li><a href="#" data-type="Customer">Customer</a></li>
                                      </ul>
                                    </div>
                                    
                                    <input type="hidden" name="utype" value="Customer" />
                                </div>
                                
                                <div class="form-group studio_only hidden">
                                
                                
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" class="form-control" name="name" placeholder="Agency Name">
                                    </div>
                                </div>
                                
        
                                <div class="row notfor_studio">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                <input type="text" class="form-control" name="first_name" placeholder="First Name" value="<?php echo (isset($social['first_name'])) ? $social['first_name'] : ''; ?>">
                                            </div>
                                        </div>
        
                                    </div>
                                
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="<?php echo (isset($social['last_name'])) ? $social['last_name'] : ''; ?>">
                                            </div>
                                        </div>
                                    
                                    </div>
                                
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-building-o fa-fw"></i></span>
                                                <input type="text" class="form-control the_company_field" name="company" placeholder="Company Name">
                                            </div>
                                        </div>
                                    
                                    </div>
                                
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                                <input type="email" class="form-control" name="email" placeholder="Email Address" value="<?php echo (isset($social['email'])) ? $social['email'] : ''; ?>">
                                            </div>
                                        </div>
                               
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-phone
                fa-fw"></i></span>
                                                <input type="text" class="form-control" name="phone" placeholder="Phone">
                                            </div>
                                        </div>
                              
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <small>By clicking on 'Get Started', I agree to the <a href="#" class="terms_btn">Terms of use</a> and <a href="#" class="privacy_btn">Privacy Policy</a>. </small>
                                        </div>
                               
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <button class="btn btn-warning btn-block" type="submit">Get Started</button>
                                        </div>
                                    </div>
                                    
                                </div>
              
              
                                <?php /*?><div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-globe
        fa-fw"></i></span>
                                        <input type="text" class="form-control the_domain_field" name="domain" placeholder="Company Domain">
                                    </div>
                                    <p class="text-muted the_domain_text hidden"></p>
                                </div>
                                
              
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                        <input  type="password" class="form-control" name="password" placeholder="Password">
                                    </div>
                                </div><?php */?>
        
            
                            </form>
                        </div>
                    </div>
                
                
                </div> 
                
            </div>



    </div> <!-- /container -->
</div> <!-- /homepage-what -->
