    <section class="section section-simple no-padding-bottom">
        <div class="container">  

            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <h1 class="section-heading text-center"><?php echo $title; ?></h1>
                    <p class="text-center"><a href="#" class="btn btn-primary confirm_quote_btn <?php echo ($booking[0]['status'] != 'S') ? 'disabled' : '' ?>" data-id="<?php echo $id ?>" data-uid="<?php echo $user_id?>"><i class="fa fa-file-text"></i> <?php echo ($booking[0]['status'] != 'S') ? 'Confirmed' : 'Confirm' ?></a></p>
                    <hr class="dark"/>

                    <object data="<?php echo $quote_url ?>" type="application/pdf" style="width:100%; height: 350px;">
                        <embed src="<?php echo $quote_url ?>" type="application/pdf" />
                    </object>                                            
                    <div class="alert"><a href="<?php echo $quote_url ?>" target="_blank">Preview</a> quote in PDF. </div>
                </div>
            </div>
      
        </div>

        
    </section>
