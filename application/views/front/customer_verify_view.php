    <section class="section section-simple no-padding-bottom">
        <div class="container">

    

            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
                    <h1 class="section-heading text-center"><?php echo $title; ?></h1>
                    <p class="text-center">Hi <?php echo $this->common->customer_name($agency[0]['id']) ?>, please assign a password to complete your registration.</p>
                    <hr class="dark"/>

                    <form class="" id="customer_password_form">

                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="pw" id="pw" class="form-control" />
                            <input type="hidden" name="id" class="form-control" value="<?php echo $agency[0]['id'] ?>" />
                        </div>


                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="pwstrength_viewport_progress"></div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="well well-sm">
                                        Use a mix of letters, numbers, and symbols in your password
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label>Repeat Password</label>
                            <input type="password" name="pw2" class="form-control" />
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                                
                </div>
            </div>
      
      
    
        </div>

        
    </section>
