<div id="home-whatx" class="section login-section">
    <div class="container">


            <div class="row row-signup">
                <div class="col-md-8 col-md-offset-2">
                    <h1>Account Activation</h1>
                    <hr class="star-primary">
                    <p class="lead text-center">
						<?php // echo ($user['agency_id'] == '0') ? 'Yay! Almost there! Verify SMS and add your subdomain. Your subdomain is the unique url your team will use to login to your ebinder account.' : 'Yay! Almost there! Verify SMS and assign your password to complete profile.' ?>
                    </p>
                    <div class="alert alert-success" style="display: none;">
                        <button type="button" class="close" onclick="$(this).closest('.alert').hide();" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <span class="my-text"></span>
                    </div>

                    <?php
                    $success = $this->session->flashdata('success');
                    if(!empty($success)) { ?>
                  		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; ?>" style="margin-bottom: 15px;">
                      	<?php echo $success ?>
                      </div>
                    <?php } ?>


                </div>

               	<div class="col-sm-12">
                    <div class="row text-left">
                        <div class="col-xs-12 col-md-8 col-md-offset-2">

                            <form id="sms_verify_form" novalidate="novalidate" <?php echo ($user['sms_verified'] == 'Y') ? 'style="display: none"' : '' ?>>
                                <input type="hidden" class="form-control" name="id" value="<?php echo $user['id'] ?>">
                                <div class="row <?php echo (isset($user['country_id']) && $user['country_id'] != '') ? 'hidden' : ''; ?>">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <select class="form-control" name="country_id" required="required"data-live-search="true" data-size="auto">
                                                <option value="">Select Location Code</option>
                                                <?php foreach($countries as $r=>$value) {
                                                    echo '<option value="'.$value['country_id'].'"';
                                                    if(isset($user['country_id'])){
                                                        if($user['country_id'] == $value['country_id']){
                                                            echo ' selected="selected"';
                                                        }
														else{
															if($country_code == $value['iso2']){
																echo ' selected="selected"';
															}
														}
                                                    }
                                                    echo '>'.$value['short_name_iso2'].'</option>';
                                                }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">

                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-phone
                fa-fw"></i></span>
                                                <input type="text" class="form-control" name="phone" placeholder="Mobile Number" value="<?php echo $user['calling_digits']?>" required="required">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <p class=" <?php
                                 $agency_name = $this->common->db_field_id('agency', 'name', $user['agency_id']);
                                 echo (isset($user['country_id']) && $user['country_id'] == '') ? 'hidden' : ''; ?>">
                                    To make sure your Ebinder account is secure, we have to verify your identity. Click on the link below and we’ll text you the 4 digital code to mobile number +<?php echo '***********'.substr($user['contact_no'], -3) ?> provided by <?php echo $agency_name; ?>.

                                </p>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <button class="btn btn-warning" type="submit">Send 4 Digit Code</button>
                                        </div>
                                    </div>

                                </div>

                                <div class="row row-phonecode" <?php // echo ($user['sms_verified'] == 'N') ? 'style="display: none"' : '' ?>>
                                    <div class="col-md-5">
                                        <label>
                                          Enter 4 digit code

                                        </label>
                                        <div class="row">

                                          <div class="col-md-3">
                                              <div class="form-group">
                                                  <input type="text" class="form-control input-number" name="phonecode[]" value="<?php echo (empty($user['sms_code']) || $user['sms_verified'] == 'N') ? '' : substr($user['sms_code'], 0, 1) ?>">
                                              </div>
                                          </div>

                                          <div class="col-md-3">
                                              <div class="form-group">
                                                  <input type="text" class="form-control input-number" name="phonecode[]" value="<?php echo (empty($user['sms_code']) || $user['sms_verified'] == 'N') ? '' : substr($user['sms_code'], 1, 1) ?>">
                                              </div>
                                          </div>

                                          <div class="col-md-3">
                                              <div class="form-group">
                                                  <input type="text" class="form-control input-number" name="phonecode[]" value="<?php echo (empty($user['sms_code']) || $user['sms_verified'] == 'N') ? '' : substr($user['sms_code'], 2, 1) ?>">
                                              </div>
                                          </div>

                                          <div class="col-md-3">
                                              <div class="form-group">
                                                  <input type="text" class="form-control input-number" name="phonecode[]" value="<?php echo (empty($user['sms_code']) || $user['sms_verified'] == 'N') ? '' : substr($user['sms_code'], 3, 1) ?>">
                                              </div>
                                          </div>

                                        </div>

                                    </div>


                                    <div class="col-md-4">
                                        <label>
                                          &nbsp;
                                        </label><br>
                                        <span class="form-control-feedback">
                                          <i class="fa fa-times-circle-o text-danger hidden" data-toggle="tooltip" data-title="Code not match"></i>
                                          <i class="fa fa-check text-success <?php echo ($user['sms_verified'] == 'N') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="SMS Verified"></i>
                                        </span>

                                    </div>


                                </div>


                        	</form>


                          <div class="signup-step2" <?php  echo ($user['sms_verified'] == 'N') ? 'style="display: none"' : '' ?>>



                            <p>To access Ebinder, you can either link your Google+ or LinkedIn account, or alternatively use your email and assign a new password below. </p>
                            <div class="row social-btns">
                                <div class="col-sm-4">
                                    <div class="form-groupx">
                                        <button id="customBtn" type="button" class="btn btn-default btn-block"><i class="fa fa-google-plus text-danger"></i> Connect Google+</button>

                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-groupx">
                                        <button id="MyLinkedInButton" type="button" class="btn btn-primary btn-linkedin btn-block"><i class="fa fa-linkedin"></i> Connect LinkedIn</button>
                                    </div>
                                </div>

                                <div class="col-sm-4">
                                    <div class="form-groupx">
                                        <a href="<?php echo base_url().'twitter?id='.$_GET['id'] ?>" class="btn btn-primary btn-twitter btn-block"><i class="fa fa-twitter"></i> Connect LinkedIn</a>
                                    </div>
                                </div>
                            </div>
                            <p></p>
                            <p class="text-center" style="margin-bottom: -30px">
                            	<span  style="background:#fff; padding: 0 5px;">or</span>
                            </p>
                            <hr />

                            <form class="omb_loginForm activation_form" id="activation_form" novalidate="novalidate">
                                <input type="hidden" class="form-control" name="id" value="<?php echo $user['id'] ?>">
                                <input type="hidden" class="form-control" name="agency_id" value="<?php echo $user['agency_id'] ?>">
                                <input type="hidden" class="form-control" name="google_id">
                                <input type="hidden" class="form-control" name="facebook_id">
                                <input type="hidden" class="form-control refer_from" name="refer_from" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; } else { echo $this->session->userdata('id'); }?>">

                                <input type="hidden" class="form-control" name="google_img">
                                <input type="hidden" class="form-control" name="fb_img">
                                <input type="hidden" class="form-control" id="social_type" name="social_type">
                                <input type="hidden" class="form-control" id="social_site" name="social_site">


                                <div class="form-group hidden">
                                    <div class="dropdown dropdown-form dropdown-signup">
                                      <button class="btn btn-primary btn-block btn-outline"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fa fa-chevron-down fa-fw pull-right"></i>
                                        Sign up as <span class="u_type"> - Agency</span>
                                      </button>
                                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                                        <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                        <li><a href="#" data-type="Customer">Customer</a></li>
                                      </ul>
                                    </div>

                                    <input type="hidden" name="utype" value="Customer" />
                                </div>

                                <div class="form-group studio_only hidden">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                        <input type="text" class="form-control" name="name" placeholder="Agency Name">
                                    </div>
                                </div>


                                <div class="row notfor_studio">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                <input type="text" class="form-control" name="first_name" placeholder="First Name" value="<?php echo $user['first_name']?>" required="required">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="<?php echo $user['last_name']?>" required="required">
                                            </div>
                                        </div>

                                    </div>

                                </div>


                                <div class="row">

                                    <div class="col-md-6 hidden">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-phone
                fa-fw"></i></span>
                                                <input type="text" class="form-control the_sms_code" name="sms_code" placeholder="4 digit code" value="<?php echo $user['sms_code']?>" required="required">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                                <input  type="password" class="form-control" name="password" placeholder="Password" required="required" id="pw" data-rule-minlength="6">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                                <input  type="password" class="form-control" name="password2" placeholder="Confirm Password" required="required" data-rule-equalTo="#pw" data-rule-minlength="6">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="pwstrength_viewport_progress"></div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="well well-sm">
                                                        Use a mix of letters, numbers, and symbols in your password
                                                    </div>
                                                </div>

                                            </div>
                                        </div>



                                    </div>

                                </div>




                                <?php if($user['agency_id'] == '0'){ ?>
                                <div class="row">

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-building-o fa-fw"></i></span>
                                                <input type="text" class="form-control the_company_field" name="company" placeholder="Company Name" value="<?php echo $user['business_name']?>" required="required">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="fa fa-globe
                fa-fw"></i></span>
                                                <input type="text" class="form-control the_domain_field" name="domain" placeholder="Company Domain" required="required">
                                                <span class="input-group-addon the_domain_text">.ebinder.com.au</span>


                                            </div>
                                            <p class="text-muted the_domain_text2 hidden"></p>
                                        </div>

                                    </div>


                                </div>
                                <?php } ?>

                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="form-group">
                                          <div class="checkbox">
                                            <label style="color: #666">
                                              <input type="checkbox" name="terms" required="required"> I agree with the <a href="<?php echo base_url().'landing/terms_condition_page'; ?>" target="_blank" class="terms_btnx" data-field="terms2">terms &amp; conditions</a>.
                                            </label>
                                          </div>

                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="form-group">
                                            <button class="btn btn-warning btn-block" type="submit">Create My Portal</button>
                                        </div>
                                    </div>
                                </div>


                                <?php /*?><div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-globe
        fa-fw"></i></span>
                                        <input type="text" class="form-control the_domain_field" name="domain" placeholder="Company Domain">
                                    </div>
                                    <p class="text-muted the_domain_text hidden"></p>
                                </div>


                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                        <input  type="password" class="form-control" name="password" placeholder="Password">
                                    </div>
                                </div><?php */?>


                            </form>
                        </div>
                      </div><!--step2-->


                    </div>


                </div>

            </div>



    </div> <!-- /container -->
</div> <!-- /homepage-what -->
