<div id="" class="section section-about">
    <div class="container-fluid">
		<div class="about-image">
	    	<div class="row" style="margin-left: 0; margin-right: 0">
	    		<div class="col-md-8 col-md-offset-2 about-content text-center">


						<h1><?php echo $page_info[0]['title']?></h1>

				</div>
			</div>
		</div>
	</div>
    <div class="container">
    	<div class="row">
    		<div class="col-md-8 col-md-offset-2 about-content text-left">



				<h2><?php echo $page_info[0]['content']?></h2>



    		</div>
    	</div>
    </div> <!-- /container -->
</div> <!-- /homepage-what -->
