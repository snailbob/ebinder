    <section class="bg-primary no-padding-bottom">
        <div class="container">
            <div class="col-xs-12">
                <h2 class="section-heading text-center"><?php echo $title; ?></h2>
                <hr class="light"/>
                <p class="text-center">Hi <?php echo $this->common->agency_name($agency[0]['id']) ?>, please assign a password to complete your registration.</p>
            </div>
        </div>
    </section>
    
    <section class="login_section">

        <div class="container">
            
    

            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-lg-6 col-lg-offset-3">
    

                    <form class="" id="agency_password_form">

                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" name="pw" id="pw" class="form-control" />
                            <input type="hidden" name="id" class="form-control" value="<?php echo $agency[0]['id'] ?>" />
                        </div>
                        <div class="form-group">
                            <label>Repeat Password</label>
                            <input type="password" name="pw2" class="form-control" />
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">Submit</button>
                        </div>
                    </form>

                                
                </div>
            </div>
      
      
    
        </div>

        
    </section>
