

	<?php if($this->uri->segment(1) != 'search'){ ?>
    <section id="contact" class="bg-dark <?php // if($this->uri->segment(1) == 'search') { echo 'hidden'; }?>">
        <div class="container">
            <div class="row">

            
            
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Let's Get In Touch!</h2>
                    <hr class="primary">
                    <p>&nbsp;</p>
                </div>
                
                
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="col-lg-4 text-center">
                        <i class="fa fa-envelope-o fa-3x wow bounceIn" data-wow-delay=".1s"></i>
                        <p><a href="mailto:hello@transitinsurace.com.au">hello@transitinsurace.com.au</a></p>
                    </div>
                    <div class="col-lg-4 text-center">
                        <i class="fa fa-facebook fa-3x wow bounceIn" data-wow-delay=".2s"></i>
                        <p><a href="https://facebook.com">Facebook</a></p>
                    </div>
                    <div class="col-lg-4 text-center">
                        <i class="fa fa-twitter fa-3x wow bounceIn" data-wow-delay=".3s"></i>
                        <p><a href="https://twitter.com">Twitter</a></p>
                    </div>
                
                </div>
            </div>
        </div>
    </section>
    
    <section class="bg-dark" style="padding-top: 0px; padding-bottom: 0px;">
        <div class="container">
            <div class="row">
                <p class="text-center"><a href="#" class="">Terms of Use</a> | <a href="#" class="">Privacy Policy</a></p>
                <p class="text-muted text-center">&copy; Transit Insurance 2015</p>
            </div>
        </div>
    </section>
    <?php } ?>


    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">
                        
                            <h2>Forgot Password</h2>
                            <hr class="star-primary">
                            <p class="text-center">Enter your email below and we will send you instructions on how to reset your password</p>
                            
                            
                
                            <div class="omb_login">
                
                                <div class="row omb_row-sm-offset-3">
                                    <div class="col-xs-12 col-sm-6">	
                                        <form class="omb_loginForm" id="forgot_form">
                                            <div class="form-group">
                                                <div class="input-group input-group-lg">
                                                    <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                                    <input type="email" class="form-control" name="email" placeholder="Email Address">
                                                </div>
                                            </div>
                
                                            <div class="form-group">
                                                <button class="btn btn-lg btn-primary btn-block" type="submit">Send</button>
                                            </div>
                        
                                        </form>
                                    </div>
                                </div>
                            </div>
                                                                

                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->    


    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="signupModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">
                        
                            <h2>Customer Sign Up</h2>
                            <hr class="star-primary">
                            <p class="text-center">Already have an account? <a href="#" class="login_btn">Login</a></p>
                                        
                            <div class="omb_login">
                            
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                    	<div class="alert alert-success" style="display: none;">
                                        	<button type="button" class="close" onclick="$(this).closest('.alert').hide();" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            <span class="my-text"></span>
                                        </div>
                                    
                                    </div>
                                </div>
                                <?php /*?><div class="row omb_row-sm-offset-3 omb_socialButtons">
                                    <div class="col-xs-6 col-sm-3">
                                        <a href="#" class="btn btn-lg btn-block omb_btn-facebook" data-type="signup" data-site="fb" scope="public_profile,email" onClick="fb_login();">
                                            <i class="fa fa-facebook visible-xs"></i>
                                            <span class="hidden-xs">Facebook</span>
                                        </a>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <a href="#" class="btn btn-lg btn-block omb_btn-google" data-type="signup" data-site="google" onClick="handleAuthClick();">
                                            <i class="fa fa-google-plus visible-xs"></i>
                                            <span class="hidden-xs">Google+</span>
                                        </a>
                                    </div>	
                                </div>
                        
                                <div class="row omb_row-sm-offset-3 omb_loginOr">
                                    <div class="col-xs-12 col-sm-6">
                                        <hr class="omb_hrOr">
                                        <span class="omb_spanOr">or</span>
                                    </div>
                                </div><?php */?>
                        
                                <div class="row omb_row-sm-offset-3">
                                    <div class="col-xs-12 col-sm-6">	
                                        <form class="omb_loginForm" id="signup_form">
                                            <input type="hidden" class="form-control" name="google_id">
                                            <input type="hidden" class="form-control" name="facebook_id">
                                            <input type="hidden" class="form-control" name="google_img">
                                            <input type="hidden" class="form-control" name="fb_img">
                                            <input type="hidden" class="form-control" id="social_type" name="social_type">
                                            <input type="hidden" class="form-control" id="social_site" name="social_site">
                                            

                                            <div class="form-group hidden">
                                                <div class="dropdown dropdown-form dropdown-signup">
                                                  <button class="btn btn-primary btn-block btn-outline btn-lg"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-chevron-down fa-fw pull-right"></i>
                                                    Sign up as <span class="u_type"> - Agency</span>
                                                  </button>
                                                  <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                                    <li><a href="#" data-type="Customer">Customer</a></li>
                                                  </ul>
                                                </div>
                                                
                                                <input type="hidden" name="utype" value="Customer" />
                                            </div>
                                            
                                            <div class="form-group studio_only hidden">
                                            
                                            
                                                <div class="input-group input-group-lg">
                                                    <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                    <input type="text" class="form-control" name="name" placeholder="Agency Name">
                                                </div>
                                            </div>
                                            

                                            <div class="row notfor_studio">
                                            	<div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group input-group-lg">
                                                            <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                            <input type="text" class="form-control" name="first_name" placeholder="First Name">
                                                        </div>
                                                    </div>

                                                </div>
                                            
                                            	<div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group input-group-lg">
                                                            <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                            <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            
                                            </div>
                                            
                                            
                          
                                            <div class="form-group">
                                                <div class="input-group input-group-lg">
                                                    <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                                    <input type="email" class="form-control" name="email" placeholder="Email Address">
                                                </div>
                                            </div>
                          
                                            <div class="form-group">
                                                <div class="input-group input-group-lg">
                                                    <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                                    <input  type="password" class="form-control" name="password" placeholder="Password">
                                                </div>
                                            </div>
                
                                            <div class="form-group">
                                                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign Up</button>
                                            </div>
                        
                                        </form>
                                    </div>
                                </div>
                            </div>
                        

                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->    



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">
                        
                            <h2>Login</h2>
                            <hr class="star-primary">
                            <p class="text-center">Don't have an account? <a href="#" class="signup_btn">Sign Up</a></p>
                
                            <div class="omb_login">
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                    	<div class="alert alert-danger" style="display: none;">
                                        	<button type="button" class="close" onclick="$(this).closest('.alert').hide();" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            <span class="my-text"></span>
                                        </div>
                                    
                                    </div>
                                </div>
                                <?php /*?><div class="row omb_row-sm-offset-3 omb_socialButtons">
                                    <div class="col-xs-6 col-sm-3">
                                        <a href="#" class="btn btn-lg btn-block omb_btn-facebook" data-type="login" data-site="fb" scope="public_profile,email" onClick="fb_login();">
                                            <i class="fa fa-facebook visible-xs"></i>
                                            <span class="hidden-xs">Facebook</span>
                                        </a>
                                    </div>
                                    <div class="col-xs-6 col-sm-3">
                                        <a href="#" class="btn btn-lg btn-block omb_btn-google" data-type="login" data-site="google" onClick="handleAuthClick();">
                                            <i class="fa fa-google-plus visible-xs"></i>
                                            <span class="hidden-xs">Google+</span>
                                        </a>
                                    </div>	
                                </div>
                        
                                <div class="row omb_row-sm-offset-3 omb_loginOr">
                                    <div class="col-xs-12 col-sm-6">
                                        <hr class="omb_hrOr">
                                        <span class="omb_spanOr">or</span>
                                    </div>
                                </div><?php */?>
                        
                                <div class="row omb_row-sm-offset-3">
                                    <div class="col-xs-12 col-sm-6">	
                                        <form class="omb_loginForm" id="login_form">
                                            <input type="hidden" class="form-control" name="google_id">
                                            <input type="hidden" class="form-control" name="facebook_id">

                                            <div class="form-group">
                                                <div class="dropdown dropdown-form dropdown-login">
                                                  <button class="btn btn-primary btn-block btn-outline btn-lg"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-chevron-down fa-fw pull-right"></i>
                                                    Login as <span class="u_type"> - Agency</span>
                                                  </button>
                                                  <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                                    <li><a href="#" data-type="Customer">Customer</a></li>
                                                  </ul>
                                                </div>
                                                
                                                <input type="hidden" name="utype" value="Agency" />
                                            </div>

                                            <div class="form-group">
                                                <div class="input-group input-group-lg">
                                                    <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                                    <input type="email" class="form-control" name="email" placeholder="Email Address">
                                                </div>
                                            </div>
                          
                                            <div class="form-group">
                                                <div class="input-group input-group-lg">
                                                    <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                                    <input  type="password" class="form-control" name="password" placeholder="Password">
                                                </div>
                                            </div>
                          
                                            <div class="form-group hidden">
                                            	<label>Login as</label>
                                                <input type="checkbox" name="usertype" checked>
                                            </div>
                          
                                            <div class="form-group">
                                                <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                                            </div>
                
                        
                                        </form>
                                    </div>
                                </div>
                                <div class="row omb_row-sm-offset-3">
                                    <div class="col-xs-12 col-sm-3 hidden-xs">
                                        <p class="omb_noAcc">
                                            
                                        </p>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <p class="omb_forgotPwd">
                                            <a href="#" class="forgot_btn">Forgot password?</a>
                                        </p>
                                    </div>
                                </div>	    	
                            </div>
                        
                        
	
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->    

    <!-- jQuery -->
    <script src="<?php echo base_url() ?>assets/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.fittext.js"></script>
    <script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>

    <!-- Plugin JavaScript -->
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    <script src="<?php echo base_url() ?>assets/plugins/geocomplete/jquery.geocomplete.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/datatables-bs3.js"></script>
    
    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/validate/jquery.validate.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/validate/additional-methods.min.js"></script>
    
    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bootbox/bootbox.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/jquery.form/jquery.form.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/dependencies/moment.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bs-switch/js/bootstrap-switch.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/cropper/js/cropper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/cropper/js/crop-avatar.js"></script>


    
	<?php /* if($this->uri->segment(1) == 'buy') { ?>
    <!-- Plugin JavaScript -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/1.2.3/jquery.payment.min.js"></script>
    
    <!-- If you're using Stripe for payments -->
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <?php } */ ?>


    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/creative.js"></script>
    <script src="<?php echo base_url() ?>assets/js/script.js"></script>

</body>

</html>
