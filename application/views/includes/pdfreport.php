<?php
tcpdf();
$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
//$title = $title;
//$obj_pdf->SetTitle($title);
//$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, $schedule); //PDF_HEADER_STRING
//$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
//$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
//$obj_pdf->SetDefaultMonospacedFont('helvetica');
//$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
//$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 10);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();

/*

// Colors, line width and bold font
$obj_pdf->SetFillColor(51, 122, 183);
$obj_pdf->SetTextColor(255);
$obj_pdf->SetDrawColor(46, 109, 164);
$obj_pdf->SetLineWidth(0.3);
$obj_pdf->SetFont('', 'B');


// column titles
$header = array('Tasks');

// Header
$w = array(180);
$num_headers = count($header);
for($i = 0; $i < $num_headers; ++$i) {
	$obj_pdf->Cell($w[$i], 7, $header[$i], 1, 0, 'C', 1);
}


// data loading
$data = $this->master_model->getRecords('cf_recall_steps', array('recall_id'=>'13'));

$obj_pdf->Ln();
// Color and font restoration
$obj_pdf->SetFillColor(	217, 237, 247);
$obj_pdf->SetTextColor(0);
$obj_pdf->SetFont('');
// Data
$fill = 0;
foreach($data as $r=>$value) {
	$obj_pdf->Cell($w[0], 6, $value['question'], 'LR', 0, 'L', $fill);
//	$obj_pdf->Cell($w[1], 6, $value['step_no'], 'LR', 0, 'L', $fill);
//	$obj_pdf->Cell($w[2], 6, $value['recall_id'], 'LR', 0, 'R', $fill);
//	$obj_pdf->Cell($w[3], 6, $value['date_completed'], 'LR', 0, 'R', $fill);
	$obj_pdf->Ln();
	$fill=!$fill;
}
$obj_pdf->Cell(array_sum($w), 0, '', 'T');
*/

ob_start();
			
	$data = array(
		'data'=>$data
	);
	$this->load->view('includes/pdf-tables', $data);
    // we can have any view part here like HTML, PHP etc
    $content = ob_get_contents();
	
ob_end_clean();

$obj_pdf->writeHTML($content, true, false, true, false, '');

$upload_path = 'uploads/certifs';
$filename = str_replace(' ','_',$title).'-'.date_format(date_create($nowtime), "Y-m-d-g:ia").'.pdf';
$thepdf = $obj_pdf->Output($filename, 'I');//I


	
?>