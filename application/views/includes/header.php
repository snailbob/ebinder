<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <?php /*?><meta name="viewport" content="width=device-width, initial-scale=1"><?php */?>
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Transit Insurance | Australia’s premier online dancing community hub | find and book dance classes</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>" type="image/x-icon"/>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" type="text/css">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>assets/fonts/open-sans.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/fonts/merriweather.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400' rel='stylesheet' type='text/css'>

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/dataTables.bootstrap.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/datatables.css" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/cropper.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/crop-avatar.css" type="text/css">


    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bs-switch/css/bootstrap3/bootstrap-switch.min.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/creative.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	<script>
		var base_url = '<?php echo base_url() ?>';
		var uri_1 = '<?php echo $this->uri->segment(1) ?>';
		var uri_2 = '<?php echo $this->uri->segment(2) ?>';
		var uri_3 = '<?php echo $this->uri->segment(3) ?>';
		var uri_4 = '<?php echo $this->uri->segment(4) ?>';
		var user_name = '<?php echo $this->session->userdata('name') ?>';
		var user_location = '<?php echo $this->session->userdata('location') ?>';
		var user_address = '<?php echo $this->session->userdata('address') ?>';
		var user_type = '<?php echo $this->session->userdata('type') ?>';
		var stripe_id = '<?php echo $this->session->userdata('stripe_id') ?>';
    var domain_name = '<?php echo $this->common->the_domain() ?>';

	</script>



</head>

<body id="page-top">

	<?php
		//vars
		$colors = $this->common->colors();
		$usertype = $this->session->userdata('type');
		$opposite_type = 'studio';
		if($usertype != 'instructor'){
			$opposite_type = 'instructor';
		}
	?>
<?php /*?>
	<script>

	// This is called with the results from from FB.getLoginStatus().
	function statusChangeCallback(response) {
		console.log('statusChangeCallback');
		console.log(response);
		// The response object is returned with a status field that lets the
		// app know the current login status of the person.
		// Full docs on the response object can be found in the documentation
		// for FB.getLoginStatus().
		if (response.status === 'connected') {
		// Logged into your app and Facebook.
			testAPI();
//		FB.logout(function(response) {
//			// Person is now logged out
//			console.log('logged out!');
//		});
		} else if (response.status === 'not_authorized') {
		  // The person is logged into Facebook, but not your app.
		  //document.getElementById('status').innerHTML = 'Please log ' +
		   // 'into this app.';
		} else {
		  // The person is not logged into Facebook, so we're not sure if
		  // they are logged into this app or not.
		  //document.getElementById('status').innerHTML = 'Please log ' +
		   // 'into Facebook.';
		}
	}

	// This function is called when someone finishes with the Login
	// Button.  See the onlogin handler attached to it in the sample
	// code below.
	function checkLoginState() {
		FB.getLoginStatus(function(response) {
			statusChangeCallback(response);
		});
	}

	window.fbAsyncInit = function() {
	FB.init({
		appId      : '1160945953932415', //532984193504131',
		cookie     : true,  // enable cookies to allow the server to access
							// the session
		xfbml      : true,  // parse social plugins on this page
		version    : 'v2.1' // use version 2.1
	});

	// Now that we've initialized the JavaScript SDK, we call
	// FB.getLoginStatus().  This function gets the state of the
	// person visiting this page and can return one of three states to
	// the callback you provide.  They can be:
	//
	// 1. Logged into your app ('connected')
	// 2. Logged into Facebook, but not your app ('not_authorized')
	// 3. Not logged into Facebook and can't tell if they are logged into
	//    your app or not.
	//
	// These three cases are handled in the callback function.

	FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	});

	};

	// Load the SDK asynchronously
	(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));


	// Here we run a very simple test of the Graph API after login is
	// successful.  See statusChangeCallback() for when this call is made.
	function testAPI() {
		console.log('Welcome!  Fetching your information.... ');
		FB.api('/me', function(response) {
		  console.log('Successful login for: ' + response.name);
		  console.log(JSON.stringify(response));
		  //document.getElementById('status').innerHTML =
		   // 'Thanks for logging in, ' + response.name + '!';
		});
	}

	function getFbData(){

		FB.api('/me', function(response) {
			console.log(JSON.stringify(response));

			var u_email = '';
			if(response.email){
				u_email = response.email;
			}
			var soc_img = 'https://graph.facebook.com/'+response.id+'/picture?type=large';

			var $soc_btn = $('.omb_btn-facebook');
			$soc_btn.addClass('disabled');

			var actiontype = $('[name="social_type"]').val();
			var social_site = $('[name="social_site"]').val();
			var $signup_form = $('#signup_form');
			if(actiontype == 'signup'){
				$signup_form.find('[name="email"]').val(u_email);
				$signup_form.find('[name="facebook_id"]').val(response.id);
				$signup_form.find('[name="fb_img"]').val(soc_img);
				$signup_form.closest('.omb_login').find('.omb_btn-facebook').html('<i class="fa fa-check-circle"></i> Facebook');

				$signup_form.closest('.omb_login').find('.my-text').html('<i class="fa fa-check-circle"></i> Facebook successfully connected. Complete the form below to create your Agency  account.');
				$signup_form.closest('.omb_login').find('.alert').show();

				return false;
			}

			var dataa = {
				"id": response.id,
				"name": response.first_name+' '+response.last_name,
				"type": 'fb',
				"action_type": actiontype,
				"email": u_email,
				"img": soc_img
			};

			console.log(dataa);
			dataa = $.param(dataa); // $(this).serialize() + "&" +

			$.post(
				base_url+'formsubmits/social_login',
				dataa,
				function(res){
					console.log(res);

					if(res.action == 'login' && res.result == 'ok'){
						$soc_btn.html('<i class="fa fa-check-circle"></i> Facebook');
						window.location.href = base_url+'dashboard';
					}
					else{
						$('.modal').modal('hide');
						$soc_btn.removeClass('disabled');
						bootbox.alert('<h4>'+res.message+'</h4>');
					}

				},
				'json'
			);



		});

	}//getFbData end


	function fb_login(){
		  FB.getLoginStatus(function(response) {
				if(response.status == 'connected'){
					//get fb data
					console.log(response);
					getFbData();
				}else{

					FB.login(function(response) {
					  console.log(response.status);
					  if (response.status === 'connected') {
						// Logged into your app and Facebook.
						console.log(response.authResponse.accessToken);
						getFbData();
					  } else if (response.status === 'not_authorized') {
						// The person is logged into Facebook, but not your app.

					  } else {
						// The person is not logged into Facebook, so we're not sure if
						// they are logged into this app or not.
					  }
					}, {scope: 'public_profile,email'});

				}

		  });

	}//login_fb end

  	function connectFb(){
		console.log('connect my fb');
	}//connectFb end

	</script>

    <script type="text/javascript">
      // Enter a client ID for a web application from the Google Developer Console.
      // The provided clientId will only work if the sample is run directly from
      // https://google-api-javascript-client.googlecode.com/hg/samples/authSample.html
      // In your Developer Console project, add a JavaScript origin that corresponds to the domain
      // where you will be running the script.
      var clientId = '768874366247-tffbevn91f5r46v72954pd78prpsbg2u.apps.googleusercontent.com'; //1041052586833-p5gn0f5828unp0ve0t8i0girpp8mgba8.apps.googleusercontent.com';

      // Enter the API key from the Google Develoepr Console - to handle any unauthenticated
      // requests in the code.
      // The provided key works for this sample only when run from
      // https://google-api-javascript-client.googlecode.com/hg/samples/authSample.html
      // To use in your own application, replace this API key with your own.
      var apiKey = 'AIzaSyDVL8WaKGrSPhJ7ZY8XHrJeWascHtNA0qc'; //AIzaSyALyiCnNODtJToaFtuZHESkc_ZHdfx_GRc'; //AIzaSyCxXrGNIjpnWIf6FlqbbwlSp-oYYgfTADU'; //AIzaSyAVK5bSoKOZh4tjiy3NpJPFXzc8qCktm4w';

      // To enter one or more authentication scopes, refer to the documentation for the API.
      var scopes = 'https://www.googleapis.com/auth/plus.profile.emails.read'; //plus.me';

      // Use a button to handle authentication the first time.
      function handleClientLoad() {
        gapi.client.setApiKey(apiKey);
        window.setTimeout(checkAuth,1);
      }

      function checkAuth() {
        gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: true} , handleAuthResult);
      }


      function handleAuthResult(authResult) {
        var authorizeButton = document.getElementById('authorize-button');

         //authorizeButton.style.visibility = '';
         // authorizeButton.onclick = handleAuthClick;
		  console.log(JSON.stringify(authResult));

			if (authResult && !authResult.error) {
			//authorizeButton.style.visibility = 'hidden';
			//	makeApiCall();
				//console.log(JSON.stringify(authResult));

			}

			else {
				//bootbox.alert('<p class="lead"><i class="fa fa-info-circle"></i> Something went wrong. Please contact admin.</p>');
			}
      }

      function handleAuthClick(event) {
        gapi.auth.authorize({client_id: clientId, scope: scopes, immediate: false}, makeApiCall);
		//handleAuthResult);
		//makeApiCall();

        return false;
      }

      // Load the API and make an API call.  Display the results on the screen.
      function makeApiCall() {
        gapi.client.load('plus', 'v1', function() {
          var request = gapi.client.plus.people.get({
            'userId': 'me'
          });
          request.execute(function(resp) {
				//console.log(JSON.stringify(resp));
				console.log(resp.emails[0].value);
				//console.log(resp.displayName);
				console.log(resp.id);
				console.log(resp.name.givenName);
				console.log(resp.name.familyName);
				console.log(resp.image.url);
				//console.log(resp.placesLived[0].value);


				var $soc_btn = $('.omb_btn-google')
				$soc_btn.addClass('disabled');

				var actiontype = $('[name="social_type"]').val();
				var social_site = $('[name="social_site"]').val();
				var $signup_form = $('#signup_form');
				if(actiontype == 'signup'){
					$signup_form.find('[name="email"]').val(resp.emails[0].value);
					$signup_form.find('[name="google_id"]').val(resp.id);
					$signup_form.find('[name="google_img"]').val(resp.image.url);
					$signup_form.closest('.omb_login').find('.omb_btn-google').html('<i class="fa fa-check-circle"></i> Google+');

					$signup_form.closest('.omb_login').find('.my-text').html('<i class="fa fa-check-circle"></i> Google+ successfully connected. Complete the form below to create your Agency  account.');
					$signup_form.closest('.omb_login').find('.alert').show();

					return false;
				}


				var dataa = {
					"id": resp.id,
					"name": resp.name.givenName+' '+resp.name.familyName,
					"type": 'google',
					"action_type": actiontype,
					"email": resp.emails[0].value,
					"img": resp.image.url
				};

				console.log(dataa);
				dataa = $.param(dataa); // $(this).serialize() + "&" +
				$.post(
					base_url+'formsubmits/social_login',
					dataa,
					function(res){
						console.log(res);

						if(res.action == 'login' && res.result == 'ok'){
							window.location.href = base_url+'dashboard';
							$soc_btn.html('<i class="fa fa-check-circle"></i> Google+');
						}
						else{
							$('.modal').modal('hide');
							$soc_btn.removeClass('disabled');
							bootbox.alert('<h4>'+res.message+'</h4>');
						}

					},
					'json'
				);



          });
        });
      }
    </script>
    <script src="https://apis.google.com/js/client.js?onload=handleClientLoad"></script>

    <div id="fb-root"></div>

<?php */?>

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top bg-black-trans">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display  bg-black-trans -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="<?php if($this->uri->segment(1) != '') { echo base_url(); } else {echo '#page-top';}?>">
                	<img src="<?php echo base_url().'assets/img/logo.png'?>" alt="Dance Pass" title="" class="hidden" />
                    Transit Insurance
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">


				<?php if($this->session->userdata('name') != ''){ ?>


                    <div class="dropdown my-dropdown my-dropdown-merge navbar-btn navbar-right" style="margin-right: 15px;">
                      <button class="btn btn-primary btn-outline" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-user"></i> <?php echo $this->session->userdata('name');?>
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li class="<?php if($this->uri->segment(1) == 'dashboard') { echo 'active'; }?>">
                            <a href="<?php echo base_url().'dashboard' ?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>

                        <li class="<?php if($this->uri->segment(2) == 'profile' && $this->uri->segment(3) == '') { echo 'active'; }?>"><a href="<?php echo base_url().'settings/profile' ?>"><i class="fa fa-user fa-fw"></i> <?php echo ucfirst($usertype); ?> Profile</a></li>

                        <li class="<?php if($this->uri->segment(1) == 'rating') { echo 'active'; }?> <?php if($this->session->userdata('type') != 'agency') { echo 'hidden';} ?>">
                            <a href="<?php echo base_url().'rating' ?>"><i class="fa fa-bar-chart fa-fw"></i>
                                Rating
                            </a>
                        </li>

						<?php if($this->session->userdata('multiple_account') == 'yes'){ ?>
                        <li><a href="#" data-toggle="modal" data-target="#loginModal"><i class="fa fa-exchange fa-fw"></i> Switch to <?php echo ucfirst($opposite_type); ?></a></li>
                        <?php } ?>
                        <li><a href="<?php echo base_url().'settings/profile/pw' ?>"><i class="fa fa-key fa-fw"></i> Password</a></li>
                        <li><a href="<?php echo base_url().'dashboard/logout' ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>

                      </ul>
                    </div>




                <?php } else{ ?>

                    <div class="dropdown my-dropdown my-dropdown-merge dropdown-login navbar-btn navbar-right" style="margin-right: 15px;">
                      <button class="btn btn-primary btn-outline" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Login to Trans In
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="#" class="login_btn" data-type="Agency"><i class="fa fa-sign-in fa-fw"></i> Login as Agency </a></li>

                        <li><a href="#" class="login_btn" data-type="Customer"><i class="fa fa-sign-in fa-fw"></i> Login as Customer</a></li>
                      </ul>
                    </div>

                    <button class="navbar-btn navbar-right btn btn-primary btn-outline signup_btn" data-type="Agency" type="button" style="margin-right: 15px;">
                        Sign Up Here
                    </button>

                    <div class="hidden dropdown my-dropdown my-dropdown-merge dropdown-signup navbar-btn navbar-right" style="margin-right: 15px;">
                      <button class="btn btn-primary btn-outline" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Sign Up Here
                        <span class="caret"></span>
                      </button>
                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="#" class="signup_btn" data-type="Agency"><i class="fa fa-pencil fa-fw"></i> Sign Up as Agency </a></li>

                        <li><a href="#" class="signup_btn" data-type="Customer"><i class="fa fa-pencil fa-fw"></i> Sign Up as Customer</a></li>
                      </ul>
                    </div>

				<?php

				}
                ?>


                <ul class="nav navbar-nav navbar-right">


                    	<?php
							if($this->session->userdata('name') != ''){
								echo '<li class="visible-xs">';
								echo '<a href="'.base_url().'dashboard"><i class="fa fa-user"></i> '.$this->session->userdata('name').'</a>';
								echo '</li>';
							}
							else{
								echo '<li class="visible-xs">';
								echo '<a href="#" class="login_btn">Login to Transit Insurance</a>';
								echo '</li>';

								echo '<li class="visible-xs">';
								echo '<a href="#" class="signup_btn">Agency Registration</a>';
								echo '</li>';
							}
						?>




                    <li class="visible-xs">
                        <a class="page-scroll" href="<?php if($this->uri->segment(1) != '') { echo base_url(); }?>#about">About</a>
                    </li>
                    <li class="visible-xs">
                        <a class="page-scroll" href="<?php if($this->uri->segment(1) != '') { echo base_url(); }?>#services">Services</a>
                    </li>
                    <li class="visible-xs">
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>



	<?php if($this->uri->segment(1) == ''){
		 //   <source src="assets/videos/shutterstock_v5239256.mp4" type="video/mp4">
 			//poster="assets/img/dance.png"
		?>
    <video autoplay id="bgvid" onended="switchVid()">
    <source src="<?php echo base_url() ?>assets/videos/shutterstock_v6441026.mp4" type="video/mp4">
    </video>
    <?php } ?>
