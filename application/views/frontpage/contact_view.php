<style>
.form-inline .form-group {
display: inline-block;
margin-bottom: 20px;
vertical-align: middle;
text-align: left;
}
</style>


<div id="home-what" class="section" >
    <div class="container">
        <h1>Contact Us</h1>
                
            <div class="alert alert-success cont_success alert-dismissable col-md-8 column col-md-offset-2" style="display: none;">
              <button type="button" class="close" onclick="$('.start_success').hide();" aria-hidden="true">&times;</button>
              Form successfully submitted.
            </div>   

			<div id="success_msg"></div>
            <?php if ($success !=''){ ?>
			<div class="alert alert-success alert-dismissable">
            <?php echo $success; ?>
            </div>
            
            <?php } ?>
            <div class="alert alert-warning alert-dismissable " id="succ_mess" style="height: 50px; display: none;">
            </div>
            <div style="clear: both"></div>
            <div class="col-md-8 column col-md-offset-2"  id="contact" style=" background: rgba(255,255,255,.5); padding:20px; padding-top: 40px; margin-top: 20px; border-radius: 5px;">
            

                <form class="form-inline" role="form" method="post" id="contact_form" action="<?php echo base_url()?>success">
                
                	<div class="row">
                           
                         <div class="col-lg-6">
                         	<div class="form-group">
                                
                                <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                  <input type="text" class="form-control" required id="inputname" name="inputname" minlength="3" placeholder="Your Name*">
                                
                                </div><!-- /input-group -->
                     
                            </div>
                          </div><!-- /.col-lg-6 -->
                          
                          <div class="col-lg-6">
                         	<div class="form-group">
                                <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-globe"></i></span>
            
                                        <select class="form-control" name="inputCountry" id="inputCountry" style="width: 100%;" required>
                                        <option value="">Country*</option>
            
                                        <?php
                                            $countries = $this->master_model->getRecords('country_t');
                                            foreach($countries as $c=>$cou){
                                                echo '<option value="'.$cou['short_name'].'">'.$cou['short_name'].'</option>';
                                            }
                                        ?>
                                        </select>                    
            
                                </div><!-- /input-group -->                     
                            </div>
                    

                          </div><!-- /.col-lg-6 -->
                                              
                    </div>
                    <div class="row">
                    
                         <div class="col-lg-6">
                         	<div class="form-group">
                                <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-building"></i></span>
                                <input type="text" class="form-control" id="inputwebsite" name="inputwebsite" placeholder="Company*" required>
                                </div><!-- /input-group -->
                     
                            </div>
                          </div><!-- /.col-lg-6 -->
        
                          
                          <div class="col-lg-6">
                         	<div class="form-group">
                                
                                <div class="input-group">
                                  <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                <input type="email" class="form-control" id="inputemail" name="inputemail" placeholder="Email Address*" required >
                                </div><!-- /input-group -->
                     
                            </div>
                          </div><!-- /.col-lg-6 -->
                          
                    </div>
                 
                 

                  <div class="col-lg-12 form-group">
                    <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-link"></i></span>
                    <input type="text" class="form-control" id="inputsubject" name="inputsubject" placeholder="Subject">
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-6 -->
                  <div class="col-lg-12 form-group">
                    <div class="input-group">
                      <span class="input-group-addon "><i class="fa fa-envelope"></i></span>
                    <textarea class="form-control" rows="4" id="inputmessage" name="inputmessage" placeholder="Your Message*" required minlength="3"></textarea>
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-6 --><br>
    
                  <div class="col-lg-12 form-group">
                    <div class="input-group">
                             <button type="submit"  id="form_submit" class="btn btn-primary btn-rounded">SEND MESSAGE</button> * Required Fields
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-6 --><br>
                </form>

                
            </div>
            
            <div class="col-md-8 column col-md-offset-2"  id="contact" style=" background: rgba(255,255,255,.5); padding:20px; margin-top: 20px; border-radius: 5px;">
    			<div style=" text-align:left; margin-top: 20px;">
                	<div class="col-md-6 col-sm-12">
                    <p class="lead">Suite 3, Level 8, 179 Queen St<br />
                    Melbourne, Victoria<br />
                    Australia 3000</p>
                    </div>
                	<div class="col-md-6 col-sm-12 hidden-sm hidden-xs">
                    <img class="pull-right" src="<?php echo base_url()?>assets/frontpage/images/staticmap.png" width="70%" height="70%" alt="Map"/>
                	</div>
                	<div class="col-md-6 col-sm-12 visible-sm visible-xs">
                    <img src="<?php echo base_url()?>assets/frontpage/images/staticmap.png" width="70%" height="70%" alt="Map"/>
                	</div>
                </div>
                <div style="clear: both; height: 30px;"></div>
            </div>

    </div> <!-- /container -->
</div> <!-- /homepage-what -->
