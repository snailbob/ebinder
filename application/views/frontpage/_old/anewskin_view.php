
<div id="landing-feature">

<div class="container-fluid">

    <div class="text-center request-btn-banner hidden-xs">
        <a class="btn btn-primary btn-lg btn-rounded hidden" href="#loginblue" data-toggle="modal" data-controls-modal="loginblue" style="width: 190px">Request Demo</a>
    </div>
    
</div>
            
            
            
	<div id="feature-slide-4" class="home-jumbo jumbotron slide4" data-0="background-position:right 0px;" data-654="background-position:right -350px;">
		<div class="white-overlay visible-xs visible-sm visible-md visible-lg"></div>
	    <div class="container">
	      <div class="row">
	        <div id="slide-4-content" class="col-md-10 col-md-offset-1 text-center animated fadeInDown animated-top" style="opacity:1">
	            <h1><?php echo $banners[2]['title'] ?></h1>
            	<p class="text-center visible-xs">
                    <a class="btn btn-primary btn-lg btn-rounded hidden" href="#loginblue" data-toggle="modal" data-controls-modal="loginblue">Request Demo</a>
                </p>
	       	</div> <!-- /col-md-6-->
	      </div> <!-- /row-->
	   </div> <!-- /container-->
	</div> <!-- /feature-slide-3-->
	
	<div id="feature-slide-3" class="home-jumbo jumbotron slide3" data-0="background-position:right 0px;" data-654="background-position:right -350px;">
		<div class="white-overlay visible-xs visible-sm visible-md visible-lg"></div>
	    <div class="container">
	      <div class="row">
	        <div id="slide-3-content" class="col-md-10 col-md-offset-1 text-center animated fadeInDown animated-top" style="opacity:1">
	            <h1><?php echo $banners[1]['title'] ?></h1>
            	<p class="text-center visible-xs">
                    <a class="btn btn-primary btn-lg btn-rounded hidden" href="#loginblue" data-toggle="modal" data-controls-modal="loginblue">Request Demo</a>
                </p>
	       	</div> <!-- /col-md-6-->
	      </div> <!-- /row-->
	   </div> <!-- /container-->
	</div> <!-- /feature-slide-3-->
	
    
	<div id="feature-slide-1" class="home-jumbo jumbotron slide2" data-0="background-position:right 0px;" data-654="background-position:right -350px;">
		<div class="white-overlay visible-xs visible-sm visible-md visible-lg"></div>
	    <div class="container">
	      <div class="row">
	        <div id="slide-1-content" class="col-md-10 col-md-offset-1 text-center animated fadeInDown animated-top" style="opacity:1">
	            <h1><?php echo $banners[0]['title'] ?></h1>
            	<p class="text-center visible-xs">
                    <a class="btn btn-primary btn-lg btn-rounded hidden" href="#loginblue" data-toggle="modal" data-controls-modal="loginblue">Request Demo</a>
                </p>
	        </div> <!-- /col-med -->
	      </div> <!-- /row -->
	    </div> <!-- /container -->
	</div> <!-- /jumbotron -->

</div> <!-- /landing-feature -->

<?php /*?><div id="home-nav" class="sub-nav hidden-xs hidden-sm">
    <div class="container">
        <ul class="nav nav-pills nav-justified">
            <li class="active menuItem"><a id="yolo" class="menuScroll" data-scrollto="#home-what">What is CrisisFlo?</a></li>
            <li class="menuItem"><a class="menuScroll" data-scrollto="#our-solutions">Our Solution</a></li>
            <li class="menuItem"><a class="menuScroll" data-scrollto="#home-why">Collaboration Features</a></li>
            <!--<li class="menuItem hidden"><a class="menuScroll" data-scrollto="#home-how">How it Works</a></li>-->
            <li class="menuItem"><a class="menuScroll" data-scrollto="#more-reasons">More Reasons Why</a></li>
            <li class="menuItem"><a class="menuScroll" data-scrollto="#pricing">Pricing</a></li>
        
         </ul>
    </div> <!-- /container -->
</div> <!-- /homepage-nav --><?php */?>

<div id="home-what" class="section" >
    <div class="container">
        <h1><?php echo $firsthomesection[0]['title']?></h1>
        
        <h2 style="font-size: 21px;"><?php echo $firsthomesection[0]['content']?></h2>
        
        <div class="text-center crisis-on-device">
            <img class="img-responsive" src="<?php echo base_url().'uploads/avatars/'.$firsthomesection[0]['image']; ?>" alt="CrisisFlo" title="" />
        </div>
        
        
    </div> <!-- /container -->
</div> <!-- /homepage-what -->





<div id="our-solutions" class="section" style="border-bottom: 1px solid #eee;" data-center="background-color:rgb(240, 247, 252);" data-center-top="background-color:rgb(255, 255, 255);">
    <div class="container">

        <div class="row centered our-boxes-holder" style="margin-top: 0px;">
            <h1><?php echo $alternatingcontenthead[0]['title']?></h1>
            
            <h2 style="font-size: 21px;"><?php echo $alternatingcontenthead[0]['content']?></h2>

			<?php
				$alternatingcontentscount = 1;
				foreach($alternatingcontents as $r=>$value){
					$mod = $alternatingcontentscount%2;
					//left text
					if($mod == 0){ ?>
                    
                    
        	<div class="col-sm-6 text-left hidden-xs">
                <h3 style="margin-top: 25px;font-size: 160%;" class="text-info"><?php echo $value['title']?></h3>
                <p style="font-size: 120%;"><?php echo $value['content']?></p>
            </div>
        
        	<div class="col-sm-6">
                <img src="<?php echo base_url().'uploads/avatars/'.$value['image'] ?>" class="img-responsive thumbnail" style="margin: 0 auto;"/>
                <h3 style="font-size: 125%;" class="text-info hidden-lg hidden-md hidden-sm"><?php echo $value['title']?></h3>
                <p style="font-size: 120%;" class="hidden-lg hidden-md hidden-sm"><?php echo $value['content']?></p>
            </div>
        
                    
                    
					<?php
					}
					//right text
					else{ ?>
                    
                    


        	<div class="col-sm-6">
                <img src="<?php echo base_url().'uploads/avatars/'.$value['image'] ?>" class="img-responsive thumbnail" style="margin: 0 auto;"/>
                <h3 style="font-size: 125%;" class="text-info hidden-lg hidden-md hidden-sm"><?php echo $value['title']?></h3>
                <p style="font-size: 120%;" class="hidden-lg hidden-md hidden-sm"><?php echo $value['content']?></p>
            </div>
        
        	<div class="col-sm-6 text-left hidden-xs">
                <h3 style="margin-top: 25px;font-size: 160%;" class="text-info"><?php echo $value['title']?></h3>
                <p style="font-size: 120%;"><?php echo $value['content']?></p>
            </div>
                            
                    <?php					
					
					}
					
					if($alternatingcontentscount != count($alternatingcontents)){
				?>
                
        
        	<!--separator-->
			<div style="clear: both; border-bottom: 1px solid #eee; padding-top: 30px; margin-top: 30px; margin-bottom: 30px;" class="hidden-xs"></div>
			<div style="clear: both; border-bottom: 1px solid #eee; margin-top: 15px; margin-bottom: 15px;" class="hidden-lg hidden-md hidden-sm"></div>
        	<!--separator-->                    
                
                
            <?php
					}
					
					$alternatingcontentscount++;
				}
			?>
		

        <?php /*?>
        
        	<!--separator-->
			<div style="clear: both; border-bottom: 1px solid #eee; padding-top: 30px; margin-top: 30px; margin-bottom: 30px;" class="hidden-xs"></div>
			<div style="clear: both; border-bottom: 1px solid #eee; margin-top: 15px; margin-bottom: 15px;" class="hidden-lg hidden-md hidden-sm"></div>
        	<!--separator-->
        
        	<div class="col-sm-6 text-right hidden-xs">
            	
                <h3 style="margin-top: 25px;font-size: 160%;" class="text-info">Situational awareness at your fingertips</h3>
                <p style="font-size: 120%;">Know where your incident team members are and their proximity to affected sites to optimize resource assignment.</p>
            </div>

        	<div class="col-sm-6">
            	<img src="<?php echo base_url().'assets/frontpage/images/situation-map.jpg' ?>" class="img-responsive thumbnail" style="margin: 0 auto;"/>
                <h3 style="font-size: 125%;" class="text-info hidden-lg hidden-md hidden-sm">Situational awareness at your fingertips</h3>
                <p style="font-size: 120%;" class="hidden-lg hidden-md hidden-sm">Know where your incident team members are and their proximity to affected sites to optimize resource assignment.</p>
            </div>
                    
        
        	<!--separator-->
			<div style="clear: both; border-bottom: 1px solid #eee; padding-top: 30px; margin-top: 30px; margin-bottom: 30px;" class="hidden-xs"></div>
			<div style="clear: both; border-bottom: 1px solid #eee; margin-top: 15px; margin-bottom: 15px;" class="hidden-lg hidden-md hidden-sm"></div>
        	<!--separator-->
        

        	<div class="col-sm-6">
            	<img src="<?php echo base_url().'assets/frontpage/images/massnoti.png' ?>" class="img-responsive thumbnail" style="margin: 0 auto;"/>
                <h3 style="margin-top: 25px;font-size: 125%;" class="text-info hidden-lg hidden-md hidden-sm">Notifications made easy</h3>
                <p style="font-size: 120%;" class="hidden-lg hidden-md hidden-sm">Get your message out to key stakeholders early and frequently. Streamline your bulk email and SMS notification process and centrally monitor confirmed recipients. </p>
            </div>
        
        	<div class="col-sm-6 text-left hidden-xs">
            	
                <h3 style="margin-top: 25px;font-size: 160%;" class="text-info">Notifications made easy</h3>
                <p style="font-size: 120%;">Get your message out to key stakeholders early and frequently. Streamline your bulk email and SMS notification process and centrally monitor confirmed recipients. </p>
            </div>
        
        
        	<!--separator-->
			<div style="clear: both; border-bottom: 1px solid #eee; padding-top: 30px; margin-top: 30px; margin-bottom: 30px;" class="hidden-xs"></div>
			<div style="clear: both; border-bottom: 1px solid #eee; margin-top: 15px; margin-bottom: 15px;" class="hidden-lg hidden-md hidden-sm"></div>
        	<!--separator-->
        
        
        	<div class="col-sm-6 text-right hidden-xs">
            	
                <h3 style="margin-top: 25px;font-size: 160%;" class="text-info">Monitor your recall costs</h3>
                <p style="font-size: 120%;">Record your recall costs as you incur them to ensure your total cost of recall is fully accounted for. </p>
            </div>

        	<div class="col-sm-6">
            	<img src="<?php echo base_url().'assets/frontpage/images/costmonitor.png' ?>" class="img-responsive thumbnail" style="margin: 0 auto;"/>
                <h3 style="margin-top: 25px;font-size: 125%;" class="text-info hidden-lg hidden-md hidden-sm">Monitor your recall costs</h3>
                <p style="font-size: 120%;" class="hidden-lg hidden-md hidden-sm">Record your recall costs as you incur them to ensure your total cost of recall is fully accounted for. </p>
            </div>

        	<!--separator-->
			<div style="clear: both; border-bottom: 1px solid #eee; padding-top: 30px; margin-top: 30px; margin-bottom: 30px;" class="hidden-xs"></div>
			<div style="clear: both; border-bottom: 1px solid #eee; margin-top: 15px; margin-bottom: 15px;" class="hidden-lg hidden-md hidden-sm"></div>
        	<!--separator-->
        
        

        	<div class="col-sm-6">
            	<img src="<?php echo base_url().'assets/frontpage/images/reportpdf.jpg' ?>" class="img-responsive thumbnail" style="margin: 0 auto;"/>
                <h3 style="margin-top: 25px;font-size: 125%;" class="text-info hidden-lg hidden-md hidden-sm">Generate Status Reports</h3>
                <p style="font-size: 120%;" class="hidden-lg hidden-md hidden-sm">Generate status reports in PDF for distribution to regulators in order to keep them informed of your recall efforts.</p>
            </div>
                    
        	<div class="col-sm-6 text-left hidden-xs">
            	
                <h3 style="margin-top: 25px;font-size: 160%;" class="text-info">Generate Status Reports</h3>
                <p style="font-size: 120%;">Generate status reports in PDF for distribution to regulators in order to keep them informed of your recall efforts.</p>
            </div>

            <?php */?>
        
        <?php /*?></div><!-- /our-boxes-holder-->
        <div class="row centered our-boxes-holder"><?php */?>
        
        <?php /*?>
        	<!--separator-->
			<div style="clear: both; border-bottom: 1px solid #eee; padding-top: 30px; margin-top: 30px; margin-bottom: 30px;" class="hidden-xs"></div>
			<div style="clear: both; border-bottom: 1px solid #eee; margin-top: 15px; margin-bottom: 15px;" class="hidden-lg hidden-md hidden-sm"></div>
        	<!--separator-->
        
        
        
        
            <h2 style="font-weight:bold;"><big>Other products & services</big></h2>
            <div class="col-md-3 col-sm-12 item our-solution-box hidden">
				<h2 class="our-solution-title text-primary" style="font-weight: bold;"><i class="fa fa-history fa-fw"></i> Recall Manager</h2>
				<h2 class="our-solution-title text-primary visible-md">Recall Manager</h2>
                <p class="our-text"> Our Recall Manager solution was developed and customized for the consumer goods industry to help deal with product recall scenarios.</p>
                <p class="text-center" style="margin: 0; padding:0;">
                	<a href="#recallsolution" class="btn btn-info btn-xs scroll_btn">Read More</a>
                </p>
                
            </div>
        
            <div class="col-md-4 col-sm-12 item our-solution-box first">
				<h2 class="our-solution-title text-primary hidden-md"><i class="fa fa-fire fa-fw"></i> Crisis Manager</h2>
				<h2 class="our-solution-title text-primary visible-md">Crisis Manager</h2>
                <p class="our-text"> Conduct crisis planning and assign tasks and responsibilities pre-incident, enabling efficient mobilization of your response team if and when a crisis occurs.</p>
            </div>
        
            <div class="col-md-4 col-sm-12 item our-solution-box">
				<h2 class="our-solution-title text-primary hidden-md"><i class="fa fa-life-ring fa-fw"></i> Case Manager</h2>
				<h2 class="our-solution-title text-primary visible-md">Case Manager</h2>
                <p class="our-text"> Dispatch the right case manager based on proximity, availability and experience, ensuring optimal outcomes for your investigations.</p>
            </div>
        
            <div class="col-md-4 col-sm-12 item our-solution-box last">
				<h2 class="our-solution-title text-primary hidden-md"><i class="fa fa-gears fa-fw"></i> Customized Solution</h2>
				<h2 class="our-solution-title text-primary visible-md">Customized Solution</h2>
                <p class="our-text"> Need a customized solution? We can design and build bespoke solutions for enterprise customers.</p>
            </div>
            
            <div class="row">
                <div class="col-xs-12">
                    <h2 class="alert alert-info" id="recallsolution">Our Crisis Manager solution is offered free of charge for non-profit organizations. Please contact us to find out more.</h2>
                </div>
            </div>
            <?php */?>
                      
        </div><!-- /our-boxes-holder-->
        
                        
        
    </div> <!-- /container -->
</div> <!-- /our-solutions -->



<?php /*?><div id="home-why" class="section" style="padding-bottom: 25px;" data-center="background-color:rgb(239, 239, 239);" data-center-top="background-color:rgb(255, 255, 255);">
    <div class="container">
        <h1>Collaboration Features</h1>
        <h2><big>Anytime, Anywhere Collaboration</big></h2>
        <h2>From the start, your teams are equipped with the right plans, people, files, task lists, and alerts. Team members in any location or time zone work together as if they were in the same room. They share information, access files, manage tasks, send alerts and ensure that no details are overlooked.</h2>
        <div class="row">
        
        
        
            <div class="col-md-4 col-sm-6 text-center">
                <i class="fa fa-video-camera fa-2x text-light-blue"></i>
                <h4 class="text-light-blue">Video Conferencing</h4>
            </div>
        
            <div class="col-md-4 col-sm-6 text-center">
                <i class="fa fa-smile-o fa-2x text-light-blue"></i>
                <h4 class="text-light-blue">Instant Messaging</h4>
            </div>
        
            <div class="col-md-4 col-sm-6 text-center">
                <i class="fa fa-mobile fa-2x text-light-blue"></i>
                <h4 class="text-light-blue">SMS Notification</h4>
            </div>
            
        	<div class="col-sm-12 hidden-xs hidden-sm" style="padding: 15px;"></div>
            
            <div class="col-md-4 col-sm-6 text-center">
                <i class="fa fa-envelope fa-2x text-light-blue"></i>
                <h4 class="text-light-blue">Private Messaging</h4>
            </div>
        
            <div class="col-md-4 col-sm-6 text-center">
                <i class="fa fa-headphones fa-2x text-light-blue"></i>
                <h4 class="text-light-blue">Private Forums</h4>
            </div>
        
            <div class="col-md-4 col-sm-6 text-center">
                <i class="fa fa-file-zip-o fa-2x text-light-blue"></i>
                <h4 class="text-light-blue">Document Repository</h4>
            </div>
        
        
        
        
        
        </div>
        
        
        <div class="row centered hidden">
            <div class="col-md-6 col-sm-6 item">
                <img src="<?php echo base_url()?>assets/frontpage/corporate/images/icons/home-center.png">
                <h4>Crisis Command Centre</h4>
                <p>Coordinate crisis activities with ease and efficiency.</p>
            </div>
            
            <div class="col-md-6 col-sm-6 item">
                <img src="<?php echo base_url()?>assets/frontpage/corporate/images/icons/home-incident.png">
                <h4>Pre-Incident Planning</h4>
                <p>Create crisis scenarios and assign tasks and responsibilities.</p>
            </div>
                      
            <div class="col-md-6 col-sm-6 item">
                <img src="<?php echo base_url()?>assets/frontpage/corporate/images/icons/home-communicate.png">
                <h4>Contact Management</h4>
                <p style="padding-bottom:20px">Easily maintain stakeholder contact details.</p>
            </div>
            
            <div class="col-md-6 col-sm-6 item">
                <img src="<?php echo base_url()?>assets/frontpage/corporate/images/icons/home-anytime.png">
                <h4>Crisis Document Repository</h4>
                <p>Centrally store all your mission-critical crisis documents.</p>
            </div>

        </div> <!-- /row-->

    </div> <!-- /container -->
</div> <!-- /homepage-what --><?php */?>


<div id="more-reasons" class="section" data-center="background-color:rgb(239, 239, 239);" data-center-top="background-color:rgb(255, 255, 255);">

    <div class="container">
       <?php /*?> <h1>More Reasons Why</h1>
        
        <h2>Because your reputation is everything.</h2><?php */?>
       
        <h1><?php echo $threepartshead[0]['title']?></h1>
        
        <h2><?php echo $threepartshead[0]['content']?></h2>
        
        <div class="row centered">
        	<?php foreach($threeparts as $r=>$value){ ?>
            
            <div class="col-md-4 col-sm-12 item">
                <img src="<?php echo base_url().'uploads/avatars/'.$value['image']?>">
                <h4><?php echo $value['title'] ?></h4>
                <p><?php echo $value['content'] ?></p>
            </div>
                
			<?php } ?>
        
            
            <?php /*?><div class="col-md-4 col-sm-12 item">
                <img src="<?php echo base_url()?>assets/frontpage/corporate/images/icons/quote-greenstraight.png">
                <h4 class="text-center">Straightforward and Simple to Use</h4>
                <p>CrisisFlo is easy to use, yet advanced enough to support major disasters. With the intuitive interface, users quickly become productive, even with minimal training.</p>
            </div>
            <div class="col-md-4 col-sm-12 item">
                <img src="<?php echo base_url()?>assets/frontpage/corporate/images/icons/quote-greenbulb.png">
                <h4 class="text-center">Smarter Crisis Command Centre</h4>
                <p>A real-time dashboard puts up-to-the-minute information at your fingertips. Every authorized user has a clear, centralized view of activities, even across multiple events and departments —information that's critical for decision-making.</p>
            </div>
            
            <div class="col-md-4 col-sm-12 item">
                <img src="<?php echo base_url()?>assets/frontpage/corporate/images/icons/quote-greenipad.png">
                <h4 class="text-center">Tablet Ready</h4>
                <p>When you're dealing with a crisis, you're probably not sitting at your desk. You need to drive the crisis response from any location. CrisisFlo is tablet compatible, putting the power of the Crisis Command Centre in the palm on your hand.</p>
            </div><?php */?>

        </div> <!-- /row -->

    </div> <!-- /container -->
</div> <!-- /more-reasons -->





 
    <!-- Modal for new task -->
    <div class="portfolio-modal modal fade" id="loginblue" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">
                        
                            <h1>Request Demo</h1>
                            <hr class="star-primary">
                      


                            <form id="trial_form" class="text-left">
                                <div class="alert trial_message alert-dismissable text-left" style="display: none;">
                                    Form successfully submitted.
                                </div>   
                        
                                

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">First Name</label>
                                            <input type="text" class="form-control" name="first_name"/>
                                        </div> 
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Last Name</label>
                                            <input type="text" class="form-control" name="last_name"/>
                                        </div> 
                                    </div>
                                </div>
                                             
                                <div class="form-group">
                                    <label class="control-label">Organization/Business Name</label>
                                    <input type="text" class="form-control" name="organization"/>
                                </div> 
            
                                <div class="form-group">
                                    <label class="control-label">Address</label>
                                    <input type="text" class="form-control geocomplete" placeholder="Enter location" name="address"/>
                                    <input type="hidden" class="form-control" name="lat"/>
                                    <input type="hidden" class="form-control" name="lng"/>
                                </div> 

                                <div class="form-group">
                                    <label class="control-label">Business Email</label>
                                    <input type="text" class="form-control" name="email"/>
                                </div> 
                                                     

                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label class="control-label">Country Calling Code</label>
                                            <select class="form-control" name="country_code">
                                                <option value="" data-code="">Select..</option>
                                                <?php
                                                    foreach($mobile_code as $r=>$mc){
                                                        echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'">'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                
                                    <div class="col-sm-7">
                                        <div class="form-group mobile_input">
                                            <label class="control-label">Mobile Number</label>
                                            <div class="input-group">
                                              <span class="input-group-addon addon-shortcode">+</span>
                                              <input type="text" class="form-control" placeholder="Mobile Number" name="mobile">
                                            </div>
                                            <input type="hidden" name="country_short" value=""/>
                                        </div>
                                    </div>
                                </div>
                                             
                                <div class="form-group hidden">
                                    <label class="control-label">Subscription Plan</label>
                                    <input type="hidden" name="plan" value="5"/>
                                </div>    
                                             
                                <p class="text-right hidden"><small>By signing up, you agree to CrisisFlo's <a href="#" class="terms_btn">Terms of Use</a> and <a href="#" class="privacy_btn">Privacy Policy</a></small></p>
                                             
                                <div class="form-group">
                                    <button class="btn btn-primary btn-rounded pull-right">Submit</button>
                                </div> 

                                             
                            </form>
                                             
                        

              
                        </div><!--modal-body-->
                    </div>
                </div>
            </div>
            
            
            
        </div>
    </div><!--portfolio-modal-->
                
          