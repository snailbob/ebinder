	<?php if($this->uri->segment(1) != '' && $this->session->userdata('id') != '') { ?>
        </div><!--/.row-->
    </div>
    <!-- /.container -->
    <?php }?>                


    <div id="footer">
    	<div class="container">
        	<div class="row">
            <div class="col-md-12 col-md-offset-1 addLeftPadding">
            	<!--<div id="footer-tweets" class="col-md-4 col-sm-4 col-xs-12">
                	<h3>@CrisisFloOnline Tweets</h3>

                   	<div id="epact-tweets"></div>
                </div>-->
                
            	
            	<div class="col-md-3 col-md-offset-3 col-xs-12">
                	<h3>Company</h3>
                    <ul>
	                	<li><a target="_self" href="<?php echo base_url()?>about">About</a></li>
	                	<!--<li><a target="_self" href="<?php echo base_url()?>">Partnerships</a></li>-->
	                	<li><a target="_self" href="<?php echo base_url()?>contact">Contact Us</a></li>
	                	<li><a target="_self" href="<?php echo base_url().'faq'?>">FAQ</a></li>
                    </ul>
                </div>
            	<div class="col-md-3 col-xs-12">
                	<h3>Location</h3>
                    <ul>
	                	<li>Suite 3, Level 8, 179 Queen St<br />
Melbourne, Victoria<br />Australia 3000</li>
                    </ul>
                </div>
            </div><!--offset2-->
            </div> <!-- /row -->
            
            <div id="footer-bottom" class="row">
            	<a id="footer-linkedin" target="_blank" href="http://www.linkedin.com/company/crisis-flo" class="footer-icon"></a>
            	<a id="footer-twitter" target="_blank" href="https://twitter.com/" class="footer-icon"></a>
            	<a id="footer-facebook" target="_blank" href="https://www.facebook.com/" class="footer-icon" style="display: none;"></a>
            	<!--<a id="footer-youtube" target="_blank" href="http://www.youtube.com/" class="footer-icon"></a>
            	<a id="footer-gplus" target="_blank" href="https://plus.google.com/" class="footer-icon"></a>
            	<a id="footer-rss" target="_blank" href="<?php echo base_url()?>" class="footer-icon"></a>-->
                
                <div id="copyright">
                	<ul>
                    	<li class="text">&copy; 2015 Risk Monitor Pty Ltd</li>
                    </ul>
                	<ul>
                    
                    	<li><a href="#" class="terms_btn">Terms of Use</a></li>
                    	<li><a href="#" class="privacy_btn">Privacy Policy</a></li>
                    </ul>
                    
                    <?php /*?><div>
                    <a href="https://monitor18.sucuri.net/verify.php?r=969146be47e4cdd09378fd563400956cfeca2f907b">
                        <img src="<?php echo base_url().'assets/img/sucuri-verified-badge-1-medium.png'; ?>" style="width: 144px"/>
                    </a>                
					<script type="text/javascript" src="https://sealserver.trustwave.com/seal.js?style=invert"></script>                    
                    </div><?php */?>
                </div>
            </div>
        </div> <!-- /container -->
    </div> <!-- /footer -->
    
    
    <div id="top-button" class="animated fadeInUp" data-0="display: none" data-100="display: block">
    	<img src="<?php echo base_url()?>assets/frontpage/corporate/images/top-button.png" />
    </div>

    
    
    
    <!-- termsModal -->
    <div class="portfolio-modal modal fade" id="termsModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="modal-body">
                        
                            <h1>Terms of Use</h1>
                            <hr class="star-primary">
                            
                            <div class="text_content text-left"></div>
              
                        </div><!--modal-body-->
                        <div class="modal-footer hidden">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
        </div>
    </div><!--portfolio-modal-->
              
              
    <!-- privacyModal -->
    <div class="portfolio-modal modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">
                        
                            <h1>Privacy Policy</h1>
                            <hr class="star-primary">
                            
                            
                            <div class="text_content text-left"></div>
    

              
                        </div><!--modal-body-->
                        <div class="modal-footer hidden">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>
            
            
            
        </div>
    </div><!--portfolio-modal-->
    


    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">
                        
                            <h1>Forgot Password</h1>
                            
                            
                
                            <div class="omb_login">
                
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                        <hr class="star-primary">
                                        <p class="text-center">Enter your email below and we will send you instructions on how to reset your password</p>
                                    	
                                        <form class="omb_loginForm" id="forgot_form">
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                                    <input type="email" class="form-control" name="email" placeholder="Email Address">
                                                </div>
                                            </div>
                
                                            <div class="form-group">
                                                <button class="btn btn-primary btn-block" type="submit">Send</button>
                                            </div>
                        
                                        </form>
                                    </div>
                                </div>
                            </div>
                                                                

                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->    


    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="signupModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">
                            <h1>Sign Up</h1>
                            <div class="omb_login">
                            
<?php /*?>                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                     
                                        <hr class="star-primary">
                                        <p class="text-center">Already have an account? <a href="#" class="login_btn">Log In</a><br /><br /></p>
                                    	<div class="alert alert-success" style="display: none;">
                                        	<button type="button" class="close" onclick="$(this).closest('.alert').hide();" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            <span class="my-text"></span>
                                        </div>
                                    
                                    </div>
                                </div>

                                <div class="row text-left">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3">	
                                        <div class="row social-btns">
                                            <div class="col-sm-6">
                                                <div class="form-groupx">
                                                    <button type="button" class="btn btn-default btn-block"><i class="fa fa-google-plus text-danger"></i> Google+</button>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-6">
                                                <div class="form-groupx">
                                                    <button type="button" class="btn btn-primary btn-block"><i class="fa fa-linkedin"></i> LinkedIn</button>
                                                </div>
                                            </div> 
                                        </div>
                                        <hr class="star-primary">
                     
                                        <form class="omb_loginForm" id="signup_form">
                                            <input type="hidden" class="form-control" name="google_id">
                                            <input type="hidden" class="form-control" name="facebook_id">
                                            <input type="hidden" class="form-control refer_from" name="refer_from" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; } else { echo $this->session->userdata('id'); }?>">
                                            
                                            <input type="hidden" class="form-control" name="google_img">
                                            <input type="hidden" class="form-control" name="fb_img">
                                            <input type="hidden" class="form-control" id="social_type" name="social_type">
                                            <input type="hidden" class="form-control" id="social_site" name="social_site">
                                            

                                            <div class="form-group hidden">
                                                <div class="dropdown dropdown-form dropdown-signup">
                                                  <button class="btn btn-primary btn-block btn-outline"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-chevron-down fa-fw pull-right"></i>
                                                    Sign up as <span class="u_type"> - Agency</span>
                                                  </button>
                                                  <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                                    <li><a href="#" data-type="Customer">Customer</a></li>
                                                  </ul>
                                                </div>
                                                
                                                <input type="hidden" name="utype" value="Customer" />
                                            </div>
                                            
                                            <div class="form-group studio_only hidden">
                                            
                                            
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                    <input type="text" class="form-control" name="name" placeholder="Agency Name">
                                                </div>
                                            </div>
                                            

                                            <div class="row notfor_studio">
                                            	<div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                            <input type="text" class="form-control" name="first_name" placeholder="First Name">
                                                        </div>
                                                    </div>

                                                </div>
                                            
                                            	<div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                            <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                            
                                            </div>
                                            
                          
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-building-o fa-fw"></i></span>
                                                    <input type="text" class="form-control the_company_field" name="company" placeholder="Company Name">
                                                </div>
                                            </div>
                          
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-globe
 fa-fw"></i></span>
                                                    <input type="text" class="form-control the_domain_field" name="domain" placeholder="Company Domain">
                                                </div>
                                                <p class="text-muted the_domain_text hidden"></p>
                                            </div>
                                            
                          
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                                    <input type="email" class="form-control" name="email" placeholder="Email Address">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                                    <input  type="password" class="form-control" name="password" placeholder="Password">
                                                </div>
                                            </div>
                
                                            <div class="form-group">
                                                <button class="btn btn-primary btn-block" type="submit">Sign Up</button>
                                            </div>
                        
                                        </form>
                                    </div>
                                </div><?php */?>
                            </div>
                        

                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->    



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">
                        
                            <h1>Log In</h1>
                
                            <div class="omb_login">
                                <div class="row" style="margin-bottom: 15px;">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                        <hr class="star-primary">
                                        <p class="text-center">Don't have an account? <a href="#" class="signup_btn">Sign Up</a></p>
                                    	<div class="alert <?php if($this->session->flashdata('info')) { echo 'alert-success'; } else { echo 'hidden'; }?>">
                                        	<button type="button" class="close" onclick="$(this).closest('.alert').addClass('hidden');" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            <span class="my-text">
                                            	<?php echo $this->session->flashdata('info')?>
                                            </span>
                                        </div>
                                    
                                    </div>
                                </div>
                                
                        
<?php /*?>                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">	
                                        <div class="row social-btns">
                                            <div class="col-sm-6">
                                                <div class="form-groupx">
                                                    <button type="button" class="btn btn-default btn-block"><i class="fa fa-google-plus text-danger"></i> Google+</button>
                                                </div>
                                            </div>
                                            
                                            <div class="col-sm-6">
                                                <div class="form-groupx">
                                                    <button type="button" class="btn btn-primary btn-block"><i class="fa fa-linkedin"></i> LinkedIn</button>
                                                </div>
                                            </div> 
                                        </div>
                                    
                                        <hr class="star-primary">

                                        <form class="omb_loginForm" id="login_form">
                                            <input type="hidden" class="form-control" name="google_id">
                                            <input type="hidden" class="form-control" name="facebook_id">
                                            <input type="hidden" class="form-control refer_from" name="refer_from" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; } else { echo $this->session->userdata('id'); }?>">
                                            <input type="hidden" class="form-control" name="from_buyform" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; }?>">

                                            <div class="form-group hidden">
                                                <div class="dropdown dropdown-form dropdown-login">
                                                  <button class="btn btn-primary btn-block btn-outline"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-chevron-down fa-fw pull-right" style="margin-right: 10px;"></i>
                                                    Log In as <span class="u_type"> - Agency</span>
                                                  </button>
                                                  <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                                    <li><a href="#" data-type="Customer">Customer</a></li>
                                                  </ul>
                                                </div>
                                                
                                                <input type="hidden" name="utype" value="Customer" />
                                            </div>

                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                                    <input type="email" class="form-control" name="email" placeholder="Email Address">
                                                </div>
                                            </div>
                          
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                                    <input  type="password" class="form-control" name="password" placeholder="Password">
                                                </div>
                                            </div>
                          
                                            <div class="form-group hidden">
                                            	<label>Log In as</label>
                                                <input type="checkbox" name="usertype" checked>
                                            </div>
                          
                                            <div class="form-group">
                                                <button class="btn btn-primary btn-block" type="submit">Log In</button>
                                            </div>
                
                        
                                        </form>

                                        <p class="omb_forgotPwd">
                                            <a href="#" class="forgot_btn">Forgot password?</a>
                                        </p>
                                    </div>
                                </div>	    	
                            </div>
                        <?php */?>
                        
	
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->        
    
    
    <!-- Modal -->
    <div class="modal fade" id="myLoadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
            <h1 class="text-center" style="margin-top: 100px">
                <i class="fa fa-spinner fa-spin"></i><br />
                <small>Loading..</small>
            </h1>
      </div>
    </div>    
    
    <script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/browserDetection.js"></script>
        
    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.fittext.js"></script>
    <script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/validate/jquery.validate.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/validate/additional-methods.min.js"></script>

    <!-- Plugin JavaScript -->
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    <script src="<?php echo base_url() ?>assets/plugins/geocomplete/jquery.geocomplete.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    
    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/datatables-bs3.js"></script>
    
    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bootbox/bootbox.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/jquery.form/jquery.form.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/dependencies/moment.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bs-switch/js/bootstrap-switch.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/silviomoreto-bootstrap-select/js/bootstrap-select.min.js"></script>

	<?php if((($this->uri->segment(2) == 'profile' && $this->session->userdata('stripe_id') == '') || ($this->uri->segment(1) == 'buy' || $this->uri->segment(1) == '')) && $this->session->userdata('customer_type') != 'N') { ?>
    <!-- Plugin JavaScript -->
	<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/validate/jquery.payment.min.js"></script>
    <!-- If you're using Stripe for payments -->
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <?php } ?>

	<?php if($this->uri->segment(1) == 'settings' && $this->uri->segment(2) == 'profile') { ?>
    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/cropper/js/cropper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/cropper/js/crop-avatar.js"></script>
	<?php } ?>
    
    <!-- Custom Theme JavaScript -->
   <?php /*?> <script src="<?php echo base_url() ?>assets/js/creative.js"></script><?php */?>
    <script src="<?php echo base_url() ?>assets/js/script.js"></script>

	<script>
		if(uri_1 == 'signup' || uri_1 == 'login'){
		
		  var googleUser = {};
		  var startApp = function() {
			gapi.load('auth2', function(){
			  // Retrieve the singleton for the GoogleAuth library and set up the client.
			  auth2 = gapi.auth2.init({
				client_id: '253167228155-mnp9rtvui3t45cben3u1vag21n4vcupk.apps.googleusercontent.com',
				cookiepolicy: 'single_host_origin',
				// Request scopes in addition to 'profile' and 'email'
				//scope: 'additional_scope'
			  });
			  attachSignin(document.getElementById('customBtn'));
			});
		  };
		
		  function attachSignin(element) {
			console.log(element.id);
			auth2.attachClickHandler(element, {},
				function(googleUser) {
					var profile = googleUser.getBasicProfile();
					console.log("ID: " + profile.getId()); // Don't send this directly to your server!
					console.log('Full Name: ' + profile.getName());
					console.log('Given Name: ' + profile.getGivenName());
					console.log('Family Name: ' + profile.getFamilyName());
					console.log("Image URL: " + profile.getImageUrl());
					console.log("Email: " + profile.getEmail());
				
					var $mdl = $('#myLoadingModal');
					$mdl.modal('show');
				
					var data = {
						id: profile.getId(),
						full_name: profile.getName(),
						first_name: profile.getGivenName(),
						last_name: profile.getFamilyName(),
						avatar: profile.getImageUrl(),
						email: profile.getEmail(),
						
					};
					console.log(data);
					$.post(
						base_url+'formsubmits/googleplus',
						{data: data, uri_1: uri_1},
						function(res){
							console.log(res);
							
							if(res.type == 'signup'){
								window.location.href = base_url + 'signup';
							}
							else{
//								console.log(res.userdata.domain);
//								window.location.href = 'http://'+res.userdata.domain+'.ebinder.com.au';
								if(res.userdata.customer_type == 'Y'){
									window.location.href = base_url+'business';
								}
								else{
									window.location.href = 'http://'+res.userdata.domain+'.ebinder.com.au';//base_url+'dashboard';
								}
							
						
							}
						},
						'json'
					).done(function(res){
						$mdl.modal('hide');
					}).error(function(err){
						console.log(err);
					});
					
					
					
	//			  document.getElementById('name').innerText = "Signed in: " +
	//				  googleUser.getBasicProfile().getName();
				}, function(error) {
				  alert(JSON.stringify(error, undefined, 2));
				});
		  }
		  
		  startApp();	 
		}
		   
    </script>
            
    
    <script type="text/javascript" src="//platform.linkedin.com/in.js">
        api_key: 8198kchnjgi54x
        authorize: true
        onLoad: onLinkedInLoad
    </script>
    
    <script type="text/javascript">
		if(uri_1 == 'signup' || uri_1 == 'login'){
			// Setup an event listener to make an API call once auth is complete
			function onLinkedInLoad() {
				//IN.Event.on(IN, "auth", getProfileData);
			}
//		
//			// Handle the successful return from the API call
//			function onSuccess(data) {
//				console.log(data);
//			}
//		
//			// Handle an error response from the API call
//			function onError(error) {
//				console.log(error);
//			}
//		
//			// Use the API call wrapper to request the member's basic profile data
			function getProfileData() {
				IN.API.Raw("/people/~").result(onSuccess).error(onError);
			}
//				
//			function liAuth(){
//			   IN.User.authorize(function(){
//				   callback();
//			   });
//			}	
			
			var linkedData = {};
			// Handle the successful return from the API call
			function onSuccess(profile) {
				console.log(profile);
				
				//$('#MyLinkedInButton').addClass('disabled');
			
				var data = {
					id: profile.id,
					full_name: profile.firstName+' '+profile.lastName,
					first_name: profile.firstName,
					last_name: profile.lastName,
					avatar: '',
					email: profile.emailAddress,
					
				};
				linkedData = data;
				console.log(data);
				

			}
			
			// Handle an error response from the API call
			function onError(error) {
				console.log(error);
			}
			
			$(document).ready(function(e) {
			
				$("#MyLinkedInButton").on('click',function () {
					
					console.log(linkedData);
					var data = linkedData;
			
					if(typeof(linkedData.id) !== 'undefined'){
						var $mdl = $('#myLoadingModal');
						$mdl.modal('show');
				
						$.post(
							base_url+'formsubmits/linkedin',
							{data: data, uri_1: uri_1},
							function(res){
								console.log(res);
								
								if(res.type == 'signup'){
									window.location.href = base_url + 'signup';
								}
								else{
//									console.log(res.userdata.domain);
//									window.location.href = 'http://'+res.userdata.domain+'.ebinder.com.au';
									if(res.userdata.customer_type == 'Y'){
										window.location.href = base_url+'business';
									}
									else{
										window.location.href = 'http://'+res.userdata.domain+'.ebinder.com.au';//base_url+'dashboard';
									}
							
								}
							},
							'json'
						).done(function(res){
							$mdl.modal('hide');
						}).error(function(err){
							console.log(err);
						});
		
			
					
					}
					return false;
				});
			
				$("#MyLinkedInButton").bind('click',function () {
					
					IN.User.authorize();
					
					return false;
				});
				
				IN.Event.on(IN, 'auth', function(){
					/* your code here */
					//for instance
					console.log('auth success');
					IN.API.Raw("people/~:(id,first-name,last-name,location,email-address)?format=json").result(onSuccess).error(onError);
			
				});
	
				
			});
		}	
		
    </script>

 

</body>
</html>
