<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    	
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="twitter:widgets:csp" content="on">
    

    <title>E-Binder</title>
        
  <script src="https://apis.google.com/js/api:client.js"></script>

    <?php /*?><meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="253167228155-mnp9rtvui3t45cben3u1vag21n4vcupk.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>

    <script>
      function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);

      };
    </script><?php */?>

<!-- Custom CSS -->
<?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/creative.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css"><?php */?>


<link href="<?php echo base_url()?>assets/frontpage/corporate/css/bootstrap.css" rel="stylesheet">

<!-- Plugin CSS -->
<?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css" type="text/css"><?php */?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/dataTables.bootstrap.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/datatables.css" type="text/css">

<!-- Plugin CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/cropper.min.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/crop-avatar.css" type="text/css">


<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bs-switch/css/bootstrap3/bootstrap-switch.min.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/silviomoreto-bootstrap-select/css/bootstrap-select.min.css" type="text/css">




<link href="<?php echo base_url()?>assets/frontpage/corporate/css/main.css" rel="stylesheet">
<?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" type="text/css"><?php */?>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,500,300,100' rel='stylesheet' type='text/css'>

<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700' rel='stylesheet' type='text/css'>

<link href="<?php echo base_url()?>assets/frontpage/corporate/css/animate.min.css" rel="stylesheet">
<link rel="shortcut icon" href="<?php echo base_url()?>assets/frontpage/images/favicon.ico" type="image/x-icon" />
<link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url()?>assets/frontpage/corporate/css/jquery.mmenu.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/libs/head.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/us/variablesCorporate"></script>
<script>
	var base_url = '<?php echo base_url() ?>';
	var uri_1 = '<?php echo $this->uri->segment(1) ?>';
	var uri_2 = '<?php echo $this->uri->segment(2) ?>';
	var uri_3 = '<?php echo $this->uri->segment(3) ?>';
	var uri_4 = '<?php echo $this->uri->segment(4) ?>';
	var user_id = '<?php echo $this->session->userdata('id') ?>';
	var user_name = '<?php echo $this->session->userdata('name') ?>';
	var user_location = '<?php echo $this->session->userdata('location') ?>';
	var user_address = '<?php echo $this->session->userdata('address') ?>';
	var user_type = '<?php echo $this->session->userdata('type') ?>';
	var stripe_id = '<?php echo $this->session->userdata('stripe_id') ?>';
	var customer_type = '<?php echo $this->session->userdata('customer_type') ?>';
	
</script>


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  
<![endif]-->
    
</head>


    
 <body id="skrollr-body" class="root home landing">
 
 
   	
	<?php
		//vars
		$colors = $this->common->colors();
		$usertype = $this->session->userdata('type');
		$opposite_type = 'agency';
		if($usertype != 'customer'){
			$opposite_type = 'customer';
		}
	?>
 
 
 
    <div id="browser-detection" class="alert alert-warning" style="display:none; position:relative;">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		Your browser is not supported by Transit Insurance. For a better experience, use one of our <a class='preventDefault' data-toggle='modal' data-target='#modal-browser-options'>supported browsers.</a>
	</div>
	<div id="cookies-detection" class="alert alert-danger" style="display:none; position:relative;">
		*__browser_cookies_disabled*
	</div>
    <nav id="my-menu">
       <ul>
			<?php /*?><?php if($this->session->userdata('id') != '') {?>
            <li>
                <a href="<?php echo base_url().'dashboard' ?>" class="<?php if($this->uri->segment(1) == 'dashboard' && $this->uri->segment(2) == '') { echo 'on-page'; }?>"><i class="fa fa-dashboard fa-fw"></i> <?php echo $this->session->userdata('name') ?></a>
            </li>
            
            <li><a href="<?php echo base_url().'settings/profile' ?>" class="<?php if($this->uri->segment(2) == 'profile' && $this->uri->segment(3) == '') { echo 'on-page'; }?>"><i class="fa fa-user fa-fw"></i> <?php if($this->session->userdata('customer_type') == 'Y') { echo ucfirst($usertype); } ?> Profile</a></li>
         
         
            <li class="hidden"><a href="<?php echo base_url().'dashboard/referrals' ?>" class="<?php if($this->uri->segment(2) == 'referrals') { echo 'on-page'; }?>"><i class="fa fa-pencil-square fa-fw"></i> Referrals <?php if($this->common->active_referral_count($this->session->userdata('id')) > 0) { ?><span class="label label-danger label-circle pull-right"><?php echo $this->common->active_referral_count($this->session->userdata('id')); ?></span></a></li><?php } ?>
            
            <li class="hidden"><a href="<?php echo base_url().'dashboard/transactions' ?>" class="<?php if($this->uri->segment(2) == 'transactions') { echo 'on-page'; }?>"><i class="fa fa-exchange fa-fw"></i> Transactions</a></li>
            
            <?php if($this->session->userdata('customer_type') == 'N'){ ?>
            <li class="hidden"><a href="<?php echo base_url().'dashboard/quotes' ?>" class="<?php if($this->uri->segment(2) == 'quotes') { echo 'on-page'; }?>"><i class="fa fa-file-text-o fa-fw"></i> Manage Quotes</a></li>
            <?php } ?>

            
            <li class="<?php if($this->session->userdata('customer_type') != 'N') { echo 'hidden';} ?>">
                <a href="<?php echo base_url().'customers' ?>" class="<?php if($this->uri->segment(1) == 'customers') { echo 'on-page'; }?>"><i class="fa fa-users fa-fw"></i> Manage Customers</a>
            </li>
            
            <li class="hidden <?php if($this->session->userdata('type') != 'customer' || $this->uri->segment(1) == '') { echo 'hidden';} ?>">
                <a href="<?php echo base_url().'buy' ?>" class="<?php if($this->uri->segment(1) == 'buy') { echo 'on-page'; }?>"><i class="fa fa-cart-plus fa-fw"></i> <?php echo ($this->session->userdata('customer_type') == 'N') ? 'Get Quote' : 'Buy Insurance' ?></a>
            </li>
            
			<?php if($this->session->userdata('customer_type') != 'N' && $this->session->userdata('type') == 'customer'){ ?>
            <li><a href="<?php echo base_url().'settings/profile/payment' ?>" class="<?php if($this->uri->segment(3) == 'payment') { echo 'on-page'; }?>"><i class="fa fa-credit-card fa-fw"></i> Payment Method</a></li>
            <?php } ?>
            <li><a href="<?php echo base_url().'settings/profile/pw' ?>" class="<?php if($this->uri->segment(3) == 'pw') { echo 'on-page'; }?>"><i class="fa fa-key fa-fw"></i> Password</a></li>
            <li><a href="<?php echo base_url().'dashboard/logout' ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
            
            <?php } else { ?><?php */?>
            <li class=""><a href="<?php echo base_url().'login' ?>" class="login_btnx"><i class="fa fa-sign-in fa-fw"></i> Log In</a></li>
            <li class=""><a href="<?php echo base_url().'signup' ?>" class="login_btnx"><i class="fa fa-user fa-fw"></i> Sign Up</a></li>
            <?php // } ?>
       </ul>
    </nav>
    
    <div id="main-container">
    <div class="mobile-spacer visible-xs hidden-sm hidden-md hidden-lg" data-0="margin-top:120px" data-1="margin-top:60px"></div>
        
      <!-- Static navbar -->
      <div class="navbar navbar-default navbar-static-top" role="navigation">
       	<div class="container">
            <div class="navbar-header">
              <button type="button" id="open-slider" class="navbar-toggle" data-0="top:0" data-1="top:-3px">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <div class="navbar-brand">
              	<a href="<?php echo base_url(); if($this->session->userdata('id') != '') { echo 'dashboard'; }?>"><img id="nav-logo" class="animated fadeIn animated-top the-logo" data-0="display: inline-block" data-50="display:none" src="<?php echo base_url()?>assets/frontpage/corporate/images/crisisflo-logo-medium.png"/></a>
                <div class="mobile-menu visible-xs">
				  
					<?php // if($this->session->userdata('id') == '') { ?>
                    <a href="#" id="btn-mini-login" class="btn btn-primary btn-rounded btn-outline login_btnx hidden" data-type="Customer">Log In</a>
                    <?php /*?><?php } else {?>	
                    <a href="<?php echo base_url().'dashboard' ?>" class="btn btn-primary btn-rounded btn-outline"><?php echo $this->session->userdata('name')?></a>
                    <?php } ?>	<?php */?>		

				
                    
                </div>
              </div>
            </div>
            <div class="navbar-collapse collapse">
            
            
                <div class="mobile-menu hidden-xs text-right" style="padding-top: 35px;">
					<?php // if($this->session->userdata('id') == '') { ?>
                    <a href="<?php echo base_url()."signin"; ?>" id="btn-mini-login" class="btn btn-primary btn-xs btn-rounded btn-outline login_btnx hidden" data-type="Customer" >Log In</a>	
                    <?php // } ?>		

                    
                </div>
            
                
				<?php // if($this->session->userdata('id') == '') { ?>
                <div class="hidden-xs"><a href="<?php echo base_url().'login'?>" class="pull-right btn btn-primary btn-rounded btn-outline login_btnxx" style="margin-top: -6px;" data-type="Customer">Log In</a></div>			
				<?php // } ?>		
      		
            
            

              <ul class="nav navbar-nav pull-right main_lg_nav">
              
                
                    <li>
                    	<a href="<?php echo base_url().'about' ?>" class="<?php if($this->uri->segment(1) == 'about') { echo 'on-page'; }?>">About Us</a>
                    </li>
                
                    <li>
                    	<a href="<?php echo base_url().'contact' ?>" class="<?php if($this->uri->segment(1) == 'contact') { echo 'on-page'; }?>">Contact Us</a>
                    </li>
                
                    <li class="<?php // echo ($this->session->userdata('id') != '') ? 'hidden' : ''; ?>">
                    	<a href="<?php echo base_url().'signup' ?>" class="<?php if($this->uri->segment(1) == 'signup') { echo 'on-page'; }?>">Sign Up</a>
                    </li>
                    

      
              	<?php /*?><?php if($this->session->userdata('id') != '' && $this->uri->segment(1) == '') { ?>

                
                    <li>
                    	<a href="<?php echo base_url().'dashboard' ?>" class="<?php if($this->uri->segment(1) == 'dashboard') { echo 'on-page'; }?>"><?php echo $this->session->userdata('name') ?></a>
                    </li>
                    
                    <li><a href="<?php echo base_url().'settings/profile' ?>" class="<?php if($this->uri->segment(2) == 'profile' && $this->uri->segment(3) == '') { echo 'on-page'; }?>"><?php if($this->session->userdata('customer_type') == 'Y') { echo ucfirst($usertype); } ?> Profile</a></li>
                    
                    
                    <li class="<?php if($this->session->userdata('customer_type') != 'N') { echo 'hidden';} ?>">
                    	<a href="<?php echo base_url().'customers' ?>" class="<?php if($this->uri->segment(1) == 'customers') { echo 'on-page'; }?>">Manage Customers</a>
                    </li>
                    
                    <li class="hidden <?php if($this->session->userdata('type') != 'customer' || $this->uri->segment(1) == '') { echo 'hidden';} ?>">
                    	<a href="<?php echo base_url().'buy' ?>" class="<?php if($this->uri->segment(1) == 'buy') { echo 'on-page'; }?>"><?php echo ($this->session->userdata('customer_type') == 'N') ? 'Get Quote' : 'Buy Insurance' ?></a>
                    </li>
                    
                
                    <li class="dropdown my-dropdown my-dropdown-merge my-dropdown-bottom-right">
                    
                        <a id="dLabel" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="<?php if($this->uri->segment(3) != '') { echo 'on-page'; }?>">
                            Settings
                            <span class="caret"></span>
                          </a>

                        <ul class="dropdown-menu" aria-labelledby="dLabel">
                        
                    		<?php if($this->session->userdata('customer_type') == 'N' && $this->session->userdata('type') == 'customer'){ ?>
                            <li class="hidden"><a href="<?php echo base_url().'settings/profile/payment' ?>"><i class="fa fa-credit-card fa-fw"></i> Payment Method</a></li>
                            <?php } ?>
                        
                            <li class="<?php echo ($this->session->userdata('customer_type') == 'N') ? '' : 'hidden' ?>"><a href="<?php echo base_url().'settings/logo' ?>"><i class="fa fa-image fa-fw"></i> Update Logo</a></li>
                        
                            <li><a href="<?php echo base_url().'settings/profile/pw' ?>"><i class="fa fa-key fa-fw"></i> Password</a></li>
                            <li><a href="<?php echo base_url().'dashboard/logout' ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                        </ul>                    
                    </li>                
                
                <?php } ?>
              	<?php if($this->session->userdata('id') != '' && $this->uri->segment(1) != '') { ?>
                  
                    <li class="dropdown my-dropdown my-dropdown-merge my-dropdown-bottom-right">
                    
                        <a id="dLabel" data-target="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" class="<?php if($this->uri->segment(3) != '') { echo 'on-page'; }?>">
                            Settings
                            <span class="caret"></span>
                          </a>

                        <ul class="dropdown-menu" aria-labelledby="dLabel">
							<?php if($this->session->userdata('customer_type') == 'N' && $this->session->userdata('type') == 'customer'){ ?>
                            <li class="hidden"><a href="<?php echo base_url().'settings/profile/payment' ?>"><i class="fa fa-credit-card fa-fw"></i> Payment Method</a></li>
                            <?php } ?>
                        
                            <li class="<?php echo ($this->session->userdata('customer_type') == 'N') ? '' : 'hidden' ?>"><a href="<?php echo base_url().'settings/logo' ?>"><i class="fa fa-image fa-fw"></i> Update Logo</a></li>
                        
                            <li><a href="<?php echo base_url().'settings/profile/pw' ?>"><i class="fa fa-key fa-fw"></i> Password</a></li>
                            <li><a href="<?php echo base_url().'dashboard/logout' ?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                        </ul>                    
                    </li>                
                <?php } ?><?php */?>
                
                
              </ul>
              
              
              
            </div><!--/.nav-collapse -->
   	 	</div> <!-- /container -->
      </div>

	<?php /*?><?php if(($this->uri->segment(1) != '' && $this->uri->segment(1) != 'contact' && $this->uri->segment(1) != 'about') && $this->session->userdata('id') != '') { ?>
    <div class="container-fluid section-simple section-simple-admin main_container main_container_admin">

        <div class="row row-nogutter">

            <div class="col-lg-2 col-md-3 col-sm-4 hidden-xs sidebar_nav">
                <div class="main_nav" <?php if($this->uri->segment(1) == 'settings') { echo 'style="display:none"'; }?>>
                    <a href="<?php echo base_url().'dashboard' ?>" class="list-group-item <?php if($this->uri->segment(1) == 'dashboard' && $this->uri->segment(2) == '') { echo 'active'; }?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    
                    <a href="<?php echo base_url().'dashboard/referrals' ?>" class="hidden list-group-item <?php if($this->uri->segment(2) == 'referrals') { echo 'active'; }?>"><i class="fa fa-pencil-square fa-fw"></i> Referrals <?php if($this->common->active_referral_count($this->session->userdata('id')) > 0) { ?><span class="label label-danger label-circle pull-right"><?php echo $this->common->active_referral_count($this->session->userdata('id')); ?></span></a><?php } ?>
                    
                    <a href="<?php echo base_url().'dashboard/transactions' ?>" class="hidden list-group-item <?php if($this->uri->segment(2) == 'transactions') { echo 'active'; }?>"><i class="fa fa-exchange fa-fw"></i> Transactions</a>
                    <?php if($this->session->userdata('customer_type') == 'N'){ ?>
                    <a href="<?php echo base_url().'dashboard/quotes' ?>" class="hidden list-group-item <?php if($this->uri->segment(2) == 'quotes') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> Manage Quotes</a>
                    <?php } ?>

                    <a href="<?php echo base_url().'customers' ?>" class="list-group-item <?php if($this->uri->segment(1) == 'customers') { echo 'active'; }?> <?php if($this->session->userdata('customer_type') != 'N') { echo 'hidden';} ?>"><i class="fa fa-users fa-fw"></i> Manage Customers</a>
                    
                    <a href="<?php echo base_url().'buy' ?>" class="hidden list-group-item <?php if($this->uri->segment(1) == 'buy') { echo 'active'; }?>"><i class="fa fa-cart-plus fa-fw"></i> <?php echo ($this->session->userdata('customer_type') == 'N') ? 'Get Quote' : 'Buy Insurance' ?></a>
                
                    <a href="javascript:;" class="list-group-item" onclick="$('.main_nav').slideUp(); $('.settings_nav').slideDown()"><i class="fa fa-gear fa-fw"></i> Settings</a>
                    
                </div>
                
                <div class="settings_nav" <?php if($this->uri->segment(1) != 'settings') { echo 'style="display:none"'; }?>>
                    <a href="javascript:;" class="list-group-item  <?php if($this->uri->segment(2) == 'settings') { echo 'label-gray'; }?>" onClick="$('.settings_nav').slideUp(); $('.main_nav').slideDown()"><i class="fa fa-arrow-left fa-fw"></i> Settings</a>
                    <a href="<?php echo base_url().'settings/profile' ?>" class="list-group-item <?php if($this->uri->segment(2) == 'profile' && $this->uri->segment(3) == '') { echo 'active'; }?>"><i class="fa fa-user fa-fw"></i> Profile</a>
                    <?php if($this->session->userdata('customer_type') != 'N') { ?>
                    <a href="<?php echo base_url().'settings/profile/payment'; ?>" class="list-group-item <?php if($this->uri->segment(3) == 'payment') { echo 'active'; }?>"><i class="fa fa-credit-card fa-fw"></i> Payment Method</a>
                    <?php } ?>
                    
                    
                    <a class="<?php echo ($this->session->userdata('customer_type') == 'N') ? '' : 'hidden' ?> list-group-item <?php if($this->uri->segment(2) == 'logo') { echo 'active'; }?>" href="<?php echo base_url().'settings/logo' ?>"><i class="fa fa-image fa-fw"></i> Update Logo</a>
                    
                    <a href="<?php echo base_url().'settings/profile/pw'; ?>" class="list-group-item <?php if($this->uri->segment(3) == 'pw') { echo 'active'; }?>"><i class="fa fa-key fa-fw"></i> Change Password</a>
                   
                    <a href="<?php echo base_url().'dashboard/logout'; ?>" class="list-group-item"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                
                </div>
                
                
            </div><!--end sidebar_nav-->

    <?php }?>   <?php */?>             