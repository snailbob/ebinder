


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="twitter:widgets:csp" content="on">

    <?php
      $topheadtitle = $this->master->getRecords('admin_contents', array('type'=>'topheadtitle'));
    ?>
    <title><?php echo $topheadtitle[0]['title']?></title>
		<link rel="shortcut icon" href="<?php echo base_url()?>assets/frontpage/images/favicon.ico" type="image/x-icon" />

	  <script src="https://apis.google.com/js/api:client.js"></script>


		<link href="<?php echo base_url()?>assets/frontpage/corporate/css/bootstrap.css" rel="stylesheet">

		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">


		<style media="screen">

			.btn-linkedin{
				background-color: #0077b5;
				border-color: #0077b5;
			}
			.btn-linkedin:hover, .btn-linkedin:active, .btn-linkedin:focus{
				background-color: #07689b;
			}


			.btn-twitter{
				background-color: #33ccff;
				border-color: #33ccff;
			}
			.btn-twitter:hover, .btn-twitter:active, .btn-twitter:focus{
				background-color: #2fb5e2;
				border-color: #2fb5e2;
			}
		</style>
		<script>
			var base_url = '<?php echo $this->common->base_url() ?>';
			var uri_1 = '<?php echo $this->uri->segment(1) ?>';
			var uri_2 = '<?php echo $this->uri->segment(2) ?>';
			var uri_3 = '<?php echo $this->uri->segment(3) ?>';
			var uri_4 = '<?php echo $this->uri->segment(4) ?>';
			var user_id = '<?php echo $this->session->userdata('id') ?>';
			var user_name = '<?php echo $this->session->userdata('name') ?>';
			var user_location = '<?php echo $this->session->userdata('location') ?>';
			var user_address = '<?php echo $this->session->userdata('address') ?>';
			var user_type = '<?php echo $this->session->userdata('type') ?>';
			var stripe_id = '<?php echo $this->session->userdata('stripe_id') ?>';
			var customer_type = '<?php echo $this->session->userdata('customer_type') ?>';
			var domain_name = '<?php echo (isset($_GET['the_domain'])) ? $_GET['the_domain'] : $this->common->the_domain(); ?>';

		</script>


  </head>

  <body>

			<div class="rowx social-btns" style="padding: 15px;">
					<div class="col-sm-6">
							<div class="form-group">
									<?php /*?><div class="g-signin2" data-onsuccess="onSignIn">Sign Up</div><?php */?>


									<button type="button" class="btn btn-default btn-block google-auth-btn" id="customBtn"><i class="fa fa-google-plus text-danger"></i> Google+</button>
							</div>
					</div>

					<div class="col-sm-6">
							<div class="form-group">
									<?php /*?><script type="in/Login"></script><?php */?>

									<button type="button" class="btn btn-primary btn-block btn-linkedin linkedin-auth-btn" id="MyLinkedInButton"><i class="fa fa-linkedin"></i> LinkedIn</button>
							</div>
					</div>

					<div class="col-sm-6">
							<div class="form-group">
									<?php /*?><script type="in/Login"></script><?php */?>

									<a href="<?php echo base_url().'twitter'; ?>" class="btn btn-primary btn-block btn-twitter"><i class="fa fa-twitter"></i> Twitter</a>
							</div>
					</div>

					<hr class="star-primary">

			</div>


<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/jquery.min.js"></script>

<script type="text/javascript">
		//disable window resize
		// var size = [window.width,window.height];  //public variable
		// $(window).resize(function(){
		//     window.resizeTo(250,250);
		// });


		var action_messages = '';
		var current_class_booked = {};

		//get action messages
		$(document).ready(function(e) {

		//	$.validator.addMethod("alphanumeric", function(value, element) {
		//		return this.optional(element) || /^\w+$/i.test(value);
		//	}, "Letters, numbers, and underscores only please");

			var jqxhr = $.post(
				base_url+'formsubmits/action_messages',
				{},
				function(res){
					console.log(res);
					action_messages = res;
				},
				'json'
			).error(function(err){
				console.log(err);
			});

			jqxhr.done(function(){
				var banners = action_messages.banners;
				var c = 2;
				for(p in banners){
					console.log(banners[p].image);
					console.log($('.home-jumbo.slide'+c));

					var img = base_url+'uploads/avatars/'+banners[p].image;
					if(banners[p].image != ''){
						$('.home-jumbo.slide'+c).css('background-image', 'url(' + img + ')');
					}
					c++;
				}
			});
		});



		if(true){ //uri_1 == 'signup' || uri_1 == 'login' || uri_1 == ''

			$(document).ready(function(e) {
				// $('.google-auth-btn').on('click', function(){
				// 	$('#customBtn').click();
				// });
				// $('.linkedin-auth-btn').on('click', function(){
				// 	$('#MyLinkedInButton').click();
				// });


						});

			var googleUser = {};
			var startApp = function() {
			gapi.load('auth2', function(){
				// Retrieve the singleton for the GoogleAuth library and set up the client.
				auth2 = gapi.auth2.init({
				client_id: '253167228155-mnp9rtvui3t45cben3u1vag21n4vcupk.apps.googleusercontent.com',
				cookiepolicy: 'single_host_origin',
				// Request scopes in addition to 'profile' and 'email'
				//scope: 'additional_scope'
				});
				attachSignin(document.getElementById('customBtn'));
			});
			};

			function attachSignin(element) {
			console.log(element.id);
			auth2.attachClickHandler(element, {},
				function(googleUser) {
					var profile = googleUser.getBasicProfile();
					console.log("ID: " + profile.getId()); // Don't send this directly to your server!
					console.log('Full Name: ' + profile.getName());
					console.log('Given Name: ' + profile.getGivenName());
					console.log('Family Name: ' + profile.getFamilyName());
					console.log("Image URL: " + profile.getImageUrl());
					console.log("Email: " + profile.getEmail());


					var data = {
						id: profile.getId(),
						full_name: profile.getName(),
						first_name: profile.getGivenName(),
						last_name: profile.getFamilyName(),
						avatar: profile.getImageUrl(),
						email: profile.getEmail(),

					};
					console.log(data);
					$.post(
						base_url+'formsubmits/googleplus',
						{data: data, domain_name: domain_name, uri_1: uri_1, id: "<?php echo (isset($_GET['id'])) ? $_GET['id'] : ''?>"},
						function(res){
							console.log(res);
							//return false;

							if(res.type == 'signup'){
								alert('User not found.');
								// var d = new Date();
								// var n = d.getMilliseconds();
								//
								// window.location.href = base_url + '?n=' + n + '#signup';
							}
							else if(res.type == 'complete_signup'){
								$('#customBtn').html('<i class="fa fa-google-plus text-danger"></i> Connnected to Google+').addClass('disabled');
								alert(action_messages.success.complete_profile_via_social);
							}
							else{
								//window.opener.location.reload();
								window.close();
								return false;
								//
								// if(res.userdata.super_admin == 'Y'){
								// 	// window.location.href = base_url+'webmanager';
								// 	console.log(base_url+'webmanager');
								// }
								// else{
								// 	// window.location.href = base_url+'panel'; //'http://'+res.userdata.domain+'.ebinder.com.au/panel';
								// 	console.log(base_url+'panel');
								// }


							}
						},
						'json'
					).done(function(res){


					}).error(function(err){
						console.log(err);
					});



	//			  document.getElementById('name').innerText = "Signed in: " +
	//				  googleUser.getBasicProfile().getName();
				}, function(error) {
					alert(JSON.stringify(error, undefined, 2));
				});
			}

			startApp();
		}

		</script>


		<script type="text/javascript" src="//platform.linkedin.com/in.js">
				api_key: 8198kchnjgi54x
				authorize: true
				onLoad: onLinkedInLoad
		</script>

		<script type="text/javascript">
		if(true){ //uri_1 == 'signup' || uri_1 == 'login' || uri_1 == ''
			// Setup an event listener to make an API call once auth is complete
			function onLinkedInLoad() {
				IN.Event.on(IN, "auth", getProfileData);
			}

			// Use the API call wrapper to request the member's basic profile data
			function getProfileData() {
				if(IN.User.isAuthorized()){
					IN.API.Raw("people/~:(id,first-name,last-name,location,email-address)?format=json").result(onSuccess).error(onError);
				}
			}


			var linkedData = {};
			var linked_fetch = 0;
			// Handle the successful return from the API call
			function onSuccess(profile) {
				var linked = $("#MyLinkedInButton").data('linked');

				//$('#MyLinkedInButton').addClass('disabled');

				var data = {
					id: profile.id,
					full_name: profile.firstName+' '+profile.lastName,
					first_name: profile.firstName,
					last_name: profile.lastName,
					avatar: '',
					email: profile.emailAddress
				};

				console.log('onLinkedInLoad', profile);
				console.log(data);
				linkedData = data;

				if(typeof(linked) !== 'undefined'){

					$.post(
						base_url+'formsubmits/linkedin',
						{data: data, domain_name: domain_name, uri_1: uri_1, id: "<?php echo (isset($_GET['id'])) ? $_GET['id'] : ''?>"},
						function(res){
							console.log(res);

							if(res.type == 'signup'){
								alert('User not found.');
								// var d = new Date();
								// var n = d.getMilliseconds();
								//
								// window.location.href = base_url + '?n=' + n + '#signup';
							}
							else if(res.type == 'complete_signup'){
								$('#MyLinkedInButton').html('<i class="fa fa-linkedin"></i> Connnected to LinkedIn').addClass('disabled');
								alert(action_messages.success.complete_profile_via_social);
							}

							else{
								//window.opener.location.reload();
								window.close();
								return false;
								//
								// if(res.userdata.super_admin == 'Y'){
								// 	window.location.href = base_url+'webmanager';
								// 	console.log(base_url+'webmanager');
								// }
								// else{
								// 	window.location.href = base_url+'panel'; //'http://'+res.userdata.domain+'.ebinder.com.au/panel';
								// 	console.log(base_url+'panel');
								// }

							}
						},
						'json'
					).done(function(res){

					}).error(function(err){
						console.log(err);
					});

				}
				linked_fetch ++;


			}

			// Handle an error response from the API call
			function onError(error) {
				console.log(error);
			}

			$(document).ready(function(e) {

				$("#MyLinkedInButton").on('click',function () {

					console.log('MyLinkedInButton', linkedData);
					var data = linkedData;
					var $self = $(this);
					$self.data('linked',data);

					if(typeof(linkedData.id) !== 'undefined'){

						$.post(
							base_url+'formsubmits/linkedin',
							{data: data, domain_name: domain_name, uri_1: uri_1, id: "<?php echo (isset($_GET['id'])) ? $_GET['id'] : ''?>"},
							function(res){
								console.log(res);

								if(res.type == 'signup'){
									alert('User not found.');
									// var d = new Date();
									// var n = d.getMilliseconds();
									//
									// window.location.href = base_url + '?n=' + n + '#signup';
								}
								else if(res.type == 'complete_signup'){
									$('#MyLinkedInButton').html('<i class="fa fa-linkedin"></i> Connnected to LinkedIn').addClass('disabled');

									alert(action_messages.success.complete_profile_via_social);
								}

								else{
									//window.opener.location.reload();
									window.close();
									return false;
									//
									// if(res.userdata.super_admin == 'Y'){
									// 	window.location.href = base_url+'webmanager';
									// 	console.log(base_url+'webmanager');
									// }
									// else{
									// 	window.location.href = base_url+'panel'; //'http://'+res.userdata.domain+'.ebinder.com.au/panel';
									// 	console.log(base_url+'panel');
									// }

								}
							},
							'json'
						).done(function(res){

						}).error(function(err){
							console.log(err);
						});

					}

					return false;
				});

				$("#MyLinkedInButton").bind('click',function () {
					IN.User.authorize();
					//
					// IN.Event.on(IN, 'auth', function(){
					// 	/* your code here */
					// 	//for instance
					// 	console.log('auth success');
					// 	IN.API.Raw("people/~:(id,first-name,last-name,location,email-address)?format=json").result(function(res){
					// 		console.log('peps', res);
					// 	}).error(onError);
					// });

					return false;
				});


			});
		}

		</script>



  </body>
</html>
