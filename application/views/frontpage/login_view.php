<div class="section section-simple">
    <div class="container">
    
    	<div class="row">
        	<div class="col-sm-12">
            
                <h1 class="text-center">Log In</h1>
                        
                    
                            <p class="text-center">Don't have an account? <a href="#" class="signup_btn">Sign Up</a></p>
                
                            <div class="omb_login">
                            
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                    	<div class="alert alert-success" style="display: none;">
                                        	<button type="button" class="close" onclick="$(this).closest('.alert').hide();" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            <span class="my-text"></span>
                                        </div>
                                    
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                    	<div class="alert alert-danger" style="display: none;">
                                        	<button type="button" class="close" onclick="$(this).closest('.alert').hide();" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            <span class="my-text"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row omb_row-sm-offset-3">
                                    <div class="col-xs-12 col-sm-6">	
                                        <form class="omb_loginForm" id="login_form">
                                            <input type="hidden" class="form-control" name="google_id">
                                            <input type="hidden" class="form-control" name="facebook_id">

                                            <div class="form-group hidden">
                                                <div class="dropdown dropdown-form dropdown-login">
                                                  <button class="btn btn-primary btn-block btn-outline btn-lg"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fa fa-chevron-down fa-fw pull-right"></i>
                                                    Login as <span class="u_type"> - Agency</span>
                                                  </button>
                                                  <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                    <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                                    <li><a href="#" data-type="Customer">Customer</a></li>
                                                  </ul>
                                                </div>
                                                

                                            </div>

                                            <div class="form-group">
                                                <div class="input-group input-group-lg">
                                                    <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                                    <input type="email" class="form-control" name="email" placeholder="Email Address">
                                                </div>
                                            </div>
                          
                                            <div class="form-group">
                                                <div class="input-group input-group-lg">
                                                    <span class="input-group-addon"><i class="fa fa-lock fa-fw"></i></span>
                                                    <input  type="password" class="form-control" name="password" placeholder="Password">
                                                </div>
                                            </div>
                      
                                            <div class="form-group">
                                            	<select name="utype" class="form-control input-lg">
                                                	<option value="Agency">Agency</option>
                                                	<option value="Customer">Customer</option>
                                                </select>
                                            </div>

                                            <div class="form-group hidden">
                                            	<label>Login as</label>
                                                <input type="checkbox" name="usertype" checked>
                                            </div>
                          
                                            <div class="form-group">
                                                <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
                                            </div>
                
                        
                                        </form>
                                    </div>
                                </div>
                                <div class="row omb_row-sm-offset-3">
                                    <div class="col-xs-12 col-sm-3 hidden-xs">
                                        <p class="omb_noAcc">
                                            
                                        </p>
                                    </div>
                                    <div class="col-xs-12 col-sm-3">
                                        <p class="omb_forgotPwd">
                                            <a href="#" class="forgot_btn">Forgot password?</a>
                                        </p>
                                    </div>
                                </div>	            
                            </div>	            
            
            </div>
        </div>
    </div> <!-- /container -->
</div> <!-- /homepage-what -->
