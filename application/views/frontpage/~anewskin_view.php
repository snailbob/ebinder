<div id="landing-feature">


	<div id="feature-slide-1x" class="home-jumbo jumbotron" data-0="background-position:right 0px;" data-654="background-position:right -350px;">
		<div class="white-overlay visible-xs visible-sm visible-md visible-lg"></div>
	    <div class="container">
	      <div class="row">
	        <div id="slide-1-content" class="col-md-10 col-md-offset-1 text-center animated fadeInDown animated-top" style="opacity:1">
	            <?php /*?><h1>Lorem ipsum dolor sit amet, atqui nihil denique ad est, vis verear periculis ex</h1><?php */?>
                <?php echo $admin_info[0]['banner_text'] ?>
            	<p class="text-center">
                    <a class="btn btn-primary btn-lg btn-rounded buy_transit_btn">
                    	Buy Single Cargo Transit Policy
                    </a>
                </p>
	        </div> <!-- /col-med -->
	      </div> <!-- /row -->
	    </div> <!-- /container -->
	</div> <!-- /jumbotron -->

</div> <!-- /landing-feature -->

<div id="home-nav" class="sub-nav hidden">
    <div class="container">
        <ul class="nav nav-pills nav-justified">
            <li class="active menuItem"><a id="yolo" class="menuScroll" data-scrollto="#home-what">What is Transit Insurance?</a></li>
            <li class="menuItem"><a class="menuScroll" data-scrollto="#our-solutions">Our Solution</a></li>
            <li class="menuItem"><a class="menuScroll" data-scrollto="#home-why">Collaboration Features</a></li>
            <li class="menuItem"><a class="menuScroll" data-scrollto="#more-reasons">More Reasons Why</a></li>
            <li class="menuItem"><a class="menuScroll" data-scrollto="#pricing">Pricing</a></li>
        
         </ul>
    </div> <!-- /container -->
</div> <!-- /homepage-nav -->

<div id="home-what" class="section" >
    <div class="container">
        <?php /*?><h1> Facilisi nominati eu eos</h1>
        
        <h2 style="font-size: 21px;">Sea cu nisl graeci iuvaret. Sumo mucius possit ex duo, duo an ubique nominati incorrupte. Fugit dicta cetero per ei, cum tamquam instructior et. Mea voluptatum persequeris philosophia cu, probatus partiendo reprehendunt at his.<br /><br />

Duo an illum mediocrem, ut nam feugiat recteque. Per ei deleniti consetetur. Te mei impedit lucilius abhorreant. Nisl case eu his. Eu sit legimus molestie delicatissimi, ea apeirian volutpat pri.


Has diam ornatus in, ei persius discere complectitur qui. Cu nam dicat ubique voluptaria, cu quo sale utinam, cum ei modo affert quaerendum. Fierent democritum id pri. Cu eum iusto errem, nam sonet affert aperiam ex, at ridens saperet sit. Mea quidam efficiantur concludaturque id, in paulo nostro est. Eu has putant eripuit, an pro lucilius pericula.</h2><?php */?>

		<?php echo $admin_info[0]['light_section'] ?>
        
        
    </div> <!-- /container -->
</div> <!-- /homepage-what -->





<div id="our-solutions" class="hidden section" style="border-bottom: 1px solid #eee;" data-center="background-color:rgb(240, 247, 252);" data-center-top="background-color:rgb(255, 255, 255);">
   
    </div> <!-- /container -->
</div> <!-- /pricing -->




    
    <!-- Modal for new task -->
    <div class="portfolio-modal modal fade" id="buyNowModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="modal-body">
                        
                            <h1 class="buy_title hidden">Buy Single Marine Transit Policy</h1>
                            <hr class="star-primary hidden">
             
                            <!-- progressbar -->
                            <ul id="progressbar" class="progressbar-main">
                                <li class="active" data-target="tab-transit">Transit</li>
                                <li data-target="tab-cargo">Cargo</li>
                                <li data-target="tab-quote">Quote</li>
                                <li data-target="tab-details">Policy</li>
                                <li data-target="tab-confirm">Confirm</li>
                                <li data-target="tab-payment">Payment</li>
                            </ul>   
                            <div class="clearfix"></div>                       
                                <!-- Tab panes -->
                                <div class="tab-content tab-content-buy">
                                
                               
                                    <div role="tabpanel" class="tab-pane active" id="tab-transit">
                                        <form id="buy_form">
                                    	<h4 class="">&nbsp;</h4>
                                        
                                         <div class="row">
                                           
                                            <div class="col-sm-6 form-group">
                                            	<label for="shipment_date">Date of Shipment *</label>
                                                <input type="text" class="form-control input-date" id="shipment_date" name="shipment_date" value="<?php echo $shipment_date;?>" />                       
                                            </div>
                                           
                                            <div class="col-sm-6 form-group">
                                            	<label for="vessel_name">Name of Vessel/Aircraft *</label>
                                                <input type="text" class="form-control" id="vessel_name" name="vessel_name" value="<?php echo $vessel_name;?>" />                       
                                            </div>
                                        </div>
                                                                 
                                         <div class="row">
                                           
                                            <div class="col-sm-6 form-group">
                                            	<label for="transitfrom">Transit From *</label>
                                                <select class="form-control" name="transitfrom" id="transitfrom" data-live-search="true">
                                                  <option value="">Select Country</option>
                                                  <?
                                                  foreach($countries as $r=>$value){
                                                      $price = 0;
													  $referral = '';
                                                      if(isset($country_prices[$value['country_id']])){
                                                          $zone = $country_prices[$value['country_id']] - 1;
														  
														  //zone multiplier
														  if(isset($zone_multiplier[$zone])){
															  $price = $zone_multiplier[$zone];
														  }
                                                      }
													  
													  if(isset($country_referral[$value['country_id']])){
														  $referral = 'yes';
													  }
                                                      
                                                      echo '<option value="'.$value['country_id'].'" data-price="'.$price.'" data-referral="'.$referral.'" data-iso="'.$value['iso2'].'"';
                                                      if($transitfrom == $value['country_id']){
                                                          echo ' selected="selected"';											  	
                                                      }
                                                      echo '>'.$value['short_name'].' ('.$value['iso2'].')</option>';
                                                            
                                                  }?>
                                                  
                                                  
                                                </select>
        
                                            </div>
                                            <div class="col-sm-6 form-group">
                                            	<label for="portloading">Port of Loading</label><br />
                                                <a href="#" class="btn btn-default btn-block select_port_btn <?php if($transitfrom == '') { echo 'disabled'; } ?>" <?php if($portloading != '') { echo 'style="display: none;"'; } ?> data-type="portloading">Select Port of Loading</a>
                                                 
                                                <input type="hidden" name="portloading" id="portloading" value="<?php echo $portloading; ?>"> 

                                                <div class="btn-group" <?php if($portloading == '') { echo 'style="display: none;"'; } ?>>
                                                  <a href="#" class="btn btn-default select_port_btn" data-type="portloading" ><?php if(is_numeric($portloading)) { echo $this->common->selectedport($portloading); } else { echo $portloading; }?></a>
                                                  <a href="#" class="btn btn-default remove_selected_btn"><i class="fa fa-times-circle"></i></a>
                                                </div>

                                            </div>
                                         </div>

                                         <div class="row">
                                           
                                            <div class="col-sm-6 form-group">
                                            	<label for="transitto">Transit To *</label>
        	
                                                <select class="form-control" name="transitto" id="transitto" data-live-search="true">
                                                  <option value="">Select Country</option>
                                                  <?
                                                  foreach($countries as $r=>$value){
                                                      $price = 0;
													  $referral = '';
                                                      if(isset($country_prices[$value['country_id']])){
                                                          $zone = $country_prices[$value['country_id']] - 1;
														  
														  //zone multiplier
														  if(isset($zone_multiplier[$zone])){
															  $price = $zone_multiplier[$zone];
														  }
                                                      }
													  
													  if(isset($country_referral[$value['country_id']])){
														  $referral = 'yes';
													  }
                                                      
                                                      echo '<option value="'.$value['country_id'].'" data-price="'.$price.'" data-referral="'.$referral.'" data-iso="'.$value['iso2'].'"';
                                                      if($transitto == $value['country_id']){
                                                          echo ' selected="selected"';											  	
                                                      }
                                                      echo '>'.$value['short_name'].' ('.$value['iso2'].')</option>';
                                                            
                                                  }?>
                                                  
                                                </select>
                                            </div>
                                            <div class="col-sm-6 form-group">
                                            	<label for="portdischarge">Port of Discharge</label><br />
                                                <a href="#" class="btn btn-default btn-block select_port_btn <?php if($transitto == '') { echo 'disabled'; } ?>" <?php if($portdischarge != '') { echo 'style="display: none;"'; } ?> data-type="portdischarge">Select Port of Discharge</a>
                                                 
                                                <input type="hidden" name="portdischarge" id="portdischarge" value="<?php echo $portdischarge; ?>"> 

                                                <div class="btn-group" <?php if($portdischarge == '') { echo 'style="display: none;"'; } ?>>
                                                  <a href="#" class="btn btn-default select_port_btn" data-type="portdischarge"><?php if(is_numeric($portdischarge)) { echo $this->common->selectedport($portdischarge); } else { echo $portdischarge; }?></a>
                                                  <a href="#" class="btn btn-default remove_selected_btn"><i class="fa fa-times-circle"></i></a>
                                                </div>
                                            </div>
                                          </div><!--row-->
                                          

                                          <div class="row">
                                          
                                              <div class="col-sm-6 form-group">
                                                <label for="currency">Currency * <i class="fa fa-info-circle text-info" data-toggle="tooltip" data-placement="top" data-title="(*) Not supported on American Express."></i></label>
                                                <select class="form-control" name="currency" id="currency" data-live-search="true">
                                                  <option value="">Select Currency</option>
                                                  <?
                                                  foreach($currencies as $r=>$value){
    
                                                      echo '<option value="'.$value['currency'].'"';
                                                      if($currency == $value['currency']){
                                                          echo ' selected="selected"';                                                      }
                                                      echo '>'.$value['name'].'</option>';
                                                            
                                                  }?>
                                                  
                                                </select>                                            
                                              </div>    
                                              
                                              <div class="col-sm-6 form-group">
                                                    <label for="invoice">Insured Value *</label>
                                                    <input type="text" class="form-control" name="invoice" id="invoice" placeholder="Invoice Value" value="<?php echo $invoice; ?>" data-max="<?php echo $max_insured; ?>">
                                              </div>
                                          </div>
                                          
                                          
                                          <div class="form-group">

                                            	<button class="btn btn-warning pull-right" type="submit">Continue</button>
                                          </div>
                                          
                                        </form>

                                    </div><!--tab-transit-->
                                   
                                    <div role="tabpanel" class="tab-pane" id="tab-cargo">
                                        <form id="cargotab_form">
                                            <h4 class="text-left">&nbsp;</h4>

                                          <div class="form-group fg-cargo-cat">
                                          	<div class="row">
                                            	<div class="col-sm-12">
                                                    <label>Cargo *</label><br />
                                                    <p><a href="#cargoCatModal" data-toggle="modal" data-controls-modal="cargoCatModal" class="btn btn-primary btn-block">Cargo Description</a></p>
                                             
                                                    <select class="form-control hidden" name="cargocat" id="cargocat" data-live-search="true">
                                                      <option value="">Select Cargo</option>
                                                      <?
                                                      foreach($cargos as $r=>$value){
														$acargo_price = $value['multiplier'];
														$acargo_ref = $value['referral'];
														if(count($the_agency) > 0) {
															if(isset($cargo_prices[$value['id']])) {
																$acargo_price = str_replace(',','', $cargo_prices[$value['id']]);
															}
														}
													  
														  echo '<option value="'.$value['id'].'"';
														  echo ' data-info="'.$acargo_price.','.$acargo_ref.','.$value['uninsurable'].'"';
                                                          if($cargocat == $value['id']){
                                                              echo ' selected="selected"';                                                          }
                                                          echo '>'.$value['hs_code'].' &middot; '.$value['description'].'</option>';
                                                                
                                                      }?>
                                                      
                                                    </select>                                            
                                                    
                                                
                                                </div>
                                                <div class="col-sm-12">

											   
                                                    <?php /*?><label>&nbsp;</label><br /><?php */?>
                                                    <div class="genre_inputs" style="display: none;">
                                                        <?php /*?><input type="hidden" name="cargocat" data-info="<?php echo $cargo_price ?>,<?php echo $cargo_max_val?>,<?php echo $bindoption?>" value="<?php echo $cargocat; ?>"/><?php */?>
                                                    </div>
                                                    
                                                    <div class="well well-sm genre_well" <?php if($cargocat == '') { echo 'style="display: none;"'; }?>>
                                                        <?php /*?><span class="label label-info"><?php echo $this->common->cargo_name($cargocat); ?> <i class="fa fa-times close-gnre"></i></span><?php */?>
                                                        <div class="thumbnail thumbnail-cargo"><button type="button" aria-label="Close" class="close close-cargo"><span aria-hidden="true">&times;</span></button><?php echo $this->common->cargo_name($cargocat); ?></div>
                                                    </div>
                                                </div>
                                            </div>

                                          </div>
                                          
                                          <div class="form-group">
                                                <label>Brief Description of Goods *</label><br />
                                                <input type="text" class="form-control input-count-char" name="goods_desc" id="goods_desc" value="<?php echo $goods_desc; ?>" />
                                                <span class="small text-muted pull-right"><?php echo $goods_count; ?> characters remaining.</span>
                                          </div>
                                          
                                          <div class="form-group">
                                                <label>Transportation *</label><br />
        

                                                <div class="the_genres">
                                                    <div class="genre_list gutter-sm">
													  <?php
													  $trans_iall = '';
												      $count = 0;
                                                      foreach($transportations as $r){
														  $price = 0;
														  if(isset($transportation_prices[$count])){
															$price = $transportation_prices[$count];
														  }
                                                    ?>
                                                    <div class="col-xs-6 col-md-3">
                                                    <a href="#" class="thumbnail text-center genre_thumbs <?php if($transmethod == $r) { echo 'active" '; } ?>">
                                                    
														
                                                        <span>
															<?php
															//hide icon for last item
															if(($count + 1) != count($transportations)){
															
																$trans_i = '<i class="'.$trans_icon[$count].' fa-fw"></i>';	
																echo $trans_i;
																$trans_iall .= $trans_i;
															}
															else{
																echo $trans_iall;
															}
															
															 echo '<br>'.$r?>
                                                        </span>

                                                    <div class="checkbox hidden">                                        
                                                      <label>
                                                        <input type="radio" name="transmethod" <?php echo ' data-price="'.$price.' "value="'.$r.'"';
														  if($transmethod == $r) {
																echo 'checked="checked" '; 
														  }
														
														?>
                                                            
                                                             /><span><?php echo $r?></span>
                                                        
                                                      </label>
                                                    </div> <!--checkbox-->
                                                    </a>                                   
                                                    </div><!--col-xs-6-->
                                                    <?php $count++; } ?>
                                                    
                                                    </div>
                                                </div>
                                             
                                          </div>                                          
                                          <div class="form-group">
                                                <input type="hidden" name="base_rate" value="<?php echo $the_agency[0]['base_rate']; ?>">
                                                <input type="hidden" name="minimum_premium" value="<?php echo $the_agency[0]['minimum_premium']; ?>">
                                                <input type="hidden" name="cargo_price" value="<?php echo $cargo_price ?>" />
                                                <input type="hidden" name="cargo_max_val" value="<?php echo $cargo_max_val ?>" />
                                                <input type="hidden" name="premium" value="<?php echo $premium ?>" />
                                                <input type="hidden" name="default_deductible" value="<?php echo $selected_deductible ?>" />
                        
                        
                                                <a href="#" class="btn btn-default buy_back_btn pull-left">Back</a>
                                            	<button class="btn btn-warning pull-right" type="submit">Continue</button>
                                          </div>
                                          
                                          

                                        </form>
                                    </div><!--tab-cargo-->
                                    <div role="tabpanel" class="tab-pane" id="tab-quote">
                                    	<h4 class="text-left">Quote</h4>
                                        <div class="well well-premium text-center">
                                            <h1 style="font-size:75px !important">23</h1>
                                        </div>
                                        
                                        <?php if(count($deductibles) > 1){ ?>
                                        <div class="form-group">
                                            <h4 class="text-left">Deductible</h4>
                                            <select name="deductible_dd" class="form-control">
                                            	<?
													foreach($deductibles as $r=>$value){
														echo '<option value="'.$value['rate'].'"';
														if($value['rate'] == $selected_deductible){
															echo ' selected="selected"';
														}
														echo '>$'.$value['deductible'].'</option>';
													}
												?>
                                            </select>
                                        
                                        </div>
                                        <?php } ?>
                                        
                                    
                                        <?php
										$referral_comment = $this->session->userdata('referral_comment');
										if($referral_comment != ''){
											echo '<blockquote><p>'.$referral_comment.'</p><footer>- comment from <cite title="Source Title">Admin</cite></footer></blockquote>';
										}
                                        
                                        if(count($policy_docs) > 0) {
                                            echo '<p class="lead text-right policydoc"><a href="'.base_url().'uploads/policyd/'.$policy_docs[0]['name'].'" target="_blank" class="btn btn-link btn-lg"><i class="fa fa-file-pdf-o"></i> View Policy Document</a></p>';	
                                            
                                        }
                                        ?>
                                        
                                        <div class="form-group">
                                            <a href="#" class="btn btn-default buy_back_btn pull-left">Back</a>
                                            <button class="btn btn-warning quote_continue_btn pull-right" data-id="<?php echo $this->session->userdata('id')?>">Continue</button>
                                        </div>
                              
                                    </div><!--tab-quote-->
                                   
                                   
                                    <div role="tabpanel" class="tab-pane" id="tab-details">
                                    	<h4>&nbsp;</h4>
                                        

                                        <div class="select_consignee <?php if(count($mycustomers) == 0) { echo 'hidden'; } ?>">
                                        	<form class="mycustomer_form">
                                                <div class="well form-group">
                                                    <label>Select Existing Consignor</label><br />
        
                                                    <select name="mycustomers" class="form-control">
                                                        <option value="">Select Consignor</option>
                                                        <?php
                                                        foreach($mycustomers as $r=>$value){
                                                            $details = unserialize($value['details']);
                                                            echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';	
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group hidden">
                                                    <a href="#" class="btn btn-info" onclick="$('.new_consignee').removeClass('hidden'); $(this).closest('.select_consignee').addClass('hidden');">New Consignee</a>
                                                    <button type="submit" class="btn btn-warning pull-right">Continue</button>
                                                    
                                                </div>
                                                
                                                
                                            </form>    
                                        </div><!--agent mycustomers-->
                                        
                                        
                                        <div class="well">
                                    	<h4>Consignor's Details</h4>
                                        <form id="single_purchase_form" class="single_purchase_form">
                                        
                                         <div class="row">
                                           
                                            <div class="col-sm-12 form-group">
                                                <div class="wellx">
                                                    <h3 class="hidden">Single Purchase</h3>
                                                    <div class="row gutter-md">
                                                        <div class="col-md-6 form-group">
                                                            <label>First Name</label>
                                                            <input type="text" class="form-control" name="first" placeholder="First Name" value="<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('first_name') : ''?>"/>
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label>Last Name</label>
                                                            <input type="text" class="form-control" name="last" placeholder="Last Name" value="<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('last_name') : '' ?>"/>
                                                        </div>
                                                        
                                                    </div><!--name row-->
                                                                
                                                    <div class="row gutter-md">
                                                        <div class="col-sm-12 form-group">
                                                            <label>Business Name</label>
                                                            <input type="text" class="form-control" name="bname" placeholder="Business Name" value="<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('business_name') : '' ?>"/>
                                                        </div>
                                                    </div><!--bname row-->
                                                                
                                                    <div class="row gutter-md">
                                                        <div class="col-sm-12 form-group">
                                                            <label>Business Address</label>
                                                            <input type="text" class="form-control input_location" name="address" placeholder="Business Address" value="<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('address') : '' ?>"/>
                                                            <input type="hidden" name="lat" value="<?php echo $this->session->userdata('lat') ?>"/>
                                                            <input type="hidden" name="lng" value="<?php echo $this->session->userdata('lng') ?>"/>
                                                            
                                                        </div>
                                                    </div><!--bname row-->

                                                    <div class="row gutter-md">
                                                        <div class="col-sm-12 form-group">
                                                            <label>Email</label>
                                                            <input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('email') : '' ?>"/>
                                                        </div>
                                                    </div><!--bname row-->

                                                    <div class="row gutter-md">
                                                    
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Country Calling Code</label>
                                                                <select class="form-control country_code_dd" name="country_code" data-live-search="true">
                                                                    <option value="" data-code="">Select..</option>
                                                                    <?php
                                                                        foreach($countries as $r=>$mc){
                                                                            echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';
																			if($this->session->userdata('country_id') == $mc['country_id'] && ($this->session->userdata('customer_type') == 'Y')){
																				echo ' selected="selected"';
																			}
                                                                            
                                                                            
                                                                            echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    
                                                        <div class="col-sm-6">
                                                            <div class="form-group mobile_input">
                                                                <label class="control-label">Mobile Number</label>
                                                                <div class="input-group">
                                                                  <span class="input-group-addon addon-shortcode">+<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('calling_code') : '' ?></span>
                                                                  <input type="text" class="form-control" placeholder="Mobile Number" name="mobile" value="<?php echo ($this->session->userdata('customer_type') == 'Y') ? $this->session->userdata('calling_digits') : '' ?>">
                                                                </div>
                                                                <input type="hidden" name="country_short" value=""/>
                                                            </div>
                                                        </div>
                                                    
                                                    </div><!--phone number-->
                                                                                        
                                                
                                                </div>
                                            
                                            </div>
                                           

                                            <div class="col-sm-12 form-group hidden">

                                                <a href="#" class="btn btn-default pull-left buy_back_btn">Back</a>
                                                <button class="btn btn-warning pull-right" type="submit">Continue</button>

                                            </div>
                                                
                                        </div>
                                     
                                        </form><!--hide form if logged in-->
                                        
                                        <hr style="margin-top: 0px;" />
                                    	<h4>Consignee's Details</h4>
                                        <form id="consignee_form" class="single_purchase_form">
                                         <input type="hidden" name="type" value="consignee" />
                                        
                                         <div class="row">
                                           
                                            <div class="col-sm-12 form-group">
                                                <div class="wellx">
                                                    <h3 class="hidden">Single Purchase</h3>
                                                    <div class="row gutter-md">
                                                        <div class="col-md-6 form-group">
                                                            <label>First Name</label>
                                                            <input type="text" class="form-control" name="first" placeholder="First Name" value=""/>
                                                        </div>
                                                        <div class="col-md-6 form-group">
                                                            <label>Last Name</label>
                                                            <input type="text" class="form-control" name="last" placeholder="Last Name" value=""/>
                                                        </div>
                                                        
                                                    </div><!--name row-->
                                                                
                                                    <div class="row gutter-md">
                                                        <div class="col-sm-12 form-group">
                                                            <label>Business Name</label>
                                                            <input type="text" class="form-control" name="bname" placeholder="Business Name" value=""/>
                                                        </div>
                                                    </div><!--bname row-->
                                                                
                                                    <div class="row gutter-md">
                                                        <div class="col-sm-12 form-group">
                                                            <label>Business Address</label>
                                                            <input type="text" class="form-control input_location" name="address" placeholder="Business Address" value=""/>
                                                            <input type="hidden" name="lat" value=""/>
                                                            <input type="hidden" name="lng" value=""/>
                                                            
                                                        </div>
                                                    </div><!--bname row-->

                                                    <div class="row gutter-md">
                                                        <div class="col-sm-12 form-group">
                                                            <label>Email</label>
                                                            <input type="text" class="form-control" name="email" placeholder="Email" value=""/>
                                                        </div>
                                                    </div><!--bname row-->

                                                    <div class="row gutter-md">
                                                    
                                                        <div class="col-sm-6">
                                                            <div class="form-group">
                                                                <label class="control-label">Country Calling Code</label>
                                                                <select class="form-control country_code_dd" name="country_code" data-live-search="true">
                                                                    <option value="" data-code="">Select..</option>
                                                                    <?php
                                                                        foreach($countries as $r=>$mc){
                                                                            echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'">'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                                        }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    
                                                        <div class="col-sm-6">
                                                            <div class="form-group mobile_input">
                                                                <label class="control-label">Mobile Number</label>
                                                                <div class="input-group">
                                                                  <span class="input-group-addon addon-shortcode">+</span>
                                                                  <input type="text" class="form-control" placeholder="Mobile Number" name="mobile" value="">
                                                                </div>
                                                                <input type="hidden" name="country_short" value=""/>
                                                            </div>
                                                        </div>
                                                    
                                                    </div><!--phone number-->
                                                                                        
                                                </div>
                                                
        
                                            
                                            </div>

                                            <?php /*?><div class="col-sm-12 form-group hidden">
    
                                                <a href="#" class="btn btn-default pull-left buy_back_btn">Back</a>
                                                <button class="btn btn-warning pull-right">Continue</button>
    
                                            </div><?php */?>
                                                                                            
                                                
                                        </div>
                                        </form><!--hide form if logged in-->
                                        </div>                                        
                  

                                        <div class="form-group">
                                            <a href="#" class="btn btn-default pull-left buy_back_btn">Back</a>
                                            <button class="btn btn-warning pull-right policy_btn">Continue</button>
                                        </div>
                                        
                                    <?php /*?></div><!--tab-details-->
                                    <div role="tabpanel" class="tab-pane" id="tab-consignee"><?php */?>
                                    
                                    
                                        
                                        
                                        <?php /*?><div class="alert alert-info new_consignee <?php if(count($mycustomers) > 0) { echo 'hidden'; } ?>">
                                        	<p>Same address as the Consignor?</p><hr />
                                        	<a href="#" class="btn btn-info a_consignee" data-type="yes">Yes</a> 
                                        	<a href="#" class="btn btn-default a_consignee" data-type="no">No</a>
                                        </div><?php */?>
                                    	
                                    </div><!--tab-consignee-->
                                                                        
                                   
                                    <div role="tabpanel" class="tab-pane" id="tab-confirm">
                                    	<h4 class="text-left">Confirm</h4>
                                        
                                        <div class="confirm-holder">
                                        
                                        </div>
                                        
                                        
                                        <div class="form-group">
                                            <a href="#" class="btn btn-default buy_back_btn pull-left">Back</a>
                                            <button class="btn btn-warning quote_continue_btn pull-right" data-id="<?php echo $this->session->userdata('id')?>">Continue</button>
                                        </div>
                                        
                                    </div><!--tab-confirm-->
                                                                        
                                   
                                    <div role="tabpanel" class="tab-pane" id="tab-payment">
                                    	<h4 class="text-left"><?php echo ($this->session->userdata('customer_type') == 'N') ? 'Send' : 'Payment' ?></h4>
                                        
                                        
                                        <form role="form" id="payment-form" class="<?php if($this->session->userdata('stripe_id') != '' || $this->session->userdata('customer_type') == 'N') { echo 'hidden'; }?>">
                                        <div class="well">

                            
                                <div class="row">
                                    <!-- You can make it whatever width you want. I'm making it full width
                                         on <= small devices and 4/12 page width on >= medium devices -->
                                    <div class="col-md-12">

                                        <!-- CREDIT CARD FORM STARTS HERE -->
                                        <div class="panelx credit-card-box">

                                            <div class="panel-bodyx">
                                                <p class="text-muted text-left">
                                                    <img class="img-responsive pull-right" src="<?php echo base_url().'assets/img/'?>accepted_c22e0.png">
                                                A payment method is required in order to buy Single Marine Transit Policy.
                                                </p>
                                                                          

                                                    <div class="row hidden">
                                                        <div class="col-xs-12">
                                                            <div class="form-group">
                                                                <label for="couponCode">Card Holder's Name</label>
                                                                <input type="text" class="form-control" name="name" placeholder="Card Holder's Name" autofocus/>
                                                            </div>
                                                        </div>                        
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="form-group">
                                                                <label for="cardNumber">Card number</label>
                                                                <div class="input-group">
                                                                    <input 
                                                                        type="tel"
                                                                        class="form-control"
                                                                        name="cardNumber"
                                                                        placeholder="Valid Card Number"
                                                                        autocomplete="cc-number"
                                                                        required autofocus
                                                                    />
                                                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                                </div>
                                                            </div>                            
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-7">
                                                            <div class="form-group">
                                                                <label for="cardExpiry">Expiration Date</label>
                                                                <input 
                                                                    type="tel" 
                                                                    class="form-control" 
                                                                    name="cardExpiry"
                                                                    placeholder="MM / YY"
                                                                    autocomplete="cc-exp"
                                                                    required 
                                                                />
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5 pull-right">
                                                            <div class="form-group">
                                                                <label for="cardCVC">CV Code</label>
                                                                <input 
                                                                    type="tel" 
                                                                    class="form-control"
                                                                    name="cardCVC"
                                                                    placeholder="CVC"
                                                                    autocomplete="cc-csc"
                                                                    required
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>            
                                        <!-- CREDIT CARD FORM ENDS HERE -->
                                       
                                       	<div class="payment_completed hidden">
                                            <div style="margin-top: 50px;" class="alert alert-success text-center">Payment method added. You are completely ready to buy transit insurance. </div>
    
                                        </div>            

                                    </div>            
                                </div><!--payment form-->  
            
                                        </div><!--well-->
                                        
                                        <div class="row">
                                            <div class="col-xs-12">
                                            	<input type="hidden" name="from_buy" value="yes" />
                                                <a href="#" class="btn btn-default buy_back_btn pull-left">Back</a>
                                                <button class="btn btn-warning pull-right" type="submit">Submit</button>
                                            </div>
                                        </div>
                                        <div class="row" style="display:none;">
                                            <div class="col-xs-12">
                                                <p class="payment-errors text-danger"></p>
                                            </div>
                                        </div>
                                        
                                        </form>
                                        
                                        
                                        <div class="payment_form_buy <?php if($this->session->userdata('stripe_id') == '' || $this->session->userdata('customer_type') == 'N') { echo 'hidden';}?>">
                                            
                                            <div class="row">
                                                <div class="col-xs-12">
                                                	<div class="alert alert-success">
                                                    	<i class="fa fa-info-circle text-success"></i> You have already added your payment method. You are ready to Buy Single Marine Transit Policy. 
                                                    </div>
                                                
                                                    <a href="#" class="btn btn-default buy_back_btn pull-left">Back</a>
                                                    <button class="btn btn-warning payment_logged_btn pull-right" type="submit">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    
                                    	<?php if($this->session->userdata('customer_type') == 'N') { ?>
                                        
                                        	<div class="alert alert-info">
                                            	<a href="<?php echo base_url().'landing/quote_preview' ?>" target="_blank">Preview</a> quote in PDF.                                            
                                            </div>
                                                                                    
                                            <object data="<?php echo base_url().'landing/quote_preview' ?>" type="application/pdf" style="width:100%; height: 350px;">
                                                <embed src="<?php echo base_url().'landing/quote_preview' ?>" type="application/pdf" />
                                            </object>                                            
                                            
                                            
                                            <div class="form-group">
                                                <a href="<?php echo base_url().'landing/quote_preview' ?>" class="btn btn-default buy_back_btn pull-left">Back</a>
                                                <button class="btn btn-warning pull-right agent_save_btn" data-id="<?php echo $this->session->userdata('id')?>">Email Quote to Consignor</button>
                                            </div>
                                            
                                        <?php } ?>
                                        
                                        
                                    </div><!--tab-payment-->
                                </div><!--tabs-->
              
                        </div><!--modal-body-->
                    </div>
                </div>
            </div>
            
        </div>
    </div><!--portfolio-modal-->
                

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="portModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">
                        
                            <h1>Port Codes</h1>
                            <hr class="star-primary">
                            
                            
                            	<div class="row gutter-md">
                                	<div class="col-sm-6">
                                    	<div class="well">
                                            <label>Search Port Codes</label>
                                            <input type="search" class="form-control input-search" id="input-search" placeholder="Search port codes.." >
                                        </div>
                                    </div>
                                	<div class="col-sm-6">
                                    	<div class="well">
                                            <label>Not in the list?</label><br />
                                            <div class="input-group">
                                              <input type="text" class="form-control" placeholder="Enter port code or name">
                                              <span class="input-group-btn">
                                                <button class="btn btn-warning custom_port_btn" type="button">Submit</button>
                                              </span>
                                            </div><!-- /input-group -->

                                        </div>
                                    </div>
                                </div>
                            <form class="radio_list_form">
                            	<input type="hidden" name="type" value="" />
                            	<input type="hidden" name="field" value="" />
                                <div class="form-group">
                                    <div class="the_genres">
                                    	<div class="row genre_list">
                                        
                                            <div class="col-xs-6 col-md-4">
                                                <a href="#" class="thumbnail text-center genre_thumbs">
                                                <span></span>
                                                <div class="checkbox hidden">                                    
                                                  <label>
                                                    <input type="radio" name="cargocat" /><span></span>
                                                  </label>
                                                </div> <!--checkbox-->
                                                </a>                                   
                                            </div><!--col-xs-6-->

                                        </div>
                                    </div>
                                 
                                </div>
                                            
                            </form>    
                            

                        </div><!--modal-body-->
                    </div>
                </div>
            </div>
            
        </div>
    </div><!--portfolio-modal-->                            
                            
    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="cargoCatModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">
                        
                            <h1>Cargo</h1>
                            <hr class="star-primary">
                            
                            <div class="well">
                                <input type="search" class="form-control input-search-table" placeholder="Enter HS Code or cargo description.." >
                            </div>
                             
                             
                            <table class="table table-hover mydataTbx clickable-cargo-table">
                                <thead>
                                    <tr>
                                        <th width="10%">HS Code</th>
                                        <th width="90%">Description</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php
								foreach($cargos as $r=>$value){
                                    $acargo_price = $value['multiplier'];
                                    $acargo_ref = $value['referral'];
                                    if(count($the_agency) > 0) {
                                        if(isset($cargo_prices[$value['id']])) {
                                            $acargo_price = str_replace(',','', $cargo_prices[$value['id']]);
                                        }
									}
								?>
                                    <tr <?php echo 'data-value="'.$value['id'].'" data-price="'.$acargo_price.'" data-max="'.$acargo_ref.'" data-bindoption="'.$value['uninsurable'].'"'; if($cargocat == $value['id']){ echo ' class="active"';  } ?> data-text="<?php echo $value['hs_code'].' '.$value['description']?>" title="click to select">
                                        <td><?php echo $value['hs_code'] ?></td>
                                        <td><?php echo $value['description'] ?></td>
                                    </tr>
                                
                                <?php } ?>
                                    
                                    
                                </tbody>
                            </table>
                            <?php /*?><form id="filter_genre_form">
                            	<input type="hidden" name="id" value="<?php echo $this->session->userdata('id')?>" />
                                <div class="form-group">
                                    <div class="the_genres">
                                    	<div class="row genre_list">
                                        <?php foreach($cargos as $r=>$value){
											$acargo_price = $value['multiplier'];
											$acargo_ref = $value['referral'];
											if(count($the_agency) > 0) {
												if(isset($cargo_prices[$value['id']])) {
													$acargo_price = str_replace(',','', $cargo_prices[$value['id']]);
												}
//												$acargo_max = 0;
//												if(isset($cargo_max[$value['id']])) {
//													$acargo_max = str_replace(',','',$cargo_max[$value['id']]);
//												}
											}
											
										?>
                                        <div class="col-xs-6 col-md-4">
                                        <a href="#" class="thumbnail text-center genre_thumbs <?php if($cargocat == $value['id']){ echo 'active'; } ?>" title="<?php echo $value['description']?>">
                                        <span><?php echo (strlen($value['description']) > 43) ? $value['hs_code'].' &middot; '.substr($value['description'],0 ,40).'...' : $value['hs_code'].' &middot; '.$value['description']; ?></span>
                                        <span class="hidden"><?php echo $value['description']; ?></span>
                                        <div class="checkbox hidden">                                        
                                          <label>
                                            <input type="radio" name="cargocat" <?php echo 'value="'.$value['id'].'" data-price="'.$acargo_price.'" data-max="'.$acargo_ref.'" data-bindoption="'.$value['uninsurable'].'" ';
												if($cargocat == $value['id']){
													echo 'checked="checked"'; 
												} ?>
												
												 /><span><?php echo (strlen($value['description']) > 43) ? $value['hs_code'].' &middot; '.substr($value['description'],0 ,40).'...' : $value['hs_code'].' &middot; '.$value['description']; ?></span>
                                            
                                          </label>
                                        </div> <!--checkbox-->
                                        </a>                                   
                                        </div><!--col-xs-6-->
                                        <?php } ?>
                                        
                                        </div>
                                    </div>
                                 
                                </div>
                                
                                <div class="form-group hidden">
                                	<button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="#" class="btn btn-default" onclick="$(this).closest('form').find('input').prop('checked',false);$(this).closest('form').find('input').closest('a').removeClass('active');">Reset</a>
                                </div>
                            </form><?php */?>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
          
          
          

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="referCountryModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="modal-body">
                        
                            <h1>Referral Required</h1>
                            <hr class="star-primary">
                                 

                            <form class="single_purchase_form">
                             <input type="hidden" name="type" value="consignor" />
                             <div class="row">
                               
                                <div class="col-sm-12 form-group">
                                    <div class="well">
                                        <h3 class="hidden">Single Purchase</h3>
                                        <div class="row gutter-md">
                                            <div class="col-md-6 form-group">
                                                <label>First Name</label>
                                                <input type="text" class="form-control" name="first" placeholder="First Name" value="<?php echo $this->session->userdata('first_name') ?>"/>
                                                <input type="hidden" name="refer_country" value="yes" />
                                            </div>
                                            <div class="col-md-6 form-group">
                                                <label>Last Name</label>
                                                <input type="text" class="form-control" name="last" placeholder="Last Name" value="<?php echo $this->session->userdata('last_name') ?>"/>
                                            </div>
                                            
                                        </div><!--name row-->
                                                    
                                        <div class="row gutter-md">
                                            <div class="col-sm-12 form-group">
                                                <label>Business Name</label>
                                                <input type="text" class="form-control" name="bname" placeholder="Business Name" value="<?php echo $this->session->userdata('business_name') ?>"/>
                                            </div>
                                        </div><!--bname row-->
                                                    
                                        <div class="row gutter-md">
                                            <div class="col-sm-12 form-group">
                                                <label>Business Address</label>
                                                <input type="text" class="form-control input_location" name="address" placeholder="Business Address" value="<?php echo $this->session->userdata('address') ?>"/>
                                                <input type="hidden" name="lat" value="<?php echo $this->session->userdata('lat') ?>"/>
                                                <input type="hidden" name="lng" value="<?php echo $this->session->userdata('lng') ?>"/>
                                                
                                            </div>
                                        </div><!--bname row-->

                                        <div class="row gutter-md">
                                            <div class="col-sm-12 form-group">
                                                <label>Email</label>
                                                <input type="text" class="form-control" name="email" placeholder="Email" value="<?php echo $this->session->userdata('email') ?>"/>
                                            </div>
                                        </div><!--bname row-->

                                        <div class="row gutter-md">
                                        
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Country Calling Code</label>
                                                    <select class="form-control country_code_dd" name="country_code" data-live-search="true">
                                                        <option value="" data-code="">Select..</option>
                                                        <?php
                                                            foreach($countries as $r=>$mc){
                                                                echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';
                                                                if($this->session->userdata('country_id') == $mc['country_id']){
                                                                    echo ' selected="selected"';
                                                                }
                                                                
                                                                
                                                                echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        
                                            <div class="col-sm-6">
                                                <div class="form-group mobile_input">
                                                    <label class="control-label">Mobile Number</label>
                                                    <div class="input-group">
                                                      <span class="input-group-addon addon-shortcode">+<?php echo $this->session->userdata('calling_code') ?></span>
                                                      <input type="text" class="form-control" placeholder="Mobile Number" name="mobile" value="<?php echo $this->session->userdata('calling_digits') ?>">
                                                    </div>
                                                    <input type="hidden" name="country_short" value=""/>
                                                </div>
                                            </div>
                                        
                                        </div><!--phone number-->
                                                                            
                                    
                                    </div>
                                
                                </div>
                               

                                <div class="col-sm-12 form-group">
                                    <button class="btn btn-warning pull-right" type="submit">Continue</button>
                                </div>
                                    
                            </div>
                            </form><!--hide form if logged in-->
                                                             
                                      

                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
    
        
        
    <!-- Modal -->
    <div class="modal fade" id="checkSingleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Would you like to register to save your details and purchase history?</h4>
          </div>
          <div class="modal-body">
          	<div class="row gutter-md">
            
            	<div class="col-sm-6" style="margin-bottom: 15px;">
                	<a href="#" class="btn btn-primary btn-lg btn-block buy_thanks_btn" style="padding: 50px 0 50px 0">
                    	<span >No thanks, continue with purchase</span>
                    </a>
                </div>
            
            	<div class="col-sm-6" style="margin-bottom: 15px;">
                	<a href="#" class="btn btn-info btn-lg btn-block buy_regnow_btn" style="padding: 50px 0 50px 0">
                    	<span>Yes please! Register me now</span>
                    	
                    </a>
                </div>
            </div>

          </div>
        </div>
      </div>
    </div>
          