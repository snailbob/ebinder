



    <div class="banner-content" id="top-home" style=" height: 100vh; min-heigth: 100%; width: 100%; z-index: -2">

        <div id="banner" class="banner banner-blue" style="text-align: left; vertical-align: top; height: 563px;">

              <div class="container-fluid">
              	<div class="rowx">
                	<div class="col-lg-12">
                          <h1 class="text-center" style="color: #ea9606">
                            <?php echo $mainhometitle[0]['title'] ?>
                          </h1>

                          <h5 class="text-center">
                                <a class="typewrite text-white" data-period="2000" data-type='["<?php echo $banners[1]['title'] ?>", "<?php echo $banners[2]['title'] ?>", "<?php echo $banners[0]['title'] ?>"]'>
    <span class="wrap"><?php echo $banners[0]['title'] ?></span>
  </a><span id="cursor">|</span>
                          </h5>

                    </div>
                </div>


            </div>
        </div>

        <div id="banner" class="banner banner2">
          <div class="container-fluid" style="text-align: center;vertical-align:top">

          		<a href="javascript:;" class="smoothScroll" data-scrollto="#home-what">
                <h3 class="text-white">Learn more</h3>
                <span class="fa fa-arrow-down" style="color:#fff;font-size:20px;"></span>
              </a>

          </div>
        </div>

    </div>

  <div class="top-video-content">

    <div id="home-what" class="section homewhat_section" >
        <div class="container" id="section-2">
            <h1><?php echo $firsthomesection[0]['title']?></h1>

            <h2 style="font-size: 21px;"><?php echo $firsthomesection[0]['content']?></h2>


        </div> <!-- /container -->

    </div> <!-- /homepage-what -->





    <div id="our-solutions" class="section" style="border-bottom: 1px solid #eee;" data-center="background-color:rgb(240, 247, 252);" data-center-top="background-color:rgb(255, 255, 255);">
        <div class="container">

            <div class="row centered our-boxes-holder" style="margin-top: 0px;">
                <h1><?php echo $alternatingcontenthead[0]['title']?></h1>

                <h2 style="font-size: 21px;"><?php echo $alternatingcontenthead[0]['content']?></h2>

    			<?php
    				$alternatingcontentscount = 1;
    				foreach($alternatingcontents as $r=>$value){
    					$mod = $alternatingcontentscount%2;
    					//left text
    					if($mod == 0){ ?>


            	<div class="col-sm-6 text-left hidden-xs">
                    <h3 style="margin-top: 25px;font-size: 160%;" class="text-info"><?php echo $value['title']?></h3>
                    <p style="font-size: 120%;"><?php echo $value['content']?></p>
                </div>

            	<div class="col-sm-6">
                    <img src="<?php echo base_url().'uploads/avatars/'.$value['image'] ?>" class="img-responsive thumbnail" style="margin: 0 auto;"/>
                    <h3 style="font-size: 125%;" class="text-info hidden-lg hidden-md hidden-sm"><?php echo $value['title']?></h3>
                    <p style="font-size: 120%;" class="hidden-lg hidden-md hidden-sm"><?php echo $value['content']?></p>
                </div>



    					<?php
    					}
    					//right text
    					else{ ?>




            	<div class="col-sm-6">
                    <img src="<?php echo base_url().'uploads/avatars/'.$value['image'] ?>" class="img-responsive thumbnail" style="margin: 0 auto;"/>
                    <h3 style="font-size: 125%;" class="text-info hidden-lg hidden-md hidden-sm"><?php echo $value['title']?></h3>
                    <p style="font-size: 120%;" class="hidden-lg hidden-md hidden-sm"><?php echo $value['content']?></p>
                </div>

            	<div class="col-sm-6 text-left hidden-xs">
                    <h3 style="margin-top: 25px;font-size: 160%;" class="text-info"><?php echo $value['title']?></h3>
                    <p style="font-size: 120%;"><?php echo $value['content']?></p>
                </div>

                        <?php

    					}

    					if($alternatingcontentscount != count($alternatingcontents)){
    				?>


            	<!--separator-->
    			<div style="clear: both; border-bottom: 1px solid #eee; padding-top: 30px; margin-top: 30px; margin-bottom: 30px;" class="hidden-xs"></div>
    			<div style="clear: both; border-bottom: 1px solid #eee; margin-top: 15px; margin-bottom: 15px;" class="hidden-lg hidden-md hidden-sm"></div>
            	<!--separator-->


                <?php
    					}

    					$alternatingcontentscount++;
    				}
    			?>



            </div><!-- /our-boxes-holder-->



        </div> <!-- /container -->
    </div> <!-- /our-solutions -->




    <div id="more-reasons" class="section" data-center="background-color:rgb(239, 239, 239);" data-center-top="background-color:rgb(255, 255, 255);">

        <div class="container">
           <?php /*?> <h1>More Reasons Why</h1>

            <h2>Because your reputation is everything.</h2><?php */?>

            <h1><?php echo $threepartshead[0]['title']?></h1>

            <h2><?php echo $threepartshead[0]['content']?></h2>

            <div class="row centered">
            	<?php foreach($threeparts as $r=>$value){ ?>

                <div class="col-md-4 col-sm-12 item">
                    <img src="<?php echo base_url().'uploads/avatars/'.$value['image']?>">
                    <h4><?php echo $value['title'] ?></h4>
                    <p><?php echo $value['content'] ?></p>
                </div>

    			<?php } ?>


            </div> <!-- /row -->

        </div> <!-- /container -->
    </div> <!-- /more-reasons -->


    <?php if($threepartshead2[0]['title'] != 'placeholder title') { ?>
    <div id="system-cloud" class="section" data-center="background-color:rgb(255, 255, 255);" data-center-top="background-color:rgb(255, 255, 255);">

        <div class="container">
           <?php /*?> <h1>More Reasons Why</h1>

            <h2>Because your reputation is everything.</h2><?php */?>

            <h1><?php echo $threepartshead2[0]['title']?></h1>

            <h2><?php echo $threepartshead2[0]['content']?></h2>

            <div class="row centered">
            	<?php foreach($threeparts2 as $r=>$value){ ?>

                <div class="col-md-4 col-sm-12 item">
                    <img src="<?php echo base_url().'uploads/avatars/'.$value['image']?>" width="86px">
                    <h4><?php echo $value['title'] ?></h4>
                    <p><?php echo $value['content'] ?></p>
                </div>

    			<?php } ?>


            </div> <!-- /row -->

        </div> <!-- /container -->
    </div> <!-- /system-cloud -->
    <?php } ?>

  </div>
  <!-- -top-video-content -->


    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="signupModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">


                            <div class="row row-signup" style="margin-top: 100px;">

                                <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">

                                    <h3 style="font-size: 22px;">Signup</h3>
                                    <hr class="star-primary">
                                    <p class="text-center">Already have an account? <a href="#" class="login_btn">Log In</a><br /><br /></p>
                                    <div class="alert alert-success" style="display: none;">
                                        <button type="button" class="close" onclick="$(this).closest('.alert').hide();" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                        <span class="my-text"></span>
                                    </div>

                                    <div class="row social-btns">
                                        <div class="col-sm-6">
                                            <div class="form-groupx">
                                                <button id="customBtn" type="button" class="btn btn-default btn-block"><i class="fa fa-google-plus text-danger"></i> Google+</button>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-groupx">
                                                <button type="button" class="btn btn-primary btn-block" id="MyLinkedInButton"><i class="fa fa-linkedin"></i> LinkedIn</button>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="star-primary">


                                </div>

                                <div class="col-sm-12">
                                    <div class="row text-left">
                                        <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-8 col-md-offset-2">

                                            <form class="omb_loginForm" id="signup_form">
                                                <input type="hidden" class="form-control" name="google_id">
                                                <input type="hidden" class="form-control" name="facebook_id">
                                                <input type="hidden" class="form-control refer_from" name="refer_from" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; } else { echo $this->session->userdata('id'); }?>">

                                                <input type="hidden" class="form-control" name="google_img">
                                                <input type="hidden" class="form-control" name="fb_img">
                                                <input type="hidden" class="form-control" id="social_type" name="social_type">
                                                <input type="hidden" class="form-control" id="social_site" name="social_site">


                                                <div class="form-group hidden">
                                                    <div class="dropdown dropdown-form dropdown-signup">
                                                      <button class="btn btn-primary btn-block btn-outline"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        <i class="fa fa-chevron-down fa-fw pull-right"></i>
                                                        Sign up as <span class="u_type"> - Agency</span>
                                                      </button>
                                                      <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                        <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                                        <li><a href="#" data-type="Customer">Customer</a></li>
                                                      </ul>
                                                    </div>

                                                    <input type="hidden" name="utype" value="Customer" />
                                                </div>

                                                <div class="form-group studio_only hidden">


                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                        <input type="text" class="form-control" name="name" placeholder="Agency Name">
                                                    </div>
                                                </div>


                                                <div class="row notfor_studio">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                                <input type="text" class="form-control" name="first_name" placeholder="First Name" value="<?php echo (isset($social['first_name'])) ? $social['first_name'] : ''; ?>">
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                                                <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="<?php echo (isset($social['last_name'])) ? $social['last_name'] : ''; ?>">
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-building-o fa-fw"></i></span>
                                                                <input type="text" class="form-control the_company_field" name="company" placeholder="Company Name">
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></span>
                                                                <input type="email" class="form-control" name="email" placeholder="Email Address" value="<?php echo (isset($social['email'])) ? $social['email'] : ''; ?>">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i class="fa fa-phone
                                fa-fw"></i></span>
                                                                <input type="text" class="form-control" name="phone" placeholder="Phone">
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <small>By clicking on 'Get Started', I agree to the <a href="#" class="terms_btn" data-field="terms">Terms of use</a> and <a href="#" class="privacy_btn" data-field="privacy">Privacy Policy</a>. </small>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <button class="btn btn-warning btn-block" type="submit">Get Started</button>
                                                        </div>
                                                    </div>

                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>






                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->



    <!-- loginModal -->
    <div class="portfolio-modal modal fade" id="loginModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">

                            <div class="zendesk-loginx">


                                <div class="row" style="margin-top: 100px;">
                                    <div class="col-md-4 col-md-offset-4">
                                        <p class="text-center">
                                            <img src="<?php echo $this->common->logo(0)?>" />
                                        </p>
                                        <h3 style="font-size: 22px;">Login to your account</h3>
                                        <hr class="star-primary">
                                        <p class="text-center">Don't have an account? <a href="#" class="signup_btn">Request access</a><br /><br /></p>
                                            <div class="alert alert-info <?php echo ($this->session->flashdata('info') != '') ? '' : 'hidden'; ?>" style="margin-bottom: 15px;">
                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button>
                                              <?php echo $this->session->flashdata('info') ?>
                                            </div>


                                            <div class="row ">
                                                <div class="col-xs-12">

                                                    <hr class="star-primary" style="margin-top: 0">

                                                    <form class="omb_loginForm" id="login_form">
                                                        <input type="hidden" class="form-control" name="google_id">
                                                        <input type="hidden" class="form-control" name="facebook_id">
                                                        <input type="hidden" class="form-control refer_from" name="refer_from" value="<?php //if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; } else { echo $this->session->userdata('id'); }?>">
                                                        <input type="hidden" class="form-control" name="from_buyform" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; }?>">

                                                        <div class="form-group hidden">
                                                            <div class="dropdown dropdown-form dropdown-login">
                                                              <button class="btn btn-primary btn-block btn-outline"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-chevron-down fa-fw pull-right" style="margin-right: 10px;"></i>
                                                                Log In as <span class="u_type"> - Agency</span>
                                                              </button>
                                                              <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                                <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                                                <li><a href="#" data-type="Customer">Customer</a></li>
                                                              </ul>
                                                            </div>

                                                            <input type="hidden" name="utype" value="Customer" />
                                                        </div>

                                                        <div class="form-group input_icon">
                                                              <span class="mobile-icon">
                                                                <i class="fa fa-envelope fa-fw"></i>
                                                              </span>
                                                            <input type="email" class="form-control" name="email" placeholder="Email Address">

                                                        </div>

                                                        <div class="form-group input_icon">
                                                              <span class="mobile-icon">
                                                                <i class="fa fa-lock fa-fw fa-2x"></i>
                                                              </span>
                                                            <input  type="password" class="form-control" name="password" placeholder="Password">

                                                        </div>

                                                        <div class="form-group hidden">
                                                            <label>Log In as</label>
                                                            <input type="checkbox" name="usertype" checked>
                                                        </div>

                                                        <div class="form-group">
                                                            <button class="btn btn-warning btn-block" type="submit">Log In</button>
                                                        </div>


                                                    </form>

                                                    <p class="omb_forgotPwd">
                                                        <a href="#" class="forgot_btn">Forgot password?</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>




                                    </div>


                                </div>
                            </div>



                        </div><!--modal-body-->


                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->
