


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="twitter:widgets:csp" content="on">

    <?php
      $topheadtitle = $this->master->getRecords('admin_contents', array('type'=>'topheadtitle'));

    ?>
    <title><?php echo $topheadtitle[0]['title']?></title>

  <script src="https://apis.google.com/js/api:client.js"></script>

    <?php /*?><meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="253167228155-mnp9rtvui3t45cben3u1vag21n4vcupk.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>

    <script>
      function onSignIn(googleUser) {
        // Useful data for your client-side scripts:
        var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); // Don't send this directly to your server!
        console.log('Full Name: ' + profile.getName());
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

        // The ID token you need to pass to your backend:
        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);

      };
    </script><?php */?>

<!-- Custom CSS -->
<?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/creative.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css"><?php */?>


<link href="<?php echo base_url()?>assets/frontpage/corporate/css/bootstrap.css" rel="stylesheet">

<!-- Plugin CSS -->
<?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css" type="text/css"><?php */?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/dataTables.bootstrap.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/datatables.css" type="text/css">

<!-- Plugin CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/cropper.min.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/crop-avatar.css" type="text/css">


<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bs-switch/css/bootstrap3/bootstrap-switch.min.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/silviomoreto-bootstrap-select/css/bootstrap-select.min.css" type="text/css">




<link href="<?php echo base_url()?>assets/frontpage/corporate/css/main.css" rel="stylesheet">

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,500,300,100' rel='stylesheet' type='text/css'>

<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700' rel='stylesheet' type='text/css'>

<link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">

<link href="<?php echo base_url()?>assets/frontpage/corporate/css/animate.min.css" rel="stylesheet">
<link rel="shortcut icon" href="<?php echo base_url()?>assets/frontpage/images/favicon.ico" type="image/x-icon" />
<link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url()?>assets/frontpage/corporate/css/jquery.mmenu.css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/libs/head.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/us/variablesCorporate"></script>
<script>
	var base_url = '<?php echo $this->common->base_url() ?>';
	var uri_1 = '<?php echo $this->uri->segment(1) ?>';
	var uri_2 = '<?php echo $this->uri->segment(2) ?>';
	var uri_3 = '<?php echo $this->uri->segment(3) ?>';
	var uri_4 = '<?php echo $this->uri->segment(4) ?>';
	var user_id = '<?php echo $this->session->userdata('id') ?>';
	var user_name = '<?php echo $this->session->userdata('name') ?>';
	var user_location = '<?php echo $this->session->userdata('location') ?>';
	var user_address = '<?php echo $this->session->userdata('address') ?>';
	var user_type = '<?php echo $this->session->userdata('type') ?>';
	var stripe_id = '<?php echo $this->session->userdata('stripe_id') ?>';
	var customer_type = '<?php echo $this->session->userdata('customer_type') ?>';
	var domain_name = '<?php echo $this->common->the_domain() ?>';

</script>


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

<![endif]-->

</head>



 <body id="skrollr-body" class="root home landing" style="bacgkround-color: transparent">

	<?php
		//vars
		$colors = $this->common->colors();
		$usertype = $this->session->userdata('type');
		$opposite_type = 'agency';
		if($usertype != 'customer'){
			$opposite_type = 'customer';
		}
	?>



    <div id="browser-detection" class="alert alert-warning" style="display:none; position:relative;">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		Your browser is not supported by Transit Insurance. For a better experience, use one of our <a class='preventDefault' data-toggle='modal' data-target='#modal-browser-options'>supported browsers.</a>
	</div>
	<div id="cookies-detection" class="alert alert-danger" style="display:none; position:relative;">
		*__browser_cookies_disabled*
	</div>
    <nav id="my-menu">
       <ul>
            <li class=""><a href="<?php echo base_url().'login' ?>" class="login_btnx"><i class="fa fa-sign-in fa-fw"></i> Log In</a></li>
            <li class=""><a href="<?php echo base_url().'signup' ?>" class="login_btnx"><i class="fa fa-user fa-fw"></i> Sign Up</a></li>

       </ul>
    </nav>

    <div id="main-container">
    <?php /*?><div class="mobile-spacer visible-xs hidden-sm hidden-md hidden-lg" data-0="margin-top:120px" data-1="margin-top:60px"></div><?php */?>

      <?php /*?><!-- Static navbar -->
      <div class="navbar navbar-default navbar-static-top" role="navigation">
       	<div class="container">
            <div class="navbar-header">
              <button type="button" id="open-slider" class="navbar-toggle" data-0="top:0" data-1="top:-3px">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <div class="navbar-brand">
              	<a href="<?php echo base_url(); if($this->session->userdata('id') != '') { echo 'dashboard'; }?>"><img id="nav-logo" class="animated fadeIn animated-top the-logo" data-0="display: inline-block" data-50="display:none" src="<?php echo base_url()?>assets/frontpage/corporate/images/crisisflo-logo-medium.png"/></a>
                <div class="mobile-menu visible-xs">


                    <a href="#" id="btn-mini-login" class="btn btn-primary btn-rounded btn-outline login_btnx hidden" data-type="Customer">Log In</a>



                </div>
              </div>
            </div>
            <div class="navbar-collapse collapse">


                <div class="mobile-menu hidden-xs text-right" style="padding-top: 35px;">

                    <a href="<?php echo base_url()."signin"; ?>" id="btn-mini-login" class="btn btn-primary btn-xs btn-rounded btn-outline login_btnx hidden" data-type="Customer" >Log In</a>



                </div>



                <div class="hidden-xs"><a href="<?php echo base_url().'login'?>" class="pull-right btn btn-primary btn-rounded btn-outline login_btnxx" style="margin-top: -6px;" data-type="Customer">Log In</a></div>




              <ul class="nav navbar-nav pull-right main_lg_nav">


                    <li>
                    	<a href="<?php echo base_url().'about' ?>" class="<?php if($this->uri->segment(1) == 'about') { echo 'on-page'; }?>">About Us</a>
                    </li>

                    <li>
                    	<a href="<?php echo base_url().'contact' ?>" class="<?php if($this->uri->segment(1) == 'contact') { echo 'on-page'; }?>">Contact Us</a>
                    </li>

                    <li class="">
                    	<a href="<?php echo base_url().'signup' ?>" class="<?php if($this->uri->segment(1) == 'signup') { echo 'on-page'; }?>">Sign Up</a>
                    </li>





              </ul>



            </div><!--/.nav-collapse -->
   	 	</div> <!-- /container -->
      </div>
<?php */?>



    <header id="header" class="header navbar navbar-default affix-top navbar-static-top navbar-fixed-top header-blue <?php echo ($this->uri->segment(1) != '' && $this->uri->segment(2) != 'faq' && $this->uri->segment(1) != 'about' || ($this->common->the_domain() != ''))  ? 'hidden' : '' ?>">
      <div class="container">
        <div class="row">
          <div class="navbar-header">
            <?php /*?><button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle collapsed" aria-expanded="false"><span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
              </button><?php */?>
              <a href="<?php echo base_url() ?>" class="navbar-brand">
                <span><img src="<?php echo $this->common->logo(0);?>" /></span>
                </a>
          </div>
          <?php /*?><div id="navbar-collapse-1" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
          </div><?php */?>

          <div class="pull-right">
              <div class="navbar-nav navbar-right navbar-xmenu">
                  <div class="xmenu-background"></div>
              </div>

              <div class="navbar-nav navbar-right">
                  <a id="nav-toggle" href="#"><span></span></a>
              </div>
              <div class="navbar-nav navbar-right navbar-btn hidden-xs">
                <div class="form-group">
                    <a  <?php echo ($this->uri->segment(1) == '') ? 'href="#" class="btn btn-link login-btn btn-lg"' : 'href="'.base_url().'#login" class="btn btn-link btn-lg"' ?>>LOGIN</a>
                    <a href="#" class="btn btn-warning book-btn btn-lg requestdemo-btn">BOOK A DEMO</a>
                </div>
              </div>
          </div>



        </div>
      </div>
    </header>


    <nav id="sidebar-wrapper">
        <ul class="sidebar-nav">

            <li>
                <a href="<?php echo ($this->uri->segment(1) == '') ? '#top-home' : base_url() ?>" onclick="$('#nav-toggle').click();">Home</a>
            </li>
            <li>
                <a href="<?php echo base_url().'about'?>" onclick="$('#nav-toggle').click();">About</a>
            </li>
            <li>
                <a href="<?php echo base_url().'landing/faq'?>" onclick="$('#nav-toggle').click();">FAQ</a>
            </li>
            <li>
                <a href="#" class="requestdemo-btn" onclick="$('#nav-toggle').click();">Request Demo</a>
            </li>
            <li>
                <a <?php echo ($this->uri->segment(1) == '') ? 'href="#" class="login-btn" onclick="$(\'#nav-toggle\').click();"' : 'href="'.base_url().'#login"' ?>>Login</a>
            </li>
        </ul>
    </nav>
