<div id="home-what" class="section" >
    <div class="container">
        <h1>FAQ</h1>
        
		<?php if(count($faq) > 0){
			echo '<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">';
			$faqcount = 1;
			foreach($faq as $r=>$val) {?>
        
        
        
          <div class="panel panel-default">
            <div class="panel-heading text-left" role="tab" id="heading<?php echo $faqcount; ?>">
              <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $faqcount; ?>">
                  <?php echo $val['question']; ?>
                </a>
              </h4>
            </div>
            <div id="collapse<?php echo $faqcount; ?>" class="panel-collapse collapse">
              <div class="panel-body text-left">
                <?php echo $val['answer']; ?>
              </div>
            </div>
          </div>

       
        
        
        
        
		<?php $faqcount++; }//end foreach
			echo '</div>';
		 } else {?>
         
         
            <h2 class="text-muted">No FAQ added.</h2>
        
		<?php } ?>
        
        
        
    </div> <!-- /container -->
</div> <!-- /homepage-what -->
