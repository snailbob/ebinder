



	<?php if($this->uri->segment(1) != '' && $this->session->userdata('id') != '') { ?>
        </div><!--/.row-->
    </div>
    <!-- /.container -->
    <?php }?>

	<?php if( ($this->uri->segment(1) == '' && $this->common->the_domain() == '') || $this->uri->segment(2) == 'faq' || $this->uri->segment(1) == 'about' ) {

		$aboutus = $this->master->getRecords('admin_contents', array('type'=>'aboutus'));

		?>
    <div id="footer">
    	<div class="container">

            <div id="footer-bottom" class="row">
              <div class="col-lg-10 col-lg-offset-1">
              	<div class="row">

                    <div class="col-md-5 text-center">
                        <h3><?php echo $aboutus[0]['title']?></h3>
                        <p><?php echo $aboutus[0]['content']?> <a href="<?php echo base_url().'about'?>">Continue reading...</a></p>
                    </div>

                    <div class="col-md-4 text-center">
                        <h3>Contact Us</h3>
                        <p>
                            <a href="mailto:hello@ebinder.com.au"><strong class="text-warning2">hello@ebinder.com.au</strong></a>
                        </p>
                    </div>

                    <div class="col-md-3 text-center">
                        <h3>Find out more</h3>
                        <ul>

                            <li><a href="<?php echo base_url().'landing/faq'?>">FAQs</a></li>
                            <li><a href="#" class="terms_btn" data-field="terms">Terms of Use</a></li>
                            <li><a href="#" class="privacy_btn" data-field="privacy">Privacy Policy</a></li>
                        </ul>
                    </div>
                </div>
              </div>

            </div>
        </div> <!-- /container -->
    </div> <!-- /footer -->
    <div class="footer-line">
        <div class="container text-center">
            &copy; <?php echo date("Y"); ?> &middot; Risk Monitor Pty Ltd trading as Ebinder

        </div>
    </div>
    <?php }?>
    <?php if ($this->uri->segment(1) == '' && $this->common->the_domain() != '')  { ?>
    <div class="footer-line" style="position: fixed; bottom: 0; left: 0; right: 0; background: #fff;">
        <div class="container text-center">
            &copy; <?php echo date("Y"); ?> &middot; Powered by <a href="https://ebinder.com.au" target="_blank">Ebinder</a>

        </div>
    </div>

    <?php }?>



    <!-- Modal -->
    <div class="modal fade" id="myAgenciesModal" tabindex="-1" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">&nbsp;</h4>
          </div>
          <div class="modal-body">
           	<div class="form-group">
            	<label>Select Agency</label>
                <select class="form-control my_agency_select" name="my_agency_select">
                	<option value="">Select</option>
                </select>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning btn-lg select_agency_btn">Submit</button>
          </div>
        </div>
      </div>
    </div>



    <!-- termsModal -->
    <div class="portfolio-modal modal fade" id="termsModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row" style="margin-top: 100px;">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Terms of Use</h1>
                            <hr class="star-primary">

                            <div class="text_content text-left"></div>

                        </div><!--modal-body-->
                        <div class="modal-footer hidden">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->


    <!-- privacyModal -->
    <div class="portfolio-modal modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row" style="margin-top: 100px;">
										<div class="col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Privacy Policy</h1>
                            <hr class="star-primary">


                            <div class="text_content text-left"></div>



                        </div><!--modal-body-->
                        <div class="modal-footer hidden">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="forgotModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body" style="margin-top: 100px;">
                            <p class="text-center">
                                <img src="<?php echo (isset($active_domain[0]['id'])) ? $this->common->agency_logo($active_domain[0]['id']) : $this->common->logo(0)?>" />

                            </p>


                            <h3 style="font-size: 22px;">Forgot Password</h3>



                            <div class="omb_login">

                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                        <hr class="star-primary">
                                        <p class="text-center">Enter your email below and we will send you instructions on how to reset your password</p>

                                        <form class="omb_loginForm" id="forgot_form">
                                            <div class="form-group">
                                                <input type="email" class="form-control" name="email" placeholder="Email Address">

                                            </div>

                                            <div class="form-group">
                                                <button class="btn btn-warning btn-block" type="submit">Send</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->




    <!-- demoModal -->
    <div class="portfolio-modal modal fade" id="demoModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">

                            <div class="zendesk-loginx"  style="margin-top: 100px;">
                                <div class="row">


                                    <div class="col-md-6 col-md-offset-3">
																				<p class="text-center">
																						<img src="<?php echo $this->common->logo(0)?>" />

																				</p>

                                        <h3 style="font-size: 22px;">Request a personal demo</h3>

                                        <p class="text-muted text-center" style="font-size: 17px; margin-bottom: 45px;">Thanks for your interest in Ebinder. Enter your details below and we'll contact you shortly to schedule a demo.</p>


                                        <form novalidate="novalidate" id="demo_form" class="demo_form">
                                            <input type="hidden" name="type" value="Request personal demo">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                  <div class="form-group">
                                                    <input type="text" class="form-control" name="name" placeholder="Name" required="required">
                                                  </div>

                                                </div>
                                                <div class="col-sm-6">
                                                  <div class="form-group">
                                                    <input type="text" class="form-control" name="agency" placeholder="Company Name" required="required">
                                                  </div>
                                                </div>

																								<div class="col-sm-6">
																									<div class="form-group">
																										<input type="email" class="form-control" name="email" placeholder="Work Email" required="required">
																									</div>
																								</div>
																								<div class="col-sm-6">
																									<div class="form-group">
																										<input type="text" class="form-control" name="phone" placeholder="Phone" required="required">
																									</div>
																								</div>


                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-warning btn-lg" type="submit">REQUEST DEMO</button>
                                                </div>


                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->


    <!-- requestModal -->
    <div class="portfolio-modal modal fade" id="requestModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">

                            <div class="zendesk-loginx"  style="margin-top: 100px;">

                                <div class="row">
                                    <div class="col-md-6 col-md-offset-3">
                                        <p class="text-center">
                                            <img src="<?php echo (isset($active_domain[0]['id'])) ? $this->common->agency_logo($active_domain[0]['id']) : $this->common->logo(0)?>" />

                                        </p>

                                        <h3 style="font-size: 22px;">Request access</h3>
                                        <hr />

                                        <p class="text-muted text-center" style="font-size: 17px; margin-bottom: 15px;">
                                            <?php if($this->common->the_domain() == ''){ ?>
                                                Thanks for your interest in Ebinder. Enter your details below and we'll be in touch to discuss your requirements.
                                            <?php } else { ?>
                                                Please enter your details below to request access.
                                            <?php } ?>

                                        </p>

                                        <p class="text-center">Already have an account? <a href="#" class="login_btn">Log In</a><br /><br /></p>


                                        <form novalidate="novalidate" id="demo_form" class="demo_form">
                                            <input type="hidden" name="type" value="Request access">
                                            <div class="row">
                                                <div class="col-sm-6">

                                                  <div class="form-group">
                                                    <input type="text" class="form-control" name="name" placeholder="Name" required="required">
                                                  </div>
                                                </div>
                                                <div class="col-sm-6">
                                                  <div class="form-group">
                                                    <?php if($this->common->the_domain() == ''){ ?>
                                                        <input type="text" class="form-control" name="agency" placeholder="Company Name" required="required">
                                                    <?php } else { ?>
                                                        <input type="text" class="form-control" name="brokerage" placeholder="Brokerage" required="required">
                                                    <?php } ?>
                                                  </div>
                                                </div>

																								<div class="col-sm-6">
																									<div class="form-group">
																										<input type="email" class="form-control" name="email" placeholder="Work Email" required="required">
																									</div>
																								</div>
																								<div class="col-sm-6">
																									<div class="form-group">
																										<input type="text" class="form-control" name="phone" placeholder="Phone" required="required">
																									</div>
																								</div>

                                                <div class="col-sm-12 text-center">
                                                    <button class="btn btn-warning btn-lg" type="submit">REQUEST ACCESS</button>
                                                </div>


                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->




    <ul class="map-details hidden">
        <li>Location: <span data-geo="location"></span></li>
        <li>Route: <span data-geo="route"></span></li>
        <li>Street Number: <span data-geo="street_number"></span></li>
        <li>Postal Code: <span data-geo="postal_code"></span></li>
        <li>Locality: <span data-geo="locality"></span></li>
        <li>Country Code: <span data-geo="country_short"></span></li>
        <li>State: <span data-geo="administrative_area_level_1"></span></li>
    </ul>

    <!-- Modal -->
    <div class="modal fade" id="myLoadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
			<div class="modal-dialog" role="document">
        <div class="modal-content text-center" style="background-color: transparent; border: none; box-shadow: none; padding: 150px 0;">

    		<i class="fa fa-square-o text-primary fa-spin fa-3x"></i>
        </div>
      </div>
    </div>

    <script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/browserDetection.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.fittext.js"></script>
    <script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/validate/jquery.validate.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/validate/additional-methods.min.js"></script>

    <!-- Plugin JavaScript -->
	<script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places&amp;key=AIzaSyDVL8WaKGrSPhJ7ZY8XHrJeWascHtNA0qc"></script>
    <script src="<?php echo base_url() ?>assets/plugins/geocomplete/jquery.geocomplete.js"></script>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/datatables-bs3.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bootbox/bootbox.js"></script>



    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/pwstrength/pwstrength-bootstrap.js"></script>



    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/jquery.form/jquery.form.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/dependencies/moment.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bs-switch/js/bootstrap-switch.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/silviomoreto-bootstrap-select/js/bootstrap-select.min.js"></script>

	<?php if((($this->uri->segment(2) == 'profile' && $this->session->userdata('stripe_id') == '') || ($this->uri->segment(1) == 'buy' || $this->uri->segment(1) == '')) && $this->session->userdata('customer_type') != 'N') { ?>
			<!-- Plugin JavaScript -->
			<script type="text/javascript" src="<?php echo base_url()?>assets/plugins/validate/jquery.payment.min.js"></script>
			<!-- If you're using Stripe for payments -->
			<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
	<?php } ?>

	<?php if($this->uri->segment(1) == 'settings' && $this->uri->segment(2) == 'profile') { ?>
    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/cropper/js/cropper.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/cropper/js/crop-avatar.js"></script>
	<?php } ?>

    <!-- Custom Theme JavaScript -->
   <?php /*?> <script src="<?php echo base_url() ?>assets/js/creative.js"></script><?php */?>
    <script src="<?php echo base_url() ?>assets/js/script.js"></script>

	<script>

		$(document).ready(function(){
//			document.querySelector( "#nav-toggle" )
//			.addEventListener( "click", function() {
//				this.classList.toggle( "active" );
//			});


			// Closes the sidebar menu
			$("#menu-close").click(function(e) {
				e.preventDefault();
				$("#sidebar-wrapper").toggleClass("active");
			});

			// Opens the sidebar menu
			$("#nav-toggle").click(function(e) {
				var $self = $(this);
				console.log('nav-toggle');
				e.preventDefault();
				$("#sidebar-wrapper").toggleClass("active");
				$self.toggleClass( "active" );
				var $tgl = $('#nav-toggle');

				if($tgl.hasClass('active')){
					$('.xmenu-background').css({background: 'rgba(255, 255, 255, .95)'});
				}
				else{
					$('.xmenu-background').css({background: 'transparent'});
				}


			});

		});
		// Scrolls to the selected menu item on the page
		$(function() {
			$('a[href*=#]:not([href=#])').click(function() {
				var $self = $(this);
				var toggle = $self.data('toggle');
				console.log(toggle);

				if(typeof(toggle) === 'undefined'){
					if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
						var target = $(this.hash);
						target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
						if (target.length) {
							$('html,body').animate({
								scrollTop: target.offset().top
							}, 1000);
							return false;
						}
					}
				}
			});

			//resize cover
			function jqUpdateSize(){
				// Get the dimensions of the viewport
				var width = $(window).width();
				var height = $(window).height();
				var banner_offset = 96;
				var new_height = height - banner_offset;
				//$('#jqWidth').html(width);      // Display the width
				$('.banner-blue').css({height: new_height+'px'});    // Display the height
				var new_height2 = height - 112;
				$('#sidebar-wrapper').css({height: new_height2+'px'});
			};
			$(document).ready(jqUpdateSize);    // When the page first loads
			$(window).resize(jqUpdateSize);     // When the browser changes size


			setInterval ('cursorAnimation()', 600);

		});



		function cursorAnimation() {
			$('#cursor').animate({
				opacity: 0
			}, 'fast', 'swing').animate({
				opacity: 1
			}, 'fast', 'swing');
		}

		//typwriter
		var TxtType = function(el, toRotate, period) {
			this.toRotate = toRotate;
			this.el = el;
			this.loopNum = 0;
			this.period = parseInt(period, 10) || 2000;
			this.txt = '';
			this.tick();
			this.isDeleting = false;
		};

		TxtType.prototype.tick = function() {
			var i = this.loopNum % this.toRotate.length;
			var fullTxt = this.toRotate[i];

			if (this.isDeleting) {
			this.txt = fullTxt.substring(0, this.txt.length - 1);
			} else {
			this.txt = fullTxt.substring(0, this.txt.length + 1);
			}

			this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

			var that = this;
			var delta = 150 - Math.random() * 100;

			if (this.isDeleting) { delta /= 2; }

			if (!this.isDeleting && this.txt === fullTxt) {
			delta = this.period;
			this.isDeleting = true;
			} else if (this.isDeleting && this.txt === '') {
			this.isDeleting = false;
			this.loopNum++;
			delta = 500;
			}

			setTimeout(function() {
			that.tick();
			}, delta);
		};

		window.onload = function() {
			setTimeout(function(){
				var elements = document.getElementsByClassName('typewrite');
				for (var i=0; i<elements.length; i++) {
					var toRotate = elements[i].getAttribute('data-type');
					var period = elements[i].getAttribute('data-period');
					if (toRotate) {
					  new TxtType(elements[i], JSON.parse(toRotate), period);
					}
				}
				// INJECT CSS
			}, 2000);
//			var css = document.createElement("style");
//			css.type = "text/css";
//			css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
//			document.body.appendChild(css);
		};





		if(uri_1 == 'signup' || uri_1 == 'login' || uri_1 == '' && (domain_name == '')){

			$(document).ready(function(e) {
        $('.google-auth-btn').on('click', function(){
					$('#customBtn').click();
				});
        $('.linkedin-auth-btn').on('click', function(){
					$('#MyLinkedInButton').click();
				});


            });

		  var googleUser = {};
		  var startApp = function() {
			gapi.load('auth2', function(){
			  // Retrieve the singleton for the GoogleAuth library and set up the client.
			  auth2 = gapi.auth2.init({
				client_id: '253167228155-mnp9rtvui3t45cben3u1vag21n4vcupk.apps.googleusercontent.com',
				cookiepolicy: 'single_host_origin',
				// Request scopes in addition to 'profile' and 'email'
				//scope: 'additional_scope'
			  });
			  attachSignin(document.getElementById('customBtn'));
			});
		  };

		  function attachSignin(element) {
			console.log(element.id);
			auth2.attachClickHandler(element, {},
				function(googleUser) {
					var profile = googleUser.getBasicProfile();
					console.log("ID: " + profile.getId()); // Don't send this directly to your server!
					console.log('Full Name: ' + profile.getName());
					console.log('Given Name: ' + profile.getGivenName());
					console.log('Family Name: ' + profile.getFamilyName());
					console.log("Image URL: " + profile.getImageUrl());
					console.log("Email: " + profile.getEmail());

					var $mdl = $('#myLoadingModal');
					$mdl.modal('show');

					var data = {
						id: profile.getId(),
						full_name: profile.getName(),
						first_name: profile.getGivenName(),
						last_name: profile.getFamilyName(),
						avatar: profile.getImageUrl(),
						email: profile.getEmail(),

					};
					console.log(data);
					$.post(
						base_url+'formsubmits/googleplus',
						{data: data, uri_1: uri_1, id: "<?php echo (isset($_GET['id'])) ? $_GET['id'] : ''?>"},
						function(res){
							console.log(res);
							//return false;

							if(res.type == 'signup'){
								bootbox.alert('User not found.');
								// var d = new Date();
								// var n = d.getMilliseconds();
								//
								// window.location.href = base_url + '?n=' + n + '#signup';
							}
							else if(res.type == 'complete_signup'){
								$('#customBtn').html('<i class="fa fa-google-plus text-danger"></i> Connnected to Google+').addClass('disabled');
								bootbox.alert(action_messages.success.complete_profile_via_social);
							}
							else{
								if(res.userdata.super_admin == 'Y'){
									window.location.href = base_url+'webmanager';
									console.log(base_url+'webmanager');
								}
								else{
									window.location.href = base_url+'panel'; //'http://'+res.userdata.domain+'.ebinder.com.au/panel';
									console.log(base_url+'panel');
								}


							}
						},
						'json'
					).done(function(res){
						$mdl.modal('hide');
					}).error(function(err){
						console.log(err);
					});



	//			  document.getElementById('name').innerText = "Signed in: " +
	//				  googleUser.getBasicProfile().getName();
				}, function(error) {
				  alert(JSON.stringify(error, undefined, 2));
				});
		  }

		  startApp();
		}

    </script>


    <script type="text/javascript" src="//platform.linkedin.com/in.js">
        api_key: 8198kchnjgi54x
        authorize: true
        onLoad: onLinkedInLoad
    </script>

    <script type="text/javascript">
		if(uri_1 == 'signup' || uri_1 == 'login' || uri_1 == '' && (domain_name == '')){
			// Setup an event listener to make an API call once auth is complete
			function onLinkedInLoad() {
				IN.Event.on(IN, "auth", getProfileData);
			}

			// Use the API call wrapper to request the member's basic profile data
			function getProfileData() {
				if(IN.User.isAuthorized()){
					IN.API.Raw("people/~:(id,first-name,last-name,location,email-address)?format=json").result(onSuccess).error(onError);
				}
			}


			var linkedData = {};
			var linked_fetch = 0;
			// Handle the successful return from the API call
			function onSuccess(profile) {
				var linked = $("#MyLinkedInButton").data('linked');

				//$('#MyLinkedInButton').addClass('disabled');

				var data = {
					id: profile.id,
					full_name: profile.firstName+' '+profile.lastName,
					first_name: profile.firstName,
					last_name: profile.lastName,
					avatar: '',
					email: profile.emailAddress
				};

				console.log('onLinkedInLoad', profile);
				console.log(data);
				linkedData = data;

				if(typeof(linked) !== 'undefined'){
					var $mdl = $('#myLoadingModal');
					$mdl.modal('show');

					$.post(
						base_url+'formsubmits/linkedin',
						{data: data, uri_1: uri_1, id: "<?php echo (isset($_GET['id'])) ? $_GET['id'] : ''?>"},
						function(res){
							console.log(res);

							if(res.type == 'signup'){
								bootbox.alert('User not found.');
								// var d = new Date();
								// var n = d.getMilliseconds();
								//
								// window.location.href = base_url + '?n=' + n + '#signup';
							}
							else if(res.type == 'complete_signup'){
								$('#MyLinkedInButton').html('<i class="fa fa-linkedin"></i> Connnected to LinkedIn').addClass('disabled');
								bootbox.alert(action_messages.success.complete_profile_via_social);
							}

							else{
								if(res.userdata.super_admin == 'Y'){
									window.location.href = base_url+'webmanager';
									console.log(base_url+'webmanager');
								}
								else{
									window.location.href = base_url+'panel'; //'http://'+res.userdata.domain+'.ebinder.com.au/panel';
									console.log(base_url+'panel');
								}

							}
						},
						'json'
					).done(function(res){
						$mdl.modal('hide');
					}).error(function(err){
						console.log(err);
					});

				}
				linked_fetch ++;


			}

			// Handle an error response from the API call
			function onError(error) {
				console.log(error);
			}

			$(document).ready(function(e) {

				$("#MyLinkedInButton").on('click',function () {

					console.log('MyLinkedInButton', linkedData);
					var data = linkedData;
					var $self = $(this);
					$self.data('linked',data);

					if(typeof(linkedData.id) !== 'undefined'){
						var $mdl = $('#myLoadingModal');
						$mdl.modal('show');

						$.post(
							base_url+'formsubmits/linkedin',
							{data: data, uri_1: uri_1, id: "<?php echo (isset($_GET['id'])) ? $_GET['id'] : ''?>"},
							function(res){
								console.log(res);

								if(res.type == 'signup'){
									bootbox.alert('User not found.');
									// var d = new Date();
									// var n = d.getMilliseconds();
									//
									// window.location.href = base_url + '?n=' + n + '#signup';
								}
								else if(res.type == 'complete_signup'){
									$('#MyLinkedInButton').html('<i class="fa fa-linkedin"></i> Connnected to LinkedIn').addClass('disabled');
									bootbox.alert(action_messages.success.complete_profile_via_social);
								}

								else{
									if(res.userdata.super_admin == 'Y'){
										window.location.href = base_url+'webmanager';
										console.log(base_url+'webmanager');
									}
									else{
										window.location.href = base_url+'panel'; //'http://'+res.userdata.domain+'.ebinder.com.au/panel';
										console.log(base_url+'panel');
									}

								}
							},
							'json'
						).done(function(res){
							$mdl.modal('hide');
						}).error(function(err){
							console.log(err);
						});

					}

					return false;
				});

				$("#MyLinkedInButton").bind('click',function () {
					IN.User.authorize();
					//
					// IN.Event.on(IN, 'auth', function(){
					// 	/* your code here */
					// 	//for instance
					// 	console.log('auth success');
					// 	IN.API.Raw("people/~:(id,first-name,last-name,location,email-address)?format=json").result(function(res){
					// 		console.log('peps', res);
					// 	}).error(onError);
					// });

					return false;
				});


			});
		}

    </script>



</body>
</html>
