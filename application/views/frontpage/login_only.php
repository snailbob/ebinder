            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">
                          <?php if(count($active_domain) > 0) { ?>

                            <div class="zendesk-loginx">


                                <div class="row" style="margin-top: 100px;">
                                    <div class="col-md-4 col-md-offset-4">
                                        <p class="text-center" style="margin-bottom: 10vh">
                                          <a href="#" style="margin: 0 auto">
                                            <span><img src="<?php echo $this->common->agency_logo($active_domain[0]['id']);?>" /></span>
                                            </a>

                                        </p>


                                        <h3  class="text-center" style="font-size: 18px;">Login to your account</h3>
                                        <hr class="star-primary">
                                        <p class="text-center">Don't have an account? <a href="#" class="signup_btn">Request access</a><br /><br /></p>


                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <a href="javascript:;" onclick="PopupCenter('<?php echo base_url().'home/subhome?the_domain='.$this->common->the_domain() ?>','xtf','400','250')" class="subdomain_socials_btn btn btn-default btn-block">
                                                      Sign in using Social Account?
                                                    </a>
                                                    <hr>
                                                    <div class="row social-btns hidden">
                                                        <div class="col-sm-6">
                                                            <div class="form-groupx">
                                                                <button type="button" class="btn btn-default btn-block google-auth-btn" id="customBtn2"><i class="fa fa-google-plus text-danger"></i> Google+</button>
                                                            </div>
                                                        </div>

                                                        <div class="col-sm-6">
                                                            <div class="form-groupx">
                                                                <button type="button" class="btn btn-primary btn-block linkedin-auth-btn" id="MyLinkedInButton2"><i class="fa fa-linkedin"></i> LinkedIn</button>
                                                            </div>
                                                        </div>

                                                        <hr class="star-primary">

                                                    </div>

                                                    <form class="omb_loginForm" id="login_form">
                                                        <input type="hidden" name="domain_name" value="<?php echo $this->common->the_domain() ?>">
                                                          <input type="hidden" name="google_id">
                                                        <input type="hidden" name="facebook_id">
                                                        <input type="hidden" class="form-control refer_from" name="refer_from" value="<?php //if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; } else { echo $this->session->userdata('id'); }?>">
                                                        <input type="hidden" name="from_buyform" value="<?php if(isset($_GET['refer_from'])) { echo $_GET['refer_from']; }?>">

                                                        <div class="form-group hidden">
                                                            <div class="dropdown dropdown-form dropdown-login">
                                                              <button class="btn btn-primary btn-block btn-outline"type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-chevron-down fa-fw pull-right" style="margin-right: 10px;"></i>
                                                                Log In as <span class="u_type"> - Agency</span>
                                                              </button>
                                                              <ul class="dropdown-menu" aria-labelledby="dLabel">
                                                                <li class="active"><a href="#" data-type="Agency">Agency</a></li>

                                                                <li><a href="#" data-type="Customer">Customer</a></li>
                                                              </ul>
                                                            </div>

                                                            <input type="hidden" name="utype" value="Customer" />
                                                        </div>

                                                        <div class="form-group input_icon">
                                                              <span class="mobile-icon">
                                                                <i class="fa fa-envelope fa-fw"></i>
                                                              </span>
                                                            <input type="email" class="form-control" name="email" placeholder="Email Address">

                                                        </div>

                                                        <div class="form-group input_icon">
                                                              <span class="mobile-icon">
                                                                <i class="fa fa-lock fa-fw fa-2x"></i>
                                                              </span>
                                                            <input  type="password" class="form-control" name="password" placeholder="Password">

                                                        </div>

                                                        <div class="form-group hidden">
                                                            <label>Log In as</label>
                                                            <input type="checkbox" name="usertype" checked>
                                                        </div>

                                                        <div class="form-group">
                                                            <button class="btn btn-warning btn-block" type="submit">Log In</button>
                                                        </div>


                                                    </form>

                                                    <p class="omb_forgotPwd text-center">
                                                        <a href="#" class="forgot_btn">Forgot password?</a>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>




                                    </div>
                                </div>
                            </div>


                            <?php } else { ?>
                              <div class="row" style="padding-top: 100px;">
                                <div class="col-md-4 col-md-offset-4">
                                    <p class="text-center">
                                      <a href="#" style="margin: 0 auto">
                                        <span><img src="<?php echo $this->common->logo(0);?>" /></span>
                                        </a>
                                    </p>
                                </div>



                                <div class="col-sm-8 col-sm-offset-2">
                                  <div class="alert alert-info"style="margin-top: 50px;">
                                    <i class="fa fa-info-circle"></i> Oops, looks like the page you're trying to access is no longer available.
                                  </div>

                                </div>
                              </div>

                            <?php } ?>

                        </div><!--modal-body-->



                    </div>


                </div>
            </div>
