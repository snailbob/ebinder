
<!-- VIEWDEP -->





<!DOCTYPE html>
<!-- <html manifest="/public/manifest.appcache"> -->
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
    <meta name="format-detection" content="telephone=no">

    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/procurify/libs/images/favicon.3f215dbd8fc1.ico" type="image/x-icon" />
    <link rel="icon" href="<?php echo base_url() ?>assets/procurify/libs/images/favicon.3f215dbd8fc1.ico" type="image/x-icon" />

    <script src="<?php echo base_url() ?>assets/procurify/libs/vendors/modernizer/modernizer.284832b56faa.js" type="text/javascript"></script>

    <title>Ebinder</title>



    




<link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/modules/core/css/bootstrap-custom.38c7026bb3d0.css" />
<!-- bower:css -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/angular-ui-select/dist/select.195cf0cf9693.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/animate.css/animate.56848eb884e1.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/cropper/dist/cropper.e57b9d506002.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/ng-cropper/dist/ngCropper.all.4a4855d4ac35.css" />
<!-- endbower -->


<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/vendors/select2/css/select2.min.8969ac4614d6.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/vendors/perfect-scrollbar/perfect-scrollbar.min.7682fde59eb4.css">

<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/modules/core/css/base.4d00a8ae404e.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/modules/core/css/main.c3df550093b8.css">

<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/modules/inbox/css/inbox.96b1886386fa.css">
    
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/procurify/libs/modules/settings/css/settings.01caf03ee8de.css">

</head>
<body>

    <!-- HEADER -->
    

<header class="app-header">
    <a class="header-logo" href="/">
        <img src="<?php echo base_url() ?>assets/procurify/images/logo/logo-horizontal-64.7a505e99594f.png"/>
    </a>

    <div class="header-content">

        <!--<a class="btn__shortcut-add untouchable" role="button" tabindex="0">
            <span class="btn__shortcut-add__icon">+</span>
        </a>-->

        
            
    
        <!--<a href="javascript:;" class="btn btn--header btn--orange btn--trial">
            <div class="btn--trial_days">14</div>
            <div class="btn--trial_text">
                <p class="small">days remaining</p>
                
                
                <p class="text-bold">Enter Payment</p>
                
                
            </div>
        </a>-->
    

        


        <!--<a class="btn btn--plain btn--hollow btn--header btn--help hide"
            id="Intercom"
            href="">
            <i class="icon-message"></i><span class="caption">Chat with us</span>
        </a>-->

        <a class="header-section help-btn-js text-color-white" role="button" tabindex="0">
            <i class="header-section-icon fa fa-question-circle fa-fw"></i>
        </a>

        <!-- <div class="help-btn  ml10"><i class="fa fa-question"></i></div> -->

        

        <a class="header-section header-notification-btn-js text-color-white"
            notification-popup
            role="button"
            tabindex="0"
            id="header-notifification-app">
            <i class="header-section-icon fa fa-bell-o fa-fw"></i>
            <span class="header-section-badge ng-cloak">1</span>
        </a>

        <label class="header-profile" for="header-profile">
            <div class="header-profile-image image image--40px inline-block"
                 style="background-image: url(https://www.gravatar.com/avatar/a63b6aa45eea407787c36956598417bf?d=https://storage.googleapis.com/procurifylite/default.png&s=120)"></div>
            <p class="header-profile-name">Nga Nguyen </p>

            <i class="icon-chevron-down-thick"></i>

            <input type="checkbox" id="header-profile" class="hide"/>

            <ul class="dropdown-menu">
                <li><a href="/accounts/profile/">Account</a></li>
                <li><a href="/accounts/logout/">Logout</a></li>
            </ul>
        </label>
    </div>

</header>


    <!-- NAVIGATION -->
    <div class="l-nav-container clearfix">
    <nav class="nav ">
        
        <div class="nav-parent  " >

            
                <a class="nav-parent-a " href="/">
                    <div class="nav-icon">
                        <div class="nav-icon-tooltip" tooltip title="Dashboard"></div>
                        
                            <i class="icon-home"></i>
                        
                    </div>
                    <span class="nav-label">Dashboard</span>
                    <div class="nav-badge nav-badge-dashboard"></div>
                </a>
            

            

            

        </div>
        
        <div class="nav-parent  has-child" >

            
                <input class="hide" type="checkbox" id="nav-child-2"  />

                <label class="nav-parent-a nav--has-child" for="nav-child-2">
                    <div class="nav-icon">
                        
                            <i class="icon-request"></i>
                        
                    </div>
                    <span class="nav-label">Request</span>
                    <div class="nav-badge nav-badge-request"></div>
                </label>
            

            
            <ul class="nav-child nav-child-request">
                
                
                <li class="nav-subheader nav-subheader--collapsed-only">
                    <span>Request</span>
                </li>
                
                
                
                <li>
                    <a class="" href="/status/orders/">
                        <span class="nav-label">Order</span>
                        <div class="nav-badge nav-badge-request-order"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/status/travel/">
                        <span class="nav-label">Travel</span>
                        <div class="nav-badge nav-badge-request-travel"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/status/expense-report/">
                        <span class="nav-label">Expense</span>
                        <div class="nav-badge nav-badge-request-expense"></div>
                    </a>
                </li>
                
                
            </ul>
            

            

        </div>
        
        <div class="nav-parent  has-child" >

            
                <input class="hide" type="checkbox" id="nav-child-3"  />

                <label class="nav-parent-a nav--has-child" for="nav-child-3">
                    <div class="nav-icon">
                        
                            <i class="icon-approval"></i>
                        
                    </div>
                    <span class="nav-label">Approval</span>
                    <div class="nav-badge nav-badge-approval"></div>
                </label>
            

            
            <ul class="nav-child nav-child-approval">
                
                
                <li class="nav-subheader nav-subheader--collapsed-only">
                    <span>Approval</span>
                </li>
                
                
                
                <li>
                    <a class="" href="/approvals/pending/">
                        <span class="nav-label">Order</span>
                        <div class="nav-badge nav-badge-approval-order"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/approvals/travel/">
                        <span class="nav-label">Travel</span>
                        <div class="nav-badge nav-badge-approval-travel"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/approvals/expense-report/">
                        <span class="nav-label">Expense</span>
                        <div class="nav-badge nav-badge-approval-expense"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/approvals/bill/">
                        <span class="nav-label">Bill</span>
                        <div class="nav-badge nav-badge-approval-bill"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/approvals/payment/">
                        <span class="nav-label">Payment</span>
                        <div class="nav-badge nav-badge-approval-payment"></div>
                    </a>
                </li>
                
                
            </ul>
            

            
            <div class='nav-badge nav-badge--approval'></div>
            

        </div>
        
        <div class="nav-parent  has-child" >

            
                <input class="hide" type="checkbox" id="nav-child-4"  />

                <label class="nav-parent-a nav--has-child" for="nav-child-4">
                    <div class="nav-icon">
                        
                            <i class="icon-procure"></i>
                        
                    </div>
                    <span class="nav-label">Procure</span>
                    <div class="nav-badge nav-badge-procure"></div>
                </label>
            

            
            <ul class="nav-child nav-child-procure">
                
                
                <li class="nav-subheader nav-subheader--collapsed-only">
                    <span>Procure</span>
                </li>
                
                
                
                <li>
                    <a class="" href="/procure/">
                        <span class="nav-label">Procurement</span>
                        <div class="nav-badge nav-badge-procure-procurement"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/po/history/open/">
                        <span class="nav-label">P.O. History</span>
                        <div class="nav-badge nav-badge-procure-po-history"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/vendor/control/">
                        <span class="nav-label">Vendors</span>
                        <div class="nav-badge nav-badge-procure-vendors"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/catalog/">
                        <span class="nav-label">Product Catalog</span>
                        <div class="nav-badge nav-badge-procure-product-catalog"></div>
                    </a>
                </li>
                
                
            </ul>
            

            
            <div class='nav-badge nav-badge--procure'></div>
            

        </div>
        
        <div class="nav-parent  " >

            
                <a class="nav-parent-a " href="/receive/">
                    <div class="nav-icon">
                        <div class="nav-icon-tooltip" tooltip title="Receive"></div>
                        
                            <i class="icon-receive"></i>
                        
                    </div>
                    <span class="nav-label">Receive</span>
                    <div class="nav-badge nav-badge-receive"></div>
                </a>
            

            

            
            <div class='nav-badge nav-badge--receive'></div>
            

        </div>
        
        <div class="nav-parent  has-child" >

            
                <input class="hide" type="checkbox" id="nav-child-6"  />

                <label class="nav-parent-a nav--has-child" for="nav-child-6">
                    <div class="nav-icon">
                        
                            <i class="icon-ap"></i>
                        
                    </div>
                    <span class="nav-label">Accounts Payable</span>
                    <div class="nav-badge nav-badge-accounts-payable"></div>
                </label>
            

            
            <ul class="nav-child nav-child-accounts-payable">
                
                
                <li class="nav-subheader nav-subheader--collapsed-only">
                    <span>Accounts Payable</span>
                </li>
                
                
                
                <li>
                    <a class="" href="/invoices/">
                        <span class="nav-label">Bills</span>
                        <div class="nav-badge nav-badge-accounts-payable-bills"></div>
                    </a>
                </li>
                
                
                
                <li class="nav-subheader ">
                    <span>Purchase Order</span>
                </li>
                
                
                
                <li>
                    <a class="" href="/unbilled-items/">
                        <span class="nav-label">Unbilled Items</span>
                        <div class="nav-badge nav-badge-accounts-payable-unbilled-items"></div>
                    </a>
                </li>
                
                
                
                <li class="nav-subheader ">
                    <span>Expense</span>
                </li>
                
                
                
                <li>
                    <a class="" href="/unbilled-expense-items">
                        <span class="nav-label">Unbilled Items</span>
                        <div class="nav-badge nav-badge-accounts-payable-unbilled-items"></div>
                    </a>
                </li>
                
                
                
                <li class="nav-subheader ">
                    <span>Spend</span>
                </li>
                
                
                
                <li>
                    <a class="" href="/ap/accrual-report">
                        <span class="nav-label">Accrual Report</span>
                        <div class="nav-badge nav-badge-accounts-payable-accrual-report"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/ap/spend-tracker">
                        <span class="nav-label">Spend Tracker</span>
                        <div class="nav-badge nav-badge-accounts-payable-spend-tracker"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/payment/expense-reports/6/2016/0/">
                        <span class="nav-label">Expense Records</span>
                        <div class="nav-badge nav-badge-accounts-payable-expense-records"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/reports/receiving/">
                        <span class="nav-label">Receiving Summary</span>
                        <div class="nav-badge nav-badge-accounts-payable-receiving-summary"></div>
                    </a>
                </li>
                
                
                
                <li class="nav-subheader ">
                    <span>Payment</span>
                </li>
                
                
                
                <li>
                    <a class="" href="/ap/payment-processing">
                        <span class="nav-label">Payment Processing</span>
                        <div class="nav-badge nav-badge-accounts-payable-payment-processing"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/ap/payee-management">
                        <span class="nav-label">Payee Management</span>
                        <div class="nav-badge nav-badge-accounts-payable-payee-management"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/ap/payment-log">
                        <span class="nav-label">Payment Log</span>
                        <div class="nav-badge nav-badge-accounts-payable-payment-log"></div>
                    </a>
                </li>
                
                
            </ul>
            

            
            <div class='nav-badge nav-badge--payment'></div>
            

        </div>
        
        <div class="nav-parent  has-child" >

            
                <input class="hide" type="checkbox" id="nav-child-7"  />

                <label class="nav-parent-a nav--has-child" for="nav-child-7">
                    <div class="nav-icon">
                        
                            <i class="icon-manage"></i>
                        
                    </div>
                    <span class="nav-label">Manage</span>
                    <div class="nav-badge nav-badge-manage"></div>
                </label>
            

            
            <ul class="nav-child nav-child-manage">
                
                
                <li class="nav-subheader nav-subheader--collapsed-only">
                    <span>Manage</span>
                </li>
                
                
                
                <li>
                    <a class="" href="/finance/budget-control/">
                        <span class="nav-label">Budget Control</span>
                        <div class="nav-badge nav-badge-manage-budget-control"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/finance/chart-of-accounts/">
                        <span class="nav-label">Chart of Accounts</span>
                        <div class="nav-badge nav-badge-manage-chart-of-accounts"></div>
                    </a>
                </li>
                
                
            </ul>
            

            

        </div>
        
        <div class="nav-parent  has-child" >

            
                <input class="hide" type="checkbox" id="nav-child-8"  />

                <label class="nav-parent-a nav--has-child" for="nav-child-8">
                    <div class="nav-icon">
                        
                            <i class="icon-report"></i>
                        
                    </div>
                    <span class="nav-label">Reports</span>
                    <div class="nav-badge nav-badge-reports"></div>
                </label>
            

            
            <ul class="nav-child nav-child-reports">
                
                
                <li class="nav-subheader ">
                    <span>Report Summary</span>
                </li>
                
                
                
                <li>
                    <a class="" href="/payment/expense-reports/6/2016/0/">
                        <span class="nav-label">Expense Summary</span>
                        <div class="nav-badge nav-badge-reports-expense-summary"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/reports/receiving/">
                        <span class="nav-label">Receiving Summary</span>
                        <div class="nav-badge nav-badge-reports-receiving-summary"></div>
                    </a>
                </li>
                
                
                
                <li class="nav-subheader ">
                    <span>Spending</span>
                </li>
                
                
                
                <li>
                    <a class="" href="/reports/vendor/filter/0/6/2016/">
                        <span class="nav-label">Orders by Vendor</span>
                        <div class="nav-badge nav-badge-reports-orders-by-vendor"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/reports/spending/">
                        <span class="nav-label">Orders by User</span>
                        <div class="nav-badge nav-badge-reports-orders-by-user"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/reports/department/">
                        <span class="nav-label">Orders by Department</span>
                        <div class="nav-badge nav-badge-reports-orders-by-department"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/reports/expense/filter/">
                        <span class="nav-label">Expense by Department</span>
                        <div class="nav-badge nav-badge-reports-expense-by-department"></div>
                    </a>
                </li>
                
                
            </ul>
            

            

        </div>
        
        <div class="nav-parent  " >

            
                <a class="nav-parent-a active" href="/settings/">
                    <div class="nav-icon">
                        <div class="nav-icon-tooltip" tooltip title="Settings"></div>
                        
                            <i class="icon-settings"></i>
                        
                    </div>
                    <span class="nav-label">Settings</span>
                    <div class="nav-badge nav-badge-settings"></div>
                </a>
            

            

            

        </div>
        
        <div class="nav-parent nav-more nav-more-js">
            <label class="nav-parent-a">
                <div class="nav-icon">
                    <i class="icon-more"></i>
                </div>
                <span class="nav-label">More</span>
            </label>
        </div>
        <div class="nav-mask"></div>
        <div class="subnav"></div>

    </nav>
    <div class="nav-info text-center">
        <a class="nav-info_icon block mb20 text-center">
            <i class="icon-procurify"></i>
        </a>
        <a href="http://goo.gl/Nx797s"
           target="_blank">
           Change log
        </a>
        <a href="https://goo.gl/pwVECK"
           target="_blank">iPhone</a>
        <a href="https://goo.gl/fOv0Ng"
           target="_blank">Android</a>
        <a href="javascript:;" class="refer-btn-js">Refer a friend</a>
        <a href="http://procurify-features.uservoice.com/forums/328818-procurify-ideas"
           target="_blank">What should we add next?</a>
        
    </div>
</div>


    <!-- CONTENT -->
    <div class="l-app-container l-content-container">
        
<div ng-app="SettingsApp">
	<div notifications></div>

    <!--==================================*\
    # Templates
    - settings.main.tpl.html
    \*===================================-->
	<div ui-view>
        <div class="route-loader"></div>
    </div>
</div>

    </div>

    <!-- End of Contents -->
    



<!--<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/libs.min.432c9e1fffd8.js"></script>
-->

<!-- SYSTEM SCRIPTS -->
<script   src="https://code.jquery.com/jquery-3.0.0.min.js"   integrity="sha256-JmvOoLtYsmqlsWxa7mDSLMwa6dZ9rrIdtrrVYRnDRH0="   crossorigin="anonymous"></script>


<!-- VENDOR SCRIPTS -->
<!-- <script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/select2/js/select2.min.97eccda64530.js"></script> -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/select2/js/select2.full.af05bb973f4d.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/slimscroll/slimscroll.min.550e27427204.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/perfect-scrollbar/perfect-scrollbar.jquery.min.b4e42d596127.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/placeholders.min.d07c9c7babb3.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/medium-editor.min.b8b1ba8cc843.js"></script>
<script src="https://js.pusher.com/3.0/pusher.min.js"></script>


</body>
</html>
