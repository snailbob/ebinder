
  
  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
  
      <div class="row">
  
      
        <div class="col-lg-12">

      
           
        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>      
        <?php } ?>
        
        <div class="module_header">
            <i class="fa fa-file-text fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
        </div>

        <div class="panel">
        	<div class="panel-body">

                        <?php if(count($responses) > 0) { ?>
                        
                        
						<div class="table-responsivex">
                        
                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <th>Customer</th>
                              <th>Insurance Products</th>
                              <th>Status</th>
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>                        
                        
                        <?php
							$count = 1;
							foreach($responses as $u=>$value){
						?>
                        
                            <tr>
                             
                              <td class="hidden"><?php echo $count; ?></td>
                              <td><?php echo $value['user_info']['first'].' '.$value['user_info']['last'] ?></td>
                             
                              <td><?php echo $value['user_info']['prod_text'] ?></td>
                             
                              <td><?php echo $value['status_text']; ?></td>

                              <td>
                                                                  
                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn--blue btn--small dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                        
                                        <li><a href="/panel/products/responses/<?php echo $value['id'] ?>">View Responses</a></li>
                                      </ul>
                                    </div>                              
                              
                              
                              </td>
                            </tr>
                        
                        <?php
								$count++;
							}
						?>


                          
                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->
                          
                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">No '.$title.'</p></div>';	
						}?>
                        

            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->
    