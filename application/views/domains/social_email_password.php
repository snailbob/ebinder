

  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">

      <div class="row">


        <div class="col-lg-12">




        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>
        <?php } ?>

        <div class="module_header">
            <i class="fa fa-globe fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <?php if($user['customer_type'] == 'N' && $user['studio_id'] == '0') {?>
            <ul class="module_actions right hidden">
                <li>
                    <a class="module_action btn--red" href="/panel/addmember">
                        <div class="module_action_icon">
                            <i class="fa fa-user-plus"></i>
                        </div>
                        <span class="module_action_text text-uppercase"> Add <?php echo $singular ?></span>
                    </a>
                </li>
            </ul>
            <?php } ?>

        </div>

        <div class="panel panel-default">

          <div class="panel-heading">

            <div  class="panel-title">
                  <h4><?php echo $title; ?></h4>
            </div>
          </div>

          <div class="panel-body" style="padding: 15px 30px;">
              <div class="row">
                <div class="col-sm-6 col-sm-offset-3">


                  <form class="pwstrength_field" id="update_password_form">
                    <input type="hidden" name="id" value="<?php echo $user_sess['id'] ?>" ?>
                    <div class="form-group">
                        <input  type="password" class="form-control" name="pww" id="pww" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <input  type="password" class="form-control" name="pww2" placeholder="Confirm Password">

                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="pwstrength_viewport_progress"></div>
                            </div>
                            <div class="col-sm-12">
                                <div class="well well-sm">
                                    Use a mix of letters, numbers, and symbols in your password
                                </div>
                            </div>

                        </div>
                    </div>


                    <div class="form-group">
                        <button class="btn btn-warning" type="submit">Submit</button>
                    </div>

                  </form>


                </div>
              </div>
          </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->
