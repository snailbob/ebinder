

  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">

      <div class="row">


        <div class="col-lg-12">



        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>
        <?php } ?>


        <div class="module_header">
            <i class="fa fa-user fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <ul class="module_actions right hidden">
                <li>
                    <a class="module_action btn-warning" href="<?php echo base_url() ?>panel/quote">
                        <div class="module_action_icon">
                            <i class="fa fa-plus-square"></i>
                        </div>
                        <span class="module_action_text text-uppercase"> Add <?php echo $singular ?></span>
                    </a>
                </li>
            </ul>

        </div>



        <div class="panel panel-default">

          <div class="panel-heading">

            <div  class="panel-title">
                  <h4>Add <?php echo $title; ?></h4>
            </div>
          </div>


          <div class="panel-body">


				<form novalidate="novalidate" id="claim_form">
                    <div class="form-group">
                        <label>New claim</label>
                        <input type="text" class="form-control" name="claim" value="" required="required"/>
                    </div>

                    <div class="form-group">
                        <label>Select customer</label>
                        <select class="form-control" name="customer_id" required="required">
                            <option value="" data-name="" data-inputs='[]'>Select</option>
                            <?php
                                if(count($customers) > 0){
                                    foreach($customers as $r=>$value){
                                        echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </div>

                    <div class="form-group">
                        <label>Select policy</label>
                        <select class="form-control" name="policy_id" required="required">
                            <option value="" data-name="" data-inputs='[]'>Select</option>
                            <?php
                                if(count($biz_referral) > 0){
                                    foreach($biz_referral as $r=>$value){
                                        echo '<option value="'.$value['id'].'">'.$value['policy_id'].' - '.$value['status'].'</option>';
                                    }
                                }
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Claim details</label>
                        <input type="text" class="form-control" name="details" value="" required="required"/>
                    </div>

                    <div class="form-group">
                        <label>Upload any documents (if applicable)</label><br />

                        <a href="javascript:;" onclick="$('#myForm').find('#myfile').click()" class="btn btn-default"><i class="fa fa-paperclip"></i> Attach File</a>

                        <a class="btn btn-default disabled" id="progress" style="display: none;">
                            <div id="bar"></div>
                            <div id="percent">0%</div>
                        </a>
                    </div>


                    <div class="form-group">

                        <div class="col-xs-12">

                        	<div id="file_up_text" style="display: none;">File Attached: <div id="message"><a href=""></a></div></div>

                        </div>

                    </div>


                    <hr />

                    <div class="form-group text-right">
                        <a href="<?php echo base_url().'panel/claims' ?>" class="btn btn-default btn-cancel btn-grey">Cancel</a>
                        <button type="submit" class="btn btn-warning">Submit</button>
                    </div>
                </form>


               <div class="row upload_sec hidden">


                 <div class="col-xs-6">
                    <form id="myForm" action="<?php echo base_url().'formsubmits/upload_file' ?>" method="post" enctype="multipart/form-data">
                         <input type="file" size="60" id="myfile" name="myfile" onchange="$('#myForm').submit();">
                         <input type="hidden" id="uniquetime" name="uniquetime" value="<?php echo $uniquetime; ?>">
                         <input type="submit" class="hidden" value="Ajax File Upload">
                     </form>
                 </div>


               </div>



            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->
