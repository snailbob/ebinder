<div class="panel panel-default">

<!-- List group -->
  <ul class="list-group">
    <?php
    if(!empty($agency_log)){
      foreach($agency_log as $r=>$value){ ?>

            <li class="list-group-item">
              <strong>Last updated: </strong><?php echo date_format(date_create($value['date_added']),'d M Y') ?>
              <?php if($type == 'base_rate' || $type == 'base_currency' || $type == 'minimum_premium' || $type == 'policy_fee') { ?>
                <br><strong>Pre-change value: </strong><?php echo $value['old_val'] ?>
                <br><strong>Post-change value: </strong><?php echo $value['new_val'] ?>
              <?php }?>
              <br><strong>Updated by : </strong><?php echo ($value['user_id'] == '6') ? 'Webmanager' : $this->common->customer_name($value['user_id']); ?>
            </li>


        <?php
      }
    }
    else{
      echo '<li class="list-group-item text-center text-muted">Nothing to show you.</li>';
    }
     ?>

  </ul>

</div>
