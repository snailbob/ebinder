
  
  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
  
      <div class="row">
  
      
        <div class="col-lg-12">

                 
            
        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>      
        <?php } ?>
        
        <div class="module_header">
            <i class="fa fa-gear fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <?php if($user['customer_type'] == 'N' && $user['studio_id'] == '0') {?>
            <ul class="module_actions right">
                <li>
                    <a class="module_action btn--red" href="/panel/addmember">
                        <div class="module_action_icon">
                            <i class="fa fa-user-plus"></i>
                        </div>
                        <span class="module_action_text text-uppercase"> Add <?php echo $singular ?></span>
                    </a>
                </li>
            </ul>
            <?php } ?>
    
        </div>

        <div class="panel">
        	<div class="panel-body">

            
                <div class="row social-btns">
                    <div class="col-md-6 col-md-offset-3">
                    
                        <div class="row social-btns">
                            <div class="col-sm-6">
                                <div class="form-groupx">                              
                
                					<?php if($user['google_id'] == ''){ ?>
                                
                                    <button type="button" class="btn btn--white btn-block" id="customBtn"><i class="fa fa-google-plus text-danger"></i> Connect Google+</button>
                                    <?php } else {?>
                                    <button type="button" class="btn btn--white btn-block " disabled><i class="fa fa-google-plus text-danger"></i> Google+ Connected</button>
                                    <?php } ?>
                                    
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="form-groupx">
                					<?php if($user['fb_id'] == ''){ ?>
                
                                    <button type="button" class="btn btn--white btn-block" id="MyLinkedInButton"><i class="fa fa-linkedin text-primary"></i> Connect LinkedIn</button>
                                    <?php } else {?>
                                    <button type="button" class="btn btn--white btn-block " disabled><i class="fa fa-linkedin text-danger"></i> LinkedIn Connected</button>
                                    <?php } ?>
                                    

                                    
                                </div>
                            </div> 
                        </div>
                    </div> 
                </div>

            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->
    