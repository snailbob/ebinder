
  
  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
  
      <div class="row">
  
      
        <div class="col-lg-12">

                     
      
        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>      
        <?php } ?>
        
        
        <div class="module_header">
            <i class="fa fa-user-plus fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <ul class="module_actions right hidden">
                <li>
                    <a class="module_action" href="/approvals/order/pending/csv?subscribed=">
                        <div class="module_action_icon">
                            <i class="icon-export"></i>
                        </div>
                        <span class="module_action_text">EXPORT CSV</span>
                    </a>
                </li>
            </ul>
        </div>
        
        
    
        <div class="the-insurance-form">
        
            <form id="addmember_form" class="text-left">
       			<input type="hidden" name="user_id" value="<?php echo $user['id']?>" />
       			<input type="hidden" name="customer_type" value="<?php echo $user['customer_type']?>" />
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h3 class="heading-title">Member</h3>
                </div>
                <div class="panel-body">
        
        
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" />
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" name="first" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" name="last" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Company Name</label>
                            <input type="text" class="form-control" name="bname" />
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" class="form-control geolocation" name="location" />
                            <input type="hidden" name="lat" />
                            <input type="hidden" name="lng" />
                            
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Contact Number</label>
                                    <select class="form-control" name="country_id">
                                    	<option value="">Select Location Code</option>
										<?php foreach($countries as $r=>$value) {
											echo '<option value="'.$value['country_id'].'">'.$value['short_name_iso2'].'</option>';	
											
										}?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <input type="text" class="form-control" name="calling_digits" />
                                </div>
                            </div>
                        </div>
           
                        <div class="row">
                            <div class="col-sm-6">
                                
                   
                                <div class="form-group">
                                    <label>User Type</label>
                                    <select class="form-control" name="super_admin" required="required">
                                        <option value="" selected="selected">Select</option>
                                        <option value="N">Regular user</option>
                                        <option value="Y">Super admin</option>
                                    </select>
                                </div>

                            
                            </div>
                            <div class="col-sm-6">
                                
                   
                                <div class="form-group">
                                    <label>Privilege</label>
                                    <select class="form-control" name="privileges" required="required">
                                        <option value="" selected="selected">Select</option>
                                        <option value="Enable view billing details">Enable view billing details</option>
                                        <option value="Disable view billing details">Disable view billing details</option>
                                    </select>
                                </div>

                            
                            </div>
                        </div>

                </div>
            </div><!--.panel-->
            
            
                
                
                
            <div class="panel panel-white">
                <div class="panel-body">
    
    
                        <div class="form-groupx">
                            <button class="btn btn--orange" type="submit">Add </button>
                        </div>
                    
                </div>
            </div><!--.panel-->
            
            </form>
        </div><!--.the-insurance-form-->
    

    </div>
</div>

    </div>

    <!-- End of Contents -->
    