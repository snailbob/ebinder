<?php
	$user_session = (isset($user_session)) ? $user_session : $this->session->all_userdata();

?>
<?php if($this->uri->segment(1) != 'webmanager') { ?>
<!-- HEADER -->
<header class="app-header">

    <?php /*?><a class="" href="<?php echo $this->common->base_url() ?>">
    </a><?php */?>
    
    <a class="navbar-brand header-logo" href="#">
        <img src="<?php echo $this->common->logo($user_session['id']);//echo base_url().'assets/procurify/images/logo/logo-horizontal-64.7a505e99594f.png' ?>"/>
    </a>
    <a class="header-section text-color-white visible-xs pull-right" role="button" tabindex="0" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <i class="header-section-icon fa fa-bars fa-fw"></i>
    </a>
    <div class="header-content hidden-xs">

	
        <a class="header-section text-color-white visible-xs" role="button" tabindex="0" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <i class="header-section-icon fa fa-bars fa-fw"></i>
        </a>
        
    
        <div class="hidden-xs">
        
        
            <?php /*?><a class="header-section help-btn-js text-color-white" role="button" tabindex="0">
                <i class="header-section-icon fa fa-question-circle fa-fw"></i>
            </a><?php */?>
    
            <!-- <div class="help-btn  ml10"><i class="fa fa-question"></i></div> -->
    
            
    
            <label class="header-profile nav-toggle <?php echo (isset($user_session['customer_quote'])) ? 'hidden' : ''?>" for="header-profile">


                <i class="fa fa-cog fa-2x pull-right"></i>
              
                <p class="text-left">
					<strong><?php echo $user_session['name'] ?></strong>
					<small><?php echo $user_session['email'] ?></small>
                </p>
    
    
                <?php /*?><input type="checkbox" id="header-profile" class="hide"/>
    
                <ul class="dropdown-menu">
                    <li class="hidden"><a href="#">Settings</a></li>
                    <li><a href="<?php echo $this->common->base_url() ?>panel/logout/">Logout</a></li>
                </ul><?php */?>
            </label>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="visible-xs">
        <div id="navbar" class="collapse navbar-collapse" style="background: #009ddc">
          <ul class="nav navbar-nav my-navbar">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">Customers</a></li>
            <li><a href="#contact">Insurance Products</a></li>
            <li><a href="#contact">Settings</a></li>
            <li><a href="<?php echo $this->common->base_url() ?>panel/logout/">Logout</a></li>
          </ul>
        </div><!--/.nav-collapse -->
    </div>
    

	

</header>


    <!-- NAVIGATION -->
    <div class="l-nav-container clearfix hidden-xs <?php echo (isset($user_session['customer_view'])) ? 'hidden' : ''?>">
    <nav class="nav ">
        
        <div class="nav-parent  " >
           
                <a class="nav-parent-a <?php echo ($this->uri->segment(2) == '') ? 'active' : ''; ?>" href="<?php echo $this->common->base_url() ?><?php echo ($user_session['customer_type'] == 'Y') ? 'business/dashboard' : 'panel'?>">
                    <div class="nav-icon">
                        <div class="nav-icon-tooltip" data-toggle="tooltip" title="Dashboard" data-placement="right"></div>
                        
                            <i class="fa fa-globe fa-fw"></i>
                        
                    </div>
                    <span class="nav-label">Dashboard</span>
                    <div class="nav-badge nav-badge-dashboard"></div>
                </a>
            
        </div>
        
        
        <?php if ($user_session['customer_type'] != 'Y'){  ?>
        <div class="nav-parent  has-child" >
                <input class="hide" type="checkbox" id="nav-child-2"  />

                <label class="nav-parent-a nav--has-child <?php echo ($this->uri->segment(2) == 'customers' || $this->uri->segment(2) == 'use_customer' || $this->uri->segment(2) == 'quote' || $this->uri->segment(2) == 'managequote' || $this->uri->segment(2) == 'managepolicies') ? 'active' : ''; ?>" for="nav-child-2">
                    <div class="nav-icon">
                            <i class="fa fa-user fa-fw"></i>
                    </div>
                    <span class="nav-label">Users</span>
                    <div class="nav-badge nav-badge-request"></div>
                </label>
            
            <ul class="nav-child nav-child-request">
                <li class="nav-subheader nav-subheader--collapsed-only">
                    <span>Users</span>
                </li>
                
                <?php if($user_session['first_broker'] == 'Y') { ?>
                <li>
                    <a class="" href="<?php echo $this->common->base_url() ?>panel/customers/all">
                        <span class="nav-label">Brokers</span>
                        <div class="nav-badge nav-badge-request-order"></div>
                    </a>
                </li>
                <?php } ?>
                
                <li>
                    <a class="" href="<?php echo $this->common->base_url() ?>panel/use_customer">
                        <span class="nav-label">Manage Customers</span>
                        <div class="nav-badge nav-badge-request-travel"></div>
                    </a>
                </li>
                
                <li>
                    <a class="" href="<?php echo $this->common->base_url() ?>panel/quote">
                        <span class="nav-label">Add New Quote</span>
                        <div class="nav-badge nav-badge-request-travel"></div>
                    </a>
                </li>
             
                <li>
                    <a class="" href="<?php echo $this->common->base_url() ?>panel/managequote">
                        <span class="nav-label">Manage Quotes</span>
                        <div class="nav-badge nav-badge-request-travel"></div>
                    </a>
                </li>                
             
                <li>
                    <a class="" href="<?php echo $this->common->base_url() ?>panel/managepolicies">
                        <span class="nav-label">Manage Policies</span>
                        <div class="nav-badge nav-badge-request-travel"></div>
                    </a>
                </li>                
            </ul>
        </div>
        
		<?php if($user_session['first_broker'] == 'Y') { ?>
        <div class="nav-parent  " >
           
                <a class="nav-parent-a <?php echo ($this->uri->segment(2) == 'userlog') ? 'active' : ''; ?>" href="<?php echo $this->common->base_url() ?>panel/userlog">
                    <div class="nav-icon">
                        <div class="nav-icon-tooltip" data-toggle="tooltip" title="Access Log" data-placement="right"></div>
                        
                            <i class="fa fa-file-text-o fa-fw"></i>
                        
                    </div>
                    <span class="nav-label">Access Log</span>
                    <div class="nav-badge nav-badge-dashboard"></div>
                </a>
            
        </div>
		<?php } ?>
        
        
		<?php if($user['privileges'] == 'Enable view billing details' && $user['studio_id'] != '') {?>                
        <div class="nav-parent" >
           
                <a class="nav-parent-a <?php echo ($this->uri->segment(2) == 'billing') ? 'active' : ''; ?>" href="<?php echo $this->common->base_url() ?><?php echo 'panel/billing' ?>">
                    <div class="nav-icon">
                        <div class="nav-icon-tooltip" data-toggle="tooltip" title="Billing" data-placement="right"></div>
                        
                            <i class="fa fa-paypal fa-fw"></i>
                        
                    </div>
                    <span class="nav-label">Billing</span>
                    <div class="nav-badge nav-badge-dashboard"></div>
                </a>
            
        </div>
        <?php } ?>
        
        <div class="nav-parent" >
           
                <a class="nav-parent-a <?php echo ($this->uri->segment(2) == 'reporting') ? 'active' : ''; ?>" href="<?php echo $this->common->base_url() ?><?php echo 'panel/reporting' ?>">
                    <div class="nav-icon">
                        <div class="nav-icon-tooltip" data-toggle="tooltip" title="Reporting" data-placement="right"></div>
                        
                            <i class="fa fa-list-alt fa-fw"></i>
                        
                    </div>
                    <span class="nav-label">Reporting</span>
                    <div class="nav-badge nav-badge-dashboard"></div>
                </a>
            
        </div>
        
        <div class="nav-parent" >
           
                <a class="nav-parent-a <?php echo ($this->uri->segment(2) == 'claims' || $this->uri->segment(2) == 'claim') ? 'active' : ''; ?>" href="<?php echo $this->common->base_url() ?><?php echo 'panel/claims' ?>">
                    <div class="nav-icon">
                        <div class="nav-icon-tooltip" data-toggle="tooltip" title="Claims" data-placement="right"></div>
                        
                            <i class="fa fa-file-text fa-fw"></i>
                        
                    </div>
                    <span class="nav-label">Claims</span>
                    <div class="nav-badge nav-badge-dashboard"></div>
                </a>
            
        </div>
        
        
        <div class="nav-parent  has-child" >

            
                <input class="hide" type="checkbox" id="nav-child-3"  />

                <label class="nav-parent-a nav--has-child <?php echo ($this->uri->segment(2) == 'setup') ? 'active' : ''; ?>" for="nav-child-3">
                    <div class="nav-icon">
                        
                            <i class="fa fa-gear fa-fw"></i>
                        
                    </div>
                    <span class="nav-label">Setup</span>
                    <div class="nav-badge nav-badge-request"></div>
                </label>
            

            
            <ul class="nav-child nav-child-request">
                
                
                <li class="nav-subheader nav-subheader--collapsed-only">
                    <span>Setup</span>
                </li>
                
                
                <li>
                    <a class="" href="<?php echo $this->common->base_url() ?>panel/setup/profile">
                        <span class="nav-label">Profile</span>
                        <div class="nav-badge nav-badge-request-travel"></div>
                    </a>
                </li>
                
                <li>
                    <a class="" href="<?php echo $this->common->base_url() ?>panel/setup/logo">
                        <span class="nav-label">Logo</span>
                        <div class="nav-badge nav-badge-request-order"></div>
                    </a>
                </li>
                
                
                <?php /*?><li>
                    <a class="" href="<?php echo $this->common->base_url() ?>panel/setup/quote_slip">
                        <span class="nav-label">Quote Slip</span>
                        <div class="nav-badge nav-badge-request-travel"></div>
                    </a>
                </li><?php */?>
                
                <li>
                    <a class="" href="<?php echo $this->common->base_url() ?>panel/setup/payment_method">
                        <span class="nav-label">Payment Method</span>
                        <div class="nav-badge nav-badge-request-travel"></div>
                    </a>
                </li>
                
            </ul>

        </div>
        
        
        <?php } else {?>
        <div class="nav-parent  " >
           
                <a class="nav-parent-a <?php echo ($this->uri->segment(2) == 'customers') ? 'active' : ''; ?>" href="<?php echo $this->common->base_url() ?>business/customers">
                    <div class="nav-icon">
                        <div class="nav-icon-tooltip" data-toggle="tooltip" title="My Information" data-placement="right"></div>
                        
                            <i class="fa fa-user fa-fw"></i>
                        
                    </div>
                    <span class="nav-label">My Information</span>
                    <div class="nav-badge nav-badge-dashboard"></div>
                </a>
            
        </div>
        
        
        
        
        <div class="nav-parent  " >
                <a class="nav-parent-a <?php echo ($this->uri->segment(2) == 'settings') ? 'active' : ''; ?>" href="<?php echo $this->common->base_url() ?>panel/settings">
                    <div class="nav-icon">
                        <div class="nav-icon-tooltip" data-toggle="tooltip" title="Settings" data-placement="right"></div>
                        
                            <i class="fa fa-cog"></i>
                        
                    </div>
                    <span class="nav-label">Settings</span>
                    <div class="nav-badge nav-badge-settings"></div>
                </a>

        </div>
        
        <?php } ?>
        
        
        
        <div class="nav-mask"></div>
        <div class="subnav"></div>

    </nav>


</div>





    <nav id="sidebar-wrapper" class="sidebar-wrapper-admin">
      
        <div class="list-group">
        
        	
          <a href="<?php echo $this->common->base_url() ?>panel/setup/profile" class="list-group-item" onclick="$('.nav-toggle').click();">
          	<i class="fa fa-user fa-fw"></i> Profile
          </a>
          
          <a href="<?php echo $this->common->base_url() ?>panel/setup/logo" class="list-group-item" onclick="$('.nav-toggle').click();">
          	<i class="fa fa-file-image-o
 fa-fw"></i> Logo
          </a>
          
          <a href="<?php echo $this->common->base_url() ?>panel/setup/payment_method" class="list-group-item" onclick="$('.nav-toggle').click();">
          	<i class="fa fa-credit-card fa-fw"></i> Payment Method
          </a>
          
          
          <a href="<?php echo $this->common->base_url() ?>panel/logout" class="list-group-item" onclick="$('.nav-toggle').click();">
            <i class="fa fa-sign-out fa-fw"></i> Logout
          </a>
        </div>      
      
    </nav>
    
    
    <?php } else {?>
        
    <!-- HEADER -->
    <header class="app-header">
    	<div class="container">
            <a class="" href="#">
                <h2 style="color: #fff;">Ebinder Preview</h2>
            </a>
        </div>

    </header>
    <?php }?>