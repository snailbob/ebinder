
<!-- VIEWDEP -->





<!DOCTYPE html>
<!-- <html manifest="/public/manifest.appcache"> -->
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
    <meta name="format-detection" content="telephone=no">

    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/procurify/libs/images/favicon.3f215dbd8fc1.ico" type="image/x-icon" />
    <link rel="icon" href="<?php echo base_url() ?>assets/procurify/libs/images/favicon.3f215dbd8fc1.ico" type="image/x-icon" />

    <script src="<?php echo base_url() ?>assets/procurify/libs/vendors/modernizer/modernizer.284832b56faa.js" type="text/javascript"></script>

    <title>Ebinder</title>



    




<link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/bootstrap/dist/css/bootstrap.5ba37ad91636.css" /> <!--modules/core/css/bootstrap-custom.38c7026bb3d0.css-->
<!-- bower:css -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/angular-ui-select/dist/select.195cf0cf9693.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/animate.css/animate.56848eb884e1.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/cropper/dist/cropper.e57b9d506002.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/ng-cropper/dist/ngCropper.all.4a4855d4ac35.css" />
<!-- endbower -->


<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/vendors/select2/css/select2.min.8969ac4614d6.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/vendors/perfect-scrollbar/perfect-scrollbar.min.7682fde59eb4.css">

<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/modules/core/css/base.4d00a8ae404e.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/modules/core/css/main.c3df550093b8.css">

<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/modules/inbox/css/inbox.96b1886386fa.css">
    
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/procurify/libs/modules/settings/css/settings.01caf03ee8de.css">
    
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/procurify/libs/modules/core/css/ebinder-custom-style.css">

</head>
<body>


    <div class="container">
		<div class="row" style="padding-top: 90px;">
        
        	<div class="col-sm-12 col-md-4 col-md-offset-4">
     
                    <p class="text-center">
                        <img src="<?php echo base_url(). 'assets/frontpage/corporate/images/crisisflo-logo-medium.png'?>" alt="logo" style="margin: 0 auto; margin: 15px;">
                    </p>
                  <form class="form-signin">
                    <div class="panel">
              
              
                        <div class="panel-body">
                 
                            <h2 class="form-signin-heading">Please sign in</h2>
                            <label for="inputEmail" class="sr-only">Email address</label>
                            <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus>
                            <label for="inputPassword" class="sr-only">Password</label>
                            <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>
                            <div class="checkbox">
                              <label>
                                <input type="checkbox" value="remember-me"> Remember me
                              </label>
                            </div>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    
                        </div>
                    </div>
              </form>
    
            </div>
        </div>

    </div> <!-- /container -->

<!-- SYSTEM SCRIPTS -->
<script   src="https://code.jquery.com/jquery-2.2.4.js"   integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="   crossorigin="anonymous"></script>

<!-- Latest bootstrap compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>


<!-- VENDOR SCRIPTS -->
<!-- <script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/select2/js/select2.min.97eccda64530.js"></script> -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/select2/js/select2.full.af05bb973f4d.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/slimscroll/slimscroll.min.550e27427204.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/perfect-scrollbar/perfect-scrollbar.jquery.min.b4e42d596127.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/placeholders.min.d07c9c7babb3.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/medium-editor.min.b8b1ba8cc843.js"></script>
<script src="https://js.pusher.com/3.0/pusher.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/js/ebinder-custom-script.js"></script>

</body>
</html>
