
  
  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
  
      <div class="row">
  
      
        <div class="col-lg-12">

      
      
        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>      
        <?php } ?>

        
        <div class="module_header">
            <i class="fa fa-user fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <ul class="module_actions right hidden">
                <li>
                    <a class="module_action btn--red" href="<?php echo base_url() ?>panel/claims/add">
                        <div class="module_action_icon">
                            <i class="fa fa-plus-square"></i>
                        </div>
                        <span class="module_action_text text-uppercase"> Add <?php echo $singular ?></span>
                    </a>
                </li>
            </ul>
   
        </div>


        <div class="panel">
        	<div class="panel-body">
           		
                <div class="row">
                	<div class="col-sm-3">
                    
                
                        <?php if(count($claims) > 0) { ?>
                
                        
                        
						<div class="table-responsivex">
                        
                        <table class="table table-striped table-hoverx table-datatablex">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <?php /*?><th>Claim</th>
                              <th>Details</th>
                              <th>Customer</th>
                              <th>Status</th><?php */?>
                            </tr>
                          </thead>
                          <tbody>                        
                        
                        <?php
							$count = 1;
							foreach($claims as $u=>$value){
								
						?>
                        
                            <tr>
                             
                              <td class="hidden"><?php echo $count; ?></td>
                            </tr>
                            <tr>
                            
                              <td>Claim: <?php echo $value['claim'] ?></td>
                            </tr>
                            <tr>
                              <td>Details: <?php echo $value['details'] ?></td>
                            </tr>
                            <tr>
                              <td>Customer: <?php echo $this->common->customer_name($value['customer_id']) ?></td>
                            </tr>
                            <tr>
                              <td>Status: <?php echo $this->common->claim_status($value['status']) ?></td>
                              
                            </tr>
                            <tr>
                              <td>
                                    <a href="<?php echo base_url().'panel/managequote/'.$value['policy_id']?>" class="btn btn--white btn--small show-biz-response btn-block">Insurance details</a>                              
                              </td>
                              
                            </tr>

                        <?php
								$count++;
							}
						?>


                          
                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->
                          
                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';	
						}?>                
                

                    
                    </div>
                
                	<div class="col-sm-9" style="border-left: 1px solid #eee">
                    
                    
                        
                        <h3>Messages</h3>
        
        
        
                        <!-- chat-area -->
                        <section class="chat-area">
                            
                            <div class="chat clearfix">
                            <?php if(count($messages) > 0) {
                                foreach($messages as $r=>$value) {
                                    
                                    if($user['id'] == $value['seeker_id']) {
                                ?>
        
                                <div class="chat__item">
                                    <div class="chat__item-wrap">
                                        <div class="avatar">
                                            <div class="avatar__img" data-toggle="tooltip" data-title="<?php echo $this->common->customer_name($value['seeker_id']); ?>">
                                                <img src="https://placeholdit.imgix.net/~text?txtsize=150&txt=<?php echo substr($this->common->customer_name($value['seeker_id']), 0, 1); ?>&w=200&h=200">
                                            </div>
                                        </div>
                                        <div class="chat__item-message-wrap">
                                            <div class="triangle"></div>
                                            <div class="chat__item-message">
                                                <?php
                                                    if($value['message'] != ''){
                                                        echo $value['message'];
                                                    }
                                                    else{
                                                        echo '<a href="'.base_url().'uploads/message-uploads/'.$value['file_upload_name'].'" target="_blank">'.substr($value['file_upload_name'], 13).'</a>';
                                                    }
                                                ?>
                                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat__date">
                                        <span><?php echo date_format(date_create($value['date_added']), 'd M Y, h:i:s A')?> </span> 
                                        <i class="fa fa-check-circle fa-fw text-success"></i>
        
                                    </div>
                                </div>
                                
                                <?php } else { ?>
                                <div class="chat__item right">
                                    <div class="chat__item-wrap">
                                        <div class="chat__item-message-wrap">
                                            <div class="triangle"></div>
                                            <div class="chat__item-message">
                                                <?php
                                                    if($value['message'] != ''){
                                                        echo $value['message'];
                                                    }
                                                    else{
                                                        echo '<a href="'.base_url().'uploads/message-uploads/'.$value['file_upload_name'].'" target="_blank">'.substr($value['file_upload_name'], 13).'</a>';
                                                    }
                                                ?>
                                            
                                            </div>
                                        </div>
                                        <div class="avatar">
                                            <div class="avatar__img" data-toggle="tooltip" data-title="<?php echo $this->common->customer_name($value['seeker_id']); ?>">
                                                <img src="https://placeholdit.imgix.net/~text?txtsize=150&txt=<?php echo substr($this->common->customer_name($value['seeker_id']), 0, 1); ?>&w=200&h=200">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat__date pull-right">
                                        <span><?php echo date_format(date_create($value['date_added']), 'd M Y, h:i:s A')?> </span> 
                                        <i class="fa fa-check-circle fa-fw text-success"></i>
                                    </div>
                                </div>
        
                                
                                
                            <?php } } } ?>
        
                            </div>
        
                            <div class="chat-area__bottom">
                                <?php /*?><a class="left fa fa-paperclip fa-2x text-muted fa-fw"></a><?php */?>
                                <input class="textfield" type="" name="chat" placeholder="Type something...">
                                <button class="right claim-chat-send" data-id="<?php echo $user['id']?>" data-letter="<?php echo substr($user['first_name'], 0, 1) ?>" data-message_id = "<?php echo $message_id ?>" data-to = "<?php echo $to ?>">
                                    <i class="fa fa-send fa-2x text-primary"></i>
                                </button>
                            </div>
                        </section>
                        <!-- chat-area -->
                        
                        
                    </div><!-- col-->
                
                </div><!-- row -->
                
                

            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->
    