
<!-- VIEWDEP -->





<!DOCTYPE html>
<!-- <html manifest="/public/manifest.appcache"> -->
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
    <meta name="format-detection" content="telephone=no">

    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/procurify/libs/images/favicon.3f215dbd8fc1.ico" type="image/x-icon" />
    <link rel="icon" href="<?php echo base_url() ?>assets/procurify/libs/images/favicon.3f215dbd8fc1.ico" type="image/x-icon" />

    <script src="<?php echo base_url() ?>assets/procurify/libs/vendors/modernizer/modernizer.284832b56faa.js" type="text/javascript"></script>

    <title>Ebinder</title>



    




<link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/bootstrap/dist/css/bootstrap.5ba37ad91636.css" /> <!--modules/core/css/bootstrap-custom.38c7026bb3d0.css-->
<!-- bower:css -->
<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/angular-ui-select/dist/select.195cf0cf9693.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/animate.css/animate.56848eb884e1.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/cropper/dist/cropper.e57b9d506002.css" />
<link rel="stylesheet" href="<?php echo base_url() ?>assets/procurify/libs/ng-cropper/dist/ngCropper.all.4a4855d4ac35.css" />
<!-- endbower -->


<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/vendors/select2/css/select2.min.8969ac4614d6.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/vendors/perfect-scrollbar/perfect-scrollbar.min.7682fde59eb4.css">

<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/modules/core/css/base.4d00a8ae404e.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/modules/core/css/main.c3df550093b8.css">

<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/procurify/libs/modules/inbox/css/inbox.96b1886386fa.css">
    
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/procurify/libs/modules/settings/css/settings.01caf03ee8de.css">

<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/procurify/libs/modules/core/css/ebinder-custom-style.css">

</head>
<body>

    <!-- HEADER -->
    

<header class="app-header">
    <a class="header-logo" href="<?php echo base_url() ?>">
        <img src="<?php echo base_url() ?>assets/procurify/images/logo/logo-horizontal-64.7a505e99594f.png"/>
    </a>

    <div class="header-content">

        <!--<a class="btn__shortcut-add untouchable" role="button" tabindex="0">
            <span class="btn__shortcut-add__icon">+</span>
        </a>-->

        
            
    
        <!--<a href="javascript:;" class="btn btn--header btn--orange btn--trial">
            <div class="btn--trial_days">14</div>
            <div class="btn--trial_text">
                <p class="small">days remaining</p>
                
                
                <p class="text-bold">Enter Payment</p>
                
                
            </div>
        </a>-->
    

        


        <!--<a class="btn btn--plain btn--hollow btn--header btn--help hide"
            id="Intercom"
            href="">
            <i class="icon-message"></i><span class="caption">Chat with us</span>
        </a>-->

        <a class="header-section help-btn-js text-color-white" role="button" tabindex="0">
            <i class="header-section-icon fa fa-question-circle fa-fw"></i>
        </a>

        <!-- <div class="help-btn  ml10"><i class="fa fa-question"></i></div> -->

        

        <a class="header-section header-notification-btn-js text-color-white"
            notification-popup
            role="button"
            tabindex="0"
            id="header-notifification-app">
            <i class="header-section-icon fa fa-bell-o fa-fw"></i>
            <span class="header-section-badge ng-cloak">1</span>
        </a>

        <label class="header-profile" for="header-profile">
            <div class="header-profile-image image image--40px inline-block"
                 style="background-image: url(<?php echo $user['img'] ?>)"></div>
            <p class="header-profile-name"><?php echo $user['full_name'] ?> </p>

            <i class="fa fa-chevron-down"></i>

            <input type="checkbox" id="header-profile" class="hide"/>

            <ul class="dropdown-menu">
                <li><a href="/accounts/profile/">Account</a></li>
                <li><a href="/accounts/logout/">Logout</a></li>
            </ul>
        </label>
    </div>

</header>


    <!-- NAVIGATION -->
    <div class="l-nav-container clearfix">
    <nav class="nav ">
        
        <div class="nav-parent  " >

            
                <a class="nav-parent-a " href="/">
                    <div class="nav-icon">
                        <div class="nav-icon-tooltip" data-toggle="tooltip" title="Dashboard" data-placement="right"></div>
                        
                            <i class="fa fa-globe fa-fw"></i>
                        
                    </div>
                    <span class="nav-label">Dashboard</span>
                    <div class="nav-badge nav-badge-dashboard"></div>
                </a>
            

            

            

        </div>
        
        <div class="nav-parent  has-child" >

            
                <input class="hide" type="checkbox" id="nav-child-2"  />

                <label class="nav-parent-a nav--has-child" for="nav-child-2">
                    <div class="nav-icon">
                        
                            <i class="fa fa-user fa-fw"></i>
                        
                    </div>
                    <span class="nav-label">Customers</span>
                    <div class="nav-badge nav-badge-request"></div>
                </label>
            

            
            <ul class="nav-child nav-child-request">
                
                
                <li class="nav-subheader nav-subheader--collapsed-only">
                    <span>Customers</span>
                </li>
                
                
                
                <li>
                    <a class="" href="/status/orders/">
                        <span class="nav-label">All Customers</span>
                        <div class="nav-badge nav-badge-request-order"></div>
                    </a>
                </li>
                
                
                
                <li>
                    <a class="" href="/status/travel/">
                        <span class="nav-label">Add New</span>
                        <div class="nav-badge nav-badge-request-travel"></div>
                    </a>
                </li>
                
                
            </ul>
            

            

        </div>
        
        
        <div class="nav-parent  " >
                <a class="nav-parent-a" href="/settings/">
                    <div class="nav-icon">
                        <div class="nav-icon-tooltip" data-toggle="tooltip" title="Questionnaire" data-placement="right"></div>
                        
                            <i class="fa fa-file-text fa-fw"></i>
                        
                    </div>
                    <span class="nav-label">Questionnaire</span>
                    <div class="nav-badge nav-badge-settings"></div>
                </a>

        </div>
        
        <div class="nav-parent  " >
                <a class="nav-parent-a active" href="/settings/">
                    <div class="nav-icon">
                        <div class="nav-icon-tooltip" data-toggle="tooltip" title="Settings" data-placement="right"></div>
                        
                            <i class="fa fa-cog"></i>
                        
                    </div>
                    <span class="nav-label">Settings</span>
                    <div class="nav-badge nav-badge-settings"></div>
                </a>

        </div>
        
        
        <div class="nav-mask"></div>
        <div class="subnav"></div>

    </nav>


</div>


    <!-- CONTENT -->
    <div class="l-app-container l-content-container">
        
<div>
	<div notifications></div>

    <!--==================================*\
    # Templates goest here - html
    \*===================================-->
	<div ui-view>
        <div class="route-loader hidden"></div>
        
        <div class="panel">
        	<div class="panel-body">
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            	sdf<br><br>
            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->
    



<!--<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/libs.min.432c9e1fffd8.js"></script>
-->

<!-- SYSTEM SCRIPTS -->
<script   src="https://code.jquery.com/jquery-2.2.4.js"   integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="   crossorigin="anonymous"></script>

<!-- Latest bootstrap compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>


<!-- VENDOR SCRIPTS -->
<!-- <script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/select2/js/select2.min.97eccda64530.js"></script> -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/select2/js/select2.full.af05bb973f4d.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/slimscroll/slimscroll.min.550e27427204.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/perfect-scrollbar/perfect-scrollbar.jquery.min.b4e42d596127.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/placeholders.min.d07c9c7babb3.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/libs/vendors/medium-editor.min.b8b1ba8cc843.js"></script>
<script src="https://js.pusher.com/3.0/pusher.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/js/ebinder-custom-script.js"></script>

</body>
</html>
