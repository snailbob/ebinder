

  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">

      <div class="row">


        <div class="col-lg-12">



        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
        <div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
            <?php echo $success.$danger.$info ?>
        </div>
        <?php } ?>


        <div class="module_header">
            <i class="fa fa-user fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <ul class="module_actions right hidden">
                <li>
                    <a class="module_action btn--red" href="<?php echo base_url() ?>panel/quote">
                        <div class="module_action_icon">
                            <i class="fa fa-user-plus"></i>
                        </div>
                        <span class="module_action_text text-uppercase"> Add <?php echo $singular ?></span>
                    </a>
                </li>
            </ul>
        </div>


     <div class="panel panel-default">


        <div class="panel-heading">
              <a href="#" class="btn btn-primary pull-right new_quote_resetx" data-toggle="modal" data-target="#newQuoteProductsModal"><i class="fa fa-plus"></i> Quote<?php // echo $singular_title; ?></a>

              <div class="dropdown pull-right" style="margin-right: 5px;">
                <button class="btn btn-default" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Filter
                  <span class="caret"></span>
                </button>
                <ul class="dropdown-menu policy_filter_btn" aria-labelledby="dLabel">
                  <li class="active">
                    <a href="#" data-value="">All</a>
                  </li>
                  <li>
                    <a href="#" data-value="Current">Current</a>
                  </li>
                  <li>
                    <a href="#" data-value="Expired">Expired</a>
                  </li>
                  <li>
                    <a href="#" data-value="Referred">Referred</a>
                  </li>
                  <li>
                    <a href="#" data-value="Rejected">Rejected</a>
                  </li>
                </ul>
              </div>



            <div  class="panel-title">
                  <h4>Manage <?php echo $title; ?></h4>
            </div>
          </div>



            <div class="panel-bodyx">

                        <?php if(count($biz_referral) > 0) { ?>


                          <div class="table_search">
                              <input type="text" class="form-control input_table_search" placeholder="search for..." />
                              <i class="fa fa-search"></i>
                          </div>

                        <div class="table-responsivex">

                        <table class="table table-striped table-hover table-datatable table-policies">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <th>Quote ID</th>
                              <th>Organisation Name</th>
                              <th>Product Name</th>
                              <th>Status</th>
                              <th>Quote Expiry</th>
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>

                        <?php
							$count = 1;
							foreach($biz_referral as $u=>$value){
						?>



                            <tr>

                              <td class="hidden"><?php echo $count; ?></td>
                              <td><a href="<?php echo base_url().'panel/managequote/'.$value['id']?>" data-id='<?php echo $value['id'];?>'><?php echo $value['quote_id']; ?></a></td>

                              <td><?php echo $value['customer']['name'] ?></td>

                              <td>
                                <?php
                                $prod_iddd = (isset($value['content']['homebiz1']['1_product_id'])) ? $value['content']['homebiz1']['1_product_id'] : '';
                                echo $this->common->product_name($prod_iddd); ?>
                              </td>

                              <td><?php echo ($value['unbind_quote_status'] == 'Referred') ? '<span class="text-red">Referred</span>' : $value['unbind_quote_status']; ?></td>

                              <td><?php echo $value['exp_timeto_display'] ?></td>


                              <td>

                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">

                                        <li><a href="<?php echo base_url().'panel/managequote/'.$value['id']?>" data-id='<?php echo $value['id'];?>'>Insurance details</a></li>

                                        <?php if($value['bound'] != 'Y') {?>
                                        <li><a href="javascript:;" class="biz-bind-now" data-id='<?php echo $value['id'];?>' data-type="bind">Bind</a></li>
                                        <?php } ?>
                                        <li><a href="javascript:;" class="renew-policy" data-id='<?php echo $value['id'];?>' data-action="email">Email quote</a></li>
                                        <li><a href="javascript:;" class="delete_btn" data-id="<?php echo $value['id']?>" data-table="biz_referral">Delete quote</a></li>
                                      </ul>
                                    </div>


                              </td>
                            </tr>

                        <?php
                                $count++;
                            }
                        ?>



                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php } else{
                            echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';
                        }?>


            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->






    <!-- Modal -->
    <div class="modal fade" id="newQuoteProductsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Select product</h4>
          </div>

          <div class="modal-body">
            <div class="list-group">
              <?php
                foreach($q_products as $u_q_product=>$user_q_product){
              ?>

              <a href="#" class="list-group-item new_quote_reset" data-id="<?php echo $user_q_product['id']?>">
                <i class="fa fa-line-chart fa-fw"></i> <?php echo $user_q_product['product_name'] ?>
              </a>

              <?php } ?>
            </div>

          </div>

        </div>
      </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="historyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">History</h4>
          </div>

          <div class="modal-body">
              <div class="history-content">
              </div>
          </div>
          <div class="modal-footer">
            <a href="#" class="btn btn-cancel btn-primary" data-dismiss="modal">OK</a>
          </div>

        </div>
      </div>
    </div>





    <!-- Modal -->
    <div class="modal fade" id="myReferralModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Referral Required</h4>
          </div>
          <div class="modal-body " style="padding: 30px;">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

            <form id="biz_referral_form" novalidate="novalidate">
                <div class="hidden">

                                  <div class="row">
                                      <div class="col-sm-6">
                                          <div class="form-group">
                                              <?php
                                              $thequest = 'Contact Name';
                                              $thequest2 = url_title($thequest, 'underscore', TRUE);
                                               ?>
                                              <label><?php echo $thequest; ?></label>
                                          </div>
                                      </div>
                                      <div class="col-sm-6">
                                          <div class="form-group">

                                            <input type="text" name="5_<?php echo url_title($thequest2, 'underscore', TRUE) ?>" class="form-control" placeholder="" value="<?php echo (isset($homebiz5['5_'.$thequest2])) ? $homebiz5['5_'.$thequest2] : ''?>">
                                          </div>
                                      </div>
                                  </div>


                                  <div class="row">
                                      <div class="col-sm-6">
                                          <div class="form-group">
                                              <?php
                                              $thequest = 'Contact Number';
                                              $thequest2 = url_title($thequest, 'underscore', TRUE);
                                               ?>
                                              <label><?php echo $thequest; ?></label>
                                          </div>
                                      </div>
                                      <div class="col-sm-6">
                                          <div class="form-group">

                                            <input type="text" name="5_<?php echo url_title($thequest2, 'underscore', TRUE) ?>" class="form-control" placeholder="" value="<?php echo (isset($homebiz5['5_'.$thequest2])) ? $homebiz5['5_'.$thequest2] : ''?>">
                                          </div>
                                      </div>
                                  </div>

                                  <div class="row">
                                      <div class="col-sm-6">
                                          <div class="form-group">
                                              <?php
                                              $thequest = 'Email Address';
                                              $thequest2 = url_title($thequest, 'underscore', TRUE);
                                               ?>
                                              <label><?php echo $thequest; ?></label>
                                          </div>
                                      </div>
                                      <div class="col-sm-6">
                                          <div class="form-group">

                                            <input type="email" name="5_<?php echo url_title($thequest2, 'underscore', TRUE) ?>" class="form-control" placeholder="" value="<?php echo (isset($homebiz5['5_'.$thequest2])) ? $homebiz5['5_'.$thequest2] : ''?>">
                                          </div>
                                      </div>
                                  </div>


                </div>

                <div class="text-right">
                    <button type="button" class="btn btn-cancel btn-grey" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>

            </form>
          </div>

        </div>
      </div>
    </div>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">

            <div class="close-modal" <?php echo ($this->uri->segment(2) == 'eproducts' || $this->uri->segment(2) == 'update_quote_info' || $this->uri->segment(2) == 'brokerform') ? ' onclick="window.close()"' : ' data-dismiss="modal"' ?>>
              <div class="lr">
                  <div class="rl">
                  </div>
              </div>
            </div>




            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">


                          	<div class="row" style="margin-left: 15px; margin-right: 0">
                            	<div class="col-lg-9 col-lg-offset-3 col-md-8 col-md-offset-4">

                              	<p class="pull-right <?php echo ($this->uri->segment(1) == 'webmanager') ? '' : 'hidden' ?>" style="padding-top: 20px;">
                                      <a href="javascript:;" class="btn show_history_btn" data-type="preview_form">
                                        <i class="fa fa-history"></i> History
                                      </a>

                                      <a href="<?php echo ($this->uri->segment(4) == 'edit') ? base_url().'webmanager/eproducts/preview?tab='.$thepage.'#quote' : base_url().'webmanager/eproducts/preview/edit?tab='.$thepage.'#quote' ?>" class="btn btn-link">
                                          <?php echo ($this->uri->segment(4) == 'edit') ? 'Back to Preview Mode' : '<i class="fa fa-edit"></i> Edit' ?>
                                      </a>



                                </p>
                                <h1><?php echo $singular_title; ?></h1>
                                <hr />
                              </div>
                            </div>



<div class="row">


    <div class="col-lg-12">



  		<div class="containerx">

        	<div class="row">



            	<div class="col-lg-9 col-lg-offset-3 col-md-8 col-md-offset-4">


					<?php
                        $the_contact = (isset($user_sess['agency']['broker_id'])) ? $user_sess['agency']['broker_id'] : '';
						if(!empty($the_contact)){ ?>

                	<div class="alert alert-info">
                    	Assigned underwriter is <?php echo $this->common->customer_name($the_contact) ?>.
                        <a href="<?php echo base_url().'panel/message/'.$the_contact ?>" class="btn btn-default btn--small">Send message</a>

                    </div>
					<?php } ?>




                    <form id="quote_form" novalidate="novalidate" class="text-left">
                        <input type="hidden" name="user_id" value="<?php echo (isset($user_sess['info_format']['customer_id'])) ? $user_sess['info_format']['customer_id'] : $user['id'] ?>" />
                        <input type="hidden" name="customer_type" value="<?php echo $user['customer_type']?>" />

                        <div class="panel panel-white">
                            <div class="panel-body">
                                <?php
                  								$questionnaire = (isset($questionnaire)) ? $questionnaire : array();
                  								$this->load->view($quote_view, $questionnaire);
                                ?>
                            </div>   <!--.panel-body-->




                            <div class="panel-body text-right">



                            <?php if($this->session->userdata('logged_admin') != 'admin' && $this->uri->segment(1) != 'webmanager') {?>
                                <a class="btn btn-default pull-left btn-grey btn-cancel <?php echo ($thepage == '1' || $thepage == '0') ? 'hidden' : '' ?>" href="<?php echo base_url().'panel/quote?tab='.$theindex.'#quote'?>">Back</a>

                                <a class="btn btn-default pull-left btn-grey btn-cancel <?php echo ($thepage != '1' && $thepage != '0' && $thepage != '') ? 'hidden' : '' ?>" href="#"  <?php echo ($this->uri->segment(2) == 'eproducts' || $this->uri->segment(2) == 'update_quote_info' || $this->uri->segment(2) == 'brokerform') ? ' onclick="window.close()"' : ' data-dismiss="modal"' ?>>Cancel</a>


                                <?php if($thepage != '5'){ ?>
                                  <button class="btn btn-primary" type="submit"><?php echo ($thepage == '7') ? 'Done' : 'Continue' ?></button>
                                <?php }  ?>

                            <?php } else {?>
                                <a class="btn btn-default pull-left btn-grey btn-cancel <?php echo ($thepage == '1') ? 'hidden' : '' ?>" href="<?php echo base_url().'webmanager/eproducts/preview?tab='.$theindex.'#quote'?>">Back</a>

                                <a class="btn btn-default btn-grey btn-cancel <?php echo ($thepage != '1' && $thepage != '') ? 'hidden' : '' ?>" href="#" <?php echo ($this->uri->segment(2) == 'eproducts' || $this->uri->segment(2) == 'update_quote_info' || $this->uri->segment(2) == 'brokerform') ? ' onclick="window.close()"' : ' data-dismiss="modal"' ?>>Cancel</a>


                                <a class="btn btn-primary <?php echo ($thepage == '7') ? 'hidden' : '' ?>" href="<?php echo base_url().'webmanager/eproducts/preview?tab='.($thepage + 1).'#quote' ?>">Next</a>
                            <?php }?>


                            </div>   <!--.panel-body-->



                        </div><!--.panel-->

          			</form>

                </div>
            </div>
        </div>

    </div>
</div>




	<div class="the-sidebars">

        <div class="container-fluid hidden-sm hidden-xs">
            <div class="row">

        	<div class="col-sm-5 col-md-3 col-lg-2 leftbar-still">

                <nav>
                <div class="lifecycle">

                    <ul class="list">


                        <?php
                            $menuLength = count($themenu) - 1;
                            $menucount = ($this->uri->segment(2) == 'quote') ? 0 : 1;
                            foreach($themenu as $r=>$value){

								if($this->session->userdata('logged_admin') != 'admin' && $this->uri->segment(1) != 'webmanager') {
								?>

                        <li class="item <?php echo ($thepage == ($menucount) || !empty($value['data'])) ? ' current' : ''; echo ($r == 0) ? ' start' : ''; echo ($r == $menuLength) ? ' end' : ''; echo (!empty($value['data'])) ? ' complete' : '' ?>">
                            <?php echo (!empty($value['data']) || $thepage == ($menucount)) ? '<a class="" href="?tab='.($menucount).'#quote">' : '<div>'; ?>
                                <div class="indicator">
                                    <div class="semiline"></div>
                                    <div class="semiline"></div>
                                    <div class="circle wow animated zoomIn">
                                      <?php echo (!empty($value['data']) && $thepage != ($menucount)) ? '<i class="fa fa-check text-white"></i>' : '' ?>
                                    </div>
                                </div><span><?php echo $value['name']?></span>
                            <?php echo (!empty($value['data']) || $thepage == ($menucount)) ? '</a>' : '</div>'; ?>
                        </li>

                            <?php
								}
								else{ ?>


                        <li class="item <?php echo ($thepage == ($menucount)) ? ' current' : ''; echo ($r == 0) ? ' start' : ''; echo ($r == 6) ? ' end' : ''; echo ' complete' ?>">
                            <?php echo '<a class="" href="?tab='.($menucount).'#quote">'; ?>
                                <div class="indicator">
                                    <div class="semiline"></div>
                                    <div class="semiline"></div>
                                    <div class="circle wow animated zoomIn"></div>
                                </div><span><?php echo $value['name']?></span>
                            <?php echo '</a>'; ?>
                        </li>

                            <?php

								}
                              $menucount++;
                            }
                        ?>


                    </ul>
                </div>

                </nav>
            </div><!--.col-sm-5-->

            </div>
        </div>



    </div>
