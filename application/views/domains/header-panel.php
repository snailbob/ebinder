
<!-- VIEWDEP -->





<!DOCTYPE html>
<!-- <html manifest="/public/manifest.appcache"> -->
<html>
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
    <meta name="format-detection" content="telephone=no">

    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/procurify/libs/images/favicon.3f215dbd8fc1.ico" type="image/x-icon" />
    <link rel="icon" href="<?php echo base_url() ?>assets/frontpage/images/favicon.ico" type="image/x-icon" />

<!--     <script src="<?php echo base_url() ?>assets/procurify/libs/vendors/modernizer/modernizer.284832b56faa.js" type="text/javascript"></script>
 -->

    <?php
      $topheadtitle = $this->master->getRecords('admin_contents', array('type'=>'topheadtitle'));

    ?>
    <title><?php echo $topheadtitle[0]['title']?></title>



<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/css/summernote.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/css/summernote-bs3.css" type="text/css">


<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-switch/css/bootstrap3/bootstrap-switch.css" type="text/css">



<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css">


<link href="<?php echo base_url()?>assets/frontpage/corporate/css/bootstrap.css" rel="stylesheet">

<!-- Plugin CSS -->
<?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css" type="text/css"><?php */?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/dataTables.bootstrap.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/datatables.css" type="text/css">

<!-- Plugin CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/dist/cropper.min.css" type="text/css">
<?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/crop-avatar.css" type="text/css"><?php */?>


<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/gridstack/gridstack.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bs-switch/css/bootstrap3/bootstrap-switch.min.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/silviomoreto-bootstrap-select/css/bootstrap-select.min.css" type="text/css">


<link href="<?php echo base_url()?>assets/frontpage/corporate/css/main.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" type="text/css">


<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,500,300,100' rel='stylesheet' type='text/css'>

<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700' rel='stylesheet' type='text/css'>

<link href="<?php echo base_url()?>assets/frontpage/corporate/css/animate.min.css" rel="stylesheet">
<link rel="shortcut icon" href="<?php echo base_url()?>assets/frontpage/images/favicon.ico" type="image/x-icon" />
<link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url()?>assets/frontpage/corporate/css/jquery.mmenu.css" />

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/admin-style.css" type="text/css">

<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/libs/head.min.js"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/us/variablesCorporate"></script>
<script>
  var base_url = '<?php echo base_url() ?>';
  var base_url2 = '<?php echo base_url() ?>';
  var uri_1 = '<?php echo $this->uri->segment(1) ?>';
  var uri_2 = '<?php echo $this->uri->segment(2) ?>';
  var uri_3 = '<?php echo $this->uri->segment(3) ?>';
  var uri_4 = '<?php echo $this->uri->segment(4) ?>';
  var user_name = '<?php echo $this->session->userdata('name') ?>';
  var user_location = '<?php echo $this->session->userdata('location') ?>';
  var user_address = '<?php echo $this->session->userdata('address') ?>';
  var user_type = '<?php echo $this->session->userdata('type') ?>';
  var stripe_id = '<?php echo $this->session->userdata('stripe_id') ?>';
</script>



</head>
<body>


    <?php
        //vars
        $user_session = $this->session->all_userdata();
        $user_session['sidenav_settings'] = (isset($user_session['sidenav_settings'])) ? $user_session['sidenav_settings'] : 'expanded';
        $unique_link = (isset($user_session['agency']['agency_details']['unique_link'])) ? isset($user_session['agency']['agency_details']['unique_link']) : 'N';


        $colors = $this->common->colors();
        $usertype = $this->session->userdata('type');
        $opposite_type = 'agency';
        if($usertype != 'customer'){
            $opposite_type = 'customer';
        }
    ?>



    <div id="browser-detection" class="alert alert-warning" style="display:none; position:relative;">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        Your browser is not supported by Transit Insurance. For a better experience, use one of our <a class='preventDefault' data-toggle='modal' data-target='#modal-browser-options'>supported browsers.</a>
    </div>
    <div id="cookies-detection" class="alert alert-danger" style="display:none; position:relative;">
        *__browser_cookies_disabled*
    </div>
    <nav id="my-menu">
       <ul>
          <li class="hidden"><a href="<?php echo base_url()?>">Home</a></li>
          <?php /*?><li><a href="<?php echo base_url()?>about">About Us</a></li>
          <li><a href="<?php echo base_url()?>contact">Contact</a></li><?php */?>
       </ul>
    </nav>

    <div id="main-container" style="background: #efefef">
    <div class="mobile-spacer visible-xs hidden-sm hidden-md hidden-lg" data-0="margin-top:120px" data-1="margin-top:60px"></div>

      <!-- Static navbar -->
      <div class="navbar navbar-default navbar-static-top quote_hidden <?php echo ($this->uri->segment('2') == 'brokerform' || $this->uri->segment('2') == 'update_quote_info' || $this->uri->segment('2') == 'quote' || $this->uri->segment('2') == 'use_customer' || $this->uri->segment('2') == 'eproducts') ? 'hidden' : ''; ?>" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
              <button type="button" id="open-sliderx" class="text-info pull-right navbar-toggle nav-togglex" style="background: transparent; border: none">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-cog fa-2x"></i>
              </button>
              <div class="navbar-brand">
                <a href="<?php echo base_url().'panel'; ?>"><img id="nav-logo" class="animated fadeIn animated-top" src="<?php echo $this->common->logo($this->session->userdata('id'))?>" style="margin-top: 5px; max-height: 61px"/></a>
                <div class="mobile-menu visible-xs">

                    <?php /*?><?php if($this->session->userdata('logged_admin') != '') { ?>
                    <a href="<?php echo base_url()."dashboard/logout"; ?>" id="btn-mini-login" class="btn btn-primary btn-rounded btn-outline">Log Out</a>
                    <?php } ?>  <?php */?>



                </div>
              </div>
            </div>


              <ul class="nav navbar-nav navbar-right navbar-admin hidden-xs  <?php echo ($this->session->userdata('id') == '') ? 'hidden' : ''; ?>">
                <li>
                    <a href="#" class="nav-toggle nav-toggle-admin">
                        <i class="fa fa-cog fa-2x pull-right"></i>

                        <p>


                            <span class="img-hover pull-left">
                              <img class="img-circle img-page-preview" src="<?php echo $this->common->avatar($this->session->userdata('id'), 'customer') ?>" width="40" height="40" />
                              <span class="text-content"><span>Update</span></span>
                            </span>

                              <strong><?php echo $this->session->userdata('name')?></strong>
                              <span><?php echo $this->session->userdata('email')?></span>

                        </p>

                    </a>
                </li>


              </ul>

            <div class="navbar-collapse collapse hidden">


                <div class="mobile-menu text-right" style="padding-top: 35px;">
                    <?php if($this->session->userdata('id') == '') { ?>
                      <a href="<?php echo base_url()."webmanager"; ?>" id="btn-mini-login" class="btn btn-primary btn-xs btn-rounded btn-outline" >Log In</a>
                    <?php } ?>


                </div>
                <?php if($this->session->userdata('logged_admin') == '') { ?>
                <div class="hidden-xs"><a href="#<?php // echo base_url()."signin"; ?>" class="pull-right btn btn-primary btn-rounded btn-outline login_btn" style="margin-top: -6px;" data-type="Agency">Log In</a></div>
                <?php } ?>



            </div><!--/.nav-collapse -->
        </div> <!-- /container -->
      </div>





      <div class="container-fluid section-simple section-simple-admin main_container main_container_admin quote_hidden <?php echo ($this->uri->segment('2') == 'brokerform' || $this->uri->segment('2') == 'update_quote_info' || $this->uri->segment('2') == 'quote' || $this->uri->segment('2') == 'use_customer' || $this->uri->segment('2') == 'eproducts') ? 'hidden' : ''; ?>" style="padding-top: 120px">
    <div class="container-fluid section-simple section-simple-admin main_container main_container_admin quote_hidden <?php echo ($this->uri->segment('2') == 'update_quote_info' || $this->uri->segment('2') == 'quote' || $this->uri->segment('2') == 'use_customer' || $this->uri->segment('2') == 'eproducts') ? 'hidden' : ''; ?>" style="padding-top: 0px">



<?php


    $prod_user_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];
    $prod_agency_info = $this->master->getRecords('agency', array('id'=>$prod_user_id));

    $prod_user = $this->common->customer_format($this->session->userdata('id'));


    $prod_agency_info_format = array();
    if(!empty($prod_agency_info)){
      $ggg = array();
      foreach($prod_agency_info as $r=>$value){
        $value['agency_details'] = (@unserialize($value['agency_details'])) ? unserialize($value['agency_details']) : array();
        $ggg = $value;
      }
      $prod_agency_info_format = $ggg;
    }

    $prod_prod_id = (@unserialize($prod_user['privileges'])) ? unserialize($prod_user['privileges']) : array('3');

    //$prod_prod_id = (isset($prod_agency_info_format['agency_details']['product_id'])) ? $prod_agency_info_format['agency_details']['product_id'] : array();


    $prod_q_products = $this->master->getRecords('q_products');

?>




  <div class="the_main_container <?php echo ($user_session['sidenav_settings'] == 'collapsed') ? 'side-collapsed' : ''?>">
        <nav id="sidebar-wrapper" class="sidebar-wrapper-admin">

            <div class="list-group">




              <a href="#" data-toggle="modal" data-target="#myProfileModal" class="list-group-item" onclick="$('.nav-toggle').click();">
                <i class="fa fa-user fa-fw"></i> My Profile
              </a>

              <a href="<?php echo ($this->common->the_domain() == '') ? base_url().'settings/social_login' : 'https://ebinder.com.au/settings/social_login' ?>" class="list-group-item" onclick="$('.nav-toggle').click();">
                <i class="fa fa-google-plus-square fa-fw"></i> Social Login
              </a>

              <a href="<?php echo $this->common->bbase_url().'panel/password' ?>" class="list-group-item" onclick="$('.nav-toggle').click();">
                <i class="fa fa-key fa-fw"></i> Change Password
              </a>

              <a href="<?php echo $this->common->bbase_url().'panel/customers/all'; ?>" class="list-group-item" onclick="$('.nav-toggle').click();">
                <i class="fa fa-users fa-fw"></i> Manage Users
              </a>

              <a href="#" data-toggle="modal" data-target="#settingsBrokerageModal" class="list-group-item" onclick="$('.nav-toggle').click();">
                <i class="fa fa-file-text-o fa-fw"></i> Brokerage Profile
              </a>

              <a href="<?php echo $this->common->bbase_url().'panel/userlog'; ?>" class="list-group-item" onclick="$('.nav-toggle').click();">
                <i class="fa fa-file-text fa-fw"></i> Activity Log
              </a>

              <a href="<?php echo $this->common->bbase_url().'panel/logout'; ?>" class="list-group-item" onclick="$('.nav-toggle').click();">
                <i class="fa fa-sign-out fa-fw"></i> Logout
              </a>
            </div>


        </nav>





        <div class="row <?php echo ($this->session->userdata('id') == '') ? 'hidden' : ''; ?>">

            <div class="col-lg-2 col-md-3 col-sm-4 sidebar_nav">

                <div class="just-padding hidden-xs ">
                    <div class="list-group list-group-root" id="sideAccordion">

                        <a href="<?php echo $this->common->bbase_url().'panel'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(2) == '') { echo 'active'; }?>">
                          <i class="fa fa-dashboard fa-fw"></i> <span>Dashboard</span>
                        </a>


                        <a href="<?php echo $this->common->bbase_url().'panel/use_customer'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(2) == 'use_customer') { echo 'active'; }?>">
                          <i class="fa fa-users fa-fw"></i> <span>Manage Customers</span>
                        </a>
                        <!-- <a href="<?php echo $this->common->bbase_url().'panel/managequote'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(2) == 'managequote' || $this->uri->segment(2) == 'quote') { echo 'active'; }?>">
                          <i class="fa fa-file-text fa-fw"></i> Manage Quote
                        </a> -->

                        <a href="<?php echo $this->common->bbase_url().'panel/quote'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(2) == 'quote') { echo 'active'; }?>">
                          <i class="fa fa-cart-plus fa-fw"></i> <span>Manage Quotes</span>
                        </a>


                        <?php /*
                        <a href="<?php echo $this->common->bbase_url().'panel/transactions'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(2) == 'transactions') { echo 'active'; }?>">
                          <i class="fa fa-file-text-o fa-fw"></i> Manage Transactions
                        </a>
                        */ ?>

                        <a href="<?php echo $this->common->bbase_url().'panel/managepolicies'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(2) == 'managepolicies') { echo 'active'; }?>">
                          <i class="fa fa-file-text-o fa-fw"></i> <span>Manage Policies</span>
                        </a>
                        <a href="<?php echo $this->common->bbase_url().'panel/claims'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(2) == 'claims') { echo 'active'; }?>">
                          <i class="fa fa-list-alt fa-fw"></i> <span>Manage Claims</span>
                        </a>

                        <a href="<?php echo $this->common->bbase_url().'panel/reporting'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(2) == 'reporting') { echo 'active'; }?>">
                          <i class="fa fa-file-text-o fa-fw"></i> <span>Reporting</span>
                        </a>

                        <?php if(isset($user_sess['brokerage']['enable_whitelabel']) && $unique_link == 'Y') { ?>
                        <a href="<?php echo $this->common->bbase_url().'panel/share'; ?>" class="ripplelink list-group-item <?php if($this->uri->segment(2) == 'share') { echo 'active'; }?>">
                          <i class="fa fa-share-square-o fa-fw"></i> <span>Quote Link</span>
                        </a>
                        <?php } ?>
                        <a href="#" class="ripplelink list-group-item toggle_sidenav_expand">
                          <i class="fa <?php echo ($user_session['sidenav_settings'] == 'collapsed') ? 'fa-caret-right' : 'fa-caret-left'?> fa-fw"></i> <span>Toggle Menu</span>
                        </a>

                    </div>
                </div>





            </div><!--end sidebar_nav-->


        </div>

        <div class="row row_the_content">

            <div class="col-lg-2 col-md-3 col-sm-4">


            </div>
