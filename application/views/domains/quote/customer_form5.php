
<div class="form-group">
    <label>Claims</label>
</div>


                        <?php
                        $customer_idd = $this->session->userdata('customer_details');
                        $customer_idd = (isset($customer_idd['id'])) ? $customer_idd['id'] : '';

                        $claims = $this->master->getRecords('claims', array('customer_id'=>$customer_idd));


                         if(count($claims) > 0) { ?>

                        <div class="table_search">
                            <input type="text" class="form-control input_table_search_modal" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>


						<div class="table-responsivex">

                        <table class="table table-striped table-hover table-datatable-modal">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <th>Claim</th>
                              <th>Details</th>
                              <th>Customer</th>
                              <th>Status</th>
                            </tr>
                          </thead>
                          <tbody>

                        <?php
							$count = 1;
							foreach($claims as $u=>$value){

						?>

                            <tr>

                              <td class="hidden"><?php echo $count; ?></td>

                              <td><?php echo $value['claim'] ?></td>

                              <td><?php echo $value['details'] ?></td>

                              <td><?php echo $this->common->customer_name($value['customer_id']) ?></td>

                              <td><?php echo $this->common->claim_status($value['status']) ?></td>

                            </tr>

                        <?php
								$count++;
							}
						?>



                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';
						}?>
            <input type="hidden" name="5_test">
