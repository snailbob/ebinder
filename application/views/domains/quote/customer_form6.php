
<div class="form-group">
    <label>Quotes & Policies</label>
</div>


                        <?php
                          $broker_id = $this->session->userdata('id');
                          $customer_idd = $this->session->userdata('customer_details');
                          $customer_idd = (isset($customer_idd['id'])) ? $customer_idd['id'] : '';
                          $whr = array('broker_id'=>$broker_id, 'bound'=>'Y');
                    			$biz_referral = $this->common->format_referral($whr);

                          $biz_referral2 = array();

                         if(count($biz_referral) > 0) {
                           foreach($biz_referral as $u=>$value){
                             //echo $value['content']['homebiz1']['1_customer_id']. ' = '.$customer_idd.'<br>';
                             if($value['content']['homebiz1']['1_customer_id'] == $customer_idd) {
                               $biz_referral2[] = $value;
                             }
                           }
                         } ?>


                         <?php

                           if(count($biz_referral2) > 0) {

                            ?>

                        <div class="table_search">
                            <input type="text" class="form-control input_table_search_modal" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <div class="row hidden">
                            <div class="col-md-3 col-md-offset-9">
                                <form action="#" method="get">
                                   <input class="form-control" id="system-search" name="q" data-table="table-policies" placeholder="Search" required>

                                    <!-- <div class="input-group">
                                        <input class="form-control" id="system-search" name="q" data-table="table-policies" placeholder="Search" required>
                                        <span class="input-group-btn hidden">
                                            <button type="submit" class="btn btn--orange"><i class="fa fa-search"></i></button>
                                        </span>
                                    </div> -->
                                </form>
                            </div>
                        </div>

        						<div class="table-responsivex">

                        <table class="table table-striped table-hover table-datatable-modal table-policies">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <th>Quote ID</th>
                              <th>Policy ID</th>
                              <th>Name</th>
                              <th>Status</th>
                              <th>Expiry Date</th>
                            </tr>
                          </thead>
                          <tbody>

                        <?php
							$count = 1;
							foreach($biz_referral2 as $u=>$value) {

						?>

                            <tr>

                              <td class="hidden"><?php echo $count; ?></td>
                              <td><?php echo $value['quote_id']; ?></td>

                              <td><?php echo $value['policy_id']; ?></td>

                              <td><?php echo $value['customer']['name'] ?></td>

                              <td><?php echo $value['status'] ?></td>

                              <td><?php echo date_format(date_create($value['date_expires']), 'd M Y').' ('.$value['days_remaining'].'d left)' ?></td>


                            </tr>

                        <?php
								$count++;
              }
						?>



                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php  } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';
						}  ?>
            <input type="hidden" name="6_test">
