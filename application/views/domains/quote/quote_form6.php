
        <div class="row">
            <div class="col-sm-12">

                <div class="wellx">
                    <div class="row">

                        <div class="col-sm-12 the-form-col">
                            <div class="the-item-form-1">
                            	<div class="row">
                                	<div class="col-sm-6">
                                        <div class="form-group">
											<?php $thequest = 'Insured Name'; ?>
                                            <label><?php echo $thequest; ?></label>
                                        </div>
                                    </div>
                                	<div class="col-sm-4">
                                        <div class="form-group">

                                            <input type="text" name="6_<?php echo url_title($thequest, 'underscore', TRUE) ?>[]" class="form-control" value="">
                                        </div>


                                    </div>
                                	<div class="col-sm-2">
                                        <div class="form-group">
                                            <a href="#" class="pull-right btn btn-default add-items-btn-dyna" data-form="the-item-form-1" data-container="the-items-list-1">+</a>
                                            <p class="text-danger hidden">Please add items to this cover</p>

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>


                        <div class="col-sm-12 the-items-list-1 <?php echo (count($homebiz6['6_insured_name']) == 0) ? 'hidden' : '' ?>">
                      <?php $thequest = 'Insured Name'; ?>
                          <div class="well well-sm" style="background: transparent">
                            <p>Insured Name: </p>

                            <?php if(count($homebiz6['6_insured_name']) > 0){
                      foreach($homebiz6['6_insured_name'] as $r=>$value) {
                      if($value != ''){
                      ?>

                              <div class="badge-like">

                                <a href="#" class="pull-right close remove-item-btn" data-form="the-item-form-1" data-target="the-item-form-1-clone-copy-<?php echo $r ?>"><i class="fa fa-trash-o"></i></a>
                                <?php echo $value ?>

                              </div>



                            <div class="the-item-form-1-clone hidden the-item-form-1-clone-copy-<?php echo $r ?>">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" name="6_<?php echo url_title($thequest, 'underscore', TRUE) ?>[]" class="form-control" value="<?php echo $value ?>">

                                    </div>
                                </div>

                            </div>
                            <?php }
                      }
                      } ?>
                          </div>

                        </div>




                    </div>


                </div>

            </div>
        </div>

<?php /*
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
					<?php
                    $thequest_id = '109';
                    $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                    $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                    $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                    $thequest2 = url_title($thequest, 'underscore', TRUE);
                     ?>

                    <p><?php echo $thequest; ?>
                        <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                    </p>

                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">

                  <input type="text" name="6_<?php echo url_title($thequest2, 'underscore', TRUE) ?>" class="form-control" placeholder="" required="required" value="<?php echo (isset($homebiz6['6_'.$thequest2])) ? $homebiz6['6_'.$thequest2] : ''?>">
                </div>
            </div>
        </div>
*/ ?>


        <div class="row">
            <div class="col-sm-12">

                <div class="wellx">
                    <div class="row">

                        <div class="col-sm-12 the-form-col">
                            <div class="the-item-form-2">
                            	<div class="row">
                                	<div class="col-sm-6">
                                        <div class="form-group">
											<?php $thequest = 'Interested Parties'; ?>
                                            <label><?php echo $thequest; ?></label>
                                        </div>
                                    </div>
                                	<div class="col-sm-4">
                                        <div class="form-group">

                                            <input type="text" name="6_<?php echo url_title($thequest, 'underscore', TRUE) ?>[]" class="form-control">
                                        </div>



                                    </div>
                                	<div class="col-sm-2">
                                        <div class="form-group">
                                            <a href="#" class="pull-right btn btn-default add-items-btn-dyna" data-form="the-item-form-2" data-container="the-items-list-2">+</a>
                                            <p class="text-danger hidden">Please add items to this cover</p>

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="col-sm-12 the-items-list-2 <?php echo (count($homebiz6['6_interested_parties']) == 0) ? 'hidden' : '' ?>">

							<?php $thequest = 'Interested Parties'; ?>

                        <div class="well well-sm" style="background:transparent">
                            <p><?php echo $thequest ?></p>

                            <?php if(count($homebiz6['6_interested_parties']) > 0){
								foreach($homebiz6['6_interested_parties'] as $r=>$value) {
									if($value != ''){
								?>


                          	<div class="badge-like">

                            	<a href="#" class="pull-right close remove-item-btn" data-form="the-item-form-2" data-target="the-item-form-1-clone-copy2-<?php echo $r ?>"><i class="fa fa-trash-o"></i></a>
                              <?php echo $value ?>

                            </div>


                            <div class="the-item-form-2-clone hidden the-item-form-1-clone-copy2-<?php echo $r ?>">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" name="6_<?php echo url_title($thequest, 'underscore', TRUE) ?>[]" class="form-control" value="<?php echo $value ?>">

                                    </div>
                                </div>

                            </div>
                            <?php }
								}
							} ?>
                          </div>


                        </div>




                    </div>


                </div>

            </div>
        </div>

<?php /*
        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '110';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>

                <p><?php echo $thequest; ?>
                    <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                </p>

            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="6_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz6['6_'.$thequest2])) { echo ($homebiz6['6_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="6_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz6['6_'.$thequest2])) { echo ($homebiz6['6_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>
                </div>
            </div>
        </div>
*/ ?>
