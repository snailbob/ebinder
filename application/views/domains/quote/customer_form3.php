
  <div class="row gutter-md">
      <div class="col-sm-4">
          <div class="form-group">
              <label><b>Annual Revenue ($)</b></label>
              <input type="text" placeholder="Annual Revenue ($)" class="form-control input-number" name="annaul_revenue" value="<?php echo (isset($customer_details['annaul_revenue'])) ? $customer_details['annaul_revenue'] : ''?>" />
          </div>
      </div>
      <div class="col-sm-4">
          <div class="form-group">
              <label><b>Forecast revenue ($)</b></label>

              <input type="text" placeholder="Forecast revenue ($)" class="form-control input-number" name="forecast_revenue" value="<?php echo (isset($customer_details['forecast_revenue'])) ? $customer_details['forecast_revenue'] : ''?>" />
          </div>
      </div>

      <div class="col-sm-4">
          <div class="form-group">
              <label><b>Total assets ($)</b></label>

              <input type="text" class="form-control input-number" placeholder="Total assets ($)" name="total_assets"  value="<?php echo (isset($customer_details['total_assets'])) ? $customer_details['total_assets'] : ''?>" />
          </div>
      </div>
  </div>


  <div class="form-group">
    <div class="row gutter-md">


      <div class="col-sm-12">
        <label><b>Revenue by State (%)</b></label>

      </div>
      <div class="col-sm-2">
          <div class="padding-bottom-10">
            <label>NSW</label>
            <input type="text" name="breakdown_by_state[]" class="form-control input-number active-numpad" placeholder="NSW" value="<?php echo (isset($customer_details['breakdown_by_state'][0])) ? $customer_details['breakdown_by_state'][0] : ''?>" max="100" min="0">
          </div>
      </div>



      <div class="col-sm-2">
          <div class="padding-bottom-10">
            <label>ACT</label>

            <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="ACT"  value="<?php echo (isset($customer_details['breakdown_by_state'][1])) ? $customer_details['breakdown_by_state'][1] : ''?>" max="100" min="0">
          </div>
      </div>


      <div class="col-sm-2">
          <div class="padding-bottom-10">
            <label>QLD</label>

            <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="QLD"  value="<?php echo (isset($customer_details['breakdown_by_state'][2])) ? $customer_details['breakdown_by_state'][2] : ''?>" max="100" min="0">
          </div>
      </div>



      <div class="col-sm-2">
          <div class="padding-bottom-10">
            <label>VIC</label>

            <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="VIC"  value="<?php echo (isset($customer_details['breakdown_by_state'][3])) ? $customer_details['breakdown_by_state'][3] : ''?>" max="100" min="0">
          </div>
      </div>



      <div class="col-sm-2">
          <div class="padding-bottom-10">
            <label>TAS</label>

            <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="TAS"  value="<?php echo (isset($customer_details['breakdown_by_state'][4])) ? $customer_details['breakdown_by_state'][4] : ''?>" max="100" min="0">
          </div>
      </div>



      <div class="col-sm-2">
          <div class="padding-bottom-10">
            <label>SA</label>

            <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="SA"  value="<?php echo (isset($customer_details['breakdown_by_state'][5])) ? $customer_details['breakdown_by_state'][5] : ''?>" max="100" min="0">
          </div>
      </div>



      <div class="col-sm-2">
          <div class="padding-bottom-10">
            <label>WA</label>

            <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="WA"  value="<?php echo (isset($customer_details['breakdown_by_state'][6])) ? $customer_details['breakdown_by_state'][6] : ''?>" max="100" min="0">
          </div>
      </div>



      <div class="col-sm-2">
          <div class="padding-bottom-10">
            <label>NT</label>

            <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="NT"  value="<?php echo (isset($customer_details['breakdown_by_state'][7])) ? $customer_details['breakdown_by_state'][7] : ''?>" max="100" min="0">
          </div>
      </div>



      <div class="col-sm-2">
          <div class="padding-bottom-10">
            <label>O/S</label>

            <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="O/S"  value="<?php echo (isset($customer_details['breakdown_by_state'][8])) ? $customer_details['breakdown_by_state'][8] : ''?>" max="100" min="0">
          </div>
      </div>


      <div class="col-sm-6">
        <label>&nbsp;</label>

          <div class="form-control-static padding-top-10">
            <?php
            $total_break = 0;
            if(isset($customer_details['breakdown_by_state'])){
              for($i = 0; $i < 8; $i++){
                $total_break += $customer_details['breakdown_by_state'][$i];
              }
            }
            ?>

            Total must be 100% <strong><span class="the-total text-info">(<?php echo $total_break ?>%)</span></strong>
          </div>
      </div>

    </div>

  </div>
