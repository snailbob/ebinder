<p><strong>Business Activity</strong></p>


<div class="row gutter-md">


        <div class="col-sm-12">
            <div class="form-group">
                <select class="form-control selectpicker" name="occupation_id" data-live-search="true" required="required">
                    <option value="" data-code="">Occupation</option>
                    <?php
                        foreach($occupations as $r=>$mc){
                            echo '<option value="'.$mc['id'].'"';

                            if(isset($customer_details['occupation_id'])){
                              echo ($customer_details['occupation_id'] == $mc['id']) ? ' selected="selected"' : '';
                            }

                            echo '>'.substr($mc['specific_name'], 0, 70).'</option>';
                        }
                    ?>
                </select>
            </div>
        </div>




        <div class="col-sm-12">
            <div class="form-group">
                <textarea class="form-control" name="business_activity" rows="4" placeholder="Business activities"><?php if(isset($customer_details['business_activity'])){
                      echo nl2br($customer_details['business_activity']);
                    }
                   ?></textarea>
            </div>
        </div>



  </div>
