        <input type="hidden" name="1_product_id" value="<?php echo $this->session->userdata('quote_product_id')?>">


                                <div class="row hidden">

                                    <div class="col-sm-6">
                                        <div class="form-group">

                                            <?php
                                            $thequest = 'Customer Id';
                                            $thequest2 = url_title($thequest, 'underscore', TRUE);
                                             ?>

                                            <label><strong>Select customer if existing</strong></label>


                                        </div>
                                    </div>
                                    <div class="col-sm-4">

                                        <div class="form-group">


                                            <select class="form-control selectpicker" data-live-search="true" name="1_<?php echo $thequest2; ?>">
                                              <option value="">Select</option>
                                              <?php foreach($broker_customers as $r=>$value){


                                            echo '<option value="'.$value['id'].'"';

                                                    if($homebiz1['1_'.$thequest2] == $value['id']){
                                                      echo ' selected="selected"';
                                                    }
                                                  echo '>'.$value['first_name'].' '.$value['last_name'].'</option>';
                                              } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row hidden">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?php
                                            $thequest = 'Address Details (ABN)';
                                            $thequest2 = url_title($thequest, 'underscore', TRUE);
                                             ?>
                                            <label><strong><?php echo $thequest; ?></strong></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                          <input type="text" name="1_<?php echo $thequest2; ?>" class="form-control abn_input" placeholder="" value="<?php echo (isset($homebiz1['1_'.$thequest2])) ? $homebiz1['1_'.$thequest2] : ''?>">

                                        </div>
                                    </div>
                                    <div class="col-sm-2">


                                        <div class="form-group">
                                          <button class="btn btn-block btn-default abn_input_btn" type="button">Validate</button>
                                        </div>
                                    </div>
                                </div>





                            	<?php if($this->session->userdata('for_renewal') == 'endorse') { ?>
                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?php
                                            $thequest = 'Date of endorsement';
                                            $thequest2 = url_title($thequest, 'underscore', TRUE);
                                             ?>
                                            <label><strong><?php echo $thequest; ?></strong></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                          <input type="date" name="1_<?php echo $thequest2; ?>" class="form-control" placeholder="" required="required" value="<?php echo (isset($homebiz1['1_'.$thequest2])) ? $homebiz1['1_'.$thequest2] : ''?>">
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>


                                <div class="row">
                                    <div class="col-sm-12">
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
											<?php
                                            $thequest_id = '86';
                                            $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                                            $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                                            $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                                            $thequest2 = url_title($thequest, 'underscore', TRUE);
                                             ?>

																					 <strong>Revenue Details</strong><br>
                                            <label><?php echo $thequest; ?>

                                            <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                          <input type="text" name="1_<?php echo $thequest2; ?>" class="form-control input-number" placeholder="" required="required" value="<?php echo (isset($homebiz1['1_'.$thequest2])) ? $homebiz1['1_'.$thequest2] : ''?>">
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
											<?php
                                            $thequest_id = '87';
                                            $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                                            $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                                            $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                                            $thequest2 = url_title($thequest, 'underscore', TRUE);
                                             ?>


                                            <label><?php echo $thequest; ?>

                                            <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                          <input type="text" name="1_<?php echo $thequest2; ?>" class="form-control" placeholder="" required="required" value="<?php echo (isset($homebiz1['1_'.$thequest2])) ? $homebiz1['1_'.$thequest2] : ''?>">
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-sm-12">
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
											<?php
                                            $thequest_id = '88';
                                            $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                                            $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                                            $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                                            $thequest2 = url_title($thequest, 'underscore', TRUE);
                                             ?>

																						<strong>Employee Number</strong><br>

                                            <label><?php echo $thequest; ?>

                                            <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>

                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                          <input type="text" name="1_<?php echo $thequest2; ?>" class="form-control input-number" placeholder="" required="required" value="<?php echo (isset($homebiz1['1_'.$thequest2])) ? $homebiz1['1_'.$thequest2] : ''?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-10">
                                        <div class="well well-sm">
											<?php
                                            $thequest_id = '89';
                                            $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                                            $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                                            $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                                            $thequest2 = 'occupation_find'; //url_title($thequest, 'underscore', TRUE);
                                             ?>

                                            <p><strong><?php echo $thequest; ?></strong></p>

                                            <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>



                                            <div class="form-groupx">
                                                <input type="text" class="form-control" name="<?php echo $thequest2 ?>" placeholder="Occupation" value="<?php echo (isset($homebiz1[''.$thequest2])) ? $homebiz1[''.$thequest2] : ''?>" required="required"/>

                                            <?php
                                            $thequest2 = 'occupation_id'; //url_title($thequest, 'underscore', TRUE);
                                             ?>

                                                <input type="hidden" name="<?php echo $thequest2 ?>" value="<?php echo (isset($homebiz1[''.$thequest2])) ? $homebiz1[''.$thequest2] : ''?>"/>
                                                <div class="occupation-suggest hidden">
                                                    <div class="list-group list-group-suggest">
                                                    </div>


                                                </div>


                                            </div>

                                        </div>

                                    </div>
                                </div>



                                <div class="row">
                                	<?php
                                  $the_total = 0;
                                  foreach($the_states as $thr=>$thvalue) { ?>

                    											<?php
                                            $thequest_id = '90';
                                            $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                                            $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                                            $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                                            $thequest2 = url_title($thequest, 'underscore', TRUE);
                                             ?>

                                             <?php if($thr == 0) { ?>

			                                     <div class="col-sm-6">
			                                         <div class="form-group">

      																				    <label><?php echo $thequest; ?>

                                                  <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>

				                                        </div>
				                                    </div>
																						<div class="clearfix"></div>

                                            <!--.state wrapper-->
                                            <div class="form-group col-lg-10">
                                              <div class="row gutter-md">

                                            <?php } ?>

																		 <?php echo ($thr == 5) ? '<div class="clearfix"></div>' : '' ?>

                                    <div class="col-sm-2">
                                        <?php
                                          $nmbr_val = (isset($homebiz1['1_'.$thequest2])) ? $homebiz1['1_'.$thequest2][$thr] : '0';
                                          $the_total += $nmbr_val
                                        ?>
                                        <div class="form-groupx" style="padding-bottom: 15px">
                                        	<label><strong><?php echo $thvalue ?></strong></label>
                                          <input type="text" name="1_<?php echo $thequest2; ?>[]" class="form-control input-number" placeholder="" value="<?php echo (isset($homebiz1['1_'.$thequest2])) ? $homebiz1['1_'.$thequest2][$thr] : '' ?>" max="100" min="0">
                                        </div>
                                    </div>


                                    <?php } ?>
                                    <div class="col-sm-4">
																					<label>&nbsp;</label>

                                        <div class="form-control-static">

                                          Total must be 100% <strong><span class="the-total text-info"> (<?php echo $the_total; ?>%)</span></strong>
                                        </div>
                                    </div>


                                      </div>
                                    </div><!--.state wrapper-->


                                </div>
