    <p><b>Organisation information</b></p>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label>Organisation name</label>
                <input type="text" class="form-control" placeholder="Organisation name" name="company_name" required="required" value="<?php echo (isset($customer_details['company_name'])) ? $customer_details['company_name'] : ''?>" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <div class="form-group">
                <label>Floor/Suite Number</label>
                <input type="text" class="form-control" placeholder="Floor/Suite Number" name="suite_number" value="<?php echo (isset($customer_details['suite_number'])) ? $customer_details['suite_number'] : ''?>" />
            </div>
        </div>
        <div class="col-sm-8">
            <div class="form-group">
                <label>Street address</label>


                <input type="text" class="form-control user_geolocation" placeholder="Street address" name="address" value="<?php echo (isset($customer_details['address'])) ? $customer_details['address'] : ''?>"/>
                <input type="hidden" name="lat" value="<?php echo (isset($customer_details['lat'])) ? $customer_details['lat'] : ''?>" required="required" />
                <input type="hidden" name="lng" value="<?php echo (isset($customer_details['lng'])) ? $customer_details['lng'] : ''?>" />


            </div>
        </div>

    </div>


    <div class="row">
      <div class="col-sm-6">
        <div class="form-group">
            <label>ACN / ABN</label>
            <input type="text" class="form-control" placeholder="ACN / ABN" name="acn" min="1" value="<?php echo (isset($customer_details['acn'])) ? $customer_details['acn'] : ''?>" />
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
            <label>Date organisation established</label>
            <input type="date" class="form-control" placeholder="Date organisation established " name="date_established" value="<?php echo (isset($customer_details['date_established'])) ? $customer_details['date_established'] : ''?>" />
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-sm-6">
      <div class="form-group">
          <label>Type of organisation</label>
          <select class="form-control selectpicker" name="organisation_type" data-live-search="false">
              <option value="" data-code="">Select..</option>
              <?php
                  $type_org = array(
                    'Publicly listed',
                    'Pty Ltd',
                    'Public unlisted',
                    'Non-profit',
                    'Partnership',
                    'Sole trader',
                    'Trust',
                    'Other'
                  );

                  foreach($type_org as $r=>$value){
                      echo '<option value="'.$r.'"';

                      if(isset($customer_details['organisation_type'])){
                        if($customer_details['organisation_type'] == $r){
                          echo ' selected="selected"';
                        }
                      }
                      echo '>'.$value.'</option>';
                  }
              ?>
          </select>
        </div>
      </div>
      <div class="col-sm-6">
        <div class="form-group">
            <label>Website</label>
            <input type="text" class="form-control" placeholder="Website" name="website" value="<?php echo (isset($customer_details['website'])) ? $customer_details['website'] : ''?>" />
        </div>
      </div>
    </div>



    <p><strong>Primary contact details</strong></p>

    <div class="row">

        <div class="col-sm-6">
          <div class="form-group">
              <label>First Name</label>

                <input type="text" placeholder="First Name" class="form-control" name="first" required="required" value="<?php echo (isset($customer_details['first'])) ? $customer_details['first'] : ''?>"/>
            </div>
        </div>
        <div class="col-sm-6">
          <div class="form-group">
              <label>Last Name</label>

                <input type="text" placeholder="Last Name" class="form-control" name="last" required="required" value="<?php echo (isset($customer_details['last'])) ? $customer_details['last'] : ''?>"/>
            </div>
        </div>
    </div>


    <div class="form-group hidden">
        <label>Brokerage Name</label>
        <select class="form-control " name="brokerage_id">
          <option value="">Select</option>
          <?php foreach($brokerage as $r=>$value){


        echo '<option value="'.$value['id'].'"';

                if($this->session->userdata('logged_admin_brokerage') == $value['id']){
                  echo ' selected="selected"';
                }
              echo '>'.$value['company_name'].'</option>';
          } ?>
        </select>
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="email" placeholder="Email" class="form-control" name="email" required="required" value="<?php echo (isset($customer_details['email'])) ? $customer_details['email'] : ''?>" />
    </div>





    <div class="row">

        <div class="col-sm-6">
          <div class="form-group">
                <label>Country Calling Code</label>
                <select class="form-control selectpicker country_code_dd" name="country_code" data-live-search="true" required="required">
                    <option value="" data-code="">Select ..</option>
                    <?php
                        foreach($countries as $r=>$mc){
                            echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';


                            if(isset($customer_details['country_code'])){
                              if($customer_details['country_code'] == $mc['country_id']){
                                echo ' selected="selected"';
                              }
                            }

                            echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                        }
                    ?>
                </select>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="form-group mobile_input">

                  <label>Mobile number</label>
                  <span class="mobile-icon">
                    <i class="fa fa-mobile-phone fa-2x"></i>
                  </span>

                  <input type="text" class="form-control mobile_number" placeholder="Mobile number" name="mobile" autocomplete="nope" required="required" data-rule-minlength="8" value="<?php echo (isset($customer_details['mobile'])) ? $customer_details['mobile'] : ''?>">

                  <input type="hidden" name="country_short" value="<?php echo (isset($customer_details['country_short'])) ? $customer_details['country_short'] : ''?>"/>
            </div>
        </div>

    </div><!--phone number-->
