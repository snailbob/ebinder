<?php
if($this->session->userdata('customer_quote') == '') { // || $this->uri->segment(2) != 'quote'
  $default_cargo = $this->common->default_cargo();
  $deductibles = $this->master->getRecords('deductibles', array('agency_id'=>$default_cargo), '', array('not_default'=>'DESC'));

  ?>

        <div class="row gutter-sm">
          <div class="col-lg-8 col-md-7">
            <div class="well well-premium">
                <p class="text-center"><strong>Total Premium Payable</strong></p>
                <h2 style="font-size:70px !important; margin: 35px 0 35px 0; text-align: center">
                  $<?php echo (isset($the_quote['total_format'])) ? $the_quote['total_format'] : 0 ?>
                </h2>
            </div>
            <?php if(count($deductibles) > 1){ ?>
            <div class="row">
              <div class="col-sm-4 col-lg-3">
                <label class="form-control-static">Change Excess</label>
                <?php
                        $thequest_id = '0';
                        $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                        $thequest = (!empty($question)) ? $question[0]['question'] : 'Change Excess';
                        $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                        $thequest2 = url_title($thequest, 'underscore', TRUE);
                         ?>

              </div>
              <div class="col-sm-8 col-lg-9">
                <div class="form-group">

                    <select name="5_<?php echo $thequest2 ?>" class="form-control">
                      <?php
                        foreach($deductibles as $r=>$value){
                          echo '<option value="'.$value['rate'].'"';
                          if (isset($homebiz5['5_'.$thequest2])){
                            if($value['rate'] == $homebiz5['5_'.$thequest2]){
                              echo ' selected="selected"';
                            }

                          }
                          echo '>$'.$value['deductible'].'</option>';
                        }
                      ?>
                    </select>
                </div>
              </div>
            </div>
            <?php } ?>



          </div>

          <div class="col-lg-4 col-md-5">
            <button type="submit" class="btn btn-primary btn-block quote_continue_btnx" style="padding: 58px 15px; white-space: none; margin-bottom: 4px">
              <span style="font-size: 200%"><strong>BUY NOW</strong></span>
            </button>
            <div class="row sm-gutter">
              <div class="col-sm-6">

                <button type="button" class="btn btn-block quoteaction-button" data-val="Email">
                  <span>Email Quote</span>
                </button>
              </div>
              <div class="col-sm-6">

                <button type="button" class="btn btn-block quoteaction-button" data-val="Save">
                  <span>Save Quote</span>
                </button>
                <div class="clearfix"></div>
              </div>

            </div>

          </div>
        </div>
        <hr>



        <div class="row">
            <div class="col-sm-12">
            	<p><strong>Coverage Selection</strong></p>

            </div>
            <div class="col-sm-6">
				<?php
                $thequest_id = '105';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>

                <p><?php echo $thequest; ?>
                    <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                </p>

            </div>
            <div class="col-sm-4">
                <div class="form-group">

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="5_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz5['5_'.$thequest2])) { echo ($homebiz5['5_'.$thequest2] == '$1M') ? 'checked="checked"' : ''; }?>><span></span></div>
                        $1M
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="5_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz5['5_'.$thequest2])) { echo ($homebiz5['5_'.$thequest2] != '$2M') ? 'checked="checked"' : ''; }?>><span></span></div>
                        $2M
                    </label>

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="5_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz5['5_'.$thequest2])) { echo ($homebiz5['5_'.$thequest2] != '$5M') ? 'checked="checked"' : ''; }?>><span></span></div>
                        $5M
                    </label>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '106';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>

                <p><?php echo $thequest; ?>
                    <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                </p>

            </div>
            <div class="col-sm-4">
                <div class="form-group">

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="5_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz5['5_'.$thequest2])) { echo ($homebiz5['5_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="5_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz5['5_'.$thequest2])) { echo ($homebiz5['5_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
					<?php
                    $thequest_id = '107';
                    $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                    $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                    $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                    $thequest2 = url_title($thequest, 'underscore', TRUE);
                     ?>

                    <p><?php echo $thequest; ?>
                        <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                    </p>

                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                  <input type="text" name="5_<?php echo $thequest2; ?>" class="form-control promo-code-input" placeholder="" value="<?php echo (isset($homebiz5['5_'.$thequest2])) ? $homebiz5['5_'.$thequest2] : ''?>">
                    <p class="help-block"></p>
                </div>
            </div>
            <div class="col-sm-2">
                <div class="form-group">
                  <a href="#" class="btn btn-default promo-code-btn">Validate</a>
                </div>
            </div>
        </div>

        <?php
        $thequest = 'discount';
        $thequest2 = url_title($thequest, 'underscore', TRUE);
        ?>
        <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo (isset($homebiz5['5_'.$thequest2])) ? $homebiz5['5_'.$thequest2] : ''?>">



        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
					<?php
                    $thequest_id = '108';
                    $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                    $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                    $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                    $thequest2 = url_title($thequest, 'underscore', TRUE);
                     ?>

                    <p><?php echo $thequest; ?>
                        <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                    </p>

                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                	<strong>$<?php echo $the_quote['policy_fee']?></strong>
                  <input type="hidden" name="5_<?php echo $thequest2; ?>" class="form-control" placeholder="" value="<?php echo $the_quote['policy_fee'] ?>">
                </div>
            </div>
        </div>



        <?php
        $thequest = 'base_rate';
        $thequest2 = url_title($thequest, 'underscore', TRUE);
        ?>
        <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['base_rate'] ?>">


        <?php
        $thequest = 'annaul_revenue';
        $thequest2 = url_title($thequest, 'underscore', TRUE);
        ?>
        <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['annaul_revenue'] ?>">


        <?php
        $thequest = 'occupation_multiplier';
        $thequest2 = url_title($thequest, 'underscore', TRUE);
        ?>
        <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['occupation_multiplier'] ?>">


        <?php
        $thequest = 'employee_multiplier';
        $thequest2 = url_title($thequest, 'underscore', TRUE);
        ?>
        <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['employee_multiplier'] ?>">


        <?php
        $thequest = 'Total Premium Payable (Including GST) :';
        $thequest2 = url_title($thequest, 'underscore', TRUE);
        ?>
        <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['total_format'] ?>">




        <hr />
		<?php if($this->session->userdata('for_renewal') == 'endorse') { ?>
		<?php // if(isset($the_quote['endorse'])) {?>
        <div class="row">
            <div class="col-sm-6">
                <p><strong>Current premium:</strong></p>
            </div>
            <div class="col-sm-4">
                <p class="text-primary"><strong>$<?php echo $the_quote['endorse']['current_premium']; ?></strong></p>

            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <p><strong>Endorsement premium:</strong></p>
            </div>
            <div class="col-sm-4">
                <p class="text-primary"><strong> - $<?php echo $the_quote['endorse']['endorsed_premium']; ?></strong></p>

            </div>
        </div>
        <hr />


        <div class="row">
            <div class="col-sm-6">
                <p><strong>Premium difference: </strong></p>
            </div>
            <div class="col-sm-4">
                <p class="lead text-primary"><strong>$<?php echo $the_quote['endorse']['prorate']; ?></strong> <small class="text-muted small">prorated</small></p>

            </div>
        </div>


			<?php
            $thequest = 'current_premium';
            $thequest2 = url_title($thequest, 'underscore', TRUE);
            ?>
            <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['endorse']['current_premium'] ?>">

			<?php
            $thequest = 'endorsed_premium';
            $thequest2 = url_title($thequest, 'underscore', TRUE);
            ?>
            <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['endorse']['endorsed_premium'] ?>">

			<?php
            $thequest = 'premium_difference';
            $thequest2 = url_title($thequest, 'underscore', TRUE);
            ?>
            <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['endorse']['premium_difference'] ?>">

			<?php
            $thequest = 'days_prorate';
            $thequest2 = url_title($thequest, 'underscore', TRUE);
            ?>
            <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['endorse']['days_prorate'] ?>">

			<?php
            $thequest = 'prorate';
            $thequest2 = url_title($thequest, 'underscore', TRUE);
            ?>
            <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['endorse']['prorate'] ?>">



        <?php } else { ?>



          <div class="row hidden">
              <div class="col-sm-6">
  				<?php
                  $thequest_id = '0';
                  $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                  $thequest = (!empty($question)) ? $question[0]['question'] : 'Quote options: ';
                  $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                  $thequest2 = url_title($thequest, 'underscore', TRUE);
                  $action_val = 'Bind';
                  $action_val = (isset($homebiz5['5_'.$thequest2])) ? $homebiz5['5_'.$thequest2] : $action_val;
                   ?>

                  <p><?php echo $thequest; ?>
                      <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                  </p>

              </div>
              <div class="col-sm-4">
                  <div class="form-group">

                      <label class="form-checkbox">
                          <div class="enhanced-checkbox">
                              <input type="radio" class="mandatory checkbox user-success" name="5_<?php echo $thequest2 ?>" value="Bind" required="required" <?php echo ($action_val == 'Bind') ? 'checked="checked"' : ''; ?> checked="checked"><span></span></div>
                          Bind
                      </label>
                      <label class="form-checkbox">
                          <div class="enhanced-checkbox">
                              <input type="radio" class="mandatory checkbox user-success" name="5_<?php echo $thequest2 ?>" value="Email" required="required" <?php echo ($action_val == 'Email') ? 'checked="checked"' : ''; ?>><span></span></div>
                          Email
                      </label>
                      <label class="form-checkbox">
                          <div class="enhanced-checkbox">
                              <input type="radio" class="mandatory checkbox user-success" name="5_<?php echo $thequest2 ?>" value="Save" required="required" <?php echo ($action_val == 'Save') ? 'checked="checked"' : ''; ?>><span></span></div>
                          Save
                      </label>

                  </div>
              </div>
          </div>





          <div class="row hidden">
              <div class="col-sm-6">
                  <p><strong>Total Premium Payable <small>(Including GST) :</small></strong></p>
              </div>
              <div class="col-sm-4">
                  <p class="lead text-primary"><strong>$<?php echo (isset($the_quote['total_format'])) ? $the_quote['total_format'] : 0 ?></strong></p>

              </div>
          </div>
        <?php } ?>

    <input type="hidden" name="5_updated_by_customer" value="No"/>


<?php } else{ ?>
    <p class="lead text-center">Broker will provide quote</p>
    <input type="hidden" name="5_updated_by_customer" value="Yes"/>
<?php } ?>
