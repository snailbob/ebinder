
        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '95';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>

                <p><?php echo $thequest; ?>
                    <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                </p>

            </div>
            <div class="col-sm-4 col-md-3">
                <div class="form-group">

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="3_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz3['3_'.$thequest2])) { echo ($homebiz3['3_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="3_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz3['3_'.$thequest2])) { echo ($homebiz3['3_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '96';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>

                <p><?php echo $thequest; ?>
                    <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                </p>

            </div>
            <div class="col-sm-4 col-md-3">
                <div class="form-group">

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="3_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz3['3_'.$thequest2])) { echo ($homebiz3['3_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="3_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz3['3_'.$thequest2])) { echo ($homebiz3['3_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '97';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>

                <p><?php echo $thequest; ?>
                    <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                </p>

            </div>
            <div class="col-sm-4 col-md-3">
                <div class="form-group">

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="3_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz3['3_'.$thequest2])) { echo ($homebiz3['3_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="3_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz3['3_'.$thequest2])) { echo ($homebiz3['3_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '98';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>

                <p><?php echo $thequest; ?>
                    <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                </p>

            </div>
            <div class="col-sm-4 col-md-3">
                <div class="form-group">

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="3_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz3['3_'.$thequest2])) { echo ($homebiz3['3_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="3_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz3['3_'.$thequest2])) { echo ($homebiz3['3_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>
