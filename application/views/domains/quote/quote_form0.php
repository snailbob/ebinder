

<div class="row">

  <div class="col-sm-8">


    <div class="row">
        <div class="col-sm-12">
            <div class="form-groupx">
              <?php
                $thequest_id = '87';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = 'New or existing customer?'; //(!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                $thequest_exist = $thequest2;
                $thequest_current = 'Existing';

                if(isset($homebiz0['user_id'])){
                  if(!isset($homebiz0[$thequest_exist])){
                    $thequest_current = '';
                  }
                }

                 ?>


                <label><?php echo $thequest; ?>

                <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="form-group">
              <input type="checkbox" class="bs_switch" name="<?php echo $thequest2 ?>" data-on-text="Existing" data-off-text="New" data-handle-width="auto" <?php echo ($thequest_current == 'Existing') ? 'checked="checked"' : ''?> value="Existing">
            </div>
        </div>
    </div>




    <div class="row the_existing <?php echo ($thequest_current == 'Existing') ? '' : 'hidden'?>">

        <div class="col-sm-6">
            <div class="form-groupx">

                <?php
                $thequest = 'Customer Id';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>

                <label>Select customer</label>


            </div>
        </div>
        <div class="col-sm-12">

            <div class="form-group">


                <select class="form-control selectpicker" data-live-search="true" name="<?php echo $thequest2; ?>">
                  <option value="">Select</option>
                  <?php foreach($broker_customers as $r=>$value){


                echo '<option value="'.$value['id'].'"';
                        $company_name = (isset($value['customer_info_format']['company_name'])) ? $value['customer_info_format']['company_name'] : 'NA';
                        if($homebiz1['1_'.$thequest2] == $value['id']){
                          echo ' selected="selected"';
                        }
                      echo '>'.$company_name.'</option>';
                  } ?>
                </select>
            </div>
        </div>
    </div>




    <div class="row the_new_cust <?php echo (!$thequest_current == 'Existing') ? '' : 'hidden'?>">

        <div class="col-sm-6">
            <div class="form-groupx">

                <?php
                $thequest = 'New customer details';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>

                <label><strong>New customer details</strong></label>


            </div>
        </div>
        <div class="col-sm-12">
          <div class="row">
              <div class="col-sm-12">
                  <div class="form-group">
                      <label>Organisation name</label>
                      <input type="text" class="form-control" placeholder="Organisation name" name="company_name" value="<?php echo (isset($homebiz0['company_name'])) ? $homebiz0['company_name'] : ''?>" />
                  </div>
              </div>
          </div>
          <div class="row">
              <div class="col-sm-4">
                  <div class="form-group">
                      <label>Floor/Suite Number</label>
                      <input type="text" class="form-control" placeholder="Floor/Suite Number" name="suite_number" value="<?php echo (isset($homebiz0['suite_number'])) ? $homebiz0['suite_number'] : ''?>"/>
                  </div>
              </div>
              <div class="col-sm-8">
                  <div class="form-group">
                      <label>Street address</label>


                      <input type="text" class="form-control user_geolocation" placeholder="Street address" name="address" value="<?php echo (isset($homebiz0['address'])) ? $homebiz0['address'] : ''?>"/>
                      <input type="hidden" name="lat" value="<?php echo (isset($homebiz0['lat'])) ? $homebiz0['lat'] : ''?>" />
                      <input type="hidden" name="lng" value="<?php echo (isset($homebiz0['lng'])) ? $homebiz0['lng'] : ''?>" />


                  </div>
              </div>

          </div>


          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                  <label>ACN / ABN</label>
                  <input type="text" class="form-control" placeholder="ACN / ABN" name="acn" min="1" value="<?php echo (isset($homebiz0['acn'])) ? $homebiz0['acn'] : ''?>" />
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                  <label>Date organisation established</label>
                  <input type="date" class="form-control" placeholder="Date organisation established " name="date_established"  value="<?php echo (isset($homebiz0['date_established'])) ? $homebiz0['date_established'] : ''?>" />
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-sm-6">
            <div class="form-group">
                <label>Type of organisation</label>
                <select class="form-control selectpicker" name="organisation_type" data-live-search="true">
                    <option value="" data-code="">Select..</option>
                    <?php
                        $type_org = array(
                          'Publicly listed',
                          'Pty Ltd',
                          'Public unlisted',
                          'Non-profit',
                          'Partnership',
                          'Sole trader',
                          'Trust',
                          'Other'
                        );

                        foreach($type_org as $r=>$value){
                            echo '<option value="'.$r.'"';

                            if(isset($homebiz0['organisation_type'])){
                              if($homebiz0['organisation_type'] == $r){
                                echo ' selected="selected"';
                              }
                            }


                            echo '>'.$value.'</option>';
                        }
                    ?>
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                  <label>Website</label>
                  <input type="text" class="form-control" placeholder="Website" name="website" value="<?php echo (isset($homebiz0['website'])) ? $homebiz0['website'] : ''?>" />
              </div>
            </div>
          </div>



          <p><strong>Primary contact details</strong></p>

          <div class="row">

              <div class="col-sm-6">
                <div class="form-group">
                    <label>First Name</label>

                      <input type="text" placeholder="First Name" class="form-control" name="first" value="<?php echo (isset($homebiz0['first'])) ? $homebiz0['first'] : ''?>"/>
                  </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                    <label>Last Name</label>

                      <input type="text" placeholder="Last Name" class="form-control" name="last" value="<?php echo (isset($homebiz0['last'])) ? $homebiz0['last'] : ''?>" />
                  </div>
              </div>
          </div>


          <div class="form-group">
              <label>Email</label>
              <input type="email" placeholder="Email" class="form-control" name="email" value="<?php echo (isset($homebiz0['email'])) ? $homebiz0['email'] : ''?>" />
          </div>





          <div class="row">

              <div class="col-sm-6">
                <div class="form-group">
                      <label>Country Calling Code</label>
                      <select class="form-control selectpicker country_code_dd" name="country_code" data-live-search="true">
                          <option value="" data-code="">Select ..</option>
                          <?php
                              foreach($countries as $r=>$mc){
                                  echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';

                                  if(isset($homebiz0['country_short'])){
                                    if($homebiz0['country_short'] == $mc['country_id']){
                                      echo ' selected="selected"';
                                    }
                                  }


                                  echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                              }
                          ?>
                      </select>
                  </div>
              </div>

              <div class="col-sm-6">
                  <div class="form-group mobile_input">
                        <label>Mobile number</label>
                        <span class="mobile-icon">
                          <i class="fa fa-mobile-phone fa-2x"></i>
                        </span>

                        <input type="text" class="form-control mobile_number" placeholder="Mobile number" name="mobile" autocomplete="nope" value="<?php echo (isset($homebiz0['mobile'])) ? $homebiz0['mobile'] : ''?>">

                        <input type="hidden" name="country_short" value="<?php echo (isset($homebiz0['country_short'])) ? $homebiz0['country_short'] : ''?>"/>
                  </div>
              </div>

          </div><!--phone number-->



        </div>
    </div>

    <input type="hidden" name="0_customer" value=""/>


  </div>
</div>
