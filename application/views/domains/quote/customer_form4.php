
  <div class="row gutter-md">

      <div class="col-sm-12">
          <div class="form-group">
              <label><b>Employee Number</b></label>
              <div class="input-group">
                <input type="text" class="form-control input-number" placeholder="Employee Number" name="employee_number"  value="<?php echo (isset($customer_details['employee_number'])) ? $customer_details['employee_number'] : ''?>">
                <span class="input-group-btn increment_input_btn">
                  <button class="btn btn-default" type="button">
                    <i class="fa fa-minus-circle"></i>
                  </button>
                  <button class="btn btn-default" type="button">
                    <i class="fa fa-plus-circle"></i>
                  </button>
                </span>
              </div><!-- /input-group -->



          </div>
      </div>


      <div class="col-sm-12">
          <div class="form-group">

            <div class="row gutter-md">


              <div class="col-sm-12">
                <label><b>Employee by Location</b></label>

              </div>
              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>NSW</label>
                    <input type="text" name="employee_by_location[]" class="form-control input-number active-numpad" placeholder="NSW" value="<?php echo (isset($customer_details['employee_by_location'][0])) ? $customer_details['employee_by_location'][0] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>ACT</label>

                    <input type="text" name="employee_by_location[]" class="form-control input-number" placeholder="ACT"  value="<?php echo (isset($customer_details['employee_by_location'][1])) ? $customer_details['employee_by_location'][1] : ''?>" min="0">
                  </div>
              </div>


              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>QLD</label>

                    <input type="text" name="employee_by_location[]" class="form-control input-number" placeholder="QLD"  value="<?php echo (isset($customer_details['employee_by_location'][2])) ? $customer_details['employee_by_location'][2] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>VIC</label>

                    <input type="text" name="employee_by_location[]" class="form-control input-number" placeholder="VIC"  value="<?php echo (isset($customer_details['employee_by_location'][3])) ? $customer_details['employee_by_location'][3] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>TAS</label>

                    <input type="text" name="employee_by_location[]" class="form-control input-number" placeholder="TAS"  value="<?php echo (isset($customer_details['employee_by_location'][4])) ? $customer_details['employee_by_location'][4] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>SA</label>

                    <input type="text" name="employee_by_location[]" class="form-control input-number" placeholder="SA"  value="<?php echo (isset($customer_details['employee_by_location'][5])) ? $customer_details['employee_by_location'][5] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>WA</label>

                    <input type="text" name="employee_by_location[]" class="form-control input-number" placeholder="WA"  value="<?php echo (isset($customer_details['employee_by_location'][6])) ? $customer_details['employee_by_location'][6] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>NT</label>

                    <input type="text" name="employee_by_location[]" class="form-control input-number" placeholder="NT"  value="<?php echo (isset($customer_details['employee_by_location'][7])) ? $customer_details['employee_by_location'][7] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>O/S</label>

                    <input type="text" name="employee_by_location[]" class="form-control input-number" placeholder="O/S"  value="<?php echo (isset($customer_details['employee_by_location'][8])) ? $customer_details['employee_by_location'][8] : ''?>" min="0">
                  </div>
              </div>



            </div>


          </div>
      </div>




      <div class="col-sm-12">
          <div class="form-group">

            <div class="row gutter-md">


              <div class="col-sm-12">
                <label><b>Payroll by location ($)</b></label>

              </div>
              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>NSW</label>

                    <input type="text" name="payroll_by_location[]" class="form-control input-number active-numpad" placeholder="NSW" value="<?php echo (isset($customer_details['payroll_by_location'][0])) ? $customer_details['payroll_by_location'][0] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>ACT</label>

                    <input type="text" name="payroll_by_location[]" class="form-control input-number" placeholder="ACT"  value="<?php echo (isset($customer_details['payroll_by_location'][1])) ? $customer_details['payroll_by_location'][1] : ''?>" min="0">
                  </div>
              </div>


              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>QLD</label>

                    <input type="text" name="payroll_by_location[]" class="form-control input-number" placeholder="QLD"  value="<?php echo (isset($customer_details['payroll_by_location'][2])) ? $customer_details['payroll_by_location'][2] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>VIC</label>

                    <input type="text" name="payroll_by_location[]" class="form-control input-number" placeholder="VIC"  value="<?php echo (isset($customer_details['payroll_by_location'][3])) ? $customer_details['payroll_by_location'][3] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>TAS</label>

                    <input type="text" name="payroll_by_location[]" class="form-control input-number" placeholder="TAS"  value="<?php echo (isset($customer_details['payroll_by_location'][4])) ? $customer_details['payroll_by_location'][4] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>SA</label>

                    <input type="text" name="payroll_by_location[]" class="form-control input-number" placeholder="SA"  value="<?php echo (isset($customer_details['payroll_by_location'][5])) ? $customer_details['payroll_by_location'][5] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>WA</label>

                    <input type="text" name="payroll_by_location[]" class="form-control input-number" placeholder="WA"  value="<?php echo (isset($customer_details['payroll_by_location'][6])) ? $customer_details['payroll_by_location'][6] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>NT</label>

                    <input type="text" name="payroll_by_location[]" class="form-control input-number" placeholder="NT"  value="<?php echo (isset($customer_details['payroll_by_location'][7])) ? $customer_details['payroll_by_location'][7] : ''?>" min="0">
                  </div>
              </div>



              <div class="col-sm-2">
                  <div class="padding-bottom-10">
                    <label>O/S</label>

                    <input type="text" name="payroll_by_location[]" class="form-control input-number" placeholder="O/S"  value="<?php echo (isset($customer_details['payroll_by_location'][8])) ? $customer_details['payroll_by_location'][8] : ''?>" min="0">
                  </div>
              </div>



            </div>






          </div>
      </div>

  </div>
