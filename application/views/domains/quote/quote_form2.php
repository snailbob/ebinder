
        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '91';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>

                <p><?php echo $thequest; ?>
                    <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                </p>


            </div>
            <div class="col-sm-4">
                <div class="form-group">

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="2_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz2['2_'.$thequest2])) { echo ($homebiz2['2_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="2_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz2['2_'.$thequest2])) { echo ($homebiz2['2_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '92';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>

                <p><?php echo $thequest; ?>
                    <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                </p>

            </div>
            <div class="col-sm-4">
                <div class="form-group">

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="2_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz2['2_'.$thequest2])) { echo ($homebiz2['2_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="2_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz2['2_'.$thequest2])) { echo ($homebiz2['2_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '93';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>

                <p><?php echo $thequest; ?>
                    <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                </p>

            </div>
            <div class="col-sm-4">
                <div class="form-group">

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="2_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz2['2_'.$thequest2])) { echo ($homebiz2['2_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="2_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz2['2_'.$thequest2])) { echo ($homebiz2['2_'.$thequest2] == 'No') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="2_<?php echo $thequest2 ?>" value="NA" required="required" <?php if (isset($homebiz2['2_'.$thequest2])) { echo ($homebiz2['2_'.$thequest2] == 'NA') ? 'checked="checked"' : ''; }?>><span></span></div>
                        N/A
                    </label>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '94';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>

                <p><?php echo $thequest; ?>
                    <i class="text-primary fa fa-info-circle info-homebiz <?php echo ($thetip == '') ? 'hidden' : '' ?>" data-toggle="tooltip" data-title="<?php echo $thetip ?>" data-placement="bottom"></i> </label>
                </p>

            </div>
            <div class="col-sm-4">
                <div class="form-group">

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="2_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz2['2_'.$thequest2])) { echo ($homebiz2['2_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="2_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz2['2_'.$thequest2])) { echo ($homebiz2['2_'.$thequest2] == 'No') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="2_<?php echo $thequest2 ?>" value="NA" required="required" <?php if (isset($homebiz2['2_'.$thequest2])) { echo ($homebiz2['2_'.$thequest2] == 'NA') ? 'checked="checked"' : ''; }?>><span></span></div>
                        N/A
                    </label>

                </div>
            </div>
        </div>
