<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3> <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active"> <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">

                <div class="panel panel-default">


                	<div class="panel-heading">



                    	<div  class="panel-title">
                            <h4> <?php echo $title; ?></h4>
						</div>
                    </div>


                    <div class="panel-body">



                				<?php



                        if(isset($user_sess['brokerage']['enable_whitelabel'])) {
                					if($user_sess['brokerage']['enable_whitelabel'] == 'Y') { ?>

                                <div class="alert alert-info">
                                  <?php
                                    foreach($prod_q_products as $u_q_product=>$user_q_product){
                                      if(in_array($user_q_product['id'], $prod_prod_id)){
                                  ?>

                                    <p>
                                      Share this <?php echo $user_q_product['product_name'] ?> link to your customers: <br />
                                      <a class="text-success" href="<?php echo base_url().'landing/brokerform/'.$user['id'].'/'.$user_q_product['id'] ?>" target="_blank">
                                        <?php echo base_url().'landing/brokerform/'.$user['id'].'/'.$user_q_product['id'] ?>
                                      </a>
                                    </p>


                                  <?php } } ?>

                                </div>
                        <?php } } ?>


                    </div>

                </div>

            </div>


        </div><!--.row-->



</div>
