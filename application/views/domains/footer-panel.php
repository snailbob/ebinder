</div><!-- the_main_container -->

    <!-- Modal -->
    <!-- <div class="modal fade" id="myLoadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
            <h1 class="text-center" style="margin-top: 100px">
                <i class="fa fa-spinner fa-spin"></i><br />
                <small>Loading..</small>
            </h1>
      </div>
    </div> -->


    <div class="loading style-2">
      <div class="loading-wheel">
        <i class="fa fa-square-o text-primary fa-spin fa-3x"></i>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myLoadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content text-center" style="background-color: transparent; border: none; box-shadow: none; padding: 150px 0;">

    		<i class="fa fa-square-o text-primary fa-spin fa-3x"></i>
        </div>
      </div>
    </div>


    <?php
      if($this->session->userdata('id')){
        $id = $this->session->userdata('id');

        $user = $this->common->customer_format($id);

        $brokerage = $this->master->getRecords('brokerage', array('id'=>$user['brokerage_id']));

        $countries = $this->common->country_format();

    ?>



        <!-- Modal -->
        <div class="modal fade" id="myLinkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Share the Link</h4>
              </div>
              <div class="modal-body">


            				<?php
                    $user_session = $this->session->all_userdata();

                    if(isset($user_sess['brokerage']['enable_whitelabel'])) {
            					if($user_sess['brokerage']['enable_whitelabel'] == 'Y') { ?>
                            <div class="alert alert-info">
                            	Share this link to your customers: <br />
                                <a class="text-success" href="<?php echo base_url().'landing/brokerform/'.$user['id'] ?>" target="_blank">
                                	<?php echo base_url().'landing/brokerform/'.$user['id'] ?>
                                </a>
                            </div>
                    <?php } } ?>


              </div>
            </div>
          </div>
        </div>


        <!-- Portfolio Modals -->
        <div class="portfolio-modal modal fade" id="myProfileModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="modal-body">

                                <h1>My Profile</h1>
                                <hr class="star-primary">


                                <p class="text-center">


                                		<a href="#" onclick="$('#myfile').click(); return false;" title="click to update avatar">

                                     <img src="<?php echo $this->common->avatar($this->session->userdata('id'), 'customer') ?>" class="img-page-preview img-circle img-thumbnail" alt="avatar" style="width: 146px;"/>

                                   </a><br /><span class="small text-muted">150px minimum size recommended</span>

                                </p>





                                <form id="customer_details_form" novalidate="novalidate" class="text-left">
                                    <input type="hidden" name="id" value="<?php echo $user['id'] ?>" />


                                    <div class="row row-prime-contact">


                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" class="form-control" name="first" placeholder="First Name" value="<?php echo $user['first_name']?>" required="required"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text" class="form-control" name="last" placeholder="Last Name" value="<?php echo $user['last_name']?>" required="required"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row row-prime-contact">

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Position</label>
                                                <input type="text" placeholder="Position" class="form-control" name="domain" value="<?php echo $user['position']?>" required="required"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" placeholder="Email" class="form-control" name="email" value="<?php echo $user['email']?>" readonly="readonly"/>
                                            </div>
                                        </div>

                                    </div>



                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Country Calling Code</label>
                                                <select class="form-control country_code_dd" name="country_code" data-live-search="true" required="required">
                                                    <option value="" data-code="">Select..</option>
                                                    <?php
                                                        foreach($countries as $r=>$mc){
                                                            echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';

                                                            if($user['country_id'] == $mc['country_id']){
                                                                echo ' selected="selected"';
                                                            }


                                                            echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group mobile_input">
                                                  <label>Mobile number</label>
                                                  <span class="mobile-icon">
                                                    <i class="fa fa-mobile-phone fa-2x"></i>
                                                  </span>
                                                  <input type="text" class="form-control mobile_number" placeholder="Mobile Number" name="mobile" autocomplete="nope" value="<?php echo $user['calling_digits'] ?>" required>

                                                <input type="hidden" name="country_short" value="<?php echo $user['calling_code'] ?>"/>
                                            </div>
                                        </div>

                                    </div><!--phone number-->




                                    <div class="form-group text-right">
                                        <a href="#" data-dismiss="modal" class="btn btn-cancel btn-grey">Cancel</a>
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>


                                </form>







                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div><!--portfolio-modal-->


         <div class="row hidden">
          <form id="myFormAvatar" action="<?php echo base_url().'settings/update_avatar' ?>" method="post" enctype="multipart/form-data">
               <input type="file" size="60" id="myfile" name="myfile" class="myfile-profile" onchange="$('#myFormAvatar').submit();">
               <input type="hidden" id="avatar_name" value="">
               <input type="hidden" id="pagetype" value="avatar">
               <input type="submit" class="hidden" value="Ajax File Upload">
           </form>
          </div>



        <!-- Modal -->
        <div class="modal fade" id="cropImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <p class="lead">Crop</p>

                <div class="bootstrap-modal-cropper" id="customer_img"><img src="<?php echo base_url().'assets/img/avatar/background.png' ?>" class="img-responsive"/></div>



              </div>
              <div class="modal-footer">
                <a class="btn btn-primary sav_crop">Save changes</a>
              </div>
            </div>
          </div>
        </div>


        <!-- Portfolio Modals -->
        <div class="portfolio-modal modal fade" id="settingsBrokerageModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="modal-body">

                                <h1>Brokerage</h1>
                                <hr class="star-primary">


                                <form id="brokerage_form" novalidate="novalidate" class="text-left">
                                    <input type="hidden" name="id" value="<?php echo (isset($brokerage[0]['id'])) ? $brokerage[0]['id'] : ''?>"  />
                                    <input type="hidden" name="broker_id" value="<?php echo $user['id']?>"  />

                                    <div class="row">
                                        <div class="col-sm-12">
                                          <p>Broker code: <i><?php echo $this->common->brokercode($user['id']) ?></i></p>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="text" placeholder="Business Name" class="form-control" name="company_name" required="required" value="<?php echo (isset($brokerage[0]['company_name'])) ? $brokerage[0]['company_name'] : ''?>" />
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="text" placeholder="Floor/Suite number" class="form-control" name="suite_number" value="<?php echo $user['suite_number']?>" required="required"/>
                                            </div>
                                        </div>



                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="text" class="form-control user_geolocation" name="address" required="required" value="<?php echo (isset($brokerage[0]['address'])) ? $brokerage[0]['address'] : ''?>"/>
                                                <input type="hidden" name="lat"  value="<?php echo (isset($brokerage[0]['lat'])) ? $brokerage[0]['lat'] : ''?>"/>
                                                <input type="hidden" name="lng"  value="<?php echo (isset($brokerage[0]['lng'])) ? $brokerage[0]['lng'] : ''?>" />
                                            </div>
                                        </div>
                                        <div class="col-sm-4 hidden">
                                            <div class="form-group">
                                                <label>Company Domain</label>
                                                <input type="text" class="form-control" name="domain"/>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="hidden row row-prime-contact">

                                        <div class="col-sm-12">
                                            <p><strong>Broker Super Administrator</strong></p>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="first" placeholder="First Name" value="<?php echo $user['first_name']?>"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" name="last" placeholder="Last Name" value="<?php echo $user['last_name']?>"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hidden row row-prime-contact">

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <input type="email" placeholder="Email" class="form-control" name="email" value="<?php echo $user['email']?>" readonly="readonly"/>
                                            </div>
                                        </div>

                                    </div>



                                    <div class="row hidden">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label class="control-label">Country Calling Code</label>
                                                <select class="form-control country_code_dd" name="country_code" data-live-search="true" required="required">
                                                    <option value="" data-code="">Select..</option>
                                                    <?php
                                                        foreach($countries as $r=>$mc){
                                                            echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';

                                                            if($user['country_id'] == $mc['country_id']){
                                                                echo ' selected="selected"';
                                                            }


                                                            echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="form-group mobile_input">
                                                  <label>Mobile number</label>
                                                  <span class="mobile-icon">
                                                    <i class="fa fa-mobile-phone fa-2x"></i>
                                                  </span>
                                                  <input type="number" class="form-control mobile_number" placeholder="Mobile Number" name="mobile" autocomplete="nope" value="<?php echo $user['calling_digits'] ?>" required>


                                                <input type="hidden" name="country_short" value="<?php echo $user['calling_code'] ?>"/>
                                            </div>
                                        </div>

                                    </div><!--phone number-->






                                    <div class="row hidden">
                                        <div class="col-sm-12">
                                            <p><strong>White labelling</strong></p>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">

                                                <input type="checkbox" name="enable_whitelabel" data-on-text="Yes" data-off-text="No" data-handle-width="70" <?php if(isset($brokerage[0]['enable_whitelabel'])) { echo ($brokerage[0]['enable_whitelabel'] == 'Y') ? 'checked="checked"' : ''; } ?> value="Y">

    <!--
                                                <label class="radio-inline">
                                                  <input type="radio" name="enable_whitelabel" id="inlineRadio1" value="Y" required="required"> Yes
                                                </label>
                                                <label class="radio-inline">
                                                  <input type="radio" name="enable_whitelabel" id="inlineRadio2" value="N" required="required"> No
                                                </label> -->

                                            </div>
                                        </div>
                                    </div>


                                    <?php /*?><div class="form-group">
                                        <p class="text-center"><small>or</small></p>
                                        <label>Select Existing Broker</label>
                                        <select class="form-control" name="broker_id">
                                            <option value="">Select</option>
                                            <?php
                                                if(count($the_brokers) > 0){

                                                    foreach($the_brokers as $r=>$value){
                                                        echo '<option value="'.$value['id'].'">'.$value['first_name'].' '.$value['last_name'].'</option>';
                                                    }
                                                }
                                            ?>
                                        </select>
                                    </div><?php */?>

                                    <div class="form-group text-right">
                                      <a href="" data-dismiss="modal" class="btn btn-cancel btn-grey">Cancel</a>
                                      <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>


                                </form>









                              </div>
                          </div>
                      </div>
                  </div>


              </div>
          </div><!--portfolio-modal-->




    <?php } //my profile settings modal ?>







      <ul class="map-details hidden">
        <li>Location: <span data-geo="location"></span></li>
        <li>Route: <span data-geo="route"></span></li>
        <li>Street Number: <span data-geo="street_number"></span></li>
        <li>Postal Code: <span data-geo="postal_code"></span></li>
        <li>Locality: <span data-geo="locality"></span></li>
        <li>Country Code: <span data-geo="country_short"></span></li>
        <li>State: <span data-geo="administrative_area_level_1"></span></li>
      </ul>





    <script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/browserDetection.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.fittext.js"></script>
    <script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyDVL8WaKGrSPhJ7ZY8XHrJeWascHtNA0qc"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="<?php echo base_url() ?>assets/plugins/geocomplete/jquery.geocomplete.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/pwstrength/pwstrength-bootstrap.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/datatables-bs3.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/validate/jquery.validate.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/validate/additional-methods.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bootbox/bootbox.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/gridstack/underscore.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/gridstack/gridstack.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/gridstack/gridstack.jQueryUI.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/jquery.form/jquery.form.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery.maskedinput.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/dependencies/moment.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bs-switch/js/bootstrap-switch.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/summernote/js/summernote.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap-switch/js/bootstrap-switch.js"></script>




    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/cropper/dist/cropper.min.js"></script>
    <?php /*?><script src="<?php echo base_url() ?>assets/plugins/cropper/js/crop-avatar.js"></script><?php */?>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/silviomoreto-bootstrap-select/js/bootstrap-select.min.js"></script>




<script type="text/javascript" src="<?php echo base_url() ?>assets/procurify/js/ebinder-custom-script.js"></script>


<?php if($this->uri->segment(3) == 'payment_method'){ ?>

<script src="https://js.braintreegateway.com/js/braintree-2.26.0.min.js"></script>

<script>
  braintree.setup(
	// Replace this with a client token from your server
	'<?php echo $client_token ?>',
	'dropin', {
		container: 'payment-form',
		paymentMethodNonceReceived: function (event, nonce) {
			$('#payment-form').append("<input type='hidden' name='payment_method_nonce' value='" + nonce + "'></input>");
			console.log(nonce);
			$('#payment-form').find('[type="button"]').button('loading');
			$.post(
				base_url+'formsubmits/save_paymentmethod',
				{nonce: nonce},
				function(res){
					console.log(res);
					bootbox.alert(res.message, function(){
						window.location.reload(true);
					});
				},
				'json'
			).error(function(err){
				console.log(err);
			});
		}
	});
</script>


<?php } ?>

<?php if($this->uri->segment(2) == 'social_login' && $this->uri->segment(1) == 'settings'){ ?>
	<script src="https://apis.google.com/js/api:client.js"></script>

	<script>


  		if(uri_1 == 'settings' && uri_2 == 'social_login'){

  			/*
        $(document).ready(function(e) {
          $('.google-auth-btn').on('click', function(){
            $('#customBtn').click();
          });
          $('.linkedin-auth-btn').on('click', function(){
            $('#MyLinkedInButton').click();
          });

        });
  			 */

  		  var googleUser = {};
  		  var startApp = function() {
  			gapi.load('auth2', function(){
  			  // Retrieve the singleton for the GoogleAuth library and set up the client.
  			  auth2 = gapi.auth2.init({
  				client_id: '253167228155-mnp9rtvui3t45cben3u1vag21n4vcupk.apps.googleusercontent.com',
  				cookiepolicy: 'single_host_origin',
  				// Request scopes in addition to 'profile' and 'email'
  				//scope: 'additional_scope'
  			  });
  			  attachSignin(document.getElementById('customBtn'));
  			});
  		  };

  		  function attachSignin(element) {
  			console.log(element.id);
  			auth2.attachClickHandler(element, {},
  				function(googleUser) {
  					var profile = googleUser.getBasicProfile();
  					console.log("ID: " + profile.getId()); // Don't send this directly to your server!
  					console.log('Full Name: ' + profile.getName());
  					console.log('Given Name: ' + profile.getGivenName());
  					console.log('Family Name: ' + profile.getFamilyName());
  					console.log("Image URL: " + profile.getImageUrl());
  					console.log("Email: " + profile.getEmail());

  					var $mdl = $('#myLoadingModal');
  					$mdl.modal('show');

  					var data = {
  						id: profile.getId(),
  						full_name: profile.getName(),
  						first_name: profile.getGivenName(),
  						last_name: profile.getFamilyName(),
  						avatar: profile.getImageUrl(),
  						email: profile.getEmail(),

  					};
  					console.log(data);
  					$.post(
  						base_url+'formsubmits/googleplus',
  						{data: data, uri_1: uri_1, id: "<?php echo (isset($user_sess['id'])) ? $user_sess['id'] : ''?>"},
  						function(res){
  							console.log(res);
  							//return false;

  							if(res.type == 'signup'){
  								bootbox.alert('User not found.');
  								// var d = new Date();
  								// var n = d.getMilliseconds();
  								//
  								// window.location.href = base_url + '?n=' + n + '#signup';
  							}
  							else if(res.type == 'complete_signup'){
  								$('#customBtn').html('<i class="fa fa-google-plus text-danger"></i> Connnected to Google+').addClass('disabled');
  								bootbox.alert(action_messages.success.complete_profile_via_social);
  							}
                else if(res.type == 'connect'){
                  $('#customBtn').html('<i class="fa fa-google-plus text-danger"></i> Connnected to Google+').addClass('disabled');
                  bootbox.alert('Google+ account successfully connected.');
                }
  							else{
  								if(res.userdata.super_admin == 'Y'){
  									window.location.href = base_url+'webmanager';
  									console.log(base_url+'webmanager');
  								}
  								else{
  									window.location.href = base_url+'panel'; //'http://'+res.userdata.domain+'.ebinder.com.au/panel';
  									console.log(base_url+'panel');
  								}


  							}
  						},
  						'json'
  					).done(function(res){
  						$mdl.modal('hide');
  					}).error(function(err){
  						console.log(err);
  					});



  	//			  document.getElementById('name').innerText = "Signed in: " +
  	//				  googleUser.getBasicProfile().getName();
  				}, function(error) {
  				  alert(JSON.stringify(error, undefined, 2));
  				});
  		  }

  		  startApp();
  		}

      </script>


      <script type="text/javascript" src="//platform.linkedin.com/in.js">
          api_key: 8198kchnjgi54x
          authorize: true
          onLoad: onLinkedInLoad
      </script>

      <script type="text/javascript">
  		if(uri_1 == 'settings' && uri_2 == 'social_login'){
  			// Setup an event listener to make an API call once auth is complete
  			function onLinkedInLoad() {
  				IN.Event.on(IN, "auth", getProfileData);
  			}

  			// Use the API call wrapper to request the member's basic profile data
  			function getProfileData() {
  				if(IN.User.isAuthorized()){
  					IN.API.Raw("people/~:(id,first-name,last-name,location,email-address)?format=json").result(onSuccess).error(onError);
  				}
  			}


  			var linkedData = {};
  			var linked_fetch = 0;
  			// Handle the successful return from the API call
  			function onSuccess(profile) {
  				var linked = $("#MyLinkedInButton").data('linked');

  				//$('#MyLinkedInButton').addClass('disabled');

  				var data = {
  					id: profile.id,
  					full_name: profile.firstName+' '+profile.lastName,
  					first_name: profile.firstName,
  					last_name: profile.lastName,
  					avatar: '',
  					email: profile.emailAddress
  				};

  				console.log('onLinkedInLoad', profile);
  				console.log(data);
  				linkedData = data;

  				if(typeof(linked) !== 'undefined'){
  					var $mdl = $('#myLoadingModal');
  					$mdl.modal('show');

  					$.post(
  						base_url+'formsubmits/linkedin',
  						{data: data, uri_1: uri_1, id: "<?php echo (isset($user_sess['id'])) ? $user_sess['id'] : ''?>"},
  						function(res){
  							console.log(res);

  							if(res.type == 'signup'){
  								bootbox.alert('User not found.');
  								// var d = new Date();
  								// var n = d.getMilliseconds();
  								//
  								// window.location.href = base_url + '?n=' + n + '#signup';
  							}
  							else if(res.type == 'complete_signup'){
  								$('#MyLinkedInButton').html('<i class="fa fa-linkedin"></i> Connnected to LinkedIn').addClass('disabled');
  								bootbox.alert(action_messages.success.complete_profile_via_social);
  							}
  							else if(res.type == 'connect'){
  								$('#MyLinkedInButton').html('<i class="fa fa-linkedin"></i> Connnected to LinkedIn').addClass('disabled');
  								bootbox.alert('LinkedIn account successfully connected.');
  							}

  							else{
  								if(res.userdata.super_admin == 'Y'){
  									window.location.href = base_url+'webmanager';
  									console.log(base_url+'webmanager');
  								}
  								else{
  									window.location.href = base_url+'panel'; //'http://'+res.userdata.domain+'.ebinder.com.au/panel';
  									console.log(base_url+'panel');
  								}

  							}
  						},
  						'json'
  					).done(function(res){
  						$mdl.modal('hide');
  					}).error(function(err){
  						console.log(err);
  					});

  				}
  				linked_fetch ++;


  			}

  			// Handle an error response from the API call
  			function onError(error) {
  				console.log(error);
  			}

  			$(document).ready(function(e) {

  				$("#MyLinkedInButton").on('click',function () {

  					console.log('MyLinkedInButton', linkedData);
  					var data = linkedData;
  					var $self = $(this);
  					$self.data('linked',data);

  					if(typeof(linkedData.id) !== 'undefined'){
  						var $mdl = $('#myLoadingModal');
  						$mdl.modal('show');

  						$.post(
  							base_url+'formsubmits/linkedin',
  							{data: data, uri_1: uri_1, id: "<?php echo (isset($user_sess['id'])) ? $user_sess['id'] : ''?>"},
  							function(res){
  								console.log(res);

  								if(res.type == 'signup'){
  									bootbox.alert('User not found.');
  									// var d = new Date();
  									// var n = d.getMilliseconds();
  									//
  									// window.location.href = base_url + '?n=' + n + '#signup';
  								}
  								else if(res.type == 'complete_signup'){
  									$('#MyLinkedInButton').html('<i class="fa fa-linkedin"></i> Connnected to LinkedIn').addClass('disabled');
  									bootbox.alert(action_messages.success.complete_profile_via_social);
  								}
  								else if(res.type == 'connect'){
  									$('#MyLinkedInButton').html('<i class="fa fa-linkedin"></i> Connnected to LinkedIn').addClass('disabled');
                    bootbox.alert('LinkedIn account successfully connected.');
  								}

  								else{
  									if(res.userdata.super_admin == 'Y'){
  										window.location.href = base_url+'webmanager';
  										console.log(base_url+'webmanager');
  									}
  									else{
  										window.location.href = base_url+'panel'; //'http://'+res.userdata.domain+'.ebinder.com.au/panel';
  										console.log(base_url+'panel');
  									}

  								}
  							},
  							'json'
  						).done(function(res){
  							$mdl.modal('hide');
  						}).error(function(err){
  							console.log(err);
  						});

  					}

  					return false;
  				});

  				$("#MyLinkedInButton").bind('click',function () {
  					IN.User.authorize();
  					//
  					// IN.Event.on(IN, 'auth', function(){
  					// 	/* your code here */
  					// 	//for instance
  					// 	console.log('auth success');
  					// 	IN.API.Raw("people/~:(id,first-name,last-name,location,email-address)?format=json").result(function(res){
  					// 		console.log('peps', res);
  					// 	}).error(onError);
  					// });

  					return false;
  				});


  			});
  		}

      </script>

<?php } ?>




</body>
</html>
