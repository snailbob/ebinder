

  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">

      <div class="row">


        <div class="col-lg-12">



        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>
        <?php } ?>

        <div class="module_header">
            <i class="fa fa-file-text fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
        </div>

        <div class="panel">
        	<div class="panel-body">

                        <?php if(count($q_products) > 0) { ?>


						<div class="table-responsivex">

                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <th width="80%">Name</th>
                              <th>Date &amp; Time</th>
                            </tr>
                          </thead>
                          <tbody>

                        <?php
							$count = 1;
							foreach($q_products as $u=>$value){
						?>

                            <tr>

                              <td class="hidden"><?php echo $count; ?></td>
                              <td><?php echo $this->common->customer_name($value['broker_id']) ?></td>

                              <td><?php echo date_format(date_create($value['date_updated']), 'd M Y, h:i a'); ?></td>

                            </tr>

                        <?php
								$count++;
							}
						?>



                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">No '.$title.'</p></div>';
						}?>


            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->







    <!-- Modal -->
    <div class="modal fade in" id="myActivityLogModal" data-logged="<?php echo $this->session->userdata('activity_logged') ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Re-enter Password</h4>
          </div>
          <form id="password_form" novalidate="novalidate">
              <div class="modal-body">

                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" value="" required="required"/>
                </div>


              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
          </form>
        </div>
      </div>
    </div>
