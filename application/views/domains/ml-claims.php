

  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">

      <div class="row">


        <div class="col-lg-12">



        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>
        <?php } ?>


        <div class="module_header">
            <i class="fa fa-user fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <ul class="module_actions right">
                <li>
                    <a class="module_action btn--red" href="<?php echo base_url() ?>panel/claims/add">
                        <div class="module_action_icon">
                            <i class="fa fa-plus-square"></i>
                        </div>
                        <span class="module_action_text text-uppercase"> Add <?php echo $singular ?></span>
                    </a>
                </li>
            </ul>

        </div>



        <div class="panel panel-default">

          <div class="panel-heading">
              <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> <?php echo $singular_title; ?></a>

            <div  class="panel-title">
                  <h4>Manage <?php echo $title; ?></h4>
            </div>
          </div>



        	<div class="panel-bodyx">

                        <?php if(count($claims) > 0) { ?>

                        <div class="table_search">
                            <input type="text" class="form-control input_table_search" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>


						<div class="table-responsivex">

                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <th>ID</th>
                              <th>Details</th>
                              <th>Customer</th>
                              <th>Status</th>
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>

                        <?php
							$count = 1;
							foreach($claims as $u=>$value){

						?>

                            <tr>

                              <td class="hidden"><?php echo $count; ?></td>
                              <td><?php echo $this->common->format_id($value['id'], 'C') ?></td>

                              <td><?php echo $value['details'] ?></td>

                              <td><?php echo $this->common->customer_name($value['customer_id']) ?></td>

                              <td><?php echo $this->common->claim_status($value['status']) ?></td>

                              <td>

                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">

                                        <li><a href="<?php echo base_url().'panel/claim/'.$value['id']?>" data-id='<?php echo $value['id'];?>'>Details</a></li>

                                        <li><a href="javascript:;" class="delete_btn" data-id="<?php echo $value['id']?>" data-table="claims">Delete</a></li>

                                      </ul>
                                    </div>


                              </td>
                            </tr>

                        <?php
								$count++;
							}
						?>



                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';
						}?>


            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->






    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

                            <h1>Claim</h1>
                            <hr class="star-primary">


                    				<form novalidate="novalidate" class="text-left" id="claim_form">
                                <div class="form-group hidden">
                                    <label>New claim</label>
                                    <input type="text" class="form-control" placeholder="New claim" name="claim" value=""/>
                                </div>

                                <div class="form-group">
                                    <label>Select customer</label>
                                    <select class="form-control selectpicker" data-live-search="true" name="customer_id" required="required">
                                        <option value="" data-name="" data-inputs='[]'>Select</option>
                                        <?php
                                            if(count($customers) > 0){
                                                foreach($customers as $r=>$value){
                                                    echo '<option value="'.$value['id'].'">'.$value['customer_info_format']['company_name'].'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Select policy</label>
                                    <select class="form-control selectpicker" data-live-search="true" name="policy_id" required="required">
                                        <option value="" data-name="" data-inputs='[]'>Select</option>
                                        <?php
                                            if(count($biz_referral) > 0){
                                                foreach($biz_referral as $r=>$value){
                                                    echo '<option value="'.$value['id'].'">'.$value['policy_id'].' - '.$value['product_name'].'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Claim details</label>
                                    <textarea class="form-control" rows="4" name="details" placeholder="Claim details" required="required"></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Upload any documents (if applicable)</label><br />

                                    <a href="javascript:;" id="dragandrophandler" onclick="$('#myForm').find('#myfile').click()" class="btn btn-default btn-block btn-upload"><i class="fa fa-paperclip fa-3x"></i><br>Choose a File or Drag it here</a>
                                  </div>

                                  <div class="form-group">

                                    <a class="btn btn-default disabled" id="progress" style="display: none;">
                                        <div id="bar"></div>
                                        <div id="percent">0%</div>
                                    </a>
                                </div>


                                <div class="form-group">

                                    <div class="col-xs-12">

                                    	<div id="file_up_text" style="display: none;">File Attached: <div id="message"><a href=""></a></div></div>

                                    </div>

                                </div>


                                <hr />

                                <div class="form-group text-right">
                                    <a href="<?php echo base_url().'panel/claims' ?>" class="btn btn-default btn-cancel btn-grey">Cancel</a>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>



                             <div class="row upload_sec hidden">
                                 <div class="col-xs-6">
                                   <form id="myForm" action="<?php echo base_url().'formsubmits/upload_file' ?>" method="post" enctype="multipart/form-data">
                                     <input type="file" size="60" id="myfile" name="myfile" onchange="$('#myForm').submit();">
                                     <input type="hidden" id="uniquetime" name="uniquetime" value="<?php echo $uniquetime; ?>">
                                     <input type="submit" class="hidden" value="Ajax File Upload">
                                   </form>
                                 </div>
                             </div>



                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->
