

  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">

      <div class="row">


        <div class="col-lg-12">



        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
    <div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
          <?php echo $success.$danger.$info ?>
        </div>
        <?php } ?>


        <div class="module_header">
            <i class="fa fa-user fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <ul class="module_actions right">
                <li>
                    <a class="module_action btn--red" href="#" onclick="$('#broker_form').find('.form-control').val('')" data-toggle="modal" data-target="#myModal" >
                        <div class="module_action_icon">
                            <i class="fa fa-user-plus"></i>
                        </div>
                        <span class="module_action_text text-uppercase"> Add <?php echo $singular ?></span>
                    </a>
                </li>
            </ul>

        </div>


        <div class="panel panel-default">

          <div class="panel-heading">
              <a href="#myModal" class="btn btn-primary pull-right add_customer_btn"><i class="fa fa-plus"></i> <?php echo $singular_title; ?></a>



            <div  class="panel-title">
                  <h4>Manage <?php echo $title; ?></h4>
            </div>
          </div>




          <?php if(count($customers) > 0) { ?>

                        <div class="table_search">
                            <input type="text" class="form-control input_table_search" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>

          <div class="panel-bodyx">

                <table class="table table-striped table-hover table-datatable">
                  <thead>
                    <tr>
                      <th class="hidden">#</th>
                      <th>Reference</th>
                      <th>Date Added</th>
                      <th>Organisation</th>
                      <th>Floor/Suite</th>
                      <th>Address</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
            $count = 1;
            foreach($customers as $r=>$value){
              $value['customer_info'] = (@unserialize($value['customer_info'])) ? unserialize($value['customer_info']) : array();
              // echo json_encode($value['customer_info']);
              if($value['id'] != $user['id']){
              ?>

                        <tr class="<?php echo ($count == 1 && strpos($this->session->flashdata('success'), 'added') !== false) ? 'flash_animation' : '' ?>">
                          <td class="hidden"><?php echo $count?></td>
                          <td><?php echo $this->common->brokercode($value['studio_id']).'-'.$this->common->customer_format_id($value['id'])?></td>
                          <td><?php echo date_format(date_create($value['date_added']), 'd-m-Y')?></td>
                          <td><a href="javascript:;" class="customer2-update" data-id="<?php echo $value['id'] ?>" data-arr='<?php echo json_encode($value) ?>'><?php echo (isset($value['customer_info']['company_name'])) ? $value['customer_info']['company_name'] : '' ?></a></td>

                          <td><?php echo (isset($value['customer_info']['suite_number'])) ? $value['customer_info']['suite_number'] : '' ?></td>
                          <td><?php echo (isset($value['customer_info']['address'])) ? $value['customer_info']['address'] : '' ?></td>


                          <td>

                                <!-- Single button -->
                                 <div class="btn-group pull-right">
                                     <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown">
                                       Action <span class="caret"></span>
                                     </button>
                                     <ul class="dropdown-menu" role="menu">



                                          <li><a href="javascript:;" class="customer2-update" data-id="<?php echo $value['id'] ?>" data-arr='<?php echo json_encode($value) ?>'>Update</a></li>
                                           <!--<li><a href="javascript:;" onclick="customerDetails('<?php echo $value['id']; ?>');">Details</a></li>
                                            <li><a href="<?php echo base_url().'panel/add_customer_quote/'.$value['id'] ?>/">New Quote</a></li> -->
                                           <li class="divider"></li>
                                           <li><a href="javascript:;" class="delete_btn" data-id="<?php echo $value['id']?>" data-table="customers">Delete Permanently</a></li>


                                     </ul>
                                   </div>


                          </td>
                        </tr>




            <?php
              $count++;
              }
            }
            ?>


                  </tbody>
                </table>



                    <?php } else { echo '<div class="panel-body"> <p class=" text-muted text-center">Nothing to show you.</p></div>'; } ?>


            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->





    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">

            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>




            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

                          <h1><?php echo $is_edit.' '; echo $singular_title; ?></h1>
                          <hr />






                          <form id="customer2_form" class="text-left">
                              <input type="hidden" name="id" value="<?php echo (isset($customer_details['id'])) ? $customer_details['id'] : ''?>" />
                              <input type="hidden" name="thenum" value="<?php echo $thepage; ?>" />
                              <input type="hidden" name="broker_id" value="<?php echo $this->session->userdata('id')?>" />
                              <input type="hidden" name="utype" value="Y"/>


                              <?php
                                $questionnaire = (isset($questionnaire)) ? $questionnaire : array();
                                $this->load->view($quote_view, $questionnaire);
                              ?>



                              <div class="form-group hidden">
                                  <label><?php echo $singular_title; ?> Type</label><br />
                                  <label>
                                    <input type="text" name="ctype" value="Y"/>

                                  </label>
                              </div>



                              <div class="form-group text-right">
                                  <a class="btn btn-default btn-grey btn-cancel <?php echo ($thepage == '1') ? 'hidden' : '' ?>" href="<?php echo base_url().'panel/use_customer?tab='.$theindex.'#quote'?>">Back</a>

                                  <a class="btn btn-default btn-grey btn-cancel <?php echo ($thepage != '1' && $thepage != '') ? 'hidden' : '' ?>" href="#" data-dismiss="modal">Cancel</a>

                                  <?php if(count($themenu) > 5 && ($thepage == '5' || $thepage == '6') ){ } else{ ?>
                                    <button class="btn btn-primary" type="submit"><?php echo ($thepage == '5') ? 'Submit' : 'Submit'?></button>
                                  <?php } ?>
                              </div>

                          </form>




                        </div>
                    </div>
                </div>
            </div>










          	<div class="the-sidebars">


                  <div class="container-fluid hidden-sm hidden-xs">
                      <div class="row">

                  	<div class="col-sm-5 col-md-3 col-lg-2 leftbar-still">

                          <nav>
                          <div class="lifecycle">

                              <ul class="list">


                                  <?php



                                      $menulength = count($themenu) - 1;
                                      foreach($themenu as $r=>$value){ ?>

                                        <li class="item <?php echo ($thepage == ($r + 1) || !empty($value['data'])) ? ' current' : ''; echo ($r == 0) ? ' start' : ''; echo ($r == $menulength) ? ' end' : ''; echo (!empty($value['data'])) ? ' complete' : '' ?>">
                                            <?php echo (!empty($value['data']) || $thepage == ($r + 1) || $menulength > 5) ? '<a class="" href="?tab='.($r + 1).'#quote">' : '<div>'; ?>
                                                <div class="indicator">
                                                    <div class="semiline"></div>
                                                    <div class="semiline"></div>
                                                    <div class="circle wow animated zoomIn">
                                                      <?php echo (!empty($value['data']) && $thepage != ($r + 1)) ? '<i class="fa fa-check text-white"></i>' : '' ?>
                                                    </div>
                                                </div><span><?php echo $value['name']?></span>
                                            <?php echo (!empty($value['data']) || $thepage == ($r + 1) || $menulength > 5) ? '</a>' : '<div>'; ?>
                                        </li>



                                      <?php

                                      }
                                  ?>


                              </ul>
                          </div>

                          </nav>
                      </div><!--.col-sm-5-->

                      </div>
                  </div>



              </div>










        </div>
    </div><!--portfolio-modal-->

<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $singular; ?> Information</h4>
      </div>
      <div class="modal-body">
        <div class="customer_info">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
