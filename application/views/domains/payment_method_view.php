
    <!-- CONTENT -->
    <div class="l-app-container l-content-container">
        
<div>
	<div notifications></div>

    <!--==================================*\
    # Templates goest here - html
    \*===================================-->
	<div ui-view>
        <div class="route-loader hidden"></div>




		<div class="container">
            <div class="row">
                <div class="col-sm-12">
                            
                    <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
                    <div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
                        <?php echo $success.$danger.$info ?>
                    </div>      
                    <?php } ?>
                    
                    <div class="module_header">
                        <i class="fa fa-gear fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
                    </div>
                
        
                    <div class="panel panel-border-grey">
                            
                            <div class="row">
                                <div class="col-sm-4 col-md-3">
                                    
                                    <div class="list-group list-group-rabbit">
                                      <a href="<?php echo base_url().'panel/setup/profile'?>"class="list-group-item">
                                        Profile
                                      </a>
                                      <a href="<?php echo base_url().'panel/setup/logo'?>"class="list-group-item  ">
                                        Logo
                                      </a>
                                      <a href="<?php echo base_url().'panel/setup/quote_slip' ?>"class="list-group-item ">
                                        Quote Slip
                                      </a>
                                      <a href="<?php echo base_url().'panel/setup/payment_method' ?>"class="list-group-item active">
                                        Payment Method
                                      </a>
                                    </div>                                
                                </div>
                                <div class="col-sm-8 col-md-9 border-left">
                                    
                                      <div class="padding">
                                                              
                                        <?php if($user['stripe_id'] == ''){ ?>
                                    
                                        <form id="checkout" method="post">
                                          <div id="payment-form" style="margin-bottom: 15px;"></div>
                                          <input type="submit" class="btn btn-primary" value="Submit">
                                        </form>
                                        <?php } else { ?>
                                        <div class="alert alert-success">
                                            Payment have successfully added. Your first 5 customers are free. 
                                        </div>
                                        <?php } ?>


                                      </div>
                                
                                </div>
                            </div>
                            
                    </div>
            
                </div>
            </div>
        </div>                                      
        
        
        
    </div>
</div>

    </div>

    <!-- End of Contents -->
    