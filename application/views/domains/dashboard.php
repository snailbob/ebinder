

  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">

      <div class="row">


        <div class="col-lg-12">


<?php /*?>        <div class="onboarding-banner ng-isolate-scope onboarding-banner--dashboard" ng-class="banner_class" style="margin-bottom: 15px;">
            <div class="onboarding-banner_close">
                <label class="checkbox mr20">
                    <input ng-model="stay_hidden" type="checkbox" class="ng-pristine ng-untouched ng-valid"> Don't show this message again?
                </label>
                <div ng-click="close();" class="onboarding-banner_close_icon">
                    <i class=" fa fa-times fa-fw"></i>
                </div>
            </div>

            <div class="onboarding-banner_content row">
                <div ng-class="{'col-xs-6': type !== 'default' }">
                    <h1 class="mb20 ng-binding">WELCOME TO EBINDER!</h1>
                    <p class="ng-binding">Hello! Let’s get started with eBinder. This is your dashboard to quickly show you the health of your purchasing.</p>
                </div>
                <!-- ngIf: type !== 'default' -->
            </div>
        </div><!--.onboarding-banner-->
<?php */?>

        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>
        <?php } ?>

        <div class="module_header">
            <i class="fa fa-globe fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <?php if($user['customer_type'] == 'N' && $user['studio_id'] == '0') {?>
            <ul class="module_actions right hidden">
                <li>
                    <a class="module_action btn--red" href="/panel/addmember">
                        <div class="module_action_icon">
                            <i class="fa fa-user-plus"></i>
                        </div>
                        <span class="module_action_text text-uppercase"> Add <?php echo $singular ?></span>
                    </a>
                </li>
            </ul>
            <?php } ?>

        </div>

        <h3 class="text-right">
          <a href="#addWidgetModal" id="add-new-widgetx" data-toggle="modal" data-controls-modal="#addWidgetModal" class="btn btn-lgx btn-primary pull-right">
            <i class="fa fa-plus fa-fw"></i> Widget
          </a>

          <div class="form-group pull-right" style="width: 150px; margin-right: 5px;">
              <select class="form-control days_filter">
                  <?php $day = (isset($_GET['day'])) ? $_GET['day'] : 30; ?>
                  <option value="30" <?php echo ($day == 30) ? 'selected="selected"' : ''?>>30 Days</option>
                  <option value="60" <?php echo ($day == 60) ? 'selected="selected"' : ''?>>60 Days</option>
                  <option value="90" <?php echo ($day == 90) ? 'selected="selected"' : ''?>>90 Days</option>
                </select>
            </div>

        </h3>


        <div class="row padding-top-10">
          <div class="col-sm-12 padding-top-10">

                <div class="grid-stack">

                  <?php
                    $gs_x = 0;
                    for($i = 0; $i < count($q_widget); $i++) {
                      $referral = $this->common->format_referral(array('id'=>$q_widget[$i]['referral_id']));
                        //echo json_encode($referral);
                      ?>


                      <div class="grid-stack-item" id="stack-item<?=$i ?>"
                          data-gs-x="<?php echo $gs_x ?>" data-gs-y="0"
                          data-gs-width="4" data-gs-height="2">
                              <div class="grid-stack-item-content panel">

                                    <div class="panel-body">
                                      <button type="button" class="close dismiss-widget" data-target="stack-item<?php echo $i ?>" data-id="<?php echo $q_widget[$i]['id'] ?>" data-dismiss="alertx"> <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                      <?php echo $q_widget[$i]['title']?>

                                      <?php
                                       if($q_widget[$i]['type'] == 'premium'){ ?>
                                        <p style="margin-bottom: 0; margin-top: 15px; font-size: 35px" class="text-strong text-success">
                                          <span class="text-strong"><strong>$<?php echo $total_premium_all?></strong></span><br />
                                        </p>

                                      <?php } else if($q_widget[$i]['type'] == 'topcustomers') {
                                        if(count($widget_topcustomers) > 0){
                                          echo '<ol class="list-unstyled text-muted">';
                                          $tb_count = 1;
                                          foreach($widget_topcustomers as $r=>$value){
                                            echo '<li>'.$tb_count.'. '.$value.'</li>';
                                            $tb_count++;
                                          }
                                          echo '</ol>';
                                        } else{
                                          ?>
                                          <p class="text-muted">Nothing to show you.</p>
                                      <?php
                                        }
                                      } ?>


                                    </div>
                              </div>
                      </div>
                    <?php $gs_x = $gs_x + 4; } ?>

                </div>

            </div>
        </div>




<?php /*?>
        <div class="panel">
        	<div class="panel-body">


            	<?php if(isset($customers)){ ?>
                <table class="table table-striped table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Company</th>
                      <th>Email</th>
                      <th>User Type</th>
                      <th>Status</th>
                      <th>Activated</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php if(count($customers) > 0) {
						$count = 1;
						foreach($customers as $r=>$value){ ?>

                        <tr>
                          <td><?php echo $count?></td>
                          <td><?php echo $value['first_name'].' '.$value['last_name'] ?></td>
                          <td><?php echo $value['business_name']?></td>

                          <td><?php echo $value['email']?></td>
                          <td><?php echo $value['super_admin_text']?></td>
                          <td><?php echo $value['status_text']?></td>
                          <td><?php echo $value['enabled_text']?></td>

                          <td>

                                <!-- Single button -->
                                <div class="btn-group pull-right">
                                  <button type="button" class="btn btn--blue btn--small dropdown-toggle" data-toggle="dropdown">
                                    Action <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" role="menu">

                                    <li><a href="javascript: void(0)" class="deactivate-member" data-enabled="<?php echo $value['enabled']?>" data-id="<?php echo $value['id'] ?>"><?php echo ($value['enabled'] == 'N') ? 'Activate' : 'Deactivate' ?></a></li>
                                  </ul>
                                </div>


                          </td>
                        </tr>




						<?php
						$count++;
						}
						?>
                    <?php } else { echo '<tr><td colspan="8" class="text-center">No '.$singular.'</td></tr>'; } ?>


                  </tbody>
                </table>
                <?php } else { echo '<h2>Welcome to Ebinder, '.$user['first_name'].'!</h2>'; }?>
            </div>
        </div>

<?php */?>
    </div>
</div>

    </div>

    <!-- End of Contents -->



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="addWidgetModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

            							<div class="row" style="margin-top: 100px">

                            	<div class="col-sm-12">
                                	<h1>Add Widget</h1>
                                </div>

                            	<div class="col-sm-12">


                                <form id="widget_form" method="post" novalidate="novalidate">
                                  <div class="row text-left">

                                    <div class="col-md-12">
                                      <div class="form-group">
                                            <div class="btn-group btn-group-justified" data-toggle="buttons">
                                              <label class="btn btn-default" style="padding: 10px; text-align: left">
                                                <span class="pull-right btn-xs">Add</span>
                                                <input type="checkbox" name="widget[]" value="topcustomers">
                                                Top 5 customers
                                              </label>
                                            </div>
                                      </div>
                                    </div>

                                  </div>


                                  <div class="form-group text-right">
                                      <button class="btn btn-default btn-cancel btn-grey" data-dismiss="modal">Cancel</button>
                                      <button class="btn btn-primary" type="submit">Submit</button>
                                  </div>


                                </form>

                            </div>
                        </div>



                    </div>
                </div>
            </div>
        </div>


    </div>
