<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3> <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active"> <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">

                <div class="panel panel-default">

                
                	<div class="panel-heading">
                     


                    	<div  class="panel-title">
                            <h4> <?php echo $title; ?></h4>
						</div>
                    </div>

  
                    <div class="panel-body">


                            <form id="brokerage_form" novalidate="novalidate" class="text-left">
                                <input type="hidden" name="id" value="<?php echo $brokerage[0]['id']?>"  />
                                <input type="hidden" name="broker_id" value="<?php echo $user['id']?>"  />
                            
                                <div class="row">
                                    <div class="col-sm-12">
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="text" placeholder="Business Name" class="form-control" name="company_name" required="required" value="<?php echo $brokerage[0]['company_name']?>" />
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="text" placeholder="Floor/Suite number" class="form-control" name="suite_number" value="<?php echo $user['suite_number']?>" required="required"/>
                                        </div>
                                    </div>



                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="text" class="form-control user_geolocation" name="address" required="required" value="<?php echo $brokerage[0]['address']?>"/>
                                            <input type="hidden" name="lat"  value="<?php echo $brokerage[0]['lat']?>"/>
                                            <input type="hidden" name="lng"  value="<?php echo $brokerage[0]['lng']?>" />
                                        </div>
                                    </div>
                                    <div class="col-sm-4 hidden">
                                        <div class="form-group">
                                            <label>Company Domain</label>
                                            <input type="text" class="form-control" name="domain"/>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                                
                                <div class="hidden row row-prime-contact">

                                    <div class="col-sm-12">
                                        <p><strong>Broker Super Administrator</strong></p>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="first" placeholder="First Name" value="<?php echo $user['first_name']?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="last" placeholder="Last Name" value="<?php echo $user['last_name']?>"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="hidden row row-prime-contact">

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="email" placeholder="Email" class="form-control" name="email" value="<?php echo $user['email']?>" readonly="readonly"/>
                                        </div>
                                    </div>

                                </div>


                                
                                <div class="row hidden">
                                
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Country Calling Code</label>
                                            <select class="form-control country_code_dd" name="country_code" data-live-search="true" required="required">
                                                <option value="" data-code="">Select..</option>
                                                <?php
                                                    foreach($countries as $r=>$mc){
                                                        echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';

                                                        if($user['country_id'] == $mc['country_id']){
                                                            echo ' selected="selected"';
                                                        }
                                                                    

                                                        echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                
                                    <div class="col-sm-6">
                                        <div class="form-group mobile_input">
                                            <label class="control-label">&nbsp;</label>
                                              <input type="number" class="form-control" placeholder="Contact Number" name="mobile" autocomplete="nope" value="<?php echo $user['calling_digits'] ?>" required>

                                            <input type="hidden" name="country_short" value="<?php echo $user['calling_code'] ?>"/>
                                        </div>
                                    </div>
                                
                                </div><!--phone number-->
                                                                             
                                




                                <div class="row hidden">
                                    <div class="col-sm-12">
                                        <p><strong>White labelling</strong></p>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">

                                            <input type="checkbox" name="enable_whitelabel" data-on-text="Yes" data-off-text="No" data-handle-width="70" <?php echo ($brokerage[0]['enable_whitelabel'] == 'Y') ? 'checked="checked"' : '' ?> value="Y">

<!-- 
                                            <label class="radio-inline">
                                              <input type="radio" name="enable_whitelabel" id="inlineRadio1" value="Y" required="required"> Yes
                                            </label>
                                            <label class="radio-inline">
                                              <input type="radio" name="enable_whitelabel" id="inlineRadio2" value="N" required="required"> No
                                            </label> -->

                                        </div>
                                    </div>
                                </div>


                                <?php /*?><div class="form-group">
                                    <p class="text-center"><small>or</small></p>
                                    <label>Select Existing Broker</label>
                                    <select class="form-control" name="broker_id">
                                        <option value="">Select</option>
                                        <?php
                                            if(count($the_brokers) > 0){
                                                
                                                foreach($the_brokers as $r=>$value){
                                                    echo '<option value="'.$value['id'].'">'.$value['first_name'].' '.$value['last_name'].'</option>';
                                                }
                                            }
                                        ?>
                                    </select>
                                </div><?php */?>

                                <div class="form-group text-right">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            
                            
                            </form>




                    </div>

                </div>

            </div>


        </div><!--.row-->


    
</div>



    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4>Type 'DELETE' to confirm</h4>
          </div>
          <div class="modal-body">
            
            <form id="delete_form">
                <div class="form-group">
                    <input type="text" name="delete" class="form-control" />
                    <input type="hidden" name="delete2" id="delete2" class="form-control" value="DELETE"/>
                    <input type="hidden" name="id" class="form-control" value="DELETE"/>
                    <input type="hidden" name="type" value="not"/>
                
                </div>
                <button type="submit" class="hidden delete_form_btn btn btn-primary">Confirm</button>
            </form>
            <div class="form-group text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 5px">Cancel</button>
                <button type="button" class="btn btn-primary delete_form_btn2" onclick="$('.delete_form_btn').click()">Confirm</button>
            </div>
            
            
          </div>

        </div>
      </div>
    </div>    
    