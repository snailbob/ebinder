
  
  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
  
      <div class="row">
  
      
        <div class="col-lg-12">

             
           
        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>      
        <?php } ?>
        
        <div class="module_header">
            <i class="fa fa-paypal fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
        </div>

        <div class="panel">
        	<div class="panel-body">

                          
                <table class="table table-striped table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Name</th>
                      <th>Company</th>
                      <th>Date Added</th>
                      <th>Prorate</th>
                      <th>Payment Date</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php if(count($customers) > 0) {
						$count = 1;
						foreach($customers as $r=>$value){ ?>
                        
                        <tr>
                          <td><?php echo $count?></td>
                          <td><?php echo $value['full_name']?></td>
                          <td><?php echo $value['business_name']?></td>
                          <td><?php echo $value['format_date_reg']?></td>
                          <td><?php echo ($count < 6) ? 'Free' : '$'.$value['prorate']?></td>
                          <td><?php echo $value['payment_date']?></td>
                        </tr>
                        
                    
                        
						<?php
						$count++;
						}
						echo '<tr><td colspan="4"></td><td colspan="2"><strong>Total: $'.$this->common->total_billing($user['id'], $nowtime).'</strong></td></tr>';
						?>
                    <?php } else { echo '<tr><td colspan="6" class="text-center">No '.$singular.'</td></tr>'; } ?>


                  </tbody>
                </table>   
                        

            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->
    