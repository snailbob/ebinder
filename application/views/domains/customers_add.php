
  
  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
  
      <div class="row">
  
      
        <div class="col-lg-12">

      
        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>      
        <?php } ?>
        
        
        <div class="module_header">
            <i class="fa fa-user-plus fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <ul class="module_actions right hidden">
                <li>
                    <a class="module_action" href="/approvals/order/pending/csv?subscribed=">
                        <div class="module_action_icon">
                            <i class="icon-export"></i>
                        </div>
                        <span class="module_action_text">EXPORT CSV</span>
                    </a>
                </li>
            </ul>
        </div>
        
        
        
<!--
	BEGIN [Steps Wizard]
-->
        <div class="steps">
          <ol>
            <li class="level1 ">
              <div class="text-primary">
                <span>1</span>Insurance
              </div>
            </li>
            <li class="level2">
              <div>
                <span>2</span>Information
              </div>
            </li>
          </ol>
        </div>
<!--
	END [Steps Wizard]
-->         
        
        <div class="the-insurance-form">
        
            <form id="customer_form" class="text-left">
       			<input type="hidden" name="user_id" value="<?php echo $user['id']?>" />
       			<input type="hidden" name="customer_type" value="<?php echo $user['customer_type']?>" />
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h3 class="heading-title">Customer Information</h3>
                </div>
                <div class="panel-body">
        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                	<i class="fa text-muted fa-info-circle pull-right" data-toggle="tooltip" data-title="This includes all staff employed by your business, including full-time & part-time employees as well as directors. Not including contractors."></i>
                                    <label>Employees (Including the business owner)</label>
                                    <input type="text" class="form-control input-number" name="employees" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                	<i class="fa text-muted fa-info-circle pull-right" data-toggle="tooltip" data-title="This is the estimated total amount of revenue in the next 12 months from all sales and/or services that your business carries out."></i>
                                    <label>Estimated Annual Revenue (Australian Dollars)</label>

                                    <div class="input-group">
                                        <span class="input-group-addon">$</span>
                                        <input type="text" class="form-control input-number" name="annual_revenue">
                                    </div>


                                </div>
                            </div>
                        </div>        
                        
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" />
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control" name="first" />
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control" name="last" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Company Name</label>
                            <input type="text" class="form-control" name="bname" />
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" class="form-control geolocation" name="location" />
                            <input type="hidden" name="lat" />
                            <input type="hidden" name="lng" />
                            
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Contact Number</label>
                                    <select class="form-control" name="country_id">
                                    	<option value="">Select Location Code</option>
										<?php foreach($countries as $r=>$value) {
											echo '<option value="'.$value['country_id'].'">'.$value['short_name_iso2'].'</option>';	
											
										}?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <input type="text" class="form-control" name="calling_digits" />
                                </div>
                            </div>
                        </div>
    
                </div>
            </div><!--.panel-->
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h3 class="heading-title">Which category describes the business best</h3>
                </div>
       
                <div class="panel-body">
                                
                    
                      <!-- Tab panes -->
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="industrySelect">
                    
                            <div class="form-group">
                                <select name="business_category_id" class="form-control">
                                  <option value="">Select Industry</option>
                                    <?php foreach($bizcat as $r=>$value) {
                                        echo '<option value="'.$value['id'].'">'.$value['category_name'].'</option>';
                                    } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <select name="occupation" class="form-control">
                                  <option value="">Select Occupation</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <p><a data-toggle="tab" href="#industrySearch" class="btn-link btn-flat">Can't find occupation?</a></p>
                            </div>
                        
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="industrySearch">
                    
                            <div class="well">
                                <div class="form-group">
                                    <label>Find occupation</label>
                                    <input type="text" class="form-control" name="occupation_find" placeholder="Occupation" />
                                    <input type="hidden" name="occupation_id"/>
                                    <div class="occupation-suggest hidden">
                                        <div class="list-group list-group-suggest">
                                        </div>                                
                                                                        
                                    
                                    </div>
                                    
                                    
                                </div>
                                
                            </div>
                        
                            <div class="form-group">
                                <p><a data-toggle="tab" href="#industrySelect" class="btn-link btn-flat">Back to occupation list</a></p>
                            </div>
                        </div>
                      </div>
    
    
                </div>
            </div><!--.panel-->
                
                
                
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h3 class="heading-title">Product Selection</h3>
                </div>
       
                <div class="panel-body">
    
                        <div class="list-group">
                          <label href="#" class="list-group-item">
                          	<div class="row">
                                <div class="col-sm-8">
                                    <i class="fa text-muted fa-info-circle pull-right" data-toggle="tooltip" data-title="Covers your business for legal and defence costs and damages awarded against your business as a result of the service/advice that you provide"></i>
                                    <input type="checkbox" name="product[]" class="hidden" value="professional_indemnity">
    
                         
                                    <span class="fa-stack fa-lg">
                                      <i class="fa fa-square-o text-muted fa-stack-2x"></i>
                                    </span>	
                                    Professional Indemnity
                                                            
                                </div>
                            
                                <div class="col-sm-4">
                                	<small>Level of Cover</small>
                                    <select name="professional_indemnity" class="form-control input-sm">
                                    	<?php $professional_indemnity = $this->common->q_select_format('professional_indemnity');
										foreach($professional_indemnity as $r=>$value){
											echo '<option value="'.$r.'">'.$value.'</option>';
										}
										?>
                                        
                                        <?php /*?><option value="$1,000,000">$1,000,000</option>
                                        <option value="$1,500,000">$1,500,000</option>
                                        <option value="$2,000,000">$2,000,000</option>
                                        <option value="$2,500,000">$2,500,000</option>
                                        <option value="$3,000,000">$3,000,000</option>
                                        <option value="$4,000,000">$4,000,000</option>
                                        <option value="$5,000,000">$5,000,000</option>
                                        <option value="$10,000,000">$10,000,000</option><?php */?>
                                    </select>
                                </div>
                            
                            </div>

                            
                            
                          </label>
                        </div>                                
    
        
                        <div class="list-group">
                          <label href="#" class="list-group-item">
                          	<div class="row">
                                <div class="col-sm-8">
                                    <i class="fa text-muted fa-info-circle pull-right" data-toggle="tooltip" data-title="Covers your business against claims for personal injury or property damage to a third party that occur as a result of activity related to your business."></i>
                                    <input type="checkbox" name="product[]" class="hidden" value="public_liability">
                                    
                                    <span class="fa-stack fa-lg">
                                      <i class="fa fa-square-o text-muted fa-stack-2x"></i>
                                    </span>	
                                    Public Liability
                                                            
                                </div>
                            
                                <div class="col-sm-4">
                                	<small>Level of Cover</small>
                                    <select name="public_liability" class="form-control input-sm">
                                        <option value="$5,000,000">$5,000,000</option>
                                        <option value="$10,000,000">$10,000,000</option>
                                        <option value="$20,000,000">$20,000,000</option>
                                    </select>
                                </div>
                            
                            </div>

                        </div> 
                        
                                                       
                        <div class="list-group">
                          <label href="#" class="list-group-item">
                          	<div class="row">
                                <div class="col-sm-8">
                                    <i class="fa text-muted fa-info-circle pull-right" data-toggle="tooltip" data-title="This insurance covers you for lost earnings in the event of an injury or illness. You can also receive a lump sum payment in the event of death or permanent disablement."></i>
                                    <input type="checkbox" name="product[]" class="hidden" value="personal_accident_insurance">
                                    
                                    <span class="fa-stack fa-lg">
                                      <i class="fa fa-square-o text-muted fa-stack-2x"></i>
                                    </span>	
                                    Personal Accident Insurance
                                                            
                                </div>
                            
                            
                            </div>

                            
                            
                          </label>
                        </div>                                

                        <div class="list-group">
                          <label href="#" class="list-group-item">
                          	<div class="row">
                                <div class="col-sm-8">
                                    <i class="fa text-muted fa-info-circle pull-right" data-toggle="tooltip" data-title="Covers your business for costs associated with data breaches which may occur being hacked or from another loss/theft of client data."></i>
                                    <input type="checkbox" name="product[]" class="hidden" value="cyber_iability">
                                    
                                    <span class="fa-stack fa-lg">
                                      <i class="fa fa-square-o text-muted fa-stack-2x"></i>
                                    </span>	
                                    Cyber Liability
                                                            
                                </div>
               
             
                                <div class="col-sm-4">
                                	<small>Level of Cover</small>
                                    <select name="cyber_iability" class="form-control input-sm">
                                        <option value="$50,000">$50,000</option>
                                        <option value="$100,000">$100,000</option>
                                        <option value="$250,000">$250,000</option>
                                        <option value="$500,000">$500,000</option>
                                        <option value="$1,000,000">$1,000,000</option>
                                        <option value="$2,000,000">$2,000,000</option>

                                    </select>
                                </div>
                            
                            </div>

                        </div> 
                        
                            
                        <div class="list-group">
                          <label href="#" class="list-group-item">
                          	<div class="row">
                                <div class="col-sm-8">
                                    <i class="fa text-muted fa-info-circle pull-right" data-toggle="tooltip" data-title="Covers your business and its directors against legal costs for allegations of mismanagement, misconduct or legislative breaches."></i>
                                    <input type="checkbox" name="product[]" class="hidden" value="management_liability">
                                    
                                    <span class="fa-stack fa-lg">
                                      <i class="fa fa-square-o text-muted fa-stack-2x"></i>
                                    </span>	
                                    Management Liability
                                                            
                                </div>
               

             
                                <div class="col-sm-4">
                                	<small>Level of Cover</small>
                                    <select name="management_liability" class="form-control input-sm">
                                        <option value="$250,000">$250,000</option>
                                        <option value="$500,000">$500,000</option>
                                        <option value="$1,000,000">$1,000,000</option>
                                        <option value="$2,000,000">$2,000,000</option>
                                        <option value="$3,000,000">$3,000,000</option>
                                        <option value="$4,000,000">$4,000,000</option>
                                        <option value="$5,000,000">$5,000,000</option>
                                    </select>
                                </div>
                            
                            </div>

                        </div> 
                        
                            
                        <div class="form-group">
                        	<div class="well">
                            	<h4>Business Insurance</h4>
                                <hr />
                      
                            <div class="row">

                                <?php if(count($q_products) > 0){
                                    foreach($q_products as $r=>$value){ ?>
                                    <div class="col-sm-6">
                                                                        
                                        <div class="list-group">
                                          <label href="#" class="list-group-item">
                                          	<i class="fa text-muted fa-info-circle pull-right" data-toggle="tooltip" data-title="<?php echo $value['helper'] ?>"></i>
                                            <input type="checkbox" name="product[]" class="hidden" value="<?php echo $value['id'] ?>">
                                            <span class="fa-stack fa-lg">
                                              <i class="fa fa-square-o text-muted fa-stack-2x"></i>
                                            </span>										
                                            
                                            <?php echo $value['product_name']; ?>
                                          </label>
                                        </div>                                
                                    </div>
                                                                    
    
                                    <?php
                                        //echo '<option value="'.$value['id'].'">'.$value['product_name'].'</option>'; 
                                    }
                                } ?>
                            </div>
                            </div><!--.well-->
                        </div>
                        <div class="form-group">
                        	<?php if(count($customers) == 5 && $user['stripe_id'] == '' && $user['customer_type'] == 'N') { ?>
                            <p class="text-muted">You have reach the maximum free customers. Please add your payment method by clicking the button below.</p>
                            <a class="btn btn--orange" href="/panel/setup/payment_method">Add Payment Method</a>
                            <?php } else { ?>
                            <button class="btn btn--orange" type="submit">Next</button>
                            <?php } ?>
                        </div>
                    
                </div>
            </div><!--.panel-->
            
            </form>
        </div><!--.the-insurance-form-->
    
        <div class="the-information-form hidden">
            <div class="panel panel-white">
                <div class="panel-heading">
                    <h3 class="heading-title">Business</h3>
                </div>
       
                <div class="panel-body">
                </div>
            </div><!--.panel-->
      
      
        </div><!--.the-information-form-->

    </div>
</div>

    </div>

    <!-- End of Contents -->
    