<!--Building-->
<div class="row">
    
	<div class="col-sm-6">

        <p>
        	In the last 10 years, has your business or you or any partner or director:<br />
            <ul>
                <li>Had any business insurance/liability claims?</li>
                <li>Had any insurance declined or cancelled?</li>
                <li>Been convicted of any criminal offence?</li>
                <li>Been liable for any civil offence or pecuniary penalties?</li>
                <li>Been declared bankrupt or involved in a business which became insolvent?</li>
            </ul>
        </p>
        
    </div>
	<div class="col-sm-2">
        
        <div class="btn-group" data-toggle="buttons">
          <label class="btn btn-primary">
            <input type="radio" name="5_<?php echo url_title('In the last 10 years', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
          </label>
          <label class="btn btn-primary">
            <input type="radio"  name="5_<?php echo url_title('In the last 10 years', 'underscore', TRUE) ?>" value="No" required="required"> No
          </label>
        </div>
    </div>

</div>

<div class="row">
	<div class="col-sm-6">
        <p>As at today's date does the insured have Public Liability or Business Insurance currently in force that has been paid for?</p>
    </div>
	<div class="col-sm-2">
        
        <div class="btn-group" data-toggle="buttons">
          <label class="btn btn-primary">
            <input type="radio" name="5_<?php echo url_title('have Public Liability', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
          </label>
          <label class="btn btn-primary">
            <input type="radio"  name="5_<?php echo url_title('have Public Liability', 'underscore', TRUE) ?>" value="No" required="required"> No
          </label>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>As at today's date does the insured have Management Liability Insurance currently in force that has been paid for?</p>
    </div>
	<div class="col-sm-2">
        
        <div class="btn-group" data-toggle="buttons">
          <label class="btn btn-primary">
            <input type="radio" name="5_<?php echo url_title('Management Liability Insurance', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
          </label>
          <label class="btn btn-primary">
            <input type="radio"  name="5_<?php echo url_title('Management Liability Insurance', 'underscore', TRUE) ?>" value="No" required="required"> No
          </label>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>As at today's date does the insured have Cyber Liability Insurance currently in force that has been paid for?</p>
    </div>
	<div class="col-sm-2">
        
        <div class="btn-group" data-toggle="buttons">
          <label class="btn btn-primary">
            <input type="radio" name="5_<?php echo url_title('Cyber Liability Insurance', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
          </label>
          <label class="btn btn-primary">
            <input type="radio"  name="5_<?php echo url_title('Cyber Liability Insurance', 'underscore', TRUE) ?>" value="No" required="required"> No
          </label>
        </div>
    </div>
</div>
