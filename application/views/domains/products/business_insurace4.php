<!--Theft-->

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Theft Insured Amount - Contents</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="4_<?php echo url_title('Theft Insured Amount Contents', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Theft Insured Amount - Stock</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="4_<?php echo url_title('Theft Insured Amount Stock', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Theft Insured Amount - Tobacco</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="4_<?php echo url_title('heft Insured Amount Tobacco', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Replacement Value - Liquor</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="4_<?php echo url_title('Replacement Value Liquor', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>
        	Have you had any claims for theft?
        </p>
    </div>
	<div class="col-sm-4">
        <div class="form-group">
            <div class="btn-group radio-toggle-slide" data-toopen="Y" data-totoggle="<?php echo url_title('had any claims for theft', 'underscore', TRUE) ?>" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="4_<?php echo url_title('had any claims for theft', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="4_<?php echo url_title('had any claims for theft', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
    
    
    <div class="col-sm-12 <?php echo url_title('had any claims for theft', 'underscore', TRUE) ?>" style="display: none">
    	<div class="form-group">
            <label>Number of Burglary/Theft claims in the last 3 years?</label>
            <input type="text" name="4_<?php echo url_title('Theft claims last 3 years', 'underscore', TRUE) ?>" class="form-control input-lg input-number" placeholder="0">
        </div>
    	<div class="form-group">
            <label>Number of Burglary/Theft claims in the last 12 months?</label>
            <input type="text" name="4_<?php echo url_title('Theft claims last 12 months', 'underscore', TRUE) ?>" class="form-control input-lg input-number" placeholder="0">
        </div>


    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>
        	Is your property in a Retail or Office building with after hour security and no external access?
        </p>
    </div>
	<div class="col-sm-4">
        <div class="form-group">
            <div class="btn-group radio-toggle-slide" data-toopen="N" data-totoggle="<?php echo url_title('with after hour security', 'underscore', TRUE) ?>"  data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="4_<?php echo url_title('with after hour security', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="4_<?php echo url_title('with after hour security', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
    
    <div class="col-sm-12 <?php echo url_title('with after hour security', 'underscore', TRUE) ?>" style="display: none">
        
        <div class="row">
            <div class="col-sm-6">
                <p>
                    Are there deadlocks on all external doors?
                </p>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="btn-group"  data-toggle="buttons">
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('deadlocks on all external doors', 'underscore', TRUE) ?>" value="Yes"> Yes
                      </label>
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('with after hour security', 'underscore', TRUE) ?>" value="No"> No
                      </label>
                    </div>
                </div>
            </div>
        </div><!--.row-->
        
    	<div class="form-group">
            <label>Burglar Alarm details:</label>
            <select class="form-control input-lg" name="4_<?php echo url_title('Burglar Alarm details', 'underscore', TRUE) ?>">
                <option value="" selected="selected">Please Select</option>
                <option value="0" label="No Burglar Alarm">No Burglar Alarm</option>
                <option value="1" label="Local Alarm (not monitored)">Local Alarm (not monitored)</option>
                <option value="2" label="Dedicated line or wireless monitoring  with continuous streaming to dedicated security company">Dedicated line or wireless monitoring with continuous streaming to dedicated security company</option>
                <option value="3" label="Non-dedicated line (dialler) to dedicated security company">Non-dedicated line (dialler) to dedicated security company</option>
            </select>
        </div>             
        

        <div class="row">
            <div class="col-sm-6">
                <p>
                    Are there bars on all external windows (excluding display windows)?
                </p>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="btn-group"  data-toggle="buttons">
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('bars on all external windows', 'underscore', TRUE) ?>" value="Yes"> Yes
                      </label>
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('bars on all external windows', 'underscore', TRUE) ?>" value="No"> No
                      </label>
                    </div>
                </div>
            </div>
        </div><!--.row-->


        <div class="row">
            <div class="col-sm-6">
                <p>
                    Are there locks on all external windows that don’t have bars?
                </p>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="btn-group"  data-toggle="buttons">
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('locks on all external windows', 'underscore', TRUE) ?>" value="Yes"> Yes
                      </label>
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('locks on all external windows', 'underscore', TRUE) ?>" value="No"> No
                      </label>
                    </div>
                </div>
            </div>
        </div><!--.row-->

        <div class="row">
            <div class="col-sm-6">
                <p>
                    Is there external lighting?
                </p>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="btn-group"  data-toggle="buttons">
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('Is there external lighting', 'underscore', TRUE) ?>" value="Yes"> Yes
                      </label>
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('Is there external lighting', 'underscore', TRUE) ?>" value="No"> No
                      </label>
                    </div>
                </div>
            </div>
        </div><!--.row-->
        
        <div class="row">
            <div class="col-sm-6">
                <p>
                    Are <strong>bollards</strong> installed in front of glazing such as glass doors, display windows, roller shutters to prevent ram attacks?
                </p>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="btn-group"  data-toggle="buttons">
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('bollards installed in front of glazing', 'underscore', TRUE) ?>" value="Yes"> Yes
                      </label>
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('bollards installed in front of glazing', 'underscore', TRUE) ?>" value="No"> No
                      </label>
                    </div>
                </div>
            </div>
        </div><!--.row-->

        <div class="row">
            <div class="col-sm-6">
                <p>
                    Is there a taped Closed Circuit TV system installed?
                </p>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="btn-group"  data-toggle="buttons">
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('taped Closed Circuit TV', 'underscore', TRUE) ?>" value="Yes"> Yes
                      </label>
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('taped Closed Circuit TV', 'underscore', TRUE) ?>" value="No"> No
                      </label>
                    </div>
                </div>
            </div>
        </div><!--.row-->

        <div class="row">
            <div class="col-sm-6">
                <p>
                    Do security personnel conduct random patrols during non-business hours?
                </p>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="btn-group"  data-toggle="buttons">
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('security personnel conduct random patrols', 'underscore', TRUE) ?>" value="Yes"> Yes
                      </label>
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('security personnel conduct random patrols', 'underscore', TRUE) ?>" value="No"> No
                      </label>
                    </div>
                </div>
            </div>
        </div><!--.row-->
        
        <div class="row">
            <div class="col-sm-6">
                <p>
                    Is there is a fence/wall, minimum 2 meters high, totally enclosing the premises?
                </p>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="btn-group"  data-toggle="buttons">
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('totally enclosing the premises', 'underscore', TRUE) ?>" value="Yes"> Yes
                      </label>
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('totally enclosing the premises', 'underscore', TRUE) ?>" value="No"> No
                      </label>
                    </div>
                </div>
            </div>
        </div><!--.row-->

        <div class="row">
            <div class="col-sm-6">
                <p>
                    Are there any display windows ?
                </p>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <div class="btn-group radio-toggle-slide" data-toopen="Y" data-totoggle="<?php echo url_title('Are there any display windows', 'underscore', TRUE) ?>"  data-toggle="buttons">
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('Are there any display windows', 'underscore', TRUE) ?>" value="Yes"> Yes
                      </label>
                      <label class="btn btn-primary">
                        <input type="radio" name="4_<?php echo url_title('Are there any display windows', 'underscore', TRUE) ?>" value="No"> No
                      </label>
                    </div>
                </div>
            </div>
            
            
            <div class="col-sm-12 <?php echo url_title('Are there any display windows', 'underscore', TRUE) ?>" style="display: none">
                <div class="row">
                    <div class="col-sm-6">
                        <p>
                            Are all display windows protected by a minimum 11 mm plate glass with polycarbonate film or thief resistant laminate glass or security screens, grills or bars?
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <div class="btn-group" data-toggle="buttons">
                              <label class="btn btn-primary">
                                <input type="radio" name="4_<?php echo url_title('windows protected by a minimum 11 mm plate glass', 'underscore', TRUE) ?>" value="Yes"> Yes
                              </label>
                              <label class="btn btn-primary">
                                <input type="radio" name="4_<?php echo url_title('windows protected by a minimum 11 mm plate glass', 'underscore', TRUE) ?>" value="No"> No
                              </label>
                            </div>
                        </div>
                    </div>
                    
                </div><!--.row-->
                
            </div>
        </div><!--.row-->



    </div>
    
</div>
