
<div class="row">
    
	<div class="col-sm-6">
        <p>What type of Personal Accident cover would you like?</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
          
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" required="required" name="personalAccidentInsurance_<?php echo url_title('Personal Accident cover type', 'underscore', TRUE) ?>" value="Personal Accident Only"> Personal Accident Only
              </label>
              <label class="btn btn-primary">
                <input type="radio" required="required" name="personalAccidentInsurance_<?php echo url_title('Personal Accident cover type', 'underscore', TRUE) ?>" value="Personal Accident &amp; Illness"> Personal Accident &amp; Illness
              </label>
            </div>
        </div>
    </div>

</div>

<div class="row">
	<div class="col-sm-12">
        <div class="form-group">
            <label>Enter the full name of the Insured Person
</label>
            <input type="text" required="required" class="form-control input-lg" name="personalAccidentInsurance_<?php echo url_title('Insured Person', 'underscore', TRUE) ?>"/>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <label class="form-control-static">Date of Birth</label>
    </div>
	<div class="col-sm-2">
        <div class="form-group">
            <select class="form-control input-lg" name="personalAccidentInsurance_<?php echo url_title('birth day', 'underscore', TRUE) ?>" required="required">
                <option value="" selected="selected"></option>
                <option value="0" label="1">1</option>
                <option value="1" label="2">2</option>
                <option value="2" label="3">3</option>
                <option value="3" label="4">4</option>
                <option value="4" label="5">5</option>
                <option value="5" label="6">6</option>
                <option value="6" label="7">7</option>
                <option value="7" label="8">8</option>
                <option value="8" label="9">9</option>
                <option value="9" label="10">10</option>
                <option value="10" label="11">11</option>
                <option value="11" label="12">12</option>
                <option value="12" label="13">13</option>
                <option value="13" label="14">14</option>
                <option value="14" label="15">15</option>
                <option value="15" label="16">16</option>
                <option value="16" label="17">17</option>
                <option value="17" label="18">18</option>
                <option value="18" label="19">19</option>
                <option value="19" label="20">20</option>
                <option value="20" label="21">21</option>
                <option value="21" label="22">22</option>
                <option value="22" label="23">23</option>
                <option value="23" label="24">24</option>
                <option value="24" label="25">25</option>
                <option value="25" label="26">26</option>
                <option value="26" label="27">27</option>
                <option value="27" label="28">28</option>
                <option value="28" label="29">29</option>
                <option value="29" label="30">30</option>
                <option value="30" label="31">31</option>
            </select>
   
        </div>
    </div>
	<div class="col-sm-2">
        <div class="form-group">
            <select class="form-control input-lg" name="personalAccidentInsurance_<?php echo url_title('birth month', 'underscore', TRUE) ?>" required="required">
                <option value="" selected="selected"></option>
                <option value="0" label="Jan">Jan</option>
                <option value="1" label="Feb">Feb</option>
                <option value="2" label="Mar">Mar</option>
                <option value="3" label="Apr">Apr</option>
                <option value="4" label="May">May</option>
                <option value="5" label="Jun">Jun</option>
                <option value="6" label="Jul">Jul</option>
                <option value="7" label="Aug">Aug</option>
                <option value="8" label="Sep">Sep</option>
                <option value="9" label="Oct">Oct</option>
                <option value="10" label="Nov">Nov</option>
                <option value="11" label="Dec">Dec</option>
            </select>
        </div>
    </div>
	<div class="col-sm-2">
        <div class="form-group">
            <select class="form-control input-lg" name="personalAccidentInsurance_<?php echo url_title('birth year', 'underscore', TRUE) ?>" required="required">
                <option value="" selected="selected"></option>
                <option value="0" label="1946">1946</option>
                <option value="1" label="1947">1947</option>
                <option value="2" label="1948">1948</option>
                <option value="3" label="1949">1949</option>
                <option value="4" label="1950">1950</option>
                <option value="5" label="1951">1951</option>
                <option value="6" label="1952">1952</option>
                <option value="7" label="1953">1953</option>
                <option value="8" label="1954">1954</option>
                <option value="9" label="1955">1955</option>
                <option value="10" label="1956">1956</option>
                <option value="11" label="1957">1957</option>
                <option value="12" label="1958">1958</option>
                <option value="13" label="1959">1959</option>
                <option value="14" label="1960">1960</option>
                <option value="15" label="1961">1961</option>
                <option value="16" label="1962">1962</option>
                <option value="17" label="1963">1963</option>
                <option value="18" label="1964">1964</option>
                <option value="19" label="1965">1965</option>
                <option value="20" label="1966">1966</option>
                <option value="21" label="1967">1967</option>
                <option value="22" label="1968">1968</option>
                <option value="23" label="1969">1969</option>
                <option value="24" label="1970">1970</option>
                <option value="25" label="1971">1971</option>
                <option value="26" label="1972">1972</option>
                <option value="27" label="1973">1973</option>
                <option value="28" label="1974">1974</option>
                <option value="29" label="1975">1975</option>
                <option value="30" label="1976">1976</option>
                <option value="31" label="1977">1977</option>
                <option value="32" label="1978">1978</option>
                <option value="33" label="1979">1979</option>
                <option value="34" label="1980">1980</option>
                <option value="35" label="1981">1981</option>
                <option value="36" label="1982">1982</option>
                <option value="37" label="1983">1983</option>
                <option value="38" label="1984">1984</option>
                <option value="39" label="1985">1985</option>
                <option value="40" label="1986">1986</option>
                <option value="41" label="1987">1987</option>
                <option value="42" label="1988">1988</option>
                <option value="43" label="1989">1989</option>
                <option value="44" label="1990">1990</option>
                <option value="45" label="1991">1991</option>
                <option value="46" label="1992">1992</option>
                <option value="47" label="1993">1993</option>
                <option value="48" label="1994">1994</option>
                <option value="49" label="1995">1995</option>
                <option value="50" label="1996">1996</option>
                <option value="51" label="1997">1997</option>
                <option value="52" label="1998">1998</option>
                <option value="53" label="1999">1999</option>
                <option value="54" label="2000">2000</option>
            </select>  
        </div>


    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>Benefit Period</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" required="required" name="personalAccidentInsurance_<?php echo url_title('Benefit Period', 'underscore', TRUE) ?>" value="52 Weeks"> 52 Weeks
              </label>
              <label class="btn btn-primary">
                <input type="radio" required="required" name="personalAccidentInsurance_<?php echo url_title('Benefit Period', 'underscore', TRUE) ?>" value="104 Weeks"> 104 Weeks
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>Waiting Period</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" required="required" name="personalAccidentInsurance_<?php echo url_title('Waiting Period', 'underscore', TRUE) ?>" value="7 Days"> 7 Days
              </label>
              <label class="btn btn-primary">
                <input type="radio" required="required" name="personalAccidentInsurance_<?php echo url_title('Waiting Period', 'underscore', TRUE) ?>" value="14 Days"> 14 Days
              </label>
              <label class="btn btn-primary">
                <input type="radio" required="required" name="personalAccidentInsurance_<?php echo url_title('Waiting Period', 'underscore', TRUE) ?>" value="21 Days"> 21 Days
              </label>
              <label class="btn btn-primary">
                <input type="radio" required="required" name="personalAccidentInsurance_<?php echo url_title('Waiting Period', 'underscore', TRUE) ?>" value="28 Days"> 28 Days
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
        <div class="form-group">
            <label>What level of cover for Accidental Death & Disability would you like?
</label>
            <select name="personalAccidentInsurance_<?php echo url_title('level of cover for Accidental Death', 'underscore', TRUE) ?>" class="form-control input-lg" required="required">
                <option value="" selected="selected">Please Select</option>
                <option value="No, I don't want to cover this" label="No, I don't want to cover this">No, I don't want to cover this</option>
                <option value="$50,000" label="$50,000">$50,000</option>
                <option value="$100,000" label="$100,000">$100,000</option>
            </select>
        </div>
    </div>
</div>


<h4>Benefit Calculator</h4>

<div class="row">
	<div class="col-sm-6">
        <label class="form-control-static">What is your gross weekly income?</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" required="required" name="personalAccidentInsurance_<?php echo url_title('gross weekly income', 'underscore', TRUE) ?>" class="form-control" placeholder="0">
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <label class="form-control-static">Deduct any weekly expenses that you will not incur if you do not work</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" required="required" name="personalAccidentInsurance_<?php echo url_title('Deduct any weekly expenses', 'underscore', TRUE) ?>" class="form-control" placeholder="0">
            </div>
        </div>
    </div>
</div>

<hr />
    

<div class="row">
	<div class="col-sm-6">
        <label class="form-control-static">The maximum weekly benefit you can insure is:</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            $0
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <label class="form-control-static">What weekly Benefit Amount would you like insure?</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" required="required" name="personalAccidentInsurance_<?php echo url_title('weekly Benefit Amount', 'underscore', TRUE) ?>" class="form-control" placeholder="0">
            </div>
        </div>
    </div>
</div>

<div class="row">
    
	<div class="col-sm-6">
        <p>Has the Insured Person had any Personal Accident or Illness claims exceeding $3,000 over the past 5 years?</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" required="required" name="personalAccidentInsurance_<?php echo url_title('had any Personal Accident', 'underscore', TRUE) ?>"> Personal Accident Only
              </label>
              <label class="btn btn-primary">
                <input type="radio" required="required" name="personalAccidentInsurance_<?php echo url_title('had any Personal Accident', 'underscore', TRUE) ?>"> Personal Accident &amp; Illness
              </label>
            </div>
        </div>
    </div>

</div>