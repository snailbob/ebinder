<!--Glass-->
<div class="row">
	<div class="col-sm-6">
        <p>
        	Do you have any illuminated signs?
        </p>
    </div>
	<div class="col-sm-2">
        <div class="form-group">
          
            <div class="btn-group radio-toggle-slide" data-totoggle="<?php echo url_title('have any illuminated signs', 'underscore', TRUE) ?>" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="7_<?php echo url_title('have any illuminated signs', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="7_<?php echo url_title('have any illuminated signs', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>

	<div class="col-sm-12 <?php echo url_title('have any illuminated signs', 'underscore', TRUE) ?>" style="display: none;">
    	<div class="form-group">
            <label>Number of Illuminated Signs</label>
            <input type="text" class="form-control input-lg input-number" name="7_<?php echo url_title('Number of Illuminated Signs', 'underscore', TRUE) ?>" />

        </div>
        
    	<div class="form-group">
            <label>How much would you like to insure your illuminated signs for?</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="7_<?php echo url_title('How much to insure illuminated signs', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
        
    </div>

</div>

<div class="form-group">
    <label>Largest pane of glass</label>
    <select class="form-control input-lg" name="7_<?php echo url_title('Largest pane of glass', 'underscore', TRUE) ?>" required="required">
        <option value="" selected="selected">Please Select</option>
        <option value="0" label="Greater than 8 sq meters">Greater than 8 sq meters</option>
        <option value="1" label="Greater than 5 metres and less than 8 sq metres">Greater than 5 metres and less than 8 sq metres</option>
        <option value="2" label="Less than 5 sq metres">Less than 5 sq metres</option>
    </select>
</div>             



<div class="row">
	<div class="col-sm-6">
        <p>
        	Have you had any claims for glass?
        </p>
    </div>
	<div class="col-sm-2">
        <div class="form-group">
          
            <div class="btn-group radio-toggle-slide" data-totoggle="<?php echo url_title('had any claims for glass', 'underscore', TRUE) ?>" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="7_<?php echo url_title('had any claims for glass', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="7_<?php echo url_title('had any claims for glass', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>

	<div class="col-sm-12 <?php echo url_title('had any claims for glass', 'underscore', TRUE) ?>" style="display: none;">
    	<div class="form-group">
            <label>Number of Glass claims in the last 3 years?
</label>
            <input type="text" class="form-control input-lg input-number" name="7_<?php echo url_title('Number of Glass claims in the last 3 years', 'underscore', TRUE) ?>" />

        </div>
        
    	<div class="form-group">
            <label>Number of Glass claims in the last 12 months?

</label>
            <input type="text" class="form-control input-lg input-number" name="7_<?php echo url_title('Number of Glass claims in the last 12 months', 'underscore', TRUE) ?>" />

        </div>
        
    </div>

</div>


<div class="row">
	<div class="col-sm-6">
        <p>
        	Are the premises fully enclosed in a shopping mall/complex?
        </p>
    </div>
	<div class="col-sm-2">
        <div class="form-group">
          
            <div class="btn-group radio-toggle-slide" data-toopen="N" data-totoggle="<?php echo url_title('premises fully enclosed in a shopping mall', 'underscore', TRUE) ?>" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="7_<?php echo url_title('premises fully enclosed in a shopping mall', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="7_<?php echo url_title('premises fully enclosed in a shopping mall', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>

	<div class="col-sm-12 <?php echo url_title('premises fully enclosed in a shopping mall', 'underscore', TRUE) ?>" style="display: none;">
    	<div class="form-group">
            <label>What percentage of external glass is above the ground floor?
</label>
            <div class="input-group input-group-lg">
              <input type="text" name="7_<?php echo url_title('percentage of external glass', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>

        </div>
        
    </div>

</div>
