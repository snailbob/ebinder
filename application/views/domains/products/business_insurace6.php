<!--6 General Property-->
<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Number of Portable Equipment claims in the last 3 years?</label>
            <input type="text" name="6_<?php echo url_title('Portable Equipment claims', 'underscore', TRUE) ?>" class="form-control input-lg input-number" required="required">
        </div>
    </div>
</div>

Please add items to insure
<div class="row row-16">
	<div class="well">
		<div class="row">
        	<div class="col-sm-5 the-items-list-6 hidden">
				<label>Items included</label>

            </div>
            
            <div class="col-sm-12 the-form-col">
            	<div class="the-item-form-6">
                
                    <div class="form-group">
                        <label>Item</label>
                        <select name="6_<?php echo url_title('Item', 'underscore', TRUE) ?>[]" class="form-control input-lg">
                            <option value="" selected="selected">Please select</option>
                            <option value="0" label="CB Radio">CB Radio</option>
                            <option value="1" label="Surveying Equipment">Surveying Equipment</option>
                            <option value="2" label="Tools Of Trade">Tools Of Trade</option>
                            <option value="3" label="Two Way Radio Equipment">Two Way Radio Equipment</option>
                            <option value="4" label="Cleaning Equipment">Cleaning Equipment</option>
                            <option value="5" label="Compressors">Compressors</option>
                            <option value="6" label="Heavy Machinery">Heavy Machinery</option>
                            <option value="7" label="Laptops">Laptops</option>
                            <option value="8" label="Mobile Phones">Mobile Phones</option>
                            <option value="9" label="Office Equipment">Office Equipment</option>
                            <option value="10" label="Paging Units">Paging Units</option>
                            <option value="11" label="Radio Base Station">Radio Base Station</option>
                        </select>
            
                    </div>
            
                    <div class="form-group">
                        <label>Make & Model</label>
                        <input type="text" name="6_<?php echo url_title('Make Model', 'underscore', TRUE) ?>[]" class="form-control input-lg">
                    </div>
            
                    <div class="form-group">
                        <label>Serial Number</label>
                        <input type="text" name="6_<?php echo url_title('Serial Number', 'underscore', TRUE) ?>[]" class="form-control input-lg">
                    </div>
            
                    <div class="form-group">
                        <label>Sum Insured</label>
                        <div class="input-group input-group-lg">
                          <span class="input-group-addon">$</span>
                          <input type="text" name="6_<?php echo url_title('Sum Insured', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0">
                        </div>
                    </div>
                
                </div>

                <div class="form-group text-right">
                    <span class="text-danger hidden">Please add items to this cover</span>
                    <a href="#" class="btn btn--blue add-items-btn" data-form="the-item-form-6" data-container="the-items-list-6">Add</a>
                </div>
                
            </div>
        </div>


    </div>
</div>
