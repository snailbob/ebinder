<!--9 Business Interruption-->
<div class="form-group">
    <label>Gross Income</label>
    <div class="input-group input-group-lg">
      <span class="input-group-addon" id="basic-addon2">$</span>
      <input type="text" name="9_<?php echo url_title('Gross Income', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
    </div>
</div>

<div class="form-group">
    <label>Additional Increase Cost Of Working</label>
    <div class="input-group input-group-lg">
      <span class="input-group-addon" id="basic-addon2">$</span>
      <input type="text" name="9_<?php echo url_title('Additional Increase Cost Of Working', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
    </div>
</div>

<div class="form-group">
    <label>Claims Preparation Costs</label>
    <div class="input-group input-group-lg">
      <span class="input-group-addon" id="basic-addon2">$</span>
      <input type="text" name="9_<?php echo url_title('Claims Preparation Costs', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
    </div>
</div>

<div class="form-group">
    <label>Outstanding Accounts Receivable</label>
    <div class="input-group input-group-lg">
      <span class="input-group-addon" id="basic-addon2">$</span>
      <input type="text" name="9_<?php echo url_title('Outstanding Accounts Receivable', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>Are there any uninsured working expenses?</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
          
            <div class="btn-group radio-toggle-slide" data-totoggle="have_money_claims" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="9_<?php echo url_title('uninsured working expenses', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="9_<?php echo url_title('uninsured working expenses', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <label>Indemnity Period (months)</label>
    <select class="form-control input-lg" name="9_<?php echo url_title('Indemnity Period', 'underscore', TRUE) ?>" required="required">
        <option value="" selected="selected">Please Select</option>
        <option value="0" label="6 Months">6 Months</option>
        <option value="1" label="12 Months">12 Months</option>
    </select>      

</div>