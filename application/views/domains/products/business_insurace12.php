<!--12 Machinery Breakdown-->
<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Number of Machinery Breakdown claims in the last 12 months?</label>
            <input type="text" name="12_<?php echo url_title('Machinery Breakdown claims', 'underscore', TRUE) ?>" class="form-control input-lg input-number" required="required">
        </div>
    </div>
</div>

Please add items to insure
<div class="row row-16">
	<div class="well">
		<div class="row">
        	<div class="col-sm-5 the-items-list-12 hidden">
				<label>Items included</label>

            </div>
            
            <div class="col-sm-12 the-form-col">
            	<div class="the-item-form-12">
            
                    <div class="form-group">
                        <label>Item Type</label>
                        <select name="12_<?php echo url_title('Item Type', 'underscore', TRUE) ?>[]" class="form-control input-lg">
                            <option value="" selected="selected">Please select</option>
                            <option value="0" label="Air compressor">Air compressor</option>
                            <option value="1" label="Air conditioning - ducted system">Air conditioning - ducted system</option>
                            <option value="2" label="Air conditioning - portable unit">Air conditioning - portable unit</option>
                            <option value="3" label="Air conditioning - split system">Air conditioning - split system</option>
                            <option value="4" label="Air conditioning - wall/window unit">Air conditioning - wall/window unit</option>
                            <option value="5" label="Boiler">Boiler</option>
                            <option value="6" label="Auto car wash">Auto car wash</option>
                            <option value="7" label="Cash register">Cash register</option>
                            <option value="8" label="Cool Room">Cool Room</option>
                            <option value="9" label="Dish/glass washer">Dish/glass washer</option>
                            <option value="10" label="Dowling machine">Dowling machine</option>
                            <option value="11" label="Drilling machine">Drilling machine</option>
                            <option value="12" label="Dry cleaning machine">Dry cleaning machine</option>
                            <option value="13" label="Engine diagnostic unit">Engine diagnostic unit</option>
                            <option value="14" label="Espresso coffee machine">Espresso coffee machine</option>
                            <option value="15" label="Fan - supply/exhaust">Fan - supply/exhaust</option>
                            <option value="16" label="Folding machine">Folding machine</option>
                            <option value="17" label="Forklifts">Forklifts</option>
                            <option value="18" label="Refrigerators (EX cool room)">Refrigerators (EX cool room)</option>
                            <option value="19" label="Gear boxes">Gear boxes</option>
                            <option value="20" label="Grinding machine">Grinding machine</option>
                            <option value="21" label="Guillotines">Guillotines</option>
                            <option value="22" label="Hoist">Hoist</option>
                            <option value="23" label="Hydro extractors">Hydro extractors</option>
                            <option value="24" label="Ice making machine">Ice making machine</option>
                            <option value="25" label="Image setter machine">Image setter machine</option>
                            <option value="26" label="Ironing machine">Ironing machine</option>
                            <option value="27" label="Lathes">Lathes</option>
                            <option value="28" label="Lighting plant">Lighting plant</option>
                            <option value="29" label="Mills (hammer)">Mills (hammer)</option>
                            <option value="30" label="Milling machine">Milling machine</option>
                            <option value="31" label="Microwave">Microwave</option>
                            <option value="32" label="Other">Other</option>
                            <option value="33" label="Planers (multi-head)">Planers (multi-head)</option>
                            <option value="34" label="Press">Press</option>
                            <option value="35" label="Printer">Printer</option>
                            <option value="36" label="Pump - electric">Pump - electric</option>
                            <option value="37" label="Pump - petrol/diesel">Pump - petrol/diesel</option>
                            <option value="38" label="Pump - submersible">Pump - submersible</option>
                            <option value="39" label="Punching machine">Punching machine</option>
                            <option value="40" label="Rectifiers">Rectifiers</option>
                            <option value="41" label="Refrigerant reclaim unit">Refrigerant reclaim unit</option>
                            <option value="42" label="Sanding machine">Sanding machine</option>
                            <option value="43" label="Saws (all types)">Saws (all types)</option>
                            <option value="44" label="Scales">Scales</option>
                            <option value="45" label="Scanners">Scanners</option>
                            <option value="46" label="Shearing plant">Shearing plant</option>
                            <option value="47" label="Slicing/mincing/mixing machine">Slicing/mincing/mixing machine</option>
                            <option value="48" label="Soft serve ice cream units">Soft serve ice cream units</option>
                            <option value="49" label="Steam presses">Steam presses</option>
                            <option value="50" label="Switchboard">Switchboard</option>
                            <option value="51" label="Temprites">Temprites</option>
                            <option value="52" label="Thicknessers">Thicknessers</option>
                            <option value="53" label="Vacuum cleaners">Vacuum cleaners</option>
                            <option value="54" label="Vat">Vat</option>
                            <option value="55" label="Washing machine/dryers">Washing machine/dryers</option>
                            <option value="56" label="Weigh bridges - electronic">Weigh bridges - electronic</option>
                            <option value="57" label="Welder - electric">Welder - electric</option>
                            <option value="58" label="Wheel aligning/balancing units">Wheel aligning/balancing units</option>
                        </select>
            
                    </div>
            
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" name="12_<?php echo url_title('Description', 'underscore', TRUE) ?>[]" class="form-control input-lg">
                    </div>
            
                    <div class="form-group">
                        <label>Item Count</label>
                        <input type="text" name="12_<?php echo url_title('Item Count', 'underscore', TRUE) ?>[]" class="form-control input-lg input-number">
                    </div>
            
                    <div class="form-group">
                        <label>Sum Insured</label>
                        <div class="input-group input-group-lg">
                          <span class="input-group-addon">$</span>
                          <input type="text" name="12_<?php echo url_title('Sum Insured', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0">
                        </div>
                    </div>
                </div>
        
                <div class="form-group text-right">
                    <span class="text-danger hidden">Please add items to this cover</span>
                    <a href="#" class="btn btn--blue add-items-btn" data-form="the-item-form-12" data-container="the-items-list-12">Add</a>
                </div>

        
            </div>
        </div>


    </div>
</div>
