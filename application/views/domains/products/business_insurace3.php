<!--Contents-->
<div class="row">
	<div class="col-sm-6">
        <p>
        	Is there any EPS or insulated sandwich panel present within the building other than a cool room of 70 square metres or less?
        </p>
        
    </div>
	<div class="col-sm-4">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="3_<?php echo url_title('insulated sandwich panel present', 'underscore', TRUE) ?>" required="required" value="Yes"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="3_<?php echo url_title('insulated sandwich panel present', 'underscore', TRUE) ?>" required="required" value="No"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Construction of the External Walls?</label>
            <select name="3_<?php echo url_title('External Walls', 'underscore', TRUE) ?>" class="form-control input-lg" required="required">
                <option value="" selected="selected">Please Select</option>
                <option value="0" label="Brick/Stone/Concrete">Brick/Stone/Concrete</option>
                <option value="1" label="Sandwich Panel - EPS">Sandwich Panel - EPS</option>
                <option value="2" label="Iron on Steel">Iron on Steel</option>
                <option value="3" label="Iron on Wood">Iron on Wood</option>
                <option value="4" label="Mixed > 75% Brick/Concrete/Iron on Steel">Mixed &gt; 75% Brick/Concrete/Iron on Steel</option>
                <option value="5" label="Mixed < 75% Brick/Concrete/Iron on Steel">Mixed &lt; 75% Brick/Concrete/Iron on Steel</option>
                <option value="6" label="Fibro">Fibro</option>
                <option value="7" label="Timber">Timber</option>
                <option value="8" label="Other">Other</option>
            
            </select>

        </div>


    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Construction of all the Floors?</label>
            <select name="3_<?php echo url_title('Construction of Floors', 'underscore', TRUE) ?>" class="form-control input-lg " required="required">
                <option value="" selected="selected">Please Select</option>
                <option value="0" label="Concrete/Stone/Earth">Concrete/Stone/Earth</option>
                <option value="1" label="Concrete and Wood">Concrete and Wood</option>
                <option value="2" label="Wood">Wood</option>
                <option value="3" label="Other">Other</option>
            
            </select>

        </div>


    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>In approximately what year was the building constructed?</label>
            <select name="3_<?php echo url_title('approximately year building constructed', 'underscore', TRUE) ?>" class="form-control input-lg " required="required">
                <option value="" selected="selected">Please Select</option>
                <option value="0" label="2003 or later">2003 or later</option>
                <option value="1" label="1980 - 2002">1980 - 2002</option>
                <option value="2" label="Prior to 1980">Prior to 1980</option>            
            </select>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Construction of the Roof?</label>
            <select name="3_<?php echo url_title('Construction of the Roof', 'underscore', TRUE) ?>" class="form-control input-lg " required="required">
                <option value="" selected="selected">Please Select</option>
                <option value="0" label="Asbestos">Asbestos</option>
                <option value="1" label="Colorbond Steel">Colorbond Steel</option>
                <option value="2" label="Concrete">Concrete</option>
                <option value="3" label="Fibro">Fibro</option>
                <option value="4" label="Iron">Iron</option>
                <option value="5" label="Metal">Metal</option>
                <option value="6" label="Tiles">Tiles</option>
                <option value="7" label="Other">Other</option>
            </select>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>
        	Is the property fully protected by a currently serviced, automatic sprinkler system?
        </p>
    </div>
	<div class="col-sm-4">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="3_<?php echo url_title('property fully protected', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="3_<?php echo url_title('property fully protected', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Sum Insured - Building</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="3_<?php echo url_title('Sum Insured Building', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>
        	Do the Premises comply with Fire and Council regulations?
        </p>
    </div>
	<div class="col-sm-4">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="3_<?php echo url_title('Premises comply with Fire', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="3_<?php echo url_title('Premises comply with Fire', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>
        	Are the Premises connected to town water and in the area of a permanently manned Fire Station?
        </p>
    </div>
	<div class="col-sm-4">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="3_<?php echo url_title('Premises connected to town water', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="3_<?php echo url_title('Premises connected to town water', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Replacement Value - Contents</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="3_<?php echo url_title('Replacement Value Contents', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Replacement Value - Stock in Trade</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="3_<?php echo url_title('Replacement Value Stock in Trade', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Replacement Value - Customers Goods</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="3_<?php echo url_title('Replacement Value Customers Goods', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>
        	Would you like to name an interested party for your property cover?
        </p>
    </div>
	<div class="col-sm-4">
        <div class="form-group">
            <div class="btn-group radio-toggle-slide" data-toopen="Y" data-totoggle="<?php echo url_title('name an interested party', 'underscore', TRUE) ?>" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="3_<?php echo url_title('name an interested party', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="3_<?php echo url_title('name an interested party', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
    
    
    <div class="col-sm-12 <?php echo url_title('name an interested party', 'underscore', TRUE) ?>" style="display: none">
    	<div class="form-group">
            <label>Interested party name</label>
            <input type="text" name="3_<?php echo url_title('Interested party name', 'underscore', TRUE) ?>" class="form-control input-lg" placeholder="">
        </div>
        
    	<div class="form-group">
            <label>Interested party type</label>
            <select class="form-control input-lg" name="3_<?php echo url_title('Interested party type', 'underscore', TRUE) ?>">
                <option value="" selected="selected">Please Select</option>
                <option value="0" label="Mortgagee">Mortgagee</option>
                <option value="1" label="Owner">Owner</option>
                <option value="2" label="Landlord">Landlord</option>
            </select>

        </div>        

    </div>
    
</div>
