<!--Employment Practices Liability-->
<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Sum Insured - Employment Practices</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="15_<?php echo url_title('Sum Insured Employment Practices', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
        
        
    </div>
</div>
