<!--Money-->
<div class="row">
	<div class="col-sm-6">
        <p>Have you had any claims for money?</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
          
            <div class="btn-group radio-toggle-slide" data-totoggle="have_money_claims" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="13_<?php echo url_title('claims for money', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="13_<?php echo url_title('claims for money', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
    
	<div class="col-sm-12 have_money_claims" style="display: none;">
    	<div class="form-group">
            <label>Number of Money claims in the last 3 years?</label>
            <input type="text" class="form-control input-lg input-number" name="13_<?php echo url_title('money claims last 3 years', 'underscore', TRUE) ?>" required="required"/>

        </div>
        
    	<div class="form-group">
            <label>Number of Money claims in the last 12 months?</label>
            <input type="text" class="form-control input-lg input-number" name="13_<?php echo url_title('money claims last 12 months', 'underscore', TRUE) ?>" required="required"/>

        </div>
        
        
        
    </div>


    
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Sum Insured - Money</label>
            <select class="form-control input-lg" name="13_<?php echo url_title('Sum Insured - Money', 'underscore', TRUE) ?>" required="required">
                <option value="" selected="selected">Please Select</option>
                <option value="0" label="$2,500">$2,500</option>
                <option value="1" label="$5,000">$5,000</option>
                <option value="2" label="$10,000">$10,000</option>
            </select>      


        </div>
    </div>
</div>
