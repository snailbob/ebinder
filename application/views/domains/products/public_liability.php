<!--public_liability-->
<div class="row">
    
	<div class="col-sm-6">

        <p>Does Your Business engage or intend to engage non-clerical contractors, subcontractors, or staff from labour hire firms to perform work under the sole or partial direction of You?</p>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="publicLiability_<?php echo url_title('engage or intend to engage non clerical contractors', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="publicLiability_<?php echo url_title('engage or intend to engage non clerical contractors', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>

</div>

<div class="row">

	<div class="col-sm-6">
        <p>Would you like to note your landlord on your Public Liability policy?</p>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="publicLiability_<?php echo url_title('note your landlord on your Public Liability policy', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="publicLiability_<?php echo url_title('note your landlord on your Public Liability policy', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
    

</div>
