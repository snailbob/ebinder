<!--16  Selected Statutory Liability-->
<div class="form-group">
    <label>Sum Insured - Statutory Liability</label><br />
    <select class="form-control input-lg" name="16_<?php echo url_title('Sum Insured Statutory Liability', 'underscore', TRUE) ?>" required="required">
        <option value="" selected="selected">Please Select</option>
        <option value="0" label="$250,000">$250,000</option>
        <option value="1" label="$500,000">$500,000</option>
    </select>
</div>
