
<div class="row">
    
	<div class="col-sm-12">
    	<div class="form-group">
        	<label>Policy number</label>
            <input type="text" class="form-control" name="policy_number[]" placeholder="Policy number" value="<?php echo (isset($user['policies']['policy_number'][$index])) ? $user['policies']['policy_number'][$index] : ''?>" required="required"/>
        </div>
    	<div class="form-group">
        	<label>Insurer</label>
            <input type="text" class="form-control" name="insurer[]" placeholder="Insurer" value="<?php echo (isset($user['policies']['insurer'][$index])) ? $user['policies']['insurer'][$index] : '' ?>" required="required"/>
        </div>
    	<div class="form-group">
        	<label>Date of commence to date of expiry</label>
            <input type="date" class="form-control" name="policy_period[]" placeholder="Policy period: date of commence to date of expiry" value="<?php echo (isset($user['policies']['policy_period'][$index])) ?  $user['policies']['policy_period'][$index] : '' ?>" required="required"/>
        </div>
    	<div class="form-group">
        	<label>Upload File</label><br />
            <a href="#" class="btn btn--blue upload-policy-file-btn" data-id="<?php echo $product['id']; ?>">Upload File</a>
            <?php
				$whr = array(
					'product_id'=>$product['id'],
					'broker_id'=>$this->session->userdata('id')
				);
				$files = $this->master->getRecords('policy_docs', $whr);
				
				if(count($files) > 0){
					foreach($files as $r=>$value){
						echo '<a class="btn btn--red" href="'.base_url().'uploads/policyd/'.$value['name'].'" target="_blank"><i class="fa fa-paper-clip"></i> '.substr($value['name'], 13).'</a>';
					}
				}
			?>
        </div>
    </div>
</div>
