<h4>Revenue by activity split</h4>

<p>Please specify your revenue by business activity:</p>

<div class="row">
	<div class="col-sm-6">
        <label class="form-control-static">Bookkeeping / accounts preparation / management accounting</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <input type="text" name="professionalIndemnity_<?php echo url_title('Revenue by activity split', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0" max="100" required="required">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
    
	<div class="col-sm-6">
        <label class="form-control-static">Taxation</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <input type="text" name="professionalIndemnity_<?php echo url_title('Revenue by activity split', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0" max="100" required="required">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
    
	<div class="col-sm-6">
        <label class="form-control-static">Auditing</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <input type="text" name="professionalIndemnity_<?php echo url_title('Revenue by activity split', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0" max="100" required="required">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
    
	<div class="col-sm-6">
        <label class="form-control-static">Financial services authorised by a Limited AFSL</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <input type="text" name="professionalIndemnity_<?php echo url_title('Revenue by activity split', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0" max="100" required="required">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
    
	<div class="col-sm-6">
        <label class="form-control-static">Business Valuations</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <input type="text" name="professionalIndemnity_<?php echo url_title('Revenue by activity split', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0" max="100" required="required">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
    
	<div class="col-sm-6">
        <label class="form-control-static">Company secretarial / executor / trusteeship</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <input type="text" name="professionalIndemnity_<?php echo url_title('Revenue by activity split', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0" max="100" required="required">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
    
	<div class="col-sm-6">
        <label class="form-control-static">Forensic Accounting</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <input type="text" name="professionalIndemnity_<?php echo url_title('Revenue by activity split', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0" max="100" required="required">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
    
	<div class="col-sm-6">
        <label class="form-control-static">Receiverships, liquidation or bankruptcies (Non-Public listed)</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <input type="text" name="professionalIndemnity_<?php echo url_title('Revenue by activity split', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0" max="100" required="required">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
    
	<div class="col-sm-6">
        <label class="form-control-static">Self Managed Super Fund Administration</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <input type="text" name="professionalIndemnity_<?php echo url_title('Revenue by activity split', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0" max="100" required="required">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
    
	<div class="col-sm-6">
        <label class="form-control-static">Management / Business Consulting (excluding M&amp;A) </label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <input type="text" name="professionalIndemnity_<?php echo url_title('Revenue by activity split', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0" max="100" required="required">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
    
	<div class="col-sm-6">
        <label class="form-control-static">Accounting software programs</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <input type="text" name="professionalIndemnity_<?php echo url_title('Revenue by activity split', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0" max="100" required="required">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
    
	<div class="col-sm-6">
        <label class="form-control-static">Accounting Lecturing</label>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="input-group input-group-lg">
              <input type="text" name="professionalIndemnity_<?php echo url_title('Revenue by activity split', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0" max="100" required="required">
              <span class="input-group-addon" id="basic-addon2">%</span>
            </div>
        </div>
    </div>
    
	<div class="col-sm-6 text-right">
        <h4 class="text-danger">Total sum must be 100%</h4>
    </div>
	<div class="col-sm-4 text-right">
        <h4><strong>Total: <span class="the-total">0</span>%</strong></h4><p>&nbsp;</p>
    </div>

</div>


<div class="row">

	<div class="col-sm-6">
        <p>In the last 10 years, have any claims for a breach of professional duty been made against the Business, it's predecessors in business or it's current or former partners/principals/directors or employees?</p>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="btn-group radio-toggle-slide" data-toopen="Y" data-totoggle="<?php echo url_title('claims for a breach of professional duty', 'underscore', TRUE) ?>" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="professionalIndemnity_<?php echo url_title('claims for a breach of professional duty', 'underscore', TRUE) ?>" required="required" value="Yes"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="professionalIndemnity_<?php echo url_title('claims for a breach of professional duty', 'underscore', TRUE) ?>" required="required" value="No"> No
              </label>
            </div>
        </div>
    </div>
    
    <div class="col-sm-12 <?php echo url_title('claims for a breach of professional duty', 'underscore', TRUE) ?>" style="display: none">
    	<div class="form-group">
            <label>How many claims have you had?</label>
          <input type="text" name="professionalIndemnity_<?php echo url_title('How many claims have you had', 'underscore', TRUE) ?>" class="form-control input-lg input-number" placeholder="0">
        </div>
    	<div class="form-group">
            <label>What is the maximum incurred amount on any one claim?</label>
          <input type="text" name="professionalIndemnity_<?php echo url_title('maximum incurred amount on any one claim', 'underscore', TRUE) ?>" class="form-control input-lg input-number" placeholder="0">
        </div>
    </div>

</div>
<div class="row">
    
	<div class="col-sm-6">
        <p>After enquiry, are you aware of any circumstances which may result in a claim against the business or any of its Partners, Principals, Directors or employees?</p>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="btn-group radio-toggle-slide" data-toopen="Y" data-totoggle="<?php echo url_title('claim against the business', 'underscore', TRUE) ?>" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="professionalIndemnity_<?php echo url_title('claim against the business', 'underscore', TRUE) ?>" required="required" value="Yes"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="professionalIndemnity_<?php echo url_title('claim against the business', 'underscore', TRUE) ?>" required="required" value="No"> No
              </label>
            </div>
        </div>
    </div>
    
    <div class="col-sm-12 <?php echo url_title('claim against the business', 'underscore', TRUE) ?>" style="display: none">
    	<div class="form-group">
            <label>Number of notifications:</label>
          <input type="text" name="professionalIndemnity_<?php echo url_title('Number of notifications', 'underscore', TRUE) ?>" class="form-control input-lg input-number" placeholder="0">
        </div>
    </div>
    

</div>