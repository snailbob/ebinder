<!--Employee Dishonesty-->
<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Sum Insured - Employee Dishonesty</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="10_<?php echo url_title('Sum Insured Employee Dishonesty', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>Have there been any previous losses for this cover (insured or not)?</p>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="10_<?php echo url_title('any previous losses', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="10_<?php echo url_title('any previous losses', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>Are all employees to be insured for this cover?</p>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="10_<?php echo url_title('all employees to be insured', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="10_<?php echo url_title('all employees to be insured', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>Do all Financial Transactions $1,000 or above require two signatories and/or authorisation by two or more people?</p>
    </div>
	<div class="col-sm-4">
    	<div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="10_<?php echo url_title('require two signatories', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="10_<?php echo url_title('require two signatories', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Number of employees with responsibility for money, negotiable instruments, stock and/or accounts</label>
            <input type="text" name="10_<?php echo url_title('employees with responsibility for money', 'underscore', TRUE) ?>" class="form-control input-lg input-number" placeholder="0" required="required">
        </div>
        
        
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Number of employees primarily engaged as cashiers, treasurers or paymasters</label>
            <input type="text" name="10_<?php echo url_title('employees primarily engaged as cashiers', 'underscore', TRUE) ?>" class="form-control input-lg input-number" placeholder="0" required="required">
        </div>
        
        
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Number of Employees engaged in outdoor money handling, negotiable instruments, stock and/or accounts or delivering goods</label>
            <input type="text" name="10_<?php echo url_title('Employees engaged in outdoor money handling', 'underscore', TRUE) ?>" class="form-control input-lg input-number" placeholder="0" required="required">
        </div>
        
        
    </div>
</div>

<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>All other Employees</label>
            <input type="text" name="10_<?php echo url_title('All other Employees', 'underscore', TRUE) ?>" class="form-control input-lg input-number" placeholder="0" required="required">
        </div>
        
        
    </div>
</div>


