<!--11 Electronic Equipment-->
Please add items to insure
<div class="row row-16">
	<div class="well">
		<div class="row">
        	<div class="col-sm-5 the-items-list-11 hidden">
				<label>Items included</label>

            </div>
            
            <div class="col-sm-12 the-form-col">
            	<div class="the-item-form-11">

            
                    <div class="form-group">
                        <label>Item</label>
                        <select name="11_<?php echo url_title('Item', 'underscore', TRUE) ?>[]" class="form-control input-lg">
                            <option value="" selected="selected">Please select</option>
                            <option value="0" label="Computers (not Laptops)">Computers (not Laptops)</option>
                            <option value="1" label="X-Ray Tomographs">X-Ray Tomographs</option>
                            <option value="2" label="Other Equipment">Other Equipment</option>
                            <option value="3" label="Microwave Ovens">Microwave Ovens</option>
                            <option value="4" label="Communication Systems">Communication Systems</option>
                            <option value="5" label="Electronic Sales">Electronic Sales</option>
                            <option value="6" label="Video Games (excl. domestic situation)">Video Games (excl. domestic situation)</option>
                            <option value="7" label="Word Processors">Word Processors</option>
                            <option value="8" label="Office Equipment">Office Equipment</option>
                            <option value="9" label="Electro-Medical Equipment">Electro-Medical Equipment</option>
                            <option value="10" label="Audio Visual Equipment">Audio Visual Equipment</option>
                            <option value="11" label="Diagnostic Equipment">Diagnostic Equipment</option>
                            <option value="12" label="Tuning Equipment (Automotive)">Tuning Equipment (Automotive)</option>
                            <option value="13" label="Laptops">Laptops</option>
                            <option value="14" label="Medical Scanners">Medical Scanners</option>
                        </select>
            
                    </div>
            
                    <div class="form-group">
                        <label>Description</label>
                        <input type="text" name="11_<?php echo url_title('Description', 'underscore', TRUE) ?>[]" class="form-control input-lg">
                    </div>
            
                    <div class="form-group">
                        <label>Year Purchased</label>
                        <input type="text" name="11_<?php echo url_title('Year Purchased', 'underscore', TRUE) ?>[]" class="form-control input-lg input-number">
                    </div>
            
                    <div class="form-group">
                        <label>Sum Insured</label>
                        <div class="input-group input-group-lg">
                          <span class="input-group-addon">$</span>
                          <input type="text" name="11_<?php echo url_title('Sum Insured', 'underscore', TRUE) ?>[]" class="form-control input-number" placeholder="0">
                        </div>
                    </div>
                </div>

                <div class="form-group text-right">
                    <span class="text-danger hidden">Please add items to this cover</span>
                    <a href="#" class="btn btn--blue add-items-btn" data-form="the-item-form-11" data-container="the-items-list-11">Add</a>
                </div>
                
            </div>
        </div>

    </div>
</div>
