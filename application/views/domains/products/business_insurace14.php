<!--Transit-->
<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Annual Value of Sendings</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="14_<?php echo url_title('Annual Value of Sendings', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
        
        
    </div>
</div>
<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>Sum Insured - Transit</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="14_<?php echo url_title('Sum Insured Transit', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
        
        
    </div>
</div>
<div class="row">
	<div class="col-sm-12">
    	<div class="form-group">
            <label>What is the sum insured required per conveyance?</label>
            <div class="input-group input-group-lg">
              <span class="input-group-addon" id="basic-addon2">$</span>
              <input type="text" name="14_<?php echo url_title('insured required per conveyance', 'underscore', TRUE) ?>" class="form-control input-number" placeholder="0" required="required">
            </div>
        </div>
        
        
    </div>
</div>
