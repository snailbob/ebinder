
<div class="row">
	<div class="col-sm-6">
        <p>Are you a Public Listed company, Partnership or Sole Trader?</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="managementLiability_<?php echo url_title('Public Listed company', 'underscore', TRUE) ?>" required="required" value="Yes"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="managementLiability_<?php echo url_title('Public Listed company', 'underscore', TRUE) ?>" required="required" value="No"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>Does the Company require cover for Insolvency?</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="managementLiability_<?php echo url_title('Company require cover for Insolvency', 'underscore', TRUE) ?>" required="required" value="Yes"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="managementLiability_<?php echo url_title('Company require cover for Insolvency', 'underscore', TRUE) ?>" required="required" value="Yes"> No
              </label>
            </div>
        </div>
    </div>
</div>
