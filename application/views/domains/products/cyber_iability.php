
<div class="row">
	<div class="col-sm-6">
        <p>Do your business activities classify as any of the following: Adult Entertainment, Virtual Currency Exchange or Virtual Currency Miner?</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('activities classify as any', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('activities classify as any', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>Do you process, transmit or store more than 1,000,000 financial transactions or records containing personally identifiable information per year?</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('records containing personally identifiable information', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('records containing personally identifiable information', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>Does the Insured have a full business continuity plan in the event of a potential security failure, including backup and recovery procedures in place for all mission critical systems, data and information assets?</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('full business continuity plan', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('full business continuity plan', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>Do you outsource any part of your network, including storage?</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('outsource any part of your network', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('outsource any part of your network', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>After enquiry of the partners/principals/directors and employees, have there been any claims made against you for matters covered under this policy?</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('any claims made against you for matters', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('any claims made against you for matters', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>After enquiry of the partners/principals/directors and employees is the Firm aware of any Administrative Fines of a type covered by a cyber insurance policy for which this proposal form has been completed?</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('Firm aware of any Administrative Fines', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('Firm aware of any Administrative Fines', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-sm-6">
        <p>After enquiry of the partners/principals/directors and employees, has there ever been or is there pending any actual or alleged fact or circumstance or incident (including any regulatory, governmental, administrative actions or any loss of company or client data or network security breach) which may give rise to a claim against any Insured or trigger a payment under this policy?</p>
    </div>
	<div class="col-sm-6">
        <div class="form-group">
            <div class="btn-group" data-toggle="buttons">
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('pending any actual or alleged fact', 'underscore', TRUE) ?>" value="Yes" required="required"> Yes
              </label>
              <label class="btn btn-primary">
                <input type="radio" name="cyberIability_<?php echo url_title('pending any actual or alleged fact', 'underscore', TRUE) ?>" value="No" required="required"> No
              </label>
            </div>
        </div>
    </div>
</div>
