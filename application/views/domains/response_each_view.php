
  
  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
  
      <div class="row">
  
      
        <div class="col-lg-12">

      
           
        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>      
        <?php } ?>
        
        <div class="module_header">
            <i class="fa fa-user fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
        </div>


		<form id="note_comment_form">
        <div class="panel-group panel-group-products" id="accordion" role="tablist" aria-multiselectable="true">

          <div class="panel panel-white">
            <div class="panel-heading" role="tab" id="headingForst" data-toggle="collapse" data-parent="#accordionx" data-target="#collapseForst">
              <h3 class="heading-title">
              	Business Details
              </h3>
              	
            </div>    
            <div id="collapseForst" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingForst">
              <div class="panel-body">
              	<p>
                	<strong>Customer Name</strong><br />
                    <?php echo $responses['user_info']['first'].' '.$responses['user_info']['last']?>
                </p>

              	<p>
                	<strong>Email</strong><br />
                    <?php echo $responses['user_info']['email']?>
                </p>

              	<p>
                	<strong>Location</strong><br />
                    <?php echo $responses['answer']['location']?>
                </p>

              	<p>
                	<strong>Industry</strong><br />
                    <?php
						$occ_id = ($responses['user_info']['occupation_id']) ? $responses['user_info']['occupation_id'] : $responses['user_info']['occupation'];
						$ind_id = $this->common->db_field_id('q_business_specific', 'category_id', $occ_id);
						$ind_name = $this->common->db_field_id('q_business_categories', 'category_name', $ind_id);
						$occ_name = $this->common->db_field_id('q_business_specific', 'specific_name', $occ_id);
					echo $ind_name?>
                </p>
              	<p>
                	<strong>Occupation</strong><br />
                    <?php echo $occ_name; ?>
                </p>
              	<p>
                	<strong>Insurance Products to Cover</strong><br />
                    <?php echo $responses['user_info']['prod_text']?>
                </p>

              </div>
              
            </div>
          </div>
                   
          <?php 
		  $count = 1;
		  foreach($responses['prod_arr'] as $r=>$value) {
			 // if($value != '5'){
			  ?>
          <div class="panel panel-white">
            <div class="panel-heading <?php // echo ($count != 1) ? 'collapsed' : ''; ?>" role="tab" id="heading<?php echo $count ?>" data-toggle="collapse" data-parent="#accordionx" data-target="#collapse<?php echo $count ?>">
              <h3 class="heading-title">
              	
              		
                  <?php echo $value['title']?>
              </h3>
            </div>
            <div id="collapse<?php echo $count ?>" class="panel-collapse collapse <?php echo 'in'; //($count != 1) ? '' : 'in'; ?>" role="tabpanel" aria-labelledby="heading<?php echo $count ?>">
              <div class="panel-body">
                <?php

					//full question
					$fullquestion = $this->common->q_select_format($value['pval_text'][0]);
					$qcount = 0;
					foreach($value['answers'] as $aa =>$ans){
						$strip_first = explode('_', $ans['name']);
						$the_question = '';
						for($i = 1; $i < count($strip_first); $i++ ){
							$the_question .= $strip_first[$i].' ';
						}
						$the_question = str_replace ('[]', '', $the_question);
						
						$final_quest = (isset($fullquestion[$qcount])) ? $fullquestion[$qcount] : $the_question;
						
						$final_quest = (($value['pval_text'][0] == '6_' || $value['pval_text'][0] == '11_' || $value['pval_text'][0] == '12_')) ? $the_question : $final_quest;
						?>
                        
              	<p class="<?php echo ($ans['value'] == '') ? 'hidden' : ''; ?>">
                	<strong><?php echo ucwords($final_quest); ?></strong><br />
                    <?php echo $ans['value'] ?>
                </p>
                        
                        <?php	
						//add horizontal line for each group
						if($value['pval_text'][0] == '6_' || $value['pval_text'][0] == '11_' || $value['pval_text'][0] == '12_'){
							$mod = 4;
							
							if(($qcount%$mod) == 3){
								echo '<hr>';
							}
						}
						
										
						$qcount++;
					}
					
				?>
              </div>
            </div>
          </div>
          <?php $count++; }  //} ?>


          <div class="panel panel-white">
            <div class="panel-heading" role="tab" id="headingHistory" data-toggle="collapse" data-parent="#accordionx" data-target="#collapseHistory">
              <h3 class="heading-title">
              	History
              </h3>
              	
            </div>    
            <div id="collapseHistory" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingHistory">
              <div class="panel-body">
              	<p>
                	<strong>In the last 10 years, has your business or you or any partner or director:
 Had any business insurance/liability claims?</strong><br />
                    <?php echo $responses['answer']['history_in_the_last_10_years']?>
                </p>

              	<p>
                	<strong>As at today's date does the insured have Public Liability or Business Insurance currently in force that has been paid for?</strong><br />
                    <?php echo $responses['answer']['history_have_public_liability']?>
                </p>

              	<p>
                	<strong>As at today's date does the insured have Management Liability Insurance currently in force that has been paid for?</strong><br />
                    <?php echo $responses['answer']['history_have_management_liability_insurance']?>
                </p>

              	<p>
                	<strong>As at today's date does the insured have Cyber Liability Insurance currently in force that has been paid for?</strong><br />
                    <?php echo $responses['answer']['history_have_cyber_liability_insurance']?>
                </p>



              </div>
              
            </div>
          </div>
          <div class="panel panel-white">
            <div class="panel-heading" role="tab" id="headingNotes" data-toggle="collapse" data-parent="#accordionx" data-target="#collapseNotes">
              <h3 class="heading-title">
              	Notes to underwriter
              </h3>
              	
            </div>    
            <div id="collapseNotes" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingNotes">
              <div class="panel-body">
              	<div class="form-group">
              		<input type="hidden" name="id" value="<?php echo $responses['id'] ?>">
              		<textarea name="notes" rows="5" class="form-control" placeholder="Notes to underwriter comment" required="required"><?php echo $responses['comment'] ?></textarea>
                </div>


              </div>
              
            </div>
          </div>
                
        </div><!--.panel-group-->
    
          <div class="panel panel-white">


              <div class="panel-body">
                <button type="submit" class="btn btn--orange">Submit</button>
                <a href="#" class="btn btn--blue" onclick="window.print(); return false;">Print Copy</a>
    
    
              </div>
              
          </div>
		</form>
    </div>
</div>

    </div>

    <!-- End of Contents -->
    