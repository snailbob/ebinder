
  
  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
  
      <div class="row">
  
      
        <div class="col-lg-12">

      


		<div class="container">
            <div class="row">
                <div class="col-sm-12">
                            
                    <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
                    <div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
                        <?php echo $success.$danger.$info ?>
                    </div>      
                    <?php } ?>
                    
                    <div class="module_header">
                        <i class="fa fa-gear fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
                    </div>
                
        
                    <div class="panel panel-border-grey">
                            
                            <div class="row">
                                <div class="col-sm-4 col-md-3">
                                    
                                    <div class="list-group list-group-rabbit">
                                      <a href="<?php echo base_url().'panel/setup/profile'?>"class="list-group-item">
                                        Profile
                                      </a>
                                      <a href="<?php echo base_url().'panel/setup/logo'?>"class="list-group-item  ">
                                        Logo
                                      </a>
                                      <a href="<?php echo base_url().'panel/setup/quote_slip' ?>"class="list-group-item active">
                                        Quote Slip
                                      </a>
                                      <a href="<?php echo base_url().'panel/setup/payment_method' ?>"class="list-group-item ">
                                        Payment Method
                                      </a>
                                    </div>                                
                                </div>
                                <div class="col-sm-8 col-md-9 border-left">
                                    
                                      <div class="padding">
                            
                                            <div class="summernote">
                                                <?php
                                                $yum = array(
                                                    '[[logolink]]',
                                                    '[[signlink]]'
                                                );
                                                $subb = array(
                                                    $this->common->logo($user_sess['id']), 
                                                    base_url().'assets/img/cert/cert-signiture.jpg'
                                                );
                                                
                                                echo str_replace($yum, $subb, $admin_info[0]['certificate_template']);?>
                                            </div>
                                            <p style="margin-top: 15px;">
                                                <a href="<?php echo base_url().'landing/certificate'?>" class="btn btn-primary pull-right" target="_blank">View Template as PDF</a>
                                                <a href="#" class="btn btn-warning save_certificate_btn">Save Changes</a>
                                            </p>
                            

                                      </div>
                                
                                </div>
                            </div>
                            
                    </div>
            
                </div>
            </div>
        </div>                                                                            
                 
                 
    </div>
</div>

    </div>

    <!-- End of Contents -->
    