
  
  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
  
      <div class="row">
  
      
        <div class="col-lg-12">

      
      
        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>      
        <?php } ?>

        
        <div class="module_header">
            <i class="fa fa-user fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <ul class="module_actions right hidden">
                <li>
                    <a class="module_action btn--red" href="<?php echo base_url() ?>panel/quote">
                        <div class="module_action_icon">
                            <i class="fa fa-user-plus"></i>
                        </div>
                        <span class="module_action_text text-uppercase"> Add <?php echo $singular ?></span>
                    </a>
                </li>
            </ul>
   
        </div>


     <div class="panel panel-default">

      
        <div class="panel-heading">
              <a class="btn btn-primary pull-right" href="<?php echo base_url().'panel/quote' ?>"><i class="fa fa-plus"></i> <?php echo $singular_title; ?></a>


          
            <div  class="panel-title">
                  <h4>Manage <?php echo $title; ?></h4>
            </div>
          </div>



        	<div class="panel-bodyx">
                
                        <?php if(count($biz_referral) > 0) { ?>
                
                      
                        <div class="table_search">
                            <input type="text" class="form-control input_table_search" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>  

                        
						<div class="table-responsivex">
                        
                        <table class="table table-striped table-hover table-datatable table-policies">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <th>Quote ID</th>
                              <th>Policy ID</th>
                              <th>Name</th>
                              <th>Contact No.</th>
                              <th>Email</th>
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>                        
                        
                        <?php
							$count = 1;
							foreach($biz_referral as $u=>$value){
								$contact = unserialize($value['customer']);
								
						?>
                        
                            <tr>
                             
                              <td class="hidden"><?php echo $count; ?></td>
                              <td><?php echo $this->common->quote_id_format($value['id']); ?></td>
                              
                              <td><?php echo ($value['bound'] == 'Y') ? $this->common->policy_id_format($value['id']) : 'NA'; ?></td>
                              
                              <td><?php echo $contact[0]['value'] ?></td>
                             
                              <td><?php echo $contact[1]['value'] ?></td>
                             
                              <td><?php echo $contact[2]['value'] ?></td>


                              <td>
                                                                  
                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                        
                                        <li><a href="<?php echo base_url().'panel/managequote/'.$value['id']?>" data-id='<?php echo $value['id'];?>'>Insurance details</a></li>
                                        
                                        <?php if($value['bound'] != 'Y') {?>
                                        <li><a href="javascript:;" class="biz-bind-now" data-id='<?php echo $value['id'];?>'>Bind</a></li>
                                        <?php } ?>
                                        <li><a href="mailto:<?php echo $contact[2]['value'] ?>">Contact Now</a></li>
                                      </ul>
                                    </div>                              
                              
                              
                              </td>
                            </tr>
                        
                        <?php
								$count++;
							}
						?>


                          
                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->
                          
                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';	
						}?>                
                

            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->
    