
  
  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
  
      <div class="row">
  
      
        <div class="col-lg-12">

      

		<div class="container">
            <div class="row">
                <div class="col-sm-10">
                            
                    <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
                    <div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
                        <?php echo $success.$danger.$info ?>
                    </div>      
                    <?php } ?>
                    
                    <div class="module_header">
                        <i class="fa fa-gear fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
                    </div>
                
        
                    <div class="panel panel-border-grey">
                            
                            <div class="row">
                                <div class="col-sm-4 col-md-3">
                                    
                                    <div class="list-group list-group-rabbit">
                                      <a href="<?php echo base_url().'panel/setup/profile'?>"class="list-group-item">
                                        Profile
                                      </a>
                                      <a href="<?php echo base_url().'panel/setup/logo'?>"class="list-group-item  active">
                                        Logo
                                      </a>
                                      <a href="<?php echo base_url().'panel/setup/quote_slip' ?>"class="list-group-item ">
                                        Quote Slip
                                      </a>
                                      <a href="<?php echo base_url().'panel/setup/payment_method' ?>"class="list-group-item hidden ">
                                        Payment Method
                                      </a>
                                    </div>                                
                                </div>
                                <div class="col-sm-8 col-md-9 border-left">
                                    
                                      <div class="padding">
                                      
                                                                  
                                           <p class="text-center">
                                            <img src="<?php echo $this->common->logo($user_sess['id']) ?>" class="" alt="logo" title="logo" style="width: 146px;"/><br /><br />
                            
                            
                                                <a href="#" class="btn btn-primary" onclick="$('#myfile').click(); return false;"><i class="fa fa-upload"></i> Update Logo</a><br /><span class="text-muted">310 x 64px recommended</span>
                            
                                           </p> 
                            

                                      </div>
                                
                                </div>
                            </div>
                            
                    </div>
            
                </div>
            </div>
        </div>                                      

        <?php /*?><div class="panel">
        	<div class="panel-body" style="background : #ccc">
       

            </div>
        </div><?php */?>
    </div>
</div>

    </div>

    <!-- End of Contents -->
    
<div class="row hidden">
<form id="myFormAvatar" action="<?php echo base_url().'settings/update_avatar' ?>" method="post" enctype="multipart/form-data">
     <input type="file" size="60" id="myfile" name="myfile" class="myfile-profile" onchange="$('#myFormAvatar').submit();">
     <input type="hidden" id="img_type" name="img_type" value="logo" data-id="">
     <input type="hidden" id="avatar_name" name="avatar_name" value="">
     
     <input type="submit" class="hidden" value="Ajax File Upload">
 </form>                                                   
</div>

<!-- Modal -->
<div class="modal fade" id="cropImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<p class="lead">Crop</p>
        
        <div class="bootstrap-modal-cropper"><img src="<?php echo base_url().'assets/img/avatar/background.png' ?>" class="img-responsive"/></div>
	
        

      </div>
      <div class="modal-footer">
        <a class="btn btn-primary sav_crop">Save changes</a>
      </div>
    </div>
  </div>
</div>