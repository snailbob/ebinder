
  
  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
  
      <div class="row">
  
      
        <div class="col-lg-12">

       
           
        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>      
        <?php } ?>
        
        <div class="module_header">
            <i class="fa fa-exchange fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
        </div>

        <div class="panel panel-default">

          <div class="panel-heading">
              <a href="#myModal" class="btn btn-primary pull-right hidden" href="#" onclick="$('#myModal').find('.form-control').val('')" data-toggle="modal" data-controls-modal="#myModal"><i class="fa fa-plus"></i> <?php echo $singular_title; ?></a>

          
            <div  class="panel-title">
                  <h4>Manage <?php echo $title; ?></h4>
            </div>
          </div>




        	<div class="panel-bodyx">
              <?php if(count($transactions) > 0) { ?>
          
                        <div class="table_search">
                            <input type="text" class="form-control input_table_search" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>  

                <table class="table table-striped table-hover table-datatable">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th></th>

                    </tr>
                  </thead>
                  <tbody>
                  	<?php

        						$count = 1;
        						foreach($transactions as $r=>$value){ ?>
                        
                        <tr>
                          <td><?php echo $count?></td>
                          <td><?php echo '$'.$value['arr']['amount']. ' was charged last '.$value['date_added_format'].' to your account.'?></td>
                        </tr>
                        
                    
                                
        						<?php
        						$count++;
                  }
        						?>


                  </tbody>
                </table>   
                   
                <?php

                 } else { ?>

                    <div class="panel-body">
                      
                        <p class="text-muted text-center">
                          Nothing to show you.
                        </p>
                    </div>
                <?php } ?>




            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->
    