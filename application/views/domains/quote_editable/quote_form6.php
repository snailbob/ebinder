
        <div class="row">
            <div class="col-sm-12">
        
                <div class="wellx">
                    <div class="row">
                        <div class="col-sm-12 the-items-list-1">
							<?php $thequest = 'Insured Name'; ?>
                
                            <?php if(count($homebiz6['6_insured_name']) > 0){
								foreach($homebiz6['6_insured_name'] as $r=>$value) {
									if($value != ''){
								?>
                
                            <div class="well well-sm panel">
                            	<div class="">
                            	<a href="#" class="pull-right remove-item-btn" data-form="the-item-form-1" data-target="the-item-form-1-clone-copy-<?php echo $r ?>">Remove</a>
                                Insured Name: <?php echo $value ?><br>
                                </div>
                            </div>
                            <div class="the-item-form-1-clone hidden the-item-form-1-clone-copy-<?php echo $r ?>">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" name="6_<?php echo url_title($thequest, 'underscore', TRUE) ?>[]" class="form-control" value="<?php echo $value ?>">

                                    </div>
                                </div>
                         
                            </div>
                            <?php }
								}							
							} ?>
                            
                        </div>
                        
                        <div class="col-sm-12 the-form-col">
                            <div class="the-item-form-1">
                            	<div class="row">
                                	<div class="col-sm-6">
                                        <div class="form-group">
											<?php $thequest = 'Insured Name'; ?>
                                            <label><?php echo $thequest; ?></label>
                                        </div>
                                    </div>
                                	<div class="col-sm-4">
                                        <div class="form-group">
                                            
                                            <input type="text" name="6_<?php echo url_title($thequest, 'underscore', TRUE) ?>[]" class="form-control" value="">
                                        </div>
            
                                        
                                    </div>
                                	<div class="col-sm-2">
                                        <div class="form-group">
                                            <a href="#" class="pull-right btn btn--blue add-items-btn-dyna" data-form="the-item-form-1" data-container="the-items-list-1">+</a>
                                            <p class="text-danger hidden">Please add items to this cover</p>
                                            
                                        </div>
                                    </div>
                                </div>
                     
                            </div>
                            
                        </div>
                    </div>
            
            
                </div>
        
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
    
                    <?php
                    $thequest_id = '109';
                    $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                    $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                    $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                    $thequest2 = url_title($thequest, 'underscore', TRUE);
                     ?>
                     
                   
                    <label>
                        <span>
                            <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                            <div class="text_form hidden">
                                <div class="form-group">
                                    <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                    <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                    <input type="hidden" name="field_name" value="question" />
                                    <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                    <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                </div>
                            </div>                                                
                        </span>
                        
                        <br />
                        
                        <span>
                            <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                            <div class="text_form hidden">
                                <div class="form-group">
                                    <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                    <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                    <input type="hidden" name="field_name" value="tooltip" />
                                    <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                    <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                </div>
                            </div>
                        
                        </span>
                                                                         
                    </label>    
    


                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
              
                  <input type="text" name="6_<?php echo url_title($thequest2, 'underscore', TRUE) ?>" class="form-control" placeholder="" required="required" value="<?php echo (isset($homebiz6['6_'.$thequest2])) ? $homebiz6['6_'.$thequest2] : ''?>">
                </div>
            </div>
        </div>
  


        <div class="row">
            <div class="col-sm-12">
        
                <div class="wellx">
                    <div class="row">
                        <div class="col-sm-12 the-items-list-2">

							<?php $thequest = 'Interested Parties'; ?>
                
                            <?php if(count($homebiz6['6_interested_parties']) > 0){
								foreach($homebiz6['6_interested_parties'] as $r=>$value) {
									if($value != ''){
								?>
                
                            <div class="well well-sm panel">
                            	<div class="">
                            	<a href="#" class="pull-right remove-item-btn" data-form="the-item-form-2" data-target="the-item-form-1-clone-copy2-<?php echo $r ?>">Remove</a>
                                Interested Parties: <?php echo $value ?><br>
                                </div>
                            </div>
                            <div class="the-item-form-2-clone hidden the-item-form-1-clone-copy2-<?php echo $r ?>">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <input type="text" name="6_<?php echo url_title($thequest, 'underscore', TRUE) ?>[]" class="form-control" value="<?php echo $value ?>">

                                    </div>
                                </div>
                         
                            </div>
                            <?php }
								}
							} ?>
                            

                        </div>
                        
                        <div class="col-sm-12 the-form-col">
                            <div class="the-item-form-2">
                            	<div class="row">
                                	<div class="col-sm-6">
                                        <div class="form-group">
											<?php $thequest = 'Interested Parties'; ?>
                                            <label><?php echo $thequest; ?></label>
                                        </div>
                                    </div>
                                	<div class="col-sm-4">
                                        <div class="form-group">
                                            
                                            <input type="text" name="6_<?php echo url_title($thequest, 'underscore', TRUE) ?>[]" class="form-control">
                                        </div>

            
                                        
                                    </div>
                                	<div class="col-sm-2">
                                        <div class="form-group">
                                            <a href="#" class="pull-right btn btn--blue add-items-btn-dyna" data-form="the-item-form-2" data-container="the-items-list-2">+</a>
                                            <p class="text-danger hidden">Please add items to this cover</p>
                                            
                                        </div>
                                    </div>
                                </div>
                     
                            </div>
                            
                        </div>
                    </div>
            
            
                </div>
        
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
    
                    <?php
                    $thequest_id = '110';
                    $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                    $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                    $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                    $thequest2 = url_title($thequest, 'underscore', TRUE);
                     ?>
                     
                   
                    <label>
                        <span>
                            <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                            <div class="text_form hidden">
                                <div class="form-group">
                                    <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                    <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                    <input type="hidden" name="field_name" value="question" />
                                    <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                    <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                </div>
                            </div>                                                
                        </span>
                        
                        <br />
                        
                        <span>
                            <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                            <div class="text_form hidden">
                                <div class="form-group">
                                    <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                    <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                    <input type="hidden" name="field_name" value="tooltip" />
                                    <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                    <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                </div>
                            </div>
                        
                        </span>
                                                                         
                    </label>    
    

            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="6_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz6['6_'.$thequest2])) { echo ($homebiz6['6_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="6_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz6['6_'.$thequest2])) { echo ($homebiz6['6_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>
                </div>
            </div>
        </div>
        