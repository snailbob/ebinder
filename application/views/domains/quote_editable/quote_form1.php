								<?php if($this->session->userdata('for_renewal') == 'endorse') { ?>
                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?php
                                            $thequest = 'Date of endorsement';
                                            $thequest2 = url_title($thequest, 'underscore', TRUE);
                                             ?>
                                            <label><strong><?php echo $thequest; ?></strong></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                          <input type="date" name="1_<?php echo $thequest2; ?>" class="form-control" placeholder="" required="required" value="<?php echo (isset($homebiz1['1_'.$thequest2])) ? $homebiz1['1_'.$thequest2] : ''?>">
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>


                                <div class="row">
                                    <div class="col-sm-12">
                                    	<strong>Revenue Details</strong>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?php
                                            $thequest_id = '86';
											$question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                                            $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                                            $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                                            $thequest2 = url_title($thequest, 'underscore', TRUE);
                                             ?>


                                            <label>
                                            	<span>
                                                    <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                                                    <div class="text_form hidden">
                                                        <div class="form-group">
                                                            <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                                            <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                                            <input type="hidden" name="field_name" value="question" />
                                                            <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                                            <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                                        </div>
                                                    </div>
                                                </span>

                                                <br />

                                                <span>
                                                    <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thetip ?></span>
                                                    <div class="text_form hidden">
                                                        <div class="form-group">
                                                            <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                                            <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                                            <input type="hidden" name="field_name" value="tooltip" />
                                                            <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                                            <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                                        </div>
                                                    </div>

                                                </span>

                                            </label>






                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                          <input type="text" name="1_<?php echo $thequest2; ?>" class="form-control input-number" placeholder="" required="required" value="<?php echo (isset($homebiz1['1_'.$thequest2])) ? $homebiz1['1_'.$thequest2] : ''?>">
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">

                                            <?php
                                            $thequest_id = '87';
											$question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                                            $thequest = (!empty($question)) ? $question[0]['question'] : '';
                                            $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                                            $thequest2 = url_title($thequest, 'underscore', TRUE);
                                             ?>


                                            <label>
                                            	<span>
                                                    <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                                                    <div class="text_form hidden">
                                                        <div class="form-group">
                                                            <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                                            <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                                            <input type="hidden" name="field_name" value="question" />
                                                            <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                                            <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                                        </div>
                                                    </div>
                                                </span>

                                                <br />

                                                <span>
                                                    <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                                                    <div class="text_form hidden">
                                                        <div class="form-group">
                                                            <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                                            <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                                            <input type="hidden" name="field_name" value="tooltip" />
                                                            <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                                            <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                                        </div>
                                                    </div>

                                                </span>

                                            </label>


                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                          <input type="text" name="1_<?php echo $thequest2; ?>" class="form-control" placeholder="" required="required" value="<?php echo (isset($homebiz1['1_'.$thequest2])) ? $homebiz1['1_'.$thequest2] : ''?>">
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-sm-12">
                                    	<strong>Employee Number</strong>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">

                                            <?php
                                            $thequest_id = '88';
											$question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                                            $thequest = (!empty($question)) ? $question[0]['question'] : '';
                                            $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                                            $thequest2 = url_title($thequest, 'underscore', TRUE);
                                             ?>


                                            <label>
                                            	<span>
                                                    <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                                                    <div class="text_form hidden">
                                                        <div class="form-group">
                                                            <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                                            <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                                            <input type="hidden" name="field_name" value="question" />
                                                            <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                                            <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                                        </div>
                                                    </div>
                                                </span>

                                                <br />

                                                <span>
                                                    <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                                                    <div class="text_form hidden">
                                                        <div class="form-group">
                                                            <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                                            <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                                            <input type="hidden" name="field_name" value="tooltip" />
                                                            <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                                            <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                                        </div>
                                                    </div>

                                                </span>

                                            </label>

                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                          <input type="text" name="1_<?php echo $thequest2; ?>" class="form-control input-number" placeholder="" required="required" value="<?php echo (isset($homebiz1['1_'.$thequest2])) ? $homebiz1['1_'.$thequest2] : ''?>">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-10">
                                        <div class="well well-sm">

                                            <?php
                                            $thequest_id = '89';
											$question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                                            $thequest = (!empty($question)) ? $question[0]['question'] : '';
                                            $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                                            $thequest2 = 'occupation_find'; //url_title($thequest, 'underscore', TRUE);
                                             ?>


                                            <label>
                                            	<span>
                                                    <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                                                    <div class="text_form hidden">
                                                        <div class="form-group">
                                                            <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                                            <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                                            <input type="hidden" name="field_name" value="question" />
                                                            <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                                            <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                                        </div>
                                                    </div>
                                                </span>

                                                <br />

                                                <span>
                                                    <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                                                    <div class="text_form hidden">
                                                        <div class="form-group">
                                                            <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                                            <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                                            <input type="hidden" name="field_name" value="tooltip" />
                                                            <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                                            <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                                        </div>
                                                    </div>

                                                </span>

                                            </label>




                                            <div class="form-groupx">
                                                <input type="text" class="form-control" name="<?php echo $thequest2 ?>" placeholder="Occupation" value="<?php echo (isset($homebiz1[''.$thequest2])) ? $homebiz1[''.$thequest2] : ''?>" required="required"/>

                                            <?php
                                            $thequest2 = 'occupation_id'; //url_title($thequest, 'underscore', TRUE);
                                             ?>

                                                <input type="hidden" name="<?php echo $thequest2 ?>" value="<?php echo (isset($homebiz1[''.$thequest2])) ? $homebiz1[''.$thequest2] : ''?>"/>
                                                <div class="occupation-suggest hidden">
                                                    <div class="list-group list-group-suggest">
                                                    </div>


                                                </div>


                                            </div>

                                        </div>

                                    </div>
                                </div>



                                <div class="row">
                                	<?php foreach($the_states as $thr=>$thvalue) { ?>

                                            <?php
                                            $thequest_id = '90';
											$question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                                            $thequest = (!empty($question)) ? $question[0]['question'] : '';
                                            $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                                            $thequest2 = url_title($thequest, 'underscore', TRUE);
                                             ?>

                                           	<?php if($thr == 0) { ?>


	                                    <div class="col-sm-6">
	                                        <div class="form-group">


																			      <label>
                                            	<span>
                                                    <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                                                    <div class="text_form hidden">
                                                        <div class="form-group">
                                                            <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                                            <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                                            <input type="hidden" name="field_name" value="question" />
                                                            <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                                            <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                                        </div>
                                                    </div>
                                                </span>

                                                <br />

                                                <span>
                                                    <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                                                    <div class="text_form hidden">
                                                        <div class="form-group">
                                                            <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                                            <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                                            <input type="hidden" name="field_name" value="tooltip" />
                                                            <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                                        </div>
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                                            <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                                        </div>
                                                    </div>

                                                </span>

                                            </label>


	                                        </div>
	                                    </div>

																			<div class="clearfix"></div>


                                            <?php } ?>

																		<?php echo ($thr == 5) ? '<div class="clearfix"></div>' : '' ?>

		                                    <div class="col-sm-2">
		                                        <div class="form-group">
		                                        	<label><strong><?php echo $thvalue ?></strong></label>
		                                          <input type="text" name="1_<?php echo $thequest2; ?>[]" class="form-control input-number" placeholder="" value="<?php echo (isset($homebiz1['1_'.$thequest2])) ? $homebiz1['1_'.$thequest2][$thr] : ''?>" max="100" min="0">
		                                        </div>
		                                    </div>


                                    <?php } ?>
                                    <div class="col-sm-4">
																				<label>&nbsp;</label>

                                        <div class="form-control-static">

                                          Total must be 100% <strong><span class="the-total text-info"></span></strong>
                                        </div>
                                    </div>
                                </div>
