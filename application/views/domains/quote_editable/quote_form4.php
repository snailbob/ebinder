        
        <div class="row">
            <div class="col-sm-6">

				<?php
                $thequest_id = '99';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>
                 
               
                <label>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>                                                
                    </span>
                    
                    <br />
                    
                    <span>
                        <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="tooltip" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    
                    </span>
                                                                     
                </label>    
				
                
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="4_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz4['4_'.$thequest2])) { echo ($homebiz4['4_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="4_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz4['4_'.$thequest2])) { echo ($homebiz4['4_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>
                    
                </div>
            </div>
        </div>
        
                
        <div class="row">
            <div class="col-sm-6">


				<?php
                $thequest_id = '100';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>
                 
               
                <label>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>                                                
                    </span>
                    
                    <br />
                    
                    <span>
                        <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="tooltip" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    
                    </span>
                                                                     
                </label>    


            </div>
            <div class="col-sm-4">
                <div class="form-group">
                
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="4_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz4['4_'.$thequest2])) { echo ($homebiz4['4_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="4_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz4['4_'.$thequest2])) { echo ($homebiz4['4_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>
                    
                </div>
            </div>
        </div>
        
                
        <div class="row">
            <div class="col-sm-6">


				<?php
                $thequest_id = '101';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>
                 
               
                <label>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>                                                
                    </span>
                    
                    <br />
                    
                    <span>
                        <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="tooltip" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    
                    </span>
                                                                     
                </label>    


            </div>
            <div class="col-sm-4">
                <div class="form-group">
                
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="4_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz4['4_'.$thequest2])) { echo ($homebiz4['4_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="4_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz4['4_'.$thequest2])) { echo ($homebiz4['4_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>
                    
                </div>
            </div>
        </div>
        
        
                
        <div class="row">
            <div class="col-sm-6">


				<?php
                $thequest_id = '102';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>
                 
               
                <label>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>                                                
                    </span>
                    
                    <br />
                    
                    <span>
                        <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="tooltip" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    
                    </span>
                                                                     
                </label>    


            </div>
            <div class="col-sm-4">
                <div class="form-group">
                
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="4_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz4['4_'.$thequest2])) { echo ($homebiz4['4_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="4_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz4['4_'.$thequest2])) { echo ($homebiz4['4_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>
                    
                </div>
            </div>
        </div>
        
        
                
        <div class="row">
            <div class="col-sm-6">


				<?php
                $thequest_id = '103';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>
                 
               
                <label>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>                                                
                    </span>
                    
                    <br />
                    
                    <span>
                        <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="tooltip" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    
                    </span>
                                                                     
                </label>    


            </div>
            <div class="col-sm-4">
                <div class="form-group">
                
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="4_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz4['4_'.$thequest2])) { echo ($homebiz4['4_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="4_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz4['4_'.$thequest2])) { echo ($homebiz4['4_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>
                    
                </div>
            </div>
        </div>
        
        
                
        <div class="row">
            <div class="col-sm-6">


				<?php
                $thequest_id = '104';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>
                 
               
                <label>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>                                                
                    </span>
                    
                    <br />
                    
                    <span>
                        <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="tooltip" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    
                    </span>
                                                                     
                </label>    


            </div>
            <div class="col-sm-4">
                <div class="form-group">
                
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="4_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz4['4_'.$thequest2])) { echo ($homebiz4['4_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="4_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz4['4_'.$thequest2])) { echo ($homebiz4['4_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>
                    
                </div>
            </div>
        </div>
        
        