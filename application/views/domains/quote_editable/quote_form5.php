<?php if($this->session->userdata('customer_quote') == '') { ?>        
        <div class="row">
            <div class="col-sm-12">
            	<p><strong>Coverage Selection</strong></p>
                
            </div>
            <div class="col-sm-6">


				<?php
                $thequest_id = '105';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>
                 
               
                <label>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>                                                
                    </span>
                    
                    <br />
                    
                    <span>
                        <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="tooltip" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    
                    </span>
                                                                     
                </label>    


            </div>
            <div class="col-sm-4">
                <div class="form-group">
                
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="5_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz5['5_'.$thequest2])) { echo ($homebiz5['5_'.$thequest2] == '$1M') ? 'checked="checked"' : ''; }?>><span></span></div>
                        $1M
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="5_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz5['5_'.$thequest2])) { echo ($homebiz5['5_'.$thequest2] != '$2M') ? 'checked="checked"' : ''; }?>><span></span></div>
                        $2M
                    </label>
                    
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="5_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz5['5_'.$thequest2])) { echo ($homebiz5['5_'.$thequest2] != '$5M') ? 'checked="checked"' : ''; }?>><span></span></div>
                        $5M
                    </label>
                    
                </div>
            </div>
        </div>

                
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
					<?php
                    $thequest_id = '106';
                    $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                    $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                    $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                    $thequest2 = url_title($thequest, 'underscore', TRUE);
                     ?>
                     
                   
                    <label>
                        <span>
                            <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                            <div class="text_form hidden">
                                <div class="form-group">
                                    <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                    <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                    <input type="hidden" name="field_name" value="question" />
                                    <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                    <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                </div>
                            </div>                                                
                        </span>
                        
                        <br />
                        
                        <span>
                            <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                            <div class="text_form hidden">
                                <div class="form-group">
                                    <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                    <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                    <input type="hidden" name="field_name" value="tooltip" />
                                    <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                    <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                </div>
                            </div>
                        
                        </span>
                                                                         
                    </label>    


                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="5_<?php echo $thequest2 ?>" value="Yes" required="required" <?php if (isset($homebiz5['5_'.$thequest2])) { echo ($homebiz5['5_'.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="5_<?php echo $thequest2 ?>" value="No" required="required" <?php if (isset($homebiz5['5_'.$thequest2])) { echo ($homebiz5['5_'.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>
                    
                </div>
            </div>
        </div>
        

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
    
                    <?php
                    $thequest_id = '107';
                    $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                    $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                    $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                    $thequest2 = url_title($thequest, 'underscore', TRUE);
                     ?>
                     
                   
                    <label>
                        <span>
                            <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                            <div class="text_form hidden">
                                <div class="form-group">
                                    <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                    <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                    <input type="hidden" name="field_name" value="question" />
                                    <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                    <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                </div>
                            </div>                                                
                        </span>
                        
                        <br />
                        
                        <span>
                            <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                            <div class="text_form hidden">
                                <div class="form-group">
                                    <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                    <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                    <input type="hidden" name="field_name" value="tooltip" />
                                    <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                    <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                </div>
                            </div>
                        
                        </span>
                                                                         
                    </label>    
    
    

                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                  <input type="text" name="5_<?php echo $thequest2; ?>" class="form-control promo-code-input" placeholder="" value="<?php echo (isset($homebiz5['5_'.$thequest2])) ? $homebiz5['5_'.$thequest2] : ''?>">
                    <p class="help-block"></p>
                </div>
            </div>
        </div>
        
        <?php
        $thequest = 'discount';
        $thequest2 = url_title($thequest, 'underscore', TRUE);
        ?>
        <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo (isset($homebiz5['5_'.$thequest2])) ? $homebiz5['5_'.$thequest2] : ''?>">

        
        
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">

    
                    <?php
                    $thequest_id = '108';
                    $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                    $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                    $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                    $thequest2 = url_title($thequest, 'underscore', TRUE);
                     ?>
                     
                   
                    <label>
                        <span>
                            <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                            <div class="text_form hidden">
                                <div class="form-group">
                                    <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                    <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                    <input type="hidden" name="field_name" value="question" />
                                    <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                    <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                </div>
                            </div>                                                
                        </span>
                        
                        <br />
                        
                        <span>
                            <i class="fa fa-info-circle text-muted small"></i> <span class="price-text small text-muted" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo (empty($thetip)) ? 'Add help text' : $thetip ?></span>
                            <div class="text_form hidden">
                                <div class="form-group">
                                    <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thetip; ?>" />
                                    <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                    <input type="hidden" name="field_name" value="tooltip" />
                                    <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn--green btn--small"><i class="fa fa-save fa-fw"></i></button>
                                    <a href="#" class="btn btn--white btn--small"><i class="fa fa-times fa-fw"></i></a>
                                </div>
                            </div>
                        
                        </span>
                                                                         
                    </label>    
    


                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                	<strong>$<?php echo $the_quote['policy_fee']?></strong>
                  <input type="hidden" name="5_<?php echo $thequest2; ?>" class="form-control" placeholder="" value="<?php echo $the_quote['policy_fee'] ?>">
                </div>
            </div>
        </div>
        
        

        <?php
        $thequest = 'base_rate';
        $thequest2 = url_title($thequest, 'underscore', TRUE);
        ?>
        <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['base_rate'] ?>">


        <?php
        $thequest = 'annaul_revenue';
        $thequest2 = url_title($thequest, 'underscore', TRUE);
        ?>
        <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['annaul_revenue'] ?>">
        
        
        <?php
        $thequest = 'occupation_multiplier';
        $thequest2 = url_title($thequest, 'underscore', TRUE);
        ?>
        <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['occupation_multiplier'] ?>">

        
        <?php
        $thequest = 'employee_multiplier';
        $thequest2 = url_title($thequest, 'underscore', TRUE);
        ?>
        <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['employee_multiplier'] ?>">

        
        <?php
        $thequest = 'Total Premium Payable (Including GST) :';
        $thequest2 = url_title($thequest, 'underscore', TRUE);
        ?>
        <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['total_format'] ?>">

        


        <hr />
		<?php if($this->session->userdata('for_renewal') == 'endorse') { ?>
		<?php // if(isset($the_quote['endorse'])) {?>                
        <div class="row">
            <div class="col-sm-6">
                <p><strong>Current premium:</strong></p>
            </div>
            <div class="col-sm-4">
                <p class="text-primary"><strong>$<?php echo $the_quote['endorse']['current_premium']; ?></strong></p>
                    
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <p><strong>Endorsement premium:</strong></p>
            </div>
            <div class="col-sm-4">
                <p class="text-primary"><strong> - $<?php echo $the_quote['endorse']['endorsed_premium']; ?></strong></p>
                    
            </div>
        </div>
        <hr />
       
        
        <div class="row">
            <div class="col-sm-6">
                <p><strong>Premium difference: </strong></p>
            </div>
            <div class="col-sm-4">
                <p class="lead text-primary"><strong>$<?php echo $the_quote['endorse']['prorate']; ?></strong> <small class="text-muted small">prorated</small></p>
                    
            </div>
        </div>
        
        
			<?php
            $thequest = 'current_premium';
            $thequest2 = url_title($thequest, 'underscore', TRUE);
            ?>
            <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['endorse']['current_premium'] ?>">
            
			<?php
            $thequest = 'endorsed_premium';
            $thequest2 = url_title($thequest, 'underscore', TRUE);
            ?>
            <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['endorse']['endorsed_premium'] ?>">
            
			<?php
            $thequest = 'premium_difference';
            $thequest2 = url_title($thequest, 'underscore', TRUE);
            ?>
            <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['endorse']['premium_difference'] ?>">
            
			<?php
            $thequest = 'days_prorate';
            $thequest2 = url_title($thequest, 'underscore', TRUE);
            ?>
            <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['endorse']['days_prorate'] ?>">
            
			<?php
            $thequest = 'prorate';
            $thequest2 = url_title($thequest, 'underscore', TRUE);
            ?>
            <input type="hidden" name="5_<?php echo $thequest2 ?>" value="<?php echo $the_quote['endorse']['prorate'] ?>">
            
        
        
        <?php } else { ?>
                
        <div class="row">
            <div class="col-sm-6">
                <p><strong>Total Premium Payable <small>(Including GST) :</small></strong></p>
            </div>
            <div class="col-sm-4">
                <p class="lead text-primary"><strong>$<?php echo (isset($the_quote['total_format'])) ? $the_quote['total_format'] : 0 ?></strong></p>
                    
            </div>
        </div>
        <?php } ?>
        
    <input type="hidden" name="5_updated_by_customer" value="No"/>

        
<?php } else{ ?>
    <p class="lead text-center">Broker will provide quote</p>
    <input type="hidden" name="5_updated_by_customer" value="Yes"/>
<?php } ?>
