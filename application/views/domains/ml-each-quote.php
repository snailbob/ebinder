

  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">

      <div class="row">


        <div class="col-lg-12">



        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>
        <?php } ?>


        <div class="module_header">
            <i class="fa fa-user fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <ul class="module_actions right hidden">
                <li>
                    <a class="module_action btn--red" href="<?php echo base_url() ?>panel/quote">
                        <div class="module_action_icon">
                            <i class="fa fa-user-plus"></i>
                        </div>
                        <span class="module_action_text text-uppercase"> Add <?php echo $singular ?></span>
                    </a>
                </li>
            </ul>

        </div>


        <div class="panel">
        	<div class="panel-body">

                   <?php /*?> <a href="#" class="btn btn--orange biz-bind-now pull-right" data-id="<?php echo $this->uri->segment(3) ?>">Bind</a><?php */?>
                    <h2>
                        Customer: <?php if (isset($content['homebiz1']['1_customer_id'])) { echo ($content['homebiz1']['1_customer_id'] != '') ? $this->common->customer_name($content['homebiz1']['1_customer_id']) : ' - '; } ?>

                    </h2>

<?php
	foreach($content['themenu'] as $r=>$value){
		if(!empty($value['data']) && $value['name'] != 'Done'){ //
			echo '<hr>';

			echo '<h3>'.$value['name'].'</h3>';
			foreach($value['data'] as $vr=>$vdata){
				$strip_first = explode('_', $vr);
				$the_question = '';

				$i = 1;
				if($vr == 'occupation_find' || $vr == 'occupation_id'){
					$i = 0;
				}
				for($i; $i < count($strip_first); $i++ ){
					$the_question .= $strip_first[$i].' ';
				}
				$the_question = str_replace ('[]', '', $the_question);
				$the_question = ucfirst($the_question);

				if($vr != 'user_id' && $vr != 'customer_type'){

					if(is_array($vdata)){
						echo '<p>'.$the_question.' :<br>';
						foreach($vdata  as $vdr=>$vdv){
							echo $the_states[$vdr].': '.$vdv.'%<br>';
						}
						echo '</p>';
					}
					else{
						echo '<p>'.$the_question.' : <strong>'.$vdata.'</strong></p>';
					}

				}

			}
		}
	}
?>



            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->
