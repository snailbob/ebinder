
    <!-- CONTENT -->
    <div class="l-app-container l-content-container">
        
<div>
	<div notifications></div>

    <!--==================================*\
    # Templates goest here - html
    \*===================================-->
	<div ui-view>
        <div class="route-loader hidden"></div>
                
      
        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>        
        	<?php echo $success.$danger.$info ?>
        </div>      
        <?php } ?>
        
        
        <div class="module_header">
            <i class="fa fa-user-plus fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <ul class="module_actions right hidden">
                <li>
                    <a class="module_action" href="/approvals/order/pending/csv?subscribed=">
                        <div class="module_action_icon">
                            <i class="icon-export"></i>
                        </div>
                        <span class="module_action_text">EXPORT CSV</span>
                    </a>
                </li>
            </ul>
        </div>
        
        
        
        <!--
            BEGIN [Steps Wizard]
        -->
        <div class="steps">
          <ol>
            <li class="level1 ">
              <div>
                <span>1</span>Insurance
              </div>
            </li>
            <li class="level2">
              <div class="text-primary">
                <span>2</span>Information
              </div>
            </li>
          </ol>
        </div>
        <!--
            END [Steps Wizard]
        -->         
        
        <form id="tab2_form">
        <div class="panel panel-white">
          <div class="panel-heading">
            <h3 class="heading-title">Your business</h3>
          </div>
          <div class="panel-body">
    
                <p>Please enter the full address of your physical business premises (a P.O. Box is not acceptable).</p>
    
                <!--<div class="form-group">
                    <label>Postcode</label>
                    <input type="text" name="postcode" />
                </div>-->
    
                <div class="form-group">
                    <label>Address</label>
                    <input type="text" class="form-control input-lg geolocation" name="location" value="<?php echo (isset($user_sess['info_format']['location'])) ? $user_sess['info_format']['location'] : '' ?>" required="required"/>
                    <input type="hidden" name="lat" value="<?php echo (isset($user_sess['info_format']['lat'])) ? $user_sess['info_format']['lat'] : '' ?>"/>
                    <input type="hidden" name="lng" value="<?php echo (isset($user_sess['info_format']['lng'])) ? $user_sess['info_format']['lng'] : '' ?>"/>
                    
                </div>
          </div>
        </div>   
        
             
        <div class="panel-group panel-group-products" id="accordion" role="tablist" aria-multiselectable="true">
          <?php 
		  $count = 1;
		  foreach($user_sess['info_format']['product'] as $r=>$value) {
			  if($value != '5'){
			  ?>
          <div class="panel panel-white">
            <div class="panel-heading <?php echo ($count != 1) ? 'collapsed' : ''; ?>" role="tab" id="heading<?php echo $count ?>" data-toggle="collapse" data-parent="#accordionx" data-target="#collapse<?php echo $count ?>">
              <h3 class="heading-title">
              	
              		
                  <?php if(!is_numeric($value)) {
					  if($value != 'cyber_iability'){
						  echo str_replace('_', ' ', $value);
					  } else{
						  echo 'CYBER LIABILITY';
					  }
				  }else {
					  if($value != '5'){
						  if($value == '3'){
						  	echo 'CONTENTS AND BUILDING';
						  }
						  else{
						  	echo $this->common->db_field_id('q_products', 'product_name', $value);
						  }
					  }
				  }?>
              </h3>
            </div>
            <div id="collapse<?php echo $count ?>" class="panel-collapse collapse <?php echo ($count != 1) ? '' : 'in'; ?>" role="tabpanel" aria-labelledby="heading<?php echo $count ?>">
              <div class="panel-body">
                <?php
					if(!is_numeric($value)){
						$this->load->view('domains/products/'.$value);
					}
					else{
						$this->load->view('domains/products/business_insurace'.$value);
					}
				?>
              </div>
            </div>
          </div>
          <?php $count++; } } ?>

          <div class="panel panel-white">
            <div class="panel-heading collapsed" role="tab" id="headingHistory" data-toggle="collapse" data-parent="#accordionx" data-target="#collapseHistory">
              <h3 class="heading-title">
              	History
              </h3>
              	
            </div>    
            <div id="collapseHistory" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingHistory">
              <div class="panel-body">
              	<?php
					$this->load->view('domains/products/history');

				?>


              </div>
              
            </div>
          </div>
                
        </div><!--.panel-group-->
        
        <div class="panel panel-white">
        	<div class="panel-body">
            
            	<button type="submit" class="btn btn--orange">Submit</button>
            </div>
        </div>
		</form><!--#tab2_form-->

    </div>
</div>

    </div>

    <!-- End of Contents -->
    