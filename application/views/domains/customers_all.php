

  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">

      <div class="row">


        <div class="col-lg-12">


        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
    <div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
          <?php echo $success.$danger.$info ?>
        </div>
        <?php } ?>


        <div class="module_header">
            <i class="fa fa-user fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <ul class="module_actions right">
                <li>
                    <a class="module_action btn--red" href="#" onclick="$('#broker_form').find('input').val('')" data-toggle="modal" data-target="#myModal">
                        <div class="module_action_icon">
                            <i class="fa fa-user-plus"></i>
                        </div>
                        <span class="module_action_text text-uppercase"> Add <?php echo $singular ?></span>
                    </a>
                </li>
            </ul>

        </div>


        <div class="panel panel-default">



          <div class="panel-heading">
              <a href="#myModal" class="btn btn-primary pull-right" href="#" onclick="$('#myModal').find('.form-control').val('')" data-toggle="modal" data-controls-modal="#myModal"><i class="fa fa-plus"></i> <?php echo $singular_title; ?></a>



            <div  class="panel-title">
                  <h4>Manage <?php echo $title; ?></h4>
            </div>
          </div>



          <?php if(count($customers) > 1) { ?>

                        <div class="table_search">
                            <input type="text" class="form-control input_table_search" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>


          <div class="panel-bodyx">

                <table class="table table-striped table-hover table-datatable">
                  <thead>
                    <tr>
                      <th class="hidden">#</th>
                      <th>Date added</th>
                      <th>Name</th>
                      <th>Status</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
            $count = 1;
            foreach($customers as $r=>$value){

              //if($value['id'] != $user['id']){
              ?>

                        <tr>
                          <td class="hidden"><?php echo $count?></td>
                          <td><?php echo date_format(date_create($value['date_added']), 'd-m-Y'); ?></td>
                          <td><?php echo $value['first_name'].' '.$value['last_name'] ?></td>


                          <td>
                            <?php
                                if($user['enabled'] == 'Y' && !empty($user['password'])){
                                  echo '<span class="text-success">Activated</span>';
                                }
                                else if($user['enabled'] == 'N' && !empty($user['password'])){
                                  echo '<span class="text-danger">Deactivated</span>';
                                }
                                else if($value['status'] == 'Y'){
                                    echo '<span class="text-success">Verified</span>';
                                }

                                else{
                                    echo '<span class="text-danger">Not Verified</span>';
                                }
                            ?>
                          </td>

                          <td>

                                <!-- Single button -->
                                <div class="btn-group pull-right">
                                  <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    Action <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" role="menu">

                    <?php /*?><?php if($value['enabled'] == 'Y') {?>
                                        <li><a href="javascript:;" onclick="customerActivate('N','<?php echo $value['id']; ?>');">Deactivate</a></li>
                                        <?php } else {?>
                                        <li><a href="javascript:;" onclick="customerActivate('Y','<?php echo $value['id']; ?>');">Activate</a></li>
                                        <?php } ?><?php */?>



                                        <li><a href="javascript:;" class="customer-update" data-id="<?php echo $value['id'] ?>" data-arr='<?php echo json_encode($value) ?>'>Update</a></li>
                                        <li><a href="javascript:;" onclick="customerDetails('<?php echo $value['id']; ?>');">Details</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:;" class="delete_btn" data-id="<?php echo $value['id']?>" data-table="customers">Delete Permanently</a></li>


                                  </ul>
                                </div>


                          </td>
                        </tr>




            <?php
              $count++;
              }
            //}
            ?>


                  </tbody>
                </table>


                    <?php } else { echo '<div class="panel-body"> <p class=" text-muted text-center">Nothing to show you.</p></div>'; } ?>


            </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->




    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">

            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>




            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

                          <h1><?php echo $singular_title; ?></h1>
                          <hr />





                          <form id="broker_form" class="text-left">
                              <input type="hidden" name="id" />


                              <div class="row">
                                  <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>First Name</label>
                                          <input type="text" placeholder="First Name" class="form-control" name="first" required="required"/>
                                      </div>
                                  </div>
                                  <div class="col-sm-6">
                                      <div class="form-group">
                                          <label>Last Name</label>
                                          <input type="text" placeholder="Last Name" class="form-control" name="last" required="required"/>
                                      </div>
                                  </div>
                              </div>


                              <div class="form-group">
                                  <label>Email</label>
                                  <input type="email" placeholder="Email" class="form-control" name="email" required="required"/>
                              </div>


                              <div class="form-group">
                                  <label>Position</label>
                                  <input type="position" placeholder="Position" class="form-control" name="position"/>
                              </div>


                              <div class="row">

                                  <div class="col-sm-6">
                                      <div class="form-group">
                                          <label class="control-label">Country Calling Code</label>
                                          <select class="form-control selectpicker country_code_dd" name="country_code" data-live-search="true" required="required">
                                              <option value="" data-code="">Select..</option>
                                              <?php
                                                  foreach($countries as $r=>$mc){
                                                      echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';
                                                      //
                                                      // if($user['country_id'] == $mc['country_id']){
                                                      //     echo ' selected="selected"';
                                                      // }

                                                      echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                  }
                                              ?>
                                          </select>
                                      </div>
                                  </div>

                                  <div class="col-sm-6">
                                      <div class="form-group mobile_input">
                                            <label>Mobile number</label>
                                            <span class="mobile-icon">
                                              <i class="fa fa-mobile-phone fa-2x"></i>
                                            </span>
                                            <input type="text" class="form-control mobile_number" placeholder="Mobile Number" name="mobile" autocomplete="nope" value="" required>

                                          <input type="hidden" name="country_short" value=""/>
                                      </div>
                                  </div>

                              </div><!--phone number-->




                              <div class="form-group">
                                  <label>Broker Type</label>

                                  <div class="row">

                                    <?php
                                     $report_header = array(
                                       'Regular User',
                                       'Super Administrator'
                                     );
                                     foreach($report_header as $r=>$value) {?>
                                      <div class="col-md-6">
                                      	<div class="form-group">
                                              <div class="btn-group btn-group-justified" data-toggle="buttons">
                                                <label class="btn btn-default" style="padding: 30px 0; border: dashed 1px #ccc; text-align: center">
                                                  <input type="radio" name="first_broker" value="<?php echo ($r == 0) ? 'N' : 'Y' ?>">
                            											<?php echo $value ?>
                                                </label>
                                              </div>
                                          </div>
                                      </div>
                                      <?php } ?>
                                  </div>


                                  <!-- <select class="form-control" name="first_broker" required="required">
                                      <option value="">Select</option>
                                      <option value="N">Regular User</option>
                                      <option value="Y">Super Administrator</option>
                                  </select> -->
                              </div>




                              <div class="form-group text-right">
                                  <button class="btn btn-default btn-cancel btn-grey" data-dismiss="modal">Cancel</button>
                                  <button class="btn btn-primary" type="submit">Submit</button>
                              </div>
                          </form>



                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->


<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $singular; ?> Information</h4>
      </div>
      <div class="modal-body">
        <div class="customer_info">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
