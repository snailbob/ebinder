<!-- progressbar -->
<div class="row">
    <ul id="progressbar" class="progressbar-clickable">
        <li class="active" data-target="#home">Transit</li>
        <li data-target="#quote">Quote</li>
        <li data-target="#profile">Consignor</li>
        <li data-target="#messages">Consignee</li>
        <li data-target="#decision" class="">Decision</li>
    </ul>   
</div>

<div>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs progressbar-control hidden" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Transit</a></li>
    <li role="presentation"><a href="#quote" aria-controls="home" role="tab" data-toggle="tab">Quote</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Consignor</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Consignee</a></li>
    <li role="presentation"><a href="#decision" aria-controls="decision" role="tab" data-toggle="tab">Decision</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane fade in active" id="home">

<b>&nbsp;</b>
<?php
$insurance = $this->common->the_cert_data($id);
if(count($buy) > 0){
?>
<div class="well well-sm">
	<div class="row gutter-md">
        <div class="col-sm-12">
          <!-- List group -->
          <ul class="list-group">
            <li class="list-group-item"><strong>Shipment Date: </strong><br><?php echo $insurance['shipmentdate'] ?></li>
            <li class="list-group-item"><strong>Insured Value: </strong><br><?php echo $insurance['insurance'] ?> <?php echo ($insurance['conv_insurance']) ? '(Converted base currency: '.$insurance['conv_insurance'].')' : '' ?>
            
            </li>
            <li class="list-group-item"><strong>Transit From: </strong><br><?php echo $insurance['transitfrom'] ?></li>
            <li class="list-group-item"><strong>Port of Loading: </strong><br><?php echo $insurance['portloading'] ?></li>
            <li class="list-group-item"><strong>Transit To: </strong><br><?php echo $insurance['transitto'] ?></li>
            <li class="list-group-item"><strong>Port of Discharge: </strong><br><?php echo $insurance['portdischarge'] ?></li>
            <li class="list-group-item"><strong>Cargo Cat: </strong><br><?php echo $insurance['cargocat'] ?></li>
            <li class="list-group-item"><strong>Description: </strong><br><?php echo $insurance['description'] ?></li>
            <li class="list-group-item"><strong>Deductible: </strong><br><?php echo $insurance['deductible'] ?></li>
          </ul>
        </div>
    
    </div>
</div>
<?php } else { echo '<p class="text-muted">No transit details found.</p>'; } ?>


    </div>
    <div role="tabpanel" class="tab-pane fade" id="quote">
        <b>&nbsp;</b>
        <div class="well well-sm text-center">
            <h1 style="font-size: 50px"><?php echo '<small>'.$insurance['currency'].'</small>'.number_format($details['premium'], 2, '.', ',') ?></h1>
        </div>
    </div>
    <div role="tabpanel" class="tab-pane fade" id="profile">



<b>&nbsp;</b>
<?php if(count($single) > 0){ ?>
<div class="well well-sm">
	<div class="row gutter-md">
    	<?php /*?><div class="col-sm-4">
        	<div class="row gutter-sm">
                <div class="col-xs-8 col-xs-offset-2">
                    <img src="<?php echo $avatar ?>" class="img-circle img-responsive img-thumbnail" alt="" title="">
                </div>
            </div>
        </div><?php */?>
        
        <div class="col-sm-12">
          <!-- List group -->
          <ul class="list-group">
            <li class="list-group-item"><strong>Name: </strong><br><?php echo $single['first_name'].' '.$single['last_name'] ?></li>
            <li class="list-group-item"><strong>Business Name: </strong><br><?php echo $single['business_name'] ?></li>
            <li class="list-group-item"><strong>Address: </strong><br><?php echo $single['location'] ?></li>
            <li class="list-group-item"><strong>Email : </strong><br><?php echo $single['email'] ?></li>
            <li class="list-group-item"><strong>Mobile: </strong><br><?php echo $single['calling_code'].$single['calling_digits'] ?></li>
          </ul>
        </div>
    
    </div>
</div>
<?php } else { echo '<p class="text-muted">No consignor details found.</p>'; } ?>

    </div>
    <div role="tabpanel" class="tab-pane fade" id="messages">


<b>&nbsp;</b>
<?php if(count($consignee) > 0){ ?>
<div class="well well-sm">
	<div class="row gutter-md">
        <div class="col-sm-12">
          <!-- List group -->
          <ul class="list-group">
            <li class="list-group-item"><strong>Name: </strong><br><?php echo $consignee['first_name'].' '.$consignee['last_name'] ?></li>
            <li class="list-group-item"><strong>Business Name: </strong><br><?php echo $consignee['business_name'] ?></li>
            <li class="list-group-item"><strong>Address: </strong><br><?php echo $consignee['location'] ?></li>
            <li class="list-group-item"><strong>Email : </strong><br><?php echo $consignee['email'] ?></li>
            <li class="list-group-item"><strong>Mobile: </strong><br><?php echo $consignee['calling_code'].$consignee['calling_digits'] ?></li>
          </ul>
        </div>
    
    </div>
</div>
<?php } else { echo '<p class="text-muted">No consignee details found.</p>'; } ?>

    </div>
    <div role="tabpanel" class="tab-pane fade" id="decision">
    	<b>&nbsp;</b>
        <form class="admin_review_form" action="<?php echo base_url().'webmanager/dashboard/accept_request/' ?>" method="post">

      		<div class="form-group">
            	<label>Adjust Quoted Premium</label>
            	<input type="text" class="form-control input-currency" name="premium" value="<?php echo number_format($details['premium'], 2, '.', ','); ?>" />
            	<input type="hidden" class="form-control" name="id" value="<?php echo $id ?>" />
            </div>
        
      		<div class="form-group">
            	<label>Comments</label>
            	<textarea name="comment" class="form-control"><?php echo $comment; ?></textarea>
            </div>
        
      		<div class="form-group">
                <a href="#" class="btn btn-danger pull-left reject_btn">Reject</a>
                <button type="submit" class="btn btn-primary pull-right admin_review_submit_btn">Accept</button>
            </div>


        </form>


    </div>
    
  </div>

</div>