<div class="panel panel-default">
  <div class="panel-body text-center">
    <img src="<?php echo $avatar ?>" class="img-rounded img-responsive img-thumbnail" alt="" title="" style="max-width: 220px;">
  </div>
  
<!-- List group -->
  <ul class="list-group">
  	
  	
    <li class="list-group-item"><strong>Name: </strong><br><?php echo $name ?></li>
    <li class="list-group-item"><strong>Location: </strong><br><?php echo $address ?></li>
    <li class="list-group-item hidden"><strong>Facilities: </strong><br><?php echo $facilities ?></li>
    <li class="list-group-item hidden"><strong>Background: </strong><br><?php echo $background ?></li>
    <li class="list-group-item"><strong>Contact No: </strong><br><?php echo $contact_no ?></li>
    <li class="list-group-item"><strong>Minimum Premium: </strong><br><?php echo $minimum_premium?></li>
  </ul>

</div>