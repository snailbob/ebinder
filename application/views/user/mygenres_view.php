
<section class="section section-simple">
	<div class="container">
    	<div class="row">
        
        	<div class="col-md-12">
            
            	<?php if ($this->session->flashdata('ok') != ''){ ?>
                <div class="alert alert-success fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>

                	<?php echo $this->session->flashdata('ok'); ?>
                </div>
                <?php } ?>
            
            	<?php if ($this->session->flashdata('error') != ''){ ?>
                <div class="alert alert-danger fade in">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>

                	<?php echo $this->session->flashdata('error'); ?>
                </div>
                <?php } ?>
            
            
            	<div class="panel panel-default">
                
                	<div class="panel-body">
                        <h2 class="my-panel-title"><?php echo $title; ?></h2>
                        
                        
                        <?php 
							if(count($dance_genres) > 0){
								echo '<div class="row">';
								foreach($dance_genres as $r){ ?>
                                
                                
                                	<div class="col-sm-4 col-md-3 col-lg-2">
                                        <div class="thumbnail text-center" style="padding-top: 25px; padding-bottom: 25px;">
                                            <i class="fa fa-child fa-2x"></i><br />
                                            <?php echo $this->common->genre_name($r); ?>

                                        </div>
                                    </div>
                                
                                
                                <?php
								}
								echo '</div>';
								
							}
							else{
							 echo '<p class="text-center text-muted">No genres.</p>';
							}
						?>
                        
                    	<p class="lead text-center">
                            <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#formModal">Update My Genres</a>
                        </p>
                        

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>






    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="formModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="modal-body">
                        
                            <h2>Genres</h2>
                            <hr class="star-primary">
                                
                            <form id="instructor_genre_form">
                            	<input type="hidden" name="id" value="<?php echo $this->session->userdata('id')?>" />
                                <div class="form-group">
                                    <div class="the_genres">
                                    	<div class="row genre_list">
                                        <?php foreach($genres as $r=>$value){ ?>
                                        <div class="col-xs-6 col-md-3 col-lg-2">
                                        <a href="#" class="thumbnail text-center genre_thumbs  <?php if(in_array($value['id'], $dance_genres)) { echo 'active';}?>">
                                        <?php echo $value['name']?>
                                        <div class="checkbox hidden">
                                          <label>
                                            <input type="checkbox" name="genres[]" value="<?php echo $value['id']?>" <?php if(in_array($value['id'], $dance_genres)) { echo 'checked="checked"';}?>> <span><?php echo $value['name']?></span>
                                            
                                          </label>
                                        </div> <!--checkbox-->
                                        </a>                                   
                                        </div><!--col-xs-6-->
                                        <?php } ?>
                                        
                                        </div>
                                    </div>
                                 
                                </div>
                                
                                <div class="form-group">
                                	<button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="#" class="btn btn-default" onclick="$(this).closest('form').find('input').prop('checked',false);$(this).closest('form').find('input').closest('a').removeClass('active');">Reset</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
