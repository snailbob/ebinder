
            
            <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
            
                <div class="row">
            
                
                  <div class="col-lg-12">
                  
                        
                    <?php if ($this->session->flashdata('ok') != ''){ ?>
                    <div class="alert alert-success fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
    
                        <?php echo $this->session->flashdata('ok'); ?>
                    </div>
                    <?php } ?>
                
                    <?php if ($this->session->flashdata('error') != ''){ ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
    
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                    <?php } ?>
                
    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
								<h4><?php echo $title; ?></h4>
                            </div>
                        </div>
                       
                       
                        <?php if(count($insurances) > 0){ ?>
                        <div class="bg-white">
                        <table class="table table-hover mydataTb">
                            <thead>
                                <tr>
                                    <th class="hidden">sort</th>
                                    <th>Date</th>
                                    <th>Consignor</th>
                                    <th>Transit from</th>
                                    <th>Transit to</th>
                                    <th>Premium</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
								$count = 1;
								foreach($insurances as $r=>$value){
									$insurance = $this->common->the_quote_data($value['id']);

                                    $details = unserialize($value['details']);
                                    $premium = (isset($details['premium'])) ? $details['premium'] : 0;
									$premium = number_format($premium, 2, '.', ',');
                                    $buy_inputs = (isset($details['buy_inputs'])) ? $details['buy_inputs'] : array();
									$insurance_details = array();
									foreach($buy_inputs as $bi=>$bival){
										$insurance_details[$bival['name']] = $bival['value'];
									}
									
									$single_input = (isset($details['single_input'])) ? $details['single_input'] : array();
									$cust_name = 'Unknown';
									if(isset($single_input['first_name']) && isset($single_input['last_name'])){
										$cust_name = $single_input['first_name'].' '.$single_input['last_name'];
									}
									$transitto = (isset($insurance_details['transitto'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitto'], 'country_id') : 'not specified';
									$transitfrom = (isset($insurance_details['transitfrom'])) ? $this->common->db_field_id('country_t', 'short_name', $insurance_details['transitfrom'], 'country_id') : 'not specified';

									
                                    $date_purch = date_format(date_create($value['date_added']), 'F d, Y - l');
                                    if($value['date_added'] == '0000-00-00 00:00:00'){
                                        $date_purch = 'NA';
                                    }
                                ?>
                                <tr>
                                    <th class="hidden"><?php echo $count; ?></th>
                                    <td><?php echo $date_purch ?></td>
                                    <td><?php echo $cust_name ?></td>
                                    <td><?php echo $transitfrom; ?></td>
                                    <td><?php echo $transitto; ?></td>
                                    <td><?php echo $insurance['currency'].$premium; ?></td>
                                    <td><?php echo $this->common->quote_status($value['status']); ?></td>
                                    <td>
                                    	<button class="btn btn-default btn-xs btn-block pull-right bind_quote_btn <?php echo ($value['status'] != 'A') ? 'disabled' : ''?>" data-id="<?php echo $value['id'] ?>">
											<?php echo ($value['status'] == 'P') ? 'Bound' : 'Bind'?>
                                        </button>
                                        
                                    </td>
                                </tr>
                                <?php $count++; } ?>
                            </tbody>
                        </table>
                        </div>
                        
						<?php
                          //loop sorted_schedule
						 } 
						 else { echo '<p class="text-center text-muted" style="padding: 50px;">No records found.</p>'; }
						 ?>
                   
                    </div><!--panel-->
                    
                              
                  
                  
                                        
                    
                    
                  </div><!-- /.col-lg-12 --> 
                
                </div><!-- /.row --> 
                <!-- end PAGE TITLE AREA -->
                
            </div><!--end of main_content-->
            


<!-- Modal for new task -->
<div class="portfolio-modal modal fade" id="infoModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="modal-body">
                    
                        <h1 class="buy_title hidden">Buy Single Marine Transit Policy</h1>
                        <hr class="star-primary hidden">
         
                
                        <div class="txt_details"></div>


                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
</div><!--portfolio-modal-->
