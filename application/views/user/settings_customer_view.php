            
            <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
            
                <div class="row">
            
                
                  <div class="col-lg-12">
                  
                        
                    <?php if ($this->session->flashdata('ok') != ''){ ?>
                    <div class="alert alert-success fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
    
                        <?php echo $this->session->flashdata('ok'); ?>
                    </div>
                    <?php } ?>
                
                    <?php if ($this->session->flashdata('error') != ''){ ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
    
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                    <?php } ?>
                            
            
            
                    <div class="row">
                    
            
                        <div class="col-md-3 col-sm-4 <?php echo 'hidden'; // if($this->session->userdata('customer_type') == 'N') {echo 'hidden';}?>">
                           <div class="row hidden">
                            <form id="myFormAvatar" action="<?php echo base_url().'settings/update_avatar' ?>" method="post" enctype="multipart/form-data">
                                 <input type="file" size="60" id="myfile" name="myfile" class="myfile-profile" onchange="$('#myFormAvatar').submit();">
                                 <input type="hidden" id="avatar_name" value="">
                                 <input type="submit" class="hidden" value="Ajax File Upload">
                             </form>                                                   
                            </div>
                
            
                            <a href="#" class="portfolio-box text-center" onclick="$('.myfile-profile').click(); return false;">
                                <img src="<?php echo $this->common->avatar($customer[0]['id'], 'customer') ?>" class="img-circle img-thumbnail" alt="" width="100%">
                                <div class="portfolio-box-caption div-circle">
                                    <div class="portfolio-box-caption-content">
                                        <div class="project-category text-faded">
                                            
                                        </div>
                                        <div class="project-name">
                                            Update Picture
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <p></p>
            
                            <div id="progress" class="progress-profile" style="display: none;">
                                <div id="bar" class="bar-profile"></div>
                                <div id="percent" class="percent-profile">0%</div >
                            </div>
            
            
            
            
                        </div>
            
                        <div class="<?php echo 'col-md-12'; //if($this->session->userdata('customer_type') == 'N') {echo 'col-md-12';} else { echo 'col-md-9 col-sm-8'; }?>">
                            <div class="panel panel-default">
                            
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <h4><?php echo $title; ?></h4>
                                    </div>
                                </div>
            
                                <div class="panel-body">
            
                                    <div>
                                    
                                      <!-- Nav tabs -->
                                      <ul class="nav nav-tabs hidden" role="tablist">
                                        <li class="<?php if($this->uri->segment(3) == '') { echo 'active';}?>"><a href="#studio" aria-controls="studio" role="tab" data-toggle="tab"><?php echo ucfirst($this->session->userdata('type')); ?> Profile</a></li>
                                        <li class="hidden <?php if($this->uri->segment(3) == 'connect') { echo 'active';}?>"><a href="#connect" aria-controls="connect" role="tab" data-toggle="tab">Connect</a></li>
                                        <li class="<?php if($this->uri->segment(3) == 'payment') { echo 'active';}?>"><a href="#payment" aria-controls="payment" role="tab" data-toggle="tab">Payment Method</a></li>
                                        <li class="<?php if($this->uri->segment(3) == 'pw') { echo 'active';}?>"><a href="#password" aria-controls="password" role="tab" data-toggle="tab">Password</a></li>
                                      </ul>
                                    
                                      <!-- Tab panes -->
                                      <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane fade <?php if($this->uri->segment(3) == '') { echo 'in active';}?>" id="studio">
                                            <p></p>
                                                        
                                            <form class="" id="customer_profile_form">
                                                
                                                <?php foreach($the_fields as $r){ ?>
                                                <div class="form-group">
                                                    <label><?php echo ucfirst(str_replace('_', ' ', $r)); ?></label>
                                                    <input type="text" name="<?php echo 'inst_'.$r ?>" class="form-control" value="<?php echo $customer[0][$r] ?>" placeholder="<?php echo ucfirst(str_replace('_', ' ', $r)); ?>"/>
                                                </div>
                                                    
                                                <?php } ?>
                                                
                                                <div class="form-group">
                                                    <label>Business Name</label>
                                                    <input type="text" name="business_name" class="form-control" value="<?php echo $customer[0]['business_name'] ?>" placeholder="Business Name"/>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <input type="text" name="address" class="form-control input_location" value="<?php echo $customer[0]['location'] ?>" placeholder="Location"/>
                                                    <input type="hidden" name="lat" value="<?php echo $customer[0]['lat']; ?>"/>
                                                    <input type="hidden" name="long" value="<?php echo $customer[0]['lng']; ?>"/>
                                                    <input type="hidden" name="inst_id" value="<?php echo $customer[0]['id']?>"/>
                                                </div>
                                                    
                                                
                                                <div class="row">
                                                
                                                    <div class="col-sm-5">
                                                        <div class="form-group">
                                                            <label class="control-label">Country Calling Code</label>
                                                            <select class="form-control country_code_dd" name="country_code">
                                                                <option value="" data-code="">Select..</option>
                                                                <?php
                                                                    foreach($countries as $r=>$mc){
                                                                        echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';
                                                                        
                                                                        if($customer[0]['country_id'] == $mc['country_id']){
                                                                            echo ' selected="selected"';
                                                                        }
                                                                        
                                                                        echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                                    }
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                
                                                    <div class="col-sm-7">
                                                        <div class="form-group mobile_input">
                                                            <label class="control-label">Mobile Number</label>
                                                            <div class="input-group">
                                                              <span class="input-group-addon addon-shortcode">+</span>
                                                              <input type="text" class="form-control" placeholder="Mobile Number" name="mobile" value="<?php echo $customer[0]['calling_digits']?>">
                                                            </div>
                                                            <input type="hidden" name="country_short" value=""/>
                                                        </div>
                                                    </div>
                                                
                                                </div>
                                                
                                                <div class="form-group hidden">
                                                    <label>About</label>
                                                    <textarea name="about" class="form-control" placeholder="About"><?php echo $customer[0]['about']?></textarea>
                                                </div>
                        
                                                
                                                                                        
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                                        
                                        
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade <?php if($this->uri->segment(3) == 'connect') { echo 'in active';}?>" id="connect">
                                            <p></p>
                                            googogle
                                        
                                        </div>
                                        
                 
                                        <div role="tabpanel" class="tab-pane fade <?php if($this->uri->segment(3) == 'payment') { echo 'in active';}?>" id="payment">
                                            <p></p>
                    
            
                                        
                                            <div class="row">
                                                <!-- You can make it whatever width you want. I'm making it full width
                                                     on <= small devices and 4/12 page width on >= medium devices -->
                                                <div class="col-md-8 col-md-offset-2">
                                                
                                                    <?php if($customer[0]['stripe_id'] == '') {?>
                                                    <!-- CREDIT CARD FORM STARTS HERE -->
                                                    <div class="panel panel-default credit-card-box">
            
                                                        <div class="panel-body">
                                                            <p class="text-muted">
                                                                <img class="img-responsive pull-right" src="<?php echo base_url().'assets/img/'?>accepted_c22e0.png">
                                                            A payment method is required in order to buy transit insurance.
                                                            </p>
                                                                                      
            
                                                            <form role="form" id="payment-form">
                                                                <div class="row hidden">
                                                                    <div class="col-xs-12">
                                                                        <div class="form-group">
                                                                            <label for="couponCode">Card Holder's Name</label>
                                                                            <input type="text" class="form-control" name="name" placeholder="Card Holder's Name" autofocus/>
                                                                        </div>
                                                                    </div>                        
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12">
                                                                        <div class="form-group">
                                                                            <label for="cardNumber">Card number</label>
                                                                            <div class="input-group">
                                                                                <input 
                                                                                    type="tel"
                                                                                    class="form-control"
                                                                                    name="cardNumber"
                                                                                    placeholder="Valid Card Number"
                                                                                    autocomplete="cc-number"
                                                                                    required autofocus
                                                                                />
                                                                                <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                                                            </div>
                                                                        </div>                            
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-7 col-md-7">
                                                                        <div class="form-group">
                                                                            <label for="cardExpiry">Expiration Date</label>
                                                                            <input 
                                                                                type="tel" 
                                                                                class="form-control" 
                                                                                name="cardExpiry"
                                                                                placeholder="MM / YY"
                                                                                autocomplete="cc-exp"
                                                                                required 
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xs-5 col-md-5 pull-right">
                                                                        <div class="form-group">
                                                                            <label for="cardCVC">CV Code</label>
                                                                            <input 
                                                                                type="tel" 
                                                                                class="form-control"
                                                                                name="cardCVC"
                                                                                placeholder="CVC"
                                                                                autocomplete="cc-csc"
                                                                                required
                                                                            />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-12">
                                                                        <button class="btn btn-primary btn-lg btn-block" type="submit">Submit</button>
                                                                    </div>
                                                                </div>
                                                                <div class="row" style="display:none;">
                                                                    <div class="col-xs-12">
                                                                        <p class="payment-errors text-danger"></p>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>            
                                                    <!-- CREDIT CARD FORM ENDS HERE -->
                                                    
                                                    <?php } else { ?>
                                                    <div style="margin-top: 50px;" class="alert alert-success text-center">Payment method added. You are completely ready to buy transit insurance. </div>
            
                                                    <form id="remove_card_form" style="padding-bottom: 50px;">
                                                        <button type="submit" class="btn btn-primary btn-block btn-lg remove_card_btn" data-id="<?php echo $customer[0]['id']?>" data-stripe="<?php echo $customer[0]['stripe_id']?>">Remove Card</button>
                                                    </form>
            
                                                    
                                                    <?php }?>
                                                    
                                                </div>            
                                            </div>            
                        
            
            
                                        
                                        </div><!---payment tab-->
                                        
                                                                    
                                        
                                        <div role="tabpanel" class="tab-pane fade <?php if($this->uri->segment(3) == 'pw') { echo 'in active';}?>" id="password">
                                            <p></p>
                                            <form class="" id="customer_password_form">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input type="password" name="pw" id="pw" class="form-control" />
                                                    <input type="hidden" name="id" class="form-control" value="<?php echo $customer[0]['id'] ?>" />
                                                    <input type="hidden" name="type" class="form-control" value="customer" />
                                                </div>
                                                <div class="form-group">
                                                    <label>Confirm Password</label>
                                                    <input type="password" name="pw2" class="form-control" />
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                    
                                        </div>
                                      </div>
                                    
                                    </div>                            
                                    
                                </div>
                                                    
                                
            
                            </div>
                        </div>
                    </div>

                  </div><!-- /.col-lg-12 --> 
                
                </div><!-- /.row --> 
                <!-- end PAGE TITLE AREA -->
                
            </div><!--end of main_content-->

<!-- Modal -->
<div class="modal fade" id="cropImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<p class="lead">Crop</p>
        
        <div class="bootstrap-modal-cropper" id="customer_img"><img src="<?php echo base_url().'assets/img/avatar/background.png' ?>" class="img-responsive"/></div>
	
        

      </div>
      <div class="modal-footer">
        <a class="btn btn-primary sav_crop">Save changes</a>
      </div>
    </div>
  </div>
</div>