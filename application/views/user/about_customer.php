<div class="panel panel-default">
 <?php /*?> <div class="panel-body text-center">
    <img src="<?php echo $avatar ?>" class="img-circle img-responsive img-thumbnail" alt="" title="" style="max-width: 220px;">
  </div><?php */
  
  ?>
  
  
  
<!-- List group -->
  <ul class="list-group">
    <li class="list-group-item"><strong>Name: </strong><br><?php echo $this->common->customer_name($id) ?></li>
    <li class="list-group-item"><strong>Agency: </strong><br><?php echo (isset($agency['name'])) ? $agency['name'] : '' ?></li>
    <li class="list-group-item"><strong>Domain: </strong><br><?php echo (isset($agency['domain'])) ? $agency['domain'] : '' ?></li>
    <li class="list-group-item"><strong>Brokerage: </strong><br><?php echo (isset($brokerage['company_name'])) ? $brokerage['company_name'] : 'NA' ?></li>
    <li class="list-group-item"><strong>Address: </strong><br><?php echo $location ?></li>
    <li class="list-group-item"><strong>Email: </strong><br><?php echo $email ?></li>
    <li class="list-group-item"><strong>Mobile No.: </strong><br><?php echo $contact_no ?></li>

  </ul>

</div>