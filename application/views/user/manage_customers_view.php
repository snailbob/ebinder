            
            <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
            
                <div class="row">
            
                
                  <div class="col-lg-12">
                  
                        
                    <?php if ($this->session->flashdata('ok') != ''){ ?>
                    <div class="alert alert-success fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
    
                        <?php echo $this->session->flashdata('ok'); ?>
                    </div>
                    <?php } ?>
                
                    <?php if ($this->session->flashdata('error') != ''){ ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
    
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                    <?php } ?>

            
                
                    <p class="text-right">
                        <a href="#" class="btn btn-primary openmodal_add_btn" data-target="#addCustModal"><i class="fa fa-plus"></i> New Customer</a>
                    </p>
    
    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
								<h4><?php echo $title; ?></h4>
                            </div>
                        </div>
    
                        
                        <?php if(count($customers) > 0){ ?>
                        <div class="bg-white">
                        <table class="table table-hover mydataTb">
                            <thead>
                                <tr>
                                    <th>Customer Name</th>
                                    <th>Company Name</th>
                                    <th>Email</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($customers as $r=>$value){
                                    $details = unserialize($value['details']);
                                ?>
                                <tr>
                                    <td><?php echo $value['name']?></td>
                                    <td><?php echo $details['business_name']?></td>
                                    <td><?php echo $value['email']?></td>
                                    <td>
                                        <!-- Single button -->
                                        <div class="btn-group pull-right">
                                          <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                            Action <span class="caret"></span>
                                          </button>
                                          <ul class="dropdown-menu" role="menu">
                                            <?php
                                                $data_txt = '';
                                                foreach($details as $ds=>$dsval){
                                                    $data_txt .= ' data-'.$ds.'="'.$dsval.'"';
                                                }
                                            ?>
                                            <li><a href="#" class="view_agentcust_btn" data-id="<?php echo $value['id']?>" data-target="#infoModal">View Details</a></li>
                                            <li><a href="#" class="update_btn" data-id="<?php echo $value['id']?>" data-target="#addCustModal" <?php echo $data_txt; ?>>Update Details</a></li>
                                            <li><a href="#" class="delete_btn" data-id="<?php echo $value['id']?>" data-table="agent_customers">Delete</a></li>
                                          </ul>
                                        </div>                              
                                    
                                    </td>
                                </tr>
                                    
                                <?php } ?>
                            </tbody>
                        </table>
                        </div>
                        
                    <?php
                      //loop sorted_schedule
                     } ?>
                   
                    </div><!--panel-->
                    
                  
                

                  </div><!-- /.col-lg-12 --> 
                
                </div><!-- /.row --> 
                <!-- end PAGE TITLE AREA -->
                
            </div><!--end of main_content-->
            
            
	<!-- Modal -->
    <div class="modal fade" id="addCustModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Customer</h4>
          </div>

          <div class="modal-body">
                <form id="consignee_form" class="single_purchase_form">
                 <input type="hidden" name="type" value="agent_customer" />
                 <div class="hidden">
                 	<input type="text" class="id" name="id" value="" />
                 </div>
                
                 <div class="row">
                   
                    <div class="col-sm-12 form-group">
                        <div class="wellx">
                            <h3 class="hidden">Single Purchase</h3>
                            <div class="row gutter-md">
                                <div class="col-md-6 form-group">
                                    <label>First Name</label>
                                    <input type="text" class="form-control first_name" name="first" placeholder="First Name" value=""/>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Last Name</label>
                                    <input type="text" class="form-control last_name" name="last" placeholder="Last Name" value=""/>
                                </div>
                                
                            </div><!--name row-->
                                        
                            <div class="row gutter-md">
                                <div class="col-sm-12 form-group">
                                    <label>Company Name</label>
                                    <input type="text" class="form-control business_name" name="bname" placeholder="Company Name" value=""/>
                                </div>
                            </div><!--bname row-->
                                        
                            <?php /*?><div class="row gutter-md">
                                <div class="col-sm-12 form-group">
                                    <label>Business Address</label>
                                    <input type="text" class="form-control input_location location" name="address" placeholder="Company Address" value=""/>
                                    <input type="hidden" name="lat" class="lat" value=""/>
                                    <input type="hidden" name="lng" class="lng" value=""/>
                                    
                                </div>
                            </div><!--bname row--><?php */?>
    
                            <div class="row gutter-md">
                                <div class="col-sm-12 form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control email" name="email" placeholder="Email" value=""/>
                                </div>
                            </div><!--bname row-->
    
                            <div class="row gutter-md">
                            
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Country Calling Code</label>
                                        <select class="form-control country_code_dd country_id" name="country_code" data-live-search="true">
                                            <option value="" data-code="">Select..</option>
                                            <?php
                                                foreach($countries as $r=>$mc){
                                                    echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'">'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            
                                <div class="col-sm-6">
                                    <div class="form-group mobile_input">
                                        <label class="control-label">Mobile Number</label>
                                        <div class="input-group">
                                          <span class="input-group-addon addon-shortcode">+</span>
                                          <input type="text" class="form-control calling_digits" placeholder="Mobile Number" name="mobile" value="">
                                        </div>
                                        <input type="hidden" name="country_short" value=""/>
                                    </div>
                                </div>
                            
                            </div><!--phone number-->
                                                                
                        </div>
                    
                    </div>
                   
    
                    <div class="col-sm-12 form-group">
                        <button class="btn btn-warning pull-right" type="submit">Submit</button>
    
                    </div>
                        
                </div>
                </form><!--hide form if logged in-->
                
          </div>

        </div>
      </div>
    </div>

    
    <!-- Modal -->
    <div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Customer Details</h4>
          </div>
          <div class="modal-body">
            <div class="txt_details">
            
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
    
