<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>


<body>

<select name="source" id="source" class="form-control">

    <option value="">Select Source Currency</option>

    
        <option value="ARS">Argentina Peso</option>

    
        <option value="AUD">Australia Dollar</option>

    
        <option value="BTC">Bitcoin</option>

    
        <option value="BRL">Brazil Real</option>

    
        <option value="CAD">Canada Dollar</option>

    
        <option value="CLP">Chile Peso</option>

    
        <option value="CNY">China Yuan Renminbi</option>

    
        <option value="CZK">Czech Republic Koruna</option>

    
        <option value="DKK">Denmark Krone</option>

    
        <option value="EUR">Euro Member Countries</option>

    
        <option value="FJD">Fiji Dollar</option>

    
        <option value="HNL">Honduras Lempira</option>

    
        <option value="HKD">Hong Kong Dollar</option>

    
        <option value="HUF">Hungary Forint</option>

    
        <option value="ISK">Iceland Krona</option>

    
        <option value="INR">India Rupee</option>

    
        <option value="IDR">Indonesia Rupiah</option>

    
        <option value="ILS">Israel Shekel</option>

    
        <option value="JPY">Japan Yen</option>

    
        <option value="KRW">Korea (South) Won</option>

    
        <option value="MYR">Malaysia Ringgit</option>

    
        <option value="MXN">Mexico Peso</option>

    
        <option value="NZD">New Zealand Dollar</option>

    
        <option value="NOK">Norway Krone</option>

    
        <option value="PKR">Pakistan Rupee</option>

    
        <option value="PHP">Philippines Peso</option>

    
        <option value="PLN">Poland Zloty</option>

    
        <option value="RUB">Russia Ruble</option>

    
        <option value="SGD">Singapore Dollar</option>

    
        <option value="ZAR">South Africa Rand</option>

    
        <option value="SEK">Sweden Krona</option>

    
        <option value="CHF">Switzerland Franc</option>

    
        <option value="TWD">Taiwan New Dollar</option>

    
        <option value="THB">Thailand Baht</option>

    
        <option value="TRY">Turkey Lira</option>

    
        <option value="GBP">United Kingdom Pound</option>

    
        <option value="USD">United States Dollar</option>

    
        <option value="VND">Viet Nam Dong</option>

    
</select>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/jquery.min.js"></script>

<script>
	var arr = [];
	var count = 0;
	$('select option').each(function(index, element) {
		var obj = {
			'currency': $(this).val(),
			'name': $(this).html()
		};
        if(count != 0){
			arr.push(obj);
		}
		count++;
    });
	console.log(arr);
	
	var base_url = 'http://localhost/nguyen/transit/';
//	$.post(
//		base_url+'home/savecurr',
//		{arr: arr},
//		function(res){
//			console.log(res);
//		},
//		'json'
//	).error(function(res){
//		console.log(res);
//	});
</script>

</body>
</html>
