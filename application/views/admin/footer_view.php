
        </div><!--/.row-->
    </div>
    <!-- /.container -->
</div><!-- the_main_container -->



    <div id="footer" class="">
    	<div class="container hidden">
        	<div class="row hidden">
            <div class="col-md-12 col-md-offset-1 addLeftPadding">


            	<div class="col-md-5 col-xs-12">
                	<h3>Recent Blogs</h3>
                </div>

            	<div class="col-md-2 col-xs-12">
                	<h3>Company</h3>
                </div>
            	<div class="col-md-5 col-xs-12">
                	<h3>Location</h3>
                </div>
            </div><!--offset2-->
            </div> <!-- /row -->

            <div id="footer-bottom" class="row">

                <div id="copyright">
                	<ul>
                    	<li class="text">&copy; 2015 Your Company Pty Ltd</li>
                    </ul>
                	<ul>

                    	<li><a href="#" class="terms_btnx">Terms of Use</a></li>
                    	<li><a href="#" class="privacy_btnx">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div> <!-- /container -->
    </div> <!-- /footer -->


    <div id="top-button" class="animated fadeInUp" data-0="display: none" data-100="display: block">
    	<img src="<?php echo base_url()?>assets/frontpage/corporate/images/top-button.png" />
    </div>

	</div>


    <div class="loading style-2">
      <div class="loading-wheel">
        <i class="fa fa-square-o text-primary fa-spin fa-3x"></i>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myLoadingModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content text-center" style="background-color: transparent; border: none; box-shadow: none; padding: 150px 0;">

    		<i class="fa fa-square-o text-primary fa-spin fa-3x"></i>
        </div>
      </div>
    </div>




    <!-- termsModal -->
    <div class="portfolio-modal modal fade" id="termsModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Transit Insurance - Terms of Use</h1>
                            <hr class="star-primary">


                            <div class="text_content text-left"></div>



                        </div><!--modal-body-->
                        <div class="modal-footer">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->


    <!-- privacyModal -->
    <div class="portfolio-modal modal fade" id="privacyModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                        <div class="modal-body">

                            <h1>Privacy Policy</h1>
                            <hr class="star-primary">


                            <div class="text_content text-left"></div>



                        </div><!--modal-body-->
                        <div class="modal-footer">
                        	<button class="btn btn-primary btn-rounded" data-dismiss="modal">OK</button>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div><!--portfolio-modal-->



<!--cargo tab panel-->


<?php

    $admin_info = $this->master->getRecords('admin');
    $user_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];
    $agency_info = $this->master->getRecords('agency', array('id'=>$user_id));

    $whr = array(
      'customer_type'=>'N',
      'super_admin'=>'Y',
    );
    $whr['agency_id'] = $user_id;
    $underwriters = $this->master->getRecords('customers', $whr);
    $base_currency = $this->common->db_field_id('agency', 'base_currency', $user_id); //quote expiry




    $deductibles = $this->master->getRecords('deductibles', array('agency_id'=>$user_id));
    $cargo = $this->master->getRecords('q_business_specific'); //this->master->getRecords('cargo_category');
    $transportations = $this->common->transportations();
    $countries = $this->master->getRecords('country_t');
    $currencies = $this->master->getRecords('api_currencies', array('in_stripe'=>'Y'));


    $cargo_prices = $this->common->agency_prices($user_id,'cargo_prices');
    $cargo_max = $this->common->agency_prices($user_id,'cargo_max');
    $transportation_prices = $this->common->agency_prices($user_id,'transportation_prices');
    $country_prices = $this->common->agency_prices($user_id,'country_prices');
    $country_referral = $this->common->agency_prices($user_id,'country_referral');
    $zone_multiplier = $this->common->agency_prices($user_id,'zone_multiplier');
    $base_currency = ($agency_info[0]['base_currency']) ? $agency_info[0]['base_currency'] : 'AUD';

    if($this->uri->segment('3') !='ratings') {

?>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="newProductModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

                            <h1>Product</h1>
                			<hr />


                            <form id="product_form" class="text-left">
                            	<input type="hidden" name="id" />
                            	<input type="hidden" name="broker_id" name="0" />
                                <div class="form-group">
                                    <label>Product Name</label>
                                    <input type="text" class="form-control" name="product_name" />
                                </div>
                                <div class="form-group hidden">
                                    <label>Text Helper</label>
                                    <input type="text" class="form-control" name="helper" />
                                </div>
                                <div class="form-group text-right">
                                    <button class="btn btn-default btn-cancel btn-grey" data-dismiss="modal">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>


                            </form>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->


    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="assignAgentModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                            <p class="pull-right" style="padding-top: 20px;">
                              <a href="#historyModal" class="btn show_history_btn" data-type="assign_underwriter">
                                <i class="fa fa-history"></i> History
                              </a>
                            </p>

                            <h1>Assign Underwriter</h1>
                            <hr class="star-primary">

                            <form id="assign_underwriter_form" class="text-left">
                                <div class="form-group">
                                    <label>Underwriter</label>
                                    <select class="form-control selectpicker" data-live-search="true" required="required" name="underwriter">
                                        <option value="">Select</option>
                                        <?php
                                            if(count($underwriters) > 0){
                                                foreach($underwriters as $r=>$value){

                                                    echo '<option value="'.$value['id'].'"';

                                                        if($agency_info[0]['assigned_underwriter'] == $value['id']){ echo ' selected="selected"'; }
                                                    echo '>'.$value['first_name'].' '.$value['last_name'].'</option>';
                                                ?>


                                                <?php
                                                }
                                            }

                                        ?>

                                    </select>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary btn-xl" type="submit">Submit</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="baseRateModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                            <p class="pull-right" style="padding-top: 20px;">
                              <a href="#historyModal" class="btn show_history_btn" data-type="base_rate">
                                <i class="fa fa-history"></i> History
                              </a>
                            </p>
                            <h1>Base Rate</h1>
                            <hr class="star-primary">

                            <form class="form-rate form-input-lg">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="rate" value="<?php echo $agency_info[0]['base_rate']?>" />
                                    <input type="hidden" name="type" value="base_rate" />
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary btn-xl" type="submit">Submit</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="minimumPremiumModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                            <p class="pull-right" style="padding-top: 20px;"
                            >
                              <a href="#historyModal" class="btn show_history_btn" data-type="minimum_premium">
                                <i class="fa fa-history"></i> History
                              </a>
                            </p>

                            <h1>Minimum Premium</h1>
                            <hr class="star-primary">

                            <form class="form-rate form-input-lg">
                                <div class="form-group">
                                    <input type="text" class="form-control input-number" name="rate" value="<?php echo $agency_info[0]['minimum_premium']?>" />
                                    <input type="hidden" name="type" value="minimum_premium" />
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary btn-xl" type="submit">Submit</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="policyFeeModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                            <p class="pull-right" style="padding-top: 20px;">
                              <a href="#historyModal" class="btn show_history_btn" data-type="policy_fee">
                                <i class="fa fa-history"></i> History
                              </a>
                            </p>
                            <h1>Policy Fee</h1>
                            <hr class="star-primary">

                            <form class="form-rate form-input-lg">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="rate" value="<?php echo $agency_info[0]['policy_fee']?>" />
                                    <input type="hidden" name="type" value="policy_fee" />
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary btn-xl" type="submit">Submit</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="maxInsuredModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

                            <h1>Max Insured Value</h1>
                            <hr class="star-primary">

                            <form class="form-rate form-input-lg">
                                <div class="form-group">
                                    <label>Max Insured Value</label>
                                    <input type="text" class="form-control input-currency" name="rate" value="<?php echo $agency_info[0]['max_insurance']?>" />
                                    <input type="hidden" name="type" value="max_insurance" />
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary btn-xl" type="submit">Submit</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->


    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="cargoCategoryModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal close-modal-occupation" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>


            <div class="container">




                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="modal-body">


                            <div class="row">

                              <div class="col-lg-12 occupation-header">

                                <p class="pull-right text-right" style="margin-top: 35px;">
                                  <a href="#historyModal" class="btn show_history_btn" data-type="occupation">
                                    <i class="fa fa-history"></i> History
                                  </a>

                                  <a href="#addCargoModal" class="btn" data-toggle="modal" data-controls-modal="addCargoModal"><i class="fa fa-plus" ></i> Add</a>
                                  <a href="#addBulkModal" class="btn" data-toggle="modal" data-controls-modal="addBulkModal"><i class="fa fa-upload" ></i> Upload</a>
                                </p>
                                <h1>
                                  Occupation
                                </h1>
                                <hr class="star-primary">



                                <div class="well">
                                    <input type="search" class="form-control input-search-table" placeholder="Enter occupation.." >
                                </div>

                              </div>
                            </div>




                            <div class="occupation-contentx">

                            <?php
                                if(count($cargo) > 0) {
                            ?>
                            <table class="table table-hover mydataTbx">
                                <thead>
                                    <tr>
                                        <?php /*?><th width="15%">HS Code</th><?php */?>
                                        <th width="60%">Occupation Name</th>
                                        <th width="15%">Multiplier</th>
                                        <th width="10%">Referral</th>
                                        <th width=""></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $countt = 1;
                                    foreach($cargo as $r=>$value){
                                        $acargo_price = 0;
                                        if(isset($cargo_prices[$value['id']])) {
                                            $acargo_price = $cargo_prices[$value['id']];
                                        }
                                        $acargo_max = 0;
                                        if(isset($cargo_max[$value['id']])) {
                                            $acargo_max = $cargo_max[$value['id']];
                                        }
                                    ?>

                                        <tr class="tr-<?php echo $value['id']; ?>">

                                            <td class="text-left">
                                                <span class="price-text" data-id="<?php echo $value['id'] ?>" title="click to edit"><?php echo $value['specific_name']; ?></span>
                                                <form class="text_form hidden">
                                                    <div class="form-group">
                                                        <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $value['specific_name']; ?>" />
                                                        <input type="hidden" name="id" value="<?php echo $value['id']; ?>" />
                                                        <input type="hidden" name="field_name" value="specific_name" />
                                                        <input type="hidden" name="db_name" value="q_business_specific" />
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                                        <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                                                    </div>
                                                </form>
                                            </td>
                                            <td >
                                                <span class="price-text" data-id="<?php echo $value['id'] ?>" title="click to edit"><?php echo $acargo_price; ?></span>
                                                <form class="price_form hidden">
                                                    <div class="form-group">
                                                        <input type="text" name="cargo-price" class="form-control input-sm" value="<?php echo $acargo_price; ?>" />
                                                        <input type="hidden" name="cargo-price-id" value="<?php echo $value['id']; ?>" />
                                                        <input type="hidden" name="cargo-agency-id" value="<?php echo $this->common->default_cargo() ?>" />
                                                        <input type="hidden" name="agency-price-type" value="cargo_prices" />
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                                        <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                                                    </div>
                                                </form>
                                            </td>
                                            <td >
                                                <input type="checkbox" name="cargo_referral[]" value="<?php echo $value['id']; ?>" <?php if($value['referral'] != '') { echo 'checked="checked"'; } ?> />
                                            </td>
                                            <td>
                                                <a href="#" class="btn btn-danger btn-xs delete_btn" data-id="<?php echo $value['id']?>" data-table="q_business_specific"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    <?php $countt++; } ?>



                                </tbody>
                            </table>
                            <?php } ?>

                            </div><!--full height-->



                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="countryModalx" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">

                            <h1>Countries</h1>
                            <hr class="star-primary">

                            <p class="text-right">
                                <a href="#zoneMultiModal" class="btn btn-primary" data-toggle="modal" data-controls-modal="zoneMultiModal">Zone Multiplier</a>
                            </p>

                            <?php
                                if(count($countries) > 0) {
                            ?>
                            <table class="table table-hover mydataTbx">
                                <thead>
                                    <tr>
                                        <th width="75%">Name</th>
                                        <th width="25%">Zone</th>
                                        <th>Referral</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php foreach($countries as $r=>$value){
                                        $acargo_price = 0;
                                        if(isset($country_prices[$value['country_id']])) {
                                            $acargo_price = $country_prices[$value['country_id']];
                                        }
                                    ?>

                                        <tr>
                                            <td><?php echo $value['short_name']; ?></td>
                                            <td >
                                            <span class="price-text" data-id="<?php echo $value['country_id'] ?>" title="click to edit"><?php echo $acargo_price; ?></span>
                                            <form class="price_form hidden">
                                                <div class="form-group">
                                                    <select class="form-control" name="cargo-price">
                                                        <option value="0">Select</option>
                                                        <?php
                                                            for($i = 1; $i < 6; $i++){
                                                                $rate = 0;

                                                                echo '<option value="'.$i.'"';
                                                                if($i == $acargo_price){
                                                                    echo ' selected="selected"';
                                                                }

                                                                echo '>Zone '.$i.'</option>';
                                                            }
                                                        ?>


                                                    </select>
                                                    <input type="hidden" name="cargo-price-id" value="<?php echo $value['country_id']; ?>" />
                                                    <input type="hidden" name="cargo-agency-id" value="<?php echo $this->common->default_cargo() ?>" />
                                                    <input type="hidden" name="agency-price-type" value="country_prices" />

                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                                    <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                                </div>
                                            </form>
                                            <?php /*?><form class="price_form hidden">
                                                <div class="form-group">
                                                    <input type="text" name="cargo-price" class="form-control input-sm" value="<?php echo $acargo_price; ?>" />
                                                    <input type="hidden" name="cargo-price-id" value="<?php echo $value['country_id']; ?>" />
                                                    <input type="hidden" name="cargo-agency-id" value="<?php echo $this->common->default_cargo() ?>" />
                                                    <input type="hidden" name="agency-price-type" value="country_prices" />
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                                    <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                                </div>
                                            </form><?php */?>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="country_referral[]" value="<?php echo $value['country_id']?>" <?php if(isset($country_referral[$value['country_id']])) { echo 'checked="checked"'; } ?> />
                                            </td>
                                        </tr>
                                    <?php } ?>

                                </tbody>
                            </table>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="transportationModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">

                            <p class="pull-right text-right" style="padding-top: 20px;">
                              <a href="#historyModal" class="btn show_history_btn" data-type="employees">
                                <i class="fa fa-history"></i> History
                              </a>
                              <a href="#" class="btn edit_employee_btn">
                                <i class="fa fa-edit"></i> Edit
                              </a>
                            </p>
                            <h1>Employee</h1>
                            <hr class="star-primary">

                            <?php
                                if(count($cargo) > 0) {
                            ?>
                            <table class="table table-hover mydataTbx text-left">
                                <thead>
                                    <tr>
                                        <th width="75%">Name</th>
                                        <th width="25%">Multiplier</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $count = 0;
                                    foreach($transportations as $r){
                                        $acargo_price = 0;
                                        if(isset($transportation_prices[$count])) {
                                            $acargo_price = $transportation_prices[$count];
                                        }
                                    ?>

                                        <tr>
                                            <td><?php echo $r; ?></td>
                                            <td >
                                            <span class="price-text not-editable" data-id="<?php echo $count ?>"><?php echo $acargo_price; ?></span>
                                            <form class="price_form hidden">
                                                <div class="form-group">
                                                    <input type="text" name="cargo-price" class="form-control input-sm" value="<?php echo $acargo_price; ?>" />
                                                    <input type="hidden" name="cargo-price-id" value="<?php echo $count; ?>" />
                                                    <input type="hidden" name="cargo-agency-id" value="<?php echo $this->common->default_cargo() ?>" />
                                                    <input type="hidden" name="agency-price-type" value="transportation_prices" />
                                                </div>
                                                <div class="form-group hidden">
                                                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                                    <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                                </div>
                                            </form>
                                            </td>
                                        </tr>
                                    <?php
                                        $count++;
                                    } ?>



                                </tbody>
                            </table>
                            <?php } ?>

                            <div class="form-group submit_employee_form hidden text-right">
                                <button type="submit" class="btn btn-primary btn-lg submit_employee_btn">Submit</button>
                                <a href="#" class="btn btn-cancel btn-grey btn-lg " data-dismiss="modal">Cancel</a>
                            </div>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="deductibleModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">

                            <p class="pull-right text-right" style="padding-top: 20px;">
                              <a href="#historyModal" class="btn show_history_btn" data-type="deductible">
                                <i class="fa fa-history"></i> History
                              </a>
                              <a href="#addDedModal" class="btn" data-toggle="modal" data-controls-modal="addDedModal">
                                <i class="fa fa-plus"></i> New Deductible
                              </a>
                            </p>
                            <h1>Deductible </h1>
                            <hr class="star-primary">

                            <?php
                                if(count($deductibles) > 0){
                            ?>
                            <table class="table table-hover mydataTbx">
                                <thead>
                                    <tr>
                                        <th width="50%">Deductible</th>
                                        <th width="40%">Multiplier</th>
                                        <th width="10%">Default</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $count = 0;
                                    foreach($deductibles as $r=>$value){

                                    ?>

                                          <tr class="tr-<?php echo $value['id']; ?>">
                                            <td>
                                            <span class="price-text" data-id="<?php echo $value['id'] ?>" title="click to edit"><?php echo $value['deductible']; ?></span>
                                            <form class="price_form hidden">
                                                <div class="form-group">
                                                    <input type="text" name="cargo-price" class="form-control input-sm input-currency" value="<?php echo $value['deductible']; ?>" />
                                                    <input type="hidden" name="cargo-price-id" value="<?php echo $value['id']; ?>" />
                                                    <input type="hidden" name="cargo-agency-id" value="<?php echo $value['id'] ?>" />
                                                    <input type="hidden" name="db-name" value="deductibles" />
                                                    <input type="hidden" name="agency-price-type" value="deductible" />
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
                                                    <a href="#" class="btn btn-default btn-sm pull-right"><i class="fa fa-times"></i></a>
                                                </div>
                                            </form>

                                            </td>
                                            <td >
                                            <span class="price-text" data-id="<?php echo $value['id'] ?>" title="click to edit"><?php echo $value['rate']; ?></span>
                                            <form class="price_form hidden">
                                                <div class="form-group">
                                                    <input type="text" name="cargo-price" class="form-control input-sm" value="<?php echo $value['rate']; ?>" />
                                                    <input type="hidden" name="cargo-price-id" value="<?php echo $value['id']; ?>" />
                                                    <input type="hidden" name="cargo-agency-id" value="<?php echo $value['id'] ?>" />
                                                    <input type="hidden" name="db-name" value="deductibles" />
                                                    <input type="hidden" name="agency-price-type" value="rate" />
                                                </div>
                                                <div class="form-group">
                                                  <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save"></i></button>
                                                  <a href="#" class="btn btn-default btn-sm pull-right"><i class="fa fa-times"></i></a>
                                                </div>
                                            </form>
                                            </td>
                                            <td>
                                                <input type="checkbox" name="ded_default[]" value="<?php echo $value['id']?>" data-agency="<?php echo $this->common->default_cargo(); ?>" <?php if($value['not_default'] == 'N') { echo 'checked="checked"';}?> />
                                            </td>
                                            <td>
                                                <a href="#" class="btn btn-danger btn-xs delete_btn" data-id="<?php echo $value['id']?>" data-table="deductibles"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    <?php
                                        $count++;
                                    } ?>



                                </tbody>
                            </table>
                            <?php }

                                else{
                                    echo '<p class="text-center text-muted">No deductibles</p>';
                                }

                            ?>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->




    <!-- Modal -->
    <div class="modal fade" id="addBulkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Upload Bulk Occupation</h4>
          </div>


            <div class="hidden">
                <form id="myCsvForm" action="<?php echo base_url().'webmanager/ratings/upload' ?>" method="post" enctype="multipart/form-data">
                     <input type="file" size="99999" id="mycsv_input" name="mycsv_input" onchange="$('#myCsvForm').submit();">
                     <input type="hidden" class="bulk_id" name="bulk_id" />
                     <input type="submit" class="hidden" value="Ajax File Upload">
                 </form>
            </div>


              <div class="modal-body">

                <div class="form-group text-center">
                    <a href="javascript:;" class="btn btn-primary btn-lg upload_file_btn" onclick="$('#mycsv_input').click();"><i class="fa fa-cloud-upload"></i> Select File to Upload (CSV)</a>
                </div>

                <div class="form-group" style="margin-top: 15px;">
                    <div class="text-danger bulk_error_mess text-center hidden"></div>
                </div>

                <div class="form-group">
                    <div id="progress" class="progress-csv" style="display: none;">
                        <div id="bar" class="bar-csv"></div>
                        <div id="percent" class="percent-csv">0%</div >
                    </div>
                </div>

                <div class="form-group">
                    <div id="file_up_text" style="display: none;"><div id="message"><a href=""></a></div></div>
                </div>

                <div class="form-group" style="border-top: 1px solid #eee; margin-top: 15px; padding-top: 15px;">
                    <p class="text-center">Example File (.csv file extension)</p>
                    <img src="<?php echo base_url().'assets/img/occupation-template.jpg' ?>" alt="" class="img-responsive thumbnail" />
                </div>

                <div class="form-group">
                    <a href="<?php echo base_url().'assets/img/occupation-template.csv' ?>" target="_blank">Download sample file</a>
                </div>


              </div>

        </div>
      </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="addCargoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">New Occupation</h4>
          </div>
          <form id="add_cargo_form">
              <div class="modal-body" style="padding: 15px 30px;">
                <div class="form-group">
                    <label>Occupation Name</label>
                    <input type="text" class="form-control" name="name" />
                </div>
                <div class="form-group">
                    <label>Multiplier</label>
                    <input type="text" class="form-control" name="price" />
                </div>
                <div class="form-group">
                    <label>Referral</label><br />
                    <input type="checkbox" name="referral" value="Yes"/>
                </div>
              </div>
              <div class="modal-footer">
                <a href="#" class="btn btn-cancel btn-grey" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
          </form>
        </div>
      </div>
    </div>


      <!-- Modal -->
      <div class="modal fade" id="historyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">History</h4>
            </div>

            <div class="modal-body">
                <div class="history-content">
                </div>
            </div>
            <div class="modal-footer">
              <a href="#" class="btn btn-cancel btn-primary" data-dismiss="modal">OK</a>
            </div>

          </div>
        </div>
      </div>



      <!-- Portfolio Modals -->
      <div class="portfolio-modal modal fade" id="quoteExpiryModal" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-content">
              <div class="close-modal" data-dismiss="modal">
                  <div class="lr">
                      <div class="rl">
                      </div>
                  </div>
              </div>

              <div class="container">
                  <div class="row">
                      <div class="col-md-6 col-md-offset-3">
                          <div class="modal-body">
                              <p class="pull-right" style="padding-top: 20px;">
                                <a href="#historyModal" class="btn show_history_btn" data-type="base_currency">
                                  <i class="fa fa-history"></i> History
                                </a>
                              </p>
                              <h1>Quote Expiry</h1>
                              <hr class="star-primary">

                              <form class="form-rate form-input-lg">
                                  <div class="form-group">
                                      <input type="text" class="form-control input-currency" name="rate" value="<?php echo $agency_info[0]['base_currency']?>" />
                                      <input type="hidden" name="type" value="base_currency" />
                                  </div>
                                  <div class="form-group text-right">
                                      <a href="#" class="btn btn-cancel btn-grey" data-dismiss="modal">Cancel</a>
                                      <button class="btn btn-primary btn-xl" type="submit">Submit</button>
                                  </div>
                              </form>

                          </div>
                      </div>
                  </div>
              </div>


          </div>
      </div><!--portfolio-modal-->


    <!-- Modal -->
    <div class="modal fade" id="addDedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">New Deductible</h4>
          </div>
          <form id="add_ded_form">
              <div class="modal-body" style="padding: 15px 30px;">
                <div class="form-group">
                    <label>Deductible</label>
                    <input type="text" class="form-control input-currency" name="name" />
                </div>
                <div class="form-group">
                    <label>Multiplier</label>
                    <input type="text" class="form-control" name="price" />
                </div>
              </div>
              <div class="modal-footer">
                <a href="#" class="btn btn-grey btn-cancel" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
          </form>
        </div>
      </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="baseCurrencyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Base Currency</h4>
          </div>
          <form id="base_curr_form">
                <input type="hidden" name="id" value="<?php echo $this->common->default_cargo(); ?>" />
              <div class="modal-body">
                  <div class="form-group">
                    <label for="currency">Currency</label>
                    <select class="form-control" name="currency" id="currency" data-live-search="true">
                      <option value="">Select Currency</option>
                      <?
                      foreach($currencies as $r=>$value){

                          echo '<option value="'.$value['currency'].'"';
                          if($base_currency == $value['currency']){
                              echo ' selected="selected"';
                          }
                          echo '>'.$value['name'].'</option>';

                      }?>

                    </select>
                  </div>

              </div>
              <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
          </form>
        </div>
      </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="zoneMultiModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Zone Multiplier</h4>
          </div>
          <form id="zone_multi_form">
                <input type="hidden" name="id" value="<?php echo $this->common->default_cargo(); ?>" />
              <div class="modal-body">
                <?php
                    for($i = 1; $i < 6; $i++){
                        $rate = 0;
                        if(isset($zone_multiplier[$i])){
                            $rate = $zone_multiplier[$i];
                        }
                ?>
                <div class="row form-group">
                    <div class="col-sm-6">
                        <label>Zone <?php echo $i; ?></label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="zone[]" value="<?php echo $rate; ?>" />
                    </div>
                </div>
                <?php } ?>
              </div>
              <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
          </form>
        </div>
      </div>
    </div>

<!--end cargo tab panel-->
<?php } ?>


<?php
  if($this->session->userdata('id')){
    $id = $this->session->userdata('id');

    $user = $this->common->customer_format($id);

    $brokerage = $this->master->getRecords('brokerage', array('id'=>$user['brokerage_id']));

    $countries = $this->common->country_format();

?>


    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="myProfileModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

                            <h1>My Profile</h1>
                            <hr class="star-primary">


                            <p class="text-center">


                            		<a href="#" onclick="$('#myfile').click(); return false;" title="click to update avatar">

                                 <img src="<?php echo $this->common->avatar($this->session->userdata('id'), 'customer') ?>" class="img-page-preview img-circle img-thumbnail" alt="avatar" style="width: 146px;"/>

                               </a><br /><span class="small text-muted">150px minimum size recommended</span>

                            </p>





                            <form id="customer_details_form" novalidate="novalidate" class="text-left">
                                <input type="hidden" name="id" value="<?php echo $user['id'] ?>" />


                                <div class="row row-prime-contact">


                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" class="form-control" name="first" placeholder="First Name" value="<?php echo $user['first_name']?>" required="required"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" class="form-control" name="last" placeholder="Last Name" value="<?php echo $user['last_name']?>" required="required"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-prime-contact">

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="">Position</label>
                                            <input type="text" placeholder="Position" class="form-control" name="domain" value="<?php echo $user['position']?>" required="required"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="">Email</label>
                                            <input type="email" placeholder="Email" class="form-control" name="email" value="<?php echo $user['email']?>" readonly="readonly"/>
                                        </div>
                                    </div>

                                </div>



                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Country Calling Code</label>
                                            <select class="form-control country_code_dd" name="country_code" data-live-search="true" required="required">
                                                <option value="" data-code="">Select..</option>
                                                <?php
                                                    foreach($countries as $r=>$mc){
                                                        echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';

                                                        if($user['country_id'] == $mc['country_id']){
                                                            echo ' selected="selected"';
                                                        }


                                                        echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group mobile_input">

                                              <label>Mobile number</label>
                                              <span class="mobile-icon">
                                                <i class="fa fa-mobile-phone fa-2x"></i>
                                              </span>
                                              <input type="text" class="form-control mobile_number" placeholder="Mobile Number" name="mobile" autocomplete="nope" value="<?php echo $user['calling_digits'] ?>">

                                            <input type="hidden" name="country_short" value="<?php echo $user['calling_code'] ?>"/>
                                        </div>
                                    </div>

                                </div><!--phone number-->



                                <div class="form-group text-right">
                                    <a href="#" data-dismiss="modal" class="btn btn-cancel btn-grey">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>


                            </form>




                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->


     <div class="row hidden">
      <form id="myFormAvatar" action="<?php echo base_url().'settings/update_avatar' ?>" method="post" enctype="multipart/form-data">
           <input type="file" size="60" id="myfile" name="myfile" class="myfile-profile" onchange="$('#myFormAvatar').submit();">
           <input type="hidden" id="avatar_name" value="">
           <input type="hidden" id="pagetype" value="avatar">
           <input type="submit" class="hidden" value="Ajax File Upload">
       </form>
      </div>



    <!-- Modal -->
    <div class="modal fade" id="cropImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <p class="lead">Crop</p>

            <div class="bootstrap-modal-cropper" id="customer_img"><img src="<?php echo base_url().'assets/img/avatar/background.png' ?>" class="img-responsive"/></div>



          </div>
          <div class="modal-footer">
            <a class="btn btn-primary sav_crop">Save changes</a>
          </div>
        </div>
      </div>
    </div>


<?php
  $agency_id = $this->session->userdata('agency_id');
  $agency = $this->master->getRecords('agency', array('id'=>$agency_id));

 ?>


    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="agencyProfileModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

                            <h1>Agency Profile</h1>
                            <hr class="star-primary">


                            <form id="agencies_form" novalidate="novalidate" class="text-left">
                                <input type="hidden" name="id" value="<?php echo (isset($agency[0]['id'])) ? $agency[0]['id'] : '' ?>" />
                                <input type="hidden" name="utype" />

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Underwriting Agency Name</label>
                                            <input type="text" class="form-control" name="name" required="required" value="<?php echo $agency[0]['name'] ?>"/>
                                        </div>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label>Floor/Suite number</label>
                                    <input type="text" class="form-control" name="background" required="required" value="<?php echo $agency[0]['background'] ?>"/>
                                </div>


                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control geolocation" name="address" required="required" value="<?php echo $agency[0]['address'] ?>"/>
                                    <input type="hidden" name="lat" value="<?php echo $agency[0]['lat'] ?>" />
                                    <input type="hidden" name="lng" value="<?php echo $agency[0]['lng'] ?>"/>
                                </div>



                                <div class="form-group text-right">
                                    <a href="<?php echo base_url().'webmanager/dashboard'?>" class="btn btn-grey">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>


                            </form>





                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->


<?php } //my profile settings modal ?>



      <ul class="map-details hidden">
        <li>Location: <span data-geo="location"></span></li>
        <li>Route: <span data-geo="route"></span></li>
        <li>Street Number: <span data-geo="street_number"></span></li>
        <li>Postal Code: <span data-geo="postal_code"></span></li>
        <li>Locality: <span data-geo="locality"></span></li>
        <li>Country Code: <span data-geo="country_short"></span></li>
        <li>State: <span data-geo="administrative_area_level_1"></span></li>
      </ul>


    <script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/browserDetection.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/js/jquery.easing.min.js"></script>
    <script src="<?php echo base_url() ?>assets/js/jquery.fittext.js"></script>
    <script src="<?php echo base_url() ?>assets/js/wow.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&amp;key=AIzaSyDVL8WaKGrSPhJ7ZY8XHrJeWascHtNA0qc"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="<?php echo base_url() ?>assets/plugins/geocomplete/jquery.geocomplete.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/pwstrength/pwstrength-bootstrap.js"></script>



    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/dataTables.bootstrap.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/dataTables/js/datatables-bs3.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/validate/jquery.validate.min.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/validate/additional-methods.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bootbox/bootbox.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/gridstack/underscore.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/gridstack/gridstack.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/gridstack/gridstack.jQueryUI.js"></script>


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/jquery.form/jquery.form.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/jquery-ui/jquery.maskedinput.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/dependencies/moment.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/datetimepicker/js/bootstrap-datetimepicker.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bs-switch/js/bootstrap-switch.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/summernote/js/summernote.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap-switch/js/bootstrap-switch.js"></script>



    <!-- <script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal-stack/js/bootstrap-modalmanager.js"></script>
    <script src="<?php echo base_url() ?>assets/plugins/bootstrap-modal-stack/js/bootstrap-modal.js"></script> -->


    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/cropper/dist/cropper.min.js"></script>
    <?php /*?><script src="<?php echo base_url() ?>assets/plugins/cropper/js/crop-avatar.js"></script><?php */?>

    <!-- Plugin JavaScript -->
    <script src="<?php echo base_url() ?>assets/plugins/silviomoreto-bootstrap-select/js/bootstrap-select.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url() ?>assets/admin/js/admin-script.js"></script>
    <script src="<?php echo base_url() ?>assets/admin/js/admin-validation.js"></script>
    <script src="<?php echo base_url() ?>assets/js/script.js"></script>



    <?php if($this->uri->segment(2) == 'social_login' && $this->uri->segment(1) == 'settings'){ ?>
    	<script src="https://apis.google.com/js/api:client.js"></script>

    	<script>


      		if(uri_1 == 'settings' && uri_2 == 'social_login'){

      			// $(document).ready(function(e) {
            //   $('.google-auth-btn').on('click', function(){
      			// 		$('#customBtn').click();
      			// 	});
            //   $('.linkedin-auth-btn').on('click', function(){
      			// 		$('#MyLinkedInButton').click();
      			// 	});
            //
            // });

      		  var googleUser = {};
      		  var startApp = function() {
      			gapi.load('auth2', function(){
      			  // Retrieve the singleton for the GoogleAuth library and set up the client.
      			  auth2 = gapi.auth2.init({
      				client_id: '253167228155-mnp9rtvui3t45cben3u1vag21n4vcupk.apps.googleusercontent.com',
      				cookiepolicy: 'single_host_origin',
      				// Request scopes in addition to 'profile' and 'email'
      				//scope: 'additional_scope'
      			  });
      			  attachSignin(document.getElementById('customBtn'));
      			});
      		  };

      		  function attachSignin(element) {
      			console.log(element.id);
      			auth2.attachClickHandler(element, {},
      				function(googleUser) {
      					var profile = googleUser.getBasicProfile();
      					console.log("ID: " + profile.getId()); // Don't send this directly to your server!
      					console.log('Full Name: ' + profile.getName());
      					console.log('Given Name: ' + profile.getGivenName());
      					console.log('Family Name: ' + profile.getFamilyName());
      					console.log("Image URL: " + profile.getImageUrl());
      					console.log("Email: " + profile.getEmail());

      					var $mdl = $('#myLoadingModal');
      					$mdl.modal('show');

      					var data = {
      						id: profile.getId(),
      						full_name: profile.getName(),
      						first_name: profile.getGivenName(),
      						last_name: profile.getFamilyName(),
      						avatar: profile.getImageUrl(),
      						email: profile.getEmail(),

      					};
      					console.log(data);
      					$.post(
      						base_url+'formsubmits/googleplus',
      						{data: data, uri_1: uri_1, id: "<?php echo (isset($user_sess['id'])) ? $user_sess['id'] : ''?>"},
      						function(res){
      							console.log(res);
      							//return false;

      							if(res.type == 'signup'){
      								bootbox.alert('User not found.');
      								// var d = new Date();
      								// var n = d.getMilliseconds();
      								//
      								// window.location.href = base_url + '?n=' + n + '#signup';
      							}
      							else if(res.type == 'complete_signup'){
      								$('#customBtn').html('<i class="fa fa-google-plus text-danger"></i> Connnected to Google+').addClass('disabled');
      								bootbox.alert(action_messages.success.complete_profile_via_social);
      							}
                    else if(res.type == 'connect'){
                      $('#customBtn').html('<i class="fa fa-google-plus text-danger"></i> Connnected to Google+').addClass('disabled');
                      bootbox.alert('Google+ account successfully connected.');
                    }
      							else{
      								if(res.userdata.super_admin == 'Y'){
      									window.location.href = base_url+'webmanager';
      									console.log(base_url+'webmanager');
      								}
      								else{
      									window.location.href = base_url+'panel'; //'http://'+res.userdata.domain+'.ebinder.com.au/panel';
      									console.log(base_url+'panel');
      								}


      							}
      						},
      						'json'
      					).done(function(res){
      						$mdl.modal('hide');
      					}).error(function(err){
      						console.log(err);
      					});



      	//			  document.getElementById('name').innerText = "Signed in: " +
      	//				  googleUser.getBasicProfile().getName();
      				}, function(error) {
      				  alert(JSON.stringify(error, undefined, 2));
      				});
      		  }

      		  startApp();
      		}

          </script>


          <script type="text/javascript" src="//platform.linkedin.com/in.js">
              api_key: 8198kchnjgi54x
              authorize: true
              onLoad: onLinkedInLoad
          </script>

          <script type="text/javascript">
      		if(uri_1 == 'settings' && uri_2 == 'social_login'){
      			// Setup an event listener to make an API call once auth is complete
      			function onLinkedInLoad() {
      				IN.Event.on(IN, "auth", getProfileData);
      			}

      			// Use the API call wrapper to request the member's basic profile data
      			function getProfileData() {
      				if(IN.User.isAuthorized()){
      					IN.API.Raw("people/~:(id,first-name,last-name,location,email-address)?format=json").result(onSuccess).error(onError);
      				}
      			}


      			var linkedData = {};
      			var linked_fetch = 0;
      			// Handle the successful return from the API call
      			function onSuccess(profile) {
      				var linked = $("#MyLinkedInButton").data('linked');

      				//$('#MyLinkedInButton').addClass('disabled');

      				var data = {
      					id: profile.id,
      					full_name: profile.firstName+' '+profile.lastName,
      					first_name: profile.firstName,
      					last_name: profile.lastName,
      					avatar: '',
      					email: profile.emailAddress
      				};

      				console.log('onLinkedInLoad', profile);
      				console.log(data);
      				linkedData = data;

      				if(typeof(linked) !== 'undefined'){
      					var $mdl = $('#myLoadingModal');
      					$mdl.modal('show');

      					$.post(
      						base_url+'formsubmits/linkedin',
      						{data: data, uri_1: uri_1, id: "<?php echo (isset($user_sess['id'])) ? $user_sess['id'] : ''?>"},
      						function(res){
      							console.log(res);

      							if(res.type == 'signup'){
      								bootbox.alert('User not found.');
      								// var d = new Date();
      								// var n = d.getMilliseconds();
      								//
      								// window.location.href = base_url + '?n=' + n + '#signup';
      							}
      							else if(res.type == 'complete_signup'){
      								$('#MyLinkedInButton').html('<i class="fa fa-linkedin"></i> Connnected to LinkedIn').addClass('disabled');
      								bootbox.alert(action_messages.success.complete_profile_via_social);
      							}
      							else if(res.type == 'connect'){
      								$('#MyLinkedInButton').html('<i class="fa fa-linkedin"></i> Connnected to LinkedIn').addClass('disabled');
      								bootbox.alert('LinkedIn account successfully connected.');
      							}

      							else{
      								if(res.userdata.super_admin == 'Y'){
      									window.location.href = base_url+'webmanager';
      									console.log(base_url+'webmanager');
      								}
      								else{
      									window.location.href = base_url+'panel'; //'http://'+res.userdata.domain+'.ebinder.com.au/panel';
      									console.log(base_url+'panel');
      								}

      							}
      						},
      						'json'
      					).done(function(res){
      						$mdl.modal('hide');
      					}).error(function(err){
      						console.log(err);
      					});

      				}
      				linked_fetch ++;


      			}

      			// Handle an error response from the API call
      			function onError(error) {
      				console.log(error);
      			}

      			$(document).ready(function(e) {

      				$("#MyLinkedInButton").on('click',function () {

      					console.log('MyLinkedInButton', linkedData);
      					var data = linkedData;
      					var $self = $(this);
      					$self.data('linked',data);

      					if(typeof(linkedData.id) !== 'undefined'){
      						var $mdl = $('#myLoadingModal');
      						$mdl.modal('show');

      						$.post(
      							base_url+'formsubmits/linkedin',
      							{data: data, uri_1: uri_1, id: "<?php echo (isset($user_sess['id'])) ? $user_sess['id'] : ''?>"},
      							function(res){
      								console.log(res);

      								if(res.type == 'signup'){
      									bootbox.alert('User not found.');
      									// var d = new Date();
      									// var n = d.getMilliseconds();
      									//
      									// window.location.href = base_url + '?n=' + n + '#signup';
      								}
      								else if(res.type == 'complete_signup'){
      									$('#MyLinkedInButton').html('<i class="fa fa-linkedin"></i> Connnected to LinkedIn').addClass('disabled');
      									bootbox.alert(action_messages.success.complete_profile_via_social);
      								}
      								else if(res.type == 'connect'){
      									$('#MyLinkedInButton').html('<i class="fa fa-linkedin"></i> Connnected to LinkedIn').addClass('disabled');
                        bootbox.alert('LinkedIn account successfully connected.');
      								}

      								else{
      									if(res.userdata.super_admin == 'Y'){
      										window.location.href = base_url+'webmanager';
      										console.log(base_url+'webmanager');
      									}
      									else{
      										window.location.href = base_url+'panel'; //'http://'+res.userdata.domain+'.ebinder.com.au/panel';
      										console.log(base_url+'panel');
      									}

      								}
      							},
      							'json'
      						).done(function(res){
      							$mdl.modal('hide');
      						}).error(function(err){
      							console.log(err);
      						});

      					}

      					return false;
      				});

      				$("#MyLinkedInButton").bind('click',function () {
      					IN.User.authorize();
      					//
      					// IN.Event.on(IN, 'auth', function(){
      					// 	/* your code here */
      					// 	//for instance
      					// 	console.log('auth success');
      					// 	IN.API.Raw("people/~:(id,first-name,last-name,location,email-address)?format=json").result(function(res){
      					// 		console.log('peps', res);
      					// 	}).error(onError);
      					// });

      					return false;
      				});


      			});
      		}

          </script>

    <?php } ?>






</body>
</html>
