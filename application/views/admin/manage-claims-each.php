<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>
                        <li><a href="<?php echo base_url()?>webmanager/products" class="preloadThis">Products</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">


                <div class="panel panel-default">

                
                	<div class="panel-heading">
						<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>
                    
                  
                
					<div class="panel-body">
                    
                    
                    
           		
                <div class="row">
                	<div class="col-sm-3">
                    
                
                        <?php if(count($claims) > 0) { ?>
                
                        
                        
						<div class="table-responsivex">
                        
                        <table class="table table-striped table-hoverx table-datatablex">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <?php /*?><th>Claim</th>
                              <th>Details</th>
                              <th>Customer</th>
                              <th>Status</th><?php */?>
                            </tr>
                          </thead>
                          <tbody>                        
                        
                        <?php
							$count = 1;
							foreach($claims as $u=>$value){
								
						?>
                        
                            <tr>
                             
                              <td class="hidden"><?php echo $count; ?></td>
                            </tr>
                            <tr>
                            
                              <td>Claim: <?php echo $value['claim'] ?></td>
                            </tr>
                            <tr>
                              <td>Details: <?php echo $value['details'] ?></td>
                            </tr>
                            <tr>
                              <td>Customer: <?php echo $this->common->customer_name($value['customer_id']) ?></td>
                            </tr>
                            <tr>
                              <td>Status: <?php echo $this->common->claim_status($value['status']) ?></td>
                              
                            </tr>
                            <tr>
                              <td>
                                    <a href="javascript:;" class="pull-right btn btn-default btn-sm show-biz-response btn-block" data-id='<?php echo $value['policy_id'];?>'>Insurance details</a>                              
                              </td>
                              
                            </tr>
                        <?php
								$count++;
							}
						?>


                          
                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->
                          
                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';	
						}?>                
                

                    
                    </div>
                
                	<div class="col-sm-9" style="border-left: 1px solid #eee">
                    
                                        
                    
                            
                  
        
                        <h3>
            
                            <!-- Single button -->
                            <div class="btn-group pull-right">
                              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                Action <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu claims-action-btn" data-id="<?php echo $message_id; ?>" role="menu">
                              	
                              
                                <li class="<?php echo ($claims[0]['status'] == 'S') ? '' : 'hidden' ?>"><a href="javascript:;" data-status="P">Pending Acceptance</a></li>
                                <li class="<?php echo ($claims[0]['status'] == 'P') ? '' : 'hidden' ?>"><a href="javascript:;" data-status="U">Under Review</a></li>
                                <li class="<?php echo ($claims[0]['status'] == 'U' || $claims[0]['status'] == 'R') ? '' : 'hidden' ?>"><a href="javascript:;" data-status="D">Decline</a></li>
                                <li class="<?php echo ($claims[0]['status'] == 'U' || $claims[0]['status'] == 'R') ? '' : 'hidden' ?>"><a href="javascript:;" data-status="A">Approve</a></li>
                                <li class="<?php echo ($claims[0]['status'] == 'A' || $claims[0]['status'] == 'D') ? '' : 'hidden' ?>"><a href="javascript:;" data-status="R">Re-open</a></li>
            
                              </ul>
                            </div>                              
                        
                            Messages
                        
                        </h3>
                        <!-- chat-area -->
                        <section class="chat-area">
                            
                            <div class="chat clearfix">
                            <?php if(count($messages) > 0) {
                                foreach($messages as $r=>$value) {
                                    
                                    if($user['id'] == $value['seeker_id']) {
                                ?>
        
                                <div class="chat__item">
                                    <div class="chat__item-wrap">
                                        <div class="avatar">
                                            <div class="avatar__img" data-toggle="tooltip" data-title="<?php echo $this->common->customer_name($value['seeker_id']); ?>">
                                                <img src="https://placeholdit.imgix.net/~text?txtsize=150&txt=<?php echo substr($this->common->customer_name($value['seeker_id']), 0, 1); ?>&w=200&h=200">
                                            </div>
                                        </div>
                                        <div class="chat__item-message-wrap">
                                            <div class="triangle"></div>
                                            <div class="chat__item-message">
                                                <?php
                                                    if($value['message'] != ''){
                                                        echo $value['message'];
                                                    }
                                                    else{
                                                        echo '<a href="'.base_url().'uploads/message-uploads/'.$value['file_upload_name'].'" target="_blank">'.substr($value['file_upload_name'], 13).'</a>';
                                                    }
                                                ?>
                                            
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat__date">
                                        <span><?php echo date_format(date_create($value['date_added']), 'd M Y, h:i:s A')?> </span> 
                                        <i class="fa fa-check-circle fa-fw text-success"></i>
        
                                    </div>
                                </div>
                                
                                <?php } else { ?>
                                <div class="chat__item right">
                                    <div class="chat__item-wrap">
                                        <div class="chat__item-message-wrap">
                                            <div class="triangle"></div>
                                            <div class="chat__item-message">
                                                <?php
                                                    if($value['message'] != ''){
                                                        echo $value['message'];
                                                    }
                                                    else{
                                                        echo '<a href="'.base_url().'uploads/message-uploads/'.$value['file_upload_name'].'" target="_blank">'.substr($value['file_upload_name'], 13).'</a>';
                                                    }
                                                ?>
                                            
                                            </div>
                                        </div>
                                        <div class="avatar">
                                            <div class="avatar__img" data-toggle="tooltip" data-title="<?php echo $this->common->customer_name($value['seeker_id']); ?>">
                                                <img src="https://placeholdit.imgix.net/~text?txtsize=150&txt=<?php echo substr($this->common->customer_name($value['seeker_id']), 0, 1); ?>&w=200&h=200">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="chat__date pull-right">
                                        <span><?php echo date_format(date_create($value['date_added']), 'd M Y, h:i:s A')?> </span> 
                                        <i class="fa fa-check-circle fa-fw text-success"></i>
                                    </div>
                                </div>
        
                                
                                
                            <?php } } } ?>
        
                            </div>
        
                            <div class="chat-area__bottom">
                                <?php /*?><a class="left fa fa-paperclip fa-2x text-muted fa-fw"></a><?php */?>
                                <input class="textfield" type="" name="chat" placeholder="Type something...">
                                <button class="right claim-chat-send" data-id="<?php echo $user['id']?>" data-letter="<?php echo substr($user['first_name'], 0, 1) ?>" data-message_id = "<?php echo $message_id ?>" data-to = "<?php echo $to ?>">
                                    <i class="fa fa-send fa-2x text-primary"></i>
                                </button>
                            </div>
                        </section>
                        <!-- chat-area -->
                        
                        

                        
                    </div><!-- col-->
                
                </div><!-- row -->                            
                            
                    
                    
                    </div>
                </div>

            </div>


        </div><!--.row-->


    
</div>




<!-- Portfolio Modals -->
<div class="portfolio-modal modal fade" id="formModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="modal-body">
                    
                        <h1>Quote details</h1>
                        <hr />


                        <div class="the_referral_response text-left">
                            theview
                        
                        </div>

                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
</div><!--portfolio-modal-->      