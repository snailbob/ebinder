<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage Popup Messages</h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage Popup Messages</li>

                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

					<?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading">

                        <div class="panel-title">

                            <h4>Popup Messages</h4>

                        </div>
                        <div class="clearfix"></div>

                    </div>


                        							<div class="table-responsive">

							<table class="table table-hover table-striped" id="example-table">

                                <thead>

                                    <tr>

                                        <th width="20%">Name</th>
                                        <th width="20%">Confirm message</th>
                                        <th width="20%">Success Message</th>
                                        <th width="20%">Error/Info Message</th>
                                        <th></th>

                                    </tr>

                                </thead>

								<tbody>

								<?php

								if(count($messages)>0)

								{

									foreach($messages as $r => $value)

									{

							?>

                                    <tr>

                                    	<td><?php echo $value['name']; ?><br /><small class="text-muted"><?php echo $value['description']; ?></small></td>

                                    	<td><?php echo $value['confirm_message']; ?></td>

                                    	<td><?php echo $value['success_message']; ?></td>

                                    	<td><?php echo $value['error_message']; ?></td>

                                        <td>


        <!-- Single button -->
        <div class="btn-group pull-right">
            <button type="button" class="btn btn-default dropdown-toggle btn-sm" data-toggle="dropdown">
            Action <span class="caret"></span>
            </button>
          <ul class="dropdown-menu" role="menu" style="font-size: 90%; min-width: 82px;">
            <li>
                <a href="#" class="update_valmess_btn" data-err="<?php echo $value['error_message']; ?>" data-succ="<?php echo $value['success_message']; ?>" data-conf="<?php echo $value['confirm_message']; ?>" data-desc="<?php echo $value['description']; ?>" data-id="<?php echo $value['id']; ?>">Edit</a>
            </li>
            <li class="hidden">
                <a href="javascript:;" class="delete_valmess_btn" data-id="<?php echo $value['id'];?>">Delete</a>
            </li>

          </ul>
        </div>

                                       </td>

                                    </tr>

								<?php



									}

								}

								?>



                                                </tbody>





                                            </table>

                                        </div>

                                        <!-- /.table-responsive -->





                </div>

            </div>


        </div><!--.row-->



</div>



<!-- Modal -->
<div class="modal fade" id="valMessModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <form id="valmess_form" novalidate="novalidate">
        	<div class="form-group">
            	<label>Description</label>
                <input type="text" class="form-control" name="description" readonly="readonly" />
                <input type="hidden" class="form-control" name="id" />
            </div>

        	<div class="form-group">
            	<label>Confirm Message</label>
                <input type="text" class="form-control" name="confirm" />
                <input type="hidden" class="form-control" name="id" />
            </div>

        	<div class="form-group">
            	<label>Success Message</label>
                <input type="text" class="form-control" name="success" />
                <input type="hidden" class="form-control" name="id" />
            </div>

        	<div class="form-group">
            	<label>Error Message</label>
                <input type="text" class="form-control" name="error" />
                <input type="hidden" class="form-control" name="id" />
            </div>

            <div class="form-group text-right">
                <button class="btn btn-default btn-cancel btn-grey" data-dismiss="modal">Cancel</button>
                <button class="btn btn-primary" type="submit">Submit</button>
            </div>


        </form>
      </div>
    </div>
  </div>
</div>
