<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">

                <div class="panel panel-default">


                	<div class="panel-heading">
                        <a href="#addAgencyModal" class="show_mdl_form_btn btn btn-primary pull-right <?php echo ($this->common->default_cargo() == '6') ? 'hidden' :''?>" onclick="$('#addAgencyModal').find('.form-control').val(''); $('#addAgencyModal').find('h1').html('Add Broker');" data-controls-modal="#addAgencyModal"><i class="fa fa-plus"></i> <?php echo $singular_title; ?></a>



                    	<div  class="panel-title">
                            <h4>Manage <?php echo $title; ?></h4>
						</div>
                    </div>

                        <?php if(count($users) > 0) { ?>
                        <div class="table_search">
                            <input type="text" class="form-control input_table_search" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>



						<div class="table-responsivex">

                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <th>ID</th>
                              <th>Date Added</th>
                              <th>Name</th>
                              <th>Status</th>
                              <th>Level/suite</th>
                              <th>Street address</th>
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>

                        <?php
                        $unique_link = 'N';
                        if(isset($logged_user['agency']['agency_details']['unique_link'])) {
                          $unique_link = $logged_user['agency']['agency_details']['unique_link'];
                        } ?>
                        <?php
                        $countt = 1;
							foreach($users as $u=>$user){
								$white = $this->common->db_field_id('brokerage', 'enable_whitelabel', $user['brokerage_id']);
                $brokerage = $this->master->getRecords('brokerage', array('id'=>$user['brokerage_id']));
                $user['brokerage'] = (!empty($brokerage)) ? $brokerage[0] : array();
                $user['privileges'] = (@unserialize($user['privileges'])) ? unserialize($user['privileges']) : array();
                $user['privileges_text'] = $this->common->product_enabled_text($user['privileges']);
                $user['unique_link'] = $unique_link;

						?>

                            <tr class="<?php echo (($countt == 1 && strpos($this->session->flashdata('success'), 'added') !== false) || (strpos($this->session->flashdata('success'), 'broker_'.$user['brokerage_id']) !== false)) ? 'flash_animation' : '' ?>">
                              <td class="hidden"><?php echo $countt; ?></td>
                              <td>
                                <?php echo $this->common->brokercode($user['id']) ?>
                              </td>
                              <td>
                                <?php echo date_format(date_create($user['date_added']), 'd-m-Y') ?>
                              </td>

                              <td>
                                <a href="javascript:;" title="click to view details" class="customer-update2" data-id="<?php echo $user['id'] ?>" data-arr='<?php echo json_encode($user) ?>'>
                                  <?php echo (isset($user['brokerage']['company_name'])) ? $user['brokerage']['company_name'] : 'NA'; ?>
                                </a>
                              </td>
                              <td>
                  							  	<?php
                                      if($user['enabled'] == 'Y' && !empty($user['password'])){
                                        echo '<span class="text-success">Activated</span>';
                                      }
                                      else if($user['enabled'] == 'N' && !empty($user['password'])){
                                        echo '<span class="text-red">Deactivated</span>';
                                      }
                                    	else if($user['status'] == 'Y'){
                    										echo '<span class="text-success">Verified</span>';
                    									}
                    									else{
                    										echo '<span class="text-red">Not Verified</span>';
                    									}
                  								?>
                              </td>

                              <td>
                                <?php echo $user['suite_number'] ?>
                              </td>
                              <td>
                                <?php echo $user['location'] ?>
                              </td>


                              <td>

                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a href="javascript:;" class="customer-update2" data-id="<?php echo $user['id'] ?>" data-arr='<?php echo json_encode($user) ?>'>Update</a></li>

                                        <?php if($this->session->userdata('logged_admin') == 'admin') {?>
                                        <li><a href="<?php echo base_url().'webmanager/customers/accessportal/'.$user['id']?>" target="_blank">Access Portal</a></li>
                                        <?php } ?>


                                        <?php if($user['enabled'] == 'Y') {?>
                                        <li><a href="javascript:;" onclick="customerActivate('N','<?php echo $user['id']; ?>');">Deactivate</a></li>
                                        <?php } else {?>
                                        <li><a href="javascript:;" onclick="customerActivate('Y','<?php echo $user['id']; ?>');">Activate</a></li>
                                        <?php } ?>




                                        <li><a href="javascript:;" class="delete_btn_confirm" data-id="<?php echo (isset($user['brokerage']['id'])) ? $user['brokerage']['id'] : '' ?>" data-table="brokerage">Delete</a></li>


                                        <?php /*

                                        <?php if($this->session->userdata('logged_admin') == 'admin') {?>
                                        <li><a href="<?php echo base_url().'webmanager/customers/accessportal/'.$user['id']?>" target="_blank">Access Portal</a></li>
                                        <?php } ?>

                                        <?php if($user['enabled'] == 'Y') {?>
                                        <li><a href="javascript:;" onclick="customerActivate('N','<?php echo $user['id']; ?>');">Deactivate</a></li>
                                        <?php } else {?>
                                        <li><a href="javascript:;" onclick="customerActivate('Y','<?php echo $user['id']; ?>');">Activate</a></li>
                                        <?php } ?>


                                        <li class="<?php echo ($white == 'Y') ? '' : 'hidden' ?>"><a href="javascript:;" class="broker-white-link" data-id="<?php echo $user['id'] ?>" data-arr='<?php echo json_encode($user) ?>'>Email Unique Link</a></li>

                                        <li><a href="<?php echo base_url().'webmanager/customers/manage/brokers/'.$user['id'].'/message'?>">Message Broker</a></li>

                                        <li><a href="javascript:;" onclick="customerDetails('<?php echo $user['id']; ?>');">Details</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:;" class="delete_btn" data-id="<?php echo $user['id']?>" data-table="customers">Delete Permanently</a></li>

                                        */ ?>




                                      </ul>
                                    </div>


                              </td>
                            </tr>

                        <?php
                        $countt++;

							}
						?>



                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';
						}?>

                    <div class="panel-body hidden">

                    </div>

                </div>

            </div>


        </div><!--.row-->



</div>





<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $singular_title; ?> Information</h4>
      </div>
      <div class="modal-body">
       	<div class="customer_info">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="addAgencyModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">

            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>




            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

                            <p class="pull-right edit_actions" style="padding-top: 20px;">
                              <a href="#" class="user_edit_btn btn pull-right" data-modal="#addAgencyModal" style="margin-left: 5px;"><i class="fa fa-edit"></i> Edit</a>
                              <a href="#" class="btn text-danger pull-right delete_btn_confirm" data-id="" data-table="brokerage"><i class="fa fa-trash-o"></i> Delete</a>
                            </p>
                            <h1><?php echo $singular_title; ?></h1>
                            <hr />




                            <div class="modal-container-scrollx">


                              <div class="brokerage_details text-left">

                                  <div class="detailsss">


                                  </div>



                                  <div class="checkbb <?php echo ($unique_link == 'N') ? 'hidden' : ''; ?>">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p><strong>Enable unique quick quote link?</strong></p>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">

                                                <input type="checkbox" class="unique_link_edit" name="enable_whitelabel" data-on-text="Yes" data-off-text="No" data-handle-width="70" value="Y">

                                            </div>
                                        </div>
                                    </div>

                                  </div>

                              </div>

                              <form id="brokerage_form" novalidate="novalidate" class="hidden text-left">



                                  <input type="hidden" name="id" />
                                  <input type="hidden" name="broker_id" />

                                  <div class="row">
                                      <div class="col-sm-12">
                                          <p><strong>Broker Details</strong></p>
                                      </div>
                                      <div class="col-sm-12">
                                          <div class="form-group">
                                              <label>Brokerage name</label>
                                              <input type="text" placeholder="" class="form-control" name="company_name" required="required"/>
                                          </div>
                                      </div>
                                      <div class="col-sm-12">
                                          <div class="form-group">
                                              <label>Level/suite</label>
                                              <input type="text" placeholder="" class="form-control" name="suite_number"/>
                                          </div>
                                      </div>

                                      <div class="col-sm-12">
                                          <div class="form-group">
                                              <label>Street address</label>
                                              <input type="text" class="form-control user_geolocation" placeholder="" name="address" />
                                              <input type="hidden" name="lat" required="required"/>
                                              <input type="hidden" name="lng" />
                                          </div>
                                      </div>
                                      <div class="col-sm-4 hidden">
                                          <div class="form-group">
                                              <label>Company Domain</label>
                                              <input type="text" class="form-control" name="domain"/>
                                          </div>
                                      </div>

                                  </div>

                                  <div class="row row-prime-contact">

                                      <div class="col-sm-12">
                                          <p><strong>Broker Super Administrator</strong></p>
                                      </div>

                                      <div class="col-sm-6">
                                          <div class="form-group">
                                              <label>First name</label>
                                              <input type="text" class="form-control" name="first" placeholder="" required="required"/>
                                          </div>
                                      </div>
                                      <div class="col-sm-6">
                                          <div class="form-group">
                                              <label>Last name</label>
                                              <input type="text" class="form-control" name="last" placeholder="" required="required"/>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="row row-prime-contact">

                                      <div class="col-sm-12">
                                          <div class="form-group">
                                              <label>Email</label>
                                              <input type="email" placeholder="Email" class="form-control" name="email" required="required"/>
                                          </div>
                                      </div>

                                  </div>






                                  <div class="row">

                                      <div class="col-sm-6">
                                          <div class="form-group">
                                              <label class="control-label">Country calling code</label><br>
                                              <a href="#countryModal" class="country_btn btn btn-default btn-block" data-toggle="modal" data-controls-modal="#countryModal">Select Country Code..</a>
                                              <input type="hidden" name="country_code" required="required">
                                          </div>
                                      </div>

                                      <div class="col-sm-6">
                                          <div class="form-group mobile_input">

                                                <label>Mobile number</label>
                                                <span class="mobile-icon">
                                                  <i class="fa fa-mobile-phone fa-2x"></i>
                                                </span>
                                                <input type="text" class="form-control mobile_number" placeholder="" name="mobile" autocomplete="nope" value="04">

                                              <input type="hidden" name="country_short" value=""/>
                                          </div>
                                      </div>

                                  </div><!--phone number-->

                                  <div class="row <?php echo ($unique_link == 'N') ? 'hidden' : ''; ?>">
                                      <div class="col-sm-12">
                                          <p><strong>Enable unique quick quote link?</strong></p>
                                      </div>
                                      <div class="col-sm-12">
                                          <div class="form-group">

                                              <input type="checkbox" name="enable_whitelabel" data-on-text="Yes" data-off-text="No" data-handle-width="70" <?php echo ($unique_link == 'N') ? '' : 'checked="checked"'; ?> value="Y">

                                          </div>
                                      </div>
                                  </div>

                                  <div class="row">
                                      <div class="col-sm-12">
                                          <div class="form-group">
                                              <p><strong>Assign products</strong></p>

                                              <div class="row">
                                                  <?php foreach($q_products as $r=>$value) {
                                                      ?>
                                                  <div class="col-md-12">
                                                      <div class="form-groupx" style="margin-bottom: 10px;">
                                                          <div class="btn-group btn-group-justified" data-toggle="buttons">
                                                            <label class="btn btn-default" style="padding: 10px; text-align: left">
                                                              <span class="pull-right btn-xs">Add</span>
                                                              <input type="checkbox" name="product_id[]" value="<?php echo $value['id'] ?>" required="required">
                                                              <?php echo $value['product_name'] ?>
                                                          </div>
                                                        </label>
                                                      </div>
                                                  </div>
                                                  <?php } ?>



                                              </div>



                                          </div>
                                      </div>
                                  </div>








                                  <?php /*?><div class="form-group">
                                      <p class="text-center"><small>or</small></p>
                                      <label>Select Existing Broker</label>
                                      <select class="form-control" name="broker_id">
                                          <option value="">Select</option>
                                          <?php
                                              if(count($the_brokers) > 0){

                                                  foreach($the_brokers as $r=>$value){
                                                      echo '<option value="'.$value['id'].'">'.$value['first_name'].' '.$value['last_name'].'</option>';
                                                  }
                                              }
                                          ?>
                                      </select>
                                  </div><?php */?>



                                  <div class="form-group text-right">
                                      <button class="btn btn-default btn-cancel btn-grey" data-dismiss="modal">Cancel</button>
                                      <button class="btn btn-primary" type="submit">Submit</button>
                                  </div>

                              </form>


                            </div>




                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->



    <!-- Modal -->
    <div class="modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="table_search clearfix">
                <input type="text" class="form-control input_table_search_modal" placeholder="search for..." />
                <i class="fa fa-search"></i>
            </div>



          </div>
          <div class="modal-body" style="height: 400px; overflow-y: scroll;">


              <table class="table table-striped table-hover table-datatable-modal">
                <thead>
                  <tr>
                    <th>Select Country</th>
                  </tr>
                </thead>

                <tbody>
                <?php
                    foreach($countries as $r=>$mc){ ?>
                      <tr class="select_country_tbl" data-id="<?php echo $mc['country_id']?>" data-calling="<?php echo $mc['calling_code']; ?>" data-short="<?php echo $mc['short_name'].'(+'.$mc['calling_code'].')'; ?>">
                        <td><?php echo $mc['short_name'].'(+'.$mc['calling_code'].')'; ?></td>
                      </tr>

                <?php } ?>


                </tbody>
              </table>


          </div>

        </div>
      </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4>Type 'DELETE' to confirm</h4>
          </div>
          <div class="modal-body">

            <form id="delete_form">
                <div class="form-group">
                    <input type="text" name="delete" class="form-control" />
                    <input type="hidden" name="delete2" id="delete2" class="form-control" value="DELETE"/>
                    <input type="hidden" name="id" class="form-control" value="DELETE"/>
                    <input type="hidden" name="type" value="not"/>

                </div>
                <button type="submit" class="hidden delete_form_btn btn btn-primary">Confirm</button>
            </form>
            <div class="form-group text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 5px">Cancel</button>
                <button type="button" class="btn btn-primary delete_form_btn2" onclick="$('.delete_form_btn').click()">Confirm</button>
            </div>


          </div>

        </div>
      </div>
    </div>
