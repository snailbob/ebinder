<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">

                <div class="panel panel-default clearfix">


                	<div class="panel-heading">

                        <a href="#addAgencyModal" class="btn btn-primary pull-right open_modal_form_btn" data-controls-modal="#addAgencyModal"><i class="fa fa-plus"></i> <?php echo $singular_title; ?></a>

                		<div  class="panel-title">
                            <h4>Manage <?php echo $title; ?></h4>
						</div>
                    </div>

                        <?php if(count($users) > 0) { ?>

                        <div class="table_search">
                            <input type="text" class="form-control input_table_search" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>




						<div class="table-responsivex">

                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <th style="width: 15%">Date Added</th>
                              <th style="width: 15%">Name</th>
                              <th style="width: 10%">Domain</th>
                              <th style="width: 50%">Address</th>
                              <th style="width: 5%">Status</th>
                              <th style="width: 5%"> </th>
                            </tr>
                          </thead>
                          <tbody>

                        <?php
                        $countt = 1;
							foreach($users as $u=>$value){
                $value['agency_details'] =  (@unserialize($value['agency_details'])) ? unserialize($value['agency_details']) : array();

						?>

                            <tr>

                              <td class="hidden"><?php echo $countt ?></td>
                              <td><?php echo date_format(date_create($value['date_added']), 'd-m-Y') ?></td>
                              <td><?php echo $value['name'] ?></td>
                              <td>
              							  	<?php echo $value['domain'] ?>
                              </td>

                              <td><?php $addtxt = $value['address']; echo $addtxt; ?></td>
                              <td>
                							  	<?php
                                  if($value['enabled'] == 'Y' && !empty($value['password'])){
                                    echo '<span class="text-success">Activated</span>';
                                  }
                                  else if($value['enabled'] == 'N' && !empty($value['password'])){
                                    echo '<span class="text-red">Deactivated</span>';
                                  }

                								?>
                              </td>

                              <td>

                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                        <?php if($value['enabled'] == 'Y') {?>
                                        <li><a href="javascript:;" onclick="userActivate('N','<?php echo $value['id']; ?>');">Deactivate</a></li>
                                        <?php } else {?>
                                        <li><a href="javascript:;" onclick="userActivate('Y','<?php echo $value['id']; ?>');">Activate</a></li>
                                        <?php } ?>


                                        <li><a href="<?php echo base_url().'webmanager/settings/logo/'.$value['id']?>">Upload Logo</a></li>
                                        <li><a href="javascript:;" class="agency-update" data-id="<?php echo $value['id'] ?>" data-arr='<?php echo json_encode($value) ?>'>Update</a></li>
                                        <li><a href="<?php echo base_url().'webmanager/customers/agencies/'.$value['id']?>">View Underwriter(s)</a></li>
                                        <li><a href="javascript:;" class="delete_btn_confirm" data-id="<?php echo $value['id']?>" data-table="agency">Delete</a></li>
                                      </ul>
                                    </div>


                              </td>
                            </tr>

                        <?php
                        $countt++;

							}
						?>



                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';
						}?>

                    <div class="panel-body hidden">

                    </div>

                </div>

            </div>


        </div><!--.row-->



</div>





<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $singular_title; ?> Information</h4>
      </div>
      <div class="modal-body">
       	<div class="customer_info">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="addAgencyModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

                            <h1><?php echo $singular_title; ?></h1>
                			<hr />


                            <form id="agencies_form" novalidate="novalidate" class="text-left">
                            	<input type="hidden" name="id" />
                            	<input type="hidden" name="utype" />

                            	<div class="row">
                                	<div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Underwriting Agency Name</label>
                                            <input type="text" class="form-control" placeholder="Underwriting Agency Name" name="name" required="required"/>
                                        </div>
                                    </div>
                                	<div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Domain</label>
                                            <input type="text" class="form-control" placeholder="Domain" name="domain" required="required"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Floor/suite number</label>
                                    <input type="text" class="form-control" placeholder="Floor/suite number" name="background" />
                                </div>

                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control geolocation" name="address" required="required"/>
                                    <input type="hidden" name="lat" />
                                    <input type="hidden" name="lng" />
                                </div>


                            	<!-- <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label>Address 1</label>
                                                                            <input type="text" class="form-control" name="address1" required="required"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label>Address 2</label>
                                                                            <input type="text" class="form-control" name="address2"/>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                <div class="row">
                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label>City</label>
                                                                            <input type="text" class="form-control" name="city" required="required"/>
                                                                        </div>
                                                                    </div>


                                                                    <div class="col-sm-6">
                                                                        <div class="form-group">
                                                                            <label>State</label>
                                                                            <select class="form-control" name="state" required="required">
                                                                                <option value="VIC">VIC</option>
                                                                                <option value="NSW">NSW</option>
                                                                                <option value="ACT">ACT</option>
                                                                                <option value="QLD">QLD</option>
                                                                                <option value="SA">SA</option>
                                                                                <option value="WA">WA</option>
                                                                                <option value="NT">NT</option>
                                                                                <option value="TAS">TAS</option>

                                                                            </select>
                                                                        </div>
                                                                    </div>


                                                                </div> -->


                            	<div class="row">
                                	<div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Product</label>

                                            <div class="row">
												<?php foreach($q_products as $r=>$value) {?>
                                                <div class="col-md-12">
                                                    <div class="form-groupx" style="padding-bottom: 15px">
                                                        <div class="btn-group btn-group-justified" data-toggle="buttons">
                                                          <label class="btn btn-default" style="padding: 10px; text-align: left">
                                                            <span class="pull-right btn-xs">Add</span>
                                                            <input type="checkbox" name="product_id[]" value="<?php echo $value['id'] ?>" required="required">
                                                            <?php /*?><i class="fa <?php echo $report_icons[$r] ?> fa-2x"></i><br /><?php */?>
                                                            <?php echo $value['product_name'] ?>
                                                          </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php } ?>



                                            </div>


                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <p><strong>Enable Unique Link feature to this agency?</strong></p>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <input type="checkbox" name="unique_link" class="checkbox-switch" data-on-text="Yes" data-off-text="No" data-handle-width="70" checked="checked" value="Y">

                                                    </div>
                                                </div>
                                            </div>



                                        </div>
                                    </div>
                                </div>

                                <div class="first-under show_addcontent">

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <p><strong>Primary Contact</strong></p>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input type="text" placeholder="First Name" class="form-control" name="first"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input type="text" placeholder="Last Name" class="form-control" name="last"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="email" placeholder="Email" class="form-control" name="email"/>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Phone</label>
                                                <input type="text" placeholder="Phone" class="form-control" name="phone"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group text-right">
                                    <button class="btn btn-default btn-cancel btn-grey" data-dismiss="modal">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>


                            </form>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->


    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4>Type 'DELETE' to confirm</h4>
          </div>
          <div class="modal-body">

            <form id="delete_form">
                <div class="form-group">
                    <input type="text" name="delete" class="form-control" />
                    <input type="hidden" name="delete2" id="delete2" class="form-control" value="DELETE"/>
                    <input type="hidden" name="id" class="form-control" value="DELETE"/>
                    <input type="hidden" name="type" value="table"/>
                </div>
                <button type="submit" class="hidden delete_form_btn btn btn-primary">Confirm</button>

            </form>
            <div class="form-group text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right: 5px">Cancel</button>
                <button type="button" class="btn btn-primary delete_form_btn2" onclick="$('.delete_form_btn').click()">Confirm</button>
            </div>

          </div>

        </div>
      </div>
    </div>
