<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                 <?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">
            


                <div class="panel panel-default">

                
                	<div class="panel-heading">
            			<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>
                    
                    

                    <div class="panel-body">
                        
                        
                        <div>
                        
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                            <li class="active"><a href="#terms" aria-controls="terms" role="tab" data-toggle="tab">Terms of Use</a></li>
                            <li><a href="#privacy" aria-controls="privacy" role="tab" data-toggle="tab">Privacy Policy</a></li>
                          </ul>
                        
                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="terms">
                            	<p>&nbsp;</p>
                                <div class="summernote"><?php echo $admin_info[0]['terms']; ?></div>
                                <p style="margin-top: 15px;">
                                    <a href="#" class="btn btn-warning save_contents_btn" data-field="terms" data-title="Terms of Use">Save Changes</a>
                                </p>

                            </div><!--terms-->
                            <div role="tabpanel" class="tab-pane fade" id="privacy">
                            	<p>&nbsp;</p>
                                <div class="summernote"><?php echo $admin_info[0]['privacy']; ?></div>
                                <p style="margin-top: 15px;">
                                    <a href="#" class="btn btn-warning save_contents_btn" data-field="privacy" data-title="Privacy Policy">Save Changes</a>
                                </p>
                            
                            </div><!--terms-->
                          </div>
                        
                        </div>

                    
                    </div><!--panelbody-->                    
                </div><!--panel-->

                   
            </div>
        </div><!--.row-->


    
</div>


