<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage Certificate</h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage Certificate</li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                 <?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">
            


                <div class="panel panel-default">

                
                	<div class="panel-heading">
            			<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>
                    

                    <div class="panel-body">


                        <div class="summernote">
							<?php
							$yum = array(
								'[[logolink]]',
								'[[signlink]]'
							);
							$subb = array(
								base_url().'assets/frontpage/corporate/images/crisisflo-logo-medium.png', 
								base_url().'assets/img/cert/cert-signiture.jpg'
							);
							
							echo str_replace($yum, $subb, $admin_info[0]['certificate_template']);?>
                        </div>
                    	<p style="margin-top: 15px;">
                            <a href="<?php echo base_url().'landing/certificate'?>" class="btn btn-primary pull-right" target="_blank">View Template as PDF</a>
                            <a href="#" class="btn btn-warning save_certificate_btn">Save Changes</a>
                        </p>

                    
                    </div><!--panelbody-->                    
                </div><!--panel-->

                   
            </div>
        </div><!--.row-->


    
</div>


