<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

					<?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">
            	<p class="text-right hidden">
                	<a href="<?php echo base_url().'webmanager/contents/alternate'?>" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $title; ?></a>
                </p>

                <div class="panel panel-default">

                    <div class="panel-heading">
						<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>


                        <?php if(count($admin_info) > 0) { ?>


						<div class="table-responsivex">

                        <table class="table table-striped table-hover table-datatablex">
                          <thead>
                            <tr>
                              <th>Title</th>
                              <th>Content</th>
                              <th>Image</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>

                        <?php
							foreach($admin_info as $u=>$user){
						?>

                            <tr>

                              <td><?php echo $user['title'] ?></td>
                              <td><?php echo $user['content'] ?></td>

                              <td>
							  	<?php
									if($user['image'] != ''){
										echo '<img src="'.base_url().'uploads/avatars/'.$user['image'].'" style="width: 150px;">';
									}
								?>
                              </td>

                              <td>

                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">

                                        <li><a href="<?php echo base_url().'webmanager/contents/threepart/'.$user['id'].$params?>">Update</a></li>
                                        <?php /*?><li class="divider"></li>
                                        <li><a href="javascript:;" onclick="deleteAlternate('<?php echo $user['id']; ?>');">Delete Permanently</a></li><?php */?>
                                      </ul>
                                    </div>


                              </td>
                            </tr>

                        <?php

							}
						?>



                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">No <?php echo $title; ?></p></div>';
						}?>

                    <div class="panel-body hidden">

                    </div>

                </div>

            </div>


        </div><!--.row-->



</div>
