<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>
                        <li><a href="<?php echo base_url()?>webmanager/products" class="preloadThis">Products</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>
                  <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('success');   ?>
                  </div>
                <?php } if($this->session->flashdata('error')!=""){ ?>
                  <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <?php echo $this->session->flashdata('error');   ?>
                  </div>
                <?php } ?>

            </div>
            <div class="col-lg-12">


                <div class="panel panel-default">


                	<div class="panel-heading">

						<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>



                        <?php if(count($claims) > 0) { ?>

                        <div class="table_search form-group">
                            <input type="text" class="form-control input_table_search" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>


						<div class="table-responsivex">

                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <th>ID</th>
                              <th>Details</th>
                              <th>Customer</th>
                              <th>Assigned</th>
                              <th>Status</th>
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>

                        <?php
							$count = 1;
							foreach($claims as $u=>$value){

						?>

                            <tr>

                              <td class="hidden"><?php echo $count; ?></td>

                              <td><?php echo $this->common->format_id($value['id'], 'C') ?></td>

                              <td><?php echo $value['details'] ?></td>

                              <td><?php echo $this->common->customer_name($value['customer_id']) ?></td>

                              <td><?php echo ($value['assigned_user'] == '0') ? 'Not Assigned' : $this->common->customer_name($value['assigned_user']) ?></td>

                              <td><?php echo $this->common->claim_status($value['status']) ?></td>

                              <td>

                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">

                                        <?php if($this->session->userdata('first_underwriter') == 'Y' || $this->session->userdata('id') == $value['assigned_user']){ ?>
                                          <li>
                                            <a href="<?php echo base_url().'webmanager/claims/eachclaim/'.$value['id']?>" data-id='<?php echo $value['id'];?>'>Claim Details</a>
                                          </li>
                                        <?php } ?>

                                        <li>
                                            <a href="javascript:;" class="show-biz-response" data-id='<?php echo $value['policy_id'];?>'>Quote details</a>
                                        </li>
                                        <li>
                                            <a href="javascript:;" class="assign_claim_btn" data-modal="#assignUserModal" data-id='<?php echo $value['id'];?>'>Assign</a>
                                        </li>

                                        <li><a href="javascript:;" class="delete_btn" data-id="<?php echo $value['id']?>" data-table="claims">Delete</a></li>

                                      </ul>
                                    </div>


                              </td>
                            </tr>

                        <?php
								$count++;
							}
						?>



                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';
						}?>





                </div>

            </div>


        </div><!--.row-->



</div>



<!-- Portfolio Modals -->
<div class="portfolio-modal modal fade" id="formModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="modal-body">

                        <h1>Quote details</h1>
                        <hr />


                        <div class="the_referral_response text-left">
                            theview

                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div>
</div><!--portfolio-modal-->



<!-- Portfolio Modals -->
<div class="portfolio-modal modal fade" id="assignUserModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="modal-body">

                        <h1>Assign</h1>

                        <hr />

                				<form novalidate="novalidate" class="text-left" id="claim_assign_form">
                          <input type="hidden" name="id" value="" />
                          <div class="form-group">
                              <select class="form-control selectpicker" data-live-search="true" name="customer_id" required="required">
                                  <option value="" data-name="" data-inputs='[]'>Select</option>
                                  <?php
                                      if(count($first_under) > 0){
                                          foreach($first_under as $r=>$value){
                                              echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
                                          }
                                      }
                                  ?>
                              </select>
                          </div>


                          <div class="form-group text-right">
                              <a href="#" data-dismiss="modal" class="btn btn-default btn-cancel btn-grey">Cancel</a>
                              <button type="submit" class="btn btn-primary">Submit</button>
                          </div>

                        </form>


                    </div>
                </div>
            </div>
        </div>


    </div>
</div><!--portfolio-modal-->
