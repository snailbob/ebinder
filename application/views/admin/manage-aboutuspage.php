<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                 <?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">
            


                <div class="panel panel-default">

                
                	<div class="panel-heading">
            			<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>
                    
                    

                    <div class="panel-body">
                    	<div class="form-group">
                        	<label>Title</label>
                            <input type="text" value="<?php echo $admin_info[0][$titlefield]; ?>" name="titlefield" class="form-control" />
                            <input type="hidden" id="pagetype" value="<?php echo $pagetype; ?>" data-id="<?php echo $admin_info[0]['id']; ?>"/>
                        </div>
                
                    	<?php if($admin_info[0]['image']) { //$pagetype != 'aboutus' && $pagetype != 'alternatingcontenthead' && $pagetype != 'threepartshead' && $pagetype != 'firsthomesection') {?>
                      
                        <p style="margin-top: 15px;">
                            <a href="#" class="btn btn-default btn-lg btn-block" onclick="$('#myfile').click(); return false;"><i class="fa fa-upload"></i> <i class="fa fa-plus"></i> Image</a>
                        </p>
                      	
                        <p style="margin-top: 15px;" class="text-center<?php if($admin_info[0]['image'] == ''){ echo ' hidden'; } ?>">
                            <img class="img-thumbnail img-page-preview" src="<?php echo base_url().'uploads/avatars/'.$admin_info[0]['image'] ?>" style="height: 60px;" />
                        </p>
                        
                        <?php } ?>
                    	<?php if($admin_info[0][$contentfield]) {?>
                        <div class="summernote"><?php echo $admin_info[0][$contentfield]; ?></div>
                        <?php } ?>
                        
                        <p style="margin-top: 15px;">
                            <a href="#" class="btn btn-warning save_pages_contents_btn" data-type="<?php echo $pagetype ?>" data-id="<?php echo $admin_info[0]['id']; ?>" data-title="<?php echo $title; ?>">Save Changes</a>
                            
                            <?php if($pagetype == 'alternatingcontenthead') {?>
                            <a href="<?php echo base_url().'webmanager/contents/manage_alternates'; ?>" class="btn btn-default">Manage Items</a>
                            <?php } ?>
                            
                            <?php if($pagetype == 'threepartshead') {?>
                            <a href="<?php echo base_url().'webmanager/contents/manage_threeparts'; ?>" class="btn btn-default">Manage Columns</a>
                            <?php } ?>
                        </p>

                    </div><!--panelbody-->                    
                </div><!--panel-->

                   
            </div>
        </div><!--.row-->
        
        <div class="row hidden">
        <form id="myFormAvatar" action="<?php echo base_url().'settings/update_avatar' ?>" method="post" enctype="multipart/form-data">
             <input type="file" size="60" id="myfile" name="myfile" class="myfile-profile" onchange="$('#myFormAvatar').submit();">
             <input type="hidden" id="img_type" name="img_type" value="<?php echo $pagetype; ?>" data-id="<?php echo $admin_info[0]['id']; ?>">
             <input type="hidden" id="pageid" name="pageid" value="<?php echo $admin_info[0]['id']; ?>">
             <input type="hidden" id="avatar_name" name="avatar_name" value="">
             
             <input type="submit" class="hidden" value="Ajax File Upload">
         </form>                                                   
        </div>
    
</div>




<!-- Modal -->
<div class="modal fade" id="cropImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<p class="lead">Crop</p>
        
        <div class="bootstrap-modal-cropper"><img src="<?php echo base_url().'assets/img/avatar/background.png' ?>" class="img-responsive"/></div>
	
        

      </div>
      <div class="modal-footer">
        <a class="btn btn-primary sav_crop">Save changes</a>
      </div>
    </div>
  </div>
</div>