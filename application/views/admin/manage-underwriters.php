<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">

                <div class="panel panel-default">


                	<div class="panel-heading">
                        <a href="#addAgencyModal" class="btn btn-primary pull-right new_uw_user"><i class="fa fa-plus"></i> <?php echo $singular_title; ?></a>

                        <a href="<?php echo base_url().'webmanager/customers/agencies'?>" class="btn btn-default pull-right <?php echo ($this->uri->segment(3) != 'agencies') ? 'hidden' :''?>">&larr; Back</a>


                    	<div  class="panel-title">
                            <h4>Manage <?php echo $title; ?></h4>
						</div>
                    </div>

                        <?php if(count($users) > 0) { ?>

                        <div class="table_search">
                            <input type="text" class="form-control input_table_search" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>



						<div class="table-responsivex">

                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <th>Date Added</th>
                              <th>Name</th>
                              <th>Status</th>
                              <th>User role</th>
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>

                        <?php
                        $countt = 1;

							foreach($users as $u=>$user){
								$user['access_rights'] = (@unserialize($user['access_rights'])) ? unserialize($user['access_rights']) : array();
						?>

                            <tr class="<?php echo ($this->session->flashdata('success') != '' && $countt == 1 && strpos($this->session->flashdata('success'), 'added') !== false) ? 'flash_animation' : '' ?>">
                              <td class="hidden"><?php echo $countt; ?></td>

                              <td><?php echo date_format(date_create($user['date_added']), 'd-m-Y') ?></td>
                              <td><?php echo $this->common->customer_name($user['id']) ?></td>
                              <td>
                							  	<?php
                                  if($user['enabled'] == 'Y' && !empty($user['password'])){
                                    echo '<span class="text-success">Activated</span>';
                                  }
                                  else if($user['enabled'] == 'N' && !empty($user['password'])){
                                    echo '<span class="text-red">Deactivated</span>';
                                  }
                									else if($user['status'] == 'Y'){
                										echo '<span class="text-success">Verified</span>';
                									}

                									else{
                										echo '<span class="text-red">Not Verified</span>';
                									}
                								?>
                              </td>
                              <td>
                							  	<?php
                                  $sss = 'Claims Handler';

                									if($user['first_underwriter'] == 'Y' && count($user['access_rights']) > 4){
                										$sss = 'Super Administrator';
                									}

                									else if($user['first_broker'] == 'Y'){
                                    $sss = 'Broker';
                									}
                                  echo $sss;
                								?>
                              </td>

                              <td>

                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">

                                        <?php if($this->session->userdata('logged_admin') == 'admin' || $this->session->userdata('first_underwriter') == 'Y'){ ?>

                      											<?php if($user['enabled'] == 'Y') {?>
                                              <li><a href="javascript:;" onclick="customerActivate('N','<?php echo $user['id']; ?>');">Deactivate</a></li>
                                              <?php } else {?>
                                              <li><a href="javascript:;" onclick="customerActivate('Y','<?php echo $user['id']; ?>');">Activate</a></li>
                                            <?php } ?>

                    										<?php } ?>


                                        <li><a href="javascript:;" class="customer-update" data-id="<?php echo $user['id'] ?>" data-arr='<?php echo json_encode($user) ?>'>Update</a></li>
                                        <!-- <li><a href="javascript:;" onclick="customerDetails('<?php echo $user['id']; ?>');">Details</a></li> -->
                                        <?php if(($this->session->userdata('first_underwriter') == 'Y' || $this->session->userdata('logged_admin') == 'admin')){ ?>


                                            <?php /* if($user['first_broker'] == 'N'){ ?>

                                              <?php if($user['first_underwriter'] == 'N') {?>                                        <li><a href="javascript:;" class="underwriter-first-user" data-to="Y" data-id="<?php echo $user['id'] ?>" data-agency_id="<?php echo $user['agency_id'] ?>" data-info='<?php echo json_encode($user) ?>'>Make First Underwriter</a></li>
                                              <?php } ?>

                                              <?php if($user['first_underwriter'] != 'N') {?>                                        <li><a href="javascript:;" class="underwriter-first-user" data-to="N" data-id="<?php echo $user['id'] ?>" data-agency_id="<?php echo $user['agency_id'] ?>" data-info='<?php echo json_encode($user) ?>'>Unassign as First User</a></li>
                                              <?php } ?>

                                            <?php } */ ?>

                                            <?php if($user['id'] != $this->session->userdata('id')) {?>
                                              <li class="divider"></li>

                                              <li><a href="javascript:;" class="delete_btn" data-id="<?php echo $user['id']?>" data-table="customers">Delete Permanently</a></li>
                                            <?php } ?>

                                        <?php } ?>
                                      </ul>
                                    </div>


                              </td>
                            </tr>

                        <?php
                  $countt++;
							}
						?>



                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';
						}?>

                    <div class="panel-body hidden">

                    </div>

                </div>

            </div>


        </div><!--.row-->



</div>





<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $singular_title; ?> Information</h4>
      </div>
      <div class="modal-body">
       	<div class="customer_info">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="addAgencyModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

                            <h1><?php echo $singular_title; ?></h1>
                			<hr />


                            <form id="customer_form" class="text-left">
                            	<input type="hidden" name="id" />
                            	<input type="hidden" name="studio_id" value="<?php echo $this->uri->segment(5)?>" />
                            	<input type="hidden" name="is_agency" value="yes" />

                            	<div class="row">
                                	<div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">First Name</label>
                                            <input type="text" placeholder="First Name" class="form-control" name="first" />
                                        </div>
                                    </div>
                                	<div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="">Last Name</label>
                                            <input type="text" placeholder="Last Name" class="form-control" name="last" />
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="">Email</label>
                                    <input type="email" placeholder="Email" class="form-control" name="email" />
                                </div>

                                <div class="form-group hidden" <?php if($this->uri->segment(4) =='customers') { echo 'hidden'; }?>>
                                    <label>Brokerage Name</label>
                                    <select class="form-control" name="brokerage_id">
                                    	<option value="">Select</option>
                                    	<?php foreach($brokerage as $r=>$value){

                    												echo '<option value="'.$value['id'].'"';

                    													if($this->session->userdata('logged_admin_brokerage') == $value['id']){
                    														echo ' selected="selected"';
                    													}
                    												echo '>'.$value['company_name'].'</option>';
                    										} ?>

                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="">Position</label>
                                    <input type="text" placeholder="Position" class="form-control" name="position" />
                                </div>


                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Country Calling Code</label><br>
                                            <a href="#countryModal" class="country_btn btn btn-default btn-block" data-toggle="modal" data-controls-modal="#countryModal">Select Country Code..</a>
                                            <input type="text" class="form-control hidden" name="country_code" required="required">
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group mobile_input">
                                              <label>Mobile number</label>
                                              <span class="mobile-icon">
                                                  <i class="fa fa-mobile-phone fa-2x"></i>
                                              </span>
                                              <input type="text" class="form-control mobile_number" placeholder="Mobile number" name="mobile" autocomplete="nope" value="" required>

                                            <input type="hidden" name="country_short" value=""/>
                                        </div>
                                    </div>

                                </div><!--phone number-->





                                <div class="form-group hidden">
                                    <label><?php echo $singular_title; ?> Type</label><br />
									<label>
                                    	<input type="hidden" name="ctype" value="N"/>

                                    </label>
                                </div>

                                <div class="form-group">
                                    <label>User Type</label>

                                    <div class="row">

                                      <?php
                                       $report_header = array(
                                         'Super Administrator',
                                         'Claims handler'
                                       );
                                       foreach($report_header as $r=>$value) {?>
                                        <div class="col-md-6">
                                        	<div class="form-groupx" style="margin-bottom: 15px;">
                                                <div class="btn-group btn-group-justified" data-toggle="buttons">
                                                  <label class="btn btn-default" style="padding: 30px 0; border: dashed 1px #ccc; text-align: center">
                                                    <input type="radio" name="user_role" value="<?php echo $value ?>" required="required">
                              											<?php echo $value ?>
                                                  </label>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>



                                <div class="form-group hidden">
                                    <label><strong>Access rights</strong></label>
                                  	<div class="row row-sm">

                    										<?php foreach($access_rights as $r=>$value) {?>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="btn-group btn-group-justified" data-toggle="buttons">
                                                  <label class="btn btn-primary active" style="padding: 10px; text-align: left">
                                                    <span class="pull-right btn-xs"><i class="fa fa-trash"></i></span>

                                                    <input type="checkbox" class="access_rights" name="access_rights[]" checked="checked" value="<?php echo $value ?>">
                                                    <?php echo $value ?>
                                                  </label>
                                                </div>
                                            </div>
                                        </div>
                                        <?php } ?>


                                    </div>
                                </div>


                                <div class="form-group text-right">
                                    <button class="btn btn-default btn-cancel btn-grey" data-dismiss="modal">Cancel</button>
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>



                            </form>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->



    <!-- Modal -->
    <div class="modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="table_search clearfix">
                <input type="text" class="form-control input_table_search_modal" placeholder="search for..." />
                <i class="fa fa-search"></i>
            </div>



          </div>
          <div class="modal-body" style="height: 400px; overflow-y: scroll;">


              <table class="table table-striped table-hover table-datatable-modal">
                <thead>
                  <tr>
                    <th>Select Country</th>
                  </tr>
                </thead>

                <tbody>
                <?php
                    foreach($countries as $r=>$mc){ ?>
                      <tr class="select_country_tbl" data-id="<?php echo $mc['country_id']?>" data-calling="<?php echo $mc['calling_code']; ?>" data-short="<?php echo $mc['short_name'].'(+'.$mc['calling_code'].')'; ?>">
                        <td><?php echo $mc['short_name'].'(+'.$mc['calling_code'].')'; ?></td>
                      </tr>

                <?php } ?>


                </tbody>
              </table>


          </div>

        </div>
      </div>
    </div>
