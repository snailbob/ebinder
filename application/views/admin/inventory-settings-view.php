<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Inventory Settings</h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Inventory Settings</li>

                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                 <?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error'); ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">




                <div class="panel panel-default">

                    <div class="panel-heading">
                        <div class="panel-title">
                            <h4>Inventory Settings <i class="fa fa-check-circle text-success hidden"></i></h4>
                        </div>
                    </div>
                       
                   
                    
                    <div class="panel-body">
                    
                        <form id="inventory_form">
                        	<div class="form-group">

                                
                                <div class="btn-group" data-toggle="buttons">
                                  <label class="btn btn-success <?php if($admin_settings[0]['inventory_setting'] == 'Y') { echo 'active';} ?>">
                                    <input type="radio" name="options" id="option1" autocomplete="off" <?php if($admin_settings[0]['inventory_setting'] == 'Y') { echo 'checked="checked"';} ?> value="Y"> Activate
                                  </label>
                                  <label class="btn btn-success <?php if($admin_settings[0]['inventory_setting'] == 'N') { echo 'active';} ?>">
                                    <input type="radio" name="options" id="option2" autocomplete="off" <?php if($admin_settings[0]['inventory_setting'] == 'N') { echo 'checked="checked"';} ?> value="N"> Deactivate
                                  </label>
                                </div>

                            </div>
                        </form>
            
                    </div>
              
              
                  
                   
                        
                        
                        
               
               
                </div>


            </div><!--.col-lg-12-->


        </div><!--.row-->


    
</div>

