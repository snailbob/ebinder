<?php
	$user_session = $this->session->all_userdata();
	$user_session['sidenav_settings'] = (isset($user_session['sidenav_settings'])) ? $user_session['sidenav_settings'] : 'expanded';
	$domain = (isset($user_session['domain'])) ? $user_session['domain'] : '';
	if($this->session->userdata('logged_admin') == ''){
		redirect('#login');
	}

	else{

		if($this->common->base_url() == 'http://localhost/nguyen/ml.ebinder.com.au/' || $domain == ''){
			if(($this->common->the_domain() != $domain) && ($this->common->the_domain() == '')){
				// redirect($this->common->base_url().'webmanager/dashboard');

			}
		}
		else{
			if($this->uri->segment(2) == 'social_login'){

			}
			else if(($this->common->the_domain() != $domain)){
				$url = 'https://'.$user_session['domain'].'.ebinder.com.au/webmanager/dashboard';
				redirect($url);
			}
		}
	}


	$access_rights = $this->session->userdata('access_rights');
	$first_underwriter = $this->session->userdata('first_underwriter');
	$access_rights = (!empty($access_rights)) ? $access_rights : $this->common->access_rights();
	$access_rights = ($first_underwriter == 'Y') ? $this->common->access_rights_all() : $access_rights;

	if($this->session->userdata('first_broker') == 'Y'){
		$access_rights = array('Manage Claims');
	}

	$unassigned_claims = ($first_underwriter == 'Y') ? $this->master->getRecordCount('claims', array('agency_id'=>$user_session['agency_id'], 'assigned_user'=>'')) : 0;
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">


    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="twitter:widgets:csp" content="on">



    <?php
      $topheadtitle = $this->master->getRecords('admin_contents', array('type'=>'topheadtitle'));

    ?>
    <title><?php echo $topheadtitle[0]['title']?></title>

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/css/summernote.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/css/summernote-bs3.css" type="text/css">


<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bootstrap-switch/css/bootstrap3/bootstrap-switch.css" type="text/css">



<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css">


<link href="<?php echo base_url()?>assets/frontpage/corporate/css/bootstrap.css" rel="stylesheet">

<!-- Plugin CSS -->
<?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css" type="text/css"><?php */?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/dataTables.bootstrap.css" type="text/css">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/datatables.css" type="text/css">

<!-- Plugin CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/dist/cropper.min.css" type="text/css">
<?php /*?><link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/crop-avatar.css" type="text/css"><?php */?>


<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/gridstack/gridstack.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datetimepicker/css/bootstrap-datetimepicker.min.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/bs-switch/css/bootstrap3/bootstrap-switch.min.css" type="text/css">

<!-- Custom CSS -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/silviomoreto-bootstrap-select/css/bootstrap-select.min.css" type="text/css">


<link href="<?php echo base_url()?>assets/frontpage/corporate/css/main.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" type="text/css">


<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,500,300,100' rel='stylesheet' type='text/css'>

<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700' rel='stylesheet' type='text/css'>

<link href="<?php echo base_url()?>assets/frontpage/corporate/css/animate.min.css" rel="stylesheet">
<link rel="shortcut icon" href="<?php echo base_url()?>assets/frontpage/images/favicon.ico" type="image/x-icon" />
<link type="text/css" rel="stylesheet" media="all" href="<?php echo base_url()?>assets/frontpage/corporate/css/jquery.mmenu.css" />




<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/admin-style.css" type="text/css">

<!-- <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-modal-stack/css/bootstrap-modal-bs3patch.css" rel="stylesheet" />
<link href="<?php echo base_url(); ?>assets/plugins/bootstrap-modal-stack/css/bootstrap-modal.css" rel="stylesheet" /> -->


<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/js/libs/head.min.js"></script>

<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.0/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/corporate/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url()?>assets/frontpage/us/variablesCorporate"></script>
<script>
	var base_url = '<?php echo base_url() ?>';
	var uri_1 = '<?php echo $this->uri->segment(1) ?>';
	var uri_2 = '<?php echo $this->uri->segment(2) ?>';
	var uri_3 = '<?php echo $this->uri->segment(3) ?>';
	var uri_4 = '<?php echo $this->uri->segment(4) ?>';
	var user_name = '<?php echo $this->session->userdata('name') ?>';
	var user_location = '<?php echo $this->session->userdata('location') ?>';
	var user_address = '<?php echo $this->session->userdata('address') ?>';
	var user_type = '<?php echo $this->session->userdata('type') ?>';
	var stripe_id = '<?php echo $this->session->userdata('stripe_id') ?>';
	var logged_admin = '<?php echo $this->session->userdata('logged_admin') ?>';
</script>


<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

<![endif]-->

</head>



 <body id="skrollr-body" class="root home landing" style="background: #efefef">



	<?php
		//vars
		$colors = $this->common->colors();
		$usertype = $this->session->userdata('type');
		$opposite_type = 'agency';
		if($usertype != 'customer'){
			$opposite_type = 'customer';
		}
	?>



    <div id="browser-detection" class="alert alert-warning" style="display:none; position:relative;">
		<button type="button" class="close" data-dismiss="alert">&times;</button>
		Your browser is not supported by Transit Insurance. For a better experience, use one of our <a class='preventDefault' data-toggle='modal' data-target='#modal-browser-options'>supported browsers.</a>
	</div>
	<div id="cookies-detection" class="alert alert-danger" style="display:none; position:relative;">
		*__browser_cookies_disabled*
	</div>
    <nav id="my-menu">
       <ul>
          <li class="hidden"><a href="<?php echo base_url()?>">Home</a></li>
          <?php /*?><li><a href="<?php echo base_url()?>about">About Us</a></li>
          <li><a href="<?php echo base_url()?>contact">Contact</a></li><?php */?>
       </ul>
    </nav>

    <div id="main-container" style="background: #efefef">
    <div class="mobile-spacer visible-xs hidden-sm hidden-md hidden-lg" data-0="margin-top:120px" data-1="margin-top:60px"></div>

      <!-- Static navbar -->
      <div class="navbar navbar-default navbar-static-top" role="navigation">
       	<div class="container-fluid">
            <div class="navbar-header">
              <button type="button" id="open-sliderx" class="text-info pull-right navbar-toggle nav-togglex" style="background: transparent; border: none">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-cog fa-2x"></i>
              </button>
              <div class="navbar-brand">
              	<a href="<?php echo base_url(); if($this->session->userdata('logged_admin') != '') { echo 'webmanager'; }?>"><img id="nav-logo" class="animated fadeIn animated-top" src="<?php echo $this->common->logo($this->session->userdata('id'))?>" style="margin-top: 5px; max-height: 61px"/></a>
                <div class="mobile-menu visible-xs">

					<?php /*?><?php if($this->session->userdata('logged_admin') != '') { ?>
                    <a href="<?php echo base_url()."dashboard/logout"; ?>" id="btn-mini-login" class="btn btn-primary btn-rounded btn-outline">Log Out</a>
                    <?php } ?>	<?php */?>



                </div>
              </div>
            </div>


              <ul class="nav navbar-nav navbar-right navbar-admin hidden-xs">
                <li>
                    <a href="#" class="nav-toggle nav-toggle-admin">
                        <i class="fa fa-cog fa-2x pull-right"></i>

            						<p>

                          <?php if($this->session->userdata('id') == ''){ ?>

                            <img src="<?php echo $this->common->avatar($this->session->userdata('id'), 'customer') ?>" class="img-circle pull-left" style="margin-right: 10px; width: 40px" />

                            <?php } else { ?>

                            <span class="img-hover pull-left">
                              <img class="img-circle img-page-preview" src="<?php echo $this->common->avatar($this->session->userdata('id'), 'customer') ?>" width="40" height="40" />
                              <span class="text-content"><span>Update</span></span>
                            </span>

                            <?php } ?>



                            <strong><?php echo $this->session->userdata('logged_admin')?></strong>
                            <span><?php echo $this->session->userdata('logged_admin_email')?></span>
                        </p>

                    </a>
                </li>


              </ul>

            <div class="navbar-collapse collapse">


                <div class="mobile-menu hidden-xs text-right" style="padding-top: 35px;">
					<?php if($this->session->userdata('id') == '') { ?>
                    <a href="<?php echo base_url()."webmanager"; ?>" id="btn-mini-login" class="hidden btn btn-primary btn-xs btn-rounded btn-outline" >Log In</a>
                    <?php } ?>


                </div>
				<?php if($this->session->userdata('logged_admin') == '') { ?>
                <div class="hidden-xs"><a href="#<?php // echo base_url()."signin"; ?>" class="pull-right btn btn-primary btn-rounded btn-outline login_btn" style="margin-top: -6px;" data-type="Agency">Log In</a></div>
				<?php } ?>



            </div><!--/.nav-collapse -->

						<?php if($this->session->userdata('is_admin')){ ?>
							<p class="text-center" style="margin-top: -60px">
								Logged in as: <strong><?php echo $this->session->userdata('logged_admin')?></strong><br>
								<a href="<?php echo base_url().'webmanager/dashboard/back_to_admin' ?>">Back to admin panel</a>
							</p>
						<?php } ?>

   	 	</div> <!-- /container -->
      </div>





    <div class="container-fluid section-simple section-simple-admin main_container main_container_admin" style="padding-top: 120px">

        <div class="row row-nogutter row-product-tab2 hidden" id="menu-tab2">
            <div class="col-lg-10 col-lg-offset-2 col-md-9 col-md-offset-3 col-sm-8 col-sm-offset-4" style="padding-top: 15px;">
              <!-- <h3>
                &nbsp;
                <button type="button" class="close producttab_btn_close"><span aria-hidden="true">&times;</span></button>

              </h3> -->



<?php

    $admin_info = $this->master->getRecords('admin');
    $user_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];
    $agency_info = $this->master->getRecords('agency', array('id'=>$user_id));

    $whr = array(
      'customer_type'=>'Y',
      'super_admin'=>'Y',
    );
    $whr['agency_id'] = $user_id;
    $underwriters = $this->master->getRecords('customers', $whr);




    $deductibles = $this->master->getRecords('deductibles', array('agency_id'=>$user_id));
    $cargo = $this->master->getRecords('q_business_specific'); //this->master->getRecords('cargo_category');
    $transportations = $this->common->transportations();
    $countries = $this->master->getRecords('country_t');
    $currencies = $this->master->getRecords('api_currencies', array('in_stripe'=>'Y'));


    $cargo_prices = $this->common->agency_prices($user_id,'cargo_prices');
    $cargo_max = $this->common->agency_prices($user_id,'cargo_max');
    $transportation_prices = $this->common->agency_prices($user_id,'transportation_prices');
    $country_prices = $this->common->agency_prices($user_id,'country_prices');
    $country_referral = $this->common->agency_prices($user_id,'country_referral');
    $zone_multiplier = $this->common->agency_prices($user_id,'zone_multiplier');
    $base_currency = ($agency_info[0]['base_currency']) ? $agency_info[0]['base_currency'] : 'AUD';






    $prod_user_id = $this->common->default_cargo(); //admin_info[0]['default_cargo'];
    $prod_agency_info = $this->master->getRecords('agency', array('id'=>$prod_user_id));

    $prod_agency_info_format = array();
    if(!empty($prod_agency_info)){
      $ggg = array();
      foreach($prod_agency_info as $r=>$value){
        $value['agency_details'] = (@unserialize($value['agency_details'])) ? unserialize($value['agency_details']) : array();
        $ggg = $value;
      }
      $prod_agency_info_format = $ggg;
    }


    $prod_prod_id = (isset($prod_agency_info_format['agency_details']['product_id'])) ? $prod_agency_info_format['agency_details']['product_id'] : array();


    $prod_q_products = $this->master->getRecords('q_products');

?>





                      <div class="row gutter-md product_submenu" id="sortable">
                          <div class="col-sm-6 col-lg-4">
                                <a href="#baseRateModal" data-toggle="modal" data-controls-modal="baseRateModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                          <i class="fa fa-bolt fa-2x"></i><br />
                                            <h4 class="panel-title">
																							Base Rate
																						</h4>
																						<?php
																							$agency_logtype = 'base_rate';
																							$agency_log = $this->master->getRecords('agency_log', array('type'=>$agency_logtype, 'agency_id'=>$prod_user_id), '*', array('id'=>'DESC'));
																							$last_date = (!empty($agency_log)) ? date_format(date_create($agency_log[0]['date_added']),'d-m-Y') : 'NA';
																						?>
																						<span class="show_history_btnx" data-type="<?php echo $agency_logtype ?>">
																							<small>Last updated: <?php echo $last_date; ?></small>
																						</span>


                                        </div>
                                    </div>
                                </a>
                            </div>

                          <div class="col-sm-6 col-lg-4">
                                <a href="#minimumPremiumModal" data-toggle="modal" data-controls-modal="minimumPremiumModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                          <i class="fa fa-calculator fa-2x"></i><br />
                                            <h4 class="panel-title">Minimum Premium</h4>
																						<?php
																							$agency_logtype = 'minimum_premium';
																							$agency_log = $this->master->getRecords('agency_log', array('type'=>$agency_logtype, 'agency_id'=>$prod_user_id), '*', array('id'=>'DESC'));
																							$last_date = (!empty($agency_log)) ? date_format(date_create($agency_log[0]['date_added']),'d-m-Y') : 'NA';
																						?>
																						<span class="show_history_btnx" data-type="<?php echo $agency_logtype ?>">
																							<small>Last updated: <?php echo $last_date; ?></small>
																						</span>


                                        </div>
                                    </div>
                                </a>
                            </div>

                          <div class="col-sm-6 col-lg-4">
                                <a href="#cargoCategoryModal" data-toggle="modal" data-controls-modal="cargoCategoryModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                          <i class="fa fa-building-o fa-2x"></i><br />
                                            <h4 class="panel-title">Occupation</h4>
																						<?php
																							$agency_logtype = 'occupation';
																							$agency_log = $this->master->getRecords('agency_log', array('type'=>$agency_logtype, 'agency_id'=>$prod_user_id), '*', array('id'=>'DESC'));
																							$last_date = (!empty($agency_log)) ? date_format(date_create($agency_log[0]['date_added']),'d-m-Y') : 'NA';
																						?>
																						<span class="show_history_btnx" data-type="<?php echo $agency_logtype ?>">
																							<small>Last updated: <?php echo $last_date; ?></small>
																						</span>


                                        </div>
                                    </div>
                                </a>

                            </div>

                          <div class="col-sm-6 col-lg-4">
                                <a href="#transportationModal" data-toggle="modal" data-controls-modal="transportationModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                          <i class="fa fa-users fa-2x"></i><br />
                                            <h4 class="panel-title">Employee</h4>
																						<?php
																							$agency_logtype = 'employees';
																							$agency_log = $this->master->getRecords('agency_log', array('type'=>$agency_logtype, 'agency_id'=>$prod_user_id), '*', array('id'=>'DESC'));
																							$last_date = (!empty($agency_log)) ? date_format(date_create($agency_log[0]['date_added']),'d-m-Y') : 'NA';
																						?>
																						<span class="show_history_btnx" data-type="<?php echo $agency_logtype ?>">
																							<small>Last updated: <?php echo $last_date; ?></small>
																						</span>


                                        </div>
                                    </div>
                                </a>
                            </div>

                          <?php /*?><div class="col-sm-6 col-lg-4">
                              <div class="well well-maroon text-center">
                                  <h4 class="panel-title">Country</h4><br />

                                    <a href="#countryModal" data-toggle="modal" data-controls-modal="countryModal" class="btn btn-default btn-outline btn-sm">Update rating <i class="fa fa-caret-right"></i></a>
                                </div>
                            </div><?php */?>

                          <div class="col-sm-6 col-lg-4">
                                <a href="#deductibleModal" data-toggle="modal" data-controls-modal="countryModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                          <i class="fa fa-bullseye fa-2x"></i><br />
                                      <h4 class="panel-title">Deductible</h4>
																			<?php
																				$agency_logtype = 'deductible';
																				$agency_log = $this->master->getRecords('agency_log', array('type'=>$agency_logtype, 'agency_id'=>$prod_user_id), '*', array('id'=>'DESC'));
																				$last_date = (!empty($agency_log)) ? date_format(date_create($agency_log[0]['date_added']),'d-m-Y') : 'NA';
																			?>
																			<span class="show_history_btnx" data-type="<?php echo $agency_logtype ?>">
																				<small>Last updated: <?php echo $last_date; ?></small>
																			</span>


                                        </div>
                                    </div>
                                </a>
                            </div>

                          <div class="col-sm-6 col-lg-4">
                                <a href="<?php echo base_url().'webmanager/referral/manage/3'?>">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                          <i class="fa fa-cubes fa-2x"></i><br />
                                      <h4 class="panel-title">Referral Points</h4>
																			<?php
																				$agency_logtype = 'referral_point';
																				$agency_log = $this->master->getRecords('agency_log', array('type'=>$agency_logtype, 'agency_id'=>$prod_user_id), '*', array('id'=>'DESC'));
																				$last_date = (!empty($agency_log)) ? date_format(date_create($agency_log[0]['date_added']),'d-m-Y') : 'NA';
																			?>
																			<span class="show_history_btnx" data-type="<?php echo $agency_logtype ?>">
																				<small>Last updated: <?php echo $last_date; ?></small>
																			</span>


                                        </div>
                                    </div>
                                </a>

                            </div>


                          <div class="col-sm-6 col-lg-4">
                                <a href="#policyFeeModal" data-toggle="modal" data-controls-modal="policyFeeModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                          <i class="fa fa-usd fa-2x"></i><br />
                                            <h4 class="panel-title">Policy Fee</h4>
																						<?php
																							$agency_logtype = 'policy_fee';
																							$agency_log = $this->master->getRecords('agency_log', array('type'=>$agency_logtype, 'agency_id'=>$prod_user_id), '*', array('id'=>'DESC'));
																							$last_date = (!empty($agency_log)) ? date_format(date_create($agency_log[0]['date_added']),'d-m-Y') : 'NA';
																						?>
																						<span class="show_history_btnx" data-type="<?php echo $agency_logtype ?>">
																							<small>Last updated: <?php echo $last_date; ?></small>
																						</span>



                                        </div>
                                    </div>
                                </a>
                            </div>

                          <div class="col-sm-6 col-lg-4">
                                <a href="<?php echo base_url().'webmanager/ratings/promo/3' ?>">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                          <i class="fa fa-file-text-o fa-2x"></i><br />
                                            <h4 class="panel-title">Promo Code</h4>
																						<?php
																							$agency_logtype = 'promo_code';
																							$agency_log = $this->master->getRecords('agency_log', array('type'=>$agency_logtype, 'agency_id'=>$prod_user_id), '*', array('id'=>'DESC'));
																							$last_date = (!empty($agency_log)) ? date_format(date_create($agency_log[0]['date_added']),'d-m-Y') : 'NA';
																						?>
																						<span class="show_history_btnx" data-type="<?php echo $agency_logtype ?>">
																							<small>Last updated: <?php echo $last_date; ?></small>
																						</span>


                                        </div>
                                    </div>
                                </a>
                            </div>

                          <div class="col-sm-6 col-lg-4">
                                <a href="<?php echo base_url().'webmanager/eproducts/preview?tab=1&product_id=3#quote' ?>" target="_blank">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                          <i class="fa fa-edit fa-2x"></i><br />
                                            <h4 class="panel-title">Preview Form</h4>
																						<?php
																							$agency_logtype = 'preview_form';
																							$agency_log = $this->master->getRecords('agency_log', array('type'=>$agency_logtype, 'agency_id'=>$prod_user_id), '*', array('id'=>'DESC'));
																							$last_date = (!empty($agency_log)) ? date_format(date_create($agency_log[0]['date_added']),'d-m-Y') : 'NA';
																						?>
																						<span class="show_history_btnx" data-type="<?php echo $agency_logtype ?>">
																							<small>Last updated: <?php echo $last_date; ?></small>
																						</span>

                                        </div>
                                    </div>
                                </a>
                            </div>

                          <div class="col-sm-6 col-lg-4">
                                <a href="#assignAgentModal" data-toggle="modal" data-controls-modal="assignAgentModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                          <i class="fa fa-user fa-2x"></i><br />
                                            <h4 class="panel-title">Assign Underwriter</h4>
																						<?php
																							$agency_logtype = 'assign_underwriter';
																							$agency_log = $this->master->getRecords('agency_log', array('type'=>$agency_logtype, 'agency_id'=>$prod_user_id), '*', array('id'=>'DESC'));
																							$last_date = (!empty($agency_log)) ? date_format(date_create($agency_log[0]['date_added']),'d-m-Y') : 'NA';
																						?>
																						<span class="show_history_btnx" data-type="<?php echo $agency_logtype ?>">
																							<small>Last updated: <?php echo $last_date; ?></small>
																						</span>

                                        </div>
                                    </div>
                                </a>
                            </div>

                          <div class="col-sm-6 col-lg-4">
                                <a href="<?php echo base_url().'webmanager/policydoc/manage/3'; ?>">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                          <i class="fa fa-file-pdf-o fa-2x"></i><br />
                                            <h4 class="panel-title">Policy Document</h4>
																						<?php
																							$agency_logtype = 'policy_doc';
																							$agency_log = $this->master->getRecords('agency_log', array('type'=>$agency_logtype, 'agency_id'=>$prod_user_id), '*', array('id'=>'DESC'));
																							$last_date = (!empty($agency_log)) ? date_format(date_create($agency_log[0]['date_added']),'d-m-Y') : 'NA';
																						?>
																						<span class="show_history_btnx" data-type="<?php echo $agency_logtype ?>">
																							<small>Last updated: <?php echo $last_date; ?></small>
																						</span>

                                        </div>
                                    </div>
                                </a>
                            </div>

                          	<div class="col-sm-6 col-lg-4">
                                <a href="#quoteExpiryModal" data-toggle="modal" data-controls-modal="quoteExpiryModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                          <i class="fa fa-file-pdf-o fa-2x"></i><br />
                                            <h4 class="panel-title">Quote Expiry</h4>
																						<?php
																							$agency_logtype = 'base_currency';
																							$agency_log = $this->master->getRecords('agency_log', array('type'=>$agency_logtype, 'agency_id'=>$prod_user_id), '*', array('id'=>'DESC'));
																							$last_date = (!empty($agency_log)) ? date_format(date_create($agency_log[0]['date_added']),'d-m-Y') : 'NA';
																						?>
																						<span class="show_history_btnx" data-type="<?php echo $agency_logtype ?>">
																							<small>Last updated: <?php echo $last_date; ?></small>
																						</span>

                                        </div>
                                    </div>
                                </a>
                            </div>



                      </div>



											<div class="div_product_actions clearfix pull-right <?php echo ($this->session->userdata('logged_admin') != 'admin') ? 'hidden' : '' ?>">
												<a href="#" class="btn open_modal_form_btn" data-controls-modal="#newProductModal"><i class="fa fa-plus"></i> Product</a>
												<a href="#" class="btn update-product">Update</a>
												<a href="#" class="btn text-danger delete_btn" data-table="q_products" data-reload="yes">Delete</a>
											</div>




            </div><!--end of col product tab2-->

        </div>


<div class="the_main_container <?php echo ($user_session['sidenav_settings'] == 'collapsed') ? 'side-collapsed' : ''?>">

        <nav id="sidebar-wrapper" class="sidebar-wrapper-admin">

            <div class="list-group">


              <?php if($this->session->userdata('logged_admin_id') != '') { ?>
              <a href="#" data-toggle="modal" data-target="#myProfileModal" class="list-group-item" onclick="$('.nav-toggle').click();">
                <i class="fa fa-user fa-fw"></i> My Profile
              </a>

	            <a href="<?php echo ($this->common->the_domain() == '') ? base_url().'settings/social_login' : 'https://ebinder.com.au/settings/social_login' ?>" class="list-group-item" onclick="$('.nav-toggle').click();">
	              <i class="fa fa-google-plus-square fa-fw"></i> Social Login
	            </a>

	            <a href="<?php echo $this->common->bbase_url().'webmanager/settings/password' ?>" class="list-group-item" onclick="$('.nav-toggle').click();">
	              <i class="fa fa-key fa-fw"></i> Change Password
	            </a>

              <a href="<?php echo $this->common->bbase_url().'webmanager/settings/users'; ?>" class="list-group-item" onclick="$('.nav-toggle').click();">
                <i class="fa fa-users fa-fw"></i> Manage Users
              </a>

              <?php } ?>

              <?php if($this->session->userdata('first_underwriter') == 'Y') { ?>

              <a href="#" data-toggle="modal" data-target="#agencyProfileModal" class="list-group-item" onclick="$('.nav-toggle').click();">
                <i class="fa fa-building-o fa-fw"></i> Agency Profile
              </a>


              <?php } ?>


              <?php // if(in_array('Activity Log', $access_rights)) { ?>
              <a href="<?php echo $this->common->bbase_url().'webmanager/settings/activitylog'; ?>" class="list-group-item" onclick="$('.nav-toggle').click();">
                <i class="fa fa-file-text-o fa-fw"></i> Activity Log
              </a>
              <?php //} ?>



           <?php if($this->session->userdata('logged_admin_id') == '') { ?>
              <a href="<?php echo $this->common->bbase_url().'webmanager/settings/changePassword'; ?>" class="list-group-item" onclick="$('.nav-toggle').click();">
                <i class="fa fa-envelope fa-fw"></i> Email &amp; Password
              </a>
              <?php } ?>


              <a href="<?php echo $this->common->bbase_url().'webmanager/dashboard/logout'; ?>" class="list-group-item" onclick="$('.nav-toggle').click();">
                <i class="fa fa-sign-out fa-fw"></i> Logout
              </a>
            </div>



        </nav>




        <div class="row">

            <div class="col-lg-2 col-md-3 col-sm-4 hidden-xs sidebar_nav">

                <div class="just-padding">
                    <div class="list-group list-group-root" id="sideAccordion">



                        <?php
                        //for underwriters only
                        if($this->session->userdata('logged_admin_id') != '') { ?>
														<?php if(in_array('Dashboard', $access_rights)) { ?>
		                        <a href="<?php echo $this->common->bbase_url().'webmanager/dashboard' ?>" class="ripplelink list-group-item <?php if($this->uri->segment(2) == 'dashboard' && $this->uri->segment(3) == '') { echo 'active'; }?>"><i class="fa fa-dashboard fa-fw"></i> <span>Dashboard</span></a>
		                        <?php } ?>


                            <?php if(in_array('Manage Brokers', $access_rights)) { ?>
                            <a href="<?php echo $this->common->bbase_url().'webmanager/customers/manage/brokers'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(4) == 'brokers' || $this->uri->segment(3) == 'broker') { echo 'active'; }?>"><i class="fa fa-users fa-fw"></i> <span>Manage Brokers</span></a>
                            <?php } ?>


                            <?php if(in_array('Manage Referrals', $access_rights)) { ?>
                            <a href="<?php echo $this->common->bbase_url().'webmanager/referral/submitted'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'submitted') { echo 'active'; }?>"><i class="fa fa-files-o fa-fw"></i> <span>Manage Referrals</span>
														</a>
                            <?php } ?>

                            <?php if(in_array('Manage Policies', $access_rights)) { ?>
                            <a href="<?php echo $this->common->bbase_url().'webmanager/referral/policies'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'policies') { echo 'active'; }?>"><i class="fa fa-book fa-fw"></i> <span>Manage Policies</span></a>
                            <?php } ?>



                            <?php if(in_array('Manage Claims', $access_rights)) { ?>
                            <a href="<?php echo $this->common->bbase_url().'webmanager/claims/'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(2) == 'claims') { echo 'active'; }?>"><i class="fa fa-list fa-fw"></i> <span>Manage Claims</span>
															<span class="pull-right badge badge-red <?php echo (empty($unassigned_claims)) ? 'hidden' : ''; ?>"><?php echo $unassigned_claims; ?></span>
														</a>
                            <?php } ?>



                            <?php if(in_array('Manage Ratings', $access_rights)) { ?>

                              <a href="#item-2" class="ripplelink list-group-item" data-toggle="collapse" data-parent="#sideAccordion">
                                <i class="fa fa-cart-plus fa-fw"></i> <span>Manage Products</span>
                                <i class="glyphicon glyphicon-chevron-<?php echo ($this->uri->segment(2) == 'ratings' || $this->uri->segment(2) == 'policydoc' || ($this->uri->segment(2) == 'referral' && $this->uri->segment(3) == 'manage')) ? 'up' : 'down' ?> pull-right"></i>

                              </a>
                              <div class="list-group collapse <?php echo ($this->uri->segment(2) == 'ratings' || $this->uri->segment(2) == 'policydoc' || ($this->uri->segment(2) == 'referral' && $this->uri->segment(3) == 'manage')) ? 'in' : '' ?>" id="item-2">

                                <?php
                                  foreach($prod_q_products as $u=>$user){
                                    if(in_array($user['id'], $prod_prod_id)){
                                ?>

                                <a href="#" class="producttab_btn ripplelink list-group-item preloadThis <?php echo ($this->uri->segment(4) == $user['id']) ? 'active' : '' ?>" data-id="<?php echo $user['id']?>">
																	<i class="fa fa-line-chart fa-fw"></i> <span><?php echo $user['product_name'] ?></span>
																	<!-- <i class="fa fa-caret-right pull-right"></i> -->
																</a>

                                <?php } } ?>
                              </div>


                            <?php } ?>


                            <?php if(in_array('Access Log', $access_rights)) { ?>
                            <a href="<?php echo $this->common->bbase_url().'webmanager/customers/accesslog'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'accesslog') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> <span>Access Log</span></a>
                            <?php } ?>


                        <?php } ?>



                        <?php
                        //for webmanager only
                        if($this->session->userdata('logged_admin_id') == '') { ?>

													<div class="group-items">
														<a href="#item-bmodule" class="ripplelink list-group-item" data-toggle="collapse" data-parent="#sideAccordion">
															<i class="fa fa-user fa-fw"></i> <span>Broker Module</span>
															<i class="glyphicon glyphicon-chevron-down pull-right"></i>
														</a>
														<div class="list-group collapse" id="item-bmodule">
															<a href="<?php echo $this->common->bbase_url().'webmanager/productowners'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(2) == 'productowners') { echo 'active'; }?>"><i class="fa fa-child fa-fw"></i> <span>Product Owners</span></a>

														</div><!-- item-bmodule -->

													</div><!-- group-items -->

													<div class="group-items">
														<a href="#item-umodule" class="ripplelink list-group-item" data-toggle="collapse" data-parent="#sideAccordion">
															<i class="fa fa-building-o fa-fw"></i> <span>Underwriter Module</span>
															<i class="glyphicon glyphicon-chevron-down pull-right"></i>

														</a>
														<div class="list-group collapse" id="item-umodule">


															<?php if(in_array('Dashboard', $access_rights)) { ?>
															<a href="<?php echo $this->common->bbase_url().'webmanager/dashboard' ?>" class="ripplelink list-group-item <?php if($this->uri->segment(2) == 'dashboard' && $this->uri->segment(3) == '') { echo 'active'; }?>"><i class="fa fa-dashboard fa-fw"></i> <span>Dashboard</span></a>
															<?php } ?>


	                            <a href="#item-2" class="ripplelink list-group-item" data-toggle="collapse" data-parent="#sideAccordion">
	                              <i class="fa fa-cart-plus fa-fw"></i> <span>Manage Products</span>
	                              <i class="glyphicon glyphicon-chevron-<?php echo ($this->uri->segment(2) == 'ratings' || $this->uri->segment(2) == 'policydoc' || ($this->uri->segment(2) == 'referral' && $this->uri->segment(3) == 'manage')) ? 'up' : 'down' ?> pull-right"></i>

	                            </a>
	                            <div class="list-group collapse <?php echo ($this->uri->segment(2) == 'ratings' || $this->uri->segment(2) == 'policydoc' || ($this->uri->segment(2) == 'referral' && $this->uri->segment(3) == 'manage')) ? 'in' : '' ?>" id="item-2">

	                              <?php
	                                foreach($prod_q_products as $u=>$user){
	                              ?>

	                              <a href="#" class="producttab_btn ripplelink list-group-item preloadThis <?php echo ($this->uri->segment(4) == $user['id']) ? 'active' : '' ?>" data-id="<?php echo $user['id']?>" data-info='<?php echo json_encode($user)?>'>
																	<i class="fa fa-line-chart fa-fw"></i> <span><?php echo $user['product_name'] ?></span>
																	<!-- <i class="fa fa-caret-right pull-right"></i> -->
																</a>
	                              <?php } ?>
	                            </div>



	                            <a href="<?php echo $this->common->bbase_url().'webmanager/customers/agencies'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'agencies' || $this->uri->segment(3) == 'logo') { echo 'active'; }?>"><i class="fa fa-briefcase fa-fw"></i> <span>Manage Agencies</span></a>

	                            <a href="<?php echo $this->common->bbase_url().'webmanager/customers/underwriters'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'underwriters') { echo 'active'; }?>"><i class="fa fa-male fa-fw"></i> <span>Manage Underwriters</span></a>

	                            <a href="<?php echo $this->common->bbase_url().'webmanager/customers/manage/brokers'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(4) == 'brokers' || $this->uri->segment(3) == 'broker') { echo 'active'; }?>"><i class="fa fa-users fa-fw"></i> <span>Manage Brokers</span></a>

	                            <a href="<?php echo $this->common->bbase_url().'webmanager/emails/manage'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(2) == 'emails') { echo 'active'; }?>"><i class="fa fa-envelope fa-fw"></i> <span>Outgoing Emails</span></a>

	                            <a href="<?php echo $this->common->bbase_url().'webmanager/popups/manage'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(2) == 'popups') { echo 'active'; }?>"><i class="fa fa-bell fa-fw"></i> <span>Pop up Messages</span></a>


														</div><!-- item-umodule -->


													</div><!-- group-items -->



                        <?php } ?>



                        <?php if(in_array('Reporting', $access_rights)) { ?>
                        <a href="<?php echo $this->common->bbase_url().'webmanager/reporting/form'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(2) == 'reporting') { echo 'active'; }?>"><i class="fa fa-file-excel-o fa-fw"></i> <span>Reporting</span></a>
                        <?php } ?>


                        <?php
                        //for webmanager only
                        if($this->session->userdata('logged_admin_id') == '') { ?>

                              <a href="#item-22" class="ripplelink list-group-item" data-toggle="collapse" data-parent="#sideAccordion">
                                <i class="fa fa-file-text-o fa-fw"></i> <span>Manage Content</span>
                                <i class="glyphicon glyphicon-chevron-<?php echo ($this->uri->segment(2) == 'contents') ? 'up' : 'down' ?> pull-right"></i>

                              </a>
                              <div class="list-group collapse <?php echo ($this->uri->segment(2) == 'contents') ? 'in' : '' ?>" id="item-22">

                                    <a href="<?php echo $this->common->bbase_url().'webmanager/contents/legal'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'legal') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> Manage Legal</a>

                                    <a href="<?php echo $this->common->bbase_url().'webmanager/contents/legal2'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'legal2') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> Common Terms &amp; Conditions</a>

                                    <a href="<?php echo $this->common->bbase_url().'webmanager/contents/faq'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'faq') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> FAQs</a>

                                    <a href="<?php echo $this->common->bbase_url().'webmanager/contents/topheadtitle'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'topheadtitle') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> Top Head Title</a>

                                    <a href="<?php echo $this->common->bbase_url().'webmanager/contents/mainhometitle'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'mainhometitle') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> Home Main Title</a>


                                    <a href="<?php echo $this->common->bbase_url().'webmanager/contents/banners'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'banners' || $this->uri->segment(3) == 'banner') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> Typo Effect</a>

                                    <a href="<?php echo $this->common->bbase_url().'webmanager/contents/firstsection'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'firstsection') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> First Section</a>

                                    <a href="<?php echo $this->common->bbase_url().'webmanager/contents/alternatehead'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'alternatehead' || $this->uri->segment(3) == 'alternate' || $this->uri->segment(3) == 'manage_alternates') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> Alternating Content Head</a>

                                    <a href="<?php echo $this->common->bbase_url().'webmanager/contents/threeparts'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'threeparts' || $this->uri->segment(3) == 'threepart' || $this->uri->segment(3) == 'manage_threeparts' ) { echo (!isset($_GET['type'])) ? 'active' : ''; }?>"><i class="fa fa-file-text-o fa-fw"></i> 3 Columns Content</a>

                                    <a href="<?php echo $this->common->bbase_url().'webmanager/contents/threeparts?type=threepartshead2'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'threeparts' || $this->uri->segment(3) == 'threepart' || $this->uri->segment(3) == 'manage_threeparts' ) { echo (isset($_GET['type'])) ? 'active' : ''; }?>"><i class="fa fa-file-text-o fa-fw"></i> 3 Columns Content Pt 2</a>



                                    <a href="<?php echo $this->common->bbase_url().'webmanager/contents/aboutus'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'aboutus') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> About Us Footer</a>

                                    <a href="<?php echo $this->common->bbase_url().'webmanager/contents/aboutuspage'; ?>" class="ripplelink list-group-item preloadThis <?php if($this->uri->segment(3) == 'aboutus') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> About Us Page</a>
                              </div>


                        <?php } ?>

                        <?php if(false) { //in_array('Settings', $access_rights)) { ?>


                              <a href="#item-1" class="list-group-item" data-toggle="collapse" data-parent="#sideAccordion">
                                <i class="fa fa-gear fa-fw"></i> Settings
                                <i class="glyphicon glyphicon-chevron-<?php echo ($this->uri->segment(2) == 'settings') ? 'down' : 'right' ?> pull-right"></i>

                              </a>
                              <div class="list-group collapse <?php echo ($this->uri->segment(2) == 'settings') ? 'in' : '' ?>" id="item-1">

                                <?php if($this->session->userdata('logged_admin_id') != '') { ?>
                                <a href="<?php echo $this->common->bbase_url().'webmanager/settings/users'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(3) == 'users') { echo 'active'; }?>"><i class="fa fa-user fa-fw"></i> Manage Users</a>


                                    <?php // if(in_array('Activity Log', $access_rights)) { ?>
                                    <a href="<?php echo $this->common->bbase_url().'webmanager/settings/activitylog'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(3) == 'activitylog') { echo 'active'; }?>"><i class="fa fa-file-text-o fa-fw"></i> Activity Log</a>
                                    <?php //} ?>

                                <?php } ?>

                                <?php if($this->session->userdata('logged_admin_id') == '') { ?>
                                <a href="<?php echo $this->common->bbase_url().'webmanager/settings/changePassword'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(3) == 'changePassword') { echo 'active'; }?>"><i class="fa fa-key fa-fw"></i> Email &amp; Password</a>
                                <?php } ?>

                              </div>

                        <?php } ?>
												<a href="#" class="ripplelink list-group-item toggle_sidenav_expand">
                          <i class="fa <?php echo ($user_session['sidenav_settings'] == 'collapsed') ? 'fa-caret-right' : 'fa-caret-left'?> fa-fw"></i> <span>Toggle Menu</span>
                        </a>


                    </div>
                </div>





            </div><!--end sidebar_nav-->




        </div>

        <div class="row row_the_content">

            <div class="col-lg-2 col-md-3 col-sm-4">

            </div>
