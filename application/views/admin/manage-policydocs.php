<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage Policy Documents</h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage Policy Documents</li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

					<?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">
                <div class="panel panel-default">
                	<div class="panel-heading">

                      <a href="#" class="btn btn-primary pull-right open_modal_form_btn" data-controls-modal="#policyModal" style="margin-right: 5px;"><i class="fa fa-plus"></i> <?php echo $singular_title ?></a>

                      <a href="#" class="btn btn-primary pull-right open_modal_form_btn" data-controls-modal="#endorseModal" style="margin-right: 5px;"><i class="fa fa-plus"></i> Endorsement</a>

                      <a href="#" class="btn btn-default pull-right producttab_btn" data-id="<?php echo $this->uri->segment(4) ?>" style="margin-right: 5px;">Back</a>
                      <a href="#historyModal" class="btn pull-right show_history_btn" data-type="policy_doc" style="margin-right: 5px;">
                        <i class="fa fa-history"></i> History
                      </a>
          						<div  class="panel-title">
                          <h4><?php echo $title; ?></h4>
          						</div>
                    </div>

                    <div class="panel-body">

                       <?php if(count($policy_docs) > 0) { ?>


            						<div class="table-responsive">

                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th class="hidden">#</th>
                              <th style="width: 110px">Date Added</th>
                              <th style="width: 120px">Type</th>
                              <th style="width: 120px">Reference</th>
                              <th style="">Endorsement Title</th>
                              <th style="width: 5%"> </th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php
                              $countt = 1;
                							foreach($policy_docs as $r=>$value){
                                 $value['file_name'] = substr($value['name'], 13);
                						?>

                                <tr class="<?php echo (($countt == 1 && $this->session->flashdata('success') != '')) ? 'flash_animation' : '' ?>">

                                  <td class="hidden">
                                    <?php echo $countt; ?>
                                  </td>

                                  <td class="">
                                    <?php echo date_format(date_create($value['date_added']), 'd-m-Y') ?>
                                  </td>

                                  <td class="">
                                    <?php echo ($value['doc_type'] == '0') ? 'Wording' : 'Endorsement'; ?>
                                  </td>

                                  <td class="">
                                    <?php echo $value['reference']; ?>
                                  </td>

                                  <td class="">
                                    <a href="javascript:;" class="update_policydoc_btn" data-info='<?php echo json_encode($value)?>'>
                                      <?php echo $value['title'] ?>
                                    </a>
                                  </td>



                                  <td>

                                        <!-- Single button -->
                                        <div class="btn-group pull-right">
                                          <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                            Action <span class="caret"></span>
                                          </button>
                                          <ul class="dropdown-menu" role="menu">
                                            <li><a href="javascript:;" class="update_policydoc_btn" data-info='<?php echo json_encode($value)?>'>Update</a></li>
                                            <li><a href="javascript:;" class="delete_btn" data-id="<?php echo $value['id']?>" data-table="policy_docs">Delete</a></li>
                                          </ul>
                                        </div>


                                  </td>
                                </tr>

                            <?php
                                $countt++;
                							}
                						?>



                          </tbody>
                        </table>
                        <p class="lead">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php } else{
            							echo '<p class="text-center text-muted">Nothing to show you.</p>';
            						}?>


                        <?php /*

                                <p class="text-center">
                                    <a href="#" class="btn btn-primary" onclick="$('#myfile').click()">Upload Policy Document</a>
                                </p>

                                <div id="progress" class="progress-profile" style="display: none;">
                                    <div id="bar" class="bar-profile"></div>
                                    <div id="percent" class="percent-profile">0%</div >
                                </div>

                                <p class="lead text-center policydoc">
                                	<?php

        							if(count($policy_docs) > 0) {
        								echo '<a href="'.base_url().'uploads/policyd/'.$policy_docs[0]['name'].'" target="_blank" class=""><i class="fa fa-file-pdf-o"></i> '.substr($policy_docs[0]['name'], 13).'</a> <small><i class="fa fa-times-circle text-muted delete_policy_btn"></i></small>';

        							}
        							?>

                                </p>


                        */ ?>
                    </div>

                </div>

            </div>

           <div class="row hidden">
            <form id="uploadPolicyForm" action="<?php echo base_url().'settings/policydoc' ?>" method="post" enctype="multipart/form-data">
                 <input type="file" size="60" id="myfile" name="myfile" class="myfile-profile" onchange="$(this).closest('form').submit();">
                 <input type="hidden" id="avatar_name" value="">
                 <input type="hidden" name="policydoc" id="policy_id" value="">
                 <input type="submit" class="hidden" value="Ajax File Upload">
             </form>
            </div>


        </div><!--.row-->



</div>

<!-- policyModal -->
<div class="portfolio-modal modal fade" id="policyModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                    <div class="modal-body">

                        <h1>Wording</h1>
                        <hr class="star-primary">


                        <form id="policy_doc_form" novalidate="novalidate" class="text-left">
                            <input type="hidden" name="id" value="" />
                            <input type="hidden" name="product_id" value="<?php echo $this->uri->segment(4); ?>" />

                            <div class="form-group">
                                <label for="">Policy reference</label>
                                <input type="text" class="form-control" name="reference" placeholder="" value="" required="required"/>
                            </div>


                            <div class="form-group">
                              <label>Add policy wording doc (PDF)</label><br />

                              <a href="javascript:;" id="dragandrophandler" data-form="#uploadPolicyForm" onclick="$('#myfile').click()" class="btn btn-default btn-block btn-upload"><i class="fa fa-paperclip fa-3x"></i><br>Choose a File or Drag it here</a>
                            </div>

                            <div class="form-group">
                                <div id="progress" class="progress-profile" style="display: none;">
                                    <div id="bar" class="bar-profile"></div>
                                    <div id="percent" class="percent-profile">0%</div >
                                </div>
                            </div>

                            <div class="form-group policydoc">

                            </div>


                            <div class="form-group text-right">
                                <a href="#" data-dismiss="modal" class="btn btn-cancel btn-grey">Cancel</a>
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>


                        </form>

                    </div><!--modal-body-->

                </div>
            </div>
        </div>



    </div>
</div><!--portfolio-modal-->




<!-- policyModal -->
<div class="portfolio-modal modal fade" id="endorseModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="true">
    <div class="modal-content">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
                    <div class="modal-body">

                        <h1>Endorsement</h1>
                        <hr class="star-primary">


                        <form id="endorse_doc_form" novalidate="novalidate" class="text-left">
                            <input type="hidden" name="id" value="" />
                            <input type="hidden" name="product_id" value="<?php echo $this->uri->segment(4); ?>" />

                            <div class="form-group">
                                <label for="">Endorsement Reference</label>
                                <input type="text" class="form-control" name="reference" placeholder="e.g. EN001" value=""/>
                            </div>

                            <div class="form-group">
                                <label for="">Endorsement Title</label>
                                <input type="text" class="form-control" name="title" placeholder="" value="" required="required"/>
                            </div>


                            <div class="form-group">
                                <label for="">Endorsement description</label>
                                <textarea class="form-control" name="description" required="required" rows="4"></textarea>
                            </div>

                            <div class="form-group text-right">
                                <a href="#" data-dismiss="modal" class="btn btn-cancel btn-grey">Cancel</a>
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>


                        </form>

                    </div><!--modal-body-->

                </div>
            </div>
        </div>



    </div>
</div><!--portfolio-modal-->
