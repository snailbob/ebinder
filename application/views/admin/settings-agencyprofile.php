<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3><?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active"><?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">

                <div class="panel panel-default">

                
                	<div class="panel-heading">
                     
                    	<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>
                    <div class="panel-body">


                            <form id="agencies_form" novalidate="novalidate" class="text-left">
                                <input type="hidden" name="id" value="<?php echo $agency[0]['id'] ?>" />
                                <input type="hidden" name="utype" />
                            
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Underwriting Agency Name</label>
                                            <input type="text" class="form-control" name="name" required="required" value="<?php echo $agency[0]['name'] ?>"/>
                                        </div>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <label>Address</label>
                                    <input type="text" class="form-control geolocation" name="address" required="required" value="<?php echo $agency[0]['address'] ?>"/>
                                    <input type="hidden" name="lat" value="<?php echo $agency[0]['lat'] ?>" />
                                    <input type="hidden" name="lng" value="<?php echo $agency[0]['lng'] ?>"/>
                                </div>



                                <div class="form-group text-right">
                                    <a href="<?php echo base_url().'webmanager/dashboard'?>" class="btn btn-grey">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            
                            
                            </form>




                    </div>

                </div>

            </div>


        </div><!--.row-->


    
</div>




