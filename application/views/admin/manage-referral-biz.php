<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>


                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">
                <p class="hidden text-right">
                    <a href="#formModal" class="btn btn-primary" data-toggle="modal" data-controls-modal="#formModal"><i class="fa fa-plus"></i> <?php echo $singular_title; ?></a>
                </p>
                <div class="panel panel-default">


                	<div class="panel-heading">
                      <a href="#formModal" class="btn btn-primary pull-right" data-toggle="modal" data-controls-modal="#formModal">
                        <i class="fa fa-plus"></i> Referral Point
                      </a>
                      <a href="#" class="btn btn-default pull-right producttab_btn" data-id="<?php echo $this->uri->segment(4) ?>" style="margin-right: 5px;">
                        Back
                      </a>
                      <a href="#historyModal" class="btn pull-right show_history_btn" data-type="referral_point" style="margin-right: 5px;">
                        <i class="fa fa-history"></i> History
                      </a>

						<div  class="panel-title">
                            <h4>
							<?php echo $title; ?></h4>
						</div>
                    </div>




                    <div class="panel-body">



                    	<form id="the-referral-biz-form">
							<input type="hidden" name="admin" value="yes" />


                            <div>

                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Questionnaire</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Claims</a></li>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Declarations</a></li>
                                <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Quote</a></li>
                              </ul>

                              <!-- Tab panes -->
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">

                                    <p class="lead"></p>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '91';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '92';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '93';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>


                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '94';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>


                </div>
            </div>
        </div>




        <?php
		$newtabs = $this->master->getRecords('biz_prod_questionnaire', array('tab'=>'2', 'referral_custom'=>'Y'));
		if(count($newtabs) > 0) {
			foreach($newtabs as $r=>$value) {
		?>
        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = $value['id'];
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>


                </div>
            </div>
        </div>
        <?php
			}
		}
		?>




                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">

                                    <!--part 2-->
                                    <p class="lead"></p>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '95';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '96';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '97';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '98';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>


        <?php
		$newtabs = $this->master->getRecords('biz_prod_questionnaire', array('tab'=>'3', 'referral_custom'=>'Y'));
		if(count($newtabs) > 0) {
			foreach($newtabs as $r=>$value) {
		?>
        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = $value['id'];
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>


                </div>
            </div>
        </div>
        <?php
			}
		}
		?>

                                </div>
                                <div role="tabpanel" class="tab-pane" id="messages">


                                    <!--part 3-->
                                    <p class="lead"></p>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '99';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '100';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '101';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '102';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '103';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '104';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">

                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>



        <?php
		$newtabs = $this->master->getRecords('biz_prod_questionnaire', array('tab'=>'4', 'referral_custom'=>'Y'));
		if(count($newtabs) > 0) {
			foreach($newtabs as $r=>$value) {
		?>
        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = $value['id'];
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>


                </div>
            </div>
        </div>
        <?php
			}
		}
		?>


                                </div>
                                <div role="tabpanel" class="tab-pane" id="settings">


                                    <!--part 4-->
                                    <p class="lead"></p>



        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = '106';
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>

                <div class="form-group">

                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>

                </div>
            </div>
        </div>


        <?php
		$newtabs = $this->master->getRecords('biz_prod_questionnaire', array('tab'=>'5', 'referral_custom'=>'Y'));
		if(count($newtabs) > 0) {
			foreach($newtabs as $r=>$value) {
		?>
        <div class="row">
            <div class="col-sm-6">
				<?php
                $thequest_id = $value['id'];
                $question = $this->master->getRecords('biz_prod_questionnaire', array('id'=>$thequest_id));
                $thequest = (!empty($question)) ? $question[0]['question'] : 'Total Annual Revenue $';
                $thetip = (!empty($question)) ? $question[0]['tooltip'] : '';
                $thequest2 = url_title($thequest, 'underscore', TRUE);
                 ?>


                <b>
                    <span>
                        <span class="price-text" data-id="<?php echo $thequest_id ?>" title="click to edit"><?php echo $thequest; ?></span>
                        <div class="text_form hidden">
                            <div class="form-group">
                                <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $thequest; ?>" />
                                <input type="hidden" name="id" value="<?php echo $thequest_id ?>" />
                                <input type="hidden" name="field_name" value="question" />
                                <input type="hidden" name="db_name" value="biz_prod_questionnaire" />
                            </div>
                            <div class="form-group">
                                <button type="button" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                            </div>
                        </div>
                    </span>
                    <br /><br />
                 </b>

            </div>
            <div class="col-sm-4">
                <a href="#" class="btn reset-inline-checkbox btn-link pull-right">Not required</a>
                <div class="form-group">
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" id="yesModified" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="Yes" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] == 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        Yes
                    </label>
                    <label class="form-checkbox">
                        <div class="enhanced-checkbox">
                            <input type="radio" class="mandatory checkbox user-success" name="<?php echo $thequest2 ?>" value="No" <?php if (isset($homebiz1[''.$thequest2])) { echo ($homebiz1[''.$thequest2] != 'Yes') ? 'checked="checked"' : ''; }?>><span></span></div>
                        No
                    </label>


                </div>
            </div>
        </div>
        <?php
			}
		}
		?>



                                </div>
                              </div>

                            </div>

                            <hr />

                            <div class="form-group text-right">
                              <button type="submit" class="btn btn-primary">Submit</button>

                              <a href="#" class="hidden btn btn-grey pull-right producttab_btn" data-id="<?php echo $this->uri->segment(4) ?>" style="margin-right: 5px;">Cancel</a>
                            </div>

                        </form>




                    </div>

                </div>

            </div>


        </div><!--.row-->



</div>





<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $singular_title; ?> Information</h4>
      </div>
      <div class="modal-body">
       	<div class="customer_info">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="formModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

                            <h1>Referral Point</h1>
            			           <hr />


                            <form id="prod_questionnaire_form" class="text-left">
                            	<input type="hidden" name="id" />
                            	<input type="hidden" name="broker_id" name="0" />
                                <div class="form-group">
                                    <label>Question</label>
                                    <input type="text" class="form-control" name="product_name" />
                                </div>
                                <div class="form-group">
                                    <label>Text Helper</label>
                                    <input type="text" class="form-control" name="helper" />
                                </div>
                                <div class="form-group">
                                    <label>Step</label>
                                    <select class="form-control" name="tab">
                                    	<option value="">Select</option>
                                    	<option value="2">Questionnaire</option>
                                    	<option value="3">Claims</option>
                                    	<option value="4">Declarations</option>
                                    	<option value="5">Quote</option>
                                    </select>
                                </div>
                                <div class="form-group text-right">
                                  <a href="#" class="btn btn-cancel btn-grey" data-dismiss="modal">Cancel</a>
                                  <button class="btn btn-primary" type="submit">Submit</button>
                                </div>


                            </form>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->
