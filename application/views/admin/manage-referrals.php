
            
            <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">
            
                <div class="row">
            
                
                  <div class="col-lg-12">
                  
                    <div class="page-title">
                    
                      <h3>Manage <?php echo $title; ?></h3>
                      
                      <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url().'webmanager' ?>">Dashboard</a></li>
                        <li class="active">Manage <?php echo $title; ?></li>
                      </ol>
                    </div>
                        
                        
                    <?php if ($this->session->flashdata('ok') != ''){ ?>
                    <div class="alert alert-success fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
    
                        <?php echo $this->session->flashdata('ok'); ?>
                    </div>
                    <?php } ?>
                
                    <?php if ($this->session->flashdata('error') != ''){ ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
    
                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                    <?php } ?>
                
    
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
								<h4><?php echo $title; ?></h4>
                            </div>
                        </div>
                       
                       
                        <?php if(count($insurances) > 0){ ?>
                        <div class="bg-white">
                        <table class="table table-hover mydataTb">
                            <thead>
                                <tr>
                                    <th class="hidden">sort</th>
                                    <th>Customer Name</th>
                                    <th>Purchase Date</th>
                                    <th>Premium</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
								$count = 1;
								foreach($insurances as $r=>$value){
									$insurance = $this->common->the_cert_data($value['id']);
                              
                                    $details = unserialize($value['details']);
                                    $premium = (isset($details['premium'])) ? $details['premium'] : 0;
									$premium = number_format($premium, 2, '.', ',');
                                    $buy_inputs = $details['buy_inputs'];
									$single_input = array();
									if(isset($details['single_input'])){
										$single_input = $details['single_input'];
									}
									$cust_name = $this->common->customer_name($value['customer_id']);
									if($cust_name == 'Not exist'){
										$cust_name = 'Single Purchaser';
									}
									
									
                                    $date_purch = date_format(date_create($value['date_purchased']), 'F d, Y - l');
                                    if($value['date_purchased'] == '0000-00-00 00:00:00'){
                                        $date_purch = 'NA';
                                    }
                                ?>
                                <tr>
                                    <th class="hidden"><?php echo $count; ?></th>
                                    <td><?php echo $cust_name ?></td>
                                    <td><?php echo $date_purch ?></td>
                                    <td><?php echo $insurance['currency'].$premium; ?></td>
                                    <td><?php echo $this->common->policy_request_status($value['status']); ?></td>
                                    <td>
                                    	<a href="#" class="btn btn-default btn-xs pull-right referral_info_btn" data-id="<?php echo $value['id'] ?>" data-target="#infoModal" data-type="">Review</a>
                                        <div class="dropdown pull-right hidden">
                                          <button class="btn btn-default btn-xs" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            Action
                                            <span class="caret"></span>
                                          </button>
                                        
                                          <ul class="dropdown-menu" aria-labelledby="dLabel">
    
                                            <li><a href="#" class="referral_info_btn" data-id="<?php echo $value['id'] ?>" data-target="#infoModal">View Details</a></li>
                                            <li><a href="#" class="review_policy_btn" data-id="<?php echo $value['id'] ?>" data-premium="<?php echo $premium; ?>" data-comment="<?php echo $value['comment']?>">Review</a></li>
                                            
                                            <?php if($value['status'] == 'P'){ ?>
                                            <li class="divider"></li>
                                            <li><a href="#" class="send_certificate_btn" data-id="<?php echo $value['id'] ?>">Send Certificate</a></li>
                                            <?php } ?>
                                          </ul>
                                        </div>                                
                                    
                                    </td>
                                </tr>
                                <?php $count++; } ?>
                            </tbody>
                        </table>
                        </div>
                        
                    <?php
                      //loop sorted_schedule
                     } ?>
                   
                    </div><!--panel-->
                    
                              
                  
                  
                                        
                    
                    
                  </div><!-- /.col-lg-12 --> 
                
                </div><!-- /.row --> 
                <!-- end PAGE TITLE AREA -->
                
            </div><!--end of main_content-->
            


<!-- Modal -->
<div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Review Policy</h4>
      </div>
        <form id="admin_review_form">
      <div class="modal-body">
      		<div class="form-group">
            	<label>Adjust Quoted Premium</label>
            	<input type="text" class="form-control" name="premium" value="" />
            	<input type="hidden" class="form-control" name="id" value="" />
            </div>
        
      		<div class="form-group">
            	<label>Comments</label>
            	<textarea name="comment" class="form-control"></textarea>
            </div>
        
      </div>
      <div class="modal-footer">
        <a href="#" class="btn btn-danger pull-left reject_btn">Reject</a>
        <button type="submit" class="btn btn-primary">Accept</button>
      </div>
        </form>
    </div>
  </div>
</div>



    <!-- Modal for new task -->
    <div class="portfolio-modal modal fade" id="infoModal" tabindex="-1" role="dialog" aria-hidden="true" data-keyboard="false">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="modal-body">
                        
                            <h1 class="buy_title hidden">Buy Single Marine Transit Policy</h1>
                            <hr class="star-primary hidden">
             
                    
                            <div class="txt_details"></div>


                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->