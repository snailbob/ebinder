<div class="col-lg-9 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage FAQs</h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage FAQs</li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

					<?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12" style="padding-bottom: 15px">
            	<a href="#" class="btn btn-sky pull-right" data-toggle="modal" data-target="#faqModal">Add FAQ</a>
            </div>
            <div style="clear: both;"></div>

            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading">

                        <div class="panel-title">

                            <h4>FAQs</h4>

                        </div>
                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">

                        <?php if(count($faqs) > 0) { ?>
                        
                        
						<div class="table-responsive">
                        
                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th class="">Question</th>
                              <th class="">Answer</th>
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>                        
                        
                        <?php
							foreach($faqs as $f=>$faq){
						?>
                        
                            <tr>
                              <td class=""><?php echo $faq['question'] ?></td>
                              <td class=""><?php echo nl2br($faq['answer']); ?></td>
                              <td>
                                                                  
                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a href="javascript:;" onclick="getFaq('<?php echo $faq['id']; ?>');">Update</a></li>
                                        <li><a href="javascript:;" onclick="deleteFaq('<?php echo $faq['id']; ?>');">Delete Permanently</a></li>
                                      </ul>
                                    </div>                              
                              
                              
                              </td>
                            </tr>
                        
                        <?php
								
							}
						?>


                          
                          </tbody>
                        </table>
                        <p class="lead">&nbsp;</p>
                        </div><!--end of table responsive-->
                          
                        <?php } else{
							echo '<p class="lead text-center text-muted">No FAQ</p>';	
						}?>
                        
                        

                    </div>

                </div>

            </div>


        </div><!--.row-->


    
</div>


<!-- Modal for faqs -->
<div class="modal fade bottom" id="faqModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <p class="lead">FAQ</p>
    
            <form id="faq_form" role="form">
            <fieldset>
            <input type="hidden" class="form-control" name="faq_id" id="faq_id" value=""/>
                <div class="form-group">
                    <textarea class="form-control" placeholder="Question" name="question" id="question"></textarea>
                </div>
                <div class="form-group">
                    <textarea class="form-control" placeholder="Answer" name="answer" id="answer" rows="5"></textarea>
                </div>
                <div class="form-group">
                <button type="submit" class="btn btn-sky">Submit</button>
                </div>
            </fieldset>
            </form>
			
      </div>
    </div>
  </div>
</div><!-- /.modal -->
