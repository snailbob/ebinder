<?php // if(!isset($_SESSION['logged_admin'])){redirect(base_url().'admin');} ?>
<?php if($this->session->userdata('logged_admin') == ''){
  redirect(base_url().'webmanager');
} ?>



<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>E-Binder | Admin</title>
    <link rel="shortcut icon" href="<?php echo base_url('assets/img/favicon.png')?>" type="image/x-icon"/>
    <!-- PACE LOAD BAR PLUGIN - This creates the subtle load bar effect at the top of the page. -->
    <?php /*?><link href="<?php echo base_url('assets/user/css/plugins/pace/pace.css') ?>" rel="stylesheet">

    <script src="<?php echo base_url('assets/user/js/plugins/pace/pace.js') ?>"></script><?php */?>



	<script>
		var base_url = '<?php echo base_url() ?>';
		var uri_1 = '<?php echo $this->uri->segment(1) ?>';
		var uri_2 = '<?php echo $this->uri->segment(2) ?>';
		var uri_3 = '<?php echo $this->uri->segment(3) ?>';
		var uri_4 = '<?php echo $this->uri->segment(4) ?>';

    </script>



    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" type="text/css">

    <!-- Custom Fonts -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css" type="text/css">


    <!-- Custom Google Web Font -->
    <link href="<?php echo base_url(); ?>assets/fonts/open-sans.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url(); ?>assets/fonts/merriweather.css" rel="stylesheet" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/dataTables.bootstrap.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/dataTables/css/datatables.css" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/cropper.min.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/cropper/css/crop-avatar.css" type="text/css">

    <!-- Add custom CSS here -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/creative.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/admin-style.css" type="text/css">


    <!-- CSS plugins here -->
<?php /*?>    <link href="<?php echo base_url('assets/user/css/plugins/awesome-bootstrap-checkbox/build.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/user/css/plugins/datepicker/bootstrap-datetimepicker.min.css') ?>" rel="stylesheet">

	<link href="<?php echo base_url('assets/user/css/plugins/dataTables/datatables.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/user/css/plugins/dataTables/dataTables.bootstrap.css')?>" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="<?php echo base_url('assets/user/css/main.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/admin/css/admin-style.css') ?>" rel="stylesheet">

    <link href="<?php echo base_url() ?>assets/user/js/plugins/morris/css/morris.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/user/js/plugins/jvectormap/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/user/js/plugins/flot/css/flot.plugins.css" rel="stylesheet">
	<link href="<?php echo base_url('assets/user/css/plugins/summernote/summernote.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/user/css/plugins/summernote/summernote.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/user/css/plugins/summernote/summernote-bs3.css')?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/user/js/plugins/bootstrap3-editable/css/bootstrap-editable.css')?>" rel="stylesheet">
<?php */?>

</head>

<body style="background: #fff;">

    <nav class="navbar navbar-default navbar-static-top navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <?php /*?><a class="navbar-brand hidden-xs" href="<?php echo base_url('webmanager'); ?>"><img src="<?php echo base_url('assets/img/logo.png')?>"></a>

                <a class="navbar-brand visible-xs" href="<?php echo base_url('webmanager'); ?>"><img src="<?php echo base_url('assets/img/logo.png')?>"></a><?php */?>
                <a class="navbar-brand"><span class="text-muted">Transit Insurance</span></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-inverse navbar-right">
                    <li class="dropdown hidden-xs page-scroll">

                      <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars fa-fw"></i></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url('webmanager/settings/changePassword'); ?>" class="preloadThis"><i class="fa fa-key fa-fw"></i> Change Password</a>
                        <li class="hidden"><a href="<?php echo base_url('webmanager/settings/manageEmail'); ?>" class="preloadThis"><i class="fa fa-gear fa-fw"></i> Update Email ID</a>
                        <li><a href="<?php echo base_url('webmanager/dashboard/logout')?>"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>


                      </ul>
                    </li>

                    <li class="page-scroll"> <a href="<?php echo base_url('webmanager/dashboard'); ?>" class="visible-xs"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a></li>
                    <li class="page-scroll"><a href="<?php echo base_url('webmanager/settings/changePassword'); ?>" class="visible-xs"><i class="fa fa-key fa-fw"></i> Change Password</a>
                    <li class="page-scroll"><a href="<?php echo base_url('webmanager/settings/manageEmail'); ?>" class="visible-xs"><i class="fa fa-gear fa-fw"></i> Update Email ID</a>
                    <li class="page-scroll"> <a href="<?php echo base_url('webmanager/dashboard/logout'); ?>" class="visible-xs"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <div class="container-fluid main_container main_container_admin">

        <div class="row">

            <div class="col-lg-2 col-md-3 col-sm-4 hidden-xs sidebar_nav">
            	<div class="text-center" style="padding-top: 20px;">
            	<img  class="img-circle img-thumbnail" src="<?php echo base_url('uploads/avatars/download.jpg')?>" style="height: 150px">
                <h4>Admin</h4>
                </div>
                <div class="main_nav" <?php if($this->uri->segment(2) == 'settings') { echo 'style="display:none"'; }?>>
                    <a href="<?php echo base_url().'webmanager/dashboard' ?>" class="list-group-item <?php if($this->uri->segment(2) == 'dashboard') { echo 'active'; }?>"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>


                    <a href="<?php echo base_url().'webmanager/users/manage'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(2) == 'users') { echo 'active'; }?>"><i class="fa fa-user fa-fw"></i> Manage Agency</a>

                    <a href="<?php echo base_url().'webmanager/customers/manage'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(2) == 'customers') { echo 'active'; }?>"><i class="fa fa-users fa-fw"></i> Manage Customers</a>

                    <a href="<?php echo base_url().'webmanager/cargo/manage'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(2) == 'cargo') { echo 'active'; }?>"><i class="fa fa-truck fa-fw"></i> Default Agency</a>

                    <a href="<?php echo base_url().'webmanager/insurance/manage'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(2) == 'insurance') { echo 'active'; }?>"><i class="fa fa-cart-plus fa-fw"></i> Bought Insurances</a>


                    <a href="<?php echo base_url().'webmanager/faq/manage'; ?>" class="hidden list-group-item preloadThis <?php if($this->uri->segment(2) == 'faq') { echo 'active'; }?>"><i class="fa fa-question-circle fa-fw"></i> Manage FAQs</a>

                    <a href="<?php echo base_url().'webmanager/pages/manage'; ?>" class="hidden list-group-item preloadThis <?php if($this->uri->segment(3) == 'aboutus') { echo 'active'; }?>"><i class="fa fa-info-circle fa-fw"></i> Confirmation messages</a>

                    <a href="<?php echo base_url().'webmanager/emails/manage'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(2) == 'emails') { echo 'active'; }?>"><i class="fa fa-envelope fa-fw"></i> Outgoing Emails</a>


                    <a href="javascript:;" class="list-group-item" onclick="$('.main_nav').slideUp(); $('.settings_nav').slideDown()"><i class="fa fa-gear fa-fw"></i> Settings</a>

                </div>
                <div class="settings_nav" <?php if($this->uri->segment(2) != 'settings') { echo 'style="display:none"'; }?>>
                    <a href="javascript:;" class="list-group-item  <?php if($this->uri->segment(2) == 'settings') { echo 'label-gray'; }?>" onClick="$('.settings_nav').slideUp(); $('.main_nav').slideDown()"><i class="fa fa-arrow-left fa-fw"></i> Settings</a>
                    <a href="<?php echo base_url().'webmanager/settings/changePassword'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(3) == 'changePassword') { echo 'active'; }?>"><i class="fa fa-key fa-fw"></i> Change Password</a>
                   <?php /*?> <a href="<?php echo base_url().'webmanager/settings/manageEmail'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(3) == 'manageEmail') { echo 'active'; }?>"><i class="fa fa-user fa-fw"></i> Manage Email ID</a>
                    <a href="<?php echo base_url().'webmanager/settings/commission'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(3) == 'commission') { echo 'active'; }?>"><i class="fa fa-exchange fa-fw"></i> Update Commission</a>
                    <a href="<?php echo base_url().'webmanager/settings/inventory'; ?>" class="list-group-item preloadThis <?php if($this->uri->segment(3) == 'inventory') { echo 'active'; }?>"><i class="fa fa-list-alt fa-fw"></i> Inventory Settings</a><?php */?>
                </div>


            </div><!--end sidebar_nav-->
