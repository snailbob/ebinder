<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage Rating</h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage Rating</li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                 <?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">
            
         
       
            	<div class="panel panel-default">
                
                	<div class="panel-heading">
						<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>
                    <div class="panel-body">
                    	<div class="row gutter-md">
                        	<div class="col-sm-6 col-lg-4">
                                <a href="#baseRateModal" data-toggle="modal" data-controls-modal="baseRateModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                        	<i class="fa fa-bolt fa-2x"></i><br />
                                            <h4 class="panel-title">Base Rate</h4>
        
                                        </div>
                                    </div>
                                </a>                                    
                            </div>
                        
                        	<div class="col-sm-6 col-lg-4">
                                <a href="#minimumPremiumModal" data-toggle="modal" data-controls-modal="minimumPremiumModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                        	<i class="fa fa-calculator fa-2x"></i><br />
                                            <h4 class="panel-title">Minimum Premium</h4>
                                        </div>
                                    </div>
                                </a>                                
                            </div>
                        
                        	<div class="col-sm-6 col-lg-4">
                                <a href="#cargoCategoryModal" data-toggle="modal" data-controls-modal="cargoCategoryModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                        	<i class="fa fa-building-o fa-2x"></i><br />
                                            <h4 class="panel-title">Occupation</h4>

                                        </div>
                                    </div>
                                </a>                                

                            </div>
                        
                        	<div class="col-sm-6 col-lg-4">
                                <a href="#transportationModal" data-toggle="modal" data-controls-modal="transportationModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                        	<i class="fa fa-users fa-2x"></i><br />
                                            <h4 class="panel-title">Employee</h4>

                                        </div>
                                    </div>
                                </a>          
                            </div>
                        
                        	<?php /*?><div class="col-sm-6 col-lg-4">
                            	<div class="well well-maroon text-center">
                                	<h4 class="panel-title">Country</h4><br />

                                    <a href="#countryModal" data-toggle="modal" data-controls-modal="countryModal" class="btn btn-default btn-outline btn-sm">Update rating <i class="fa fa-caret-right"></i></a> 
                                </div>
                            </div><?php */?>
                        
                        	<div class="col-sm-6 col-lg-4">
                                <a href="#deductibleModal" data-toggle="modal" data-controls-modal="countryModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                        	<i class="fa fa-bullseye fa-2x"></i><br />
                                			<h4 class="panel-title">Deductible</h4>

                                        </div>
                                    </div>
                                </a>          
                            </div>
                        
                        	<div class="col-sm-6 col-lg-4">
                                <a href="<?php echo base_url().'webmanager/referral/manage'?>">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                        	<i class="fa fa-cubes fa-2x"></i><br />
                                			<h4 class="panel-title">Referral Points</h4>

                                        </div>
                                    </div>
                                </a>    

                            </div>
                            
                        
                        	<div class="col-sm-6 col-lg-4">
                                <a href="#policyFeeModal" data-toggle="modal" data-controls-modal="policyFeeModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                        	<i class="fa fa-usd fa-2x"></i><br />
                                            <h4 class="panel-title">Policy Fee</h4>
                                        </div>
                                    </div>
                                </a>                                
                            </div>
                            
                        	<div class="col-sm-6 col-lg-4">
                                <a href="<?php echo base_url().'webmanager/ratings/promo' ?>">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                        	<i class="fa fa-file-text-o fa-2x"></i><br />
                                            <h4 class="panel-title">Promo Code</h4>
                                        </div>
                                    </div>
                                </a>                                
                            </div>
                            
                        	<div class="col-sm-6 col-lg-4">
                                <a href="<?php echo base_url().'webmanager/eproducts/preview?tab=1' ?>" target="_blank">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                        	<i class="fa fa-edit fa-2x"></i><br />
                                            <h4 class="panel-title">Preview Form</h4>
                                        </div>
                                    </div>
                                </a>                                
                            </div>
                            
                        	<div class="col-sm-6 col-lg-4">
                                <a href="#assignAgentModal" data-toggle="modal" data-controls-modal="assignAgentModal">
                                    <div class="panel panel-primary">
                                        <div class="panel-body text-center">
                                        	<i class="fa fa-user fa-2x"></i><br />
                                            <h4 class="panel-title">Assign Underwriter</h4>
                                        </div>
                                    </div>
                                </a>                                
                            </div>
                            
                            
                        
                        	<?php /*?><div class="col-sm-6 col-lg-4 col-lg-offset-2">
                            	<div class="well well-blue text-center">
                                	<h4 class="panel-title">Max. Insured Value - <strong class="max_insurance"><?php echo ($agency_info[0]['max_insurance']) ? $agency_info[0]['max_insurance'] : 0 ?></strong></h4><br />

                                    <a href="#maxInsuredModal" data-toggle="modal" data-controls-modal="maxInsuredModal" class="btn btn-default btn-outline btn-sm">Update rating <i class="fa fa-caret-right"></i></a> 
                                </div>
                            </div>
                        
                        	<div class="col-sm-6 col-lg-4">
                            	<div class="well well-warning text-center">
                                	<h4 class="panel-title">Base Currency- <strong class="base_currency"><?php echo $base_currency ?></strong></h4><br />

                                    <a href="#baseCurrencyModal" data-toggle="modal" data-controls-modal="maxInsuredModal" class="btn btn-default btn-outline btn-sm">Update currency <i class="fa fa-caret-right"></i></a> 
                                </div>
                            </div><?php */?>
                        
                        </div>
                    	
                    </div>
                   
                </div>         

            </div><!--col-12-->


        </div><!--.row-->



</div><!--main container-->



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="assignAgentModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                            <h1>Assign Underwriter</h1>
                            <hr class="star-primary">
                            
                            <form id="assign_underwriter_form" class="text-left">
                            	<div class="form-group">
                                	<label>Underwriter</label>
                                    <select class="form-control" required="required" name="underwriter">
                                    	<option value="">Select</option>
                                    	<?php
											if(count($underwriters) > 0){
												foreach($underwriters as $r=>$value){
												
													echo '<option value="'.$value['id'].'"';
													
														if($agency_info[0]['assigned_underwriter'] == $value['id']){ echo ' selected="selected"'; }
													echo '>'.$value['first_name'].' '.$value['last_name'].'</option>';
												?>
                                                
                                                	
                                                <?php
												}
											}
										
										?>
										
                                    </select>
                                </div>
                            	<div class="form-group">
                                	<button class="btn btn-primary btn-xl" type="submit">Submit</button>
                                </div>
                            </form>
                     
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
    


    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="baseRateModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                            <h1>Base Rate</h1>
                            <hr class="star-primary">
                            
                            <form class="form-rate form-input-lg">
                            	<div class="form-group">
                                	<label>Base Rate</label>
                                    <input type="text" class="form-control" name="rate" value="<?php echo $agency_info[0]['base_rate']?>" />
                                    <input type="hidden" name="type" value="base_rate" />
                                </div>
                            	<div class="form-group">
                                	<button class="btn btn-primary btn-xl" type="submit">Submit</button>
                                </div>
                            </form>
                     
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
    
    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="minimumPremiumModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                            <h1>Minimum Premium</h1>
                            <hr class="star-primary">
                            
                            <form class="form-rate form-input-lg">
                            	<div class="form-group">
                                	<label>Minimum Premium</label>
                                    <input type="text" class="form-control" name="rate" value="<?php echo $agency_info[0]['minimum_premium']?>" />
                                    <input type="hidden" name="type" value="minimum_premium" />
                                </div>
                            	<div class="form-group">
                                	<button class="btn btn-primary btn-xl" type="submit">Submit</button>
                                </div>
                            </form>
                     
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
        
    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="policyFeeModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                            <h1>Policy Fee</h1>
                            <hr class="star-primary">
                            
                            <form class="form-rate form-input-lg">
                            	<div class="form-group">
                                	<label>Fee</label>
                                    <input type="text" class="form-control" name="rate" value="<?php echo $agency_info[0]['policy_fee']?>" />
                                    <input type="hidden" name="type" value="policy_fee" />
                                </div>
                            	<div class="form-group">
                                	<button class="btn btn-primary btn-xl" type="submit">Submit</button>
                                </div>
                            </form>
                     
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
    
    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="maxInsuredModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                            <h1>Max Insured Value</h1>
                            <hr class="star-primary">
                            
                            <form class="form-rate form-input-lg">
                            	<div class="form-group">
                                	<label>Max Insured Value</label>
                                    <input type="text" class="form-control input-currency" name="rate" value="<?php echo $agency_info[0]['max_insurance']?>" />
                                    <input type="hidden" name="type" value="max_insurance" />
                                </div>
                            	<div class="form-group">
                                	<button class="btn btn-primary btn-xl" type="submit">Submit</button>
                                </div>
                            </form>
                     
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
    
                                
    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="cargoCategoryModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-lg-offset-1">
                        <div class="modal-body">
                        
                            <h1>
                            Occupation 
                            <a href="#addCargoModal" class="btn btn-primary" data-toggle="modal" data-controls-modal="addCargoModal"><i class="fa fa-plus" data-toggle="tooltip" data-title="Add"></i></a> 
                            <a href="#addBulkModal" class="btn btn-primary" data-toggle="modal" data-controls-modal="addBulkModal"><i class="fa fa-upload" data-title="Upload" data-toggle="tooltip"></i></a> 
                            </h1>
                            <hr class="star-primary">
                            
                            <div class="well">
                                <input type="search" class="form-control input-search-table" placeholder="Enter occupation.." >
                            </div>
                            
							<?php
                                if(count($cargo) > 0) {
                            ?>
                            <table class="table table-hover mydataTbx">
                                <thead>
                                    <tr>
                                        <?php /*?><th width="15%">HS Code</th><?php */?>
                                        <th width="60%">Occupation Name</th>
                                        <th width="15%">Multiplier</th>
                                        <th width="10%">Referral</th>
                                        <th width=""></th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    <?php
									$countt = 1;
									foreach($cargo as $r=>$value){
                                        $acargo_price = 0;
                                        if(isset($cargo_prices[$value['id']])) {
                                            $acargo_price = $cargo_prices[$value['id']];
                                        }
										$acargo_max = 0;
                                        if(isset($cargo_max[$value['id']])) {
                                            $acargo_max = $cargo_max[$value['id']];
                                        }
                                    ?>
                                    
                                        <tr class="tr-<?php echo $value['id']; ?>">
                                          
                                            <td class="text-left">
                                                <span class="price-text" data-id="<?php echo $value['id'] ?>" title="click to edit"><?php echo $value['specific_name']; ?></span>
                                                <form class="text_form hidden">
                                                    <div class="form-group">
                                                        <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $value['specific_name']; ?>" />
                                                        <input type="hidden" name="id" value="<?php echo $value['id']; ?>" />
                                                        <input type="hidden" name="field_name" value="specific_name" />
                                                        <input type="hidden" name="db_name" value="q_business_specific" />
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                                        <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                                                    </div>
                                                </form>
                                            </td>
                                            <td >
                                                <span class="price-text" data-id="<?php echo $value['id'] ?>" title="click to edit"><?php echo $acargo_price; ?></span>
                                                <form class="price_form hidden">
                                                    <div class="form-group">
                                                        <input type="text" name="cargo-price" class="form-control input-sm" value="<?php echo $acargo_price; ?>" />
                                                        <input type="hidden" name="cargo-price-id" value="<?php echo $value['id']; ?>" />
                                                        <input type="hidden" name="cargo-agency-id" value="<?php echo $this->common->default_cargo() ?>" />
                                                        <input type="hidden" name="agency-price-type" value="cargo_prices" />
                                                    </div>
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i></button>
                                                        <a href="#" class="btn btn-default btn-sm"><i class="fa fa-times fa-fw"></i></a>
                                                    </div>
                                                </form>
                                            </td>
                                            <td >
                                            	<input type="checkbox" name="cargo_referral[]" value="<?php echo $value['id']; ?>" <?php if($value['referral'] != '') { echo 'checked="checked"'; } ?> />
                                            </td>
                                            <td>
                                            	<a href="#" class="btn btn-danger btn-xs delete_btn" data-id="<?php echo $value['id']?>" data-table="q_business_specific"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    <?php $countt++; } ?>
                                    
                                    
                                    
                                </tbody>
                            </table>
                            <?php } ?>                            
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
                           
    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="countryModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                        
                            <h1>Countries</h1>
                            <hr class="star-primary">
                            
                            <p class="text-right">
                            	<a href="#zoneMultiModal" class="btn btn-primary" data-toggle="modal" data-controls-modal="zoneMultiModal">Zone Multiplier</a>
                            </p>
                            
							<?php
                                if(count($countries) > 0) {
                            ?>
                            <table class="table table-hover mydataTbx">
                                <thead>
                                    <tr>
                                        <th width="75%">Name</th>
                                        <th width="25%">Zone</th>
                                        <th>Referral</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    <?php foreach($countries as $r=>$value){
                                        $acargo_price = 0;
                                        if(isset($country_prices[$value['country_id']])) {
                                            $acargo_price = $country_prices[$value['country_id']];
                                        }
                                    ?>
                                    
                                        <tr>
                                            <td><?php echo $value['short_name']; ?></td>
                                            <td >
                                            <span class="price-text" data-id="<?php echo $value['country_id'] ?>" title="click to edit"><?php echo $acargo_price; ?></span>
                                            <form class="price_form hidden">
                                                <div class="form-group">
                                                    <select class="form-control" name="cargo-price">
                                                    	<option value="0">Select</option>
														<?php
                                                            for($i = 1; $i < 6; $i++){
                                                                $rate = 0;
                                                                
																echo '<option value="'.$i.'"';
																if($i == $acargo_price){
                                                                    echo ' selected="selected"';
                                                                }
																
																echo '>Zone '.$i.'</option>';
															}
                                                        ?>
                                                        
                                                        
                                                    </select>
                                                    <input type="hidden" name="cargo-price-id" value="<?php echo $value['country_id']; ?>" />
                                                    <input type="hidden" name="cargo-agency-id" value="<?php echo $this->common->default_cargo() ?>" />
                                                    <input type="hidden" name="agency-price-type" value="country_prices" />
                                             
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                                    <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                                </div>
                                            </form>
                                            <?php /*?><form class="price_form hidden">
                                                <div class="form-group">
                                                    <input type="text" name="cargo-price" class="form-control input-sm" value="<?php echo $acargo_price; ?>" />
                                                    <input type="hidden" name="cargo-price-id" value="<?php echo $value['country_id']; ?>" />
                                                    <input type="hidden" name="cargo-agency-id" value="<?php echo $this->common->default_cargo() ?>" />
                                                    <input type="hidden" name="agency-price-type" value="country_prices" />
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                                    <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                                </div>
                                            </form><?php */?>
                                            </td>
                                            <td>
                                            	<input type="checkbox" name="country_referral[]" value="<?php echo $value['country_id']?>" <?php if(isset($country_referral[$value['country_id']])) { echo 'checked="checked"'; } ?> />
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    
                                </tbody>
                            </table>
                            <?php } ?>                            
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->
                     
    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="transportationModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                        
                            <h1>Employee</h1>
                            <hr class="star-primary">
                            
							<?php
                                if(count($cargo) > 0) {
                            ?>
                            <table class="table table-hover mydataTbx text-left">
                                <thead>
                                    <tr>
                                        <th width="75%">Name</th>
                                        <th width="25%">Multiplier</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    <?php
									$count = 0;
									foreach($transportations as $r){
										$acargo_price = 0;
				                        if(isset($transportation_prices[$count])) {
                                            $acargo_price = $transportation_prices[$count];
                                        }
                                    ?>
                                    
                                        <tr>
                                            <td><?php echo $r; ?></td>
                                            <td >
                                            <span class="price-text" data-id="<?php echo $count ?>" title="click to edit"><?php echo $acargo_price; ?></span>
                                            <form class="price_form hidden">
                                                <div class="form-group">
                                                    <input type="text" name="cargo-price" class="form-control input-sm" value="<?php echo $acargo_price; ?>" />
                                                    <input type="hidden" name="cargo-price-id" value="<?php echo $count; ?>" />
                                                    <input type="hidden" name="cargo-agency-id" value="<?php echo $this->common->default_cargo() ?>" />
                                                    <input type="hidden" name="agency-price-type" value="transportation_prices" />
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                                    <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                                </div>
                                            </form>
                                            </td>
                                        </tr>
                                    <?php
										$count++;
									} ?>
                                    
                                    
                                    
                                </tbody>
                            </table>
                            <?php } ?>                            
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->

    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="deductibleModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                        
                            <h1>Deductible <a href="#addDedModal" class="btn btn-primary" data-toggle="modal" data-controls-modal="addDedModal"><i class="fa fa-plus"></i></a></h1>
                            <hr class="star-primary">
       						
                            <?php
								if(count($deductibles) > 0){
                            ?>
                            <table class="table table-hover mydataTbx">
                                <thead>
                                    <tr>
                                        <th width="50%">Deductible</th>
                                        <th width="40%">Multiplier</th>
                                        <th width="10%">Default</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    <?php
									$count = 0;
									foreach($deductibles as $r=>$value){

                                    ?>
                                    
                                        <tr>
                                            <td>
                                            <span class="price-text" data-id="<?php echo $value['id'] ?>" title="click to edit"><?php echo $value['deductible']; ?></span>
                                            <form class="price_form hidden">
                                                <div class="form-group">
                                                    <input type="text" name="cargo-price" class="form-control input-sm input-currency" value="<?php echo $value['deductible']; ?>" />
                                                    <input type="hidden" name="cargo-price-id" value="<?php echo $value['id']; ?>" />
                                                    <input type="hidden" name="cargo-agency-id" value="<?php echo $value['id'] ?>" />
                                                    <input type="hidden" name="db-name" value="deductibles" />
                                                    <input type="hidden" name="agency-price-type" value="deductible" />
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                                    <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                                </div>
                                            </form>
											
											</td>
                                            <td >
                                            <span class="price-text" data-id="<?php echo $value['id'] ?>" title="click to edit"><?php echo $value['rate']; ?></span>
                                            <form class="price_form hidden">
                                                <div class="form-group">
                                                    <input type="text" name="cargo-price" class="form-control input-sm" value="<?php echo $value['rate']; ?>" />
                                                    <input type="hidden" name="cargo-price-id" value="<?php echo $value['id']; ?>" />
                                                    <input type="hidden" name="cargo-agency-id" value="<?php echo $value['id'] ?>" />
                                                    <input type="hidden" name="db-name" value="deductibles" />
                                                    <input type="hidden" name="agency-price-type" value="rate" />
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                                    <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                                </div>
                                            </form>
                                            </td>
                                            <td>
                                            	<input type="checkbox" name="ded_default[]" value="<?php echo $value['id']?>" data-agency="<?php echo $this->common->default_cargo(); ?>" <?php if($value['not_default'] == 'N') { echo 'checked="checked"';}?> />
                                            </td>
                                            <td>
                                            	<a href="#" class="btn btn-danger btn-xs delete_btn" data-id="<?php echo $value['id']?>" data-table="deductibles"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    <?php
										$count++;
									} ?>
                                    
                                    
                                    
                                </tbody>
                            </table>
                            <?php }
							
								else{
									echo '<p class="text-center text-muted">No deductibles</p>';	
								}
							
							?>
                            
                          
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->




	<!-- Modal -->
    <div class="modal fade" id="addBulkModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Upload Bulk Occupation</h4>
          </div>
        
    
            <div class="hidden">
                <form id="myCsvForm" action="<?php echo base_url().'webmanager/ratings/upload' ?>" method="post" enctype="multipart/form-data">
                     <input type="file" size="99999" id="mycsv_input" name="mycsv_input" onchange="$('#myCsvForm').submit();">
                     <input type="hidden" class="bulk_id" name="bulk_id" />
                     <input type="submit" class="hidden" value="Ajax File Upload">
                 </form>                                                   
            </div>     
               

              <div class="modal-body">

                <div class="form-group text-center">
                    <a href="javascript:;" class="btn btn-primary btn-lg upload_file_btn" onclick="$('#mycsv_input').click();"><i class="fa fa-cloud-upload"></i> Select File to Upload (CSV)</a>
                </div>
                
                <div class="form-group" style="margin-top: 15px;">
                    <div class="text-danger bulk_error_mess text-center hidden"></div>
                </div>

                <div class="form-group">
                    <div id="progress" class="progress-csv" style="display: none;">
                        <div id="bar" class="bar-csv"></div>
                        <div id="percent" class="percent-csv">0%</div >
                    </div>
                </div>

                <div class="form-group">
                    <div id="file_up_text" style="display: none;"><div id="message"><a href=""></a></div></div>
                </div>

                <div class="form-group" style="border-top: 1px solid #eee; margin-top: 15px; padding-top: 15px;">
                	<p class="text-center">Example File (.csv file extension)</p>
                    <img src="<?php echo base_url().'assets/img/occupation-template.jpg' ?>" alt="" class="img-responsive thumbnail" />
                </div>

                <div class="form-group">
                	<a href="<?php echo base_url().'assets/img/occupation-template.csv' ?>" target="_blank">Download sample file</a>
                </div>


              </div>

        </div>
      </div>
    </div>

	<!-- Modal -->
    <div class="modal fade" id="addCargoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">New Occupation</h4>
          </div>
          <form id="add_cargo_form">
              <div class="modal-body">
                <div class="form-group">
                    <label>Occupation Name</label>
                    <input type="text" class="form-control" name="name" />
                </div>
                <div class="form-group">
                    <label>Multiplier</label>
                    <input type="text" class="form-control" name="price" />
                </div>
                <div class="form-group">
                    <label>Referral</label><br />
                    <input type="checkbox" name="referral" value="Yes"/>
                </div>
              </div>
              <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
          </form>
        </div>
      </div>
    </div>


	<!-- Modal -->
    <div class="modal fade" id="addDedModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">New Deductible</h4>
          </div>
          <form id="add_ded_form">
              <div class="modal-body">
                <div class="form-group">
                    <label>Deductible</label>
                    <input type="text" class="form-control input-currency" name="name" />
                </div>
                <div class="form-group">
                    <label>Multiplier</label>
                    <input type="text" class="form-control" name="price" />
                </div>
              </div>
              <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
          </form>
        </div>
      </div>
    </div>


	<!-- Modal -->
    <div class="modal fade" id="baseCurrencyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Base Currency</h4>
          </div>
          <form id="base_curr_form">
          		<input type="hidden" name="id" value="<?php echo $this->common->default_cargo(); ?>" />
              <div class="modal-body">
                  <div class="form-group">
                    <label for="currency">Currency</label>
                    <select class="form-control" name="currency" id="currency" data-live-search="true">
                      <option value="">Select Currency</option>
                      <?
                      foreach($currencies as $r=>$value){
    
                          echo '<option value="'.$value['currency'].'"';
                          if($base_currency == $value['currency']){
                              echo ' selected="selected"';											  	
                          }
                          echo '>'.$value['name'].'</option>';
                                
                      }?>
                      
                    </select>                                            
                  </div>    
                
              </div>
              <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
          </form>
        </div>
      </div>
    </div>
    
    
	<!-- Modal -->
    <div class="modal fade" id="zoneMultiModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Zone Multiplier</h4>
          </div>
          <form id="zone_multi_form">
          		<input type="hidden" name="id" value="<?php echo $this->common->default_cargo(); ?>" />
              <div class="modal-body">
              	<?php
					for($i = 1; $i < 6; $i++){
						$rate = 0;
						if(isset($zone_multiplier[$i])){
							$rate = $zone_multiplier[$i];
						}
				?>
              	<div class="row form-group">
                    <div class="col-sm-6">
                        <label>Zone <?php echo $i; ?></label>
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="zone[]" value="<?php echo $rate; ?>" />
                    </div>
                </div>
                <?php } ?>
              </div>
              <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
          </form>
        </div>
      </div>
    </div>