<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>About Us Page</h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage About Us Page</li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                 <?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading">

                        <div class="panel-title">

                            <h4>About Us <a href="<?php echo base_url().'aboutus' ?>" target="_blank" class="btn btn-primary pull-right btn-xs"><i class="fa fa-search"></i> Preview</a>
</h4>

                        </div>
                        <div class="clearfix"></div>

                    </div>

                    <div class="panel-body">
                        <form id="about_form">
                            <div class="form-group">
                                <div id="summernot"><?php // echo $about_page[0]['content'] ?>
                                
                                	<?php
									
										if(count($about_us) > 0){
											foreach($about_us as $r=>$value){

												echo '<'.$value['html_tag'].' class="'.$value['classes'].'" data-pk="'.$value['id'].'">';
												
												echo $value['content'];
												
												echo '</'.$value['html_tag'].'>';
											  }

										}
									?>
                                
                                
                                

        <hr>
    
    	<div class="row co-founders">
        
            <h2 class="text-center">Meet the Co-founders</h2>        
        
            <div class="col-sm-2"></div>
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="<?php echo base_url() ?>assets/img/Nga-profile-pic.jpg" class="img-responsive img-circle" alt="">

					<?php
						//name
						$nguys_pos = $nguyen[0];

						echo '<'.$nguys_pos['html_tag'].' class="'.$nguys_pos['classes'].'" data-pk="'.$nguys_pos['id'].'">';
						echo $nguys_pos['content'];
						echo '</'.$nguys_pos['html_tag'].'>';
					?>


					<?php
						//position
						$nguys_pos = $nguyen[1];

						echo '<'.$nguys_pos['html_tag'].' class="'.$nguys_pos['classes'].'" data-pk="'.$nguys_pos['id'].'">';
						echo $nguys_pos['content'];
						echo '</'.$nguys_pos['html_tag'].'>';
					?>
                    


                    <ul class="list-inline social-buttons">
                        <li><a href="http://au.linkedin.com/pub/nga-nguyen/13/b1b/0" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </li>
                        <li><a href="http://twitter.com/Nga_Van_Nguyen" target="_blank"><i class="fa fa-twitter"></i></a>
                        </li>
                    </ul>
                    <br/>
					<?php
						//desc
						$nguys_desc = $nguyen[2];
						echo '<'.$nguys_desc['html_tag'].' class="'.$nguys_desc['classes'].'" data-pk="'.$nguys_desc['id'].'">';
						echo $nguys_desc['content'];
						echo '</'.$nguys_desc['html_tag'].'>';
					?>
                </div>
            </div> 
            
            <div class="col-sm-4">
                <div class="team-member">
                    <img src="<?php echo base_url() ?>assets/img/geoff-profile-pic.jpg" class="img-responsive img-circle" alt="">
					<?php
						//name
						$stooke_name = $stooke[0];
						echo '<'.$stooke_name['html_tag'].' class="'.$stooke_name['classes'].'" data-pk="'.$stooke_name['id'].'">';
						echo $stooke_name['content'];
						echo '</'.$stooke_name['html_tag'].'>';
					?>


					<?php
						//position
						$stooke_pos = $stooke[1];
						echo '<'.$stooke_pos['html_tag'].' class="'.$stooke_pos['classes'].'" data-pk="'.$stooke_pos['id'].'">';
						echo $stooke_pos['content'];
						echo '</'.$stooke_pos['html_tag'].'>';
					?>

                    <ul class="list-inline social-buttons">
                        <li><a href="http://au.linkedin.com/pub/geoff-stooke/64/ba6/910" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </li>
                        <li><a href="http://twitter.com/GeoffStooke" target="_blank"><i class="fa fa-twitter"></i></a>
                        </li>
                    </ul>
                    <br/>
					<?php
						//desc
						$stooke_desc = $stooke[2];
						echo '<'.$stooke_desc['html_tag'].' class="'.$stooke_desc['classes'].'" data-pk="'.$stooke_desc['id'].'">';
						echo $stooke_desc['content'];
						echo '</'.$stooke_desc['html_tag'].'>';
					?>
                </div>

            </div> 
            <div class="col-sm-2"></div>
            
        
        </div><!--row co-founders-->
                                

        <hr>
    
    	<div class="row co-founders advisor_holder">
        
            <h2 class="text-center">Advisory Board <a class="btn btn-default add_advisor_btn"><i class="fa fa-plus"></i> Add</a></h2>
            
            
            <?php
				if(count($advisors) > 0){
				
					
					foreach($advisors as $r=>$value){
						
						if($value['name'] != '' && $value['name'] != 'nguyen' && $value['name'] != 'stooke'){
					
							echo '
							<div class="col-sm-4">
								<a href="#" class="delete_editable_el" title="remove" style="display: none;"><i class="fa fa-times pull-right"></i></a>
								<div class="team-member">';	
					
					
							
							echo '<'.$value['html_tag'].' class="'.$value['classes'].'" data-pk="'.$value['id'].'">';
							echo $value['content'];
							echo '</'.$value['html_tag'].'>';
							
							$advi_title = $this->master_model->getRecords('pages_content',array('html_tag'=> 'p', 'page_type'=> 'aboutus', 'name'=>$value['name'], 'classes'=>'text-muted'));
							
							if(count($advi_title) > 0){
								foreach($advi_title as $at=>$at_value){
									echo '<'.$at_value['html_tag'].' class="'.$at_value['classes'].'" data-pk="'.$at_value['id'].'">';
									echo $at_value['content'];
									echo '</'.$at_value['html_tag'].'>';
								}
							}
							
							$advi_desc = $this->master_model->getRecords('pages_content',array('html_tag'=> 'p', 'page_type'=> 'aboutus', 'name'=>$value['name'], 'classes'=>''));
							
							if(count($advi_desc) > 0){
								foreach($advi_desc as $ad=>$ad_value){
									echo '<'.$ad_value['html_tag'].' class="'.$ad_value['classes'].'" data-pk="'.$ad_value['id'].'">';
									echo $ad_value['content'];
									echo '</'.$ad_value['html_tag'].'>';
								}
							}
							
								
						echo '
							</div>
						</div>';

							
							
						}//if an adviser
						
					}//foreach advisor
					
				}
			?>       
        
            <div class="col-sm-4 advisor_col hidden" id="advisor_col">
            	<a href="#" class="delete_editable_el" title="remove" style="display: none;"><i class="fa fa-times pull-right"></i></a>
                <div class="team-member">
                	<h4>Name</h4>
                	<p class="text-muted">Title</p>
                	<p>Text Description</p>
                </div>
            </div>
        
        </div><!--row co-founders-->
                                
                                
                                
                                </div>
                                <span class="text-danger" style="display: none;">This field is required.</span>
                            </div>
                            <div class="form-group hidden">
                                <button type="submit" class="btn btn-sky">Submit</button>
                            </div>
                        </form>
                        
                            
                    </div>
                </div>
                
                <a href="<?php echo base_url().'aboutus' ?>" target="_blank" class="btn btn-primary btn-block"><i class="fa fa-search"></i> Preview About Us Page</a>
                
            </div>

        </div><!--.row-->


    
</div>

