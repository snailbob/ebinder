<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">




    <div class="row">

        <div class="col-lg-12">

            <div class="page-title">

                <h3>Update Emails</h3>

                <ol class="breadcrumb">

                    <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                    <li class="active">Admin Update Emails</li>



                </ol>

            </div>

        </div>

        <!-- /.col-lg-12 -->

    </div>

    <!-- /.row -->

    <!-- end PAGE TITLE AREA -->





    



    <!-- Form AREA -->

    <div class="row">

        <div class="col-lg-12">

            <?php 

            if($this->session->flashdata('success')!="")

            {

            ?>

            <div class="alert alert-success alert-dismissable">

            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>

            <strong>Success! </strong><?php echo $this->session->flashdata('success'); ?></div>

            <?php    

            } 

            if($this->session->flashdata('error')!="")

            {

            ?>

            <div class="text-red">

            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            <strong>Error: </strong><?php echo $this->session->flashdata('error'); ?></div>

            <?php

            } 

            ?>

            <div class="panel panel-default">

                <div class="panel-heading">

                    <div class="panel-title">

                        <h4>Update Admin Emails</h4>

                    </div>

                    <div class="clearfix"></div>

                </div>

                    <div class="panel-body">

                        

            <form action='' name="frm-add-cc" id="frm-add-cc" method='post' class="form-horizontal" role="form" validate>

            

            <div class="form-group">

                <label for="firstname" class="col-md-2 control-label">Contact Email</label>

                <div class="col-md-10">

                    <input type="email" class="form-control" name="contact_email" id="contact_email" value="<?php echo $admin_email[0]['contact_email']; ?>"  required data-msg-required="Please enter first name"><?php echo form_error('contact_email'); ?>

                </div>

            </div>

            <div class="form-group">

                <label for="lastname" class="col-md-2 control-label">Info Email</label>

                <div class="col-md-10">

                <input type="email" class="form-control" name="info_email" id="info_email" value="<?php echo $admin_email[0]['info_email']; ?>" required data-msg-required="Please enter last name"><?php echo form_error('cc_lastname'); ?>

                </div>

            </div>

            <div class="form-group">

                <label for="organisation" class="col-md-2 control-label">Support Email</label>

                <div class="col-md-10">

                <input type="email" class="form-control" name="support_email" id="support_email"  value="<?php echo $admin_email[0]['support_email']; ?>" required data-msg-required="Please enter email"><?php echo form_error('support_email'); ?>

                </div>

            </div>

                          

                            <div class="form-group">

                                <label class="col-md-2 control-label"></label>

                                <div class="col-md-10">

                                    <button type="submit" class="btn btn-primary" name="admin_update_email" id="admin_update_email">Submit</button>

                                </div>

                            </div>

                       </form>									

                    </div>

                </div>

            </div>

    </div><!--.row-->



    
</div>


