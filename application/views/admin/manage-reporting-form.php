<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

					<?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">
                <div class="panel panel-default">
                	<div class="panel-heading">
						<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>

					<div class="panel-body">

                      <form class="generate_report_form complete_loading_content hidden">
                          <div class="modal-body">
                             <div class="row">

                                <div class="col-sm-5 form-group">
                                    <label for="shipment_date">From</label>
                                    <input type="text" class="form-control date_from" id="date_from" name="date_from" value="<?php echo date('d-m-Y') ?>" />
                                    <input type="hidden" class="report-type" name="type" value="" />
                                </div>

                                <div class="col-sm-5 form-group">
                                    <label for="date_to">To</label>
                                    <input type="text" class="form-control date_to" id="date_to" name="date_to" value="" />
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label>New template? </label><br />
                                    	<input type="checkbox" name="new_template" data-on-text="Yes" data-off-text="No" data-handle-width="auto" checked="checked">

                                    </div>
                                </div>
                            </div>


                            <div class="row row-select-template" style="display: none;">
                                <div class="col-sm-12">
                                    <div class="form-group">

                                        <label>Select template</label>
                                        <input type="radio" class="hidden" name="report_template" value="">

                                        <table class="table table-hover">
                                          <tbody>
                                            <?php
                                                if(count($report_templates) > 0){
                                                    foreach($report_templates as $r=>$value){
                                                        $tdata = unserialize($value['details']);
                                                        ?>
                                                        <tr>
                                                            <td>
                                                            <a href="#" class="btn btn-danger pull-right delete_btn" data-id="<?php echo $value['id']?>" data-table="report_template">
                                                            	<i class="fa fa-trash"></i>
                                                            </a>
                                                            <div class="radio" style="width: 90%">
                                                              <label>
                                                                <input type="radio" name="report_template" value="<?php echo $value['id'] ?>" data-inputs='<?php echo json_encode($tdata) ?>'>
                                                                <?php echo $value['name']; ?>
                                                              </label>
                                                            </div>
                                                              </td>
                                                          </tr>
                                                        <?php
                                                    }
                                                }
                                            ?>


                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                            </div>

                            <div class="row row-new-template">

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Template Name</label>
                                        <input type="text" class="form-control" name="template_name" value="" />
                                    </div>
                                </div>

                            </div>



                            <div class="row report-attributes">
                            	<div class="col-sm-12">
                                	<label>Data attributes to export</label>
                                </div>
                            	<?php foreach($report_header as $r=>$value) {?>
                                <div class="col-md-4 col-lg-3">
                                	<div class="form-group">
                                        <div class="btn-group btn-group-justified" data-toggle="buttons">
                                          <label class="btn btn-default" style="padding: 10px; text-align: left">
                                            <span class="pull-right btn-xs">Add</span>
                                            <input type="checkbox" name="attributes[]" value="<?php echo $r ?>">
                                            <?php /*?><i class="fa <?php echo $report_icons[$r] ?> fa-2x"></i><br /><?php */?>
                      											<?php echo $value ?>
                                          </label>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>

                            </div>
                          </div>


                          <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Generate</button>
                          </div>
                    	</form>
                    </div>

                </div>

            </div>

        </div><!--.row-->

</div>



<!-- Modal -->
<div class="modal fade" id="selectTemplateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Select template</h4>
          </div>

          <div class="modal-body">


            <div class="row row-select-template">
                <div class="col-sm-12">
                    <div class="form-group">

                        <input type="radio" class="hidden" name="report_template" value="">

                        <table class="table table-hover">
                          <tbody>
                            <?php
                                if(count($report_templates) > 0){
                                    foreach($report_templates as $r=>$value){
                                        $tdata = unserialize($value['details']);
                                        ?>
                                        <tr>
                                            <td>
                                            <a href="#" class="btn btn-danger pull-right delete_btn" data-id="<?php echo $value['id']?>" data-table="report_template" data-alert="no">
                                            	<i class="fa fa-trash"></i>
                                            </a>
                                            <div class="radio" style="width: 90%">
                                              <label>
                                                <input type="radio" name="report_template" value="<?php echo $value['id'] ?>" data-inputs='<?php echo json_encode($tdata) ?>'>
                                                <?php echo $value['name']; ?>
                                              </label>
                                            </div>
                                              </td>
                                          </tr>
                                        <?php
                                    }
                                }
                            ?>


                            </tbody>
                        </table>


                    </div>
                </div>
            </div>

          </div>
    </div>
  </div>
</div>
