<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3><?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active"><?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                 <?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading">

                        <div class="panel-title">

                            <h4><?php echo $title; ?></h4>

                        </div>
                        <div class="clearfix"></div>

                    </div>

                        <div class="panel-body">
                           <p class="text-center">


                           		<a href="#" onclick="$('#myfile').click(); return false;" title="click to update avatar">

                                <img src="<?php echo $this->common->avatar($this->session->userdata('id'), 'customer') ?>" class="img-page-preview img-circle img-thumbnail" alt="avatar" style="width: 146px;"/>

                              </a><br /><span class="small text-muted">150px minimum size recommended</span>

                           </p> 



                            <form id="customer_details_form" novalidate="novalidate" class="text-left">
                                <input type="hidden" name="id" value="<?php echo $user['id'] ?>" />
                            

                                <div class="row row-prime-contact">


                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="first" placeholder="First Name" value="<?php echo $user['first_name']?>" required="required"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="last" placeholder="Last Name" value="<?php echo $user['last_name']?>" required="required"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-prime-contact">

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="text" placeholder="Position" class="form-control" name="domain" value="<?php echo $user['domain']?>"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <input type="email" placeholder="Email" class="form-control" name="email" value="<?php echo $user['email']?>" readonly="readonly"/>
                                        </div>
                                    </div>

                                </div>


                                
                                <div class="row">
                                
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Country Calling Code</label>
                                            <select class="form-control country_code_dd" name="country_code" data-live-search="true" required="required">
                                                <option value="" data-code="">Select..</option>
                                                <?php
                                                    foreach($countries as $r=>$mc){
                                                        echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';

                                                        if($user['country_id'] == $mc['country_id']){
                                                            echo ' selected="selected"';
                                                        }
                                                                    

                                                        echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                
                                    <div class="col-sm-6">
                                        <div class="form-group mobile_input">
                                            <label class="control-label">&nbsp;</label>
                                              <input type="number" class="form-control" placeholder="Contact Number" name="mobile" autocomplete="nope" value="<?php echo $user['calling_digits'] ?>" required>

                                            <input type="hidden" name="country_short" value="<?php echo $user['calling_code'] ?>"/>
                                        </div>
                                    </div>
                                
                                </div><!--phone number-->
                                                                             
                                


                                <div class="form-group text-right">
                                    <a href="<?php echo base_url().'webmanager/dashboard'?>" class="btn btn-grey">Cancel</a>
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            
                            
                            </form>






                        </div>
            
                

                    </div>

                </div>


        </div><!--.row-->


    
</div>


 <div class="row hidden">
  <form id="myFormAvatar" action="<?php echo base_url().'settings/update_avatar' ?>" method="post" enctype="multipart/form-data">
       <input type="file" size="60" id="myfile" name="myfile" class="myfile-profile" onchange="$('#myFormAvatar').submit();">
       <input type="hidden" id="avatar_name" value="">
       <input type="hidden" id="pagetype" value="avatar">
       <input type="submit" class="hidden" value="Ajax File Upload">
   </form>                                                   
  </div>



<!-- Modal -->
<div class="modal fade" id="cropImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
    <p class="lead">Crop</p>
        
        <div class="bootstrap-modal-cropper" id="customer_img"><img src="<?php echo base_url().'assets/img/avatar/background.png' ?>" class="img-responsive"/></div>
  
        

      </div>
      <div class="modal-footer">
        <a class="btn btn-primary sav_crop">Save changes</a>
      </div>
    </div>
  </div>
</div>