<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">
                <p class="text-right hidden">
                </p>
                <div class="panel panel-default">


                	<div class="panel-heading">
                      <div class="dropdown pull-right">
                        <button class="btn btn-default" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          Filter
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu policy_filter_btn" aria-labelledby="dLabel">
                          <li class="active">
                            <a href="#" data-value="">All</a>
                          </li>
                          <li>
                            <a href="#" data-value="Quote_filter">Quote</a>
                          </li>
                          <li>
                            <a href="#" data-value="Policy_filter">Policy</a>
                          </li>
                        </ul>
                      </div>



                    	<div  class="panel-title">
                            <h4>Manage <?php echo $title; ?></h4>
						</div>
                    </div>


                        <?php if(count($format_referral) > 0) { ?>
                        <div class="table_search">
                            <input type="text" class="form-control input_table_search" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>




						<div class="table-responsivex">

                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <th>Quote ID</th>
                              <th>Policy ID</th>
                              <th>Name</th>
                              <th>Status</th>
                              <th>Expiry Date</th>
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>

                        <?php
							$count = 1;
							foreach($format_referral as $u=>$value){


						?>

                            <tr>

                              <td class="hidden"><?php echo $count; ?></td>
                              <td>
                                <?php echo $value['quote_id']; ?>
                                <span class="hidden"><?php echo ($value['bound'] == 'Y') ? 'Policy_filter' : 'Quote_filter' ?></span>

                              </td>

                              <td><?php echo $value['policy_id']; ?></td>

                              <td><?php echo $value['customer']['name'] ?></td>

                              <td><?php echo $value['status'] ?></td>

                              <td><?php echo date_format(date_create($value['date_expires']), 'd-m-Y').' ('.$value['days_remaining'].'d left)' ?></td>

                              <td>

                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                        <?php if($value['days_remaining'] < 61) { ?>
                                        <li><a href="javascript:;" class="renewal-advice" data-id='<?php echo $value['id'];?>'>Renewal Advice Email</a></li>
                                        <?php } ?>
                                        <li><a href="javascript:;" class="show-biz-response" data-id='<?php echo $value['id'];?>'>Insurance details</a></li>
                                        <li><a href="mailto:<?php echo $value['customer']['email'] ?>">Contact Now</a></li>
                                        <li><a href="#" class="delete_btn" data-id="<?php echo $value['id'] ?>" data-table="biz_referral">Delete</a></li>
                                      </ul>
                                    </div>


                              </td>
                            </tr>

                        <?php
								$count++;
							}
						?>



                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';
						}?>

                    <div class="panel-body hidden">

                    </div>

                </div>

            </div>


        </div><!--.row-->



</div>





<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $singular_title; ?> Information</h4>
      </div>
      <div class="modal-body">
       	<div class="customer_info">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="formModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

                            <h1><?php echo $singular_title; ?> Form Inputs</h1>
                			<hr />


							<div class="the_referral_response text-left">
                            	theview

                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->
