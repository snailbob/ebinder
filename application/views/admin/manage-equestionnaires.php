<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>
                        <li><a href="<?php echo base_url()?>webmanager/products" class="preloadThis">Products</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">


                <div class="panel panel-default">

                
                	<div class="panel-heading">
                    	<a href="<?php echo base_url().'webmanager/eproducts/manage' ?>" class="btn btn-default pull-right">&larr; Back</a>
						<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>
                    
                    <div class="panel-body">
                    
<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#questionnaire" aria-controls="profile" role="tab" data-toggle="tab">Questionnaire</a></li>
    <li role="presentation"><a href="#claims" aria-controls="messages" role="tab" data-toggle="tab">Claims</a></li>
    <li role="presentation"><a href="#declarations" aria-controls="settings" role="tab" data-toggle="tab">Declarations</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content" style="padding-top: 15px;">
    <div role="tabpanel" class="tab-pane active" id="questionnaire">
			<?php echo $questionnaires ?>
    
    </div>
    <div role="tabpanel" class="tab-pane" id="claims">
			<?php echo $claims ?>
    
    </div>
    <div role="tabpanel" class="tab-pane" id="declarations">
			<?php echo $declarations ?>
    
    </div>
  </div>

</div>                    
                    
                    </div>


                        
                    <div class="panel-body hidden">

                    </div>

                </div>

            </div>


        </div><!--.row-->


    
</div>





<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $singular_title; ?> Information</h4>
      </div>
      <div class="modal-body">
       	<div class="customer_info">
        
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="formModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                            <h1><?php echo $singular_title; ?></h1>
                			<hr />

                    
                            <form id="question_form" class="text-left">
                            	<input type="hidden" name="id" />
                            	<input type="hidden" name="broker_id" name="0" />
                            	<input type="hidden" name="product_id" value="<?php echo $product_id ?>" />
                                <div class="form-group">
                                    <label>Question</label>
                                    <input type="text" class="form-control" name="question" />
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            
                            
                            </form>

	
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->      