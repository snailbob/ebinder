<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">

                <div class="panel panel-default">

                
                	<div class="panel-heading">
                        <a href="#addAgencyModal" class="hidden btn btn-primary pull-right <?php echo ($this->common->default_cargo() == '6') ? 'hidden' :''?>" data-toggle="modal" data-controls-modal="#addAgencyModal"><i class="fa fa-plus"></i> <?php echo $singular_title; ?></a>
                    
                    
                    	<div  class="panel-title">
                            <h4>Manage <?php echo $title; ?></h4>
						</div>
                    </div>

                        <?php if(count($q_products) > 0) { ?>
                        <div class="table_search">
                            <input type="text" class="form-control input_table_search" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>

    

                        
						<div class="table-responsivex">
                        
                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th>Name</th>
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>                        
                        
                        <?php
							foreach($q_products as $u=>$user){
								if(in_array($user['id'], $prod_id)){ 
						?>
                        
                            <tr>
                             
                              <td><?php echo $user['product_name'] ?></td>


                              <td>
                                                                  
                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">

                                        <li><a href="<?php echo base_url().'webmanager/ratings/product/'.$user['id'] ?>" data-id="<?php echo $user['id']?>" data-table="brokerage">Manage</a></li>
                                      </ul>
                                    </div>                              
                              
                              
                              </td>
                            </tr>
                        
                        <?php
								}
								
							}
						?>


                          
                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->
                          
                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';	
						}?>
                        
                    <div class="panel-body hidden">

                    </div>

                </div>

            </div>


        </div><!--.row-->


    
</div>





<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $singular_title; ?> Information</h4>
      </div>
      <div class="modal-body">
       	<div class="the-info">
        
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="addAgencyModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="modal-body">
                        
                            <h1><?php echo $singular_title; ?></h1>
                			<hr />

                    
                            <form id="rating_product_form" novalidate="novalidate" class="text-left">
                            	<input type="hidden" name="id" />
                            
                            	<div class="row">
                                	<div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Select</label>
                                            
                                            <div class="row">
												<?php foreach($q_products as $r=>$value) {
													?>
                                                <div class="col-md-3 col-xs-6">
                                                    <div class="form-group">
                                                        <div class="btn-group btn-group-justified" data-toggle="buttons">
                                                          <label class="btn <?php echo (in_array($value['id'], $prod_id)) ? 'active btn-primary' : 'btn-default' ?>" style="padding-top: 25px;padding-bottom: 25px;">
                                                            <input type="checkbox" name="product_id[]" value="<?php echo $value['id'] ?>" <?php echo (in_array($value['id'], $prod_id)) ? 'checked="checked"' : '' ?>>
                                                            <?php echo $value['product_name'] ?>
                                                          </label>
                                                        </div>
                                                    </div>                                
                                                </div>
                                                <?php } ?>
                                            
                                            
                                            
                                            </div>
                                            
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                                            
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            
                            
                            </form>

	
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
        
    </div><!--portfolio-modal-->      
    
    

    <!-- Modal -->
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
            Type 'DELETE' to confirm
            
            <form id="delete_form">
                <div class="form-group">
                    <input type="text" name="delete" class="form-control" />
                    <input type="hidden" name="delete2" id="delete2" class="form-control" value="DELETE"/>
                    <input type="hidden" name="id" class="form-control" value="DELETE"/>
                
                </div>
                <div class="form-group text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
            
            
            </div>
            
          </div>

        </div>
      </div>
    </div>    
    