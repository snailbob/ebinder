<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage Outgoing Emails</h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage Outgoing Emails</li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                 <?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading">
						<div  class="panel-title">
                            <h4>Outgoing Emails</h4>
						</div>
                    </div>

                    <div class="panel-body">

                        <?php if(count($emails) > 0) { ?>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>
                        
						<div class="table-responsivex">
                        
                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th class="visible-xs visible-sm">Email Info</th>
                              <th class="hidden-xs hidden-sm">Emailer</th>
                              <th class="hidden-xs hidden-sm">Subject</th>
                              <th class="hidden-xs hidden-sm">Content</th>
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>                        
                        
                        <?php
							foreach($emails as $e=>$email){
						?>
                        
                            <tr>
                              <td class="visible-xs visible-sm">
							  	<b class="text-primary"><?php echo $email['name'] ?></b><br /><br />
							  	<span class="text-muted">
								Subject: <?php echo $email['subject'] ?><br /><br />
								Message: <?php echo $email['content'] ?><br /><br />
                                
                                </span>
                              
                              </td>
                              <td class="hidden-xs hidden-sm"><?php echo $email['name'] ?></td>

                              <td class="hidden-xs hidden-sm"><?php echo $email['subject'] ?></td>

                              <td class="hidden-xs hidden-sm"><?php echo $email['content'] ?></td>

                              <td>
                                                                  
                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                        <li><a href="<?php echo base_url().'webmanager/emails/update/'.$email['id'] ?>">Update Email</a></li>
                                      </ul>
                                    </div>                              
                              
                              
                              </td>
                            </tr>
                        
                        <?php
								
							}
						?>


                          
                          </tbody>
                        </table>
                        </div><!--end of table responsive-->
                          
                        <?php } else{
							echo '<p class="lead">No Users</p>';	
						}?>
                        
                        

                    </div>

                </div>

            </div>


        </div><!--.row-->


    
</div>

