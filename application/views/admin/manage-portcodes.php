<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage Port Codes</h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage Port Codes</li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                 <?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">
            	<p class="text-right">
                	<a href="#addPortModal" class="btn btn-primary" data-toggle="modal" data-content-theme="addPortModal"><i class="fa fa-plus"></i> Port Code</a>                
                </p>
            


                <div class="panel panel-default">

                
                	<div class="panel-heading">
						<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>
                    <div class="panel-body" style="padding-bottom: 0px">
                    	<div class="well well-sm">
							<form method="get" action="<?php echo base_url().'webmanager/insurance/portcodes'?>">
                             	<input type="hidden" name="search_by" value="<?php echo $search_by; ?>" />
                                <div class="input-group">
                                  <input type="text" class="form-control" name="search" placeholder="Search" value="<?php echo $search_key; ?>">
                                  <span class="input-group-btn">
                                    <button class="btn btn-warning" type="submit"><i class="fa fa-search"></i> Search</button>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Search by <span class="caret"></span></button>
                                    <ul class="dropdown-menu filter-portcode">
                                      <li class="<?php if($search_by == 'country_code') { echo 'active'; }?>"><a href="#" data-field="country_code">Country Code</a></li>
                                      <li class="<?php if($search_by == 'port_code') { echo 'active'; }?>"><a href="#" data-field="port_code">Port Code</a></li>
                                      <li class="<?php if($search_by == 'port_name') { echo 'active'; }?>"><a href="#" data-field="port_name">Port Name</a></li>
                                    </ul>
                                  </span>
                                </div><!-- /input-group -->                            
                            </form>
                        </div>
                        
                    </div>

                    
                    <?php if(count($port_codes) > 0){ ?>
                    
                    <div class="panel-body">
                        <div class="pull-right">
                            <?php echo $this->pagination->create_links(); ?>
                        </div>
                        <div class="">
                            <a href="#" class="btn bnt-sm btn-link disabled">
                                <span class="text-muted">
                                    <?php echo 'Showing '.$page_from.' to '.$page_to.' of '.$port_codes_count.' entries'; ?>
                                </span>
                            </a>
                            
                        </div>
                    </div>
                    
					<div class="bg-white">
                    <table class="table table-hover table-datatablex">
                    	<thead>
                        	<tr>
                            	<th width="33%">Country Code</th>
                            	<th width="33%">Port Code</th>
                            	<th width="34%">Port Name</th>
                            	<th></th>
                            </tr>
                        </thead>
                    	<tbody>
                        	<?php foreach($port_codes as $r=>$value){ ?>
                        	<tr>
                                <td>
                                    <span class="price-text" data-id="<?php echo $value['id'] ?>" title="click to edit"><?php echo $value['country_code']; ?></span>
                                    <form class="text_form hidden">
                                        <div class="form-group">
                                            <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $value['country_code']; ?>" />
                                            <input type="hidden" name="id" value="<?php echo $value['id']; ?>" />
                                            <input type="hidden" name="field_name" value="country_code" />
                                            <input type="hidden" name="db_name" value="port_codes" />
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                            <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                        </div>
                                    </form>
                                
                                </td>
                            	<td>
                                    <span class="price-text" data-id="<?php echo $value['id'] ?>" title="click to edit"><?php echo $value['port_code']; ?></span>
                                    <form class="text_form hidden">
                                        <div class="form-group">
                                            <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $value['port_code']; ?>" />
                                            <input type="hidden" name="id" value="<?php echo $value['id']; ?>" />
                                            <input type="hidden" name="field_name" value="port_code" />
                                            <input type="hidden" name="db_name" value="port_codes" />
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                            <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                        </div>
                                    </form>
                                </td>
                            	<td>
                                    <span class="price-text" data-id="<?php echo $value['id'] ?>" title="click to edit"><?php echo $value['port_name']; ?></span>
                                    <form class="text_form hidden">
                                        <div class="form-group">
                                            <input type="text" name="text_input" class="form-control input-sm" value="<?php echo $value['port_name']; ?>" />
                                            <input type="hidden" name="id" value="<?php echo $value['id']; ?>" />
                                            <input type="hidden" name="field_name" value="port_name" />
                                            <input type="hidden" name="db_name" value="port_codes" />
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                            <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                        </div>
                                    </form>
                                </td>
                                <td>
                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                     
                                        <li><a href="#" class="delete_btn" data-id="<?php echo $value['id']?>" data-table="port_codes">Delete</a></li>
                                      </ul>
                                    </div>                              
                                
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
					</div>
                    
                    <div class="panel-body">
                    	<div class="pull-right">
							<?php echo $this->pagination->create_links(); ?>
                        </div>
                    	<div class="">
							<a href="#" class="btn bnt-sm btn-link disabled">
                                <span class="text-muted">
									<?php echo 'Showing '.$page_from.' to '.$page_to.' of '.$port_codes_count.' entries'; ?>
                                </span>
                            </a>
                            
                        </div>
                    </div>
                    
                <?php
                  //loop sorted_schedule
                 }
				 else{
				 	echo '<p class="text-center text-muted" style="margin: 25px 0;">No records found.</p>';
				 }
				 ?>					

         
                </div>
                        
         

            </div>


        </div><!--.row-->

   
</div>


	<!-- Modal -->
    <div class="modal fade" id="addPortModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">New Port Code</h4>
          </div>
          <form id="add_port_form">
              <div class="modal-body">
                <div class="form-group">
                    <label for="ccode">Country Code</label>
                    <select class="form-control" name="ccode" id="ccode" data-live-search="true">
                      <option value="">Select Country</option>
                      <?
                      foreach($countries as $r=>$value){
                          echo '<option value="'.$value['iso2'].'">'.$value['short_name'].' ('.$value['iso2'].')</option>';
                      }?>
                    </select>

                </div>
                <div class="form-group">
                    <label>Port Code</label>
                    <input type="text" class="form-control" name="portcode" />
                </div>
                <div class="form-group">
                    <label>Port Name</label>
                    <input type="text" class="form-control" name="portname" />
                </div>
              </div>
              <div class="modal-footer">
                <a href="#" class="btn btn-default" data-dismiss="modal">Cancel</a>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>
          </form>
        </div>
      </div>
    </div>

