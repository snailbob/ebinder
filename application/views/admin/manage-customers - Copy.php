<div class="col-lg-9 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">

                <div class="panel panel-default">

                
                	<div class="panel-heading">
                        <a href="#addAgencyModal" class="btn btn-primary pull-right <?php echo ($this->common->default_cargo() == '6') ? 'hidden' :''?>" onclick="$('#addAgencyModal').find('input').val('');" data-toggle="modal" data-controls-modal="#addAgencyModal">Add <?php echo $singular_title; ?></a>
                    
                    	<div  class="panel-title">
                            <h4>Manage <?php echo $title; ?></h4>
						</div>
                    </div>

                        <?php if(count($users) > 0) { ?>
                        
                        
						<div class="table-responsivex">
                        
                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th>Name</th>
                              
                              <th>Status</th>
                              <th>Activated</th>
                              <th>Broker Code</th>
                              
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>                        
                        
                        <?php
							foreach($users as $u=>$user){
								$white = $this->common->db_field_id('brokerage', 'enable_whitelabel', $user['brokerage_id']);
						?>
                        
                            <tr>
                             
                              <td><?php echo $this->common->customer_name($user['id']) ?></td>
                              <td>
							  	<?php
									if($user['status'] == 'Y'){
										echo '<span class="text-success">Verified</span>';
									}
									
									else{
										echo '<span class="text-red">Not Verified</span>';
									}
								?>
                              </td>
                              <td>
							  	<?php
									if($user['enabled'] == 'Y'){
										echo '<span class="text-success">Yes</span>';
									}
									
									else{
										echo '<span class="text-red">No</span>';
									}
								?>
                              </td>
                              <td>
							  	<?php echo $this->common->brokercode($user['id']) ?>
                              </td>

                              
                              <td>
                                                                  
                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
                                        <?php if($this->session->userdata('logged_admin') == 'admin') {?>
                                        <li><a href="<?php echo base_url().'webmanager/customers/accessportal/'.$user['id']?>" target="_blank">Access Portal</a></li>
                                        <?php } ?>

                                        <?php if($user['enabled'] == 'Y') {?>                                       
                                        <li><a href="javascript:;" onclick="customerActivate('N','<?php echo $user['id']; ?>');">Deactivate</a></li>
                                        <?php } else {?>
                                        <li><a href="javascript:;" onclick="customerActivate('Y','<?php echo $user['id']; ?>');">Activate</a></li>
                                        <?php } ?>
                                        
                                       <?php /*?> <?php if($this->uri->segment('4') == 'brokers'){ ?>
                                        <li><a href="<?php echo base_url().'webmanager/customers/manage/customers/'.$user['id']?>" class="view-customer-btn" data-id="<?php echo $user['id'] ?>">View Customers</a></li>
                                        
										<?php } ?><?php */?>
                                        
                                        <li class="<?php echo ($white == 'Y') ? '' : 'hidden' ?>"><a href="javascript:;" class="broker-white-link" data-id="<?php echo $user['id'] ?>" data-arr='<?php echo json_encode($user) ?>'>Email Unique Link</a></li>
                                        
                                        <li><a href="<?php echo base_url().'webmanager/customers/manage/brokers/'.$user['id'].'/message'?>">Message Broker</a></li>
                                        
                                        <li><a href="javascript:;" class="customer-update" data-id="<?php echo $user['id'] ?>" data-arr='<?php echo json_encode($user) ?>'>Update</a></li>
                                        <li><a href="javascript:;" onclick="customerDetails('<?php echo $user['id']; ?>');">Details</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:;" class="delete_btn" data-id="<?php echo $user['id']?>" data-table="customers">Delete Permanently</a></li>
                                      </ul>
                                    </div>                              
                              
                              
                              </td>
                            </tr>
                        
                        <?php
								
							}
						?>


                          
                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->
                          
                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';	
						}?>
                        
                    <div class="panel-body hidden">

                    </div>

                </div>

            </div>


        </div><!--.row-->


    
</div>





<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $singular_title; ?> Information</h4>
      </div>
      <div class="modal-body">
       	<div class="customer_info">
        
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="addAgencyModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                            <h1><?php echo $singular_title; ?></h1>
                			<hr />

                    
                            <form id="customer_form" class="text-left">
                            	<input type="hidden" name="id" />
                            	<input type="hidden" name="studio_id" value="<?php echo $this->uri->segment(5)?>" />
                                <input type="hidden" name="utype" value="<?php echo ($this->uri->segment(4) != 'brokers') ? 'Y' : 'N' ?>"/>
                                
                                
                            	<div class="row">
                                	<div class="col-sm-6">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" class="form-control" name="first" />
                                        </div>
                                    </div>
                                	<div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control" name="last" />
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group" <?php if($this->uri->segment(4) =='customers') { echo 'hidden'; }?>>
                                    <label>Brokerage Name</label>
                                    <select class="form-control" name="brokerage_id">
                                    	<option value="">Select</option>
                                    	<?php foreach($brokerage as $r=>$value){
											
												echo '<option value="'.$value['id'].'"'; 
												
													if($this->session->userdata('logged_admin_brokerage') == $value['id']){
														echo ' selected="selected"';
													}
												echo '>'.$value['company_name'].'</option>';
										} ?>
                                    
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" />
                                </div>
                                <div class="form-group hidden">
                                    <label><?php echo $singular_title; ?> Type</label><br />
									<label>
                                    	<input type="text" name="ctype" value="<?php if($this->uri->segment(4) == 'brokers') { echo 'N'; } else { echo 'Y'; } ?>"/>
                                    	
                                    </label>
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            
                            
                            </form>

	
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->      