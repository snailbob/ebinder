<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage Bought Insurances</h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage Bought Insurances</li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                 <?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">
            


                <div class="panel panel-default">

                
                	<div class="panel-heading">
						<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>

                    
                    <?php if(count($insurances) > 0){ ?>
					<div class="bg-white">
                    <table class="table table-hover table-datatable">
                    	<thead>
                        	<tr>
                            	<th>Quotation Details</th>
                            	<th>Premium Cost</th>
                            	<th>Customer Name</th>
                            	<th>Date Purchased</th>
                            	<th></th>
                            </tr>
                        </thead>
                    	<tbody>
                        	<?php foreach($insurances as $r=>$value){ ?>
                        	<tr>
                            	<td>
                                	<?php
										$details = unserialize($value['details']);
										$premium = $details['premium'];
										$buy_inputs = $details['buy_inputs'];
										
										foreach($buy_inputs as $buy){
											$buyval = $buy['value'];
											if($buy['name'] == 'cargocat'){
												$buyval = $this->common->cargo_name($buy['value']);
											}
											echo ucfirst($buy['name']) .': <span class="text-muted">'.$buyval.'</span></br>';
										}
									
									?>
                                
                                </td>
                            	<td><?php echo '$'.$premium; ?></td>
                            	<td><?php echo $this->common->customer_name($value['customer_id']); ?></td>
                            	<td><?php echo date_format(date_create($value['date_added']), 'F d, Y - l') ?></td>
                            	<td>
                                
                                    <div class="dropdown pull-right">
                                      <button class="btn btn-default btn-xs" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        Action
                                        <span class="caret"></span>
                                      </button>
                                    
                                      <ul class="dropdown-menu" aria-labelledby="dLabel">

                                        <li><a href="#" class="delete_btn" data-id="<?php echo $value['id'] ?>" data-table="bought_insurance">Delete</a></li>
                                      </ul>
                                    </div>                                
                                
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
					</div>
                    
                <?php
                  //loop sorted_schedule
                 } ?>					

                    
                </div>
                        
         
         
         

            </div>


        </div><!--.row-->


    
</div>


