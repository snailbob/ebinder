<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

					<?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">
            	<p class="text-right">
                	<a href="<?php echo base_url().'webmanager/contents/alternate'?>" class="btn btn-primary"><i class="fa fa-plus"></i> <?php //echo $title; ?></a>
                </p>

                <div class="panel panel-default">

                    <div class="panel-heading">
						<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>


                        <?php if(count($admin_info) > 0) { ?>


						<div class="table-responsivex">

                        <table class="table table-striped table-hover table-datatablex">
                          <thead>
                            <tr>
                              <th>Title</th>
                              <th>Content</th>
                              <th>Image</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody id="faq_sortable" data-table="admin_contents">

                        <?php
							foreach($admin_info as $u=>$user){
						?>

                            <tr id="item-<?php echo $user['id'] ?>">

                              <td><i class="fa fa-arrows-alt fa-fw"></i> <?php echo $user['title'] ?></td>
                              <td><?php echo $user['content'] ?></td>

                              <td>
							  	<?php
									if($user['image'] != ''){
										echo '<img src="'.base_url().'uploads/avatars/'.$user['image'].'" style="width: 150px;">';
									}
								?>
                              </td>

                              <td>

                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">

                                        <li><a href="<?php echo base_url().'webmanager/contents/alternate/'.$user['id']?>">Update</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:;" onclick="deleteAlternate('<?php echo $user['id']; ?>');">Delete Permanently</a></li>
                                      </ul>
                                    </div>


                              </td>
                            </tr>

                        <?php

							}
						?>



                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">No <?php echo $title; ?></p></div>';
						}?>

                    <div class="panel-body hidden">

                    </div>

                </div>

            </div>


        </div><!--.row-->



</div>



<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $title; ?> Information</h4>
      </div>
      <div class="modal-body">
       	<div class="agency_info">

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add<?php echo $title; ?>Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $title; ?></h4>
      </div>
      <div class="modal-body">
      	<form id="agency_form">
        	<div class="form-group">
            	<label><?php echo $title; ?> Name</label>
                <input type="text" class="form-control" name="name" />
            </div>
        	<div class="form-group">
            	<label>Email</label>
                <input type="text" class="form-control" name="email" />
            </div>
        	<div class="form-group">
            	<button class="btn btn-primary" type="submit">Submit</button>
            </div>


        </form>

      </div>

    </div>
  </div>
</div>
