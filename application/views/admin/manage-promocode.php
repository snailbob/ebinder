<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>
                        <li><a href="<?php echo base_url()?>webmanager/products" class="preloadThis">Products</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">


                <div class="panel panel-default">


                	<div class="panel-heading">

                      <a href="#formModal" class="btn btn-primary pull-right" data-toggle="modal" data-controls-modal="#formModal" onclick="$('#formModal').find('input').val('')"><i class="fa fa-plus"></i> <?php echo $singular_title; ?></a>
                      <a href="#" class="btn btn-default pull-right producttab_btn" data-id="<?php echo $this->uri->segment(4) ?>" style="margin-right: 5px;">Back</a>
                      <a href="#historyModal" class="btn pull-right show_history_btn" data-type="promo_code" style="margin-right: 5px;">
                        <i class="fa fa-history"></i> History
                      </a>

                    	<div  class="panel-title">
                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>



                        <?php if(count($promocodes) > 0) { ?>


                        <div class="table_search">
                            <input type="text" class="form-control input_table_search" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>

						<div class="table-responsivex">

                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th class="hidden"></th>
                              <th>Promo Code</th>
                              <th>Discount (%)</th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>

                        <?php
							$count = 1;
							foreach($promocodes as $u=>$value){
						?>

                            <tr>

                              <td class="hidden"><?php echo $count; ?></td>
                              <td><?php echo $value['promo_code'] ?></td>

                              <td><?php echo $value['discount'] ?></td>
                              <td>

                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">

                                        <li><a href="javascript:;" class="update-promocode" data-info='<?php echo json_encode($value);?>'>Update</a></li>
                                        <li><a href="#" class="delete_btn" data-id="<?php echo $value['id'] ?>" data-table="biz_promocode">Delete</a></li>
                                      </ul>
                                    </div>

                              </td>

                            </tr>

                        <?php
								$count++;
							}
						?>



                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->

                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';
						}?>



                </div>

            </div>


        </div><!--.row-->



</div>





    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="formModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

                            <h1><?php echo $singular_title; ?></h1>
                			<hr />


                            <form id="promocode_form" class="text-left">
                            	<input type="hidden" name="id" />
                                <div class="form-group">
                                    <label>Promo Code</label>
                                    <input type="text" class="form-control" name="promo_code" required="required"/>
                                </div>
                                <div class="form-group">
                                    <label>Discount (%)</label>
                                    <input type="number" max="100" min="0" class="form-control" name="discount" required="required"/>
                                </div>
                                <div class="form-group text-right">
                                    <a class="btn btn-default btn-grey btn-cancel" href="#" data-dismiss="modal">Cancel</a>

                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>


                            </form>


                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div><!--portfolio-modal-->
