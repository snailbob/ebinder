

            <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">

                <div class="row">


                  <div class="col-lg-12">

                    <div class="page-title">

                      <h3> <?php echo $title; ?> Admin Dashboard</h3>

                      <ol class="breadcrumb">
                        <li class="active"><i class="fa fa-dashboard"></i> Dashboard</li>
                      </ol>
                    </div>


                    <?php if ($this->session->flashdata('ok') != ''){ ?>
                    <div class="alert alert-success fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>

                        <?php echo $this->session->flashdata('ok'); ?>
                    </div>
                    <?php } ?>


                    <?php if ($this->session->flashdata('error') != ''){ ?>
                    <div class="alert alert-danger fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>

                        <?php echo $this->session->flashdata('error'); ?>
                    </div>
                    <?php } ?>

                    <?php if ($this->session->flashdata('success') != ''){ ?>
                    <div class="alert alert-success fade in">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>

                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <?php } ?>


                    <?php /*?><div class="panel panel-default">
                        <div class="panel-heading">
                            <div class="panel-title">
								<h4><?php echo $title; ?></h4>
                            </div>
                        </div>

                        <div class="panel-body">
                        	<div class="col-sm-6 col-sm-offset-3">

                            </div>
                        </div>


                    </div><!--panel--><?php */?>


                  	<h3 class="text-right">
                      <a href="#addWidgetModal" id="add-new-widgetx" data-toggle="modal" data-controls-modal="#addWidgetModal" class="btn btn-lgx btn-primary pull-right">
                      	<i class="fa fa-plus fa-fw"></i> Widget
                      </a>

                    	<div class="form-group pull-right" style="width: 150px; margin-right: 5px;">
                        	<select class="form-control days_filter">
                            	<?php $day = (isset($_GET['day'])) ? $_GET['day'] : 30; ?>
                            	<option value="30" <?php echo ($day == 30) ? 'selected="selected"' : ''?>>30 Days</option>
                            	<option value="60" <?php echo ($day == 60) ? 'selected="selected"' : ''?>>60 Days</option>
                            	<option value="90" <?php echo ($day == 90) ? 'selected="selected"' : ''?>>90 Days</option>
                            </select>
                        </div>

                    </h3>


                    <div class="row padding-top-10">
                    	<div class="col-sm-12 padding-top-10">

                            <div class="grid-stack">


                                <?php /*?><div class="grid-stack-item" id="stack-itemR2"
                                    data-gs-x="0" data-gs-y="0"
                                    data-gs-width="4" data-gs-height="2">
                                        <div class="grid-stack-item-content panel">

                                              <div class="panel-body">
                                                <button type="button" class="close" data-target="#stack-itemR2" data-dismiss="alert"> <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                Total bound premium

                                                <p style="margin-bottom: 0; margin-top: 15px; font-size: 35px" class="text-strong text-success">
                                                	<span class="text-strong"><strong>$<?php echo $total_premium_all?></strong></span><br />
                                                </p>

                                              </div>
                                        </div>
                                </div><?php */?>



                              <?php
							  $gs_x = 0;
							  for($i = 0; $i < count($q_widget); $i++) {
								  $referral = $this->common->format_referral(array('id'=>$q_widget[$i]['referral_id']));
								  	//echo json_encode($referral);
								  ?>


                                <div class="grid-stack-item" id="stack-item<?=$i ?>"
                                    data-gs-x="<?php echo $gs_x ?>" data-gs-y="0"
                                    data-gs-width="4" data-gs-height="2">
                                        <div class="grid-stack-item-content panel">

                                              <div class="panel-body">
                                                <button type="button" class="close dismiss-widget" data-target="stack-item<?php echo $i ?>" data-id="<?php echo $q_widget[$i]['id'] ?>" data-dismiss="alertx"> <span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <?php echo $q_widget[$i]['title']?>

                                                <?php
                                                 if($q_widget[$i]['type'] == 'premium'){ ?>
                                                  <p style="margin-bottom: 0; margin-top: 15px; font-size: 35px" class="text-strong text-success">
                                                  	<span class="text-strong"><strong>$<?php echo $total_premium_all?></strong></span><br />
                                                  </p>

                                                <?php } else if($q_widget[$i]['type'] == 'topbrokers') {
                                                  if(count($widget_topbrokers) > 0){
                                                    echo '<ol class="list-unstyled text-muted">';
                                                    $tb_count = 1;
                                                    foreach($widget_topbrokers as $r=>$value){
                                                      echo '<li>'.$tb_count.'. '.$this->common->customer_name($value).'</li>';
                                                      $tb_count++;
                                                    }
                                                    echo '</ol>';
                                                  } else{
                                                    ?>
                                                    <p class="text-muted">Nothing to show you.</p>
                                                <?php
                                                  }
                                                } ?>


                                              </div>
                                        </div>
                                </div>
                              <?php $gs_x = $gs_x + 4; } ?>

                            </div>

                        </div>
                    </div>



                  </div><!-- /.col-lg-12 -->

                </div><!-- /.row -->
                <!-- end PAGE TITLE AREA -->

            </div><!--end of main_content-->





    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="addWidgetModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">

            							<div class="row" style="margin-top: 100px">

                            	<div class="col-sm-12">
                                	<h1>Add Widget</h1>
                                </div>

                            	<div class="col-sm-12">


                                    <form id="widget_form" method="post" novalidate="novalidate">
                                      <div class="row text-left">

                                        <div class="col-md-12">
                                          <div class="form-group">
                                                <div class="btn-group btn-group-justified" data-toggle="buttons">
                                                  <label class="btn btn-default" style="padding: 10px; text-align: left">
                                                    <span class="pull-right btn-xs">Add</span>
                                                    <input type="checkbox" name="widget[]" value="premium">
                                                    Total bound premium
                                                  </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                          <div class="form-group">
                                                <div class="btn-group btn-group-justified" data-toggle="buttons">
                                                  <label class="btn btn-default" style="padding: 10px; text-align: left">
                                                    <span class="pull-right btn-xs">Add</span>
                                                    <input type="checkbox" name="widget[]" value="topbrokers">
                                                    Top 5 brokers
                                                  </label>
                                                </div>
                                          </div>
                                        </div>

                                      </div>


                                      <div class="form-group text-right">
                                          <button class="btn btn-default btn-cancel btn-grey" data-dismiss="modal">Cancel</button>
                                          <button class="btn btn-primary" type="submit">Submit</button>
                                      </div>


                                    </form>

                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>


        </div>
