<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                <?php if($this->session->flashdata('success')!=""){ ?>

                <div class="alert alert-success alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                </div>

                <?php } if($this->session->flashdata('error')!=""){ ?>

                <div class="alert alert-danger alert-dismissable">

                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                <?php } ?>

            </div>
            <div class="col-lg-12">

                <div class="panel panel-default">

                
                	<div class="panel-heading">
                        <a href="#addAgencyModal" class="btn btn-primary pull-right <?php echo ($this->common->default_cargo() == '6') ? 'hidden' :''?>" onclick="$('#addAgencyModal').find('.form-control').val('');" data-toggle="modal" data-controls-modal="#addAgencyModal"><i class="fa fa-plus"></i> <?php echo $singular_title; ?></a>
                    

                   
                    	<div  class="panel-title">
                            <h4>Manage <?php echo $title; ?></h4>
						</div>
                    </div>

                        <?php if(count($users) > 0) { ?>
                        <div class="table_search">
                            <input type="text" class="form-control input_table_search" placeholder="search for..." />
                            <i class="fa fa-search"></i>
                        </div>

                        <h4 class="lead loading-table text-center text-muted"><i class="fa fa-spinner fa-spin"></i></h4>

    

                        
						<div class="table-responsivex">
                        
                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th>Name</th>
                              
                              <th>Status</th>
                              <th>Activated</th>
                              
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>                        
                        
                        <?php
							foreach($users as $u=>$user){
                                $user['customer_info'] = (@unserialize($user['customer_info'])) ? unserialize($user['customer_info']) : array();
								$white = $this->common->db_field_id('brokerage', 'enable_whitelabel', $user['brokerage_id']);
						?>
                        
                            <tr>
                             
                              <td><?php echo $this->common->customer_name($user['id']) ?></td>
                              <td>
							  	<?php
									if($user['status'] == 'Y'){
										echo '<span class="text-success">Verified</span>';
									}
									
									else{
										echo '<span class="text-red">Not Verified</span>';
									}
								?>
                              </td>
                              <td>
							  	<?php
									if($user['enabled'] == 'Y'){
										echo '<span class="text-success">Yes</span>';
									}
									
									else{
										echo '<span class="text-red">No</span>';
									}
								?>
                              </td>


                              <td>
                                                                  
                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">


                                        <?php if($user['enabled'] == 'Y') {?>                                       
                                        <li><a href="javascript:;" onclick="customerActivate('N','<?php echo $user['id']; ?>');">Deactivate</a></li>
                                        <?php } else {?>
                                        <li><a href="javascript:;" onclick="customerActivate('Y','<?php echo $user['id']; ?>');">Activate</a></li>
                                        <?php } ?>
                                        
                                       <?php /*?> <?php if($this->uri->segment('4') == 'brokers'){ ?>
                                        <li><a href="<?php echo base_url().'webmanager/customers/manage/customers/'.$user['id']?>" class="view-customer-btn" data-id="<?php echo $user['id'] ?>">View Customers</a></li>
                                        
                                        <?php if($this->session->userdata('logged_admin') == 'admin') {?>
                                        <li><a href="<?php echo base_url().'webmanager/customers/accessportal/'.$user['id']?>" target="_blank">Access Portal</a></li>
                                        <?php } ?>

                                        <li><a href="<?php echo base_url().'webmanager/customers/manage/brokers/'.$user['id'].'/message'?>">Message Broker</a></li>
                                    	<?php } ?><?php */?>
                                        
                                        <li class="<?php echo ($white == 'Y') ? '' : 'hidden' ?>"><a href="javascript:;" class="broker-white-link" data-id="<?php echo $user['id'] ?>" data-arr='<?php echo json_encode($user) ?>'>Email Unique Link</a></li>
                                       
                                        
                                        <li><a href="javascript:;" class="customer2-update" data-id="<?php echo $user['id'] ?>" data-arr='<?php echo json_encode($user) ?>'>Update</a></li>
                                        <li><a href="javascript:;" onclick="customerDetails('<?php echo $user['id']; ?>');">Details</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:;" class="delete_btn" data-id="<?php echo $user['id']?>" data-table="customers">Delete Permanently</a></li>
                                      </ul>
                                    </div>                              
                              
                              
                              </td>
                            </tr>
                        
                        <?php
								
							}
						?>


                          
                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->
                          
                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';	
						}?>
                        
                    <div class="panel-body hidden">

                    </div>

                </div>

            </div>


        </div><!--.row-->


    
</div>





<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $singular_title; ?> Information</h4>
      </div>
      <div class="modal-body">
       	<div class="customer_info">
        
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



    <!-- Portfolio Modals -->
    <div class="portfolio-modal modal fade" id="addAgencyModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="modal-body">
                        
                            <h1><?php echo $singular_title; ?></h1>
                			<hr />

                    
                            <form id="customer2_form" class="text-left">
                            	<input type="hidden" name="id" />
                                <input type="hidden" name="broker_id" value="<?php echo $this->uri->segment(5)?>" />
                                <input type="hidden" name="utype" value="<?php echo ($this->uri->segment(4) != 'brokers') ? 'Y' : 'N' ?>"/>
                                
                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Business/Company Name</label>
                                            <input type="text" class="form-control" name="company_name" required="required" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Business Address</label>

                                            <input type="text" class="form-control user_geolocation" name="address" required="required"/>
                                            <input type="hidden" name="lat" />
                                            <input type="hidden" name="lng" />


                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Annual Revenue</label>
                                            <input type="text" class="form-control" name="annaul_revenue" required="required" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Employee Number</label>
                                            <input type="text" class="form-control" name="employee_number" required="required" />
                                        </div>
                                    </div>
                                </div>
                                

                                <div class="row">
                                                                        

                                    <div class="col-sm-12">
                                        <strong>Revenue by State</strong>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>NSW</label> 
                                          <input type="text" name="breakdown_by_state[]" class="form-control input-number active-numpad" placeholder="" required="required" value="" max="100" min="0">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>&nbsp;</label>   
                                        <div class="form-control-static">
                                          %
                                        </div>
                                    </div>
                                                                        

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>ACT</label> 
                                          <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="" required="required" value="" max="100" min="0">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>&nbsp;</label>   
                                        <div class="form-control-static">
                                          %
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                                                        

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>QLD</label> 
                                          <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="" required="required" value="" max="100" min="0">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>&nbsp;</label>   
                                        <div class="form-control-static">
                                          %
                                        </div>
                                    </div>
                                                                        

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>VIC</label> 
                                          <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="" required="required" value="" max="100" min="0">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>&nbsp;</label>   
                                        <div class="form-control-static">
                                          %
                                        </div>
                                    </div>
                                                                        

                                    <div class="clearfix"></div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>TAS</label> 
                                          <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="" required="required" value="" max="100" min="0">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>&nbsp;</label>   
                                        <div class="form-control-static">
                                          %
                                        </div>
                                    </div>
                                                                        

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>SA</label>  
                                          <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="" required="required" value="" max="100" min="0">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>&nbsp;</label>   
                                        <div class="form-control-static">
                                          %
                                        </div>
                                    </div>
                                         
                                    <div class="clearfix"></div>                               

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>WA</label>  
                                          <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="" required="required" value="" max="100" min="0">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>&nbsp;</label>   
                                        <div class="form-control-static">
                                          %
                                        </div>
                                    </div>
                                                                        

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>NT</label>  
                                          <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="" required="required" value="" max="100" min="0">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>&nbsp;</label>   
                                        <div class="form-control-static">
                                          %
                                        </div>
                                    </div>
                                                                        

                                    <div class="clearfix"></div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Overseas</label>    
                                          <input type="text" name="breakdown_by_state[]" class="form-control input-number" placeholder="" required="required" value="" max="100" min="0">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label>&nbsp;</label>   
                                        <div class="form-control-static">
                                          %
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <label>&nbsp;</label>   
                                        <div class="form-control-static">
                                          Total must be 100% <strong><span class="the-total text-info"> (0%)</span></strong>
                                        </div>
                                    </div>
                                </div>


                                <p><strong>Primary contact details</strong></p>

                                <div class="row">

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>First Name</label>
                                            <input type="text" class="form-control" name="first" required="required" />
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Last Name</label>
                                            <input type="text" class="form-control" name="last" required="required" />
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="form-group" <?php if($this->uri->segment(4) =='customers') { echo 'hidden'; }?>>
                                    <label>Brokerage Name</label>
                                    <select class="form-controlx" name="brokerage_id">
                                    	<option value="">Select</option>
                                    	<?php foreach($brokerage as $r=>$value){
											
												echo '<option value="'.$value['id'].'"'; 
												
													if($this->session->userdata('logged_admin_brokerage') == $value['id']){
														echo ' selected="selected"';
													}
												echo '>'.$value['company_name'].'</option>';
										} ?>
                                    
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" class="form-control" name="email" />
                                </div>
                          



                                
                                <div class="row">
                                
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Country Calling Code</label>
                                            <select class="form-control country_code_dd" name="country_code" data-live-search="true" required="required">
                                                <option value="" data-code="">Select..</option>
                                                <?php
                                                    foreach($countries as $r=>$mc){
                                                        echo '<option value="'.$mc['country_id'].'" data-code="'.$mc['calling_code'].'"';

                                                        echo '>'.$mc['short_name'].' (+'.$mc['calling_code'].')</option>';
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                
                                    <div class="col-sm-6">
                                        <div class="form-group mobile_input">
                                            <label class="control-label">&nbsp;</label>
                                              <input type="number" class="form-control" placeholder="Contact Number" name="mobile" autocomplete="nope" value="" required>

                                            <input type="hidden" name="country_short" value=""/>
                                        </div>
                                    </div>
                                
                                </div><!--phone number-->
                                                                             
                                

                                <div class="form-group hidden">
                                    <label><?php echo $singular_title; ?> Type</label><br />
									<label>
                                    	<input type="text" name="ctype" value="<?php if($this->uri->segment(4) == 'brokers') { echo 'N'; } else { echo 'Y'; } ?>"/>
                                    	
                                    </label>
                                </div>




                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            
                            
                            </form>

	
                        </div>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div><!--portfolio-modal-->      