

  <div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">

      <div class="row">


        <div class="col-lg-12">




        <?php if(!empty($success) || !empty($danger) || !empty($info)) { ?>
		<div class="alert <?php echo (!empty($success)) ? 'alert-success' : ''; echo (!empty($danger)) ? 'alert-danger' : ''; echo (!empty($info)) ? 'alert-info' : ''; ?>">
        	<?php echo $success.$danger.$info ?>
        </div>
        <?php } ?>

        <div class="module_header">
            <i class="fa fa-globe fa-fw"></i> <span class="text-uppercase"><?php echo $title ?></span>
            <?php if($user['customer_type'] == 'N' && $user['studio_id'] == '0') {?>
            <ul class="module_actions right hidden">
                <li>
                    <a class="module_action btn--red" href="/panel/addmember">
                        <div class="module_action_icon">
                            <i class="fa fa-user-plus"></i>
                        </div>
                        <span class="module_action_text text-uppercase"> Add <?php echo $singular ?></span>
                    </a>
                </li>
            </ul>
            <?php } ?>

        </div>

        <div class="panel panel-default">

          <div class="panel-heading">

            <div  class="panel-title">
                  <h4><?php echo $title; ?></h4>
            </div>
          </div>
        	<div class="panel-body">

              <div class="row">
                <div class="col-sm-12">

                  <div class="row gutter-md">
                      <div class="col-sm-4">
                          <div class="form-groupx">
                              <button type="button" class="btn btn-default btn-lg btn-block google-auth-btn <?php echo ($user_sess['google_id'] != '') ? 'disabled' : '' ?>" id="customBtn"><i class="fa fa-google-plus text-danger"></i> <?php echo ($user_sess['google_id'] != '') ? 'Google+ Connected' : 'Connect Google+' ?></button>
                          </div>
                      </div>

                      <div class="col-sm-4">
                          <div class="form-groupx">
                              <button type="button" class="btn btn-info btn-lg btn-block btn-linkedin linkedin-auth-btn <?php echo ($user_sess['fb_id'] != '') ? 'disabled' : '' ?>" id="MyLinkedInButton"><i class="fa fa-linkedin"></i> <?php echo ($user_sess['fb_id'] != '') ? 'LinkedIn Connected' : 'Connect LinkedIn' ?></button>
                          </div>
                      </div>

                      <div class="col-sm-4">
                          <div class="form-groupx">
                              <a href="<?php echo base_url().'twitter'; ?>" class="btn btn-info btn-twitter btn-lg btn-block <?php echo ($user_sess['twitter_id'] != '') ? 'disabled' : '' ?>"><i class="fa fa-twitter"></i> <?php echo ($user_sess['twitter_id'] != '') ? 'Twitter Connected' : 'Connect Twitter' ?></a>
                          </div>
                      </div>

                  </div>




                </div>

              </div>

          </div>
        </div>
    </div>
</div>

    </div>

    <!-- End of Contents -->
