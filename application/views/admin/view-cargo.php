<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage Cargo Category</h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage Cargo Category</li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                 <?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">
            
				<p class="text-left">
                	<a href="<?php echo base_url().'webmanager/cargo/manage'?>" class="btn btn-default">Back</a>
                </p>

         
                <div class="panel panel-default">

                    <div class="panel-heading">

                        <div class="panel-title">
                            <h4>
                            	<?php 
									if(count($the_agency) > 0){
										echo $the_agency[0]['name'].' ';
									}								
								?>
                            Cargo Category</h4>

                        </div>

                    </div>

					<div class="panel-bodyx">

                    <?php
						if(count($cargo) > 0) {
					
					?>

                    <div class="bg-white"><!--bg-white-->
                        <table class="table table-hover table-datatable">
                            <thead>
                                <tr>
                                    <th width="70%">Name</th>
                                    <th width="30%">Price</th>
                                </tr>
                            </thead>
                            <tbody>
                            
                                <?php foreach($cargo as $r=>$value){
									$acargo_price = $value['price'];
									if(count($the_agency) > 0) {
										$acargo_price = 0;
										if(isset($agency_prices[$value['id']])) {
											$acargo_price = $agency_prices[$value['id']];
										}
									}
									?>
                                
                                    <tr>
                                        <td><?php echo $value['name']; ?></td>
                                        <td >
                                        <span class="price-text" data-id="<?php echo $value['id'] ?>" title="click to edit"><?php echo $acargo_price; ?></span>
                                        <form class="price_form hidden">
                                            <div class="form-group">
                                                <input type="text" name="cargo-price" class="form-control input-sm" value="<?php echo $acargo_price; ?>" />
                                                <input type="hidden" name="cargo-price-id" value="<?php echo $value['id']; ?>" />
                                                <input type="hidden" name="cargo-agency-id" value="<?php if(count($the_agency) > 0) { echo $the_agency[0]['id']; } ?>" />
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-sm">Save</button>
                                                <a href="#" class="btn btn-default btn-sm">Cancel</a>
                                            </div>
                                        </form>
                                        </td>
                                    </tr>
                                <?php } ?>
                                
                                
                                
                            </tbody>
                        </table>
                        </div><!--bg-white-->
                        <?php } else { echo '<p>No records found</p>';} ?>                    
                    </div>
  
                        

                </div>

            </div>


        </div><!--.row-->


    
</div>


