<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage Agency</h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage Agency</li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

					<?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">
            	<p class="text-right">
                	<a href="#" class="btn btn-primary" data-toggle="modal" data-target="#addAgencyModal"><i class="fa fa-plus"></i> Agency</a>
                </p>

                <div class="panel panel-default">

                    <div class="panel-heading">
						<div  class="panel-title">
                            <h4>Agency</h4>
						</div>
                    </div>


                        <?php if(count($users) > 0) { ?>
                        
                        
						<div class="table-responsivex">
                        
                        <table class="table table-striped table-hover table-datatable">
                          <thead>
                            <tr>
                              <th>Name</th>
                              <th>Email</th>
                              <th>Status</th>
                              <th>Activated</th>
                              <th> </th>
                            </tr>
                          </thead>
                          <tbody>                        
                        
                        <?php
							foreach($users as $u=>$user){
						?>
                        
                            <tr>
                             
                              <td><?php echo $user['name'] ?></td>
                              <td><?php echo $user['email'] ?></td>
                              <td>
							  	<?php
									if($user['status'] == 'Y'){
										echo '<span class="text-success">Verified</span>';
									}
									
									else{
										echo '<span class="text-red">Not Verified</span>';
									}
								?>
                              </td>
                              <td>
							  	<?php
									if($user['enabled'] == 'Y'){
										echo '<span class="text-success">Yes</span>';
									}
									
									else{
										echo '<span class="text-red">No</span>';
									}
								?>
                              </td>
                              
                              <td>
                                                                  
                                    <!-- Single button -->
                                    <div class="btn-group pull-right">
                                      <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Action <span class="caret"></span>
                                      </button>
                                      <ul class="dropdown-menu" role="menu">
										<?php if($user['enabled'] == 'Y') {?>                                       
                                        <li><a href="javascript:;" onclick="userActivate('N','<?php echo $user['id']; ?>');">Deactivate</a></li>
                                        <?php } else {?>
                                        <li><a href="javascript:;" onclick="userActivate('Y','<?php echo $user['id']; ?>');">Activate</a></li>
                                        <?php } ?>
                                        
                                        
                                        <li><a href="javascript:;" onclick="userDetails('<?php echo $user['id']; ?>');">Details</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:;" onclick="userDelete('<?php echo $user['id']; ?>');">Delete Permanently</a></li>
                                      </ul>
                                    </div>                              
                              
                              
                              </td>
                            </tr>
                        
                        <?php
								
							}
						?>


                          
                          </tbody>
                        </table>
                        <p class="lead" style="padding-bottom: 15px;">&nbsp;</p>
                        </div><!--end of table responsive-->
                          
                        <?php } else{
							echo '<div class="panel-body"><p class="text-center text-muted">Nothing to show you.</p></div>';	
						}?>
                        
                    <div class="panel-body hidden">

                    </div>

                </div>

            </div>


        </div><!--.row-->


    
</div>



<!-- Modal -->
<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agency Information</h4>
      </div>
      <div class="modal-body">
       	<div class="agency_info">
        
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="addAgencyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agency</h4>
      </div>
      <div class="modal-body">
      	<form id="agency_form">
        	<div class="form-group">
            	<label>Agency Name</label>
                <input type="text" class="form-control" name="name" />
            </div>
        	<div class="form-group">
            	<label>Email</label>
                <input type="text" class="form-control" name="email" />
            </div>
        	<div class="form-group">
            	<button class="btn btn-primary" type="submit">Submit</button>
            </div>
        
        
        </form>

      </div>

    </div>
  </div>
</div>