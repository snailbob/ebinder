<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3>Manage <?php echo $title ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active">Manage <?php echo $title ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->





        



        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

					<?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">
                <div class="panel panel-default">
                	<div class="panel-heading">
						<div  class="panel-title">


                            <h4><?php echo $title; ?></h4>
						</div>
                    </div>

                   
                    <?php if(count($activity_log) > 0){ ?>
                    <div class="bg-white">
                    <table class="table table-hover activitylog-datatable">
                        <thead>
                            <tr>
                                <th class="hidden">sort</th>
                                <th>Acitivity</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $count = 1;
                            foreach($activity_log as $r=>$value){
                                
								
                            ?>
                            <tr>
                                <th class="hidden"><?php echo $count; ?></th>
                                <td><?php echo $value['name'] ?></td>
                                <td class="text-right"><?php echo date_format(date_create($value['date_updated']), 'd M Y, h:i:s A') ?></td>
                            </tr>
                            <?php $count++; } ?>
                        </tbody>
                    </table>
                    </div>
                    
                    <?php
                      //loop sorted_schedule
                     } 
                     else { echo '<p class="text-center text-muted" style="padding: 50px;">Nothing to show you.</p>'; }
                     ?>
                </div>

            </div>

        </div><!--.row-->
    
</div>


<!-- Modal -->
<div class="modal fade in" id="myActivityLogModal" data-logged="<?php echo $this->session->userdata('activity_logged') ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Re-enter Password</h4>
      </div>
      <form id="password_form" novalidate="novalidate">
          <div class="modal-body">

            <div class="form-group">
                <label>Password</label>
                <input type="password" class="form-control" name="password" value="" required="required"/>                       
            </div>
    
            
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
      </form>
    </div>
  </div>
</div>

