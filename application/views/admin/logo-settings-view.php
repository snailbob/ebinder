<div class="col-lg-10 col-md-9 col-sm-8 main_content" style="border-left: 1px solid #e7e7e7">


        <div class="row">

            <div class="col-lg-12">

                <div class="page-title">

                    <h3><?php echo $title; ?></h3>

                    <ol class="breadcrumb">

                        <li><i class="fa fa-dashboard"></i> <a href="<?php echo base_url()?>webmanager" class="preloadThis">Dashboard</a></li>

                        <li class="active"><?php echo $title; ?></li>



                    </ol>

                </div>

            </div>

            <!-- /.col-lg-12 -->

        </div>

        <!-- /.row -->

        <!-- end PAGE TITLE AREA -->









        <!-- Form AREA -->

        <div class="row">

            <div class="col-lg-12">

                 <?php if($this->session->flashdata('success')!=""){ ?>

                    <div class="alert alert-success alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Success!</strong> <?php echo $this->session->flashdata('success');   ?>

                    </div>

                    <?php } if($this->session->flashdata('error')!=""){ ?>

                    <div class="alert alert-danger alert-dismissable">

                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

                    <strong>Error:</strong> <?php echo $this->session->flashdata('error');   ?></div>

                    <?php } ?>

            </div>

            <div class="col-lg-12">

                <div class="panel panel-default">

                    <div class="panel-heading">
                        <a href="<?php echo base_url().'webmanager/customers/agencies'; ?>" class="btn btn-default pull-right">Back</a>

                        <div class="panel-title">
                            <h4><?php echo $title; ?></h4>
                        </div>
                        <div class="clearfix"></div>

                    </div>

                        <div class="panel-body">
                           <p class="text-center">
                        	<img src="<?php echo $this->common->agency_logo($agency_id) ?>" class="" alt="logo" title="logo" style="height: 61px;"/><br /><br />


                           		<a href="#" class="btn btn-primary" onclick="$('#myfile').click(); return false;"><i class="fa fa-upload"></i> Update Logo</a><br /><span class="text-muted">202 x 61px recommended</span>

                           </p>
                        </div>



                    </div>

                </div>


        </div><!--.row-->



</div>


<div class="row hidden">
<form id="myFormAvatar" action="<?php echo base_url().'settings/update_avatar' ?>" method="post" enctype="multipart/form-data">
     <input type="file" size="60" id="myfile" name="myfile" class="myfile-profile" onchange="$('#myFormAvatar').submit();">
     <input type="hidden" id="img_type" name="img_type" value="logo" data-id="">
     <input type="hidden" id="avatar_name" name="avatar_name" value="">
     <input type="hidden" id="agency_idd" name="agency_id" value="<?php echo $agency_id ?>">

     <input type="submit" class="hidden" value="Ajax File Upload">
 </form>
</div>

<!-- Modal -->
<div class="modal fade" id="cropImg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		<p class="lead">Crop</p>

        <div class="bootstrap-modal-cropper"><img src="<?php echo base_url().'assets/img/avatar/background.png' ?>" class="img-responsive"/></div>



      </div>
      <div class="modal-footer">
        <a class="btn btn-primary sav_crop">Save changes</a>
      </div>
    </div>
  </div>
</div>
